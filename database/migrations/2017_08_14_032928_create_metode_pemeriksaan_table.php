<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetodePemeriksaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metode_pemeriksaan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parameter_id');
            $table->string('kode', 4);
            $table->string('metode_pemeriksaan', 150);
            $table->timestamps();

            $table->foreign('parameter_id')->references('id')->on('parameter')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metode_pemeriksaaan');
    }
}

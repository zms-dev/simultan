<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluasiBakteriSaran extends Model
{
   protected $table = 'tb_evaluasi_bakteri_saran';
   protected $fillable = ['*'];
}

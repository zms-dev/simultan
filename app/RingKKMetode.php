<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RingKKMetode extends Model{
    protected $table = 'tb_ring_kk_metode';

    protected $fillable = ['*'];
    public $timestamps = false;
}
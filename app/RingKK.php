<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RingKK extends Model{
    protected $table = 'tb_ring_kk';

    protected $fillable = ['*'];
    public $timestamps = false;
}
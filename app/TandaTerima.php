<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TandaTerima extends Model{
    protected $table = 'tb_tandaterima';
    protected $fillable = ['id_registrasi','tanggal_penerimaan','juklak','bahan_uji','siklus','kondisi_bahan','kode','keterangan','lain_lain','penerima']; 
}
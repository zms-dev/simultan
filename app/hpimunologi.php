<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class hpimunologi extends Model{
    protected $table = 'hp_imunologi';

    protected $fillable = ['id_master_imunologi','tabung','kode_bahan_kontrol','abs_od','cut_off','sco','interpretasi'];

    // protected $table = 'tb_cobaan';
    // protected $fillable = ['id_registrasi','siklus','kode_peserta','tgl_penerimaan','tgl_pemeriksaan','kualitas','kolesterol','trigliserida','jenis','catatan','penanggung_jawab','created_by'];
    public $timestamps = false;
}
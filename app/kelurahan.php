<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class kelurahan extends Model{
    protected $table = 'villages';
    protected $fillable = ['district_id', 'name'];
}
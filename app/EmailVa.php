<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmailVa extends Model{
    protected $table = 'tb_email_va';

    protected $fillable = ['user_id','tahun','siklus'];
    public $timestamps = false;
}
<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RingKKAlat extends Model{
    protected $table = 'tb_ring_kk_alat';

    protected $fillable = ['*'];
    public $timestamps = false;
}
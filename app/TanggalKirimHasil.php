<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TanggalKirimHasil extends Model
{
    protected $table = 'tb_kirim_hasil';
    protected $fillable = ['id_registrasi','siklus','type','tanggal_kirim'];

    public $timestamps = false;
}

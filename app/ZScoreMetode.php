<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZScoreMetode extends Model
{
    protected $table = 'tb_zscore_metode';
    protected $fillable = ['*'];

    public $timestamps = false;
}

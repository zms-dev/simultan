<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Instrumen extends Model
{
    protected $table = 'tb_instrumen';
    protected $fillable = ['instrumen','id_parameter','status'];
    public $timestamps = false;
}

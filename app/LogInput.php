<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogInput extends Model{
    protected $table = 'log_input';
    protected $fillable = ['id_registrasi','status','siklus','type','created_by']; 
}
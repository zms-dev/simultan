<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluasiBakteri extends Model
{
   protected $table = 'tb_evaluasi_bakteri';
   protected $fillable = ['*'];
}

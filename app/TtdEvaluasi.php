<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TtdEvaluasi extends Model{
    protected $table = 'tb_ttd_evaluasi';
    protected $fillable = ['tahun','siklus','image','bidang','nama','nip','tanggal','periode']; 
}
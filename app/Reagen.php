<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reagen extends Model{
    protected $table = 'tb_reagen_imunologi';

    protected $fillable = ['*'];
    public $timestamps = false;
}
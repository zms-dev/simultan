<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class penggunaan extends Model{
    protected $table = 'penggunaan';
    protected $fillable = ['role','video','_token']; 
}
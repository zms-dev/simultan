<?php

namespace App\Http\Controllers;
use DB;
use Request;
use Auth;
use Input;
use PDF;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\masterimunologi as Masterimunologi;
use App\bahanimunologi as Bahanimunologi;
use App\reagenimunologi as Reagenimunologi;
use App\hpimunologi as Hpimunologi;
use App\register as Register;
use App\strategi as Strategi;
use App\kesimpulan as Kesimpulan;
use App\CatatanImun;
use Redirect;
use Validator;
use Session;
use mikehaertl\wkhtmlto\Image;
use Illuminate\Support\Facades\Storage;
use ZipArchive;
use Carbon\Carbon;


class SertifikatController extends Controller
{

    public function cetakall(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $tahun = date('Y', strtotime($tanggal));
        $bidang = DB::table('sub_bidang')->join('tb_bidang','tb_bidang.id','=','sub_bidang.id_bidang')->where('sub_bidang.id','=',$id)->first();
        $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
        ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
        ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
        ->join('tb_sertifikat','tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
        // ->join('hp_headers','hp_headers.id_registrasi','=','tb_registrasi.id')
        ->where(function($query) use ($type)
            {
                    $query->where('tb_registrasi.siklus', '=','12')
                        ->orwhere('tb_registrasi.siklus', '=', $type);
            })
        ->where('tb_sertifikat.siklus','=',$type)
        ->where('tb_sertifikat.tahun','=',$tahun)
        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
        ->where('tb_registrasi.bidang','=',$id)     
        ->where('tb_sertifikat.sub_bidang','=',$id)   
        // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
        ->select('tb_registrasi.kode_lebpes',DB::raw('"adaw" as nama_lab'),DB::raw('GROUP_CONCAT(sub_bidang.parameter) as jenis_form'),'tb_sertifikat.nomor as nomor_sertifikat','tb_registrasi.perusahaan_id','sub_bidang.alias')
        ->groupBy('tb_registrasi.perusahaan_id')
        // ->limit(5)
        ->get();
        $name = $bidang->alias; 
        $url = $bidang->link;
        Storage::deleteDirectory('tmp_sertifikat'.DIRECTORY_SEPARATOR.$name);
        if(file_exists(public_path($name.'.zip'))){
            unlink(public_path($name.'.zip'));
        }

        $make = Storage::makeDirectory('tmp_sertifikat'.DIRECTORY_SEPARATOR.$name);
        $zip = new ZipArchive;
        $zip->open(public_path($name.'.zip'), ZipArchive::CREATE);
        $zip->addEmptyDir($name);
        $zip->close();
        // if($make){
        //     foreach($data as $key => $r){
        //         $no_lab = str_replace("/","-",$r->kode_lebpes);
        //         $url = url($url.'/sertifikat/print/'.$r->perusahaan_id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal'));
        //         // dd($url);
        //         $image = new Image($url);
        //         $image->setOptions([
        //             'binary' => env('PATH_WKHTMLIMAGE', '/usr/local/bin/wkhtmltoimage')
        //         ]);

        //         if(!$image->saveAS(storage_path('app'.DIRECTORY_SEPARATOR.'tmp_sertifikat'.DIRECTORY_SEPARATOR.$name.DIRECTORY_SEPARATOR.$no_lab.'-sertifikat.png'))){
        //             dd($image->getError());
        //         }
        //     }
        //     $zipper = new \Chumper\Zipper\Zipper;
        //     $zipper->make(public_path($name.'.zip'))->add(glob(storage_path('app'.DIRECTORY_SEPARATOR.'tmp_sertifikat'.DIRECTORY_SEPARATOR.$name.DIRECTORY_SEPARATOR.'*')));
        //     $zipper->close();
        // }
        // return response()
        //     ->download(storage_path('app'.DIRECTORY_SEPARATOR.$name.'.zip'));
        return response()->json([
            'data' => $data,
            'bidang' => $bidang,
            'y' => $request->query('y'),
            'x' => $request->query('x'),
            'tanggal' => $request->query('tanggal')
        ]);

        // return storage_path('app'.DIRECTORY_SEPARATOR.$name.'.zip');


        // return view('sertifikat.cetak-all',compact('url','data','type','tanggal'));

    }

    public function cetakallimune(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $tahun = date('Y', strtotime($tanggal));
        $bidang = [
          'alias' => 'imunologi',
          'link'   => '/hasil-pemeriksaan/imunologi'
        ];

        $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
        ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
        ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
        ->join('tb_sertifikat', function($join) use($type, $tahun){
            $join->on('tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
            ->where('tb_sertifikat.bidang', 7)
            ->where('tb_sertifikat.siklus','=',$type)
            ->where('tb_sertifikat.tahun','=',$tahun);
        })
        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
        ->whereIn('tb_registrasi.bidang', [6, 7, 8, 9])
        // ->whereNotIn('perusahaan.id', [496])
        ->select('perusahaan.pemerintah','tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter) as jenis_form'),'tb_sertifikat.nomor as nomor_sertifikat','tb_registrasi.perusahaan_id','sub_bidang.alias')
        ->groupBy('tb_registrasi.perusahaan_id')
        // ->limit(10)
        ->get();
        // dd($data);
        $name = 'imunologi';
        $url = '/hasil-pemeriksaan/imunologi';

        Storage::deleteDirectory('tmp_sertifikat'.DIRECTORY_SEPARATOR.$name);
        if(file_exists(public_path($name.'.zip'))){
            unlink(public_path($name.'.zip'));
        }

        $make = Storage::makeDirectory('tmp_sertifikat'.DIRECTORY_SEPARATOR.$name);
        $zip = new ZipArchive;
        $zip->open(public_path($name.'.zip'), ZipArchive::CREATE);
        $zip->addEmptyDir($name);
        $zip->close();
        return response()->json([
            'data' => $data,
            'bidang' => $bidang,
            'y' => $request->query('y'),
            'x' => $request->query('x'),
            'tanggal' => $request->query('tanggal')
        ]);
    }

    public function cetakallmikro(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $tahun = date('Y', strtotime($tanggal));
        // if (Auth::user()->penyelenggara == 4) {
        //     $bidang = [
        //       'alias' => 'mikrobiologi',
        //       'link'   => '/hasil-pemeriksaan/mikrobiologibta'
        //     ];
        //     $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
        //     ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
        //     ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
        //     ->join('tb_sertifikat', function($join) use($type){
        //         $join->on('tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
        //         ->whereIn('tb_sertifikat.sub_bidang', [4])
        //         ->where('tb_sertifikat.siklus','=',$type);
        //     })
        //     ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
        //     ->whereIn('tb_registrasi.bidang', [4])
        //     // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
        //     ->select('tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter) as jenis_form'),'tb_sertifikat.nomor as nomor_sertifikat','tb_registrasi.perusahaan_id','sub_bidang.alias')
        //     ->groupBy('tb_registrasi.perusahaan_id')
        //     // ->limit(5)
        //     ->get();

        //     $name = 'mikrobiologi';
        //     $url = 'hasil-pemeriksaan/mikrobiologibta';
        // }else{
            $bidang = [
              'alias' => 'mikrobiologi',
              'link'   => '/hasil-pemeriksaan/mikrobiologi'
            ];
            $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->join('tb_sertifikat', function($join) use($type, $tahun){
                $join->on('tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
                ->whereIn('tb_sertifikat.bidang', [6])
                ->where('tb_sertifikat.siklus','=',$type)
                ->where('tb_sertifikat.tahun','=',$tahun);
            })
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
            ->whereIn('tb_registrasi.bidang', [4, 5 ,10])
            // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
            ->select('tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter SEPARATOR ", ") as jenis_form'),'tb_sertifikat.nomor as nomor_sertifikat','tb_registrasi.perusahaan_id','sub_bidang.alias')
            ->groupBy('tb_registrasi.perusahaan_id')
            // ->limit(5)
            ->get();

            $name = 'mikrobiologi';
            $url = '/hasil-pemeriksaan/mikrobiologi';
        // }

        Storage::deleteDirectory('tmp_sertifikat'.DIRECTORY_SEPARATOR.$name);
        if(file_exists(public_path($name.'.zip'))){
            unlink(public_path($name.'.zip'));
        }

        $make = Storage::makeDirectory('tmp_sertifikat'.DIRECTORY_SEPARATOR.$name);
        $zip = new ZipArchive;
        $zip->open(public_path($name.'.zip'), ZipArchive::CREATE);
        $zip->addEmptyDir($name);
        $zip->close();
        return response()->json([
            'data' => $data,
            'bidang' => $bidang,
            'y' => $request->query('y'),
            'x' => $request->query('x'),
            'tanggal' => $request->query('tanggal')
        ]);
    }

    public function cetakallbac(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $tahun = date('Y', strtotime($request->tanggal));

        $bidang = [
          'alias' => 'antibiotik',
          'link'   => '/hasil-pemeriksaan/antibiotik'
        ];
        $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
        ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
        ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
        ->join('tb_sertifikat', function($join) use($type, $tahun){
            $join->on('tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
            ->whereIn('tb_sertifikat.bidang', [6])
            ->where('tb_sertifikat.siklus','=',$type)
            ->where('tb_sertifikat.tahun','=',$tahun);
        })
        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
        ->whereIn('tb_registrasi.bidang', [13])
        // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
        ->select('tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter SEPARATOR ", ") as jenis_form'),'tb_sertifikat.nomor as nomor_sertifikat','tb_registrasi.perusahaan_id','sub_bidang.alias')
        ->groupBy('tb_registrasi.perusahaan_id')
        // ->limit(2)
        ->get();

        $name = 'antibiotik';
        $url = '/hasil-pemeriksaan/antibiotik';

        Storage::deleteDirectory('tmp_sertifikat'.DIRECTORY_SEPARATOR.$name);
        if(file_exists(public_path($name.'.zip'))){
            unlink(public_path($name.'.zip'));
        }

        $make = Storage::makeDirectory('tmp_sertifikat'.DIRECTORY_SEPARATOR.$name);
        $zip = new ZipArchive;
        $zip->open(public_path($name.'.zip'), ZipArchive::CREATE);
        $zip->addEmptyDir($name);
        $zip->close();
        return response()->json([
            'data' => $data,
            'bidang' => $bidang,
            'y' => $request->query('y'),
            'x' => $request->query('x'),
            'tanggal' => $request->query('tanggal')
        ]);
    }

    public function createzip(\Illuminate\Http\Request $request){
        $bidang = $request->bidang;
        $name = $bidang['alias'];
        $url = $bidang['link'];
        // $zipper = new \Chumper\Zipper\Zipper;
        // $zipper->make(public_path($name.'.zip'))->add(glob(storage_path('app'.DIRECTORY_SEPARATOR.'tmp_sertifikat'.DIRECTORY_SEPARATOR.$name.DIRECTORY_SEPARATOR.'*')));
        // $zipper->close();
        // $zip = new ZipArchive;
        // if ($zip->open(public_path($name.'.zip'), ZipArchive::CREATE) === TRUE) {
        //     // Add File in ZipArchive
        //     $nodes = glob(storage_path('app'.DIRECTORY_SEPARATOR.'tmp_sertifikat'.DIRECTORY_SEPARATOR.$name.DIRECTORY_SEPARATOR.'*'))   ;
        //     foreach ($nodes as $node) {
        //         if (is_dir($node)) {
        //             $zip->addDir($node);
        //         } else if (is_file($node))  {
        //             $zip->addFile($node);
        //         }
        //     }
        //     // Close ZipArchive
        //     $zip->close();
            return response()->json([
                'url' => url($name.'.zip'),
            ]);
        // }


    }

    public function cetakoneby(\Illuminate\Http\Request $request){
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $tahun = date('Y', strtotime($request->tanggal));
        $r      = $request->r;
        $bidang = $request->bidang;
        $name = $bidang['alias'];
        $url = $bidang['link'];
        $no_lab = str_replace("/","-",$r['kode_lebpes']);
        $url = url($url.'/sertifikat/print/'.$r['perusahaan_id'].'?x='.$request->query('x').'&y='.$request->query('y').'&tahun='.$tahun.'&tanggal='.$request->query('tanggal'));
        $image = new Image($url);
        $image->setOptions([
            'binary' => env('PATH_WKHTMLIMAGE', '/usr/local/bin/wkhtmltoimage')
        ]);

        if(!$image->saveAS(storage_path('app'.DIRECTORY_SEPARATOR.'tmp_sertifikat'.DIRECTORY_SEPARATOR.$name.DIRECTORY_SEPARATOR.$no_lab.'-sertifikat.png'))){
            return response()->json([
                'code' => 50,
                'message' => $image->getError()
            ]);
        } else{
            $zip = new ZipArchive;
            $zip->open(public_path($name.'.zip'));
            $zip->addFile(storage_path('app'.DIRECTORY_SEPARATOR.'tmp_sertifikat'.DIRECTORY_SEPARATOR.$name.DIRECTORY_SEPARATOR.$no_lab.'-sertifikat.png'),$name.'/'.$no_lab.'-sertifikat.png');
            $zip->close();
            return response()->json([
                'code' => 200,
            ]);
        }
    }

    public function cetakimun(\Illuminate\Http\Request $request, $id){
        app('debugbar')->disable();
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $tahun = date('Y', strtotime($tanggal));
        if ($request->query('download') == 'true') {
            $url = url('hasil-pemeriksaan/imunologi/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal'));
            $image = new Image($url);
            $image->setOptions([
                'binary' => env('PATH_WKHTMLIMAGE', '/usr/local/bin/wkhtmltoimage')
            ]);

            if(!$image->send('imunologi Sertifikat.png')){
                dd($image->getError());
            }
        }else{
        $data = DB::table('tb_registrasi') 
            ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->join('tb_sertifikat', function($join) use($type, $tahun){
                $join->on('tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
                ->where('tb_sertifikat.bidang','=', 7)
                ->where('tb_sertifikat.siklus','=',$type)
                ->where('tb_sertifikat.tahun','=',$tahun);
            })
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
            // ->where(function($query) use ($type){
            //     if ($type == 1) {
            //         $query->where('tb_registrasi.siklus_1','=','done');
            //     }else{
            //         $query->where('tb_registrasi.siklus_2','=','done');
            //     }
            // })
            ->whereIn('tb_registrasi.bidang',array(6,7,8,9))
            ->where('tb_registrasi.perusahaan_id',$id)
            ->select('perusahaan.pemerintah','perusahaan.id','tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter) as jenis_form'),'tb_sertifikat.nomor as nomor_sertifikat')
            ->groupBy('tb_registrasi.perusahaan_id')
            ->get();
            foreach ($data as $key => $val) {
                $input = DB::table('master_imunologi')
                            ->join('tb_registrasi','tb_registrasi.id','=','master_imunologi.id_registrasi')
                            ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                            ->join('hp_imunologi','hp_imunologi.id_master_imunologi','=','master_imunologi.id')
                            ->where('perusahaan_id', $val->id)
                            ->where('hp_imunologi.interpretasi','!=','Tanpa test')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->where('master_imunologi.siklus', $type)
                            ->groupBy('tb_registrasi.id')
                            ->select('sub_bidang.parameter')
                            ->get();
                $val->input = $input;
            }
            // dd($data);
            if ($id == '32' && $type == 1 && $tahun == 2018) {
                $data->jenis_form = 'Sifilis,HBsAg';
            }elseif($id == '169' && $type == 2 && $tahun == 2018){
                $data->jenis_form = 'Anti HIV,HBsAg';
            }

            return view('evaluasi.imunologi.sertifikat.hbsag', compact('id', 'data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal',''));
        }
    }

    public function cetakmikro(\Illuminate\Http\Request $request, $id){
        app('debugbar')->disable();
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $tahun = date('Y', strtotime($tanggal));

        if ($request->query('download') == 'true') {
           $url = url('hasil-pemeriksaan/antibiotik/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal'));
           $image = new Image($url);
           $image->setOptions([
               'binary' => env('PATH_WKHTMLIMAGE', '/usr/local/bin/wkhtmltoimage')
           ]);

           if(!$image->send('Mikrobilogi Sertifikat.png')){
               dd($image->getError());
           }
       }else{
       $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
           ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
           ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
           ->join('tb_sertifikat', function($join) use($type, $tahun){
               $join->on('tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
               ->whereIn('tb_sertifikat.bidang',[6])
               ->where('tb_sertifikat.siklus','=',$type)
               ->where('tb_sertifikat.tahun','=',$tahun);
           })
           ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
           ->whereIn('tb_registrasi.bidang',array(4,5,10))
           ->where('tb_registrasi.perusahaan_id',$id)
           // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
           ->select('tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter SEPARATOR ", ") as jenis_form'),'tb_sertifikat.nomor as nomor_sertifikat')
           ->groupBy('tb_registrasi.perusahaan_id')
           ->get();

           return view('evaluasi.imunologi.sertifikat.mikrologi', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal'));
       }
    }

    public function cetakhbsag(\Illuminate\Http\Request $request, $id){
        app('debugbar')->disable();
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $tahun = date('Y', strtotime($tanggal));
        if ($request->query('download') == 'true') {
            $url = url('hasil-pemeriksaan/anti-hiv/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal'));
            $image = new Image($url);
            $image->setOptions([
                'binary' => env('PATH_WKHTMLIMAGE', '/usr/local/bin/wkhtmltoimage')
            ]);

            if(!$image->send('imunologi Sertifikat.png')){
                dd($image->getError());
            }
        }else{
        $data = DB::table('tb_registrasi') 
            ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->join('tb_sertifikat', function($join) use($type, $tahun){
                $join->on('tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
                ->where('tb_sertifikat.bidang','=', 7)
                ->where('tb_sertifikat.siklus','=',$type)
                ->where('tb_sertifikat.tahun','=',$tahun);
            })
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
            // ->where(function($query) use ($type){
            //     if ($type == 1) {
            //         $query->where('tb_registrasi.siklus_1','=','done');
            //     }else{
            //         $query->where('tb_registrasi.siklus_2','=','done');
            //     }
            // })
            ->whereIn('tb_registrasi.bidang',array(6,7,8,9))
            ->where('tb_registrasi.perusahaan_id',$id)
            ->select('perusahaan.pemerintah','perusahaan.id','tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter) as jenis_form'),'tb_sertifikat.nomor as nomor_sertifikat')
            ->groupBy('tb_registrasi.perusahaan_id')
            ->get();
            foreach ($data as $key => $val) {
                $input = DB::table('master_imunologi')
                            ->join('tb_registrasi','tb_registrasi.id','=','master_imunologi.id_registrasi')
                            ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                            ->join('hp_imunologi','hp_imunologi.id_master_imunologi','=','master_imunologi.id')
                            ->where('perusahaan_id', $val->id)
                            ->where('hp_imunologi.interpretasi','!=','Tanpa test')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->where('master_imunologi.siklus', $type)
                            ->groupBy('tb_registrasi.id')
                            ->select('sub_bidang.parameter')
                            ->get();
                $val->input = $input;
            }
          
            if ($id == '32' && $type == 1 && $tahun == 2018) {
                $data->jenis_form = 'Sifilis,HBsAg';
            }elseif($id == '169' && $type == 2 && $tahun == 2018){
                $data->jenis_form = 'Anti HIV,HBsAg';
            }
            return view('evaluasi.imunologi.sertifikat.hbsag', compact('id', 'data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal'));
        }

    }

    public function btana(\Illuminate\Http\Request $request, $id){
        app('debugbar')->disable();
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $tahun = date('Y', strtotime($tanggal));

        if ($request->query('download') == 'true') {
             $url = url('hasil-pemeriksaan/bta/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal'));
            $image = new Image($url);
            $image->setOptions([
                'binary' => env('PATH_WKHTMLIMAGE', '/usr/local/bin/wkhtmltoimage')
            ]);

            if(!$image->send('Mikrobilogi Sertifikat.png')){
                dd($image->getError());
            }
        }else{
        $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->join('tb_sertifikat', function($join) use($type, $tahun){
                $join->on('tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
                ->where('tb_sertifikat.sub_bidang','=', 4)
                ->where('tb_sertifikat.siklus','=',$type)
                ->where('tb_sertifikat.tahun','=',$tahun);
            })
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
            ->whereIn('tb_registrasi.bidang',array(4))
            ->where('tb_registrasi.perusahaan_id',$id)
            ->select('tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter) as jenis_form'),'tb_sertifikat.nomor as nomor_sertifikat')
            ->groupBy('tb_registrasi.perusahaan_id')
            ->get();

            return view('evaluasi.imunologi.sertifikat.mikrobta', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal'));
        }
        // if (count($kesimpulan) > 0) {
        // $pdf = PDF::loadview('evaluasi.imunologi.sertifikat.mikrologi', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal'))
        //     ->setPaper('a4', 'landscape')
        //     ->setwarnings(false);
        // return $pdf->stream('Sertifikat Mikroskopis.pdf');
    }

    public function bta(\Illuminate\Http\Request $request, $id){
        app('debugbar')->disable();
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $tahun = $request->get('tahun');

        if ($request->query('download') == 'true') {
             $url = url('hasil-pemeriksaan/telur-cacing/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tahun='.$request->query('tahun').'&tanggal='.$request->query('tanggal'));
            $image = new Image($url);
            $image->setOptions([
                'binary' => env('PATH_WKHTMLIMAGE', '/usr/local/bin/wkhtmltoimage')
            ]);

            if(!$image->send('Mikrobilogi Sertifikat.png')){
                dd($image->getError());
            }
        }else{
        $data = DB::table('tb_registrasi')->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->join('tb_sertifikat', 'tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
            ->where('tb_sertifikat.bidang','=', 6)
            ->where('tb_sertifikat.siklus', '=', $type)
            ->where('tb_sertifikat.tahun', '=', $tahun)
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
            ->whereIn('tb_registrasi.bidang',array(4, 5, 10))
            ->where('tb_registrasi.perusahaan_id',$id)
            ->select('tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter SEPARATOR ", ") as jenis_form'),'tb_sertifikat.nomor as nomor_sertifikat')
            ->groupBy('tb_registrasi.perusahaan_id')
            ->get();
        // dd($tahun);
            return view('evaluasi.imunologi.sertifikat.mikrologi', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal'));
        }
        // if (count($kesimpulan) > 0) {
        // $pdf = PDF::loadview('evaluasi.imunologi.sertifikat.mikrologi', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal'))
        //     ->setPaper('a4', 'landscape')
        //     ->setwarnings(false);
        // return $pdf->stream('Sertifikat Mikroskopis.pdf');
    }

     public function bac(\Illuminate\Http\Request $request, $id){
        app('debugbar')->disable();
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $tahun = $request->get('tahun');

        if ($request->query('download') == 'true') {
            $url = url('hasil-pemeriksaan/antibiotik/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal').'&tahun='.$request->query('tahun'));
            $image = new Image($url);
            $image->setOptions([
                'binary' => env('PATH_WKHTMLIMAGE', '/usr/local/bin/wkhtmltoimage')
            ]);

            if(!$image->send('Mikrobilogi Sertifikat.png')){
                dd($image->getError());
            }
        }else{
        $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->join('tb_sertifikat', function($join) use($type, $tahun){
                $join->on('tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
                ->where('tb_sertifikat.bidang','=',6)
                ->where('tb_sertifikat.siklus','=',$type)
                ->where('tb_sertifikat.tahun','=',$tahun);
            })
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
            ->whereIn('tb_registrasi.bidang',array(13))
            ->where('tb_registrasi.perusahaan_id',$id)
            // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
            ->select('tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter) as jenis_form'),'tb_sertifikat.nomor as nomor_sertifikat')
            ->groupBy('tb_registrasi.perusahaan_id')
            ->get();
        // dd($data);

            return view('evaluasi.imunologi.sertifikat.identifikasi', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal'));
        }
    }

    public function hem(\Illuminate\Http\Request $request, $id){
        app('debugbar')->disable();
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $tahun = date('Y', strtotime($tanggal));
        $no_lab = $request->no_lab;

        if ($request->query('download') == 'true') {
            $url = url('hasil-pemeriksaan/hematologi/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal'));
            $image = new Image($url);
            $image->setOptions([
                'binary' => env('PATH_WKHTMLIMAGE', '/usr/local/bin/wkhtmltoimage')
            ]);

            if(!$image->send($no_lab.'Hematologi-Sertifikat.png')){
                dd($image->getError());
            }
        }else{
        $param = DB::table('parameter')
              ->leftjoin('hp_details', 'hp_details.parameter_id', '=', 'parameter.id')
              ->leftjoin('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
              ->join('tb_registrasi', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
              ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
              ->where('tb_registrasi.perusahaan_id', '=', $id)
              ->where('hp_headers.kategori', '=', 'hematologi')
              ->where('hp_headers.siklus','=', $type)
              ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $tahun)
              ->whereNotIn('hp_details.hasil_pemeriksaan',['-', '0', '0.0', '0.00'])
              ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
              ->select('parameter.nama_parameter')
              ->groupBy('parameter.id')
              ->orderBy('parameter.bagian', 'ASC')
              ->get();

        $data = DB::table('tb_registrasi')
              ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
              ->join('tb_sertifikat', 'tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
              ->where('tb_sertifikat.sub_bidang','=', 1)
              ->where('tb_sertifikat.siklus', '=', $type)
              ->where('tb_sertifikat.tahun', '=', $tahun)
              ->where('tb_registrasi.perusahaan_id', '=', $id)
              ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $tahun)
              ->select('tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'tb_sertifikat.nomor as nomor_sertifikat')
              ->limit('1')
              ->get();
        //------validasi 2018--------
        // $data = DB::table('parameter')
        //       ->join('hp_details', 'hp_details.parameter_id', '=', 'parameter.id')
        //       ->join('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
        //       ->join('tb_registrasi', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
        //       ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
        //       ->join('tb_sertifikat', 'tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
        //       ->where('tb_sertifikat.sub_bidang','=', 1)
        //       ->where('tb_sertifikat.siklus', '=', $type)
        //       ->where('tb_sertifikat.tahun', '=', $tahun)
        //       ->where('tb_registrasi.perusahaan_id', '=', $id)
        //       ->where('hp_headers.siklus','=', $type)
        //       ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $tahun)
        //       ->where('hp_headers.kategori', '=', 'hematologi')
        //       ->whereNotIn('hp_details.hasil_pemeriksaan',['-', '0', '0.0', '0.00'])
        //       ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
        //       ->select('tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'tb_sertifikat.nomor as nomor_sertifikat')
        //       ->groupBy('parameter.id')
        //       ->orderBy('parameter.id', 'ASC')
        //       ->limit('1')
        //       ->get();
              // dd($param);
              return view('evaluasi.imunologi.sertifikat.hem', compact('param', 'data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal'));
        }
    }

    public function kimiaklinik(\Illuminate\Http\Request $request, $id){
        app('debugbar')->disable();
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $tahun = date('Y', strtotime($tanggal));


        if($request->query('download') == 'true'){

            $url = url('hasil-pemeriksaan/kimia-klinik/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal'));
            $image = new Image($url);
            $image->setOptions([
                'binary' => env('PATH_WKHTMLIMAGE', '/usr/local/bin/wkhtmltoimage')
            ]);

            if(!$image->send('Kimia-Klinik Sertifikat.png')){
                dd($image->getError());
            }

        }else{

            $param = DB::table('parameter')
                  ->join('hp_details', 'hp_details.parameter_id', '=', 'parameter.id')
                  ->join('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                  ->join('tb_registrasi', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
                  ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                  ->where('tb_registrasi.perusahaan_id', '=', $id)
                  ->where('hp_headers.siklus','=', $type)
                  ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $tahun)
                  ->where('hp_headers.kategori', '=', 'kimia klinik')
                  ->whereNotIn('hp_details.hasil_pemeriksaan',['-', '0', '0.0', '0.00'])
                  ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
                  ->select('parameter.nama_parameter')
                  ->groupBy('parameter.id')
                  ->orderBy('parameter.id', 'ASC')
                  ->get();

            $data = DB::table('tb_registrasi')
                  ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                  ->join('tb_sertifikat', 'tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
                  ->where('tb_sertifikat.sub_bidang','=', 2)
                  ->where('tb_sertifikat.siklus', '=', $type)
                  ->where('tb_sertifikat.tahun', '=', $tahun)
                  ->where('tb_registrasi.perusahaan_id', '=', $id)
                  ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $tahun)
                  ->select('tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'tb_sertifikat.nomor as nomor_sertifikat')
                  ->limit('1')
                  ->get();
            //------validasi 2018--------
            // $data = DB::table('parameter')
            //       ->join('hp_details', 'hp_details.parameter_id', '=', 'parameter.id')
            //       ->join('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
            //       ->join('tb_registrasi', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
            //       ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
            //       ->join('tb_sertifikat', 'tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
            //       ->where('tb_sertifikat.sub_bidang','=', 2)
            //       ->where('tb_sertifikat.siklus', '=', $type)
            //       ->where('tb_sertifikat.tahun', '=', $tahun)
            //       ->where('tb_registrasi.perusahaan_id', '=', $id)
            //       ->where('hp_headers.siklus','=', $type)
            //       ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $tahun)
            //       ->where('hp_headers.kategori', '=', 'kimia klinik')
            //       ->whereNotIn('hp_details.hasil_pemeriksaan',['-', '0', '0.0', '0.00'])
            //       ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
            //       ->select('tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'tb_sertifikat.nomor as nomor_sertifikat')
            //       ->groupBy('parameter.id')
            //       ->orderBy('parameter.id', 'ASC')
            //       ->limit('1')
            //       ->get();
            // dd($data);

            return view('evaluasi.imunologi.sertifikat.kimiaklnik', compact('param', 'data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal'));
        }

    }

    public function uri(\Illuminate\Http\Request $request, $id){
        app('debugbar')->disable();
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $tahun = date('Y', strtotime($tanggal));

        if ($request->query('download') == "true") {
             $url = url('hasil-pemeriksaan/urinalisasi/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal'));
            $image = new Image($url);
            $image->setOptions([
                'binary' => env('PATH_WKHTMLIMAGE', '/usr/local/bin/wkhtmltoimage')
            ]);

            if(!$image->send('Urinalisasi Sertifikat.png')){
                dd($image->getError());
            }
        }else{
                $param = DB::table('parameter')
                      ->join('hp_details', 'hp_details.parameter_id', '=', 'parameter.id')
                      ->join('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                      ->join('tb_registrasi', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
                      ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                      ->where('tb_registrasi.perusahaan_id', '=', $id)
                      ->where('hp_headers.kategori', '=', 'urinalisa')
                      ->where('hp_headers.siklus','=', $type)
                      ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $tahun)
                      ->whereNotIn('hp_details.hasil_pemeriksaan',['-', '0', '0.0', '0.00'])
                      ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
                      ->select('parameter.nama_parameter')
                      ->groupBy('parameter.id')
                      ->orderBy('parameter.id', 'ASC')
                      ->get();

                $data = DB::table('tb_registrasi')
                      ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                      ->join('tb_sertifikat', 'tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
                      ->where('tb_sertifikat.sub_bidang','=', 3)
                      ->where('tb_sertifikat.siklus', '=', $type)
                      ->where('tb_sertifikat.tahun', '=', $tahun)
                      ->where('tb_registrasi.perusahaan_id', '=', $id)
                      ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $tahun)
                      ->select('tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'tb_sertifikat.nomor as nomor_sertifikat')
                      ->limit('1')
                      ->get();
        //------validasi 2018--------
                // $data = DB::table('parameter')
                //       ->join('hp_details', 'hp_details.parameter_id', '=', 'parameter.id')
                //       ->join('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                //       ->join('tb_registrasi', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
                //       ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                //       ->join('tb_sertifikat', 'tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
                //       ->where('tb_sertifikat.sub_bidang','=', 3)
                //       ->where('tb_sertifikat.siklus', '=', $type)
                //       ->where('tb_sertifikat.tahun', '=', $tahun)
                //       ->where('tb_registrasi.perusahaan_id', '=', $id)
                //       ->where('hp_headers.kategori', '=', 'urinalisa')
                //       ->where('hp_headers.siklus','=', $type)
                //       ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $tahun)
                //       ->whereNotIn('hp_details.hasil_pemeriksaan',['-', '0', '0.0', '0.00'])
                //       ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
                //       ->select('tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'tb_sertifikat.nomor as nomor_sertifikat')
                //       ->groupBy('parameter.id')
                //       ->orderBy('parameter.id', 'ASC')
                //       ->limit('1')
                //       ->get();

                return view('evaluasi.imunologi.sertifikat.Uri', compact('param', 'data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal'));
        }
    }

    public function kimkes(\Illuminate\Http\Request $request, $id){
        app('debugbar')->disable();
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $tahun = date('Y', strtotime($tanggal));

        if ($request->query('download') == 'true') {
            $url = url('hasil-pemeriksaan/kimia-air/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal'));
            $image = new Image($url);
            $image->setOptions([
                'binary' => env('PATH_WKHTMLIMAGE', '/usr/local/bin/wkhtmltoimage')
            ]);

            if(!$image->send('Kimia Air Sertifikat.png')){
                dd($image->getError());
            }
        }else{
            $param = DB::table('parameter')
                  ->join('hp_details', 'hp_details.parameter_id', '=', 'parameter.id')
                  ->join('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                  ->join('tb_registrasi', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
                  ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                  ->where('tb_registrasi.perusahaan_id', '=', $id)
                  ->where('parameter.kategori', '=', 'kimia air')
                  ->where('parameter.status', '=', '1')
                  ->whereNotIn('hp_details.hasil_pemeriksaan',['-'])
                  ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
                  ->select('parameter.nama_parameter')
                  ->groupBy('parameter.id')
                  ->orderBy('parameter.id', 'ASC')
                  ->get();

            $data = DB::table('parameter')
                  ->join('hp_details', 'hp_details.parameter_id', '=', 'parameter.id')
                  ->join('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                  ->join('tb_registrasi', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
                  ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                  ->join('tb_sertifikat', 'tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
                  ->where('tb_sertifikat.sub_bidang','=', 11)
                  ->where('tb_sertifikat.siklus', '=', $type)
                  ->where('tb_sertifikat.tahun', '=', $tahun)
                  ->where('tb_registrasi.perusahaan_id', '=', $id)
                  ->where('parameter.kategori', '=', 'kimia air')
                  ->where('parameter.status', '=', '1')
                  ->whereNotIn('hp_details.hasil_pemeriksaan',['-'])
                  ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
                  ->select('tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'tb_sertifikat.nomor as nomor_sertifikat')
                  ->groupBy('parameter.id')
                  ->orderBy('parameter.id', 'ASC')
                  ->limit('1')
                  ->get();
            // dd($param);
            return view('evaluasi.imunologi.sertifikat.kimkes', compact('param', 'data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal'));
        }
    }

    public function kimkesterbatas(\Illuminate\Http\Request $request, $id){
        app('debugbar')->disable();
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $tahun = date('Y', strtotime($tanggal));

        if ($request->query('download') == 'true') {
            $url = url('hasil-pemeriksaan/kimia-air-terbatas/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal'));
            $image = new Image($url);
            $image->setOptions([
                'binary' => env('PATH_WKHTMLIMAGE', '/usr/local/bin/wkhtmltoimage')
            ]);

            if(!$image->send('Kimia Air Terbatas Sertifikat.png')){
                dd($image->getError());
            }
        }else{
        // $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
        //     ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
        //     ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
        //     ->join('tb_sertifikat', function($join) use($type){
        //         $join->on('tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
        //         ->where('tb_sertifikat.bidang','=',5)
        //         ->where('tb_sertifikat.siklus','=',$type);
        //     })
        //     ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
        //     ->whereIn('tb_registrasi.bidang',array(11,12))
        //     ->where('tb_registrasi.perusahaan_id',$id)
        //     // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
        //     ->select('tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter) as jenis_form'),'tb_sertifikat.nomor as nomor_sertifikat')
        //     ->groupBy('tb_registrasi.perusahaan_id')
        //     ->get();

            $param = DB::table('parameter')
                  ->join('hp_details', 'hp_details.parameter_id', '=', 'parameter.id')
                  ->join('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                  ->join('tb_registrasi', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
                  ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                  ->where('tb_registrasi.perusahaan_id', '=', $id)
                  ->where('parameter.kategori', '=', 'kimia air terbatas')
                  ->whereNotIn('hp_details.hasil_pemeriksaan',['-'])
                  ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
                  ->select('parameter.nama_parameter')
                  ->groupBy('parameter.id')
                  ->orderBy('parameter.id', 'ASC')
                  ->get();

            $data = DB::table('parameter')
                  ->join('hp_details', 'hp_details.parameter_id', '=', 'parameter.id')
                  ->join('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                  ->join('tb_registrasi', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
                  ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                  ->join('tb_sertifikat', 'tb_sertifikat.perusahaan_id', '=', 'tb_registrasi.perusahaan_id')
                  ->where('tb_sertifikat.sub_bidang','=', 12)
                  ->where('tb_sertifikat.siklus', '=', $type)
                  ->where('tb_sertifikat.tahun', '=', $tahun)
                  ->where('tb_registrasi.perusahaan_id', '=', $id)
                  ->where('parameter.kategori', '=', 'kimia air terbatas')
                  ->whereNotIn('hp_details.hasil_pemeriksaan',['-'])
                  ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
                  ->select('tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'tb_sertifikat.nomor as nomor_sertifikat')
                  ->groupBy('parameter.id')
                  ->orderBy('parameter.id', 'ASC')
                  ->limit('1')
                  ->get();

            return view('evaluasi.imunologi.sertifikat.kimkesterbatas', compact('param', 'data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal'));
        }
    }
}

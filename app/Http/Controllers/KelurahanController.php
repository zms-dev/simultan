<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\kelurahan as Kelurahan;
use Illuminate\Support\Facades\Redirect;

class KelurahanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request)
    {
        // $data = DB::table('districts')
        //         ->join('regencies', 'districts.regency_id', '=', 'regencies.id')
        //         ->select('districts.*', 'regencies.name as Name')
        //         ->get();
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = Kelurahan::select( DB::raw('*, villages.id as Id, @rownum := @rownum +1 as rownum'));
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="kelurahan/edit/'.$data->Id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        <i class="icon-pencil"></i>
                                    </button>
                                </a> &nbsp;
                                <a href="kelurahan/delete'.'/'.$data->Id.'">
                                    <button class="btn btn-danger btn-xs">
                                        <i class="icon-trash "></i>
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('back/kelurahan/index');
    }

    public function in()
    {
        $data = DB::table('districts')->get();
        return view('back/kelurahan/insert', compact('data'));
    }

    public function edit($id)
    {
        $data = DB::table('villages')
                ->join('districts', 'villages.district_id', '=', 'districts.id')
                ->select('villages.*', 'districts.name as Name')
                ->where('villages.id', $id)
                ->get();
        $districts = DB::table('districts')->get();
        return view('back/kelurahan/update', compact('data','districts'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $data['name'] = $request->name;
        Kelurahan::where('id',$id)->update($data);
        Session::flash('message', 'Data Kelurahan Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/kelurahan');
    }    

    public function insert(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        Kelurahan::create($data);
        Session::flash('message', 'Data Kelurahan Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/kelurahan');
    }

    public function delete($id){
        Session::flash('message', 'Data Kelurahan Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Kelurahan::find($id)->delete();
        return redirect("admin/kelurahan");
    }
}

<?php
namespace App\Http\Controllers;

use Request;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
use Session;
use PDF;
use App\UploadLaporan;
use App\UploadLaporanIdentifikasi;
use Validator;
use Response;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;


class UploadLaporanAkhirController extends Controller
{
	public function index(\Illuminate\Http\Request $request){
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias, @rownum := @rownum +1 as rownum'))
                        ->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                        ->where('sub_bidang.id','=',Auth::user()->penyelenggara)
                        ->orderBy('id','DESC');
            if(!empty(Auth::user()->badan_usaha)){
                $datas->where('upload_lap_akhir.badan_usaha','=',1);
            }
            $datas = $datas->get();
            return Datatables::of($datas)

            ->addColumn('action', function($data){
                return "".'
                        <select name="forma" onchange="location = this.value;" class="form-control">
                            <option></option>
                            <option value="upload-laporan-akhir/edit'.'/'.$data->id.'">Edit</option>
                            <option value="upload-laporan-akhir/download'.'/'.$data->id.'">Download</option>
                            <option value="upload-laporan-akhir/delete'.'/'.$data->id.'">Hapus</option>
                        </select>'
            ;})
            ->make(true);


        }
        return view('evaluasi/imunologi/spl/index');
    }

    public function pendahuluan(\Illuminate\Http\Request $request){
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')->where('sub_bidang.id','=',Auth::user()->penyelenggara)->where('upload_lap_akhir.bagian','=','Pendahuluan')->orderBy('id','DESC')->get();
            return Datatables::of($datas)

            ->addColumn('action', function($data){
                return "".'<a href="edit'.'/'.$data->id.'" style="float: left; padding:10%;">
                                    <button class="btn btn-primary btn-xs">
                                        Edit
                                    </button>
                                </a> &nbsp;
                                <a href="delete'.'/'.$data->id.'">
                                    <button class="btn btn-danger btn-xs">
                                        Hapus
                                    </button>
                                </a>'
                        ;})
            ->make(true);


        }
        return view('evaluasi/imunologi/spl/pendahuluan');
    }


    public function isi(\Illuminate\Http\Request $request){
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')->where('sub_bidang.id','=',Auth::user()->penyelenggara)->where('upload_lap_akhir.bagian','=','Isi')->orderBy('id','DESC')->get();
            return Datatables::of($datas)

            ->addColumn('action', function($data){
                return "".'<a href="edit'.'/'.$data->id.'" style="float: left; padding:10%;">
                                    <button class="btn btn-primary btn-xs">
                                        Edit
                                    </button>
                                </a> &nbsp;
                                <a href="delete'.'/'.$data->id.'">
                                    <button class="btn btn-danger btn-xs">
                                        Hapus
                                    </button>
                                </a>'
                        ;})
            ->make(true);


        }
        return view('evaluasi/imunologi/spl/isi');
    }

    public function penutup(\Illuminate\Http\Request $request){
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')->where('sub_bidang.id','=',Auth::user()->penyelenggara)->where('upload_lap_akhir.bagian','=','Penutup')->orderBy('id','DESC')->get();
            return Datatables::of($datas)

            ->addColumn('action', function($data){
                return "".'<a href="edit'.'/'.$data->id.'" style="float: left; padding:10%;">
                                    <button class="btn btn-primary btn-xs">
                                        Edit
                                    </button>
                                </a> &nbsp;
                                <a href="delete'.'/'.$data->id.'">
                                    <button class="btn btn-danger btn-xs">
                                        Hapus
                                    </button>
                                </a>'
                        ;})
            ->make(true);


        }
        return view('evaluasi/imunologi/spl/penutup');
    }

    public function upload()
    {
        $parameter = DB::table('sub_bidang')->where('sub_bidang.id','=',Auth::user()->penyelenggara)->get();
        return view('evaluasi/imunologi/spl/upload', compact('parameter'));
    }
		public function download($id)
    {
        // $parameter = DB::table('sub_bidang')->where('sub_bidang.id','=',Auth::user()->penyelenggara)->get();
        $data = DB::table('upload_lap_akhir')
                ->leftjoin('sub_bidang', 'sub_bidang.id', '=', 'upload_lap_akhir.parameter')
                ->select('upload_lap_akhir.*' ,'sub_bidang.alias as alias','sub_bidang.id as ids')
                ->where('upload_lap_akhir.id', $id)
                ->first();
				// dd();
				$file = $data->file;
				$path = public_path('asset/laporan-akhir/'.$file);
				$headers = array(
						 'Content-Type: application/pdf',
					 );
				// return Response::download(file_get_contents($path), 200, [
				//     'Content-Type' => 'application/pdf',
				//     'Content-Disposition' => 'inline; filename="'.$file.'"'
				// ]);
				return Response::download($path, $file, $headers);
			}

    public function edit($id)
    {
        // $data = UploadLaporan::find('id')->get();
        // dd($data);
         $data = DB::table('upload_lap_akhir')
                ->leftjoin('sub_bidang', 'sub_bidang.id', '=', 'upload_lap_akhir.parameter')
                ->select('upload_lap_akhir.*' ,'sub_bidang.alias as alias','sub_bidang.id as ids')
                ->where('upload_lap_akhir.id', $id)
                ->get();
                // dd($data);
        $parameter = DB::table('sub_bidang')->where('sub_bidang.id','=',Auth::user()->penyelenggara)->get();

        // dd($data);
     return view('evaluasi/imunologi/spl/update', compact('parameter','id','data'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $data = UploadLaporan::find($id);
        $input = $request->all();
        $validasi = Validator::make($input,[
                'file' => 'required|mimes:pdf|max:20000'
            ]);
        if ($validasi->fails()) {
            return redirect('evaluasi/upload-laporan-akhir/edit'.'/'.$id)->withInput()->withErrors($validasi);
        }

        $file= Input::file('file');
        if (!empty($file)) {
            if (Request::hasFile('file'))
                {
                    $dest = public_path('asset/laporan-akhir/');
                    $name = Request::file('file')->getClientOriginalName();
                    Request::file('file')->move($dest, $name);
                }
        }
        $data->tahun = $request['tahun'];
        $data->file = $name;
        $data->siklus = $request['siklus'];
        $data->tipe = $request['tipe'];
        $data->parameter = $request['parameter'];
        $data->bagian = $request['bagian'];
        if(!empty(Auth::user()->badan_usaha)){
            $Upload->badan_usaha = 1;
        }
        $data->save();
        return redirect('/evaluasi/upload-laporan-akhir/');

    }

    public function delete($id)
    {
        $parameter = UploadLaporan::find($id);
        $parameter->delete();
        return back();
    }

    public function insert(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $bagian = $request->get('isi');
        $bagian = $request->get('penutup');
        // dd($bagian);

        $validasi = Validator::make($input,[
                'file' => 'required|mimes:pdf|max:20000'
            ]);
        if ($validasi->fails()) {
            return redirect('evaluasi/upload-laporan-akhir/upload')->withInput()->withErrors($validasi);
        }
        if (Request::hasFile('file'))
        {
            $dest = public_path('asset/laporan-akhir/');
            $name = Request::file('file')->getClientOriginalName();
            Request::file('file')->move($dest, $name);
        }
        $Upload = new UploadLaporan;
        $Upload->file = $name;
        $Upload->tahun = $input['tahun'];
        $Upload->siklus = $input['siklus'];
        $Upload->tipe = $input['tipe'];
        $Upload->periode = $input['periode'];
        $Upload->parameter = $input['parameter'];
        if (isset($input['bagian'])) {
            $Upload->bagian = $input['bagian'];
        }else{
            $Upload->bagian = 'Isi';
        }
        if(!empty(Auth::user()->badan_usaha)){
            $Upload->badan_usaha = 1;
        }
        $Upload->save();

        if ($bagian == 'penutup') {
            return Redirect('evaluasi/upload-laporan-akhir/penutup');
        }elseif ($bagian == "isi") {
            return Redirect('evaluasi/upload-laporan-akhir/isi');
        }else{
            return Redirect('evaluasi/upload-laporan-akhir');
        }

        return Redirect('evaluasi/upload-laporan-akhir');
    }


    public function indexidentifikasi(\Illuminate\Http\Request $request){
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = UploadLaporanIdentifikasi::select( DB::raw('upload_lap_akhir_identifikasi.*, perusahaan.nama_lab, sub_bidang.alias, @rownum := @rownum +1 as rownum'))
                    ->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir_identifikasi.parameter')
                    ->join('tb_registrasi', 'tb_registrasi.id', '=', 'upload_lap_akhir_identifikasi.id_registrasi')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->where('sub_bidang.id','=', Auth::user()->penyelenggara)
                    ->orderBy('id','DESC')
                    ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                return "".'<a href="upload-evaluasi-identifikasi/edit'.'/'.$data->id.'" style="float: left; padding:10%;">
                                    <button class="btn btn-primary btn-xs">
                                        Edit
                                    </button>
                                </a>
                                <a href="upload-evaluasi-identifikasi/delete'.'/'.$data->id.'">
                                    <button class="btn btn-danger btn-xs">
                                        Hapus
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi/imunologi/spl/identifikasi/index');
    }

    public function uploadidentifikasi()
    {
        $parameter = DB::table('sub_bidang')->where('sub_bidang.id','=',Auth::user()->penyelenggara)->get();
        $peserta = DB::table('tb_registrasi')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '13')
                    ->select('tb_registrasi.*', 'perusahaan.nama_lab')
                    ->get();
        return view('evaluasi/imunologi/spl/identifikasi/upload', compact('parameter','peserta','status'));
    }

    public function editidentifikasi($id)
    {
        $peserta = DB::table('tb_registrasi')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->join('upload_lap_akhir_identifikasi', 'upload_lap_akhir_identifikasi.id_registrasi', '=', 'tb_registrasi.id')
                    ->where('tb_registrasi.bidang', '=', '13')
                    ->where('upload_lap_akhir_identifikasi.id', $id)
                    ->select('tb_registrasi.*', 'perusahaan.nama_lab', 'upload_lap_akhir_identifikasi.id as IdLap')
                    ->get();
        // dd($peserta);
        $data = DB::table('upload_lap_akhir_identifikasi')
                ->leftjoin('sub_bidang', 'sub_bidang.id', '=', 'upload_lap_akhir_identifikasi.parameter')
                ->select('upload_lap_akhir_identifikasi.*' ,'sub_bidang.alias as alias','sub_bidang.id as ids')
                ->where('upload_lap_akhir_identifikasi.id', $id)
                ->get();

                // dd($data);
        $parameter = DB::table('sub_bidang')->where('sub_bidang.id','=',Auth::user()->penyelenggara)->get();

        // dd($data);
     return view('evaluasi/imunologi/spl/identifikasi/update', compact('parameter','id','data','peserta'));
    }

    public function updateidentifikasi(\Illuminate\Http\Request $request, $id)
    {
        $data = UploadLaporanIdentifikasi::find($id);
        // dd($data);
        $input = $request->all();
        $validasi = Validator::make($input,[
                'file' => 'required|mimes:pdf|max:20000'
            ]);
        if ($validasi->fails()) {
            return redirect('evaluasi/upload-evaluasi-identifikasi/edit'.'/'.$id)->withInput()->withErrors($validasi);
        }

        $file= Input::file('file');
        if (!empty($file)) {
            if (Request::hasFile('file'))
                {
                    $dest = public_path('asset/laporan-akhir/');
                    $name = Request::file('file')->getClientOriginalName();
                    Request::file('file')->move($dest, $name);
                }
        }
        $data->tahun = $request['tahun'];
        $data->file = $name;
        $data->siklus = $request['siklus'];
        $data->parameter = $request['parameter'];
        $data->id_registrasi = $request['id_registrasi'];
        $data->update();
        return redirect('/evaluasi/upload-evaluasi-identifikasi/');

    }

    public function deleteidentifikasi($id)
    {
        $parameter = UploadLaporanIdentifikasi::find($id);
        $parameter->delete();
        return back();
    }

    public function insertidentifikasi(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $validasi = Validator::make($input,[
                'file' => 'required|mimes:pdf|max:20000'
            ]);
        if ($validasi->fails()) {
            return redirect('evaluasi/upload-evaluasi-identifikasi/upload')->withInput()->withErrors($validasi);
        }
        if (Request::hasFile('file'))
        {
            $dest = public_path('asset/laporan-akhir/');
            $name = Request::file('file')->getClientOriginalName();
            Request::file('file')->move($dest, $name);
        }
        $parameter = $input['id_registrasi'];
        $status = DB::table('upload_lap_akhir_identifikasi')->where('id_registrasi', '=',$parameter)->first();
        if (count($status)>0) {
            Session::flash('message', 'Gagal Disimpan, Data Evaluasi Sudah Dikirim');
            Session::flash('alert-danger', 'alert-danger');
            return redirect::back();
        }else{
            $Upload = new UploadLaporanIdentifikasi;
            $Upload->file = $name;
            $Upload->tahun = $input['tahun'];
            $Upload->siklus = $input['siklus'];
            $Upload->parameter = $input['parameter'];
            $Upload->id_registrasi = $input['id_registrasi'];
            $Upload->save();
            return Redirect('evaluasi/upload-evaluasi-identifikasi');
        }

    }
}

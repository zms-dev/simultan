<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;
use App\Hide;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Sertifikat;
use Illuminate\Support\Facades\Redirect;

class DataSertifikatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = DB::table('tb_sertifikat')
                    ->join('perusahaan','perusahaan.id','=','tb_sertifikat.perusahaan_id')
                    ->join('users','users.id','=','perusahaan.created_by')
                    ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                    ->join('sub_bidang','sub_bidang.id_bidang','=','tb_sertifikat.bidang')
                    ->join('tb_bidang','tb_bidang.id','=','sub_bidang.id_bidang')
                    ->groupBy('tb_sertifikat.bidang')
                    ->groupBy('tb_sertifikat.siklus')
                    ->groupBy('tb_sertifikat.tahun')
                    ->select('tb_bidang.id','tb_bidang.bidang','tb_sertifikat.siklus','tb_sertifikat.tahun')
                    ->orderBy('tb_sertifikat.tahun','desc')
                    ->orderBy('tb_sertifikat.siklus','desc')
                    ->orderBy('tb_sertifikat.bidang','asc')
                    ->get();
        $bidang = DB::table('tb_bidang')->where('id', '<>','8')->get();
        // dd($data);
        return view('back/sertifikat/index', compact('data','bidang'));
    }

    public function insert(\Illuminate\Http\Request $request)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $bidang = $request->get('bidang');
        $cek = DB::table('tb_sertifikat')
                ->where('siklus', $siklus)
                ->where('tahun', $tahun)
                ->where('bidang', $bidang)
                ->get();
        // dd($cek);
        if (count($cek)) {
            $data = DB::table('tb_sertifikat')
                    ->join('perusahaan','perusahaan.id','=','tb_sertifikat.perusahaan_id')
                    ->join('users','users.id','=','perusahaan.created_by')
                    ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                    ->join('sub_bidang','sub_bidang.id_bidang','=','tb_sertifikat.bidang')
                    ->join('tb_bidang','tb_bidang.id','=','sub_bidang.id_bidang')
                    ->where('tb_sertifikat.bidang', $bidang)
                    ->where('tb_registrasi.status','>=','2')
                    ->where('tb_sertifikat.siklus', $siklus)
                    ->where('tb_sertifikat.tahun', $tahun)
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                    ->groupBy('tb_bidang.id')
                    ->groupBy('perusahaan.id')
                    ->orderBy('tb_sertifikat.nomor','asc')
                    ->select('tb_sertifikat.id','perusahaan.nama_lab','tb_registrasi.bidang','tb_sertifikat.nomor')
                    ->get();
            // dd($data);
            $bidang = DB::table('tb_bidang')->where('id', $bidang)->first();

            return view('back/sertifikat/update', compact('data','bidang','siklus','tahun'));

            // Session::flash('message', 'Data Sertifikat Sudah ada!'); 
            // Session::flash('alert-class', 'alert-danger'); 
            // return back();
        }else{
            if ($bidang == '5') {
                $data = DB::table('perusahaan')
                        ->join('users','users.id','=','perusahaan.created_by')
                        ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                        ->join('tb_bidang','tb_bidang.id','=','sub_bidang.id_bidang')
                        ->where('sub_bidang.id_bidang', $bidang)
                        ->where('tb_registrasi.status','>=','2')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                        ->orderBy('sub_bidang.id','asc')
                        // ->orderBy('perusahaan.id','asc')
                        ->orderBy('users.id_member','asc')
                        ->select('perusahaan.id','perusahaan.nama_lab','tb_registrasi.bidang','tb_registrasi.kode_lebpes')
                        ->get();
            }else{
                $data = DB::table('perusahaan')
                        ->join('users','users.id','=','perusahaan.created_by')
                        ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                        ->join('tb_bidang','tb_bidang.id','=','sub_bidang.id_bidang')
                        ->where('sub_bidang.id_bidang', $bidang)
                        ->where('tb_registrasi.status','>=','2')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                        ->groupBy('tb_bidang.id')
                        ->groupBy('perusahaan.id')
                        ->orderBy('users.id_member','asc')
                        ->select('perusahaan.id','perusahaan.nama_lab','tb_registrasi.bidang','tb_registrasi.kode_lebpes')
                        ->get();
            }
            $bidang = DB::table('tb_bidang')->where('id', $bidang)->first();
            // dd($data);
            return view('back/sertifikat/insert', compact('data','bidang','siklus','tahun'));
        }
    }

    public function edit($id)
    {
        $data = DB::table('tb_banner')->where('id', $id)->get();
        return view('back/banner/update', compact('data'));
    }

    public function update(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        // dd($input);
        $i = 0;
        foreach ($input['nomor'] as $nomor) {
            if($nomor != ''){
                $SaveDetail['nomor'] = $input['nomor'][$i];
                Sertifikat::where('id', $input['id'][$i])->update($SaveDetail);
            }
            $i++;
        }
        Session::flash('message', 'Data Telah Dirubah!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/nomor-sertifikat');
    }    

    public function add(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $bidang = $request->get('bidang');
        // dd($bidang);
        $i = 0;
        foreach ($input['perusahaan_id'] as $val) {
            $Save = new Sertifikat;
            $Save->perusahaan_id = $input['perusahaan_id'][$i];
            $Save->bidang = $bidang;
            $Save->sub_bidang = $input['sub_bidang'][$i];
            $Save->nomor = $input['nomor'][$i];
            $Save->siklus = $input['siklus'];
            $Save->tahun = $input['tahun'];
            $Save->save();
            $i++;
        }
  
        return redirect('admin/nomor-sertifikat');
    }

    public function delete($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Banner::find($id)->delete();
        return redirect("admin/banner");
    }

}

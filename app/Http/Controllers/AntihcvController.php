<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Carbon\Carbon;
use PDF;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\TanggalKirimHasil;
use App\masterimunologi as Masterimunologi;
use App\bahanimunologi as Bahanimunologi;
use App\reagenimunologi as Reagenimunologi;
use App\hpimunologi as Hpimunologi;
use App\register as Register;
use App\strategi as Strategi;
use App\kesimpulan as Kesimpulan;
use App\CatatanImun;
use App\LogInput;
use Redirect;
use Validator;
use Session;
use App\TanggalEvaluasi;
class AntihcvController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['cetakevaluasi','view']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request, $id)
    {   
        $siklus = $request->get('y');
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;
        
        $pendidikan = DB::table('tb_pendidikan')->get();
        $data = DB::table('parameter')->where('kategori', 'urinalisa')->get();
        $reagen = DB::table('tb_reagen_imunologi')
                    ->where('kelompok', 'Anti HCV')
                    ->where('produsen','!=', NULL)
                    ->get();
        $register = Register::find($id);
        $perusahaan = $register->kode_lebpes;
            
        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->get();

        $rujukan = DB::table('tb_rujukan_imunologi')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('parameter','=','Anti HCV')
                    ->orderBy('id', 'asc')
                    ->get();

        if (count($validasi)) {
            if (count($rujukan)) {
                return view('hasil_pemeriksaan/anti-hcv', compact('data', 'perusahaan','reagen', 'siklus','date','pendidikan','rujukan'));
            }else{
                Session::flash('message', 'Menu penginputan belum siap.');
                Session::flash('alert-class', 'alert-danger');
                return redirect('hasil-pemeriksaan');
            }
        }else{
            Session::flash('message', 'Harap Input Tanda Terima Bahan Anti HCV Terlebih Dahulu.');
            Session::flash('alert-class', 'alert-danger');
            return redirect('tanda-terima');
        }
        // if ($siklus == 2) {
        //     if ($q1->siklus == 12) {
        //         if ($q1->pemeriksaan == 'done') { 
        //             if (count($validasi)) {
        //                 return view('hasil_pemeriksaan/anti-hcv', compact('data', 'perusahaan','reagen', 'siklus','date','pendidikan'));
        //             }else{
        //                 Session::flash('message', 'Harap Input Tanda Terima Bahan Anti HCV Terlebih Dahulu.');
        //                 Session::flash('alert-class', 'alert-danger');
        //                 return redirect('tanda-terima');
        //             }
        //         }else{
        //             Session::flash('message', 'Harap Input Form Siklus 1.');
        //             Session::flash('alert-class', 'alert-danger');
        //             return back();
        //         }
        //     }else{ 
        //         if (count($validasi)) {
        //             return view('hasil_pemeriksaan/anti-hcv', compact('data', 'perusahaan','reagen', 'siklus','date','pendidikan'));
        //         }else{
        //             Session::flash('message', 'Harap Input Tanda Terima Bahan Anti HCV Terlebih Dahulu.');
        //             Session::flash('alert-class', 'alert-danger');
        //             return redirect('tanda-terima');
        //         }
        //     }
        // }else{ 
        //     if (count($validasi)) {
        //         return view('hasil_pemeriksaan/anti-hcv', compact('data', 'perusahaan','reagen', 'siklus','date','pendidikan'));
        //     }else{
        //         Session::flash('message', 'Harap Input Tanda Terima Bahan Anti HCV Terlebih Dahulu.');
        //         Session::flash('alert-class', 'alert-danger');
        //         return redirect('tanda-terima');
        //     }
        // }
    }

    public function view(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;
        $data = DB::table('master_imunologi')
                ->join('bahan_imunologi', 'master_imunologi.id', '=', 'bahan_imunologi.id_master_imunologi')
                ->leftjoin('tb_pendidikan','tb_pendidikan.id','=','master_imunologi.pendidikan_petugas')
                ->where('master_imunologi.jenis_form', 'ANTI-HCV')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->select('master_imunologi.*', 'bahan_imunologi.id as idimunologi',  'bahan_imunologi.lain', 'bahan_imunologi.id_master_imunologi', 'bahan_imunologi.no_tabung', 'bahan_imunologi.jenis', 'bahan_imunologi.tgl_diperiksa', 'bahan_imunologi.tgl_diterima','tb_pendidikan.tingkat')
                ->get();
        $data1 = DB::table('master_imunologi')
                ->join('reagen_imunologi', 'master_imunologi.id', '=', 'reagen_imunologi.id_master_imunologi')
                ->join('tb_reagen_imunologi', 'reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                ->where('master_imunologi.jenis_form', 'ANTI-HCV')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->get();
        $data2 = DB::table('master_imunologi')
                ->join('hp_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                ->where('master_imunologi.jenis_form', 'ANTI-HCV')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->get();
        // dd($data);
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;

        // return view('cetak_hasil/anti-hcv', compact('data', 'perusahaan', 'data1', 'data2'));
        $pdf = PDF::loadview('cetak_hasil/anti-hcv', compact('data', 'perusahaan', 'data1', 'data2', 'date', 'siklus'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        $kode_pes = sprintf("%04s", Auth::user()->id_member);
        if ($siklus == 1) {
            $sikprint = 'I';
        }else{
            $sikprint = 'II';
        }
        return $pdf->stream($kode_pes.' HCV S'.$sikprint.' '.$date.'.pdf');
    }

    public function insert(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $input = $request->all();
        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;
        // dd($input);
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $years = $q2->year;

        $validasi = Masterimunologi::where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $years)->where('id_registrasi','=',$id)->where('siklus','=',$siklus)->get();// dd($input);
         if (count($validasi)>0) {
            Session::flash('message', 'Hasil Anti HCV Sudah Ada!'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('hasil-pemeriksaan');
        }else{
            $SaveMaster = new Masterimunologi;
            $SaveMaster->kode_lab = $input['kode_peserta'];
            $SaveMaster->kode_peserta = $perusahaanID;
            $SaveMaster->petugas_pemeriksaan = $input['petugas_pemeriksaan'];
            $SaveMaster->pendidikan_petugas = $input['pendidikan'];
            $SaveMaster->pendidikan_lain = $input['pendidikan_lain'];
            $SaveMaster->keterangan = $input['keterangan'];
            $SaveMaster->hasil_pemeriksaan = '';
            $SaveMaster->jenis_form = 'ANTI-HCV';
            $SaveMaster->siklus = $siklus;
            $SaveMaster->id_registrasi = $id;
            $SaveMaster->save();

            $SaveMasterId = $SaveMaster->id;
            $i = 0;
            foreach ($input['no_tabung'] as $tabung) {
                if($tabung != ''){
                    $SaveBahan = new Bahanimunologi;
                    $SaveBahan->tgl_diterima = $input['tgl_diterima'];
                    $SaveBahan->tgl_diperiksa = $input['tgl_diperiksa'];
                    $SaveBahan->no_tabung = $input['no_tabung'][$i];
                    $SaveBahan->jenis = $input['jenis'][$i];
                    $SaveBahan->id_master_imunologi = $SaveMasterId;
                    $SaveBahan->save();
                }
                $i++;
            }
            $i = 0;
            foreach ($input['nama_reagen'] as $reagen) {
                if($reagen != ''){
                    $SaveBahan = new Reagenimunologi;
                    $SaveBahan->metode = $input['metode'][$i];
                    $SaveBahan->nama_reagen = $input['nama_reagen'][$i];
                    $SaveBahan->reagen_lain = $input['reagen_lain'][$i];
                    $SaveBahan->nama_produsen = $input['nama_produsen'][$i];
                    $SaveBahan->nomor_lot = $input['nomor_lot'][$i];
                    $SaveBahan->tgl_kadaluarsa = $input['tgl_kadaluarsa'][$i];
                    $SaveBahan->id_master_imunologi = $SaveMasterId;
                    $SaveBahan->save();
                }
                $i++;
            }
            $i = 0;
            foreach ($input['kode_bahan_kontrol'] as $kode) {
                if($input['kode_bahan_kontrol'][$i] ){
                    $SaveHp = new Hpimunologi;
                    $SaveHp->tabung = "1";
                    $SaveHp->kode_bahan_kontrol = $input['kode_bahan_kontrol'][$i];
                    $SaveHp->abs_od = $input['abs_od1'][$i];
                    $SaveHp->cut_off = $input['cut_off1'][$i];
                    $SaveHp->sco = $input['sco1'][$i];
                    $SaveHp->interpretasi = $input['interpretasi1'][$i];
                    $SaveHp->id_master_imunologi = $SaveMasterId;
                    $SaveHp->save();
                }
                $i++;
            }
     
            if ($siklus == '1') {
                Register::where('id',$id)->update(['siklus_1'=>'done', 'rpr1'=>'done', 'status_data1'=>'1']);
                Register::where('id',$id)->update(['pemeriksaan'=>'done']);
            }else{
                Register::where('id',$id)->update(['siklus_2'=>'done', 'rpr2'=>'done', 'status_data2'=>'1']);
                Register::where('id',$id)->update(['pemeriksaan2'=>'done']);
            }

            $log = new LogInput;
            $log->id_registrasi = $id;
            $log->status = 'Input Hasil';
            $log->created_by = Auth::user()->id;
            $log->siklus = $siklus;
            $log->type = '';
            $log->save();

            return redirect('hasil-pemeriksaan');
        }
    }

    public function edit(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;
        
        $pendidikan = DB::table('tb_pendidikan')->get();
        $data = DB::table('master_imunologi')
                ->join('bahan_imunologi', 'master_imunologi.id', '=', 'bahan_imunologi.id_master_imunologi')
                ->leftjoin('tb_pendidikan','tb_pendidikan.id','=','master_imunologi.pendidikan_petugas')
                ->where('master_imunologi.jenis_form', 'ANTI-HCV')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->select('master_imunologi.*', 'bahan_imunologi.id as idimunologi',  'bahan_imunologi.lain', 'bahan_imunologi.id_master_imunologi', 'bahan_imunologi.no_tabung', 'bahan_imunologi.jenis', 'bahan_imunologi.tgl_diperiksa', 'bahan_imunologi.tgl_diterima','tb_pendidikan.tingkat')
                ->get();
        $data1 = DB::table('master_imunologi')
                ->join('reagen_imunologi', 'master_imunologi.id', '=', 'reagen_imunologi.id_master_imunologi')
                ->join('tb_reagen_imunologi', 'reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                ->where('master_imunologi.jenis_form', 'ANTI-HCV')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->select('reagen_imunologi.*', 'reagen_imunologi.reagen_lain', 'tb_reagen_imunologi.id as Idreagen', 'tb_reagen_imunologi.reagen as namareagen')
                ->get();
        $data2 = DB::table('master_imunologi')
                ->join('hp_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                ->where('master_imunologi.jenis_form', 'ANTI-HCV')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->get();
        $reagen = DB::table('tb_reagen_imunologi')
                    ->where('kelompok', 'Anti HCV')
                    ->where('produsen','!=', NULL)
                    ->get();
        // dd($data1);
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $kodeperusahaan = $register->kode_lebpes;

        $rujukan = DB::table('tb_rujukan_imunologi')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('parameter','=','Anti HCV')
                    ->orderBy('id', 'asc')
                    ->get();

        return view('edit_hasil/anti-hcv', compact('data', 'perusahaan', 'data1', 'data2', 'siklus','kodeperusahaan','reagen','date','pendidikan','rujukan'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $input = $request->all();
        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;
        $MasterId = $request->idmaster;
        // dd($input);
        $SaveMaster['kode_peserta'] = $perusahaanID;
        $SaveMaster['petugas_pemeriksaan'] = $request->petugas_pemeriksaan;
        $SaveMaster['pendidikan_petugas'] = $request->pendidikan;
        $SaveMaster['pendidikan_lain'] = $request->pendidikan_lain;
        $SaveMaster['kode_lab'] = $request->kode_peserta;
        $SaveMaster['siklus'] = $siklus;
        $SaveMaster['hasil_pemeriksaan'] = '';
        $SaveMaster['jenis_form'] = 'ANTI-HCV';
        $SaveMaster['id_registrasi'] = $id;
        Masterimunologi::where('id_registrasi',$id)->where('id', $MasterId)->update($SaveMaster);
        
        $SaveMasterId = $MasterId;
        $i = 0;
        foreach ($input['no_tabung'] as $tabung) {
            if($tabung != ''){
                $SaveBahan['tgl_diterima'] = $request->tgl_diterima;
                $SaveBahan['tgl_diperiksa'] = $request->tgl_diperiksa;
                $SaveBahan['no_tabung'] = $request->no_tabung[$i];
                $SaveBahan['jenis'] = $request->jenis[$i];
                $SaveBahan['id_master_imunologi'] = $SaveMasterId;
                Bahanimunologi::where('id', $request->idbahan[$i])->update($SaveBahan);
            }
            $i++;
        }
        $i = 0;
        foreach ($input['nama_reagen'] as $reagen) {
            if($reagen != ''){
                $SaveReagen['metode'] = $request->metode[$i];
                $SaveReagen['nama_reagen'] = $request->nama_reagen[$i];
                $SaveReagen['reagen_lain'] = $request->reagen_lain[$i];
                $SaveReagen['nama_produsen'] = $request->nama_produsen[$i];
                $SaveReagen['nomor_lot'] = $request->nomor_lot[$i];
                $SaveReagen['tgl_kadaluarsa'] = $request->tgl_kadaluarsa[$i];
                $SaveReagen['id_master_imunologi'] = $SaveMasterId;
                Reagenimunologi::where('id', $request->idreagen[$i])->update($SaveReagen);
            }
            $i++;
        }
        $i = 0;
        foreach ($input['kode_bahan_kontrol'] as $kode) {
            if($input['kode_bahan_kontrol'][$i] ){
                $SaveHp['tabung'] = "1";
                $SaveHp['kode_bahan_kontrol'] = $request->kode_bahan_kontrol[$i];
                $SaveHp['abs_od'] = $request->abs_od1[$i];
                $SaveHp['cut_off'] = $request->cut_off1[$i];
                $SaveHp['sco'] = $request->sco1[$i];
                $SaveHp['interpretasi'] = $request->interpretasi1[$i];
                $SaveHp['id_master_imunologi'] = $SaveMasterId;
                Hpimunologi::where('id', $request->idhp[$i])->update($SaveHp);
            }
            $i++;
        }
        if ($request->simpan == "Kirim") {
            TanggalKirimHasil::where('id_registrasi', $id)
                            ->where('siklus', $siklus)
                            ->delete();

            $KirimHasil = new TanggalKirimHasil;
            $KirimHasil->id_registrasi = $id;
            $KirimHasil->siklus = $siklus;
            $KirimHasil->type = "";
            $KirimHasil->tanggal_kirim = date('Y-m-d');
            $KirimHasil->save();

            $log = new LogInput;
            $log->id_registrasi = $id;
            $log->status = 'Kirim Hasil';
            $log->created_by = Auth::user()->id;
            $log->siklus = $siklus;
            $log->type = '';
            $log->save();

            if ($siklus == '1') {
                Register::where('id',$id)->update(['status_data1'=>'2']);
            }else{
                Register::where('id',$id)->update(['status_data2'=>'2']);
            }
        }else{
            $log = new LogInput;
            $log->id_registrasi = $id;
            $log->status = 'Edit Hasil';
            $log->created_by = Auth::user()->id;
            $log->siklus = $siklus;
            $log->type = '';
            $log->save();
        }
        return redirect('edit-hasil');
    }

    public function evaluasi(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $data = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data->created_at );  
        $tahun = $tahun->year;
        $data2 = DB::table('master_imunologi')
                ->join('hp_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                ->join('tb_rujukan_imunologi', 'hp_imunologi.kode_bahan_kontrol', '=', 'tb_rujukan_imunologi.kode_bahan_uji')
                ->where('master_imunologi.jenis_form', 'ANTI-HCV')
                ->where('tb_rujukan_imunologi.parameter', 'Anti HCV')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $type)
                ->where('tb_rujukan_imunologi.siklus', $type)
                ->where('tb_rujukan_imunologi.tahun', $tahun)
                ->get();
        if (count($data2)) {
            $reagen = DB::table('reagen_imunologi')
                        ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','tb_reagen_imunologi.id')
                        ->where('id_master_imunologi', $data2[0]->id_master_imunologi)
                        ->select('tb_reagen_imunologi.reagen','reagen_imunologi.metode','reagen_imunologi.reagen_lain')
                        ->get();
            // dd($data2);
            $strategi = Strategi::where('id_master_imunologi', $data2[0]->id_master_imunologi)->where('siklus', $type)->get();   
        }
        $rujukan = DB::table('tb_rujukan_imunologi')
                ->where('siklus', $type)
                ->where('tahun', $tahun)
                ->where('parameter', 'Anti HCV')
                ->orderBy('kode_bahan_uji', 'asc')
                ->get();
        $kesimpulan = Kesimpulan::where('id_registrasi', $id)->where('siklus', $type)->where('type','=','ANTI-HCV')->first();
        $catatan = CatatanImun::where('id_registrasi', $id)->where('siklus', $type)->where('tahun', $tahun)->first();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $kodeperusahaan = $register->kode_lebpes;

        return view('evaluasi.imunologi.hcv.evaluasi', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','kesimpulan', 'register','strategi', 'reagen', 'tahun', 'catatan', 'rujukan'));
    }

    public function insertevaluasi(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('y');
        $input = $request->all();
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        // dd($input);
        $i = 0;
        $a = 0;

        // foreach ($input['strategi'] as $strategi) {
        // $a++;
        //     if($strategi != ''){
        //         $SaveStrategi = new Strategi;
        //         $SaveStrategi->id_master_imunologi = $input['id_master_imunologi'];
        //         $SaveStrategi->nilai = $input['strategi'][$i];
        //         if ($input['strategi'][$i] == '0') {
        //             $SaveStrategi->kategori = 'Tidak Sesuai';
        //         }elseif ($input['strategi'][$i] == '5') {
        //             $SaveStrategi->kategori = 'Sesuai';
        //         }else{
        //             $SaveStrategi->kategori = '';
        //         }
        //         $SaveStrategi->kode_bahan =  $input['kode_bahan'][$i];
        //         $SaveStrategi->tabung = $a;
        //         $SaveStrategi->siklus = $type;
        //         $SaveStrategi->save();
        //     }
        //     $i++;
        // }

        $SaveCatatan = new CatatanImun;
        $SaveCatatan->catatan = $input['catatan'];
        // $SaveCatatan->tanggal_ttd = $input['ttd'];
        $SaveCatatan->id_registrasi = $id;
        $SaveCatatan->siklus = $type;
        $SaveCatatan->tahun = $tahun;
        $SaveCatatan->save();

        $SaveKesimpulan = new Kesimpulan;
        $SaveKesimpulan->ketepatan = $input['ketepatan'];
        $SaveKesimpulan->id_master_imunologi = $input['id_master_imunologi'];
        $SaveKesimpulan->type = 'ANTI-HCV';
        $SaveKesimpulan->id_registrasi = $id;
        $SaveKesimpulan->siklus = $type;
        $SaveKesimpulan->save();

        return redirect('/hasil-pemeriksaan/anti-hcv/evaluasi/'.$id.'?y='.$type);
    }

    public function cetakevaluasi(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $data = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data->created_at );  
        $tahun = $tahun->year;
        $data2 = DB::table('master_imunologi')
                ->join('hp_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                ->join('tb_registrasi', 'master_imunologi.id_registrasi', '=', 'tb_registrasi.id')
                ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->join('tb_rujukan_imunologi', 'hp_imunologi.kode_bahan_kontrol', '=', 'tb_rujukan_imunologi.kode_bahan_uji')
                ->where('master_imunologi.jenis_form', 'ANTI-HCV')
                ->where('tb_rujukan_imunologi.parameter', 'Anti HCV')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $type)
                ->where('tb_rujukan_imunologi.siklus', $type)
                ->where('tb_rujukan_imunologi.tahun', $tahun)
                ->get();
        // dd($data2);
        if (count($data2)) {
            $reagen = DB::table('reagen_imunologi')
                        ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','tb_reagen_imunologi.id')
                        ->where('id_master_imunologi', $data2[0]->id_master_imunologi)
                        ->select('tb_reagen_imunologi.reagen','reagen_imunologi.metode','reagen_imunologi.reagen_lain')
                        ->get();
            // dd($reagen);
        }
        $rujukan = DB::table('tb_rujukan_imunologi')
                ->where('siklus', $type)
                ->where('tahun', $tahun)
                ->where('parameter', 'Anti TP')
                ->orderBy('kode_bahan_uji', 'asc')
                ->get();
        $kesimpulan = Kesimpulan::where('id_registrasi', $id)->where('siklus', $type)->where('type','=','ANTI-HCV')->first();
        $catatan = CatatanImun::where('id_registrasi', $id)->where('siklus', $type)->where('tahun', $tahun)->first();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $kodeperusahaan = $register->kode_lebpes;
        $tanggalevaluasi = TanggalEvaluasi::where('kategori','=','Imunologi')->first(); 
        $ttd = DB::table('tb_ttd_evaluasi')
            ->where('tahun', $tahun)
            ->where('siklus', $type)
            ->where('bidang', '=', '9')
            ->first();
        // return view('evaluasi/anti-hiv', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi'));
        if (count($kesimpulan) > 0) {
        $pdf = PDF::loadview('evaluasi.imunologi.hcv.print', compact('data', 'perusahaan','tanggalevaluasi', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan', 'rujukan','ttd'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        return $pdf->stream('Evaluasi HBsAg.pdf');
        }else{
             Session::flash('message', 'Anti HCV belum dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
    }

    public function updatevaluasi(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('y');
        $input = $request->all();
        // dd($input);
        $i = 0;
        $a = 0;
        // foreach ($input['strategi'] as $strategi) {
        // $a++;
        //     if($strategi != ''){
        //         $SaveStrategi['nilai'] = $request->strategi[$i];
        //         if ($input['strategi'][$i] == '0') {
        //             $SaveStrategi['kategori'] = 'Tidak Sesuai';
        //         }elseif ($input['strategi'][$i] == '5') {
        //             $SaveStrategi['kategori'] = 'Sesuai';
        //         }else{
        //             $SaveStrategi['kategori'] = '';
        //         }
        //         $SaveStrategi['tabung'] = $a;
        //         $SaveStrategi['kode_bahan'] = $request->kode_bahan_kontrol[$i];
        //         Strategi::where('id', $request->idevaluasi[$i])->update($SaveStrategi);
        //     }
        //     $i++;
        // }
        $SaveCatatan['catatan'] = $request->catatan;
        $SaveCatatan['tanggal_ttd'] = $request->ttd;
        CatatanImun::where('id_registrasi',$id)->update($SaveCatatan);

        $SaveKesimpulan['ketepatan'] = $request->ketepatan;
        Kesimpulan::where('id_registrasi',$id)->update($SaveKesimpulan);

        return redirect('/hasil-pemeriksaan/anti-hcv/evaluasi/'.$id.'?y='.$type);
    }
}
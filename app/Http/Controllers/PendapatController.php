<?php
namespace App\Http\Controllers; 
 
use DB; 
use Request; 
use Auth; 
use Input; 
use PDF; 
use Session; 
 
use Yajra\Datatables\Datatables; 
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\View; 
use App\pendapat as Pendapat; 
use App\Daftar; 
use App\responden as Responden; 
use App\saran as Saran; 
use Excel;
use Redirect; 
use Validator; 
 
class PendapatController extends Controller 
{ 
    /** 
     * Create a new controller instance. 
     * 
     * @return void 
     */ 
    public function __construct() 
    { 
        $this->middleware('auth'); 
    } 
 
    /** 
     * Show the application dashboard. 
     * 
     * @return \Illuminate\Http\Response 
     */ 
 
    public function index() 
    {    
        $user = Auth::user()->id;
        $siklus = DB::table('tb_siklus')->first();
        $data = DB::table('tb_registrasi')
                    ->where('created_by', '=', $user)
                    ->select(DB::raw('YEAR(created_at) as tahun'))
                    ->groupBy(DB::raw('YEAR(created_at)'))
                    ->get();
        // dd($data);
        // $tahun = $siklus->tahun; 
        
        // $pendapat = DB::table('tb_pendapat_responden')->where('tahun', $tahun)->where('id_users', $user)->get(); 
        $perusahaan = DB::table('perusahaan')->where('created_by', $user)->first(); 
        return view('pendapat/pendapat', compact('perusahaan', 'tahun','data')); 
    } 

    public function survey() 
    {    
        return view('pendapat/survey/index'); 
    } 

    public function surveyexcel(\Illuminate\Http\Request $request) 
    {   
        $data = DB::table('tb_pendapat_responden')
                ->join('tb_pendapat', 'tb_pendapat.id_responden', '=', 'tb_pendapat_responden.id')
                ->join('users', 'users.id', '=', 'tb_pendapat_responden.id_users')
                ->where('tb_pendapat_responden.siklus', $request->siklus)
                ->where('tb_pendapat_responden.tahun', $request->tahun)
                ->select('tb_pendapat_responden.id', 'tb_pendapat.*', 'users.name')
                ->get();
        // dd($data);
        // return view('pendapat/survey/excel', compact('data')); 
        Excel::create('Survey Pelanggan Siklus '.$request->siklus.' Tahun '.$request->tahun, function($excel) use ($data) {
            $excel->sheet('data', function($sheet) use ($data) {
                $sheet->loadView('pendapat/survey/excel', array('data'=>$data) );
            });

        })->download('xls');
    } 
 
    public function saran() 
    {    
        $tahun = date('Y'); 
        $user = Auth::user()->id; 
        $pendapat = DB::table('tb_saran')->where('id_users', $user)->where('tahun', $tahun)->get(); 
        $perusahaan = DB::table('perusahaan')->where('created_by', $user)->first(); 
        // dd($perusahaan); 
        return view('pendapat/saran', compact('perusahaan', 'tahun','pendapat')); 
    } 
 
    public function insertsaran(\Illuminate\Http\Request $request) 
    { 
        $input = $request->all(); 
        $tahun = date('Y'); 
        $user = Auth::user()->id; 
        $perusahaan = DB::table('perusahaan')->where('created_by', $user)->first(); 
        // dd($input); 
        $SaveSaran = new Saran; 
        $SaveSaran->id_users = $user; 
        $SaveSaran->parameter = $input['parameter']; 
        $SaveSaran->saran = $input['saran'];     
        $SaveSaran->tahun = $tahun; 
        $SaveSaran->siklus = $input['siklus'];     
        $SaveSaran->save(); 
 
        Session::flash('message', 'Saran berhasil terkirim!');  
        return view('pendapat.responden.index'); 
    } 
 
    public function laporansaran() 
    {    
        return view('pendapat/saran/index'); 
    } 

    public function laporansaranexcel(\Illuminate\Http\Request $request) 
    {   
        $data = DB::table('tb_saran')
                ->join('users', 'users.id', '=', 'tb_saran.id_users')
                ->where('tb_saran.siklus', $request->siklus)
                ->where('tb_saran.tahun', $request->tahun)
                ->select('tb_saran.*', 'users.name')
                ->get();
        // dd($data);
        Excel::create('Saran dan Komentar Siklus '.$request->siklus.' Tahun '.$request->tahun, function($excel) use ($data) {
            $excel->sheet('data', function($sheet) use ($data) {
                $sheet->loadView('pendapat/saran/excel', array('data'=>$data) );
            });
        })->download('xls');
    } 

    public function dataperusahaan(\Illuminate\Http\Request $request, $id) 
    { 
        $tahun = date('Y'); 
        $pendapat = DB::table('tb_pendapat_responden') 
                    ->join('perusahaan', 'perusahaan.created_by', '=', 'tb_pendapat_responden.id_users') 
                    ->where('id_users', $id) 
                    ->select('tb_pendapat_responden.*','perusahaan.nama_lab') 
                    ->get(); 
        return view('pendapat.responden.data', compact('tahun','pendapat')); 
    } 
 
    public function viewsurvey(\Illuminate\Http\Request $request, $id) 
    {    
        $tahun = date('Y'); 
        $user = Auth::user()->id; 
        $pendapat = DB::table('tb_pendapat_responden') 
                    ->join('perusahaan', 'perusahaan.created_by', '=', 'tb_pendapat_responden.id_users') 
                    ->join('tb_pendapat', 'tb_pendapat.id_responden', '=', 'tb_pendapat_responden.id') 
                    ->where('tb_pendapat_responden.id', $id) 
                    ->select('tb_pendapat_responden.*','perusahaan.nama_lab', 'tb_pendapat.*') 
                    ->first(); 
        // dd($pendapat); 
        // return view('pendapat.responden.print', compact('perusahaan', 'tahun','pendapat')); 
 
        $pdf = PDF::loadview('pendapat.responden.print', compact('perusahaan', 'tahun','pendapat')) 
        ->setPaper('a4', 'potrait') 
        ->setOptions(['dpi' => 135, 'defaultFont' => 'sans-serif']) 
        ->setwarnings(false); 
        return $pdf->stream('Telur Cacing.pdf'); 
    } 
    public function perusahaan(\Illuminate\Http\Request $request) 
    { 
        $tahun = date('Y'); 
        if($request->ajax()){ 
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'),'perusahaan.*','tb_registrasi.kode_lebpes','tb_registrasi.id as idregistrasi', DB::raw('(SELECT COUNT(C.id) FROM tb_pendapat_responden AS C WHERE C.id_users = perusahaan.created_by AND C.tahun = 2018 ) AS jml_na')) 
                            ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id') 
                            ->where('tb_registrasi.status','=','3') 
                            ->groupBy('perusahaan.id') 
                            ->orderby('jml_na', 'desc')
                            ->get(); 
            // dd($datas); 
            return Datatables::of($datas) 
            ->addColumn('action', function($data) use($tahun){ 
                    
                    $pendapat = db::table('tb_pendapat_responden') 
                                ->where('id_users', $data->created_by) 
                                ->where('tahun', '=' , $tahun) 
                                ->orderby('id') 
                                ->get(); 
                    if(count($pendapat) == '1'){ 
                        return "".'<a href="survey-pelanggan/'.$data->created_by.'" style="float: left;"> 
                                        <button class="btn btn-info btn-xs"> 
                                            View
                                        </button> 
                                    </a>'; 
                    }elseif (count($pendapat) == '2') { 
                        return "".'<a href="survey-pelanggan/'.$data->created_by.'" style="float: left;"> 
                                        <button class="btn btn-success btn-xs"> 
                                            View
                                        </button> 
                                    </a>'; 
                    }else{ 
                        return "".'<a href="survey-pelanggan/'.$data->created_by.'" style="float: left;"> 
                                        <button class="btn btn-primary btn-xs"> 
                                            View
                                        </button> 
                                    </a>'; 
                    } 
            ;}) 
            ->make(true); 
        } 
        return view('pendapat.responden.index'); 
    }

    public function insert(\Illuminate\Http\Request $request) 
    { 
        $siklus = DB::table('tb_siklus')->first();
        $input = $request->all(); 
        $tahun = $input['tahun']; 
        $user = Auth::user()->id; 
        $perusahaan = DB::table('perusahaan')->where('created_by', $user)->first(); 
        $cek = DB::table('tb_pendapat_responden')
                ->where('id_users', $user)
                ->where('siklus', $input['siklus'])
                ->where('tahun', $tahun)
                ->get();
        // dd($input); 
        if (count($cek)) {
            Session::flash('message', 'Pendapat Responden tidak dapat input double!');  
            Session::flash('alert-class', 'alert-danger');  
            return redirect('index'); 
        }else{       
            $SaveMaster = new Responden; 
            $SaveMaster->id_users = $user; 
            $SaveMaster->siklus = $input['siklus']; 
            $SaveMaster->tahun = $tahun; 
            $SaveMaster->save(); 
     
            $SaveMasterId = $SaveMaster->id; 
     
            $SavePendapat = new Pendapat; 
            $SavePendapat->id_responden = $SaveMasterId; 
            $SavePendapat->no_1 = $input['no_1']; 
            $SavePendapat->no_2 = $input['no_2']; 
            $SavePendapat->no_3 = $input['no_3']; 
            $SavePendapat->no_4 = $input['no_4']; 
            $SavePendapat->no_5 = $input['no_5']; 
            $SavePendapat->no_6 = $input['no_6']; 
            $SavePendapat->no_7 = $input['no_7']; 
            $SavePendapat->no_8 = $input['no_8']; 
            $SavePendapat->no_9 = $input['no_9']; 
            $SavePendapat->no_10 = $input['no_10']; 
            $SavePendapat->no_11 = $input['no_11']; 
            $SavePendapat->no_12 = $input['no_12']; 
            $SavePendapat->no_13 = $input['no_13']; 
            $SavePendapat->no_14 = $input['no_14']; 
            $SavePendapat->no_15 = $input['no_15']; 
            $SavePendapat->no_16 = $input['no_16']; 
            $SavePendapat->no_17 = $input['no_17']; 
            $SavePendapat->no_18 = $input['no_18']; 
            $SavePendapat->no_19 = $input['no_19']; 
            $SavePendapat->no_20 = $input['no_20']; 
            $SavePendapat->save(); 
     
            Session::flash('message', 'Pendapat Responden berhasil terkirim!');  
            Session::flash('alert-class', 'alert-success');  
            return redirect('index'); 
        }
    } 
 
    public function viewsaran(\Illuminate\Http\Request $request) 
    { 
        if($request->ajax()){ 
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));  
            $datas = Saran::select( DB::raw('@rownum := @rownum +1 as rownum, tb_saran.*,perusahaan.nama_lab,perusahaan.alamat')) 
                    ->join('users', 'tb_saran.id_users','=','users.id') 
                    ->join('perusahaan','users.id','=','perusahaan.created_by')
                    ->where('tahun', '=' , $tahun)
                    ->where('siklus', '=' , $siklus); 
            return Datatables::of($datas) 
            ->addColumn('action', function($data){ 
                if ($data->jawaban != NULL) {
                    return "".'<a href="jawab/'.$data->id.'" style="float: left;"> 
                                    <button class="btn btn-info btn-xs"> 
                                        Jawab 
                                    </button> 
                                </a>';
                }else{
                    return "".'<a href="jawab/'.$data->id.'" style="float: left;"> 
                                    <button class="btn btn-primary btn-xs"> 
                                        Jawab 
                                    </button> 
                                </a>' ;
                }
                }) 
            ->make(true); 
        } 
        return view('pendapat.index'); 
    } 
 
    public function jawabsaran($id) 
    {    
        $tahun = date('Y'); 
        $pendapat = DB::table('tb_saran')->where('id', $id)->first(); 
        $perusahaan = DB::table('perusahaan')->where('created_by', $pendapat->id_users)->first(); 
        // dd($pendapat); 
        return view('pendapat/jawab', compact('perusahaan', 'tahun','pendapat','id')); 
    }

    public function jawabsaranprint($id) 
    {    
        $tahun = date('Y'); 
        $pendapat = DB::table('tb_saran')->where('id', $id)->first(); 
        $perusahaan = DB::table('perusahaan')->where('created_by', $pendapat->id_users)->first(); 
        // dd($pendapat); 
        $pdf = PDF::loadview('pendapat/printsaran', compact('perusahaan', 'tahun','pendapat','id'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Registrasi.pdf');
        return view('pendapat/printsaran', compact('perusahaan', 'tahun','pendapat','id')); 
    } 
 
    public function injawabsaran(\Illuminate\Http\Request $request, $id) 
    { 
        $input = $request->all(); 
        $tahun = date('Y'); 
        $user = Auth::user()->id; 
        $perusahaan = DB::table('perusahaan')->where('created_by', $user)->first(); 
        // dd($input); 
        $SaveSaran['jawaban'] = $request->jawaban; 
        Saran::where('id',$id)->update($SaveSaran); 
 
        Session::flash('message', 'Jawaban Saran berhasil terkirim!');  
        Session::flash('alert-class', 'alert-success');  
        return redirect('index'); 
    } 
 
    public function jawabansaran(\Illuminate\Http\Request $request) 
    { 
        $user = Auth::user()->id; 
        if($request->ajax()){ 
            DB::statement(DB::raw("set @rownum=0"));  
            $datas = Saran::select( DB::raw('@rownum := @rownum +1 as rownum, tb_saran.*,perusahaan.nama_lab,perusahaan.alamat')) 
                    ->join('users', 'tb_saran.id_users','=','users.id') 
                    ->join('perusahaan','users.id','=','perusahaan.created_by') 
                    ->where('users.id',$user); 
            return Datatables::of($datas) 
            ->addColumn('action', function($data){ 
                    return "".'<a href="keluh-saran/'.$data->id.'" style="float: left;"> 
                                    <button class="btn btn-primary btn-xs"> 
                                        Lihat 
                                    </button> 
                                </a>' 
                        ;}) 
            ->make(true); 
        } 
        return view('pendapat.datauser'); 
    } 
 
    public function datajawab(\Illuminate\Http\Request $request, $id) 
    { 
        $tahun = date('Y'); 
        $pendapat = DB::table('tb_saran')->where('id', $id)->first(); 
        $perusahaan = DB::table('perusahaan')->where('created_by', $pendapat->id_users)->first(); 
        // dd($perusahaan); 
        return view('pendapat/datajawab', compact('perusahaan', 'tahun','pendapat')); 
    }  
}
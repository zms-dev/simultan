<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\bidang as Bidang;
use Illuminate\Support\Facades\Redirect;

class BidangController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $bidang = DB::table('tb_bidang')->get();
        return view('back/bidang/index', compact('bidang'));
    }

    public function edit($id)
    {
        $data = DB::table('tb_bidang')->where('id', $id)->get();
        return view('back/bidang/update', compact('data'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $data['bidang'] = $request->bidang;
        $data['created_by'] = Auth::user()->id;
        Bidang::where('id',$id)->update($data);
        Session::flash('message', 'Master Bidang Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/bidang');
    }    

    public function insert(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        $data['created_by'] = Auth::user()->id;
        Bidang::create($data);
        Session::flash('message', 'Master Bidang Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/bidang');
    }

    public function delete($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Bidang::find($id)->delete();
        return redirect("admin/bidang");
    }
}

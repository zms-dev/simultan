<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;

use mikehaertl\wkhtmlto\Pdf as WKHTMLTOPDF;
use App\HpHeader;
use App\Parameter;
use App\MetodePemeriksaan;
use App\HpDetail;
use App\ZScore;
use App\ZScoreAlat;
use App\ZScoreMetode;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Rujukanurinalisa;
use Illuminate\Support\Facades\View;
use App\kimiaklinik as Kimiaklinik;
use App\daftar as Daftar;
use App\register as Register;
use Redirect;
use Validator;
use Excel;
use Session;
use App\Jobs\Evaluasi;
use App\JobsEvaluasi;

class LaporanInputHasil extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function tknasional()
    {
        return View('laporan/tknasional/index');
    }

    public function tkpdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
            // dd($url);?jknbijb
        $url = url('/laporan-tk-nasional/pdf?bidang='.$input['bidang'].'&tahun='.$input['tahun'].'&siklus='.$input['siklus']);
        // dd($url);
        // return redirect($url);
        $image = new WKHTMLTOPDF($url);
        $image->setOptions([
            'binary' => env('PATH_WKHTMLPDF', '/usr/local/bin/wkhtmltopdf'),
            'orientation' => 'portrait',
            'page-size' => 'letter',
            'dpi' => '100',
        ]);

        if(!$image->send()){
            dd($image->getError());
        }
    }
    public function tknasionalnapdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        // dd($input);
        $bidang = DB::table('sub_bidang')
                    ->join('tb_bidang', 'tb_bidang.id', '=', 'sub_bidang.id_bidang')
                    ->where('sub_bidang.id', $input['bidang'])
                    ->first();
        if ($input['bidang'] <= 3) {
            $data = DB::table('tb_registrasi')
                    ->join('hp_headers', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
                    ->join('hp_details', 'hp_details.hp_header_id', '=', 'hp_headers.id')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->join('provinces', 'provinces.id', '=', 'perusahaan.provinsi')
                    ->join('regencies', 'regencies.id', '=', 'perusahaan.kota')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $input['tahun'])
                    ->where('tb_registrasi.bidang', $input['bidang'])
                    ->where('hp_headers.siklus', $input['siklus'])
                    ->select('tb_registrasi.id', 'perusahaan.nama_lab','perusahaan.pemantapan_mutu', 'provinces.name as provinsi', 'regencies.name as kota')
                    ->groupBy('tb_registrasi.id')
                    // ->take(1)
                    ->get();
            if ($input['bidang'] <= 2) {
                foreach ($data as $key => $val) {
                    $hasil = DB::table('hp_headers')
                            ->join('hp_details', 'hp_details.hp_header_id', '=', 'hp_headers.id')
                            ->join('parameter', 'parameter.id', '=', 'hp_details.parameter_id')
                            ->leftjoin('tb_pendidikan','tb_pendidikan.id','=','hp_headers.pendidikan_petugas')
                            ->where('hp_headers.siklus', $input['siklus'])
                            ->where('hp_headers.id_registrasi', $val->id)
                            ->orderBy('parameter.bagian', 'asc')
                            ->select('hp_headers.pendidikan_petugas','hp_headers.pendidikan_lain', 'hp_details.hasil_pemeriksaan', 'hp_headers.id_registrasi','parameter.id','tb_pendidikan.tingkat')
                            ->groupBy('parameter.id')
                            ->get();
                    $val->hasil = $hasil;
                    foreach ($hasil as $key => $has) {
                        $zscore = DB::table('tb_zscore')
                                ->join('parameter', 'parameter.id', '=', 'tb_zscore.parameter')
                                ->where('tb_zscore.id_registrasi', $has->id_registrasi)
                                ->where('parameter.id', $has->id)
                                ->where('tb_zscore.type', '=','a')
                                ->where('tb_zscore.siklus', $input['siklus'])
                                ->where('tb_zscore.tahun', $input['tahun'])
                                ->orderBy('parameter.bagian', 'asc')
                                ->select('tb_zscore.zscore', 'parameter.nama_parameter')
                                ->first();
                        $has->zscore = $zscore;
                        $zscoreb = DB::table('tb_zscore')
                                ->join('parameter', 'parameter.id', '=', 'tb_zscore.parameter')
                                ->where('tb_zscore.id_registrasi', $has->id_registrasi)
                                ->where('parameter.id', $has->id)
                                ->where('tb_zscore.type', '=','b')
                                ->where('tb_zscore.siklus', $input['siklus'])
                                ->where('tb_zscore.tahun', $input['tahun'])
                                ->orderBy('parameter.bagian', 'asc')
                                ->select('tb_zscore.zscore', 'parameter.nama_parameter')
                                ->first();
                        $has->zscoreb = $zscoreb;
                    }
                }
            }elseif($input['bidang'] == 3){
                foreach ($data as $key => $val) {
                    $hasila = DB::table('hp_headers')
                            ->join('hp_details', 'hp_details.hp_header_id', '=', 'hp_headers.id')
                            ->join('parameter', 'parameter.id', '=', 'hp_details.parameter_id')
                            ->leftjoin('tb_hp', 'tb_hp.id', '=', 'hp_details.hasil_pemeriksaan')
                            ->where('hp_headers.siklus', $input['siklus'])
                            ->where('hp_headers.type', 'a')
                            ->where('hp_headers.id_registrasi', $val->id)
                            ->orderBy('parameter.bagian', 'asc')
                            ->select('hp_details.pendidikan_petugas', 'hp_details.hasil_pemeriksaan', 'tb_hp.hp', 'tb_hp.parameter_id', 'hp_details.parameter_id as parameter', 'parameter.nama_parameter')
                            ->groupBy('parameter.id')
                            ->orderBy('parameter.id')
                            ->get();
                    foreach ($hasila as $key => $ha) {
                        $target = DB::table('tb_rujukan_urinalisa')
                                    ->where('parameter', $ha->parameter)
                                    ->where('siklus', $input['siklus'])
                                    ->where('tahun', $input['tahun'])
                                    ->where('type', 'a')
                                    ->first();
                        $ha->target = $target->rujukan;
                    }
                    $val->hasila = $hasila;

                    $hasilb = DB::table('hp_headers')
                            ->join('hp_details', 'hp_details.hp_header_id', '=', 'hp_headers.id')
                            ->join('parameter', 'parameter.id', '=', 'hp_details.parameter_id')
                            ->leftjoin('tb_hp', 'tb_hp.id', '=', 'hp_details.hasil_pemeriksaan')
                            ->where('hp_headers.siklus', $input['siklus'])
                            ->where('hp_headers.id_registrasi', $val->id)
                            ->where('hp_headers.type', 'b')
                            ->orderBy('parameter.bagian', 'asc')
                            ->select('hp_details.pendidikan_petugas', 'hp_details.hasil_pemeriksaan', 'tb_hp.hp', 'tb_hp.parameter_id', 'hp_details.parameter_id as parameter', 'parameter.nama_parameter')
                            ->groupBy('parameter.id')
                            ->orderBy('parameter.id')
                            ->get();
                    foreach ($hasilb as $key => $ha) {
                        $target = DB::table('tb_rujukan_urinalisa')
                                    ->where('parameter', $ha->parameter)
                                    ->where('siklus', $input['siklus'])
                                    ->where('tahun', $input['tahun'])
                                    ->where('type', 'b')
                                    ->first();
                        $ha->target = $target->rujukan;
                    }
                    $val->hasilb = $hasilb;
                }
            }
        }elseif ($input['bidang'] == 4) {
            $data = DB::table('tb_registrasi')
                    ->join('tb_bta', 'tb_bta.id_registrasi','=','tb_registrasi.id')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->join('provinces', 'provinces.id', '=', 'perusahaan.provinsi')
                    ->join('regencies', 'regencies.id', '=', 'perusahaan.kota')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $input['tahun'])
                    ->where('tb_registrasi.bidang', $input['bidang'])
                    ->select('tb_registrasi.id', 'perusahaan.nama_lab','perusahaan.pemantapan_mutu', 'provinces.name as provinsi', 'regencies.name as kota')
                    ->groupBy('tb_registrasi.id')
                    ->get();
            foreach ($data as $key => $val) {
                $evaluasi = DB::table('tb_evaluasi_bta')
                            ->join('tb_bta', 'tb_bta.kode','=','tb_evaluasi_bta.kode_sediaan')
                            ->leftjoin('tb_pendidikan','tb_pendidikan.id','=','tb_bta.pendidikan_petugas')
                            ->where('tb_evaluasi_bta.id_registrasi', $val->id)
                            ->where('tb_evaluasi_bta.siklus', $input['siklus'])
                            ->where('tb_evaluasi_bta.tahun', $input['tahun'])
                            ->get();
                $val->evaluasi = $evaluasi;
            }
            // dd($data);
        }elseif ($input['bidang'] == 5) {
            $data = DB::table('tb_registrasi')
                    ->join('tb_telurcacing','tb_telurcacing.id_registrasi','=','tb_registrasi.id')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->join('provinces', 'provinces.id', '=', 'perusahaan.provinsi')
                    ->join('regencies', 'regencies.id', '=', 'perusahaan.kota')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $input['tahun'])
                    ->where('tb_registrasi.bidang', $input['bidang'])
                    ->select('tb_registrasi.id', 'perusahaan.nama_lab', 'perusahaan.pemantapan_mutu', 'provinces.name as provinsi', 'regencies.name as kota')
                    ->groupBy('tb_registrasi.id')
                    ->get();
            foreach ($data as $key => $val) {
                $evaluasi = DB::table('tb_evaluasi_tc')
                        ->join('tb_telurcacing','tb_telurcacing.kode_botol','=','tb_evaluasi_tc.kode_sediaan')
                        ->leftjoin('tb_pendidikan','tb_pendidikan.id','=','tb_telurcacing.pendidikan_petugas')
                        ->where('tb_evaluasi_tc.siklus', $input['siklus'])
                        ->where('tb_evaluasi_tc.tahun', $input['tahun'])
                        ->where('tb_evaluasi_tc.id_registrasi', $val->id)
                        ->get();
                $val->evaluasi = $evaluasi;
            }
            // dd($data);
        }elseif ($input['bidang'] == 10) {
            $data = DB::table('tb_registrasi')
                    ->join('tb_malaria','tb_malaria.id_registrasi','=','tb_registrasi.id')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->join('provinces', 'provinces.id', '=', 'perusahaan.provinsi')
                    ->join('regencies', 'regencies.id', '=', 'perusahaan.kota')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $input['tahun'])
                    ->where('tb_registrasi.bidang', $input['bidang'])
                    ->select('tb_registrasi.id', 'perusahaan.nama_lab', 'perusahaan.pemantapan_mutu', 'provinces.name as provinsi', 'regencies.name as kota')
                    ->groupBy('tb_registrasi.id')
                    ->get();
            foreach ($data as $key => $val) {
                $evaluasi = DB::table('tb_evaluasi_nilai_malaria')
                        ->join('tb_malaria','tb_malaria.kode','=','tb_evaluasi_nilai_malaria.kode_sediaan')
                        ->leftjoin('tb_pendidikan','tb_pendidikan.id','=','tb_malaria.pendidikan_petugas')
                        ->where('tb_evaluasi_nilai_malaria.siklus', $input['siklus'])
                        ->where('tb_evaluasi_nilai_malaria.tahun', $input['tahun'])
                        ->where('tb_evaluasi_nilai_malaria.id_registrasi', $val->id)
                        ->get();
                $val->evaluasi = $evaluasi;
            }
        }elseif ($input['bidang'] == 13) {
            $data = DB::table('tb_registrasi')
                    ->join('data_antibiotik','data_antibiotik.id_registrasi','=','tb_registrasi.id')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->join('provinces', 'provinces.id', '=', 'perusahaan.provinsi')
                    ->join('regencies', 'regencies.id', '=', 'perusahaan.kota')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $input['tahun'])
                    ->where('tb_registrasi.bidang', $input['bidang'])
                    ->select('tb_registrasi.id', 'perusahaan.nama_lab', 'perusahaan.pemantapan_mutu', 'provinces.name as provinsi', 'regencies.name as kota')
                    ->groupBy('tb_registrasi.id')
                    ->get();
            foreach ($data as $key => $val) {
                $evaluasi = DB::select("SELECT
                                            tb_evaluasi_bakteri.kode_bahan as kode_sediaan, tb_evaluasi_bakteri.nilai_1 as nilai, data_antibiotik.pendidikan_petugas
                                        FROM
                                            `data_antibiotik`
                                        INNER JOIN tb_evaluasi_bakteri on tb_evaluasi_bakteri.kode_bahan = CONCAT(SUBSTRING(data_antibiotik.kode_lab, 1, 9),data_antibiotik.kd_bahan,'/',data_antibiotik.siklus,SUBSTRING(data_antibiotik.kode_lab, 12))
                                        LEFT JOIN tb_pendidikan on tb_pendidikan.id = data_antibiotik.pendidikan_petugas
                                        WHERE tb_evaluasi_bakteri.siklus = ".$input['siklus']."
                                        AND tb_evaluasi_bakteri.tahun = ".$input['tahun']."
                                        AND tb_evaluasi_bakteri.id_registrasi = ".$val->id."
                                        ");
                $val->evaluasi = $evaluasi;
            }
        }
        if ($input['bidang'] <= 2) {
            return View('laporan/tknasional/proses_patologi', compact('data', 'input', 'bidang'));
        }elseif ($input['bidang'] == 3) {
            return View('laporan/tknasional/proses_urinalisa', compact('data', 'input', 'bidang'));
        }elseif($input['bidang'] == 4 || $input['bidang'] == 5 || $input['bidang'] == 10 || $input['bidang'] == 13){
            return View('laporan/tknasional/proses_bta', compact('data', 'input', 'bidang'));
        }
    }
}

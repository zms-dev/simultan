<?php

namespace App\Http\Controllers;
use DB;
use Excel;
use PDF;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function wilayah($id)
    {
        $data = DB::table('provinces')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                // ->select('provinces.name', DB::raw("count(tb_registrasi.siklus) as count"))
                // ->groupBy('provinces.id')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.siklus','=',1)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->siklus1 = $checkSiklus1;
            $checkSiklus2 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.siklus','=',2)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->siklus2 = $checkSiklus2;
            $checkSiklus12 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.siklus','=',12)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->siklus12 = $checkSiklus12;

            $r->jumlah = $checkSiklus1 + $checkSiklus2 + $checkSiklus12;
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function wilayahexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('provinces')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                // ->select('provinces.name', DB::raw("count(tb_registrasi.siklus) as count"))
                // ->groupBy('provinces.id')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.siklus','=',1)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus1 = $checkSiklus1;
            $checkSiklus2 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.siklus','=',2)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus2 = $checkSiklus2;
            $checkSiklus12 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.siklus','=',12)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus12 = $checkSiklus12;

            $r->jumlah = $checkSiklus1 + $checkSiklus2 + $checkSiklus12;
        }
        // dd($data);
        // return response()->json(['Hasil'=>$data]);

        Excel::create('Rekapitulasi Peserta Berdasarkan Wilayah', function($excel) use ($data) {
            $excel->sheet('Wilayah', function($sheet) use ($data) {
                $sheet->loadView('dashboard.wilayah.view', array('data'=>$data) );
            });

        })->download('xlsx');
    }

    public function wilayahpdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('provinces')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                // ->select('provinces.name', DB::raw("count(tb_registrasi.siklus) as count"))
                // ->groupBy('provinces.id')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.siklus','=',1)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus1 = $checkSiklus1;
            $checkSiklus2 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.siklus','=',2)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus2 = $checkSiklus2;
            $checkSiklus12 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.siklus','=',12)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus12 = $checkSiklus12;

            $r->jumlah = $checkSiklus1 + $checkSiklus2 + $checkSiklus12;
        }
        // dd($data);
        // return response()->json(['Hasil'=>$data]);
        $pdf = PDF::loadview('dashboard.wilayah.view', compact('data'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Rekapitulasi Peserta Berdasarkan Wilayah.pdf');
    }

    public function badanusaha($id)
    {
        $data = DB::table('badan_usaha')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                // ->select('provinces.name', DB::raw("count(tb_registrasi.siklus) as count"))
                // ->groupBy('provinces.id')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.siklus','=',1)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->siklus1 = $checkSiklus1;
            $checkSiklus2 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.siklus','=',2)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->siklus2 = $checkSiklus2;
            $checkSiklus12 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.siklus','=',12)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->siklus12 = $checkSiklus12;

            $r->jumlah = $checkSiklus1 + $checkSiklus2 + $checkSiklus12;
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function badanusahaexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('badan_usaha')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                // ->select('provinces.name', DB::raw("count(tb_registrasi.siklus) as count"))
                // ->groupBy('provinces.id')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.siklus','=',1)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus1 = $checkSiklus1;
            $checkSiklus2 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.siklus','=',2)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus2 = $checkSiklus2;
            $checkSiklus12 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.siklus','=',12)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus12 = $checkSiklus12;

            $r->jumlah = $checkSiklus1 + $checkSiklus2 + $checkSiklus12;
        }
        // dd($data);
        // return response()->json(['Hasil'=>$data]);
        Excel::create('Rekapitulasi Peserta Berdasarkan Badan Usaha', function($excel) use ($data) {
            $excel->sheet('Badan usaha', function($sheet) use ($data) {
                $sheet->loadView('dashboard.badanusaha.view', array('data'=>$data) );
            });

        })->download('xlsx');
    }

    public function badanusahapdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('badan_usaha')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                // ->select('provinces.name', DB::raw("count(tb_registrasi.siklus) as count"))
                // ->groupBy('provinces.id')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.siklus','=',1)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus1 = $checkSiklus1;
            $checkSiklus2 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.siklus','=',2)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus2 = $checkSiklus2;
            $checkSiklus12 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.siklus','=',12)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus12 = $checkSiklus12;

            $r->jumlah = $checkSiklus1 + $checkSiklus2 + $checkSiklus12;
        }
        // dd($data);
        // return response()->json(['Hasil'=>$data]);
        $pdf = PDF::loadview('dashboard.badanusaha.view', compact('data'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Rekapitulasi Peserta Berdasarkan Badan Usaha.pdf');
    }

    public function badanusahana(){
        $provinsi = DB::table('provinces')->get();
        return view('dashboard.badanusahaprovinsi.index', compact('provinsi'));
    }

    public function badanusahaprovinsi(\Illuminate\Http\Request $request)
    {
        $asu = $request->all();
        $data = DB::table('badan_usaha')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                // ->select('provinces.name', DB::raw("count(tb_registrasi.siklus) as count"))
                // ->groupBy('provinces.id')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('provinces.id', $asu['provinsi'])
                            ->where('tb_registrasi.siklus','=',1)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $asu['tahun'])
                            ->count();
            $r->siklus1 = $checkSiklus1;
            $checkSiklus2 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('provinces.id', $asu['provinsi'])
                            ->where('tb_registrasi.siklus','=',2)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $asu['tahun'])
                            ->count();
            $r->siklus2 = $checkSiklus2;
            $checkSiklus12 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('provinces.id', $asu['provinsi'])
                            ->where('tb_registrasi.siklus','=',12)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $asu['tahun'])
                            ->count();
            $r->siklus12 = $checkSiklus12;

            $r->jumlah = $checkSiklus1 + $checkSiklus2 + $checkSiklus12;
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function badanusahaexcelprovinsi(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('badan_usaha')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                // ->select('provinces.name', DB::raw("count(tb_registrasi.siklus) as count"))
                // ->groupBy('provinces.id')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('provinces.id', $input['provinsi'])
                            ->where('tb_registrasi.siklus','=',1)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus1 = $checkSiklus1;
            $checkSiklus2 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('provinces.id', $input['provinsi'])
                            ->where('tb_registrasi.siklus','=',2)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus2 = $checkSiklus2;
            $checkSiklus12 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('provinces.id', $input['provinsi'])
                            ->where('tb_registrasi.siklus','=',12)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus12 = $checkSiklus12;

            $r->jumlah = $checkSiklus1 + $checkSiklus2 + $checkSiklus12;
        }
        // dd($data);
        // return response()->json(['Hasil'=>$data]);
        Excel::create('Rekapitulasi Peserta Badan Usaha Berdasarkan Provinsi', function($excel) use ($data) {
            $excel->sheet('Badan usaha', function($sheet) use ($data) {
                $sheet->loadView('dashboard.badanusahaprovinsi.view', array('data'=>$data) );
            });

        })->download('xlsx');
    }

    public function badanusahapdfprovinsi(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('badan_usaha')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                // ->select('provinces.name', DB::raw("count(tb_registrasi.siklus) as count"))
                // ->groupBy('provinces.id')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.siklus','=',1)
                            ->where('provinces.id', $input['provinsi'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus1 = $checkSiklus1;
            $checkSiklus2 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.siklus','=',2)
                            ->where('provinces.id', $input['provinsi'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus2 = $checkSiklus2;
            $checkSiklus12 = DB::table('tb_registrasi')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.siklus','=',12)
                            ->where('provinces.id', $input['provinsi'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus12 = $checkSiklus12;

            $r->jumlah = $checkSiklus1 + $checkSiklus2 + $checkSiklus12;
        }
        // dd($data);
        // return response()->json(['Hasil'=>$data]);
        $pdf = PDF::loadview('dashboard.badanusahaprovinsi.view', compact('data'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Rekapitulasi Peserta Badan Usaha Berdasarkan Provinsi.pdf');
    }

    public function bidang($id)
    {
        $data = DB::table('sub_bidang')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                // ->select('provinces.name', DB::raw("count(tb_registrasi.siklus) as count"))
                // ->groupBy('provinces.id')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang', $r->id)
                            ->where('tb_registrasi.siklus','=',1)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->siklus1 = $checkSiklus1;
            $checkSiklus2 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang', $r->id)
                            ->where('tb_registrasi.siklus','=',2)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->siklus2 = $checkSiklus2;
            $checkSiklus12 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang', $r->id)
                            ->where('tb_registrasi.siklus','=',12)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->siklus12 = $checkSiklus12;

            $r->jumlah = $checkSiklus1 + $checkSiklus2 + $checkSiklus12;
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function bidangexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('sub_bidang')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                // ->select('provinces.name', DB::raw("count(tb_registrasi.siklus) as count"))
                // ->groupBy('provinces.id')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang', $r->id)
                            ->where('tb_registrasi.siklus','=',1)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus1 = $checkSiklus1;
            $checkSiklus2 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang', $r->id)
                            ->where('tb_registrasi.siklus','=',2)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus2 = $checkSiklus2;
            $checkSiklus12 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang', $r->id)
                            ->where('tb_registrasi.siklus','=',12)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus12 = $checkSiklus12;

            $r->jumlah = $checkSiklus1 + $checkSiklus2 + $checkSiklus12;
        }
        // dd($data);
        // return response()->json(['Hasil'=>$data]);
        Excel::create('Rekapitulasi Peserta Berdasarkan Bidang Pengujian', function($excel) use ($data) {
            $excel->sheet('Bidang Pengujian', function($sheet) use ($data) {
                $sheet->loadView('dashboard.bidang.view', array('data'=>$data) );
            });

        })->download('xlsx');
    }

    public function bidangpdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('sub_bidang')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                // ->select('provinces.name', DB::raw("count(tb_registrasi.siklus) as count"))
                // ->groupBy('provinces.id')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang', $r->id)
                            ->where('tb_registrasi.siklus','=',1)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus1 = $checkSiklus1;
            $checkSiklus2 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang', $r->id)
                            ->where('tb_registrasi.siklus','=',2)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus2 = $checkSiklus2;
            $checkSiklus12 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang', $r->id)
                            ->where('tb_registrasi.siklus','=',12)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus12 = $checkSiklus12;

            $r->jumlah = $checkSiklus1 + $checkSiklus2 + $checkSiklus12;
        }
        // dd($data);
        // return response()->json(['Hasil'=>$data]);
        $pdf = PDF::loadview('dashboard.bidang.view', compact('data'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Rekapitulasi Peserta Berdasarkan Bidang Pengujian.pdf');
    }


    public function pembayaran($id)
    {
        $data = DB::table('pembayaran')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                // ->select('provinces.name', DB::raw("count(tb_registrasi.siklus) as count"))
                // ->groupBy('provinces.id')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->where('tb_registrasi.siklus','=',1)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->siklus1 = $checkSiklus1;
            $checkSiklus2 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->where('tb_registrasi.siklus','=',2)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->siklus2 = $checkSiklus2;
            $checkSiklus12 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->where('tb_registrasi.siklus','=',12)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->siklus12 = $checkSiklus12;

            $r->jumlah = $checkSiklus1 + $checkSiklus2 + $checkSiklus12;
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function pembayaranexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('pembayaran')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                // ->select('provinces.name', DB::raw("count(tb_registrasi.siklus) as count"))
                // ->groupBy('provinces.id')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->where('tb_registrasi.siklus','=',1)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus1 = $checkSiklus1;
            $checkSiklus2 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->where('tb_registrasi.siklus','=',2)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus2 = $checkSiklus2;
            $checkSiklus12 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->where('tb_registrasi.siklus','=',12)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus12 = $checkSiklus12;

            $r->jumlah = $checkSiklus1 + $checkSiklus2 + $checkSiklus12;
        }
        // dd($data);
        // return response()->json(['Hasil'=>$data]);
        Excel::create('Rekapitulasi Peserta Berdasarkan Pembayaran', function($excel) use ($data) {
            $excel->sheet('Pembayaran', function($sheet) use ($data) {
                $sheet->loadView('dashboard.pembayaran.view', array('data'=>$data) );
            });

        })->download('xlsx');
    }

    public function pembayaranpdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('pembayaran')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                // ->select('provinces.name', DB::raw("count(tb_registrasi.siklus) as count"))
                // ->groupBy('provinces.id')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->where('tb_registrasi.siklus','=',1)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus1 = $checkSiklus1;
            $checkSiklus2 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->where('tb_registrasi.siklus','=',2)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus2 = $checkSiklus2;
            $checkSiklus12 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->where('tb_registrasi.siklus','=',12)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->count();
            $r->siklus12 = $checkSiklus12;

            $r->jumlah = $checkSiklus1 + $checkSiklus2 + $checkSiklus12;
        }
        // dd($data);
        // return response()->json(['Hasil'=>$data]);
        $pdf = PDF::loadview('dashboard.pembayaran.view', compact('data'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Rekapitulasi Peserta Berdasarkan Pembayaran.pdf');
    }


    public function omset($id)
    {
        $data = DB::table('pembayaran')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'pembayaran.id', '=', 'tb_registrasi.id_pembayaran')
                // ->leftjoin('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                // ->select('pembayaran.*')
                // ->groupBy('pembayaran.')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('sub_bidang')
                            ->join('tb_registrasi','tb_registrasi.bidang' ,'sub_bidang.id')
                            ->select(DB::raw('count(tb_registrasi.siklus) as siklus1'), DB::raw('SUM(IF(sub_bidang.id < 6, IF(tb_registrasi.siklus = 1, sub_bidang.tarif / 2, sub_bidang.tarif), IF(tb_registrasi.siklus = 1, sub_bidang.tarif, sub_bidang.tarif * 2))) as harga1'))
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->wherein('tb_registrasi.siklus', array(1,12))
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->first();
            $r->siklus1 = $checkSiklus1->harga1;
            $checkSiklus2 = DB::table('sub_bidang')
                            ->join('tb_registrasi','tb_registrasi.bidang' ,'sub_bidang.id')
                            ->select(DB::raw('count(tb_registrasi.siklus) as siklus2'), DB::raw('SUM(IF(sub_bidang.id < 6, IF(tb_registrasi.siklus = 1, sub_bidang.tarif / 2, sub_bidang.tarif), IF(tb_registrasi.siklus = 1, sub_bidang.tarif, sub_bidang.tarif * 2))) as harga2'))
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->wherein('tb_registrasi.siklus', array(2,12))
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->first();
            $r->siklus2 = $checkSiklus2->harga2;
            $checkSiklus12 = DB::table('sub_bidang')
                            ->join('tb_registrasi','tb_registrasi.bidang' ,'sub_bidang.id')
                            ->select(DB::raw('count(tb_registrasi.siklus) as siklus3'), DB::raw('sum(sub_bidang.tarif) as harga3'))
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->where('tb_registrasi.siklus','=',12)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->first();
            $r->siklus12 = $checkSiklus12->harga3;

            $r->jumlah = $checkSiklus1->siklus1 + $checkSiklus2->siklus2;
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function omsetexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('pembayaran')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'pembayaran.id', '=', 'tb_registrasi.id_pembayaran')
                // ->leftjoin('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                // ->select('pembayaran.*')
                // ->groupBy('pembayaran.')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('sub_bidang')
                            ->join('tb_registrasi','tb_registrasi.bidang' ,'sub_bidang.id')
                            ->select(DB::raw('count(tb_registrasi.siklus) as siklus1'), DB::raw('sum(sub_bidang.tarif) as harga1'))
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->where('tb_registrasi.siklus','=',1)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->first();
            $r->siklus1 = $checkSiklus1->harga1;
            $checkSiklus2 = DB::table('sub_bidang')
                            ->join('tb_registrasi','tb_registrasi.bidang' ,'sub_bidang.id')
                            ->select(DB::raw('count(tb_registrasi.siklus) as siklus2'), DB::raw('sum(sub_bidang.tarif) as harga2'))
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->where('tb_registrasi.siklus','=',2)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->first();
            $r->siklus2 = $checkSiklus2->harga2;
            $checkSiklus12 = DB::table('sub_bidang')
                            ->join('tb_registrasi','tb_registrasi.bidang' ,'sub_bidang.id')
                            ->select(DB::raw('count(tb_registrasi.siklus) as siklus3'), DB::raw('sum(sub_bidang.tarif) as harga3'))
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->where('tb_registrasi.siklus','=',12)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->first();
            $r->siklus12 = $checkSiklus12->harga3;

            $r->jumlah = $checkSiklus1->siklus1 + $checkSiklus2->siklus2 + $checkSiklus12->siklus3;
        }
        // dd($data);
        // return response()->json(['Hasil'=>$data]);
        Excel::create('Rekapitulasi Peserta Berdasarkan Omset', function($excel) use ($data) {
            $excel->sheet('Omset', function($sheet) use ($data) {
                $sheet->loadView('dashboard.omset.view', array('data'=>$data) );
            });

        })->download('xlsx');
    }

    public function omsetpdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('pembayaran')
                // ->leftjoin('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                // ->join('tb_registrasi', 'pembayaran.id', '=', 'tb_registrasi.id_pembayaran')
                // ->leftjoin('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                // ->select('pembayaran.*')
                // ->groupBy('pembayaran.')
                ->get();
        foreach($data as $skey => $r)
        {
            $checkSiklus1 = DB::table('sub_bidang')
                            ->join('tb_registrasi','tb_registrasi.bidang' ,'sub_bidang.id')
                            ->select(DB::raw('count(tb_registrasi.siklus) as siklus1'), DB::raw('sum(sub_bidang.tarif) as harga1'))
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->where('tb_registrasi.siklus','=',1)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->first();
            $r->siklus1 = $checkSiklus1->harga1;
            $checkSiklus2 = DB::table('sub_bidang')
                            ->join('tb_registrasi','tb_registrasi.bidang' ,'sub_bidang.id')
                            ->select(DB::raw('count(tb_registrasi.siklus) as siklus2'), DB::raw('sum(sub_bidang.tarif) as harga2'))
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->where('tb_registrasi.siklus','=',2)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->first();
            $r->siklus2 = $checkSiklus2->harga2;
            $checkSiklus12 = DB::table('sub_bidang')
                            ->join('tb_registrasi','tb_registrasi.bidang' ,'sub_bidang.id')
                            ->select(DB::raw('count(tb_registrasi.siklus) as siklus3'), DB::raw('sum(sub_bidang.tarif) as harga3'))
                            ->where('tb_registrasi.id_pembayaran', $r->id)
                            ->where('tb_registrasi.siklus','=',12)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->first();
            $r->siklus12 = $checkSiklus12->harga3;

            $r->jumlah = $checkSiklus1->siklus1 + $checkSiklus2->siklus2 + $checkSiklus12->siklus3;
        }
        // dd($data);
        // return response()->json(['Hasil'=>$data]);
        $pdf = PDF::loadview('dashboard.omset.view', compact('data'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Rekapitulasi Peserta Berdasarkan Omset.pdf');
    }

    public function pnpme(){
        return view('dashboard.pnpme.index');
    }

    public function getparameter($id){
        $data = DB::table('parameter')->select('id as Kode','kategori as Kategori','nama_parameter as Parameter')->where('kategori',$id)->get();
        return response()->json(['Hasil'=>$data]);
    }

    public function pnpmep(\Illuminate\Http\Request $request){
        $input = $request->all();
        if ($input['bidang'] == 'hematologi') {
            $bidang = 'HEM';
        }elseif ($input['bidang'] == 'urinalisa') {
            $bidang = 'URI';
        }elseif ($input['bidang'] == 'kimia klinik') {
            $bidang = 'KKL';
        }
        $parameter = DB::table('parameter')
                    ->where('kategori', $input['bidang'])
                    ->get();
        $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->join('hp_headers', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
                ->leftjoin('hp_details', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                ->leftjoin('parameter', 'hp_details.parameter_id', '=', 'parameter.id')
                ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
                ->select('tb_registrasi.kode_lebpes','tb_registrasi.id', 'perusahaan.nama_lab as instansi', 'tb_registrasi.perusahaan_id', 'hp_headers.catatan')
                ->groupBy('tb_registrasi.kode_lebpes')
                ->groupBy('hp_headers.catatan')
                ->groupBy('tb_registrasi.id')
                ->groupBy('tb_registrasi.perusahaan_id')
                ->groupBy('perusahaan.nama_lab')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.siklus', $input['siklus'])
                ->where('sub_bidang.alias', $bidang)
                // ->where('parameter.id', $input['parameter'])
                ->where('hp_headers.type', $input['type'])
                ->get();
        foreach($data as $skey => $r)
        {
            $isi = DB::table('tb_registrasi')
                        ->join('hp_headers', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
                        ->leftjoin('hp_details', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                        ->leftjoin('parameter', 'hp_details.parameter_id', '=', 'parameter.id')
                        ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('tb_registrasi.id', $r->id)
                        ->where('hp_headers.type', $input['type'])
                        ->get();
            $r->h = $isi;    
        }
        // dd($parameter);
        // dd($data);
        return view('dashboard.pnpme.proses', compact('data', 'input', 'parameter'));
    }

    public function pnpmee(\Illuminate\Http\Request $request){
        $input = $request->all();
        if ($input['bidang'] == 'hematologi') {
            $bidang = 'HEM';
        }elseif ($input['bidang'] == 'urinalisa') {
            $bidang = 'URI';
        }elseif ($input['bidang'] == 'kimia klinik') {
            $bidang = 'KKL';
        }
        $parameter = DB::table('parameter')
                    ->where('kategori', $input['bidang'])
                    ->get();
        $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->join('hp_headers', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
                ->leftjoin('hp_details', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                ->leftjoin('parameter', 'hp_details.parameter_id', '=', 'parameter.id')
                ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
                ->select('tb_registrasi.kode_lebpes','tb_registrasi.id', 'perusahaan.nama_lab as instansi', 'tb_registrasi.perusahaan_id', 'hp_headers.catatan')
                ->groupBy('tb_registrasi.kode_lebpes')
                ->groupBy('hp_headers.catatan')
                ->groupBy('tb_registrasi.id')
                ->groupBy('tb_registrasi.perusahaan_id')
                ->groupBy('perusahaan.nama_lab')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.siklus', $input['siklus'])
                ->where('sub_bidang.alias', $bidang)
                // ->where('parameter.id', $input['parameter'])
                ->where('hp_headers.type', $input['type'])
                ->get();
        foreach($data as $skey => $r)
        {
            $isi = DB::table('tb_registrasi')
                        ->join('hp_headers', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
                        ->leftjoin('hp_details', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                        ->leftjoin('parameter', 'hp_details.parameter_id', '=', 'parameter.id')
                        ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('tb_registrasi.id', $r->id)
                        ->where('hp_headers.type', $input['type'])
                        ->get();
            $r->h = $isi;    
        }
        // dd($parameter);
        // dd($data);
        return view('dashboard.pnpme.excel', compact('data', 'input', 'parameter'));
    }

    public function pnpmepdf(\Illuminate\Http\Request $request){
        $input = $request->all();
        if ($input['bidang'] == 'hematologi') {
            $bidang = 'HEM';
        }elseif ($input['bidang'] == 'urinalisa') {
            $bidang = 'URI';
        }elseif ($input['bidang'] == 'kimia klinik') {
            $bidang = 'KKL';
        }
        $parameter = DB::table('parameter')
                    ->where('kategori', $input['bidang'])
                    ->get();
        $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->join('hp_headers', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
                ->leftjoin('hp_details', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                ->leftjoin('parameter', 'hp_details.parameter_id', '=', 'parameter.id')
                ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
                ->select('tb_registrasi.kode_lebpes','tb_registrasi.id', 'perusahaan.nama_lab as instansi', 'tb_registrasi.perusahaan_id', 'hp_headers.catatan')
                ->groupBy('tb_registrasi.kode_lebpes')
                ->groupBy('hp_headers.catatan')
                ->groupBy('tb_registrasi.id')
                ->groupBy('tb_registrasi.perusahaan_id')
                ->groupBy('perusahaan.nama_lab')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.siklus', $input['siklus'])
                ->where('sub_bidang.alias', $bidang)
                // ->where('parameter.id', $input['parameter'])
                ->where('hp_headers.type', $input['type'])
                ->get();
        foreach($data as $skey => $r)
        {
            $isi = DB::table('tb_registrasi')
                        ->join('hp_headers', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
                        ->leftjoin('hp_details', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                        ->leftjoin('parameter', 'hp_details.parameter_id', '=', 'parameter.id')
                        ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('tb_registrasi.id', $r->id)
                        ->where('hp_headers.type', $input['type'])
                        ->get();
            $r->h = $isi;    
        }
        // dd($parameter);
        // dd($data);
        $pdf = PDF::loadview('dashboard.pnpme.excel', compact('data', 'input', 'parameter'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream($input['bidang'].'.pdf');
    }
}

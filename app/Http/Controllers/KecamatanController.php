<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\kecamatan as Kecamatan;
use Illuminate\Support\Facades\Redirect;

class KecamatanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request)
    {
        // $data = DB::table('districts')
        //         ->join('regencies', 'districts.regency_id', '=', 'regencies.id')
        //         ->select('districts.*', 'regencies.name as Name')
        //         ->get();
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = Kecamatan::select(DB::raw('*, @rownum := @rownum +1 as rownum'));
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="kecamatan/edit/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        <i class="icon-pencil"></i>
                                    </button>
                                </a> &nbsp;
                                <a href="kecamatan/delete'.'/'.$data->id.'">
                                    <button class="btn btn-danger btn-xs">
                                        <i class="icon-trash "></i>
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('back/kecamatan/index');
    }

    public function in()
    {
        $data = DB::table('regencies')->get();
        return view('back/kecamatan/insert', compact('data'));
    }

    public function edit($id)
    {
        $data = DB::table('districts')
                ->join('regencies', 'districts.regency_id', '=', 'regencies.id')
                ->select('districts.*', 'regencies.name as Name')
                ->where('districts.id', $id)
                ->get();
        $regency = DB::table('regencies')->get();
        return view('back/kecamatan/update', compact('data','regency'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $data['name'] = $request->name;
        Kecamatan::where('id',$id)->update($data);
        Session::flash('message', 'Data Kecamatan Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/kecamatan');
    }    

    public function insert(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        Kecamatan::create($data);
        Session::flash('message', 'Data Kecamatan Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/kecamatan');
    }

    public function delete($id){
        Session::flash('message', 'Data Kecamatan Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Kecamatan::find($id)->delete();
        return redirect("admin/kecamatan");
    }
}

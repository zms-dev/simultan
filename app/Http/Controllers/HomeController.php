<?php

namespace App\Http\Controllers;
use DB;
use PDF;
use Session;

use App\User;
use App\JobsEvaluasi;
use App\Jobs\Email;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        $banner = DB::table('tb_banner')->orderBy('id', 'desc')->get();
        return view('index', compact('banner'));
    }
    public function privacypolicy() 
    {
        return view('privacy-policy');
    }
    public function berita() 
    {
        $data = DB::table('tb_berita')->orderBy('id', 'desc')->get();
        return view('index', compact('berita'));
    }
    public function ceknourut()
    {
        $tahun = date('Y');
        $register = DB::table('tb_registrasi')
                    ->select(DB::raw('CAST(no_urut AS UNSIGNED) as no_uruts'))
                    ->join('users', 'users.id','=','tb_registrasi.created_by')
                    ->where(DB::raw('Year(tb_registrasi.created_at)'), $tahun)
                    ->whereNotNull('no_urut')
                    ->groupBy('no_uruts')
                    ->orderBy('no_uruts', 'desc')
                    ->first();
        if(count($register)){
            $cekno = (int)$register->no_uruts + 1;
            $no_urut = sprintf("%04s", $cekno);
        }else{
            $no_urut = '0001';
        }
        dd($no_urut);
    }
    public function jadwal() 
    {
        $jadwal = DB::table('tb_jadwal')->orderBy('id', 'desc')->first();
        $schedule = DB::table('schedule')->orderBy('sort', 'asc')->get();
        return view('jadwal', compact('jadwal', 'schedule'));
    }
    public function tarif()
    {
        $tahun = date('Y');
        $data = DB::table('tb_bidang')
            ->join('sub_bidang', 'tb_bidang.id', '=', 'sub_bidang.id_bidang')
            ->leftjoin('tb_registrasi','tb_registrasi.bidang','=', DB::raw('sub_bidang.id AND YEAR(tb_registrasi.created_at) = '.$tahun.''))
            ->select('sub_bidang.parameter','sub_bidang.kuota_1','sub_bidang.kuota_2', 'sub_bidang.id_bidang','sub_bidang.tarif',  'tb_bidang.bidang as Bidang', 
                    DB::raw('(CASE
                                WHEN sub_bidang.kuota_1 != 0  THEN
                                    sub_bidang.kuota_1 - (
                                    COUNT(
                                        CASE WHEN (tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1) AND tb_registrasi.status > 1 THEN tb_registrasi.bidang END))
                            END
                            ) as sisa_kuota_1'),
                    DB::raw('(CASE
                                WHEN sub_bidang.kuota_2 != 0  THEN
                                    sub_bidang.kuota_2 - (
                                    COUNT(
                                        CASE WHEN (tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 2) AND tb_registrasi.status > 1 THEN tb_registrasi.bidang END))
                            END
                            ) as sisa_kuota_2'))
            ->groupBy('sub_bidang.id')
            ->where('sub_bidang.status','=','1')
            ->get();
        // dd($data);
        return view('home', compact('data'));
    }
    public function manual()
    {
        $juklak = DB::table('sub_bidang')
            ->join('juklak', 'sub_bidang.id', '=', 'juklak.id_bidang')
            ->select('juklak.*', 'sub_bidang.parameter as Bidang')
            ->get();
        $data = DB::table('manual_book')->orderBy('id', 'desc')->get();
        return view('manual_book', compact('data', 'juklak'));
    }
    public function penggunaan(){
        $this->middleware('auth');
        $user = Auth::user()->role;
        $data = DB::table('penggunaan')->where('role', $user)->get();
        return view('penggunaan', compact('data'));
    }
    public function sptjm()
    {
        $sptjm = DB::table('tb_sptjm')
            ->get();
        return view('sptjm', compact('sptjm'));
    }

    public function evaluasi()
    {
        $this->middleware('auth');
        $user = Auth::user()->role;
        $uss = Auth::id();
        // dd($user);
        $sertifikat = DB::table('tb_registrasi')
            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
            ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
            ->where('tb_registrasi.created_by', $uss)
            ->where('tb_registrasi.evaluasi', 'done')
            ->where('tb_registrasi.evaluasi2', 'done')
            ->select('tb_registrasi.id', 'perusahaan.nama_lab', 'sub_bidang.parameter')
            ->get();
        if ($user == 3) {
            $data = DB::table('evaluasi')
                ->join('tb_registrasi', 'evaluasi.id_register', '=', 'tb_registrasi.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->where('tb_registrasi.created_by', $uss)
                ->select('evaluasi.id', 'evaluasi.sertifikat', 'evaluasi.type', 'evaluasi.file', 'perusahaan.nama_lab', 'sub_bidang.parameter')
                ->paginate(10);
        }else{
            $data = DB::table('evaluasi')
                ->join('tb_registrasi', 'evaluasi.id_register', '=', 'tb_registrasi.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->select('evaluasi.id', 'evaluasi.sertifikat', 'evaluasi.type', 'evaluasi.file', 'perusahaan.nama_lab', 'sub_bidang.parameter')
                ->paginate(10);
        }
        return view('evaluasi', compact('user','data', 'sertifikat'));
    }

    public function sertifikat($id){
        $user = Auth::id();
        $data = DB::table('tb_registrasi')
            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
            ->leftjoin('evaluasi', 'tb_registrasi.id', '=', 'evaluasi.id_register')
            ->leftjoin('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
            ->where('tb_registrasi.created_by', $user)
            ->where('tb_registrasi.id', $id)
            ->where('tb_registrasi.evaluasi', 'done')
            ->where('tb_registrasi.evaluasi2', 'done')
            ->where('evaluasi.type', 'b')
            ->select('tb_registrasi.id', 'tb_registrasi.kode_lebpes', 'tb_registrasi.siklus', DB::raw('year(tb_registrasi.created_at) as tahun'), 'perusahaan.nama_lab', 'sub_bidang.parameter', 'tb_bidang.bidang', 'evaluasi.created_at')
            ->first();
            // dd($data);
        $pdf = PDF::loadview('sertifikat', compact('data'))
            ->setPaper('a4', 'landscape')
            ->setwarnings(false);
        return $pdf->stream('sertifikat.pdf');
        // return view('sertifikat');
    }

    public function sentEmail(\Illuminate\Http\Request $request)
    {
        $jobPending = JobsEvaluasi::where('name','=','email-blast')->where('status','=',0)->first();
        $showAllertPending = false;
        $showAllertSuccess = false;
        $showForm = true;
        if(!empty($jobPending)){
            $showAllertPending = true;
            $showAllertSuccess = false;
            $showForm = false;
        }else{
            $jobSuccess = JobsEvaluasi::where('name','=','email-blast')->where('status','=',1)->where('isread','=',0)->first();
            if(!empty($jobSuccess)){
                $showAllertPending = false;
                $showAllertSuccess = true;
                $showForm = true;
                JobsEvaluasi::where('name','=','email-blast')->where('status','=',1)->where('isread','=',0)->update(['isread'=>1]);
            }else{
                $showForm = true;
            }
        }
        return view('email_blast', compact('showAllertPending','showAllertSuccess','showForm'));
    }

    public function sentEmailna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        // dd($input);
        Session::flash('message', 'System sedang melakukan pengiriman Email.');
        Session::flash('alert-class', 'alert-info');
        $jobs = new JobsEvaluasi;
        $jobs->name = 'email-blast';
        $jobs->description = 'Email Blast';
        $jobs->status = 0;
        $jobs->isread = 0;
        $jobs->save();
        Email::dispatch($input,$jobs->name)->onConnection('redis');
        return redirect('email-blast');
    }

    public function emailkirim()
    {
        $data = DB::table('sub_bidang')->get();

        $jobPending = JobsEvaluasi::where('name','=','email-kirim-hasil')->where('status','=',0)->first();
        $showAllertPending = false;
        $showAllertSuccess = false;
        $showForm = true;
        if(!empty($jobPending)){
            $showAllertPending = true;
            $showAllertSuccess = false;
            $showForm = false;
        }else{
            $jobSuccess = JobsEvaluasi::where('name','=','email-kirim-hasil')->where('status','=',1)->where('isread','=',0)->first();
            if(!empty($jobSuccess)){
                $showAllertPending = false;
                $showAllertSuccess = true;
                $showForm = true;
                JobsEvaluasi::where('name','=','email-kirim-hasil')->where('status','=',1)->where('isread','=',0)->update(['isread'=>1]);
            }else{
                $showForm = true;
            }
        }
        return view('email_blast_kirim_hasil', compact('showAllertPending','showAllertSuccess','data','showForm'));
    }

    public function emailkirimna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        // dd($input);
        $siklus = $input['siklus'];
        $type = $input['bidang'];
        $date = date('Y');
        Session::flash('message', 'System sedang melakukan pengiriman Email.');
        Session::flash('alert-class', 'alert-info');
        $jobs = new JobsEvaluasi;
        $jobs->name = 'email-kirim-hasil';
        $jobs->description = 'Email Kirim Hasil';
        $jobs->status = 0;
        $jobs->isread = 0;
        $jobs->save();
        Email::dispatch($input,$jobs->name)->onConnection('redis');
        return redirect('email-blast-kirim-hasil');

    }

    public function sentEmailVa(\Illuminate\Http\Request $request)
    {
        $this->middleware('auth');
        $tahun = date('Y');
        $users = DB::table('users')
                    ->join('tb_registrasi','tb_registrasi.created_by','=','users.id')
                    ->where('tb_registrasi.status','=','1')
                    ->where(DB::raw('Year(tb_registrasi.created_at)'), $tahun)
                    ->whereNotNull('tb_registrasi.no_urut')
                    ->groupBy('users.id')
                    ->select('tb_registrasi.no_urut','users.email')
                    ->get();
        // dd($users);
        foreach($users as $usr) {
                $datu = [
                    'no_urut' => $usr->no_urut,
                ];
            $sent = Mail::send('email.transfer', $datu , function ($mail) use ($usr)
            {
                $mail->from('pme.bblksub@gmail.com', 'PNPME BBLK SURABAYA ');
                $mail->to($usr->email);
                // $mail->to('tengkufirmansyah2@gmail.com');
                $mail->subject('No VA PNPME Surabaya');
            });
        }
        return redirect('index');
    }
}

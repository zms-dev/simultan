<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;
use Excel;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\register as Register;
use App\bayar as Bayar;
use App\sptjm as SPTJM;
use App\Kwitansi;
use App\EmailVa;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;

class CekTransferController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $year = date("Y");
        $data = DB::table('sub_bidang')
                ->select('users.name', 'users.email', 'perusahaan.nama_lab', 'perusahaan.no_hp', 'tb_registrasi.no_urut', 'tb_registrasi.siklus', 'tb_registrasi.file','tb_registrasi.created_at',
                        DB::raw('group_concat(concat(tb_registrasi.id,"-",sub_bidang.tarif) SEPARATOR \'|\') as id'),
                        DB::raw('group_concat(tb_bidang.bidang SEPARATOR \'|\') as bidang'),
                        DB::raw('group_concat(sub_bidang.parameter SEPARATOR \'|\') as parameter'),
                        DB::raw('(CASE WHEN tb_registrasi.siklus != 12 THEN SUM(sub_bidang.tarif / 2) ELSE SUM(sub_bidang.tarif) END)as jumlah_tarif'),
                    'pembayaran.type'
                    )
                ->leftJoin('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->leftJoin('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->leftJoin('pembayaran', 'tb_registrasi.id_pembayaran', '=', 'pembayaran.id')
                ->leftJoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftJoin('users', 'perusahaan.created_by', '=', 'users.id')
                ->where('tb_registrasi.status', '1')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $year)
                ->groupBy('perusahaan.id')
                ->get();

        $bidang = explode("|",$data[0]->bidang);
        $parameter = explode("|",$data[0]->parameter);
        $d = [];
        foreach($bidang as $key => $val){
            $d[] = (object)[
                "bidang" => $val,
                "parameter" => $parameter[$key]
            ]; 
        }
        return view('cek_transfer', compact('data','d'));
    }

    public function pks()
    {
        $pks = DB::table('tb_pks')->get();
        // dd($data);
        return view('pks.datapks', compact('pks'));
    }

    public function cek_kwitansi(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('tahun');
        $data = DB::table('sub_bidang')
                ->select('tb_registrasi.id as idregister', 'tb_registrasi.updated_at' ,'users.name', 'users.email', 'perusahaan.nama_lab','perusahaan.id', 'tb_registrasi.siklus', 'tb_registrasi.no_urut', 'tb_registrasi.file','tb_registrasi.created_at',
                        DB::raw('group_concat(concat(sub_bidang.tarif) SEPARATOR \'|\') as total'),
                        DB::raw('group_concat(tb_bidang.bidang SEPARATOR \'|\') as bidang'),
                        DB::raw('group_concat(sub_bidang.parameter SEPARATOR \'|\') as parameter'),
                        DB::raw('sum(case 
                                when tb_registrasi.siklus != 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif / 2
                                when tb_registrasi.siklus != 12 && (sub_bidang.siklus = 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif
                                when tb_registrasi.siklus = 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif
                                when tb_registrasi.siklus = 12 && (sub_bidang.siklus = 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif 
                            end) as jumlah_tarif')
                    )
                ->leftJoin('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->leftJoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftJoin('users', 'perusahaan.created_by', '=', 'users.id')
                ->where('tb_registrasi.status','>=', '2')
                ->where('tb_registrasi.id_pembayaran','=', '1')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->groupBy('perusahaan.id')
                ->groupBy('tb_registrasi.siklus')
                ->groupBy(DB::raw('YEAR(tb_registrasi.created_at)'))
                ->orderBy('tb_registrasi.id', 'desc')
                ->get();
        // dd($data);
        foreach ($data as $key => $val) {
            $tahun = date('Y', strtotime($val->created_at));
            $tanggal = DB::table('tb_kwitansi')
                    ->where('perusahaan_id', $val->id)
                    ->where('tahun', $tahun)
                    ->select('tanggal')
                    ->first();
            $val->tanggal = $tanggal;
        }
        $judul = "Transfer";
        // dd($data);
        return view('cek_kwitansi', compact('data', 'judul', 'tahun'));
    }

    public function cek_kwitansipks(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('tahun');
        $data = DB::table('sub_bidang')
                ->select('tb_registrasi.id as idregister', 'tb_registrasi.updated_at' ,'users.name', 'users.email', 'perusahaan.nama_lab','perusahaan.id', 'tb_registrasi.siklus', 'tb_registrasi.no_urut', 'tb_registrasi.file','tb_registrasi.created_at',
                        DB::raw('group_concat(concat(sub_bidang.tarif) SEPARATOR \'|\') as total'),
                        DB::raw('group_concat(tb_bidang.bidang SEPARATOR \'|\') as bidang'),
                        DB::raw('group_concat(sub_bidang.parameter SEPARATOR \'|\') as parameter'),
                        DB::raw('sum(case 
                                when tb_registrasi.siklus != 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif / 2
                                when tb_registrasi.siklus != 12 && (sub_bidang.siklus = 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif
                                when tb_registrasi.siklus = 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif
                                when tb_registrasi.siklus = 12 && (sub_bidang.siklus = 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif 
                            end) as jumlah_tarif')
                    )
                ->leftJoin('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->leftJoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftJoin('users', 'perusahaan.created_by', '=', 'users.id')
                ->where('tb_registrasi.status','>=', '2')
                ->where('tb_registrasi.id_pembayaran','=', '2')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->groupBy('perusahaan.id')
                ->groupBy('tb_registrasi.siklus')
                ->groupBy(DB::raw('YEAR(tb_registrasi.created_at)'))
                ->orderBy('tb_registrasi.id', 'desc')
                ->get();
        foreach ($data as $key => $val) {
            $tahun = date('Y', strtotime($val->created_at));
            $tanggal = DB::table('tb_kwitansi')
                    ->where('perusahaan_id', $val->id)
                    ->where('tahun', $tahun)
                    ->select('tanggal')
                    ->first();
            $val->tanggal = $tanggal;
        }
        $judul = "Perjanjian Kerjasama";
        // dd($data);
        return view('cek_kwitansi', compact('data', 'judul', 'tahun'));
    }

    public function cetak_kwitansi(\Illuminate\Http\Request $request, $id)
    {
        $tahun = $request->get('tahun');
        $siklus = $request->get('siklus');

        $vtanggal = DB::table('tb_kwitansi')
                    ->where('perusahaan_id', $id)
                    ->where('tahun', $tahun)
                    ->select('tanggal')
                    ->first();
        if(count($vtanggal)){
            $data['tanggal'] = $request->get('tanggal');
            Kwitansi::where('perusahaan_id', $id)->where('tahun', $tahun)->update($data);
        }else{
            $data['perusahaan_id'] = $id;
            $data['tahun'] = $tahun;
            $data['tanggal'] = $request->get('tanggal');
            Kwitansi::create($data);
        }
        $tanggal = DB::table('tb_kwitansi')
                    ->where('perusahaan_id', $id)
                    ->where('tahun', $tahun)
                    ->select('tanggal')
                    ->first();
        // dd($tanggal);

        $data = DB::table('sub_bidang')
                ->select('users.name', 'users.email', 'perusahaan.nama_lab', 'tb_registrasi.siklus', 'tb_registrasi.kode_lebpes', 'tb_registrasi.file','tb_registrasi.no_urut','tb_registrasi.created_at',
                    DB::raw('group_concat(concat(case 
                            when tb_registrasi.siklus != 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif / 2
                            when tb_registrasi.siklus != 12 && (sub_bidang.siklus = 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif
                            when tb_registrasi.siklus = 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif
                            when tb_registrasi.siklus = 12 && (sub_bidang.siklus = 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif 
                        end) SEPARATOR \'|\') as total'),
                    DB::raw('group_concat(tb_registrasi.siklus SEPARATOR \'|\') as siklus'),
                    DB::raw('group_concat(tb_bidang.bidang SEPARATOR \'|\') as bidang'),
                    DB::raw('group_concat(sub_bidang.parameter SEPARATOR \'|\') as parameter'),
                    DB::raw('sum(case 
                            when tb_registrasi.siklus != 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif / 2
                            when tb_registrasi.siklus != 12 && (sub_bidang.siklus = 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif
                            when tb_registrasi.siklus = 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif
                            when tb_registrasi.siklus = 12 && (sub_bidang.siklus = 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif 
                        end) as jumlah_tarif')
                )  
                ->leftJoin('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->leftJoin('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->leftJoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftJoin('users', 'perusahaan.created_by', '=', 'users.id')
                ->where('tb_registrasi.status','>=', '2')
                ->where('perusahaan.id', $id)
                ->where('tb_registrasi.siklus', $siklus)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->groupBy('perusahaan.id')
                ->first();
        // dd($data);
        $datas = explode("|",$data->parameter);
        $prices = explode("|",$data->total);
        $siklus = explode("|",$data->siklus);
        $d = [];
        foreach($datas as $key => $val){
            $d[] = [
                $val,
                $prices[$key],
                $siklus[$key]
            ]; 
        }
        // dd($d);
        // return view('cetak_hasil/cetak_kwitansi', compact('data','d'));
        $pdf = PDF::loadview('cetak_hasil/cetak_kwitansi', compact('data','tahun','tanggal','d'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        return $pdf->stream('Cetak Kwitansi.pdf');
    }

    public function kuitansi_pks(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $date = $input['tahun'];
        $pks = $input['pks'];
        $total = DB::table('sub_bidang')
                ->select('users.name', 'users.email', 'perusahaan.nama_lab', 'tb_registrasi.siklus', 'tb_registrasi.kode_lebpes', 'tb_bayar.created_at', 'tb_registrasi.file',
                    DB::raw('group_concat(concat(sub_bidang.tarif) SEPARATOR \'|\') as total'),
                    DB::raw('group_concat(tb_bidang.bidang SEPARATOR \'|\') as bidang'),
                    DB::raw('group_concat(sub_bidang.parameter SEPARATOR \'|\') as parameter'),
                    DB::raw('(CASE WHEN tb_registrasi.siklus != 12 THEN SUM(sub_bidang.tarif / 2) ELSE SUM(sub_bidang.tarif) END)as jumlah_tarif')
                )  
                ->leftJoin('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->leftJoin('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->leftJoin('tb_bayar', 'tb_registrasi.id', '=', 'tb_bayar.id_registrasi')
                ->leftJoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftJoin('users', 'perusahaan.created_by', '=', 'users.id')
                ->where('tb_registrasi.sptjm', '!=', NULL)
                ->where('tb_registrasi.pks','=',$pks)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->first();

        $data = DB::table('sub_bidang')
                ->select('users.name', 'users.email', 'perusahaan.nama_lab', 'tb_registrasi.siklus', 'tb_registrasi.kode_lebpes', 'tb_bayar.created_at', 'tb_registrasi.file',
                    DB::raw('group_concat(concat(sub_bidang.tarif) SEPARATOR \'|\') as total'),
                    DB::raw('group_concat(tb_bidang.bidang SEPARATOR \'|\') as bidang'),
                    DB::raw('group_concat(sub_bidang.parameter SEPARATOR \'|\') as parameter'),
                    DB::raw('(CASE WHEN tb_registrasi.siklus != 12 THEN SUM(sub_bidang.tarif / 2) ELSE SUM(sub_bidang.tarif) END)as jumlah_tarif')
                )  
                ->leftJoin('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->leftJoin('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->leftJoin('tb_bayar', 'tb_registrasi.id', '=', 'tb_bayar.id_registrasi')
                ->leftJoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftJoin('users', 'perusahaan.created_by', '=', 'users.id')
                ->where('tb_registrasi.sptjm', '!=', NULL)
                ->where('tb_registrasi.pks','=', $pks)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->groupBy('perusahaan.id')
                ->get();
        // dd($data);
        
        $pdf = PDF::loadview('cetak_hasil/kwitansi_pks', compact('data', 'total'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        return $pdf->stream('Cetak Kwitansi.pdf');
    }

    public function proses(\Illuminate\Http\Request $request){

        $bidang = $request->bidang;
        $jumlah_dipilih = count($bidang);
        $user = DB::table('users')->where('role', 5)->get();
        $userna = count($user);
        // dd($request->all());
        for($x=0;$x<$jumlah_dipilih;$x++){
            $datas = $request->all();
            $exna = explode('|', $bidang[$x]);
            // dd($exna);
            $ids = "|";
            for ($u=0; $u < count($exna) ; $u++) { 
                $idTarif = explode('-', $exna[$u]);
                $data['status'] = '2';
                $bayar['tarif'] = $idTarif[1];
                $bayar['id_registrasi'] = $idTarif[0];
                $bayar['created_by'] = Auth::user()->id;
                Register::where('id', $exna[$u])->update($data);
                Bayar::create($bayar);
                $ids .= ",".$idTarif[0];
            }
            $ids = str_replace("|,", "", $ids);
            $ids = str_replace("", "", $ids);
            // $idas = "'".$ids."'";
            $email = DB::table('users')
                    ->leftjoin('tb_registrasi', 'users.id', '=', 'tb_registrasi.created_by')
                    ->leftjoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                    ->leftjoin('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                    ->select('tb_registrasi.id','tb_registrasi.perusahaan_id','users.email', 'tb_registrasi.siklus', DB::raw('group_concat(sub_bidang.parameter SEPARATOR \'|\') as parameter'), 'perusahaan.nama_lab', DB::raw('group_concat(tb_registrasi.kode_lebpes SEPARATOR \'|\') as kode_lebpes'))
                    // ->whereIn(DB::raw('' ($ids)))
                    ->whereRaw("tb_registrasi.id in ($ids)")
                    ->groupBy('users.id')
                    ->get();
            // dd($email);
            $datu = [
                    'labklinik' => $email[0]->nama_lab,
                    'siklus' => $email[0]->siklus,
                    'kode_lebpes' => $email[0]->kode_lebpes,
                    'date' => date('Y'),
                    'parameter' => $email[0]->parameter
                ];
            // dd($email);
            Mail::send('email.cek_transfer', $datu, function ($mail) use ($email)
            {
              $mail->from('pme.bblksub@gmail.com', 'BBLK Surabaya Transfer '.$email[0]->nama_lab);
              $mail->to($email[0]->email);
              $mail->subject('Pembayaran PNPME');
            });

            $penyelenggara = DB::table('users')->where('role','=','6')->get();
            foreach ($penyelenggara as $key => $val) {
                Mail::send('email.penyelenggara', $datu, function ($mail) use ($email, $val)
                {
                  $mail->from('pme.bblksub@gmail.com', 'BBLK Surabaya '.$email[0]->nama_lab);
                  $mail->to($val->email);
                    // $mail->to('tengkufirmansyah2@gmail.com');
                  $mail->subject('Pembayaran PNPME');
                });
            }
        }
        // return redirect('cetak-kwitansi/'.$email[0]->perusahaan_id);
           
        return redirect('cek_transfer');
    }

    public function pembayaran()
    {
        $tahun = date('Y');
        $pks = DB::table('tb_pks')->get();
        $data = DB::table('sub_bidang')
                ->select('users.name', 'users.email', 'perusahaan.nama_lab','perusahaan.no_hp', 'tb_registrasi.siklus', 'tb_registrasi.file','tb_registrasi.created_at',
                        DB::raw('group_concat(concat(tb_registrasi.id,"-",sub_bidang.tarif) SEPARATOR \'|\') as id'),
                        DB::raw('group_concat(tb_bidang.bidang SEPARATOR \'|\') as bidang'),
                        DB::raw('group_concat(sub_bidang.parameter SEPARATOR \'|\') as parameter'),
                        DB::raw('(CASE WHEN tb_registrasi.siklus != 12 THEN SUM(sub_bidang.tarif / 2) ELSE SUM(sub_bidang.tarif) END)as jumlah_tarif')
                    )
                ->leftJoin('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->leftJoin('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->leftJoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftJoin('users', 'perusahaan.created_by', '=', 'users.id')
                ->where('tb_registrasi.sptjm', '!=', NULL)
                ->where('tb_registrasi.id_pembayaran', '=', '2')
                ->where(DB::raw('Year(tb_registrasi.created_at)'), $tahun)
                ->where('tb_registrasi.pks', '=', NULL)
                ->groupBy('perusahaan.id')
                ->groupBy(DB::raw('Year(tb_registrasi.created_at)'))
                ->orderBy('tb_registrasi.id','desc')
                ->get();

        return view('pks.pembayaran', compact('data','pks'));
    }

    public function prosesp(\Illuminate\Http\Request $request){

        $bidang = $request->bidang;
        $pks = $request->pks;
        $jumlah_dipilih = count($bidang);
        $user = DB::table('users')->where('role', 5)->get();
        $userna = count($user);
        // dd($request->all());
        for($x=0;$x<$jumlah_dipilih;$x++){
            $datas = $request->all();
            $exna = explode('|', $bidang[$x]);
            // dd($exna);
            $ids = "|";
            for ($u=0; $u < count($exna) ; $u++) { 
                $idTarif = explode('-', $exna[$u]);
                $data['pks'] = $pks;
                Register::where('id', $exna[$u])->update($data);
                $ids .= ",".$idTarif[0];
            }
        }

        return redirect()->back();
    }

    public function updatepembayaran()
    {
        $data = DB::table('sub_bidang')
                ->select('users.name', 'users.email', 'perusahaan.nama_lab', 'tb_registrasi.siklus', 'tb_registrasi.file', 'tb_registrasi.created_at',
                        DB::raw('group_concat(concat(tb_registrasi.id,"-",sub_bidang.tarif) SEPARATOR \'|\') as id'),
                        DB::raw('group_concat(tb_bidang.bidang SEPARATOR \'|\') as bidang'),
                        DB::raw('group_concat(sub_bidang.parameter SEPARATOR \'|\') as parameter'),
                        DB::raw('(CASE WHEN tb_registrasi.siklus != 12 THEN SUM(sub_bidang.tarif / 2) ELSE SUM(sub_bidang.tarif) END)as jumlah_tarif')
                    )
                ->leftJoin('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->Join('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->leftJoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftJoin('users', 'perusahaan.created_by', '=', 'users.id')
                ->where('tb_registrasi.sptjm', '!=', NULL)
                ->where('tb_registrasi.id_pembayaran', '=', '2')
                ->groupBy('perusahaan.id')
                ->groupBy(DB::raw('Year(tb_registrasi.created_at)'))
                ->orderBy('tb_registrasi.id','desc')
                ->get();
        return view('update_pembayaran', compact('data'));
    }

    public function updateprosesp(\Illuminate\Http\Request $request){

        $bidang = $request->bidang;
        $jumlah_dipilih = count($bidang);
        $user = DB::table('users')->where('role', 5)->get();
        $userna = count($user);

        $tahun = date('Y');
        $register = DB::table('tb_registrasi')
                    ->select(DB::raw('CAST(no_urut AS UNSIGNED) as no_uruts'))
                    ->join('users', 'users.id','=','tb_registrasi.created_by')
                    ->where(DB::raw('Year(tb_registrasi.created_at)'), $tahun)
                    ->whereNotNull('no_urut')
                    ->groupBy('no_uruts')
                    ->orderBy('no_uruts', 'desc')
                    ->first();
        if(count($register)){
            $cekno = (int)$register->no_uruts + 1;
            $no_urut = sprintf("%04s", $cekno);
        }else{
            $no_urut = '0001';
        }
        // dd($request->all());
        for($x=0;$x<$jumlah_dipilih;$x++){
            $datas = $request->all();
            $exna = explode('|', $bidang[$x]);
            // dd($exna);
            $ids = "|";
            for ($u=0; $u < count($exna) ; $u++) {
                $data['status'] = '1';
                $data['id_pembayaran'] = '1';
                $data['no_urut'] = $no_urut;
                $data['sptjm'] = NULL;
                Register::where('id', $exna[$u])->update($data);
            }
        }
        return redirect()->back();
    }

    public function generateva(){
        return view('va.generate_va');
    }

    public function generatevana(\Illuminate\Http\Request $request){
        $tahun = $request->tahun;
        $data = DB::table('users')
                ->join('tb_registrasi', 'tb_registrasi.created_by', '=', 'users.id')
                ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                ->where('tb_registrasi.status','=','1')
                ->where('id_pembayaran','=','1')
                ->where(DB::raw('Year(tb_registrasi.created_at)'), '=', $tahun)
                ->groupBy('tb_registrasi.created_by')
                ->select(DB::raw('(CASE WHEN tb_registrasi.siklus != 12 THEN SUM(sub_bidang.tarif / 2) ELSE SUM(sub_bidang.tarif) END)as total'), 'users.name', 'users.email','tb_registrasi.no_urut')
                ->get();
        // dd($data);
        // return view('va.proses', compact('data'));
         Excel::create('Generate Virtual Account Simultan Tahun '.$tahun, function($excel) use ($data, $tahun) {
             $excel->sheet('Wilayah', function($sheet) use ($data, $tahun) {
                 $sheet->loadView('va.proses', array('data'=>$data,'tahun'=>$tahun) );
             });
         })->download('xls');
    }

    public function pesertava()
    {
        $tahun = date("Y");
        $data = DB::table('users')
                ->join('perusahaan','perusahaan.created_by','=','users.id')
                ->join('tb_registrasi', 'tb_registrasi.created_by', '=', 'users.id')
                ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                ->where('tb_registrasi.status','=','1')
                ->where('id_pembayaran','=','1')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'),'=', $tahun)
                ->groupBy('tb_registrasi.created_by')
                ->groupBy('tb_registrasi.no_urut')
                ->select('users.id','perusahaan.no_hp','perusahaan.no_wa','tb_registrasi.siklus',DB::raw('(CASE WHEN tb_registrasi.siklus != 12 THEN SUM(sub_bidang.tarif / 2) ELSE SUM(sub_bidang.tarif) END)as total'), 'users.name', 'users.email','users.id_member','tb_registrasi.created_at','tb_registrasi.no_urut',DB::raw('DATE_ADD(tb_registrasi.created_at,INTERVAL 3 HOUR) as coutdown'), DB::raw('DATE_ADD(tb_registrasi.created_at,INTERVAL 1 HOUR) as coutdown1jam'))
                ->orderBy('tb_registrasi.id', 'desc')
                ->get();
        foreach ($data as $key => $val) {
            $emailva = DB::table('tb_email_va')
                        ->where('user_id','=', $val->id)
                        ->where('tahun','=', $tahun)
                        ->where('siklus','=', $val->siklus)
                        ->get();
            $val->emailva = $emailva;
        }
        // dd($data);
        return view('va.peserta_va', compact('data'));
    }

    public function pesertavana(\Illuminate\Http\Request $request){
        $data = $request->all();
        $tahun = date("Y");
        foreach ($data['peserta'] as $key => $val) {
            $satuan = DB::table('tb_registrasi')
                        ->join('users','users.id','=','tb_registrasi.created_by')
                        ->where('tb_registrasi.status','=','1')
                        ->where('id_pembayaran','=','1')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'),'=', $tahun)
                        ->where('tb_registrasi.created_by','=', $val)
                        ->first();
            // dd($satuan);
            $datu = [
                'no_urut' => $satuan->no_urut
            ];
            Mail::send('email.transfer',$datu, function ($mail) use ($data, $satuan)
            {
                  $mail->from('pme.bblksub@gmail.com', 'BBLK Surabaya Daftar Peserta ');
                  $mail->to($satuan->email);
                  $mail->subject('Kode Virtual Account ');
            });

            $SaveEmail = new EmailVa;
            $SaveEmail->user_id = $satuan->created_by;
            $SaveEmail->tahun = $tahun;
            $SaveEmail->siklus = $satuan->siklus;
            $SaveEmail->save();
            $i++;
        }
        return back();
    }

    public function deletva(\Illuminate\Http\Request $request, $id)
    {
        $tahun = date("Y");
        $data = DB::table('users')
                ->join('tb_registrasi', 'tb_registrasi.created_by', '=', 'users.id')
                ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                ->where('tb_registrasi.status','=','1')
                ->where('id_pembayaran','=','1')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'),'=', $tahun)
                ->where('users.id', '=', $id)
                ->groupBy('tb_registrasi.created_by')
                ->groupBy('tb_registrasi.no_urut')
                ->select('users.id',DB::raw('(CASE WHEN tb_registrasi.siklus != 12 THEN SUM(sub_bidang.tarif / 2) ELSE SUM(sub_bidang.tarif) END)as total'), 'users.name', 'users.email','users.id_member','tb_registrasi.created_at','tb_registrasi.no_urut',DB::raw('DATE_ADD(tb_registrasi.created_at,INTERVAL 3 HOUR) as coutdown'), DB::raw('DATE_ADD(tb_registrasi.created_at,INTERVAL 1 HOUR) as coutdown1jam'))
                ->get();
        $email = DB::table('tb_email_va')
                    ->where('user_id','=', $id)
                    ->where('tahun','=', $tahun)
                    ->delete();
        $waktu = date("Y-m-d H:i:s");
        foreach ($data as $key => $val) {
            $satuan = DB::table('tb_registrasi')
                        ->where('tb_registrasi.status','=','1')
                        ->where('id_pembayaran','=','1')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'),'=', $tahun)
                        ->where('tb_registrasi.created_by','=', $val->id)
                        ->delete();
        }
        return back();
    }
}

<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;
use App\Hide;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\BlokEvaluasi;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class BlokEvaluasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $user = DB::table('blok_evaluasi')
                ->join('users','users.id','=','blok_evaluasi.user_id')
                ->join('perusahaan','perusahaan.created_by','=','users.id')
                ->select('blok_evaluasi.id','users.email','perusahaan.nama_lab','blok_evaluasi.siklus','blok_evaluasi.tahun')
                ->get();
        return view('back/blokevaluasi/index', compact('user'));
    }

    public function edit($id)
    {
        $data = DB::table('blok_evaluasi')
                ->join('users','users.id','=','blok_evaluasi.user_id')
                ->join('perusahaan','perusahaan.created_by','=','users.id')
                ->select('users.id','users.email','perusahaan.nama_lab','blok_evaluasi.siklus','blok_evaluasi.tahun')
                ->where('blok_evaluasi.id', $id)
                ->first();
        $user = DB::table('users')
                ->join('perusahaan','perusahaan.created_by','=','users.id')
                ->select('users.id','users.email','perusahaan.nama_lab')
                ->get();
        return view('back/blokevaluasi/update', compact('data','user'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $input = Request::all();
        // dd($input);
        $data['user_id'] = $input['user_id'];
        $data['siklus'] = $input['siklus'];
        $data['tahun'] = $input['tahun'];
        $data['created_by'] = Auth::user()->id;
        BlokEvaluasi::where('id',$id)->update($data);
        Session::flash('message', 'Peserta Telah di Update ke data blok list Evaluasi!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/blok-evaluasi-peserta');
    }    

    public function insert()
    {
        $user = DB::table('users')
                ->join('perusahaan','perusahaan.created_by','=','users.id')
                ->select('users.id','users.email','perusahaan.nama_lab')
                ->get();
        return view('back/blokevaluasi/insert', compact('user'));
    }
    public function insertna(\Illuminate\Http\Request $request)
    {
        $input = Request::all();
        // dd($input);
        $data['user_id'] = $input['user_id'];
        $data['siklus'] = $input['siklus'];
        $data['tahun'] = $input['tahun'];
        $data['created_by'] = Auth::user()->id;
        BlokEvaluasi::create($data);
        Session::flash('message', 'Peserta Telah di Tambahkan ke data blok list Evaluasi!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/blok-evaluasi-peserta');
    }

    public function delete($id){
        Session::flash('message', 'Peserta Telah di Hapus ke data blok list Evaluasi!'); 
        Session::flash('alert-class', 'alert-warning'); 
        BlokEvaluasi::find($id)->delete();
        return redirect("admin/blok-evaluasi-peserta");
    }

}

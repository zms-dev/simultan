<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Rujukanbta;
use App\Rujukanmalaria;
use App\Rujukanhem;
use App\Rujukantc;
use App\Rujukanurinalisa;
use App\Rujukanimunologi;
use Illuminate\Support\Facades\Redirect;

class RujukanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function indexbta()
    {
        $rujukan = DB::table('tb_rujukan_bta')->get();
        return view('back/rujukan/bta/index', compact('rujukan'));
    }
    public function editbta($id)
    {
        $data = DB::table('tb_rujukan_bta')->where('id', $id)->get();
        return view('back/rujukan/bta/update', compact('data'));
    }

    public function updatebta(\Illuminate\Http\Request $request, $id)
    {
        $data['kode_bahan_uji'] = $request->kode_bahan_uji;
        $data['rujukan'] = $request->rujukan;
        $data['siklus'] = $request->siklus;
        $data['tahun'] = $request->tahun;
        Rujukanbta::where('id',$id)->update($data);
        Session::flash('message', 'Master Rujukan BTA Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/rujukan-bta');
    }    

    public function insertbta(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        Rujukanbta::create($data);
        // dd($data);
        Session::flash('message', 'Master Rujukan BTA Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/rujukan-bta');
    }

    public function deletebta($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Rujukanbta::find($id)->delete();
        return redirect("admin/rujukan-bta");
    }


    public function indexmalaria()
    {
        $rujukan = DB::table('tb_rujukan_malaria')->orderBy('id','desc')->get();
        return view('back/rujukan/malaria/index', compact('rujukan'));
    }
    public function editmalaria($id)
    {
        $data = DB::table('tb_rujukan_malaria')->where('id', $id)->get();
        
        return view('back/rujukan/malaria/update', compact('data'));
    }

    public function updatemalaria(\Illuminate\Http\Request $request, $id)
    {
                
        $stadium="";
        if (!empty($request['stadium'])) {
            $stadium = implode(",",$request['stadium']);     
        }
        // dd($stadium);

        $data['kode_bahan_uji'] = $request->kode_bahan_uji;
        $data['rujukan'] = $request->rujukan;
        $data['spesies'] = $request->spesies;
        $data['stadium'] = $stadium;
        $data['parasit'] = $request->parasit;
        $data['siklus'] = $request->siklus;
        $data['tahun'] = $request->tahun;
        Rujukanmalaria::where('id',$id)->update($data);
        Session::flash('message', 'Master Rujukan Malaria Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/rujukan-malaria');
    }    

    public function insertmalaria(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        // dd($data);
        $stadium = "";

        if (!empty($request['stadium'])) {
            $stadium = implode(",",$request['stadium']);     
        }
        // dd($stadium);

        $save = new Rujukanmalaria;
        $save->kode_bahan_uji = $data['kode_bahan_uji'];
        $save->rujukan = $data['rujukan'];
        $save->spesies = $data['spesies'];
        $save->stadium = $stadium;
        $save->parasit = $data['parasit'];
        $save->siklus = $data['siklus'];
        $save->tahun = $data['tahun'];
        $save->save();

        // dd($data);
        Session::flash('message', 'Master Rujukan Malaria Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/rujukan-malaria');
    }

    public function deletemalaria($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Rujukanmalaria::find($id)->delete();
        return redirect("admin/rujukan-malaria");
    }


    public function indexhematologi()
    {
        $rujukan = DB::table('tb_rujukan_hematologi')->where('form', '=', 'hem')->get();
        return view('back/rujukan/hematologi/index', compact('rujukan'));
    }
    public function edithematologi($id)
    {
        $data = DB::table('tb_rujukan_hematologi')->where('id', $id)->get();
        return view('back/rujukan/hematologi/update', compact('data'));
    }

    public function updatehematologi(\Illuminate\Http\Request $request, $id)
    {
        $data['kode'] = $request->kode;
        $data['nilai'] = $request->nilai;
        $data['kriteria'] = $request->kriteria;
        $data['siklus'] = $request->siklus;
        $data['tahun'] = $request->tahun;
        Rujukanhem::where('id',$id)->update($data);
        Session::flash('message', 'Master Rujukan Hematologi Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/rujukan-hematologi');
    }    

    public function inserthematologi(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        Rujukanhem::create($data);
        // dd($data);
        Session::flash('message', 'Master Rujukan Hematologi Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/rujukan-hematologi');
    }

    public function deletehematologi($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Rujukanhem::find($id)->delete();
        return redirect("admin/rujukan-hematologi");
    }


    public function indexkimiaklinik()
    {
        $rujukan = DB::table('tb_rujukan_hematologi')->where('form', '=', 'kkl')->get();
        return view('back/rujukan/kimiaklinik/index', compact('rujukan'));
    }
    public function editkimiaklinik($id)
    {
        $data = DB::table('tb_rujukan_hematologi')->where('id', $id)->get();
        return view('back/rujukan/kimiaklinik/update', compact('data'));
    }

    public function updatekimiaklinik(\Illuminate\Http\Request $request, $id)
    {
        $data['kode'] = $request->kode;
        $data['nilai'] = $request->nilai;
        $data['kriteria'] = $request->kriteria;
        $data['siklus'] = $request->siklus;
        $data['tahun'] = $request->tahun;
        Rujukanhem::where('id',$id)->update($data);
        Session::flash('message', 'Master Rujukan Kimiaklinik Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/rujukan-kimiaklinik');
    }    

    public function insertkimiaklinik(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        Rujukanhem::create($data);
        // dd($data);
        Session::flash('message', 'Master Rujukan Kimiaklinik Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/rujukan-kimiaklinik');
    }

    public function deletekimiaklinik($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Rujukanhem::find($id)->delete();
        return redirect("admin/rujukan-kimiaklinik");
    }


    public function indexkimiakesehatan()
    {
        $rujukan = DB::table('tb_rujukan_hematologi')->where('form', '=', 'kimkes')->get();
        return view('back/rujukan/kimiakesehatan/index', compact('rujukan'));
    }
    public function editkimiakesehatan($id)
    {
        $data = DB::table('tb_rujukan_hematologi')->where('id', $id)->get();
        return view('back/rujukan/kimiakesehatan/update', compact('data'));
    }

    public function updatekimiakesehatan(\Illuminate\Http\Request $request, $id)
    {
        $data['kode'] = $request->kode;
        $data['nilai'] = $request->nilai;
        $data['hasil'] = $request->hasil;
        $data['kriteria'] = $request->kriteria;
        $data['siklus'] = $request->siklus;
        $data['tahun'] = $request->tahun;
        Rujukanhem::where('id',$id)->update($data);
        Session::flash('message', 'Master Rujukan Kimiakesehatan Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/rujukan-kimiakesehatan');
    }    

    public function insertkimiakesehatan(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        Rujukanhem::create($data);
        // dd($data);
        Session::flash('message', 'Master Rujukan Kimiakesehatan Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/rujukan-kimiakesehatan');
    }

    public function deletekimiakesehatan($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Rujukanhem::find($id)->delete();
        return redirect("admin/rujukan-kimiakesehatan");
    }


    public function indextc()
    {
        $rujukan = DB::table('tb_rujukan_tc')->where('form', '=', 'tc')->get();
        return view('back/rujukan/tc/index', compact('rujukan'));
    }
    public function edittc($id)
    {
        $data = DB::table('tb_rujukan_tc')->where('id', $id)->get();
        return view('back/rujukan/tc/update', compact('data'));
    }

    public function updatetc(\Illuminate\Http\Request $request, $id)
    {
        $data['kode_sediaan'] = $request->kode_sediaan;
        $data['rujukan'] = $request->rujukan;
        $data['siklus'] = $request->siklus;
        $data['tahun'] = $request->tahun;
        Rujukantc::where('id',$id)->update($data);
        Session::flash('message', 'Master Rujukan TC Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/rujukan-tc');
    }    

    public function inserttc(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        Rujukantc::create($data);
        // dd($data);
        Session::flash('message', 'Master Rujukan TC Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/rujukan-tc');
    }

    public function deletetc($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Rujukantc::find($id)->delete();
        return redirect("admin/rujukan-tc");
    }


    public function indeximunologi()
    {
        $hiv = 'Imunologi Anti HIV';
        $sifilis = 'Imunologi Syphilis';
        $hbsag = 'Imunologi HbsAg';
        $hcv = 'Imunologi Anti HCV';        
        $pmi = 'Anti HIV PMI';
        $rujukan = DB::table('tb_rujukan_imunologi')->groupBy('parameter','siklus','tahun')->get();
        if (Auth::user()->name == $sifilis) {
            $rujukansipilis = DB::table('tb_rujukan_imunologi')->where('parameter','=','Anti TP')->orWhere('parameter','=','RPR')->groupBy('parameter','siklus','tahun')->get();
        }elseif(Auth::user()->name == $hiv){
            $rujukanhiv = DB::table('tb_rujukan_imunologi')->where('parameter','=','Anti HIV')->groupBy('parameter','siklus','tahun')->get();
        }elseif (Auth::user()->name == $hbsag) {
            $rujukanhbsag = DB::table('tb_rujukan_imunologi')->where('parameter','=','HBsAg')->groupBy('parameter','siklus','tahun')->get();
        }elseif (Auth::user()->name == $hcv) {
            $rujukanhcv = DB::table('tb_rujukan_imunologi')->where('parameter','=','Anti HCV')->groupBy('parameter','siklus','tahun')->get();
        }elseif (Auth::user()->name == $pmi) {
            $rujukanpmi = DB::table('tb_rujukan_imunologi')->where('parameter','=','HIV PMI')->groupBy('parameter','siklus','tahun')->get();
        }
        // dd($rujukan);
        return view('back/rujukan/imunologi/index', compact('rujukan','hiv','sifilis','hbsag','hcv','rujukansipilis','rujukanhiv','rujukanhbsag','rujukanhcv','rujukanpmi','pmi'));
    }
    public function editimunologi(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');  
        $data = DB::table('tb_rujukan_imunologi')
            ->where('parameter','=', $id)
            ->where('siklus','=', $siklus)
            ->where('tahun','=', $tahun)
            ->orderBy('id', 'asc')
            // ->take(10)
            ->get();
            // dd($data);
        return view('back/rujukan/imunologi/update', compact('data'));
    }

    public function updateimunologi(\Illuminate\Http\Request $request)
    {
        $data       = request::all();
        $parameter  = $request->parameter;
        $siklus     = $request->get('siklus');
        $tahun      = $request->get('tahun');
// dd($data);
        Rujukanimunologi::where('parameter','=', $parameter)
                        ->where('siklus','=', $siklus)
                        ->where('tahun','=', $tahun)
                        ->delete();

        foreach ($data['pertama'] as $key => $val) {
            $alias = substr($val, 0,2);
            $pertama = substr($val, 2);
            $terakhir = substr($data['terakhir'][$key], 2);
            $coba = 0;
            for ($i=$pertama; $i <= $terakhir; $i++) {
                $save                   = new Rujukanimunologi;
                $save->parameter        = $parameter;
                $save->kode_bahan_uji   = $alias.sprintf("%03s", $i);
                $save->nilai_rujukan    = $data['nilai_rujukan'][$key];
                $save->siklus           = $siklus;
                $save->tahun            = $tahun;
                $save->range1           = $data['range1'][$key];
                $save->range2           = $data['range2'][$key];
                $save->group            = $key+1;
                $save->save();
            }
        }

        Session::flash('message', 'Master Rujukan Imunologi Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/rujukan-imunologi');
    }    

    public function insertimunologi(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        foreach ($data['pertama'] as $key => $val) {
            $alias = substr($val, 0,2);
            $pertama = substr($val, 2);
            $terakhir = substr($data['terakhir'][$key], 2);
            $coba = 0;
            for ($i=$pertama; $i <= $terakhir; $i++) {
                $save = new Rujukanimunologi;
                $save->parameter = $data['parameter'];
                $save->kode_bahan_uji = $alias.sprintf("%03s", $i);
                $save->nilai_rujukan = $data['nilai_rujukan'][$key];
                $save->siklus = $data['siklus'];
                $save->tahun = $data['tahun'];
                $save->range1 = $data['range1'][$key];
                $save->range2 = $data['range2'][$key];
                $save->group = $key+1;
                $save->save();
            }
        }
        Session::flash('message', 'Master Rujukan Imunologi Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/rujukan-imunologi');
    }

    public function deleteimunologi(\Illuminate\Http\Request $request,$id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        Rujukanimunologi::where('parameter','=', $id)
            ->where('siklus','=', $siklus)
            ->where('tahun','=', $tahun)->delete();
        return redirect("admin/rujukan-imunologi");
    }


    public function indexurinalisa()
    {
        $rujukan = DB::table('tb_rujukan_urinalisa')
                    ->join('parameter','tb_rujukan_urinalisa.parameter','=','parameter.id')
                    ->select('tb_rujukan_urinalisa.*','parameter.nama_parameter')
                    ->groupBy('siklus', 'tahun', 'type', 'periode')
                    ->orderBy('tb_rujukan_urinalisa.id', 'desc')
                    ->paginate(5);
        return view('back/rujukan/urinalisa/index', compact('rujukan'));
    }
    
    public function inserturinalisa()
    {
        $parameter = DB::table('parameter')
                    ->where('kategori','=','urinalisa')
                    ->where('id','!=','9')
                    ->where('id','!=','10')
                    ->get();
        return view('back/rujukan/urinalisa/insert', compact('parameter'));
    }

    public function inserturinalisa2()
    {
        $parameter = DB::table('parameter')
                    ->where('kategori','=','urinalisa')
                    ->get();
        return view('back/rujukan/urinalisa/bj-ph', compact('parameter'));
    }

    public function editurinalisa($id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','urinalisa')->get();
        $rujukan = DB::table('tb_rujukan_urinalisa')
                    ->join('parameter','tb_rujukan_urinalisa.parameter','=','parameter.id')
                    ->where('tb_rujukan_urinalisa.id', $id)
                    ->select('tb_rujukan_urinalisa.*','parameter.nama_parameter')
                    ->first(); 
        $data = DB::table('tb_rujukan_urinalisa')
                ->join('parameter','tb_rujukan_urinalisa.parameter','=','parameter.id')
                ->where('tb_rujukan_urinalisa.siklus', $rujukan->siklus)
                ->where('tb_rujukan_urinalisa.tahun', $rujukan->tahun)
                ->where('tb_rujukan_urinalisa.type', $rujukan->type)
                ->where('tb_rujukan_urinalisa.periode', $rujukan->periode)
                ->select('tb_rujukan_urinalisa.*','parameter.nama_parameter')
                ->get();
        // dd($data);
        return view('back/rujukan/urinalisa/update', compact('data','rujukan','parameter'));
    }

    public function updateurinalisa(\Illuminate\Http\Request $request)
    {
        // dd($request->all());
        $i = 0;
        foreach ($request->parameter as $parameter) {
            $SaveDetail['parameter'] = $request->parameter[$i];
            $SaveDetail['rujukan'] = $request->rujukan[$i];
            $SaveDetail['siklus'] = $request->siklus;
            $SaveDetail['type'] = $request->type;
            $SaveDetail['tahun'] = $request->tahun;
            $SaveDetail['bahan'] = $request->bahan;
            $SaveDetail['periode'] = $request->periode;
            Rujukanurinalisa::where('id', $request->id[$i])->update($SaveDetail);
            $i++;
        }
        // $data['parameter'] = $request->parameter;
        // $data['siklus'] = $request->siklus;
        // $data['tahun'] = $request->tahun;
        // $data['rujukan'] = $request->rujukan;
        // $data['bahan'] = $request->bahan;

        // Rujukanurinalisa::where('id',$id)->update($data);
        Session::flash('message', 'Master Rujukan Urinalisa Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/rujukan-urinalisa');
    }  

    public function inserturinalisana2(\Illuminate\Http\Request $request)
    {
        $data = Request::all();

        $urin = DB::table('tb_rujukan_urinalisa')
                ->where('parameter', $data['parameter'])
                ->where('siklus', $data['siklus'])
                ->where('type', $data['type'])
                ->where('tahun', $data['tahun'])
                ->where(function($query) use ($data){
                    if ($data['periode'] == 2) {
                        $query->where('periode','=','2');
                    }
                })
                ->count();
        // dd($urin);
        if ($urin > 0 ) {
            Session::flash('message', 'Master Rujukan Urinalisa Gagal Ditambahkan!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('admin/rujukan-urinalisa');
        }else{
            $ngitung = DB::table('hp_details')
                        ->join('hp_headers','hp_details.hp_header_id','=','hp_headers.id')
                        ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                        ->where(function($query) use ($data){
                            if ($data['siklus'] == 1) {
                                if ($data['periode'] == 2) {
                                    $query->where('tb_registrasi.periode','=','2');
                                }else{
                                    $query->WhereNull('tb_registrasi.periode');
                                }
                            }
                        })
                        ->where(function($query) use ($data){
                            $query->where('tb_registrasi.siklus', '12')
                                ->orwhere('tb_registrasi.siklus', $data['siklus']);
                        })
                        ->where('hp_headers.siklus', '=', $data['siklus'])
                        ->where('hp_headers.type', '=', $data['type'])
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $data['tahun'])
                        ->where('hp_details.parameter_id','=',$data['parameter'])
                        ->avg('hp_details.hasil_pemeriksaan');
            $data['rujukan'] = $ngitung;
            // dd($data);
            Rujukanurinalisa::create($data);
            Session::flash('message', 'Master Rujukan Urinalisa Berhasil Ditambahkan!'); 
            Session::flash('alert-class', 'alert-success'); 
            return redirect('admin/rujukan-urinalisa');
        }
    }  

    public function inserturinalisana(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        // dd($input);
        $urin = DB::table('tb_rujukan_urinalisa')
                ->where('siklus', $input['siklus'])
                ->where('tahun', $input['tahun'])
                ->where('type', $input['type'])
                ->where('periode', $input['periode'])
                ->where(function($query) use ($input){
                    if ($input['periode'] == 2) {
                        $query->where('periode','=','2');
                    }
                })
                ->count();
        // dd($urin);
        if ($urin > 2 ) {
            Session::flash('message', 'Master Rujukan Urinalisa Gagal Ditambahkan!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('admin/rujukan-urinalisa');
        }else{
            $i = 0;
            foreach ($input['parameter'] as $parameter) {
                $saveDetail = new Rujukanurinalisa;
                $saveDetail->parameter = $input['parameter'][$i];
                $saveDetail->rujukan = $input['rujukan'][$i];
                $saveDetail->siklus = $input['siklus'];
                $saveDetail->type = $input['type'];
                $saveDetail->tahun = $input['tahun'];
                $saveDetail->bahan = $input['bahan'];
                $saveDetail->periode = $input['periode'];
                $saveDetail->save();
                $i++;
            }
            // dd($data);
            Session::flash('message', 'Master Rujukan Urinalisa Berhasil Ditambahkan!'); 
            Session::flash('alert-class', 'alert-success'); 
            return redirect('admin/rujukan-urinalisa');
        }
    }

    public function deleteurinalisa($id){
        $rujukan = DB::table('tb_rujukan_urinalisa')
                    ->join('parameter','tb_rujukan_urinalisa.parameter','=','parameter.id')
                    ->where('tb_rujukan_urinalisa.id', $id)
                    ->select('tb_rujukan_urinalisa.*','parameter.nama_parameter')
                    ->first(); 
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Rujukanurinalisa::where('siklus', $rujukan->siklus)->where('type', $rujukan->type)->where('tahun', $rujukan->tahun)->delete();
        return redirect("admin/rujukan-urinalisa");
    }
}
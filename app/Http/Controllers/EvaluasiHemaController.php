<?php

namespace App\Http\Controllers;
use DB;
use Session;
use App\User;
use App\SdMedian;
use App\SdMedianMetode;
use App\Ring;
use App\RingKK;
use App\RingKKAlat;
use App\RingKKMetode;
use App\SdMedianAlat;
use App\daftar as Daftar;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\register as Register;
use Excel;
use PDF;
use App\JobsEvaluasi;
use mikehaertl\wkhtmlto\Pdf as PdfWk;

use App\ZScore;
use App\ZScoreAlat;
use App\ZScoreMetode;

class EvaluasiHemaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['kimiakliniklaporanpelaksanaannapdf', 'hematologirekapitulasinap','apiairtzscorelaporannilainaair','apiairtzscorelaporannilaina']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function apiairtzscorelaporannilaina(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $type = $request->get('x');
        $siklus = $request->siklus;
        $date = $request->tahun;

        $input = $request->all();
        $data = DB::table('parameter')
                ->where('parameter.kategori','=','kimia air terbatas')
                ->get();

        foreach ($data as $key => $val) {
        $perusahaan = DB::table('tb_zscore')
                    ->select('tb_registrasi.kode_lebpes')
                    ->leftJoin('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                    ->where('tb_zscore.tahun', '=', $input['tahun'])
                    ->where('tb_zscore.parameter', '=', $val->id)
                    ->where('tb_zscore.siklus', '=', $input['siklus'])
                    ->where('tb_zscore.zscore', '!=', 'Tidak di Evaluasi')
                    ->orderBy('tb_zscore.zscore', 'ASC')
                    ->get();
        $val->perusahaan = $perusahaan;
        $datas = DB::table('tb_zscore')
                    ->select('tb_zscore.zscore')
                    ->leftJoin('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                    ->where('tb_zscore.tahun', '=', $input['tahun'])
                    ->where('tb_zscore.parameter', '=', $val->id)
                    ->where('tb_zscore.siklus', '=', $input['siklus'])
                    ->where('tb_zscore.zscore', '!=', 'Tidak di Evaluasi')
                    ->orderBy('tb_zscore.zscore', 'ASC')
                    ->get();
        $val->datas =$datas;
        $subtitle = DB::table('parameter')
                    ->select('nama_parameter')
                    ->where('id', '=', $val->id)
                    ->get();
        $val->subtitle = $subtitle;
      }
        return view('evaluasi.kimiaair.laporanzscorehasilpeserta.apigrafair', compact('data','perusahaan','datas','subtitle'));
    }

    public function apiairtzscorelaporannilainaair(\Illuminate\Http\Request $request)
    {
        $type = $request->get('x');
        $siklus = $request->siklus;
        $date = $request->tahun;

        $input = $request->all();
        $data = DB::table('parameter')
                ->where('parameter.kategori','=','kimia air')
                ->get();

        foreach ($data as $key => $val) {
        $perusahaan = DB::table('tb_zscore')
                    ->select('tb_registrasi.kode_lebpes')
                    ->leftJoin('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                    ->where('tb_zscore.tahun', '=', $input['tahun'])
                    ->where('tb_zscore.parameter', '=', $val->id)
                    ->where('tb_zscore.siklus', '=', $input['siklus'])
                    ->where('tb_zscore.zscore', '!=', 'Tidak di Evaluasi')
                    ->orderBy('tb_zscore.zscore', 'ASC')
                    ->get();
        $val->perusahaan = $perusahaan;
        $datas = DB::table('tb_zscore')
                    ->select('tb_zscore.zscore')
                    ->leftJoin('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                    ->where('tb_zscore.tahun', '=', $input['tahun'])
                    ->where('tb_zscore.parameter', '=', $val->id)
                    ->where('tb_zscore.siklus', '=', $input['siklus'])
                    ->where('tb_zscore.zscore', '!=', 'Tidak di Evaluasi')
                    ->orderBy('tb_zscore.zscore', 'ASC')
                    ->get();
        $val->datas =$datas;
        $subtitle = DB::table('parameter')
                    ->select('nama_parameter')
                    ->where('id', '=', $val->id)
                    ->get();
        $val->subtitle = $subtitle;
      }
        return view('evaluasi.kimiaair.laporanzscorehasilpeserta.apigrafair', compact('data','perusahaan','datas','subtitle'));
    }

     public function hemazscorelaporan()
     {
         return view('evaluasi.hematologi.laporanzscorehasilpeserta.index');
     }

     public function hemazscorelaporanna(\Illuminate\Http\Request $request)
     {
        $input = $request->all();
        $parameterna = DB::table('parameter')
                            ->where('kategori', 'hematologi')
                            ->orderBy('bagian', 'asc')
                            ->get();
        $sql = "SELECT
                    TR.id,
                    hp_headers.kode_lab,
                    TR.kode_lebpes,
                    (SELECT zscore FROM tb_zscore WHERE id_registrasi = TR.id AND parameter = 1 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS hemoglobin,
                    (SELECT zscore FROM tb_zscore WHERE id_registrasi = TR.id AND parameter = 2 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS lekosit,
                    (SELECT zscore FROM tb_zscore WHERE id_registrasi = TR.id AND parameter = 3 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS eritrosit,
                    (SELECT zscore FROM tb_zscore WHERE id_registrasi = TR.id AND parameter = 5 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS hematokrit,
                    (SELECT zscore FROM tb_zscore WHERE id_registrasi = TR.id AND parameter = 6 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS mcv,
                    (SELECT zscore FROM tb_zscore WHERE id_registrasi = TR.id AND parameter = 7 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS mch,
                    (SELECT zscore FROM tb_zscore WHERE id_registrasi = TR.id AND parameter = 8 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS mchc,
                    (SELECT zscore FROM tb_zscore WHERE id_registrasi = TR.id AND parameter = 4 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS trombosit,
                    tb_kirim_hasil.tanggal_kirim,
                    TR.updated_at
                FROM
                    `hp_headers`
                INNER JOIN tb_registrasi AS TR ON TR.id = hp_headers.id_registrasi
                LEFT JOIN tb_kirim_hasil ON tb_kirim_hasil.id_registrasi = TR.id
                AND tb_kirim_hasil.siklus = ".$input['siklus']."
                AND tb_kirim_hasil.type = '".$input['tipe']."'
                WHERE
                    TR.bidang = 1
                AND hp_headers.siklus = ".$input['siklus']."
                AND hp_headers.type = '".$input['tipe']."'
                AND (TR.siklus = 12 OR TR.siklus = ".$input['siklus'].")
                AND YEAR (TR.created_at) = ".$input['tahun']."";
        $data = DB::table(DB::raw("($sql) t"))->select('*')->get();
        // dd($data);
        if($input['tipe'] == 'a'){ 
            $type = '01';
        }else{ 
            $type = '02'; 
        }
         // dd($data);
         // return view('evaluasi.hematologi.laporanhasilpeserta.view', compact('data', 'parameterna'));
         Excel::create('Z-Score Peserta Per Parameter Hematologi Siklus '.$input['siklus'].' Type '.$type, function($excel) use ($data, $parameterna) {
             $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                 $sheet->loadView('evaluasi.hematologi.laporanzscorehasilpeserta.view', array('data'=>$data, 'parameterna'=>$parameterna) );
             });
         })->download('xls');
     }

     public function airzscorelaporan()
     {
         return view('evaluasi.kimiaair.laporanzscorehasilpeserta.index');
     }

     public function airzscorelaporanna(\Illuminate\Http\Request $request)
     {
         $input = $request->all();
         $parameterna = DB::table('parameter')
                         ->where('kategori', 'kimia air')
                         ->orderBy('bagian', 'asc')
                         ->get();
         $data = DB::table('hp_headers')
                 ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                 ->where('tb_registrasi.bidang', '=', '11')
                 ->where('hp_headers.siklus', $input['siklus'])
                 ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $input['tahun'])
                 ->select('hp_headers.kode_lab','hp_headers.id','hp_headers.id_registrasi', 'tb_registrasi.updated_at')
                 ->get();
         foreach ($data as $key => $val) {
             $parameter = DB::table('parameter')->where('kategori', 'kimia air')->orderBy('bagian', 'asc')->get();
             foreach ($parameter as $key => $par) {
                 $detail = DB::table('tb_zscore')
                             ->where('id_registrasi', $val->id_registrasi)
                             ->where('parameter', $par->id)
                             ->limit('1')
                             ->get();
                 $par->detail = $detail;
             }
             $val->parameter = $parameter;
         }
         // dd($val);

         // dd($data);
         // return view('evaluasi.hematologi.laporanhasilpeserta.view', compact('data', 'parameterna'));
         Excel::create('Z-Score Peserta Per Parameter Kimia Air', function($excel) use ($data, $parameterna) {
             $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                 $sheet->loadView('evaluasi.kimiaair.laporanzscorehasilpeserta.view', array('data'=>$data, 'parameterna'=>$parameterna) );
             });
         })->download('xls');
     }

     public function airtzscorelaporan()
     {
         return view('evaluasi.kimiaairterbatas.laporanzscorehasilpeserta.index');
     }

     public function airtzscorelaporannilai()
     {
        return view('evaluasi.kimiaairterbatas.laporanzscorehasilpeserta.nilai');
     }

    public function airtzscorelaporannilaina(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $type = $request->get('x');
        $siklus = $request->siklus;
        $date = $request->tahun;

        $input = $request->all();
        $data = DB::table('parameter')
                ->where('parameter.kategori','=','kimia air terbatas')
                ->get();

        foreach ($data as $key => $val) {
        $perusahaan = DB::table('tb_zscore')
                    ->select('tb_registrasi.kode_lebpes')
                    ->leftJoin('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                    ->where('tb_zscore.tahun', '=', $input['tahun'])
                    ->where('tb_zscore.parameter', '=', $val->id)
                    ->where('tb_zscore.siklus', '=', $input['siklus'])
                    ->where('tb_zscore.zscore', '!=', 'Tidak di Evaluasi')
                    ->orderBy('tb_zscore.zscore', 'ASC')
                    ->get();
        $val->perusahaan = $perusahaan;
        $datas = DB::table('tb_zscore')
                    ->select('tb_zscore.zscore')
                    ->leftJoin('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                    ->where('tb_zscore.tahun', '=', $input['tahun'])
                    ->where('tb_zscore.parameter', '=', $val->id)
                    ->where('tb_zscore.siklus', '=', $input['siklus'])
                    ->where('tb_zscore.zscore', '!=', 'Tidak di Evaluasi')
                    ->orderBy('tb_zscore.zscore', 'ASC')
                    ->get();
        $val->datas =$datas;
        $subtitle = DB::table('parameter')
                    ->select('nama_parameter')
                    ->where('id', '=', $val->id)
                    ->get();
        $val->subtitle = $subtitle;
      }
        return view('evaluasi.kimiaair.laporanzscorehasilpeserta.grafair', compact('data','perusahaan','datas','subtitle'));
    }

    public function airtzscorelaporannilaiair()
     {
         return view('evaluasi.kimiaair.laporanzscorehasilpeserta.nilaiair');
     }

    public function airtzscorelaporannilainaair(\Illuminate\Http\Request $request)
    {
        $type = $request->get('x');
        $siklus = $request->siklus;
        $date = $request->tahun;

        $input = $request->all();
        $data = DB::table('parameter')
                ->where('parameter.kategori','=','kimia air')
                ->get();

        foreach ($data as $key => $val) {
        $perusahaan = DB::table('tb_zscore')
                    ->select('tb_registrasi.kode_lebpes')
                    ->leftJoin('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                    ->where('tb_zscore.tahun', '=', $input['tahun'])
                    ->where('tb_zscore.parameter', '=', $val->id)
                    ->where('tb_zscore.siklus', '=', $input['siklus'])
                    ->where('tb_zscore.zscore', '!=', 'Tidak di Evaluasi')
                    ->orderBy('tb_zscore.zscore', 'ASC')
                    ->get();
        $val->perusahaan = $perusahaan;
        $datas = DB::table('tb_zscore')
                    ->select('tb_zscore.zscore')
                    ->leftJoin('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                    ->where('tb_zscore.tahun', '=', $input['tahun'])
                    ->where('tb_zscore.parameter', '=', $val->id)
                    ->where('tb_zscore.siklus', '=', $input['siklus'])
                    ->where('tb_zscore.zscore', '!=', 'Tidak di Evaluasi')
                    ->orderBy('tb_zscore.zscore', 'ASC')
                    ->get();
        $val->datas =$datas;
        $subtitle = DB::table('parameter')
                    ->select('nama_parameter')
                    ->where('id', '=', $val->id)
                    ->get();
        $val->subtitle = $subtitle;
      }
        return view('evaluasi.kimiaair.laporanzscorehasilpeserta.grafair', compact('data','perusahaan','datas','subtitle'));
    }




     public function airtzscorelaporanna(\Illuminate\Http\Request $request)
     {
         $input = $request->all();
         $parameterna = DB::table('parameter')
                         ->where('kategori', 'kimia air terbatas')
                         ->orderBy('bagian', 'asc')
                         ->get();
         $data = DB::table('hp_headers')
                 ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                 ->where('tb_registrasi.bidang', '=', '12')
                 ->where('hp_headers.siklus', $input['siklus'])
                 ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $input['tahun'])
                 ->select('hp_headers.kode_lab','hp_headers.id','hp_headers.id_registrasi', 'tb_registrasi.updated_at')
                 ->get();
         foreach ($data as $key => $val) {
             $parameter = DB::table('parameter')->where('kategori', 'kimia air terbatas')->orderBy('bagian', 'asc')->get();
             foreach ($parameter as $key => $par) {
                 $detail = DB::table('tb_zscore')
                             ->where('id_registrasi', $val->id_registrasi)
                             ->where('parameter', $par->id)
                             ->limit('1')
                             ->get();
                 $par->detail = $detail;
             }
             $val->parameter = $parameter;
         }
         // dd($val);

         // dd($data);
         // return view('evaluasi.hematologi.laporanhasilpeserta.view', compact('data', 'parameterna'));
         Excel::create('Z-Score Peserta Per Parameter Kimia Air Terbatas', function($excel) use ($data, $parameterna) {
             $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                 $sheet->loadView('evaluasi.kimiaairterbatas.laporanzscorehasilpeserta.view', array('data'=>$data, 'parameterna'=>$parameterna) );
             });
         })->download('xls');
     }

     public function hemazscorealatlaporan()
     {
         return view('evaluasi.hematologi.laporanzscorehasilpesertaalat.index');
     }

     public function hemazscorealatlaporanna(\Illuminate\Http\Request $request)
     {
         $input = $request->all();
         $parameterna = DB::table('parameter')
                         ->where('kategori', 'hematologi')
                         ->orderBy('bagian', 'asc')
                         ->get();
        $sql = "SELECT
                    TR.id,
                    hp_headers.kode_lab,
                    TR.kode_lebpes,
                    (SELECT zscore FROM tb_zscore_alat WHERE id_registrasi = TR.id AND parameter = 1 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS hemoglobin,
                    (SELECT zscore FROM tb_zscore_alat WHERE id_registrasi = TR.id AND parameter = 2 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS lekosit,
                    (SELECT zscore FROM tb_zscore_alat WHERE id_registrasi = TR.id AND parameter = 3 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS eritrosit,
                    (SELECT zscore FROM tb_zscore_alat WHERE id_registrasi = TR.id AND parameter = 5 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS hematokrit,
                    (SELECT zscore FROM tb_zscore_alat WHERE id_registrasi = TR.id AND parameter = 6 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS mcv,
                    (SELECT zscore FROM tb_zscore_alat WHERE id_registrasi = TR.id AND parameter = 7 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS mch,
                    (SELECT zscore FROM tb_zscore_alat WHERE id_registrasi = TR.id AND parameter = 8 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS mchc,
                    (SELECT zscore FROM tb_zscore_alat WHERE id_registrasi = TR.id AND parameter = 4 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS trombosit,
                    tb_kirim_hasil.tanggal_kirim,
                    TR.updated_at
                FROM
                    `hp_headers`
                INNER JOIN tb_registrasi AS TR ON TR.id = hp_headers.id_registrasi
                LEFT JOIN tb_kirim_hasil ON tb_kirim_hasil.id_registrasi = TR.id
                AND tb_kirim_hasil.siklus = ".$input['siklus']."
                AND tb_kirim_hasil.type = '".$input['tipe']."'
                WHERE
                    TR.bidang = 1
                AND hp_headers.siklus = ".$input['siklus']."
                AND hp_headers.type = '".$input['tipe']."'
                AND (TR.siklus = 12 OR TR.siklus = ".$input['siklus'].")
                AND YEAR (TR.created_at) = ".$input['tahun']."";
        $data = DB::table(DB::raw("($sql) t"))->select('*')->get();
         // dd($val);

        if($input['tipe'] == 'a'){ 
            $type = '01';
        }else{ 
            $type = '02'; 
        }
         // dd($data[0]);
         // return view('evaluasi.hematologi.laporanhasilpeserta.view', compact('data', 'parameterna'));
         Excel::create('Z-Score Peserta Per Alat Hematologi Siklus '.$input['siklus'].' Type '.$type, function($excel) use ($data, $parameterna) {
             $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                 $sheet->loadView('evaluasi.hematologi.laporanzscorehasilpesertaalat.view', array('data'=>$data, 'parameterna'=>$parameterna) );
             });
         })->download('xls');
     }

     public function hemazscoremetodelaporan()
     {
         return view('evaluasi.hematologi.laporanzscorehasilpesertametode.index');
     }

     public function hemazscoremetodelaporanna(\Illuminate\Http\Request $request)
     {
         $input = $request->all();
         $parameterna = DB::table('parameter')
                         ->where('kategori', 'hematologi')
                         ->orderBy('bagian', 'asc')
                         ->get();
        $sql = "SELECT
                    TR.id,
                    hp_headers.kode_lab,
                    TR.kode_lebpes,
                    (SELECT zscore FROM tb_zscore_metode WHERE id_registrasi = TR.id AND parameter = 1 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS hemoglobin,
                    (SELECT zscore FROM tb_zscore_metode WHERE id_registrasi = TR.id AND parameter = 2 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS lekosit,
                    (SELECT zscore FROM tb_zscore_metode WHERE id_registrasi = TR.id AND parameter = 3 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS eritrosit,
                    (SELECT zscore FROM tb_zscore_metode WHERE id_registrasi = TR.id AND parameter = 5 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS hematokrit,
                    (SELECT zscore FROM tb_zscore_metode WHERE id_registrasi = TR.id AND parameter = 6 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS mcv,
                    (SELECT zscore FROM tb_zscore_metode WHERE id_registrasi = TR.id AND parameter = 7 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS mch,
                    (SELECT zscore FROM tb_zscore_metode WHERE id_registrasi = TR.id AND parameter = 8 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS mchc,
                    (SELECT zscore FROM tb_zscore_metode WHERE id_registrasi = TR.id AND parameter = 4 AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." ) AS trombosit,
                    tb_kirim_hasil.tanggal_kirim,
                    TR.updated_at
                FROM
                    `hp_headers`
                INNER JOIN tb_registrasi AS TR ON TR.id = hp_headers.id_registrasi
                LEFT JOIN tb_kirim_hasil ON tb_kirim_hasil.id_registrasi = TR.id
                AND tb_kirim_hasil.siklus = ".$input['siklus']."
                AND tb_kirim_hasil.type = '".$input['tipe']."'
                WHERE
                    TR.bidang = 1
                AND hp_headers.siklus = ".$input['siklus']."
                AND hp_headers.type = '".$input['tipe']."'
                AND (TR.siklus = 12 OR TR.siklus = ".$input['siklus'].")
                AND YEAR (TR.created_at) = ".$input['tahun']."";
        $data = DB::table(DB::raw("($sql) t"))->select('*')->get();
         // dd($val);

         // dd($data[0]);
        if($input['tipe'] == 'a'){ 
            $type = '01';
        }else{ 
            $type = '02'; 
        }
         // return view('evaluasi.hematologi.laporanhasilpeserta.view', compact('data', 'parameterna'));
         Excel::create('Z-Score Peserta Per Metode Hematologi Siklus '.$input['siklus'].' Type '.$type, function($excel) use ($data, $parameterna) {
             $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                 $sheet->loadView('evaluasi.hematologi.laporanzscorehasilpesertametode.view', array('data'=>$data, 'parameterna'=>$parameterna) );
             });
         })->download('xls');
     }



    public function hemalaporan()
    {
        return view('evaluasi.hematologi.laporanhasilpeserta.index');
    }

    public function hemalaporanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameterna = DB::table('parameter')
                        ->where('kategori', 'hematologi')
                        ->orderBy('bagian', 'asc')
                        ->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('tb_registrasi.bidang', '=', '1')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', $input['tipe'])
                ->where(function($query) use ($input){
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $input['siklus']);
                })
                ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                    if (Auth::user()->id_member != '0') {
                        $query->where('tb_registrasi.created_by',Auth::user()->id);
                    }
                })
                ->select('hp_headers.*', 'tb_registrasi.updated_at', 'tb_registrasi.created_at')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'hematologi')->orderBy('bagian', 'asc')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->where('hp_header_id', $val->id)
                            ->where('parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data);
        // return view('evaluasi.hematologi.laporanhasilpeserta.view', compact('data', 'parameterna'));
        Excel::create('Laporan Hasil Peserta Per Parameter Hematologi', function($excel) use ($data, $parameterna) {
            $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('evaluasi.hematologi.laporanhasilpeserta.view', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
            // $excel->sheet('Data', function($sheet)  use ($data, $parameterna){

            //     $sheet->cell('A1', function($cell) {
            //         $cell->setValue('Kode Lab');
            //     });

            //     $alphas = range('A', 'Z');
            //     $a = 1;
            //     foreach($parameterna as $val){
            //         $sheet->cell($alphas[$a].'1', function($cell) use ($val) {
            //             $cell->setValue(strip_tags($val->nama_parameter));
            //         });
            //         $a++;
            //     }

            //     $startRow = 2;
            //     foreach($data as $val){
            //         $sheet->cell('A'.$startRow, function($cell) use ($val) {
            //             $cell->setValue($val->kode_lab);
            //         });
            //         $a = 1;
            //         foreach($val->parameter as $par){
            //             foreach($par->detail as $hasil){
            //                 $sheet->cell($alphas[$a].$startRow, function($cell) use ($hasil) {
            //                     $cell->setValue($hasil->hasil_pemeriksaan);
            //                 });
            //                 if($par->catatan == "Hasil pemeriksaan menggunakan 1 (satu) desimal"){
            //                     $format = '0.0';
            //                 }elseif($par->catatan == "Hasil pemeriksaan tanpa desimal"){
            //                     $format = '0';
            //                 }
            //                 $sheet->setColumnFormat(array(
            //                     $alphas[$a].$startRow => $format
            //                 ));
            //                 $a++;
            //             }

            //         }
            //         $startRow++;
            //     }
            //     $highestRow = $startRow - 1;
            //     $startRow = 2;

            // });
        })->download('xls');
    }

    public function hemaalatlaporan()
    {
        return view('evaluasi.hematologi.laporanalat.index');
    }

    public function hemaalatlaporanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameterna = DB::table('parameter')->where('kategori', 'hematologi')->orderBy('bagian', 'asc')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where(function($query) use ($input)
                    {
                        $query->where('tb_registrasi.siklus', '12')
                            ->orwhere('tb_registrasi.siklus', $input['siklus']);
                    })
                ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                })
                ->where('tb_registrasi.bidang', '=', '1')
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', $input['tipe'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('hp_headers.*', 'tb_registrasi.updated_at')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'hematologi')->orderBy('bagian', 'asc')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                            ->where('hp_header_id', $val->id)
                            ->where('parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data);
        Excel::create('Laporan Hasil Peserta Alat / Instrument Hematologi', function($excel) use ($data, $parameterna) {
            $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('evaluasi.hematologi.laporanalat.view', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
        })->download('xls');
    }

    public function hemametodelaporan()
    {
        return view('evaluasi.hematologi.laporanmetode.index');
    }

    public function hemametodelaporanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameterna = DB::table('parameter')->where('kategori', 'hematologi')->orderBy('bagian', 'asc')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                })
                ->where(function($query) use ($input)
                    {
                        $query->where('tb_registrasi.siklus', '12')
                            ->orwhere('tb_registrasi.siklus', $input['siklus']);
                    })
                ->where('tb_registrasi.bidang', '=', '1')
                ->where('hp_headers.siklus', $input['siklus'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('hp_headers.type', $input['tipe'])
                ->select('hp_headers.*', 'tb_registrasi.updated_at')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'hematologi')->orderBy('bagian', 'asc')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                            ->where('hp_details.hp_header_id', $val->id)
                            ->where('hp_details.parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data);
        Excel::create('Laporan Hasil Peserta Metode Hematologi', function($excel) use ($data, $parameterna) {
            $excel->sheet('Laporan', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('evaluasi.hematologi.laporanmetode.view', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
        })->download('xls');
    }

    public function hemasdmedian()
    {
        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->orderBy('bagian', 'asc')->get();
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'hematologi')
            ->orderBy('tb_sd_median.id', 'desc')
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->groupby('tahun', 'siklus', 'tipe','periode')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.hematologi.sdmedian.index', compact('parameter','sd'));
    }

    public function hemasdmedianinsert()
    {
        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->orderBy('bagian', 'asc')->get();
            // dd($sd);
        return view('evaluasi.hematologi.sdmedian.insert', compact('parameter'));
    }

    public function hemasdmedianedit($id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->orderBy('bagian', 'asc')->get();
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'hematologi')
            ->where('tb_sd_median.id', $id)
            ->orderBy('tb_sd_median.id', 'desc')
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->first();
        $data = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'hematologi')
            ->where('tb_sd_median.tahun', $sd->tahun)
            ->where('tb_sd_median.siklus', $sd->siklus)
            ->where('tb_sd_median.tipe', $sd->tipe)
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->get();
        // dd($data);
        return view('evaluasi.hematologi.sdmedian.view', compact('parameter','sd', 'data'));
    }

    public function hemasdmedianupdate(\Illuminate\Http\Request $request, $id)
    {
        // dd($request->all());
        $i = 0;
        foreach($request->parameter as $key => $r){
            $Update['parameter'] = $request->parameter[$i];
            $Update['siklus'] = $request->siklus;
            $Update['tahun'] = $request->tahun;
            $Update['tipe'] = $request->tipe;
            $Update['sd'] = $request->sd[$i];
            $Update['median'] = $request->median[$i];
            SdMedian::where('id',$request->id[$i])->update($Update);
            $i++;
        }
        // dd($input);
        Session::flash('message', 'Edit SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median/hematologi');
    }

    public function hemasdmediandelet($id){
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'hematologi')
            ->where('tb_sd_median.id', $id)
            ->orderBy('tb_sd_median.id', 'desc')
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->first();

        SdMedian::where('tahun', $sd->tahun)->where('siklus', $sd->siklus)->where('tipe', $sd->tipe)->delete();
        Session::flash('message', 'Hapus Data SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        return redirect('evaluasi/input-sd-median/hematologi');
    }

    public function hemasdmedianna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $i = 0;
        foreach($input['parameter'] as $key => $r){
            $Savedata = new SdMedian;
            $Savedata->parameter = $input['parameter'][$i];
            $Savedata->siklus = $input['siklus'];
            $Savedata->tahun = $input['tahun'];
            $Savedata->tipe = $input['tipe'];
            $Savedata->periode = $input['periode'];
            $Savedata->sd = $input['sd'][$i];
            $Savedata->median = $input['median'][$i];
            $Savedata->form = 'hematologi';
            $Savedata->save();
            $i++;
        }

        Session::flash('message', 'Input SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-sd-median/hematologi');
    }

    public function hemringedit(\Illuminate\Http\Request $request, $id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->get();
        $sd = DB::table('tb_ring')
            ->join('parameter','parameter.id','=','tb_ring.parameter')
            ->where('form', '=', 'hematologi')
            ->orderBy('tb_ring.id', 'desc')
            ->select('tb_ring.*', 'parameter.nama_parameter')
            ->where('tb_ring.id', $id)
            ->first();
        $data = DB::table('tb_ring')
            ->join('parameter','parameter.id','=','tb_ring.parameter')
            ->where('form', '=', 'hematologi')
            ->orderBy('parameter.bagian', 'asc')
            ->select('tb_ring.*', 'parameter.nama_parameter')
            ->where('tb_ring.tahun', $sd->tahun)
            ->where('tb_ring.siklus', $sd->siklus)
            ->where('tb_ring.tipe', $sd->tipe)
            ->get();
        return view('evaluasi.hematologi.ring.view', compact('sd', 'parameter', 'data'));
    }

    public function hemringeditna(\Illuminate\Http\Request $request, $id)
    {
        $i = 0 ;
        foreach ($request->parameter as $key => $value) {
            $Update['parameter'] = $request->parameter[$i];
            $Update['siklus'] = $request->siklus;
            $Update['tahun'] = $request->tahun;
            $Update['tipe'] = $request->tipe;
            $Update['ring1'] = $request->ring1[$i];
            $Update['ring2'] = $request->ring2[$i];
            $Update['ring3'] = $request->ring3[$i];
            $Update['ring4'] = $request->ring4[$i];
            $Update['ring5'] = $request->ring5[$i];
            $Update['ring6'] = $request->ring6[$i];
            Ring::where('id',$request->id[$i])->update($Update);
            $i++;
        }
        // dd($input);
        Session::flash('message', 'Edit Ring telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-ring/hematologi');
    }

    public function hemringdelet($id){
        $sd = DB::table('tb_ring')
            ->join('parameter','parameter.id','=','tb_ring.parameter')
            ->where('form', '=', 'hematologi')
            ->orderBy('tb_ring.id', 'desc')
            ->select('tb_ring.*', 'parameter.nama_parameter')
            ->where('tb_ring.id', $id)
            ->first();
        Ring::where('tahun', $sd->tahun)->where('tipe', $sd->tipe)->where('siklus', $sd->siklus)->where('form', '=', 'hematologi')->delete();
        Session::flash('message', 'Hapus Ring telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        return redirect('evaluasi/input-ring/hematologi');
    }

    public function hemring()
    {
        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->get();
        $sd = DB::table('tb_ring')
            ->join('parameter','parameter.id','=','tb_ring.parameter')
            ->where('form', '=', 'hematologi')
            ->orderBy('tb_ring.id', 'desc')
            ->select('tb_ring.*', 'parameter.nama_parameter')
            ->groupby('tahun', 'siklus', 'tipe','periode')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.hematologi.ring.index', compact('parameter','sd'));
    }

    public function hemringinsert()
    {
        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->orderBy('bagian', 'asc')->get();
            // dd($sd);
        return view('evaluasi.hematologi.ring.insert', compact('parameter'));
    }
    public function hemringna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $i = 0;
        foreach ($input['parameter'] as $key => $value) {
            $Savedata = new Ring;
            $Savedata->parameter = $input['parameter'][$i];
            $Savedata->siklus = $input['siklus'];
            $Savedata->tahun = $input['tahun'];
            $Savedata->tipe = $input['tipe'];
            $Savedata->periode = $input['periode'];
            $Savedata->ring1 = $input['ring1'][$i];
            $Savedata->ring2 = $input['ring2'][$i];
            $Savedata->ring3 = $input['ring3'][$i];
            $Savedata->ring4 = $input['ring4'][$i];
            $Savedata->ring5 = $input['ring5'][$i];
            $Savedata->ring6 = $input['ring6'][$i];
            $Savedata->form = 'hematologi';
            $Savedata->save();
            $i++;
        }

        Session::flash('message', 'Input SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-ring/hematologi');
    }

    public function hemasdmedianalat()
    {
        $alat = DB::table('tb_instrumen')->where('id_parameter','=','hematologi')->get();
        $parameter = DB::table('parameter')->where('kategori', '=','hematologi')->get();
        $sd = DB::table('tb_sd_median_alat')
            ->join('tb_instrumen','tb_instrumen.id','=','tb_sd_median_alat.alat')
            ->leftjoin('parameter','parameter.id','=','tb_sd_median_alat.parameter')
            ->where('form', '=', 'hematologi')
            ->orderBy('tb_sd_median_alat.id', 'desc')
            ->select('tb_sd_median_alat.*', 'tb_instrumen.instrumen', 'parameter.nama_parameter')
            ->groupby('tahun', 'siklus', 'tipe','periode')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.hematologi.sdmedianalat.index', compact('alat','sd', 'parameter'));
    }

    public function hemasdmedianalatinsert()
    {
        $alat = DB::table('tb_instrumen')->where('id_parameter','=','hematologi')
                    ->orderBy(DB::raw('(`instrumen` = "Instrument lain :") ASC, `instrumen`'))
                    ->get();
        $parameter = DB::table('parameter')->where('kategori', '=','hematologi')->orderBy('bagian','ASC')->get();
            // dd($sd);
        return view('evaluasi.hematologi.sdmedianalat.insert', compact('alat', 'parameter'));
    }

    public function hemasdmedianalatna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $i = 0;
        foreach ($input['parameter'] as $key => $value) {
            $Savedata = new SdMedianAlat;
            $Savedata->parameter = $input['parameter'][$i];
            $Savedata->alat = $input['alat'][$i];
            $Savedata->siklus = $input['siklus'];
            $Savedata->tahun = $input['tahun'];
            $Savedata->tipe = $input['tipe'];
            $Savedata->periode = $input['periode'];
            $Savedata->sd = $input['sd'][$i];
            $Savedata->median = $input['median'][$i];
            $Savedata->form = 'hematologi';
            $Savedata->save();
            $i++;
        }

        Session::flash('message', 'Input SD dan Median Alat / Instrument telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-sd-median-alat/hematologi');
    }

    public function hemasdmedianalatedit($id)
    {
        $alat = DB::table('tb_instrumen')->where('id_parameter','=','hematologi')->get();
        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->get();
        $sd = DB::table('tb_sd_median_alat')
            ->join('tb_instrumen','tb_instrumen.id','=','tb_sd_median_alat.alat')
            ->leftjoin('parameter','parameter.id','=','tb_sd_median_alat.parameter')
            ->where('form', '=', 'hematologi')
            ->where('tb_sd_median_alat.id', $id)
            ->orderBy('tb_sd_median_alat.id', 'desc')
            ->select('tb_sd_median_alat.*', 'tb_instrumen.instrumen', 'parameter.nama_parameter')
            ->first();
        $data = DB::table('tb_sd_median_alat')
            ->join('tb_instrumen','tb_instrumen.id','=','tb_sd_median_alat.alat')
            ->leftjoin('parameter','parameter.id','=','tb_sd_median_alat.parameter')
            ->where('form', '=', 'hematologi')
            ->where('tb_sd_median_alat.tahun', $sd->tahun)
            ->where('tb_sd_median_alat.siklus', $sd->siklus)
            ->where('tb_sd_median_alat.tipe', $sd->tipe)
            ->orderBy('parameter.bagian', 'ASC')
            ->select('tb_sd_median_alat.*', 'tb_instrumen.instrumen', 'parameter.nama_parameter')
            ->get();
            // dd($sd);
        return view('evaluasi.hematologi.sdmedianalat.view', compact('parameter','sd','alat', 'data'));
    }

    public function hemasdmedianalatupdate(\Illuminate\Http\Request $request, $id)
    {
        $i = 0;
        foreach ($request->parameter as $key => $value) {
            $Update['parameter'] = $request->parameter[$i];
            $Update['alat'] = $request->alat[$i];
            $Update['siklus'] = $request->siklus;
            $Update['tahun'] = $request->tahun;
            $Update['tipe'] = $request->tipe;
            $Update['sd'] = $request->sd[$i];
            $Update['median'] = $request->median[$i];
            SdMedianAlat::where('id',$request->id[$i])->update($Update);
            $i++;
        }
        // dd($input);
        Session::flash('message', 'Edit SD dan Median Alat / Instrument telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median-alat/hematologi');
    }

    public function hemasdmedianalatdelet($id){
        $sd = DB::table('tb_sd_median_alat')
            ->join('tb_instrumen','tb_instrumen.id','=','tb_sd_median_alat.alat')
            ->leftjoin('parameter','parameter.id','=','tb_sd_median_alat.parameter')
            ->where('form', '=', 'hematologi')
            ->where('tb_sd_median_alat.id', $id)
            ->orderBy('tb_sd_median_alat.id', 'desc')
            ->select('tb_sd_median_alat.*', 'tb_instrumen.instrumen', 'parameter.nama_parameter')
            ->first();
        SdMedianAlat::where('tahun', $sd->tahun)->where('siklus', $sd->siklus)->where('tipe', $sd->tipe)->delete();
        Session::flash('message', 'Hapus Data SD dan Median Alat / Instrument telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        return redirect('evaluasi/input-sd-median-alat/hematologi');
    }

    public function perusahaanhem(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes, tb_registrasi.id as idregistrasi'))
                        ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                        ->where('tb_registrasi.bidang','=','1')
                        ->where('tb_registrasi.status','>=','2')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                        ->where('tb_registrasi.status','>=','2')
                        ->where(function($query) use ($siklus)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $siklus);
                            })
                        ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="hematologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Proses
                                    </button>
                                </a>'
                        ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'a')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'b')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('print', function($data) use ($siklus, $tahun){
                    return "".'<a href="/evaluasi/hitung-zscore-cetak/hematologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Cetak
                                    </button>
                                </a>'
                        ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib', 'print' =>  'print'])
            ->make(true);

        }
        $jobPending = JobsEvaluasi::where('name','=','evaluasi-parameter')->where('status','=',0)->first();
        $showAllertPending = false;
        $showAllertSuccess = false;
        $showForm = true;
        if(!empty($jobPending)){
            $showAllertPending = true;
            $showAllertSuccess = false;
            $showForm = false;
        }else{
            $jobSuccess = JobsEvaluasi::where('name','=','evaluasi-parameter')->where('status','=',1)->where('isread','=',0)->first();
            if(!empty($jobSuccess)){
                $showAllertPending = false;
                $showAllertSuccess = true;
                $showForm = true;
                JobsEvaluasi::where('name','=','evaluasi-parameter')->where('status','=',1)->where('isread','=',0)->update(['isread'=>1]);
            }else{
                $showForm = true;
            }
        }

        return view('evaluasi.hematologi.zscore.perusahaan',compact('showAllertPending','showAllertSuccess','showForm'));
    }

    public function dataperusahaanhem(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '1')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.zscore.dataperusahaan', compact('data', 'siklus', 'tahun'));
    }

    public function perusahaanhemcetak(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, tb_registrasi.kode_lebpes, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where('tb_registrasi.bidang','=','1')
                ->where('tb_registrasi.status','>=','2')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="hematologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Cetak
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.hematologi.zscorecetak.perusahaan');
    }

    public function perusahaanhemcetaksertifikat(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, tb_registrasi.kode_lebpes, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                ->where('tb_registrasi.status','>=','2')
                ->where(function($query) use ($siklus)
                    {
                            $query->where('tb_registrasi.siklus', '=','12')
                                ->orwhere('tb_registrasi.siklus', '=', $siklus);
                    })
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Cetak
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        $bidang = Auth::user()->penyelenggara;
        return view('evaluasi.hematologi.zscorecetak.perusahaansertifikat', compact('bidang'));
    }

    public function dataperusahaanhemcetak(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '1')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.zscorecetak.dataperusahaan', compact('data', 'siklus'));
    }

    public function dataperusahaanhemcetaksertifikat(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '1')
                ->where('tb_registrasi.status', 3)
                // ->where(function($query)
                //     {
                //         $query->where('status_data1', 2)
                //             ->orwhere('status_data2', 2)
                //             ->orwhere('status_datarpr1', 2)
                //             ->orwhere('status_datarpr2', 2);
                //     })
                // ->where(function($query)
                //     {
                //         $query->where('pemeriksaan2', 'done')
                //             ->orwhere('pemeriksaan', 'done')
                //             ->orwhere('rpr1', 'done')
                //             ->orwhere('rpr2', 'done');
                //     })
                ->get();
        return view('evaluasi.hematologi.zscorecetak.dataperusahaansertifikat', compact('data','siklus','tahun'));
    }

    public function perusahaanhemnilai(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','1')->where('tb_registrasi.status','>=','2');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="hematologi/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.hematologi.grafiknilai.perusahaan');
    }

    public function dataperusahaanhemnilai($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '1')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.grafiknilai.dataperusahaan', compact('data'));
    }

    public function perusahaanhemalat(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes, tb_registrasi.id as idregistrasi'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where('tb_registrasi.bidang','=','1')
                ->where('tb_registrasi.status','>=','2')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where(function($query) use ($siklus)
                    {
                        $query->where('tb_registrasi.siklus', '12')
                            ->orwhere('tb_registrasi.siklus', $siklus);
                    })
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="hematologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_alat')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'a')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_alat')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'b')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('print', function($data) use ($siklus, $tahun){
                    return "".'<a href="/evaluasi/hitung-zscore-cetak/hematologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Cetak
                                    </button>
                                </a>'
                        ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib', 'print' =>  'print'])
            ->make(true);
        }
        $jobPending = JobsEvaluasi::where('name','=','evaluasi-alat')->where('status','=',0)->first();
        $showAllertPending = false;
        $showAllertSuccess = false;
        $showForm = true;
        if(!empty($jobPending)){
            $showAllertPending = true;
            $showAllertSuccess = false;
            $showForm = false;
        }else{
            $jobSuccess = JobsEvaluasi::where('name','=','evaluasi-alat')->where('status','=',1)->where('isread','=',0)->first();
            if(!empty($jobSuccess)){
                $showAllertPending = false;
                $showAllertSuccess = true;
                $showForm = true;
                JobsEvaluasi::where('name','=','evaluasi-alat')->where('status','=',1)->where('isread','=',0)->update(['isread'=>1]);
            }else{
                $showForm = true;
            }
        }
        return view('evaluasi.hematologi.zscorealat.perusahaan',compact('showAllertPending','showAllertSuccess','showForm'));
    }

    public function dataperusahaanhemalat(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '1')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.zscorealat.dataperusahaan', compact('data', 'siklus','tahun'));
    }


    public function perusahaankimiaalat(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','1')->where('tb_registrasi.status','>=','2');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="kimiaklinik/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.kimiaklinik.zscorealat.perusahaan');
    }

    public function dataperusahaankimiaalat($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '2')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaklinik.zscorealat.dataperusahaan', compact('data'));
    }

    public function perusahaankimiametode(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','2')->where('tb_registrasi.status','>=','2');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="kimiaklinik/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.kimiaklinik.zscoremetode.perusahaan');
    }

    public function dataperusahaankimiametode($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '2')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaklinik.zscoremetode.dataperusahaan', compact('data'));
    }

    public function perusahaanhemametode(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes, tb_registrasi.id as idregistrasi'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','1')
                ->where('tb_registrasi.status','>=','2')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where(function($query) use ($siklus)
                    {
                        $query->where('tb_registrasi.siklus', '12')
                            ->orwhere('tb_registrasi.siklus', $siklus);
                    })
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="hematologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'a')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'b')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('print', function($data) use ($siklus, $tahun){
                    return "".'<a href="/evaluasi/hitung-zscore-cetak/hematologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Cetak
                                    </button>
                                </a>'
                        ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib', 'print' =>  'print'])
            ->make(true);
        }
        $jobPending = JobsEvaluasi::where('name','=','evaluasi-metode')->where('status','=',0)->first();
        $showAllertPending = false;
        $showAllertSuccess = false;
        $showForm = true;
        if(!empty($jobPending)){
            $showAllertPending = true;
            $showAllertSuccess = false;
            $showForm = false;
        }else{
            $jobSuccess = JobsEvaluasi::where('name','=','evaluasi-metode')->where('status','=',1)->where('isread','=',0)->first();
            if(!empty($jobSuccess)){
                $showAllertPending = false;
                $showAllertSuccess = true;
                $showForm = true;
                JobsEvaluasi::where('name','=','evaluasi-metode')->where('status','=',1)->where('isread','=',0)->update(['isread'=>1]);
            }else{
                $showForm = true;
            }
        }
        return view('evaluasi.hematologi.zscoremetode.perusahaan',compact('showAllertPending','showAllertSuccess','showForm'));
    }

    public function dataperusahaanhemametode(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '1')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.zscoremetode.dataperusahaan', compact('data', 'siklus', 'tahun'));
    }

    public function laporanpesertakimiaklinik()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view('evaluasi.kimiaklinik.laporanhasilevaluasi.index', compact('parameter'));
    }

    public function laporanpesertakimiaklinikpdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $siklus = $input['siklus'];

        $input =$request->all();
        $tahun = $input['tahun'];
        $tanggal=Date('Y m d');

        $pesertatcs = DB::table('tb_telurcacing')
                        ->select(['id_registrasi' => 'id_registrasi'])
                        ->where('siklus', $siklus)
                        ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                        ->groupby('id_registrasi')
                        ->get();

        foreach($pesertatcs as $pesertatc) {
            $id = $pesertatc->id_registrasi;

            $data[$id] = DB::table('tb_telurcacing')
                    ->where('id_registrasi', $id)
                    ->where('siklus', $siklus)
                    ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                    ->first();
            $datas[$id] = DB::table('tb_telurcacing')
                    ->where('id_registrasi', $id)
                    ->where('siklus', $siklus)
                    ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                    ->get();
            $data_peserta[$id] = DB::table('tb_telurcacing')
                    ->join('tb_registrasi','tb_registrasi.id','=','tb_telurcacing.id_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->select('perusahaan.nama_lab','perusahaan.alamat','tb_telurcacing.kode_peserta')
                    ->where('id_registrasi', $id)
                    ->where('tb_telurcacing.siklus', $siklus)
                    ->where(DB::raw('YEAR(tb_telurcacing.created_at)'), '=' , $tahun)
                    ->first();
            $rujuk[$id] = DB::table('tb_rujukan_tc')
                    ->where('siklus', $siklus)
                    ->where('tahun', '=' , $tahun)
                    ->get();
            $evaluasi[$id] = DB::table('tb_evaluasi_tc')->where('siklus', $siklus)->where('tahun',$tahun)->where('id_registrasi',$id)->get();
            $evaluasisaran[$id] = DB::table('tb_evaluasi_tc')->where('id_registrasi',$id)->where('siklus', $siklus)->where('tahun',$tahun)->first();
            $nilaievaluasi[$id] = DB::table('tb_evaluasi_tc')->where('id_registrasi',$id)->where('siklus', $siklus)->where('tahun',$tahun)->select(DB::raw('AVG(nilai) as nilai'))->first();
            $register = Register::find($id);
            $perusahaan[$id] = $register->perusahaan->nama_lab;
        }

        $i = 0;

        if($input['proses'] == 'Export Excel') {
            Excel::create('Laporan Hasil Peserta Per Parameter Kimia Klinik', function($excel) use ($datas, $pesertatcs, $rujuk, $evaluasi, $nilaievaluasi, $i) {
                $excel->sheet('Wilayah', function($sheet) use ($datas, $pesertatcs, $rujuk, $evaluasi, $nilaievaluasi, $i) {
                    $sheet->loadView('evaluasi.tc.penilaian.pesertatc.print', array('pesertatcs' => $pesertatcs, 'datas' => $datas, 'rujuk' => $rujuk, 'evaluasi' => $evaluasi, 'nilaievaluasi' => $nilaievaluasi, 'i' => $i) );
                });
            })->download('xls');
        }

        $i = 1;
        if (count($evaluasi) > 0) {
            $pdf = PDF::loadview('evaluasi/tc/penilaian/pesertatc/print', compact('pesertatcs', 'data','data_peserta','evaluasisaran','tanggal','evaluasi','rujuk','nilaievaluasi','id','siklus','tanggal','tahun','register','perusahaan','datas', 'i'))
            ->setPaper('a4', 'potrait')
            ->setOptions(['dpi' => 135, 'defaultFont' => 'sans-serif'])
            ->setwarnings(false);
            return $pdf->stream('Telur Cacing.pdf');
        }else{
            Session::flash('message', 'TC belum dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
    }

    public function kimiakliniklaporan()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view('evaluasi.kimiaklinik.laporanhasilpeserta.index', compact('parameter'));
    }

    public function kimiakliniklaporanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameterna = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                })
                ->where(function($query) use ($input){
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $input['siklus']);
                })
                ->where('tb_registrasi.bidang', '=', '2')
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', $input['tipe'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $input['tahun'])
                ->select('hp_headers.*', 'tb_registrasi.updated_at')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->where('hp_header_id', $val->id)
                            ->where('parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data);
        // return view('evaluasi.kimiaklinik.laporanhasilpeserta.view', compact('data','parameterna'));
        Excel::create('Laporan Hasil Peserta Per Parameter Kimia Klinik', function($excel) use ($data, $parameterna) {
            $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('evaluasi.kimiaklinik.laporanhasilpeserta.view', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
        })->download('xls');
    }

    public function kimiaklinikrekapitulasi()
    {
        return view('evaluasi.kimiaklinik.rekapitulasihasil.index');
    }

    public function kimiaklinikrekapitulasina(\Illuminate\Http\Request $request)
    {
        // dd($request->all());
        $input = $request->all();
        $periode = "";
        $datasiklus = "";
        if ($input['siklus'] == 1) {
            if ($input['periode'] == 2 && $input['tahun'] == 2019) {
                $periode = "AND tb_registrasi.periode = 2";
                $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1))";
            }else{
                $periode = "AND tb_registrasi.periode IS NULL";
                $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1))";
            }
        }else{
            $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 2))";
        }

        $parameter = DB::table('parameter')->where('kategori', $input['form'])->orderBy('parameter.bagian', 'ASC')->get();
        // dd($parameter);
        foreach ($parameter as $key => $val) {
            $dataa1 = "SELECT
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='a' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id." 
                            AND hasil_pemeriksaan NOT IN  ('_', '-', '_','0','0.0','0.00')
                            AND tb_zscore.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                            AND
                                (
                                    (tb_zscore.zscore > 2 AND tb_zscore.zscore <= 3)
                                    OR
                                    (tb_zscore.zscore >= -3 AND tb_zscore.zscore < -2)
                                )
                            ".$periode."
                            ".$datasiklus."
                        ) AS PERINGATAN,
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='a' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id."
                            AND
                                hasil_pemeriksaan NOT IN  ('_', '-', '_','0','0.0','0.00')
                            AND tb_zscore.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                            AND (
                                    (tb_zscore.zscore >= -2 AND tb_zscore.zscore <= 2)
                                )
                            ".$periode."
                            ".$datasiklus."
                        ) AS MEMUASKAN,
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='a' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id." 
                            AND hasil_pemeriksaan NOT IN  ('_', '-', '_','0','0.0','0.00')
                            AND tb_zscore.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                            AND
                                (
                                    (tb_zscore.zscore < -3 OR tb_zscore.zscore > 3)
                                )
                            ".$periode."
                            ".$datasiklus."
                        ) AS TIDAKS_MEMUASKAN
                    ";
            $dataalata1 = "SELECT
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='a' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id." 
                                AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                AND tb_zscore_alat.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                AND
                                    (
                                        (tb_zscore_alat.zscore > 2 AND tb_zscore_alat.zscore <= 3)
                                        OR
                                        (tb_zscore_alat.zscore >= -3 AND tb_zscore_alat.zscore < -2)
                                    )
                                ".$periode."
                                ".$datasiklus."
                            ) AS PERINGATAN_ALAT,
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='a' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id."
                                AND
                                    hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                AND tb_zscore_alat.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                AND (
                                        (tb_zscore_alat.zscore >= -2 AND tb_zscore_alat.zscore <= 2)
                                    )
                                ".$periode."
                                ".$datasiklus."
                            ) AS MEMUASKAN_ALAT,
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='a' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id." 
                                AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                AND tb_zscore_alat.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                AND
                                    (
                                        (tb_zscore_alat.zscore < -3 OR tb_zscore_alat.zscore > 3)
                                    )
                                ".$periode."
                                ".$datasiklus."
                            ) AS TIDAKS_MEMUASKAN_ALAT
                            ";

            $datametodea1 = "SELECT
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='a' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id." 
                                    AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                    AND tb_zscore_metode.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                    AND
                                        (
                                            (tb_zscore_metode.zscore > 2 AND tb_zscore_metode.zscore <= 3)
                                            OR
                                            (tb_zscore_metode.zscore >= -3 AND tb_zscore_metode.zscore < -2)
                                        )
                                    ".$periode."
                                    ".$datasiklus."
                                ) AS PERINGATAN_METODE,
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='a' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id."
                                    AND
                                        hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                    AND tb_zscore_metode.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                    AND (
                                            (tb_zscore_metode.zscore >= -2 AND tb_zscore_metode.zscore <= 2)
                                        )
                                    ".$periode."
                                    ".$datasiklus."
                                ) AS MEMUASKAN_METODE,
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='a' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id." 
                                    AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                    AND tb_zscore_metode.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                    AND
                                        (
                                            (tb_zscore_metode.zscore < -3 OR tb_zscore_metode.zscore > 3)
                                        )
                                    ".$periode."
                                    ".$datasiklus."
                                ) AS TIDAKS_MEMUASKAN_METODE
                    ";
            $dataa2 = "SELECT
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='b' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id." 
                            AND hasil_pemeriksaan NOT IN  ('_', '-', '_','0','0.0','0.00')
                            AND tb_zscore.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                            AND
                                (
                                    (tb_zscore.zscore > 2 AND tb_zscore.zscore <= 3)
                                    OR
                                    (tb_zscore.zscore >= -3 AND tb_zscore.zscore < -2)
                                )
                            ".$periode."
                            ".$datasiklus."
                        ) AS PERINGATAN,
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='b' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id."
                            AND
                                hasil_pemeriksaan NOT IN  ('_', '-', '_','0','0.0','0.00')
                            AND tb_zscore.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                            AND (
                                    (tb_zscore.zscore >= -2 AND tb_zscore.zscore <= 2)
                                )
                            ".$periode."
                            ".$datasiklus."
                        ) AS MEMUASKAN,
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='b' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id." 
                            AND hasil_pemeriksaan NOT IN  ('_', '-', '_','0','0.0','0.00')
                            AND tb_zscore.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                            AND
                                (
                                    (tb_zscore.zscore < -3 OR tb_zscore.zscore > 3)
                                )
                            ".$periode."
                            ".$datasiklus."
                        ) AS TIDAKS_MEMUASKAN
                    ";

            $dataalata2 = "SELECT
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='b' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id." 
                                AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                AND tb_zscore_alat.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                AND
                                    (
                                        (tb_zscore_alat.zscore > 2 AND tb_zscore_alat.zscore <= 3)
                                        OR
                                        (tb_zscore_alat.zscore >= -3 AND tb_zscore_alat.zscore < -2)
                                    )
                                ".$periode."
                                ".$datasiklus."
                            ) AS PERINGATAN_ALAT,
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='b' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id."
                                AND
                                    hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                AND tb_zscore_alat.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                AND (
                                        (tb_zscore_alat.zscore >= -2 AND tb_zscore_alat.zscore <= 2)
                                    )
                                ".$periode."
                                ".$datasiklus."
                            ) AS MEMUASKAN_ALAT,
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='b' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id." 
                                AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                AND tb_zscore_alat.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                AND
                                    (
                                        (tb_zscore_alat.zscore < -3 OR tb_zscore_alat.zscore > 3)
                                    )
                                ".$periode."
                                ".$datasiklus."
                            ) AS TIDAKS_MEMUASKAN_ALAT
                            ";

            $datametodea2 = "SELECT
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='b' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id." 
                                    AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                    AND tb_zscore_metode.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                    AND
                                        (
                                            (tb_zscore_metode.zscore > 2 AND tb_zscore_metode.zscore <= 3)
                                            OR
                                            (tb_zscore_metode.zscore >= -3 AND tb_zscore_metode.zscore < -2)
                                        )
                                    ".$periode."
                                    ".$datasiklus."
                                ) AS PERINGATAN_METODE,
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='b' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id."
                                    AND
                                        hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                    AND tb_zscore_metode.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                    AND (
                                            (tb_zscore_metode.zscore >= -2 AND tb_zscore_metode.zscore <= 2)
                                        )
                                    ".$periode."
                                    ".$datasiklus."
                                ) AS MEMUASKAN_METODE,
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='b' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id." 
                                    AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                    AND tb_zscore_metode.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                    AND
                                        (
                                            (tb_zscore_metode.zscore < -3 OR tb_zscore_metode.zscore > 3)
                                        )
                                    ".$periode."
                                    ".$datasiklus."
                                ) AS TIDAKS_MEMUASKAN_METODE
                    ";

            $datapesertaa1 = DB::table(DB::raw("($dataa1) t"))->select('*')->first();
            $dataalata1 = DB::table(DB::raw("($dataalata1) t"))->select('*')->first();
            $datametodea1 = DB::table(DB::raw("($datametodea1) t"))->select('*')->first();
            $datapesertaa2 = DB::table(DB::raw("($dataa2) t"))->select('*')->first();
            $dataalata2 = DB::table(DB::raw("($dataalata2) t"))->select('*')->first();
            $datametodea2 = DB::table(DB::raw("($datametodea2) t"))->select('*')->first();

            $val->datapesertaa1 = $datapesertaa1;
            $val->dataalata1 = $dataalata1;
            $val->datametodea1 = $datametodea1;
            $val->datapesertaa2 = $datapesertaa2;
            $val->dataalata2 = $dataalata2;
            $val->datametodea2 = $datametodea2;
        }
        $pdf = PDF::loadview('evaluasi.kimiaklinik.rekapitulasihasil.view', compact('parameter', 'input'))
            ->setPaper('a4', 'lanscape')
            ->setwarnings(false);
        return $pdf->stream('Laporan Pelaksanaan PME '.$input['form'].'.pdf');
    }

    public function kimiaklinikrekapitulasipengiriman(\Illuminate\Http\Request $request)
    {
        // dd($request->all());
        $input = $request->all();
        $periode = "";
        $datasiklus = "";
        $dataa = "";
        $datab = "";
        if ($input['siklus'] == 1) {
            $dataa = "AND (tb_registrasi.status_data1 != 2 OR tb_registrasi.status_data1 is NULL)";
            $datab = "AND (tb_registrasi.status_data2 != 2 OR tb_registrasi.status_data2 is NULL)";
            if ($input['periode'] == 2 && $input['tahun'] == 2019) {
                $periode = "AND tb_registrasi.periode = 2";
                $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1))";
            }else{
                $periode = "AND tb_registrasi.periode IS NULL";
                $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1))";
            }
        }else{
            $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 2))";
            $dataa = "AND (tb_registrasi.status_datarpr1 != 2 OR tb_registrasi.status_datarpr1 is NULL)";
            $datab = "AND (tb_registrasi.status_datarpr2 != 2 OR tb_registrasi.status_datarpr2 is NULL)";
        }

        $parameter = DB::table('parameter')->where('kategori', $input['form'])->orderBy('parameter.bagian', 'ASC')->get();
        // dd($datab);
        foreach ($parameter as $key => $val) {
            $dataa1 = "SELECT
                        (
                            SELECT
                                count(*)
                            FROM
                                `hp_details`
                            INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori = '".$input['form']."' AND hp_headers.siklus = ".$input['siklus']." AND hp_headers.type = 'a'
                            INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                            WHERE hp_details.parameter_id = ".$val->id."
                            AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                            AND YEAR(tb_registrasi.created_at) = ".$input['tahun']."
                            ".$periode." ".$datasiklus."
                        ) AS KIRIM_HASIL_PESERTA,
                        (
                            SELECT
                                count(*)
                            FROM
                                `hp_details`
                            INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori = '".$input['form']."' AND hp_headers.siklus = ".$input['siklus']." AND hp_headers.type = 'a'
                            INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                            WHERE hp_details.parameter_id = ".$val->id."
                            AND hasil_pemeriksaan IN ('_', '-', '_','0','0.0','0.00')
                            AND YEAR(tb_registrasi.created_at) = ".$input['tahun']."
                            ".$periode." ".$datasiklus."
                        ) AS TIDAK_KIRIM_HASIL_PESERTA_1,
                        (
                            SELECT count(*) FROM tb_registrasi
                            WHERE tb_registrasi.bidang = 2
                            AND tb_registrasi.status = 3
                            AND Year(tb_registrasi.created_at) = ".$input['tahun']."
                            ".$dataa." ".$periode." ".$datasiklus."
                        ) AS TIDAK_KIRIM_HASIL_PESERTA_2
                    ";
            $dataa2 = "SELECT
                        (
                            SELECT
                                count(*)
                            FROM
                                `hp_details`
                            INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori = '".$input['form']."' AND hp_headers.siklus = ".$input['siklus']." AND hp_headers.type = 'b'
                            INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                            WHERE hp_details.parameter_id = ".$val->id."
                            AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                            AND YEAR(tb_registrasi.created_at) = ".$input['tahun']."
                            ".$periode." ".$datasiklus."
                        ) AS KIRIM_HASIL_PESERTA,
                        (
                            SELECT
                                count(*)
                            FROM
                                `hp_details`
                            INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori = '".$input['form']."' AND hp_headers.siklus = ".$input['siklus']." AND hp_headers.type = 'b'
                            INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                            WHERE hp_details.parameter_id = ".$val->id."
                            AND hasil_pemeriksaan IN ('_', '-', '_','0','0.0','0.00')
                            AND YEAR(tb_registrasi.created_at) = ".$input['tahun']."
                            ".$periode." ".$datasiklus."
                        ) AS TIDAK_KIRIM_HASIL_PESERTA_1,
                        (
                            SELECT count(*) FROM tb_registrasi
                            WHERE tb_registrasi.bidang = 2
                            AND tb_registrasi.status = 3
                            AND Year(tb_registrasi.created_at) = ".$input['tahun']."
                            ".$datab." ".$periode." ".$datasiklus."
                        ) AS TIDAK_KIRIM_HASIL_PESERTA_2
                    ";

            $datapesertaa1 = DB::table(DB::raw("($dataa1) t"))->select('*')->first();
            $datapesertaa2 = DB::table(DB::raw("($dataa2) t"))->select('*')->first();
            $val->datapesertaa1 = $datapesertaa1;
            $val->datapesertaa2 = $datapesertaa2;
        }
        // dd($parameter);
        $pdf = PDF::loadview('evaluasi.kimiaklinik.rekapitulasihasil.view_kirim', compact('parameter', 'input'))
            ->setPaper('a4', 'lanscape')
            ->setwarnings(false);
        return $pdf->stream('Laporan Pelaksanaan PME '.$input['form'].'.pdf');
    }

    public function hemarekapitulasipengiriman(\Illuminate\Http\Request $request)
    {
        // dd($request->all());
        $input = $request->all();
        $periode = "";
        $datasiklus = "";
        $dataa = "";
        $datab = "";
        if ($input['siklus'] == 1) {
            $dataa = "AND (tb_registrasi.status_data1 != 2 OR tb_registrasi.status_data1 is null )";
            $datab = "AND (tb_registrasi.status_data2 != 2 OR tb_registrasi.status_data2 is null )";
            if ($input['periode'] == 2 && $input['tahun'] == 2019) {
                $periode = "AND tb_registrasi.periode = 2";
                $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1))";
            }else{
                $periode = "AND tb_registrasi.periode IS NULL";
                $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1))";
            }
        }else{
            $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 2))";
            $dataa = "AND (tb_registrasi.status_datarpr1 != 2 OR tb_registrasi.status_datarpr1 is null)";
            $datab = "AND (tb_registrasi.status_datarpr2 != 2 OR tb_registrasi.status_datarpr2 is null)";
        }

        $parameter = DB::table('parameter')->where('kategori', $input['form'])->orderBy('parameter.bagian', 'ASC')->get();
        // dd($datab);
        foreach ($parameter as $key => $val) {
            $dataa1 = "SELECT
                        (
                            SELECT
                                count(*)
                            FROM
                                `hp_details`
                            INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori = '".$input['form']."' AND hp_headers.siklus = ".$input['siklus']." AND hp_headers.type = 'a'
                            INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                            WHERE hp_details.parameter_id = ".$val->id."
                            AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                            AND YEAR(tb_registrasi.created_at) = ".$input['tahun']."
                            ".$periode." ".$datasiklus."
                        ) AS KIRIM_HASIL_PESERTA,
                        (
                            SELECT
                                count(*)
                            FROM
                                `hp_details`
                            INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori = '".$input['form']."' AND hp_headers.siklus = ".$input['siklus']." AND hp_headers.type = 'a'
                            INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                            WHERE hp_details.parameter_id = ".$val->id."
                            AND hasil_pemeriksaan IN ('_', '-', '_','0','0.0','0.00')
                            AND YEAR(tb_registrasi.created_at) = ".$input['tahun']."
                            ".$periode." ".$datasiklus."
                        ) AS TIDAK_KIRIM_HASIL_PESERTA_1,
                        (
                            SELECT count(*) FROM tb_registrasi
                            WHERE tb_registrasi.bidang = 1
                            AND tb_registrasi.status = 3
                            AND Year(tb_registrasi.created_at) = ".$input['tahun']."
                            ".$dataa." ".$periode." ".$datasiklus."
                        ) AS TIDAK_KIRIM_HASIL_PESERTA_2
                    ";
            $dataa2 = "SELECT
                        (
                            SELECT
                                count(*)
                            FROM
                                `hp_details`
                            INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori = '".$input['form']."' AND hp_headers.siklus = ".$input['siklus']." AND hp_headers.type = 'b'
                            INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                            WHERE hp_details.parameter_id = ".$val->id."
                            AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                            AND YEAR(tb_registrasi.created_at) = ".$input['tahun']."
                            ".$periode." ".$datasiklus."
                        ) AS KIRIM_HASIL_PESERTA,
                        (
                            SELECT
                                count(*)
                            FROM
                                `hp_details`
                            INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori = '".$input['form']."' AND hp_headers.siklus = ".$input['siklus']." AND hp_headers.type = 'b'
                            INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                            WHERE hp_details.parameter_id = ".$val->id."
                            AND hasil_pemeriksaan IN ('_', '-', '_','0','0.0','0.00')
                            AND YEAR(tb_registrasi.created_at) = ".$input['tahun']."
                            ".$periode." ".$datasiklus."
                        ) AS TIDAK_KIRIM_HASIL_PESERTA_1,
                        (
                            SELECT count(*) FROM tb_registrasi
                            WHERE tb_registrasi.bidang = 1
                            AND tb_registrasi.status = 3
                            AND Year(tb_registrasi.created_at) = ".$input['tahun']."
                            ".$datab." ".$periode." ".$datasiklus."
                        ) AS TIDAK_KIRIM_HASIL_PESERTA_2
                    ";

            $datapesertaa1 = DB::table(DB::raw("($dataa1) t"))->select('*')->first();
            $datapesertaa2 = DB::table(DB::raw("($dataa2) t"))->select('*')->first();
            $val->datapesertaa1 = $datapesertaa1;
            $val->datapesertaa2 = $datapesertaa2;
        }
        // dd($parameter);
        $pdf = PDF::loadview('evaluasi.kimiaklinik.rekapitulasihasil.view_kirim', compact('parameter', 'input'))
            ->setPaper('a4', 'lanscape')
            ->setwarnings(false);
        return $pdf->stream('Laporan Pelaksanaan PME '.$input['form'].'.pdf');
    }

    public function hematologirekapitulasi()
    {
        return view('evaluasi.hematologi.rekapitulasihasil.index');
    }

    public function hematologirekapitulasina(\Illuminate\Http\Request $request)
    {
        // dd($request->all());
        $input = $request->all();
        $periode = "";
        $datasiklus = "";
        if ($input['siklus'] == 1) {
            if ($input['periode'] == 2 && $input['tahun'] == 2019) {
                $periode = "AND tb_registrasi.periode = 2";
                $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1))";
            }else{
                $periode = "AND tb_registrasi.periode IS NULL";
                $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1))";
            }
        }else{
            $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 2))";
        }

        $parameter = DB::table('parameter')->where('kategori', $input['form'])->orderBy('parameter.bagian', 'ASC')->get();
        // dd($parameter);
        foreach ($parameter as $key => $val) {
            $dataa1 = "SELECT
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='a' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id." 
                            AND hasil_pemeriksaan NOT IN  ('_', '-', '_','0','0.0','0.00')
                            AND tb_zscore.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                            AND
                                (
                                    (tb_zscore.zscore > 2 AND tb_zscore.zscore <= 3)
                                    OR
                                    (tb_zscore.zscore >= -3 AND tb_zscore.zscore < -2)
                                )
                            ".$periode."
                            ".$datasiklus."
                        ) AS PERINGATAN,
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='a' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id."
                            AND
                                hasil_pemeriksaan NOT IN  ('_', '-', '_','0','0.0','0.00')
                            AND tb_zscore.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                            AND (
                                    (tb_zscore.zscore >= -2 AND tb_zscore.zscore <= 2)
                                )
                            ".$periode."
                            ".$datasiklus."
                        ) AS MEMUASKAN,
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='a' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id." 
                            AND hasil_pemeriksaan NOT IN  ('_', '-', '_','0','0.0','0.00')
                            AND tb_zscore.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                            AND
                                (
                                    (tb_zscore.zscore < -3 OR tb_zscore.zscore > 3)
                                )
                            ".$periode."
                            ".$datasiklus."
                        ) AS TIDAKS_MEMUASKAN
                    ";
            $dataalata1 = "SELECT
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='a' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id." 
                                AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                AND tb_zscore_alat.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                AND
                                    (
                                        (tb_zscore_alat.zscore > 2 AND tb_zscore_alat.zscore <= 3)
                                        OR
                                        (tb_zscore_alat.zscore >= -3 AND tb_zscore_alat.zscore < -2)
                                    )
                                ".$periode."
                                ".$datasiklus."
                            ) AS PERINGATAN_ALAT,
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='a' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id."
                                AND
                                    hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                AND tb_zscore_alat.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                AND (
                                        (tb_zscore_alat.zscore >= -2 AND tb_zscore_alat.zscore <= 2)
                                    )
                                ".$periode."
                                ".$datasiklus."
                            ) AS MEMUASKAN_ALAT,
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='a' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id." 
                                AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                AND tb_zscore_alat.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                AND
                                    (
                                        (tb_zscore_alat.zscore < -3 OR tb_zscore_alat.zscore > 3)
                                    )
                                ".$periode."
                                ".$datasiklus."
                            ) AS TIDAKS_MEMUASKAN_ALAT
                            ";

            $datametodea1 = "SELECT
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='a' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id." 
                                    AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                    AND tb_zscore_metode.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                    AND
                                        (
                                            (tb_zscore_metode.zscore > 2 AND tb_zscore_metode.zscore <= 3)
                                            OR
                                            (tb_zscore_metode.zscore >= -3 AND tb_zscore_metode.zscore < -2)
                                        )
                                    ".$periode."
                                    ".$datasiklus."
                                ) AS PERINGATAN_METODE,
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='a' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id."
                                    AND
                                        hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                    AND tb_zscore_metode.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                    AND (
                                            (tb_zscore_metode.zscore >= -2 AND tb_zscore_metode.zscore <= 2)
                                        )
                                    ".$periode."
                                    ".$datasiklus."
                                ) AS MEMUASKAN_METODE,
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='a' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id." 
                                    AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                    AND tb_zscore_metode.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                    AND
                                        (
                                            (tb_zscore_metode.zscore < -3 OR tb_zscore_metode.zscore > 3)
                                        )
                                    ".$periode."
                                    ".$datasiklus."
                                ) AS TIDAKS_MEMUASKAN_METODE
                    ";
            $dataa2 = "SELECT
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='b' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id." 
                            AND hasil_pemeriksaan NOT IN  ('_','-','-','_')
                            AND tb_zscore.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                            AND
                                (
                                    (tb_zscore.zscore > 2 AND tb_zscore.zscore <= 3)
                                    OR
                                    (tb_zscore.zscore >= -3 AND tb_zscore.zscore < -2)
                                )
                            ".$periode."
                            ".$datasiklus."
                        ) AS PERINGATAN,
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='b' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id."
                            AND
                                hasil_pemeriksaan NOT IN  ('_','-','-','_')
                            AND tb_zscore.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                            AND (
                                    (tb_zscore.zscore >= -2 AND tb_zscore.zscore <= 2)
                                )
                            ".$periode."
                            ".$datasiklus."
                        ) AS MEMUASKAN,
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='b' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id." 
                            AND hasil_pemeriksaan NOT IN  ('_','-','-','_')
                            AND tb_zscore.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                            AND
                                (
                                    (tb_zscore.zscore < -3 OR tb_zscore.zscore > 3)
                                )
                            ".$periode."
                            ".$datasiklus."
                        ) AS TIDAKS_MEMUASKAN
                    ";

            $dataalata2 = "SELECT
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='b' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id." 
                                AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                AND tb_zscore_alat.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                AND
                                    (
                                        (tb_zscore_alat.zscore > 2 AND tb_zscore_alat.zscore <= 3)
                                        OR
                                        (tb_zscore_alat.zscore >= -3 AND tb_zscore_alat.zscore < -2)
                                    )
                                ".$periode."
                                ".$datasiklus."
                            ) AS PERINGATAN_ALAT,
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='b' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id."
                                AND
                                    hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                AND tb_zscore_alat.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                AND (
                                        (tb_zscore_alat.zscore >= -2 AND tb_zscore_alat.zscore <= 2)
                                    )
                                ".$periode."
                                ".$datasiklus."
                            ) AS MEMUASKAN_ALAT,
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='b' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id." 
                                AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                AND tb_zscore_alat.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                AND
                                    (
                                        (tb_zscore_alat.zscore < -3 OR tb_zscore_alat.zscore > 3)
                                    )
                                ".$periode."
                                ".$datasiklus."
                            ) AS TIDAKS_MEMUASKAN_ALAT
                            ";

            $datametodea2 = "SELECT
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='b' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id." 
                                    AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                    AND tb_zscore_metode.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                    AND
                                        (
                                            (tb_zscore_metode.zscore > 2 AND tb_zscore_metode.zscore <= 3)
                                            OR
                                            (tb_zscore_metode.zscore >= -3 AND tb_zscore_metode.zscore < -2)
                                        )
                                    ".$periode."
                                    ".$datasiklus."
                                ) AS PERINGATAN_METODE,
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='b' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id."
                                    AND
                                        hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                    AND tb_zscore_metode.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                    AND (
                                            (tb_zscore_metode.zscore >= -2 AND tb_zscore_metode.zscore <= 2)
                                        )
                                    ".$periode."
                                    ".$datasiklus."
                                ) AS MEMUASKAN_METODE,
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='b' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id." 
                                    AND hasil_pemeriksaan NOT IN ('_', '-', '_','0','0.0','0.00')
                                    AND tb_zscore_metode.zscore NOT IN ('-','Tidak di analisis','Tidak dapat dievaluasi')
                                    AND
                                        (
                                            (tb_zscore_metode.zscore < -3 OR tb_zscore_metode.zscore > 3)
                                        )
                                    ".$periode."
                                    ".$datasiklus."
                                ) AS TIDAKS_MEMUASKAN_METODE
                    ";

            $datapesertaa1 = DB::table(DB::raw("($dataa1) t"))->select('*')->first();
            $dataalata1 = DB::table(DB::raw("($dataalata1) t"))->select('*')->first();
            $datametodea1 = DB::table(DB::raw("($datametodea1) t"))->select('*')->first();
            $datapesertaa2 = DB::table(DB::raw("($dataa2) t"))->select('*')->first();
            $dataalata2 = DB::table(DB::raw("($dataalata2) t"))->select('*')->first();
            $datametodea2 = DB::table(DB::raw("($datametodea2) t"))->select('*')->first();

            $val->datapesertaa1 = $datapesertaa1;
            $val->dataalata1 = $dataalata1;
            $val->datametodea1 = $datametodea1;
            $val->datapesertaa2 = $datapesertaa2;
            $val->dataalata2 = $dataalata2;
            $val->datametodea2 = $datametodea2;
        }
        $pdf = PDF::loadview('evaluasi.hematologi.rekapitulasihasil.view', compact('parameter', 'input'))
            ->setPaper('a4', 'lanscape')
            ->setwarnings(false);
        return $pdf->stream('Laporan Pelaksanaan PME '.$input['form'].'.pdf');
    }

    public function kimiakliniklaporanpelaksanaan()
    {
        $propinsi = DB::table('provinces')->get();
        return view('evaluasi.kimiaklinik.laporanpelaksanaan.index', compact('propinsi'));
    }

    public function kimiakliniklaporanpelaksanaanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        // dd($data);
        if ($input['proses'] == 'Proses') {
            $parameterna = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
            $data = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->join('provinces','provinces.id','=','perusahaan.provinsi')
                    ->leftjoin('hp_headers as HPHA',function($join)use($input){
                        $join->on("HPHA.id_registrasi",'=','tb_registrasi.id')
                        ->on("HPHA.siklus",'=',DB::raw($input['siklus']))
                        ->on("HPHA.type",'=',DB::raw('"a"'));
                    })
                    ->leftjoin('hp_headers as HPHB',function($join)use($input){
                        $join->on("HPHB.id_registrasi",'=','tb_registrasi.id')
                        ->on("HPHB.siklus",'=',DB::raw($input['siklus']))
                        ->on("HPHB.type",'=',DB::raw('"b"'));
                    })
                    ->where(function($query) use ($input){
                        $query->where('tb_registrasi.siklus', '12')
                            ->orwhere('tb_registrasi.siklus', $input['siklus']);
                    })
                    ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                    })
                    ->whereIn('tb_registrasi.bidang', ['1', '2', '3'])
                    ->whereIn('tb_registrasi.status', ['2','3'])
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'),'=', $input['tahun'])
                    ->select(
                        'kode_lebpes as kode_lab',
                        'bidang',
                        'provinces.name',
                        DB::raw('(SELECT tgl_penerimaan FROM hp_headers where hp_headers.id_registrasi = tb_registrasi.id LIMIT 0,1) as tgl_penerimaan'),
                        'HPHA.kualitas_bahan as kualitas_bahan_a',
                        'HPHB.kualitas_bahan as kualitas_bahan_b',
                        'HPHA.tgl_pemeriksaan as tgl_pemeriksaan_a',
                        'HPHB.tgl_pemeriksaan as tgl_pemeriksaan_b',
                        'tb_registrasi.updated_at as tgl_kirim_a',
                        'tb_registrasi.updated_at as tgl_kirim_b'
                    )
                    ->orderBy('tb_registrasi.kode_lebpes', 'asc')
                    ->get();
            // return view('evaluasi.kimiaklinik.laporanpelaksanaan.view', compact('data','parameterna'));
            Excel::create('LAPORAN PELAKSANAAN PNPME Kimia Klinik', function($excel) use ($data, $parameterna) {
                $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                    $sheet->loadView('evaluasi.kimiaklinik.laporanpelaksanaan.view', array('data'=>$data, 'parameterna'=>$parameterna) );
                });
            })->download('xls');
        }else{
            $url = url("evaluasi/laporan-pelaksanaan/kimiaklinik/pdf?siklus=".$input['siklus']."&tahun=".$input['tahun']."&download=true");
            // dd($url);
            return redirect($url);
            $image = new PdfWk($url);
            $image->setOptions([
                'binary' => env('PATH_WKHTMLPDF', '/usr/local/bin/wkhtmltopdf')
            ]);

            if(!$image->send()){
                dd($image->getError());
            }
        }
    }

    public function kimiakliniklaporanpelaksanaannapdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameterna = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
        $data = DB::table('tb_registrasi')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->join('provinces','provinces.id','=','perusahaan.provinsi')
                ->leftjoin('hp_headers as HPHA',function($join)use($input){
                    $join->on("HPHA.id_registrasi",'=','tb_registrasi.id')
                    ->on("HPHA.siklus",'=',DB::raw($input['siklus']))
                    ->on("HPHA.type",'=',DB::raw('"a"'));
                })
                ->leftjoin('hp_headers as HPHB',function($join)use($input){
                    $join->on("HPHB.id_registrasi",'=','tb_registrasi.id')
                    ->on("HPHB.siklus",'=',DB::raw($input['siklus']))
                    ->on("HPHB.type",'=',DB::raw('"b"'));
                })
                ->where(function($query) use ($input){
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $input['siklus']);
                })
                ->whereIn('tb_registrasi.bidang', ['1', '2', '3'])
                ->whereIn('tb_registrasi.status', ['2','3'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'),'=', $input['tahun'])
                ->select(
                    'kode_lebpes as kode_lab',
                    'bidang',
                    'provinces.name',
                    DB::raw('(SELECT tgl_penerimaan FROM hp_headers where hp_headers.id_registrasi = tb_registrasi.id LIMIT 0,1) as tgl_penerimaan'),
                    'HPHA.kualitas_bahan as kualitas_bahan_a',
                    'HPHB.kualitas_bahan as kualitas_bahan_b',
                    'HPHA.tgl_pemeriksaan as tgl_pemeriksaan_a',
                    'HPHB.tgl_pemeriksaan as tgl_pemeriksaan_b',
                    'tb_registrasi.updated_at as tgl_kirim_a',
                    'tb_registrasi.updated_at as tgl_kirim_b'
                )
                ->orderBy('tb_registrasi.kode_lebpes', 'asc')
                ->get();
        // dd($data);
        if(!empty($request->query('download'))){
                $url = url("evaluasi/laporan-pelaksanaan/kimiaklinik/pdf?siklus=".$input['siklus']."&tahun=".$input['tahun']);
                // dd($url);
                // return redirect($url);
                $image = new PdfWk($url);
                $image->setOptions([
                    'binary' => env('PATH_WKHTMLPDF', '/usr/local/bin/wkhtmltopdf')
                ]);

                if(!$image->send()){
                    dd($image->getError());
                }
        }else{
            return view('evaluasi.kimiaklinik.laporanpelaksanaan.print', compact('data', 'input'));
        }

    }


    public function kimiaklinikalatlaporan()
    {
        return view('evaluasi.kimiaklinik.laporanalat.index');
    }

    public function kimiaklinikalatlaporanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameterna = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                })
                ->where(function($query) use ($input){
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $input['siklus']);
                })
                ->where('tb_registrasi.bidang', '=', '2')
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', $input['tipe'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('hp_headers.*', 'tb_registrasi.updated_at')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                            ->where('hp_header_id', $val->id)
                            ->where('parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data);
        // return view('evaluasi.kimiaklinik.laporanalat.view', compact('parameterna', 'data'));
        Excel::create('Laporan Hasil Peserta Alat / Instrument Kimia Klinik', function($excel) use ($data, $parameterna) {
            $excel->sheet('Laporan', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('evaluasi.kimiaklinik.laporanalat.view', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
        })->download('xls');
    }

    public function kimiaringedit(\Illuminate\Http\Request $request, $id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        $sd = DB::table('tb_ring')
            ->join('parameter','parameter.id','=','tb_ring.parameter')
            ->where('form', '=', 'kimia klinik')
            ->orderBy('tb_ring.id', 'desc')
            ->select('tb_ring.*', 'parameter.nama_parameter')
            ->where('tb_ring.id', $id)
            ->first();
        return view('evaluasi.kimiaklinik.ring.view', compact('sd', 'parameter'));
    }


    public function kimiaringeditna(\Illuminate\Http\Request $request, $id)
    {
        $Update['parameter'] = $request->parameter;
        $Update['siklus'] = $request->siklus;
        $Update['tahun'] = $request->tahun;
        $Update['tipe'] = $request->tipe;
        $Update['ring1'] = $request->ring1;
        $Update['ring2'] = $request->ring2;
        $Update['ring3'] = $request->ring3;
        $Update['ring4'] = $request->ring4;
        $Update['ring5'] = $request->ring5;
        $Update['ring6'] = $request->ring6;

        Ring::where('id',$id)->update($Update);
        // dd($input);
        Session::flash('message', 'Edit Ring telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-ring/kimiaklinik');
    }

    public function kimiaringdelet(\Illuminate\Http\Request $request){
        Session::flash('message', 'Hapus Ring telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        $tahun = $request->query('tahun');
        $tipe = $request->query('tipe');
        $siklus = $request->query('siklus');

        RingKK::where('tahun','=',$tahun)->where('siklus','=',$siklus)->where('tipe','=',$tipe)->delete();
        return redirect('evaluasi/input-ring/kimiaklinik');
    }

    public function kimiaringdeletalat(\Illuminate\Http\Request $request){
        Session::flash('message', 'Hapus Ring telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        $tahun = $request->query('tahun');
        $tipe = $request->query('tipe');
        $siklus = $request->query('siklus');

        RingKKAlat::where('tahun','=',$tahun)->where('siklus','=',$siklus)->where('tipe','=',$tipe)->delete();
        return redirect('evaluasi/input-ring/kimiaklinik-alat');
    }

    public function kimiaringdeletmetode(\Illuminate\Http\Request $request){
        Session::flash('message', 'Hapus Ring telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        $tahun = $request->query('tahun');
        $tipe = $request->query('tipe');
        $siklus = $request->query('siklus');

        RingKKMetode::where('tahun','=',$tahun)->where('siklus','=',$siklus)->where('tipe','=',$tipe)->delete();
        return redirect('evaluasi/input-ring/kimiaklinik-metode');
    }

    public function kimiaring()
    {
        $sd = DB::table('tb_ring_kk')
            ->where('form', '=', 'kimia klinik')
            ->groupBy('tb_ring_kk.tahun','tb_ring_kk.siklus','tb_ring_kk.tipe','tb_ring_kk.periode')
            ->select('tb_ring_kk.tahun','tb_ring_kk.siklus','tb_ring_kk.tipe')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.kimiaklinik.ring.index', compact('sd'));
    }

    public function kimiaringalat()
    {
        $sd = DB::table('tb_ring_kk_alat')
            ->where('form', '=', 'kimia klinik')
            ->groupBy('tb_ring_kk_alat.tahun','tb_ring_kk_alat.siklus','tb_ring_kk_alat.tipe','tb_ring_kk_alat.periode')
            ->select('tb_ring_kk_alat.tahun','tb_ring_kk_alat.siklus','tb_ring_kk_alat.tipe')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.kimiaklinik.ringalat.index', compact('sd'));
    }

    public function kimiaringmetode()
    {
        $sd = DB::table('tb_ring_kk_metode')
            ->where('form', '=', 'kimia klinik')
            ->groupBy('tb_ring_kk_metode.tahun','tb_ring_kk_metode.siklus','tb_ring_kk_metode.tipe','tb_ring_kk_metode.periode')
            ->select('tb_ring_kk_metode.tahun','tb_ring_kk_metode.siklus','tb_ring_kk_metode.tipe')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.kimiaklinik.ringmetode.index', compact('sd'));
    }

    public function kimiaringna(\Illuminate\Http\Request $request)
    {
        $tahun = $request->query('tahun');
        $tipe = $request->query('tipe');
        $siklus = $request->query('siklus');
        $periode = $request->query('periode');
        // $retVal = (condition) ? a : b ;
        $sd = DB::table('parameter')
            ->leftJoin('tb_ring_kk', function ($join) use($tipe,$siklus,$tahun, $periode) {
                $join->on('parameter.id', '=', 'tb_ring_kk.parameter')
                     ->where('tb_ring_kk.tipe', '=', $tipe)
                     ->where('tb_ring_kk.tahun', '=', $tahun)
                     ->where('tb_ring_kk.siklus', '=', $siklus)
                     ->where('tb_ring_kk.form', '=', 'kimia klinik');
                     if ($periode == 2) {
                        $join->where('tb_ring_kk.periode', '=', $periode);
                     }
            })
            ->leftjoin('tb_sd_median','tb_sd_median.parameter','=','parameter.id')
            ->where('tb_sd_median.siklus', $siklus)
            ->where('tb_sd_median.tipe', $tipe)
            ->where('tb_sd_median.tahun', $tahun)
            ->where(function($query) use ($siklus, $periode){
                if ($siklus == 1) {
                    if ($periode == 2) {
                        $query->where('tb_sd_median.periode','=','2');
                    }
                }
            })
            ->where('parameter.kategori','=','kimia klinik')
            ->select('parameter.nama_parameter','parameter.catatan','parameter.id as id_parameter','tb_ring_kk.*','tb_sd_median.sd','tb_sd_median.median')
            ->get();
        // dd($sd);
        return view('evaluasi.kimiaklinik.ring.insert', compact('sd','tahun','siklus','tipe','periode'));
    }

    public function kimiaringprint(\Illuminate\Http\Request $request)
    {
        $tahun = $request->query('tahun');
        $tipe = $request->query('tipe');
        $siklus = $request->query('siklus');
        $periode = $request->query('periode');
        // $retVal = (condition) ? a : b ;
        $sd = DB::table('parameter')
            ->leftJoin('tb_ring_kk', function ($join) use($tipe,$siklus,$tahun, $periode) {
                $join->on('parameter.id', '=', 'tb_ring_kk.parameter')
                     ->where('tb_ring_kk.tipe', '=', $tipe)
                     ->where('tb_ring_kk.tahun', '=', $tahun)
                     ->where('tb_ring_kk.siklus', '=', $siklus)
                     ->where('tb_ring_kk.form', '=', 'kimia klinik');
                     if ($periode == 2) {
                        $join->where('tb_ring_kk.periode', '=', $periode);
                     }
            })
            ->leftjoin('tb_sd_median','tb_sd_median.parameter','=','parameter.id')
            ->where('tb_sd_median.siklus', $siklus)
            ->where('tb_sd_median.tipe', $tipe)
            ->where('tb_sd_median.tahun', $tahun)
            ->where(function($query) use ($siklus, $periode){
                if ($siklus == 1) {
                    if ($periode == 2) {
                        $query->where('tb_sd_median.periode','=','2');
                    }
                }
            })
            ->where('parameter.kategori','=','kimia klinik')
            ->select('parameter.nama_parameter','parameter.catatan','parameter.id as id_parameter','tb_ring_kk.*','tb_sd_median.sd','tb_sd_median.median')
            ->get();
        // dd($sd);
        $pdf = PDF::loadview('evaluasi.kimiaklinik.ring.print', compact('sd','tahun','siklus','tipe','periode'))
            ->setPaper('legal', 'landscape')
            ->setwarnings(false);
        return $pdf->stream('Laporan Nilai Range per Parameter.pdf');
    }

    public function kimiaringnaalat(\Illuminate\Http\Request $request)
    {
        $tahun = $request->query('tahun');
        $tipe = $request->query('tipe');
        $siklus = $request->query('siklus');
        $periode = $request->query('periode');

        $sd = DB::table('tb_instrumen')
            ->join('parameter','parameter.id','=','tb_instrumen.id_parameter')
            ->leftJoin('tb_ring_kk_alat', function ($join) use($tipe,$siklus,$tahun,$periode) {
                $join->on('tb_instrumen.id', '=', 'tb_ring_kk_alat.instrumen_id')
                     ->where('tb_ring_kk_alat.tipe', '=', $tipe)
                     ->where('tb_ring_kk_alat.tahun', '=', $tahun)
                     ->where('tb_ring_kk_alat.siklus', '=', $siklus)
                     ->where('tb_ring_kk_alat.form', '=', 'kimia klinik');
                     if ($periode == 2) {
                     $join->where('tb_ring_kk_alat.periode', '=', 2);
                     }
            })
            ->leftjoin('tb_sd_median_alat','tb_sd_median_alat.alat','=','tb_instrumen.id')
            ->where('tb_sd_median_alat.siklus', $siklus)
            ->where('tb_sd_median_alat.tipe', $tipe)
            ->where('tb_sd_median_alat.tahun', $tahun)
            ->where(function($query) use ($siklus, $periode){
                if ($siklus == 1) {
                    if ($periode == 2) {
                        $query->where('tb_sd_median_alat.periode','=','2');
                    }
                }
            })
            ->having('total_data','>=',8)
            ->where('parameter.kategori','=','kimia klinik')
            ->where('tb_instrumen.instrumen','!=','Alat Lain :')
            ->select('parameter.nama_parameter','parameter.catatan','tb_sd_median_alat.sd','tb_sd_median_alat.median','parameter.id as id_parameter','tb_ring_kk_alat.*','tb_instrumen.instrumen as nama_instrumen','tb_instrumen.id as id_instrumen', DB::raw("(
                SELECT COUNT(*) from hp_details as HPD
                INNER JOIN `hp_headers` ON `hp_headers`.`id` = `HPD`.`hp_header_id`
                WHERE
                HPD.alat = tb_instrumen.id
                AND
                `HPD`.`hasil_pemeriksaan` NOT IN (\"-\",\"0.0\",\"0\",\"0.00\",'\"_\"','\"-\"',\"_\")
                AND
                `hp_headers`.`type` = '".$tipe."'
                AND
                `hp_headers`.`siklus` = '".$siklus."'
                AND
                YEAR(`hp_headers`.`tgl_pemeriksaan`) = ".$tahun."

            ) as total_data"))
            ->get();
        // dd($sd);
        return view('evaluasi.kimiaklinik.ringalat.insert', compact('sd','tahun','siklus','tipe','periode'));
    }

    public function kimiaringnaalatprint(\Illuminate\Http\Request $request)
    {
        $tahun = $request->query('tahun');
        $tipe = $request->query('tipe');
        $siklus = $request->query('siklus');
        $periode = $request->query('periode');

        $sd = DB::table('tb_instrumen')
            ->join('parameter','parameter.id','=','tb_instrumen.id_parameter')
            ->leftJoin('tb_ring_kk_alat', function ($join) use($tipe,$siklus,$tahun,$periode) {
                $join->on('tb_instrumen.id', '=', 'tb_ring_kk_alat.instrumen_id')
                     ->where('tb_ring_kk_alat.tipe', '=', $tipe)
                     ->where('tb_ring_kk_alat.tahun', '=', $tahun)
                     ->where('tb_ring_kk_alat.siklus', '=', $siklus)
                     ->where('tb_ring_kk_alat.form', '=', 'kimia klinik');
                     if ($periode == 2) {
                     $join->where('tb_ring_kk_alat.periode', '=', 2);
                     }
            })
            ->leftjoin('tb_sd_median_alat','tb_sd_median_alat.alat','=','tb_instrumen.id')
            ->where('tb_sd_median_alat.siklus', $siklus)
            ->where('tb_sd_median_alat.tipe', $tipe)
            ->where('tb_sd_median_alat.tahun', $tahun)
            ->where(function($query) use ($siklus, $periode){
                if ($siklus == 1) {
                    if ($periode == 2) {
                        $query->where('tb_sd_median_alat.periode','=','2');
                    }
                }
            })
            ->having('total_data','>=',8)
            ->where('parameter.kategori','=','kimia klinik')
            ->where('tb_instrumen.instrumen','!=','Alat Lain :')
            ->select('parameter.nama_parameter','parameter.catatan','tb_sd_median_alat.sd','tb_sd_median_alat.median','parameter.id as id_parameter','tb_ring_kk_alat.*','tb_instrumen.instrumen as nama_instrumen','tb_instrumen.id as id_instrumen', DB::raw("(
                SELECT COUNT(*) from hp_details as HPD
                INNER JOIN `hp_headers` ON `hp_headers`.`id` = `HPD`.`hp_header_id`
                WHERE
                HPD.alat = tb_instrumen.id
                AND
                `HPD`.`hasil_pemeriksaan` NOT IN (\"-\",\"0.0\",\"0\",\"0.00\",'\"_\"','\"-\"',\"_\")
                AND
                `hp_headers`.`type` = '".$tipe."'
                AND
                `hp_headers`.`siklus` = '".$siklus."'
                AND
                YEAR(`hp_headers`.`tgl_pemeriksaan`) = ".$tahun."

            ) as total_data"))
            ->get();
        // dd($sd);
        $pdf = PDF::loadview('evaluasi.kimiaklinik.ringalat.print', compact('sd','tahun','siklus','tipe','periode'))
            ->setPaper('legal', 'landscape')
            ->setwarnings(false);
        return $pdf->stream('Laporan Nilai Range per Alat.pdf');
    }

    public function kimiaringnametode(\Illuminate\Http\Request $request)
    {
        $tahun = $request->query('tahun');
        $tipe = $request->query('tipe');
        $siklus = $request->query('siklus');
        $periode = $request->query('periode');

        $sd = DB::table('metode_pemeriksaan')
            ->join('parameter','parameter.id','=','metode_pemeriksaan.parameter_id')
            ->leftJoin('tb_ring_kk_metode', function ($join) use($tipe,$siklus,$tahun,$periode) {
                $join->on('metode_pemeriksaan.id', '=', 'tb_ring_kk_metode.metode_id')
                     ->where('tb_ring_kk_metode.tipe', '=', $tipe)
                     ->where('tb_ring_kk_metode.tahun', '=', $tahun)
                     ->where('tb_ring_kk_metode.siklus', '=', $siklus)
                     ->where('tb_ring_kk_metode.form', '=', 'kimia klinik');
                if ($periode == 2) {
                    $join->where('tb_ring_kk_metode.periode', '=', $periode);
                }
            })
            ->leftjoin('tb_sd_median_metode','tb_sd_median_metode.metode','=','metode_pemeriksaan.id')
            ->where('tb_sd_median_metode.siklus', $siklus)
            ->where('tb_sd_median_metode.tipe', $tipe)
            ->where('tb_sd_median_metode.tahun', $tahun)
            ->where(function($query) use ($siklus, $periode){
                if ($siklus == 1) {
                    if ($periode == 2) {
                        $query->where('tb_sd_median_metode.periode','=','2');
                    }
                }
            })
            ->having('total_data','>=',8)
            ->where('parameter.kategori','=','kimia klinik')
            ->where('metode_pemeriksaan.metode_pemeriksaan','!=','Metode Lain :')
            ->select('parameter.nama_parameter','parameter.catatan','tb_sd_median_metode.sd','tb_sd_median_metode.median','parameter.id as id_parameter','tb_ring_kk_metode.*','metode_pemeriksaan.metode_pemeriksaan as nama_metode','metode_pemeriksaan.id as id_metode',
                        DB::raw("(
                                SELECT COUNT(*) from hp_details as HPD
                                INNER JOIN `hp_headers` ON `hp_headers`.`id` = `HPD`.`hp_header_id`
                                WHERE
                                HPD.kode_metode_pemeriksaan = metode_pemeriksaan.id
                                AND
                                `HPD`.`hasil_pemeriksaan` NOT IN (\"-\",\"0.0\",\"0\",\"0.00\",'\"_\"','\"-\"',\"_\")
                                AND
                                `hp_headers`.`type` = '".$tipe."'
                                AND
                                `hp_headers`.`siklus` = '".$siklus."'
                                AND
                                YEAR(`hp_headers`.`tgl_pemeriksaan`) = ".$tahun."

                            ) as total_data"
                        )
                    )
            ->get();
        return view('evaluasi.kimiaklinik.ringmetode.insert', compact('sd','tahun','siklus','tipe','periode'));
    }

    public function kimiaringnametodeprint(\Illuminate\Http\Request $request)
    {
        $tahun = $request->query('tahun');
        $tipe = $request->query('tipe');
        $siklus = $request->query('siklus');
        $periode = $request->query('periode');

        $sd = DB::table('metode_pemeriksaan')
            ->join('parameter','parameter.id','=','metode_pemeriksaan.parameter_id')
            ->leftJoin('tb_ring_kk_metode', function ($join) use($tipe,$siklus,$tahun,$periode) {
                $join->on('metode_pemeriksaan.id', '=', 'tb_ring_kk_metode.metode_id')
                     ->where('tb_ring_kk_metode.tipe', '=', $tipe)
                     ->where('tb_ring_kk_metode.tahun', '=', $tahun)
                     ->where('tb_ring_kk_metode.siklus', '=', $siklus)
                     ->where('tb_ring_kk_metode.form', '=', 'kimia klinik');
                if ($periode == 2) {
                    $join->where('tb_ring_kk_metode.periode', '=', $periode);
                }
            })
            ->leftjoin('tb_sd_median_metode','tb_sd_median_metode.metode','=','metode_pemeriksaan.id')
            ->where('tb_sd_median_metode.siklus', $siklus)
            ->where('tb_sd_median_metode.tipe', $tipe)
            ->where('tb_sd_median_metode.tahun', $tahun)
            ->where(function($query) use ($siklus, $periode){
                if ($siklus == 1) {
                    if ($periode == 2) {
                        $query->where('tb_sd_median_metode.periode','=','2');
                    }
                }
            })
            ->having('total_data','>=',8)
            ->where('parameter.kategori','=','kimia klinik')
            ->where('metode_pemeriksaan.metode_pemeriksaan','!=','Metode Lain :')
            ->select('parameter.nama_parameter','parameter.catatan','tb_sd_median_metode.sd','tb_sd_median_metode.median','parameter.id as id_parameter','tb_ring_kk_metode.*','metode_pemeriksaan.metode_pemeriksaan as nama_metode','metode_pemeriksaan.id as id_metode',
                        DB::raw("(
                                SELECT COUNT(*) from hp_details as HPD
                                INNER JOIN `hp_headers` ON `hp_headers`.`id` = `HPD`.`hp_header_id`
                                WHERE
                                HPD.kode_metode_pemeriksaan = metode_pemeriksaan.id
                                AND
                                `HPD`.`hasil_pemeriksaan` NOT IN (\"-\",\"0.0\",\"0\",\"0.00\",'\"_\"','\"-\"',\"_\")
                                AND
                                `hp_headers`.`type` = '".$tipe."'
                                AND
                                `hp_headers`.`siklus` = '".$siklus."'
                                AND
                                YEAR(`hp_headers`.`tgl_pemeriksaan`) = ".$tahun."

                            ) as total_data"
                        )
                    )
            ->get();
        $pdf = PDF::loadview('evaluasi.kimiaklinik.ringmetode.print', compact('sd','tahun','siklus','tipe','periode'))
            ->setPaper('legal', 'landscape')
            ->setwarnings(false);
        return $pdf->stream('Laporan Nilai Range per Metode.pdf');
    }

    public function kimiaringnasave(\Illuminate\Http\Request $request)
    {
        $input = $request->input();
        foreach($input['data'] as $key => $r){
            if(!empty($r['id_tb_ring'])){
                $save = RingKK::find($r['id_tb_ring']);
            }else{
                $save = new RingKK;
            }
            unset($r['id_tb_ring']);
            $save->tahun = $input['tahun'];
            $save->siklus = $input['siklus'];
            $save->tipe = $input['tipe'];
            $save->form = 'kimia klinik';
            foreach($r as $ak => $val){
                $save->$ak = $val;
            }
            $save->save();
        }
        Session::flash('message', 'Input range telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-ring/kimiaklinik');
    }

    public function kimiaringnasavealat(\Illuminate\Http\Request $request)
    {
        $input = $request->input();
        foreach($input['data'] as $key => $r){
            if(!empty($r['id_tb_ring'])){
                $save = RingKKAlat::find($r['id_tb_ring']);
            }else{
                $save = new RingKKAlat;
            }
            unset($r['id_tb_ring']);
            $save->tahun = $input['tahun'];
            $save->siklus = $input['siklus'];
            $save->tipe = $input['tipe'];
            $save->periode = $input['periode'];
            $save->form = 'kimia klinik';
            foreach($r as $ak => $val){
                $save->$ak = $val;
            }
            $save->save();
        }
        Session::flash('message', 'Input range telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-ring/kimiaklinik-alat');
    }

    public function kimiaringnasavemetode(\Illuminate\Http\Request $request)
    {
        $input = $request->input();
        foreach($input['data'] as $key => $r){
            if(!empty($r['id_tb_ring'])){
                $save = RingKKMetode::find($r['id_tb_ring']);
            }else{
                $save = new RingKKMetode;
            }
            unset($r['id_tb_ring']);
            $save->tahun = $input['tahun'];
            $save->siklus = $input['siklus'];
            $save->tipe = $input['tipe'];
            $save->periode = $input['periode'];
            $save->form = 'kimia klinik';
            foreach($r as $ak => $val){
                $save->$ak = $val;
            }
            $save->save();
        }
        Session::flash('message', 'Input range telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-ring/kimiaklinik-metode');
    }

    public function kimiaklinikjumlahkelompok()
    {
        return view('evaluasi.kimiaklinik.jumlahkelompok.index');
    }

    public function kimiaklinikjumlahkelompokna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $alat = DB::table('hp_details')
                ->leftjoin('parameter','parameter.id','=','hp_details.parameter_id')
                ->join('tb_instrumen', 'tb_instrumen.id', '=', 'hp_details.alat')
                ->join('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                ->leftjoin('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->select('tb_instrumen.instrumen', 'parameter.nama_parameter', DB::raw('COUNT(alat) as jumlah'))
                ->groupBy('hp_details.alat')
                ->orderBy('parameter.id','asc')
                ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                })
                ->where(function($query) use ($input){
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $input['siklus']);
                })
                ->where('parameter.kategori', '=', 'kimia klinik')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('hp_headers.type', '=', $input['tipe'])
                ->where('hp_headers.siklus', '=', $input['siklus'])
                ->where('hp_details.hasil_pemeriksaan', '!=', '0')
                ->where('hp_details.hasil_pemeriksaan', '!=', '0.0')
                ->get();

        $alatlain = DB::table('hp_details')
                ->leftjoin('parameter','parameter.id','=','hp_details.parameter_id')
                ->join('tb_instrumen', 'tb_instrumen.id', '=', 'hp_details.alat')
                ->join('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                ->leftjoin('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->select('hp_details.instrument_lain', DB::raw('COUNT(alat) as jumlah'))
                ->groupBy('hp_details.instrument_lain')
                ->orderBy('parameter.id','asc')
                ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                })
                ->where('parameter.kategori', '=', 'kimia klinik')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('hp_headers.type', '=', $input['tipe'])
                ->where('hp_headers.siklus', '=', $input['siklus'])
                ->where('tb_instrumen.instrumen', '=', 'Alat Lain :')
                ->where('hp_details.hasil_pemeriksaan', '!=', '0')
                ->where('hp_details.hasil_pemeriksaan', '!=', '0.0')
                ->get();

        $metode = DB::table('hp_details')
                ->leftjoin('parameter','parameter.id','=','hp_details.parameter_id')
                ->join('metode_pemeriksaan', 'metode_pemeriksaan.id', '=', 'hp_details.kode_metode_pemeriksaan')
                ->join('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                ->leftjoin('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->groupBy('hp_details.kode_metode_pemeriksaan')
                ->orderBy('parameter.id','asc')
                ->select('metode_pemeriksaan.metode_pemeriksaan', 'parameter.nama_parameter', DB::raw('COUNT(alat) as jmlah'))
                ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                })
                ->where('parameter.kategori', '=', 'kimia klinik')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('hp_headers.type', '=', $input['tipe'])
                ->where('hp_headers.siklus', '=', $input['siklus'])
                ->where('hp_details.hasil_pemeriksaan', '!=', '0')
                ->where('hp_details.hasil_pemeriksaan', '!=', '0.0')
                ->orderBy('parameter.id', 'asc')
                ->get();

        $metodelain = DB::table('hp_details')
                ->leftjoin('parameter','parameter.id','=','hp_details.parameter_id')
                ->join('metode_pemeriksaan', 'metode_pemeriksaan.id', '=', 'hp_details.kode_metode_pemeriksaan')
                ->join('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                ->leftjoin('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->groupBy('hp_details.kode_metode_pemeriksaan', 'hp_details.metode_lain', 'parameter.nama_parameter')
                ->orderBy('parameter.id','asc')
                ->select('metode_pemeriksaan.metode_pemeriksaan', 'parameter.nama_parameter', 'hp_details.metode_lain', DB::raw('COUNT(alat) as jmlah'))
                ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                })
                ->where('parameter.kategori', '=', 'kimia klinik')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('hp_headers.type', '=', $input['tipe'])
                ->where('hp_headers.siklus', '=', $input['siklus'])
                ->where('metode_pemeriksaan.metode_pemeriksaan', '=', 'Metode lain :')
                ->where('hp_details.hasil_pemeriksaan', '!=', '0')
                ->where('hp_details.hasil_pemeriksaan', '!=', '0.0')
                ->get();

        // dd($alatlain);
        if ($input['proses'] == 'Proses') {
            Excel::create('Laporan Jumlah Kelompok Peserta Kimia Klinik', function($excel) use ($metode, $metodelain, $alat, $alatlain, $input) {
                $excel->sheet('Alat', function($sheet) use ($alat, $input) {
                    $sheet->loadView('evaluasi.kimiaklinik.jumlahkelompok.viewalat', array('alat'=>$alat, 'input'=>$input) );
                });
                $excel->sheet('Metode', function($sheet) use ($metode, $input) {
                    $sheet->loadView('evaluasi.kimiaklinik.jumlahkelompok.viewmetode', array('metode'=>$metode, 'input'=>$input) );
                });
                $excel->sheet('Metode Lain', function($sheet) use ($metodelain, $input) {
                    $sheet->loadView('evaluasi.kimiaklinik.jumlahkelompok.viewmetodelain', array('metodelain'=>$metodelain, 'input'=>$input) );
                });
                $excel->sheet('Alat Lain', function($sheet) use ($alatlain, $input) {
                    $sheet->loadView('evaluasi.kimiaklinik.jumlahkelompok.viewalatlain', array('alatlain'=>$alatlain, 'input'=>$input) );
                });
            })->download('xls');
        }else{
            $pdf = PDF::loadview('evaluasi.kimiaklinik.jumlahkelompok.cetak', compact('alat', 'input', 'metode','metodelain','alatlain'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            return $pdf->stream('Laporan Jumlah Kelompok Peserta Kimia Klinik.pdf');
        }
    }

    public function kimiaklinikmetodelaporan()
    {
        return view('evaluasi.kimiaklinik.laporanmetode.index');
    }

    public function kimiaklinikmetodelaporanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameterna = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                })
                ->where(function($query) use ($input){
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $input['siklus']);
                })
                ->where('tb_registrasi.bidang', '=', '2')
                ->where('hp_headers.siklus', $input['siklus'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $input['tahun'])
                ->where('hp_headers.type', $input['tipe'])
                ->select('hp_headers.*', 'tb_registrasi.updated_at')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                            ->where('hp_details.hp_header_id', $val->id)
                            ->where('hp_details.parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data);
        Excel::create('Laporan Hasil Peserta Metode Kimia Klinik', function($excel) use ($data, $parameterna) {
            $excel->sheet('Laporan', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('evaluasi.kimiaklinik.laporanmetode.view', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
        })->download('xls');
    }

    public function kimiakliniksdmedian()
    {
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'kimia klinik')
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->groupBy('tb_sd_median.tahun', 'tb_sd_median.siklus', 'tb_sd_median.tipe', 'tb_sd_median.periode')
            ->orderBy('tb_sd_median.siklus', 'desc')
            ->orderBy('tb_sd_median.id', 'desc')
            ->paginate(8);

        return view('evaluasi.kimiaklinik.sdmedian.index', compact('parameter','sd'));
    }

    public function kimiakliniksdmedianinsert()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view('evaluasi.kimiaklinik.sdmedian.view', compact('parameter','sd'));
    }

    public function kimiakliniksdmedianna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $data = DB::table('tb_sd_median')
                ->where('form', '=', 'kimia klinik')
                ->where('tb_sd_median.tahun', $input['tahun'])
                ->where('tb_sd_median.siklus', $input['siklus'])
                ->where('tb_sd_median.tipe', $input['tipe'])
                ->where('tb_sd_median.periode', $input['periode'])
                ->count();
        // dd($data);
        if ($data == 0) {
            // dd($input);
            $i = 0;
            foreach ($input['parameter'] as $parameter) {
                $Savedata = new SdMedian;
                $Savedata->siklus = $input['siklus'];
                $Savedata->tahun = $input['tahun'];
                $Savedata->tipe = $input['tipe'];
                $Savedata->periode = $input['periode'];
                $Savedata->parameter = $input['parameter'][$i];
                $Savedata->sd = $input['sd'][$i];
                $Savedata->median = $input['median'][$i];
                $Savedata->form = 'kimia klinik';
                $Savedata->save();
                $i++;
            }

            Session::flash('message', 'Input SD dan Median telah berhasil!');
            Session::flash('alert-class', 'alert-success');
            return redirect('evaluasi/input-sd-median/kimiaklinik');
        }else{
            Session::flash('message', 'Input SD dan Median Gagal!');
            Session::flash('alert-class', 'alert-danger');
            // dd($Savedata);
            return redirect('evaluasi/input-sd-median/kimiaklinik');
        }
    }

    public function kimiakliniksdmedianprint($id)
    {
        $edit = DB::table('tb_sd_median')->where('tb_sd_median.id', $id)->first();
        // dd($edit);
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'kimia klinik')
            ->where('tb_sd_median.tahun', $edit->tahun)
            ->where('tb_sd_median.siklus', $edit->siklus)
            ->where('tb_sd_median.tipe', $edit->tipe)
            ->where('tb_sd_median.periode', $edit->periode)
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->get();
        // dd($sd);
        $pdf = PDF::loadview('evaluasi.kimiaklinik.sdmedian.print', compact('edit', 'sd'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        return $pdf->stream('SD dan Median per Parameter.pdf');
    }

    public function kimiakliniksdmedianedit($id)
    {
        $edit = DB::table('tb_sd_median')->where('tb_sd_median.id', $id)->first();
        // dd($edit);
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'kimia klinik')
            ->where('tb_sd_median.tahun', $edit->tahun)
            ->where('tb_sd_median.siklus', $edit->siklus)
            ->where('tb_sd_median.tipe', $edit->tipe)
            ->where('tb_sd_median.periode', $edit->periode)
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->get();
        // dd($sd);
        return view('evaluasi.kimiaklinik.sdmedian.edit', compact('edit','sd'));
    }

    public function kimiakliniksdmedianupdate(\Illuminate\Http\Request $request)
    {
        $i = 0;
        foreach ($request->parameter as $parameter) {
            $Update['siklus'] = $request->siklus;
            $Update['tahun'] = $request->tahun;
            $Update['tipe'] = $request->tipe;
            $Update['parameter'] = $request->parameter[$i];
            $Update['sd'] = $request->sd[$i];
            $Update['median'] = $request->median[$i];

            SdMedian::where('id',$request->id[$i])->update($Update);
            $i++;
        }
        // dd($input);
        Session::flash('message', 'Edit SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median/kimiaklinik');
    }

    public function kimiakliniksdmediandelet($id){
        $edit = DB::table('tb_sd_median')->where('tb_sd_median.id', $id)->first();

        DB::table('tb_sd_median')->where('tahun', $edit->tahun)->where('siklus', $edit->siklus)->where('tipe', $edit->tipe)->where('form', '=', 'kimia klinik')->delete();

        Session::flash('message', 'Hapus Data SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-warning');

        return redirect('evaluasi/input-sd-median/kimiaklinik');
    }

    public function kimiakliniksdmedianalat()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        $sd = DB::table('tb_sd_median_alat')
            ->where('form', '=', 'kimia klinik')
            ->select('tb_sd_median_alat.*')
            ->groupBy('tahun', 'tipe', 'siklus','periode')
            ->orderBy('tb_sd_median_alat.siklus', 'desc')
            ->orderBy('tb_sd_median_alat.id', 'desc')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.kimiaklinik.sdmedianalat.index', compact('parameter','sd'));
    }

    public function kimiakliniksdmedianalatinsert(\Illuminate\Http\Request $request)
    {
        $siklus = $request->siklus;
        $tahun = $request->tahun;
        $tipe = $request->tipe;
        $periode = $request->periode;
        if ($periode == 2 && $siklus == 1) {
            $alat = DB::table('tb_instrumen')
                    ->select('tb_instrumen.id', 'instrumen', 'id_parameter','nama_parameter','tb_sd_median_alat.sd',DB::raw('tb_sd_median_alat.id id_sd_med'),'tb_sd_median_alat.median', DB::raw("(
                                SELECT COUNT(*) from hp_details as HPD
                                INNER JOIN `hp_headers` ON `hp_headers`.`id` = `HPD`.`hp_header_id`
                                INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `hp_headers`.`id_registrasi`
                                WHERE
                                HPD.alat = tb_instrumen.id
                                AND 
                                tb_registrasi.periode = 2
                                AND
                                `HPD`.`hasil_pemeriksaan` NOT IN (\"-\",\"0.0\",\"0\",\"0.00\",'\"_\"','\"-\"',\"_\")
                                AND
                                `hp_headers`.`type` = '".$tipe."'
                                AND
                                `hp_headers`.`siklus` = '".$siklus."'
                                AND
                                YEAR(`hp_headers`.`tgl_pemeriksaan`) = ".$tahun."

                            ) as total_data"))
                    ->join('parameter','parameter.id','=','tb_instrumen.id_parameter')
                    ->leftJoin('tb_sd_median_alat', function ($join) use($tipe,$siklus,$tahun) {
                        $join->on('tb_instrumen.id', '=', 'tb_sd_median_alat.alat')
                             ->where('tb_sd_median_alat.tipe', '=', $tipe)
                             ->where('tb_sd_median_alat.tahun', '=', $tahun)
                             ->where('tb_sd_median_alat.siklus', '=', $siklus)
                             ->where('tb_sd_median_alat.form', '=', 'kimia klinik');
                    })
                    ->where('tb_sd_median_alat.periode', '=', $periode)
                    ->where('parameter.kategori','=','kimia klinik')
                    ->where('instrumen','!=','Alat Lain :')
                    ->havingRaw('total_data >= 8')
                    ->orderBy('parameter.id', 'asc')
                    ->orderBy('instrumen', 'asc')
                    ->get();
        }else{
            $alat = DB::table('tb_instrumen')
                    ->select('tb_instrumen.id', 'instrumen', 'id_parameter','nama_parameter','tb_sd_median_alat.sd',DB::raw('tb_sd_median_alat.id id_sd_med'),'tb_sd_median_alat.median', DB::raw("(
                                SELECT COUNT(*) from hp_details as HPD
                                INNER JOIN `hp_headers` ON `hp_headers`.`id` = `HPD`.`hp_header_id`
                                WHERE
                                HPD.alat = tb_instrumen.id
                                AND
                                `HPD`.`hasil_pemeriksaan` NOT IN (\"-\",\"0.0\",\"0\",\"0.00\",'\"_\"','\"-\"',\"_\")
                                AND
                                `hp_headers`.`type` = '".$tipe."'
                                AND
                                `hp_headers`.`siklus` = '".$siklus."'
                                AND
                                YEAR(`hp_headers`.`tgl_pemeriksaan`) = ".$tahun."

                            ) as total_data"))
                    ->join('parameter','parameter.id','=','tb_instrumen.id_parameter')
                    ->leftJoin('tb_sd_median_alat', function ($join) use($tipe,$siklus,$tahun) {
                        $join->on('tb_instrumen.id', '=', 'tb_sd_median_alat.alat')
                             ->where('tb_sd_median_alat.tipe', '=', $tipe)
                             ->where('tb_sd_median_alat.tahun', '=', $tahun)
                             ->where('tb_sd_median_alat.siklus', '=', $siklus)
                             ->where('tb_sd_median_alat.form', '=', 'kimia klinik');
                    })
                    ->where('parameter.kategori','=','kimia klinik')
                    ->where('instrumen','!=','Alat Lain :')
                    ->havingRaw('total_data >= 8')
                    ->orderBy('parameter.id', 'asc')
                    ->orderBy('instrumen', 'asc')
                    ->get();
        }
            // dd($alat);
        return view('evaluasi.kimiaklinik.sdmedianalat.view', compact('alat', 'siklus', 'tahun', 'tipe','periode'));
    }

    public function kimiakliniksdmedianalatna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $i = 0;
        foreach ($input['alat'] as $alat) {
            if(!empty($input['id_sd_med'][$i])){
                $Savedata = SdMedianAlat::find($input['id_sd_med'][$i]);
            }else{
                $Savedata = new SdMedianAlat;
            }

            $Savedata->parameter = $input['parameter'][$i];
            $Savedata->siklus = $input['siklus'];
            $Savedata->tahun = $input['tahun'];
            $Savedata->tipe = $input['tipe'];
            $Savedata->periode = $input['periode'];
            $Savedata->alat = $input['alat'][$i];
            $Savedata->sd = $input['sd'][$i];
            $Savedata->median = $input['median'][$i];
            $Savedata->form = 'kimia klinik';
            $Savedata->save();

            $i++;
        }
        Session::flash('message', 'Input SD dan Median Alat / Instrument telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-sd-median-alat/kimiaklinik');
    }

    public function kimiakliniksdmedianalatprint(\Illuminate\Http\Request $request,$id)
    {
        $siklus = $request->siklus;
        $tahun = $request->tahun;
        $tipe = $request->tipe;
        $periode = $request->periode;
        if ($periode != NULL) {
            $sqlperiode = "AND tb_registrasi.periode = 2";
        }else{
            $sqlperiode = "";
        }
        $alat = DB::table('tb_instrumen')
                ->select('tb_instrumen.id', 'instrumen', 'id_parameter','nama_parameter','tb_sd_median_alat.sd',DB::raw('tb_sd_median_alat.id id_sd_med'),'tb_sd_median_alat.median', DB::raw("(
                            SELECT COUNT(*) from hp_details as HPD
                            INNER JOIN `hp_headers` ON `hp_headers`.`id` = `HPD`.`hp_header_id`
                            INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `hp_headers`.`id_registrasi`
                            WHERE
                            HPD.alat = tb_instrumen.id
                            AND
                            `HPD`.`hasil_pemeriksaan` NOT IN (\"-\",\"0.0\",\"0\",\"0.00\",'\"_\"','\"-\"',\"_\")
                            AND
                            `hp_headers`.`type` = '".$tipe."'
                            ".$sqlperiode."
                            AND
                            `hp_headers`.`siklus` = '".$siklus."'
                            AND
                            YEAR(`hp_headers`.`tgl_pemeriksaan`) = ".$tahun."

                        ) as total_data"))
                ->join('parameter','parameter.id','=','tb_instrumen.id_parameter')
                ->leftJoin('tb_sd_median_alat', function ($join) use($tipe,$siklus,$tahun) {
                    $join->on('tb_instrumen.id', '=', 'tb_sd_median_alat.alat')
                         ->where('tb_sd_median_alat.tipe', '=', $tipe)
                         ->where('tb_sd_median_alat.tahun', '=', $tahun)
                         ->where('tb_sd_median_alat.siklus', '=', $siklus)
                         ->where('tb_sd_median_alat.form', '=', 'kimia klinik');
                })
                ->where('tb_sd_median_alat.periode', '=', $periode)
                ->where('parameter.kategori','=','kimia klinik')
                ->where('instrumen','!=','Alat Lain :')
                ->havingRaw('total_data >= 8')
                ->orderBy('parameter.id', 'asc')
                ->orderBy('instrumen', 'asc')
                ->get();
            // dd($sd);
        $pdf = PDF::loadview('evaluasi.kimiaklinik.sdmedianalat.print', compact('request','alat'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        return $pdf->stream('SD dan Median per Parameter.pdf');
    }

    public function kimiakliniksdmedianalatedit($id)
    {
        $edit = DB::table('tb_sd_median_alat')->where('id', $id)->first();
        // dd($edit);
        $sd = DB::table('tb_sd_median_alat')
            ->join('tb_instrumen','tb_instrumen.id','=','tb_sd_median_alat.alat')
            ->where('form', '=', 'kimia klinik')
            ->where('parameter', $edit->parameter)
            ->where('tahun', $edit->tahun)
            ->where('siklus', $edit->siklus)
            ->where('tipe', $edit->tipe)
            ->select('tb_sd_median_alat.*', 'tb_instrumen.instrumen')
            ->get();
            // dd($sd);
        return view('evaluasi.kimiaklinik.sdmedianalat.edit', compact('edit','sd'));
    }

    public function kimiakliniksdmedianalatupdate(\Illuminate\Http\Request $request, $id)
    {
        $i = 0;
        foreach ($request->alat as $alat) {
            $Update['parameter'] = $request->parameter;
            $Update['siklus'] = $request->siklus;
            $Update['tahun'] = $request->tahun;
            $Update['tipe'] = $request->tipe;
            $Update['alat'] = $request->alat[$i];
            $Update['sd'] = $request->sd[$i];
            $Update['median'] = $request->median[$i];
            SdMedianAlat::where('id',$request->id[$i])->update($Update);
            $i++;
        }
        // dd($input);
        Session::flash('message', 'Edit SD dan Median Alat / Instrument telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median-alat/kimiaklinik');
    }

    public function kimiakliniksdmedianalatdelet(\Illuminate\Http\Request $request,$id){
        $siklus = $request->siklus;
        $tahun = $request->tahun;
        $tipe = $request->tipe;

        DB::table('tb_sd_median_alat')
        ->where('tahun', $request->tahun)
        ->where('siklus', $request->siklus)
        ->where('tipe', $request->tipe)
        ->where('form', '=', 'kimia klinik')
        ->delete();

        Session::flash('message', 'Hapus Data SD dan Median Alat / Instrument telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median-alat/kimiaklinik');
    }

    public function kimiakliniksdmedianmetode(\Illuminate\Http\Request $request)
    {
        $sd = DB::table('tb_sd_median_metode')
            ->where('form', '=', 'kimia klinik')
            ->select('tb_sd_median_metode.*')
            ->groupBy('tahun', 'tipe', 'siklus','periode')
            ->orderBy('tb_sd_median_metode.siklus', 'desc')
            ->orderBy('tb_sd_median_metode.id', 'desc')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.kimiaklinik.sdmedianmetode.index', compact('sd'));
    }

    public function kimiakliniksdmedianmetodeinsert(\Illuminate\Http\Request $request)
    {
        $siklus = $request->siklus;
        $tahun = $request->tahun;
        $tipe = $request->tipe;
        $periode = $request->periode;
        if ($periode == 2) {
            $metode = DB::table('metode_pemeriksaan')
                    ->select('metode_pemeriksaan.id', 'metode_pemeriksaan.metode_pemeriksaan', 'parameter_id','nama_parameter','tb_sd_median_metode.sd','tb_sd_median_metode.id as id_sd_med','tb_sd_median_metode.median',
                    DB::raw("(
                            SELECT COUNT(*) from hp_details as HPD
                            INNER JOIN `hp_headers` ON `hp_headers`.`id` = `HPD`.`hp_header_id`
                            INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `hp_headers`.`id_registrasi`
                            WHERE
                            HPD.kode_metode_pemeriksaan = metode_pemeriksaan.id
                            AND
                            tb_registrasi.periode = 2
                            AND
                            `HPD`.`hasil_pemeriksaan` NOT IN (\"-\",\"0.0\",\"0\",\"0.00\",'\"_\"','\"-\"',\"_\")
                            AND
                            `hp_headers`.`type` = '".$tipe."'
                            AND
                            `hp_headers`.`siklus` = '".$siklus."'
                            AND
                            YEAR(`hp_headers`.`tgl_pemeriksaan`) = ".$tahun."
                        ) as total_data")
                    )
                    ->join('parameter','parameter.id','=','metode_pemeriksaan.parameter_id')
                    ->leftJoin('tb_sd_median_metode', function ($join) use($tipe,$siklus,$tahun) {
                        $join->on('metode_pemeriksaan.id', '=', 'tb_sd_median_metode.metode')
                             ->where('tb_sd_median_metode.tipe', '=', $tipe)
                             ->where('tb_sd_median_metode.tahun', '=', $tahun)
                             ->where('tb_sd_median_metode.siklus', '=', $siklus)
                            ->where('tb_sd_median_metode.periode', '=', '2')
                             ->where('tb_sd_median_metode.form', '=', 'kimia klinik');
                    })
                    ->where('parameter.kategori','=','kimia klinik')
                    ->where('metode_pemeriksaan.metode_pemeriksaan','!=','Metode lain :')
                    ->having('total_data','>=',8)
                    ->orderBy('parameter.id', 'asc')
                    ->orderBy('metode_pemeriksaan.metode_pemeriksaan', 'asc')
                    ->get();
            // dd($metode);
        }else{
            $metode = DB::table('metode_pemeriksaan')
                    ->select('metode_pemeriksaan.id', 'metode_pemeriksaan.metode_pemeriksaan', 'parameter_id','nama_parameter','tb_sd_median_metode.sd',DB::raw('tb_sd_median_metode.id id_sd_med'),'tb_sd_median_metode.median',
                    DB::raw("(
                            SELECT COUNT(*) from hp_details as HPD
                            INNER JOIN `hp_headers` ON `hp_headers`.`id` = `HPD`.`hp_header_id`
                            WHERE
                            HPD.kode_metode_pemeriksaan = metode_pemeriksaan.id
                            AND
                            `HPD`.`hasil_pemeriksaan` NOT IN (\"-\",\"0.0\",\"0\",\"0.00\",'\"_\"','\"-\"',\"_\")
                            AND
                            `hp_headers`.`type` = '".$tipe."'
                            AND
                            `hp_headers`.`siklus` = '".$siklus."'
                            AND
                            YEAR(`hp_headers`.`tgl_pemeriksaan`) = ".$tahun."
                        ) as total_data")
                    )
                    ->join('parameter','parameter.id','=','metode_pemeriksaan.parameter_id')
                    ->leftJoin('tb_sd_median_metode', function ($join) use($tipe,$siklus,$tahun) {
                        $join->on('metode_pemeriksaan.id', '=', 'tb_sd_median_metode.metode')
                             ->where('tb_sd_median_metode.tipe', '=', $tipe)
                             ->where('tb_sd_median_metode.tahun', '=', $tahun)
                             ->where('tb_sd_median_metode.siklus', '=', $siklus)
                             ->WhereNull('tb_sd_median_metode.periode')
                             ->where('tb_sd_median_metode.form', '=', 'kimia klinik');
                    })
                    ->where('parameter.kategori','=','kimia klinik')
                    ->where('metode_pemeriksaan.metode_pemeriksaan','!=','Metode lain :')
                    ->having('total_data','>=',8)
                    ->orderBy('parameter.id', 'asc')
                    ->orderBy('metode_pemeriksaan.metode_pemeriksaan', 'asc')
                    ->get();
        }
        return view('evaluasi.kimiaklinik.sdmedianmetode.view', compact('metode', 'siklus', 'tahun', 'tipe','periode'));
    }

    public function kimiakliniksdmedianmetodena(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('tb_sd_median_metode')
                ->where('form', '=', 'kimia klinik')
                ->where('tahun', $input['tahun'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $input['tipe'])
                ->where('periode', $input['periode'])
                ->where('parameter', $input['parameter'])
                ->count();
        $i = 0;
        foreach ($input['metode'] as $metode) {
            if(!empty($input['id_sd_med'][$i])){
                $Savedata = SdMedianMetode::find($input['id_sd_med'][$i]);
            }else{
                $Savedata = new SdMedianMetode;
            }
            $Savedata->siklus = $input['siklus'];
            $Savedata->tahun = $input['tahun'];
            $Savedata->tipe = $input['tipe'];
            $Savedata->periode = $input['periode'];
            $Savedata->parameter = $input['parameter'][$i];
            $Savedata->metode = $input['metode'][$i];
            $Savedata->sd = $input['sd'][$i];
            $Savedata->median = $input['median'][$i];
            $Savedata->form = 'kimia klinik';
            $Savedata->save();

            $i++;
        }
        Session::flash('message', 'Input SD dan Median Metode telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-sd-median-metode/kimiaklinik');

    }

    public function kimiakliniksdmedianmetodeprint(\Illuminate\Http\Request $request,$id)
    {
        $siklus = $request->siklus;
        $tahun = $request->tahun;
        $tipe = $request->tipe;
        $periode = $request->periode;
        if ($periode != NULL) {
            $sqlperiode = "AND tb_registrasi.periode = 2";
        }else{
            $sqlperiode = "";
        }

        $sd = DB::table('metode_pemeriksaan')
                ->select('metode_pemeriksaan.id', 'metode_pemeriksaan.metode_pemeriksaan', 'parameter_id','nama_parameter','tb_sd_median_metode.sd',DB::raw('tb_sd_median_metode.id id_sd_med'),'tb_sd_median_metode.median',
                DB::raw("(
                        SELECT COUNT(*) from hp_details as HPD
                        INNER JOIN `hp_headers` ON `hp_headers`.`id` = `HPD`.`hp_header_id`
                        INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `hp_headers`.`id_registrasi`
                        WHERE
                        HPD.kode_metode_pemeriksaan = metode_pemeriksaan.id
                        AND
                        `HPD`.`hasil_pemeriksaan` NOT IN (\"-\",\"0.0\",\"0\",\"0.00\",'\"_\"','\"-\"',\"_\")
                        AND
                        `hp_headers`.`type` = '".$tipe."'
                        AND
                        `hp_headers`.`siklus` = '".$siklus."'
                        ".$sqlperiode."
                        AND
                        YEAR(`hp_headers`.`tgl_pemeriksaan`) = ".$tahun."

                    ) as total_data")
                )
                ->join('parameter','parameter.id','=','metode_pemeriksaan.parameter_id')
                ->leftJoin('tb_sd_median_metode', function ($join) use($tipe,$siklus,$tahun) {
                    $join->on('metode_pemeriksaan.id', '=', 'tb_sd_median_metode.metode')
                         ->where('tb_sd_median_metode.tipe', '=', $tipe)
                         ->where('tb_sd_median_metode.tahun', '=', $tahun)
                         ->where('tb_sd_median_metode.siklus', '=', $siklus)
                         ->where('tb_sd_median_metode.form', '=', 'kimia klinik');
                })
                ->where('tb_sd_median_metode.periode', '=', $periode)
                ->where('parameter.kategori','=','kimia klinik')
                ->where('metode_pemeriksaan.metode_pemeriksaan','!=','Metode lain :')
                ->having('total_data','>=',8)
                ->orderBy('parameter.id', 'asc')
                ->orderBy('metode_pemeriksaan.metode_pemeriksaan', 'asc')
                ->get();
        // dd($sd);
        $pdf = PDF::loadview('evaluasi.kimiaklinik.sdmedianmetode.print', compact('request', 'sd'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        return $pdf->stream('SD dan Median per Metode.pdf');
    }

    public function kimiakliniksdmedianmetodeedit($id)
    {
        $edit = DB::table('tb_sd_median_metode')->where('id', $id)->first();
        // dd($edit);
        $sd = DB::table('tb_sd_median_metode')
            ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','tb_sd_median_metode.metode')
            ->join('parameter','parameter.id','=','tb_sd_median_metode.parameter')
            ->where('form', '=', 'kimia klinik')
            ->where('tahun', $edit->tahun)
            ->where('siklus', $edit->siklus)
            ->where('tipe', $edit->tipe)
            ->select('tb_sd_median_metode.*', 'metode_pemeriksaan.metode_pemeriksaan', 'parameter.nama_parameter')
            ->get();
            // dd($sd);
        return view('evaluasi.kimiaklinik.sdmedianmetode.edit', compact('edit','sd'));
    }

    public function kimiakliniksdmedianmetodeupdate(\Illuminate\Http\Request $request, $id)
    {
        $i = 0;
        foreach ($request->metode as $metode) {
            $Update['siklus'] = $request->siklus;
            $Update['tahun'] = $request->tahun;
            $Update['tipe'] = $request->tipe;
            $Update['parameter'] = $request->parameter[$i];
            $Update['metode'] = $request->metode[$i];
            $Update['sd'] = $request->sd[$i];
            $Update['median'] = $request->median[$i];
            SdMedianMetode::where('id',$request->id[$i])->update($Update);
            $i++;
        }
        // dd($input);
        Session::flash('message', 'Edit SD dan Median Metode telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median-metode/kimiaklinik');
    }

    public function kimiakliniksdmedianmetodedelet($id){

        $edit = DB::table('tb_sd_median_metode')->where('tb_sd_median_metode.id', $id)->first();

        DB::table('tb_sd_median_metode')
        ->where('tahun', $edit->tahun)
        ->where('siklus', $edit->siklus)
        ->where('tipe', $edit->tipe)
        ->where('form', '=', 'kimia klinik')
        ->delete();

        Session::flash('message', 'Hapus Data SD dan Median Metode telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median-metode/kimiaklinik');
    }

    public function hematologisdmedianmetode()
    {
        $sd = DB::table('tb_sd_median_metode')
            ->join('parameter','parameter.id','=','tb_sd_median_metode.parameter')
            ->where('form', '=', 'hematologi')
            ->orderBy('tb_sd_median_metode.id', 'desc')
            ->select('tb_sd_median_metode.*', 'parameter.nama_parameter')
            ->groupBy('tahun','tipe','siklus','periode')
            ->orderBy('parameter.bagian', 'ASC')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.hematologi.sdmedianmetode.index', compact('sd'));
    }

    public function hematologisdmedianmetodeinsert(\Illuminate\Http\Request $request)
    {
        $metode = db::table('parameter')
                ->join('metode_pemeriksaan', 'metode_pemeriksaan.parameter_id','=','parameter.id')
                ->where('kategori', '=', 'hematologi')
                ->select('metode_pemeriksaan.*','parameter.nama_parameter')
                ->orderBy('parameter.id','asc')
                ->get();
            // dd($sd);
        return view('evaluasi.hematologi.sdmedianmetode.view', compact('metode'));
    }

    public function hematologisdmedianmetodena(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('tb_sd_median_metode')
                ->where('form', '=', 'hematologi')
                ->where('tahun', $input['tahun'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $input['tipe'])
                ->where('periode', $input['periode'])
                ->where('parameter', $input['parameter'])
                ->count();
        if ($data == 0) {
            $i = 0;
            foreach ($input['metode'] as $metode) {
                $Savedata = new SdMedianMetode;
                $Savedata->siklus = $input['siklus'];
                $Savedata->tahun = $input['tahun'];
                $Savedata->tipe = $input['tipe'];
                $Savedata->periode = $input['periode'];
                $Savedata->parameter = $input['parameter'][$i];
                $Savedata->metode = $input['metode'][$i];
                $Savedata->sd = $input['sd'][$i];
                $Savedata->median = $input['median'][$i];
                $Savedata->form = 'hematologi';
                $Savedata->save();

                $i++;
            }
            Session::flash('message', 'Input SD dan Median Metode telah berhasil!');
            Session::flash('alert-class', 'alert-success');
            // dd($Savedata);
            return redirect('evaluasi/input-sd-median-metode/hematologi');
        }else{
            Session::flash('message', 'Input SD dan Median Metode Gagal!');
            Session::flash('alert-class', 'alert-danger');
            // dd($Savedata);
            return redirect('evaluasi/input-sd-median-metode/hematologi');
        }
    }

    public function hematologisdmedianmetodeprint($id)
    {
        $edit = DB::table('tb_sd_median_metode')->where('id', $id)->first();
        // dd($edit);
        $sd = DB::table('tb_sd_median_metode')
            ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','tb_sd_median_metode.metode')
            ->join('parameter','parameter.id','=','tb_sd_median_metode.parameter')
            ->where('form', '=', 'hematologi')
            ->where('tahun', $edit->tahun)
            ->where('siklus', $edit->siklus)
            ->where('tipe', $edit->tipe)
            ->select('tb_sd_median_metode.*', 'metode_pemeriksaan.metode_pemeriksaan', 'parameter.nama_parameter')
            ->get();
            // dd($sd);
        $pdf = PDF::loadview('evaluasi.hematologi.sdmedianmetode.print', compact('edit', 'sd'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        return $pdf->stream('SD dan Median per Metode.pdf');
    }

    public function hematologisdmedianmetodeedit($id)
    {
        $edit = DB::table('tb_sd_median_metode')->where('id', $id)->first();
        // dd($edit);
        $sd = DB::table('tb_sd_median_metode')
            ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','tb_sd_median_metode.metode')
            ->join('parameter','parameter.id','=','tb_sd_median_metode.parameter')
            ->where('form', '=', 'hematologi')
            ->where('tahun', $edit->tahun)
            ->where('siklus', $edit->siklus)
            ->where('tipe', $edit->tipe)
            ->where('periode', $edit->periode)
            ->select('tb_sd_median_metode.*', 'metode_pemeriksaan.metode_pemeriksaan', 'parameter.nama_parameter')
            ->orderBy('parameter.bagian','asc')
            ->get();
            // dd($sd);
        return view('evaluasi.hematologi.sdmedianmetode.edit', compact('edit','sd'));
    }

    public function hematologisdmedianmetodeupdate(\Illuminate\Http\Request $request, $id)
    {
        $i = 0;
        foreach ($request->metode as $metode) {
            $Update['siklus'] = $request->siklus;
            $Update['tahun'] = $request->tahun;
            $Update['tipe'] = $request->tipe;
            $Update['parameter'] = $request->parameter[$i];
            $Update['metode'] = $request->metode[$i];
            $Update['sd'] = $request->sd[$i];
            $Update['median'] = $request->median[$i];
            SdMedianMetode::where('id',$request->id[$i])->update($Update);
            $i++;
        }
        // dd($input);
        Session::flash('message', 'Edit SD dan Median Metode telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median-metode/hematologi');
    }

    public function hematologisdmedianmetodedelet($id){

        $edit = DB::table('tb_sd_median_metode')->where('tb_sd_median_metode.id', $id)->first();

        DB::table('tb_sd_median_metode')
        ->where('tahun', $edit->tahun)
        ->where('siklus', $edit->siklus)
        ->where('tipe', $edit->tipe)
        ->where('form', '=', 'hematologi')
        ->delete();

        Session::flash('message', 'Hapus Data SD dan Median Metode telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median-metode/hematologi');
    }

    public function perusahaankimiaklinik(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.kode_lebpes'), 'tb_registrasi.id as idregistrasi')
                    ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang','=','2')
                    ->where('tb_registrasi.status','>=','2')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                    ->where(function($query) use ($siklus){
                        $query->where('tb_registrasi.siklus', '12')
                            ->orwhere('tb_registrasi.siklus', $siklus);
                    })
                    ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = db::table('tb_zscore')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->orderBy('type', 'desc')
                                ->orderBy('id', 'asc')
                                ->first();
                    return "".'<a href="kimiaklinik/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                Proses
                                            </button>
                                        </a>';
                ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'a')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'b')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib'])
            ->make(true);
        }

        $jobPending = JobsEvaluasi::where('name','=','evaluasi-kimia')->where('status','=',0)->first();
        $showAllertPending = false;
        $showAllertSuccess = false;
        $showForm = true;
        if(!empty($jobPending)){
            $showAllertPending = true;
            $showAllertSuccess = false;
            $showForm = false;
        }else{
            $jobSuccess = JobsEvaluasi::where('name','=','evaluasi-kimia')->where('status','=',1)->where('isread','=',0)->first();
            if(!empty($jobSuccess)){
                $showAllertPending = false;
                $showAllertSuccess = true;
                $showForm = true;
                JobsEvaluasi::where('name','=','evaluasi-kimia')->where('status','=',1)->where('isread','=',0)->update(['isread'=>1]);
            }else{
                $showForm = true;
            }
        }

        return view('evaluasi.kimiaklinik.zscore.perusahaan', compact('showAllertPending', 'showAllertSuccess', 'showForm'));
    }

    public function dataperusahaankimiaklinik(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '2')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        // dd($data);
        return view('evaluasi.kimiaklinik.zscore.dataperusahaan', compact('data','siklus'));
    }


    public function kimiaairlaporan()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia air')->get();
        return view('evaluasi.kimiaair.laporanhasilpeserta.index', compact('parameter'));
    }

    public function kimiaairlaporanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameterna = DB::table('parameter')->where('kategori', 'kimia air')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('tb_registrasi.bidang', '=', '11')
                ->where('hp_headers.siklus', $input['siklus'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $input['tahun'])
                ->select('hp_headers.*')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'kimia air')->orderBy('id','asc')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->where('hp_header_id', $val->id)
                            ->where('parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data);
        Excel::create('Laporan Hasil Peserta Per Parameter Kimia Air', function($excel) use ($data, $parameterna) {
            $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('evaluasi.kimiaair.laporanhasilpeserta.view', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
        })->download('xls');
    }

    public function kimiaairsdmedian()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia air')->get();
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'kimia air')
            ->orderBy('tb_sd_median.id', 'desc')
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.kimiaair.sdmedian.index', compact('parameter','sd'));
    }

    public function kimiaairsdmedianna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $Savedata = new SdMedian;
        $Savedata->parameter = $input['parameter'];
        $Savedata->siklus = $input['siklus'];
        $Savedata->tahun = $input['tahun'];
        $Savedata->sd = $input['sd'];
        $Savedata->median = $input['median'];
        $Savedata->form = 'kimia air';
        $Savedata->save();

        Session::flash('message', 'Input SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-sd-median/kimiaair');
    }

    public function kimiaairsdmedianedit($id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia air')->get();
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'kimia air')
            ->where('tb_sd_median.id', $id)
            ->orderBy('tb_sd_median.id', 'desc')
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->first();
            // dd($sd);
        return view('evaluasi.kimiaair.sdmedian.view', compact('parameter','sd'));
    }

    public function kimiaairsdmedianupdate(\Illuminate\Http\Request $request, $id)
    {
        $Update['parameter'] = $request->parameter;
        $Update['siklus'] = $request->siklus;
        $Update['tahun'] = $request->tahun;
        $Update['tipe'] = $request->tipe;
        $Update['sd'] = $request->sd;
        $Update['median'] = $request->median;

        SdMedian::where('id',$id)->update($Update);
        // dd($input);
        Session::flash('message', 'Edit SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median/kimiaair');
    }

    public function kimiaairsdmediandelet($id){
        Session::flash('message', 'Hapus Data SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        SdMedian::find($id)->delete();
        return redirect('evaluasi/input-sd-median/kimiaair');
    }

    public function perusahaankimiaair(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes,tb_registrasi.id as idregistrasi'))
                    ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                    ->where('tb_registrasi.bidang','=','11')
                    ->where('tb_registrasi.status','>=','2')
                    ->where(function($query) use ($siklus)
                        {
                            $query->where('tb_registrasi.siklus', '=', '12')
                                ->orwhere('tb_registrasi.siklus', $siklus);
                        })
                    ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_zscore')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('form','=','kimia air')
                                    ->where('siklus', $siklus)
                                    ->orderBy('id', 'desc')
                                    ->first();
                    if(count($evaluasi)){
                        return "".'<a href="kimiaair/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            Proses
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="kimiaair/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                                Proses
                                        </button>
                                    </a>';
                    }
                ;})
            ->addColumn('posisi', function($data) use ($siklus){
                    $evaluasi = DB::table('tb_zscore')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('form','=','kimia air')
                                    ->where('siklus', $siklus)
                                    ->orderBy('id', 'desc')
                                    ->first();
                    if(count($evaluasi)){
                        return "1";
                    }else{
                        return "0";
                    }
                ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_data2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia'])
            ->make(true);
        }
        return view('evaluasi.kimiaair.zscore.perusahaan');
    }

    public function dataperusahaankimiaair(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '11')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();

        return view('evaluasi.kimiaair.zscore.dataperusahaan', compact('data','siklus'));
    }

    public function perusahaankimiaairsertifikat(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes,tb_registrasi.id as idregistrasi'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)->where('tb_registrasi.status','>=','2')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_zscore')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->where('form','=','kimia air')
                                    ->orderBy('id', 'desc')
                                    ->first();
                    if(count($evaluasi)){
                        return "".'<a href="kimiaair/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            cetak
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="kimiaair/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                                cetak
                                        </button>
                                    </a>';
                    }
                ;})
            ->addColumn('posisi', function($data) use ($siklus){
                    $evaluasi = DB::table('tb_zscore')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->where('form','=','kimia air')
                                    ->orderBy('id', 'desc')
                                    ->first();
                    if(count($evaluasi)){
                        return "1";
                    }else{
                        return "0";
                    }
                ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_data2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia'])
            ->make(true);
        }
        $bidang = Auth::user()->penyelenggara;
        return view('evaluasi.kimiaair.zscore.perusahaansertifikat', compact('bidang'));
    }

    public function cetaksemua(\Illuminate\Http\Request $request){
        $input = $request->all();
        $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes,tb_registrasi.id as idregistrasi,sub_bidang.parameter'))
                ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')->where('tb_registrasi.bidang','=','11')->first();
            if ($input['simpan'] == "Cetak") {
                $pdf = PDF::loadview('evaluasi.imunologi.sertifikat.kimkes', compact('datas'))
                    ->setPaper('a4', 'landscape')
                    ->setwarnings(false);
                return $pdf->stream('Sertifikat KIMKES.pdf');
            }
    }

    public function dataperusahaankimiaairsertifikat(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '11')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.kimiaair.zscore.dataperusahaansertifikat', compact('data', 'siklus'));
    }

    public function kimiaairterbataslaporan()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia air terbatas')->get();
        return view('evaluasi.kimiaairterbatas.laporanhasilpeserta.index', compact('parameter'));
    }

    public function kimiaairterbataslaporanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameterna = DB::table('parameter')->where('kategori', 'kimia air terbatas')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('tb_registrasi.bidang', '=', '12')
                ->where('hp_headers.siklus', $input['siklus'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $input['tahun'])
                ->select('hp_headers.*')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'kimia air terbatas')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->where('hp_header_id', $val->id)
                            ->where('parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data);
        Excel::create('Laporan Hasil Peserta Per Parameter Kimia Air Terbatas', function($excel) use ($data, $parameterna) {
            $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('evaluasi.kimiaairterbatas.laporanhasilpeserta.view', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
        })->download('xls');
    }

    public function kimiaairterbatassdmedian()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia air terbatas')->get();
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'kimia air terbatas')
            ->orderBy('tb_sd_median.id', 'desc')
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.kimiaairterbatas.sdmedian.index', compact('parameter','sd'));
    }

    public function kimiaairterbatassdmedianna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $Savedata = new SdMedian;
        $Savedata->parameter = $input['parameter'];
        $Savedata->siklus = $input['siklus'];
        $Savedata->tahun = $input['tahun'];
        $Savedata->sd = $input['sd'];
        $Savedata->median = $input['median'];
        $Savedata->form = 'kimia air terbatas';
        $Savedata->save();

        Session::flash('message', 'Input SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-sd-median/kimiaairterbatas');
    }

    public function kimiaairterbatassdmedianedit($id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia air terbatas')->get();
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'kimia air terbatas')
            ->where('tb_sd_median.id', $id)
            ->orderBy('tb_sd_median.id', 'desc')
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->first();
            // dd($sd);
        return view('evaluasi.kimiaairterbatas.sdmedian.view', compact('parameter','sd'));
    }

    public function kimiaairterbatassdmedianupdate(\Illuminate\Http\Request $request, $id)
    {
        $Update['parameter'] = $request->parameter;
        $Update['siklus'] = $request->siklus;
        $Update['tahun'] = $request->tahun;
        $Update['tipe'] = $request->tipe;
        $Update['sd'] = $request->sd;
        $Update['median'] = $request->median;

        SdMedian::where('id',$id)->update($Update);
        // dd($input);
        Session::flash('message', 'Edit SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median/kimiaairterbatas');
    }

    public function kimiaairterbatassdmediandelet($id){
        Session::flash('message', 'Hapus Data SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        SdMedian::find($id)->delete();
        return redirect('evaluasi/input-sd-median/kimiaairterbatas');
    }

    public function perusahaankimiaairterbatas(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes,tb_registrasi.id as idregistrasi'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)->where('tb_registrasi.bidang','=','12')->where('tb_registrasi.status','>=','2')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_zscore')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('form','=','kimia air terbatas')
                                    ->where('siklus', $siklus)
                                    ->orderBy('id', 'desc')
                                    ->first();
                    if(count($evaluasi)){
                        return "".'<a href="kimiaairterbatas/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            Proses
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="kimiaairterbatas/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                                Proses
                                        </button>
                                    </a>';
                    }
                ;})
            ->addColumn('posisi', function($data) use ($siklus){
                    $evaluasi = DB::table('tb_zscore')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('form','=','kimia air terbatas')
                                    ->where('siklus', $siklus)
                                    ->orderBy('id', 'desc')
                                    ->first();
                    if(count($evaluasi)){
                        return "1";
                    }else{
                        return "0";
                    }
                ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_data2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia'])
            ->make(true);
        }
        return view('evaluasi.kimiaairterbatas.zscore.perusahaan');
    }

    public function dataperusahaankimiaairterbatas(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun  = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '12')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
                // dd($data);
        return view('evaluasi.kimiaairterbatas.zscore.dataperusahaan', compact('data','siklus'));
    }

    public function perusahaankimiaairterbatassertifikat(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes,tb_registrasi.id as idregistrasi'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)->where('tb_registrasi.status','>=','2')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_zscore')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->where('form','=','kimia air terbatas')
                                    ->orderBy('id', 'desc')
                                    ->first();
                    if(count($evaluasi)){
                        return "".'<a href="kimiaairterbatas/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            Cetak
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="kimiaairterbatas/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                                Cetak
                                        </button>
                                    </a>';
                    }
                ;})
            ->addColumn('posisi', function($data) use ($siklus){
                    $evaluasi = DB::table('tb_zscore')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->where('form','=','kimia air terbatas')
                                    ->orderBy('id', 'desc')
                                    ->first();
                    if(count($evaluasi)){
                        return "1";
                    }else{
                        return "0";
                    }
                ;})
            ->make(true);
        }
        $bidang = Auth::user()->penyelenggara;
        return view('evaluasi.kimiaairterbatas.zscore.perusahaansertifikat', compact('bidang'));
    }

    public function dataperusahaankimiaairterbatassertifikat(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '12')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.kimiaairterbatas.zscore.dataperusahaansertifikat', compact('data','siklus'));
    }

    public function hematologirekapitulasinap(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $request = array("tahun"=>$id, "siklus"=>$type, "form"=>"hematologi");
        $input = $request;
        // dd($input);
        $parameter = DB::table('parameter')->where('kategori', $input['form'])->orderBy('parameter.bagian','ASC')->get();
        foreach ($parameter as $key => $val) {
            if ($input['siklus'] == '1') {
                $beluminput1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=','1')
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.pemeriksaan','=', NULL)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                                ->count();
                $val->beluminput1 = $beluminput1;
                $beluminput2 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=','1')
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.pemeriksaan2','=', NULL)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                                ->count();
                $val->beluminput2 = $beluminput2;
            }else{
                $beluminput1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=','1')
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.rpr1','=', NULL)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                                ->count();
                $val->beluminput1 = $beluminput1;
                $beluminput2 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=','1')
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.rpr2','=', NULL)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                                ->count();
                $val->beluminput2 = $beluminput2;
            }

            $dataa1 = "SELECT
                        (SELECT
                                count(*)
                            FROM
                                tb_registrasi
                            WHERE
                                tb_registrasi.bidang = 1
                            AND
                                tb_registrasi.status = 3
                        ) AS JUMLAH_PESERTA,
                        (
                            SELECT
                                count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='a' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id
                            WHERE
                                hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan IN ('_','-','-','_') AND tb_zscore.zscore = '-'
                        ) AS TIDAK_DI_ANAL,
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='a' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id
                            WHERE
                                hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN  ('_','-','-','_') AND tb_zscore.zscore = '-'
                        ) AS TIDAK_DI_EVALUASI,
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='a' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN  ('_','-','-','_')
                                AND
                                (
                                    (tb_zscore.zscore > 2 AND tb_zscore.zscore <= 3)
                                    OR
                                    (tb_zscore.zscore >= -3 AND tb_zscore.zscore < -2)
                                )
                        ) AS PERINGATAN,
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='a' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id."
                                AND
                                hasil_pemeriksaan NOT IN  ('_','-','-','_')
                                AND (
                                        (tb_zscore.zscore >= -2 AND tb_zscore.zscore <= 2)
                                    )
                        ) AS MEMUASKAN,
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='a' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN  ('_','-','-','_')
                                AND
                                (
                                    (tb_zscore.zscore < -3 OR tb_zscore.zscore > 3)
                                )
                        ) AS TIDAKS_MEMUASKAN
                    ";
            $dataalata1 = "SELECT
                            (
                                SELECT
                                    count(*)
                                FROM
                                    tb_registrasi
                                WHERE
                                    tb_registrasi.bidang = 1
                            AND
                                tb_registrasi.status = 3
                            ) AS JUMLAH_PESERTA_ALAT,
                            (
                                SELECT
                                    count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='a' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id
                                WHERE
                                    hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan IN ('_','-','_') AND tb_zscore_alat.zscore = '-'
                            ) AS TIDAK_DI_ANAL_ALAT,
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='a' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id
                                WHERE
                                    hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN ('_','-','_') AND tb_zscore_alat.zscore = '-'
                            ) AS TIDAK_DI_EVALUASI_ALAT,
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='a' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN ('_','-','_')
                                    AND
                                    (
                                        (tb_zscore_alat.zscore > 2 AND tb_zscore_alat.zscore <= 3)
                                        OR
                                        (tb_zscore_alat.zscore >= -3 AND tb_zscore_alat.zscore < -2)
                                    )
                            ) AS PERINGATAN_ALAT,
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='a' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id."
                                    AND
                                    hasil_pemeriksaan NOT IN ('_','-','_')
                                    AND (
                                            (tb_zscore_alat.zscore >= -2 AND tb_zscore_alat.zscore <= 2)
                                        )
                            ) AS MEMUASKAN_ALAT,
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='a' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN ('_','-','_')
                                    AND
                                    (
                                        (tb_zscore_alat.zscore < -3 OR tb_zscore_alat.zscore > 3)
                                    )
                            ) AS TIDAKS_MEMUASKAN_ALAT
                            ";

            $datametodea1 = "SELECT
                                (
                                    SELECT
                                        count(*)
                                    FROM
                                        tb_registrasi
                                    WHERE
                                        tb_registrasi.bidang = 1
                            AND
                                tb_registrasi.status = 3
                                ) AS JUMLAH_PESERTA_METODE,
                                (
                                    SELECT
                                        count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='a' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id
                                    WHERE
                                        hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan IN ('_','-','_') AND tb_zscore_metode.zscore = '-'
                                ) AS TIDAK_DI_ANAL_METODE,
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='a' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id
                                    WHERE
                                        hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN ('_','-','_') AND tb_zscore_metode.zscore = '-'
                                ) AS TIDAK_DI_EVALUASI_METODE,
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='a' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN ('_','-','_')
                                        AND
                                        (
                                            (tb_zscore_metode.zscore > 2 AND tb_zscore_metode.zscore <= 3)
                                            OR
                                            (tb_zscore_metode.zscore >= -3 AND tb_zscore_metode.zscore < -2)
                                        )
                                ) AS PERINGATAN_METODE,
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='a' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id."
                                        AND
                                        hasil_pemeriksaan NOT IN ('_','-','_')
                                        AND (
                                                (tb_zscore_metode.zscore >= -2 AND tb_zscore_metode.zscore <= 2)
                                            )
                                ) AS MEMUASKAN_METODE,
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='a'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='a' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN ('_','-','_')
                                        AND
                                        (
                                            (tb_zscore_metode.zscore < -3 OR tb_zscore_metode.zscore > 3)
                                        )
                                ) AS TIDAKS_MEMUASKAN_METODE
                    ";
            $dataa2 = "SELECT
                        (
                            SELECT
                                count(*)
                            FROM
                                tb_registrasi
                            WHERE
                                tb_registrasi.bidang = 1
                            AND
                                tb_registrasi.status = 3
                        ) AS JUMLAH_PESERTA,
                        (
                            SELECT
                                count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='b' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id
                            WHERE
                                hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan IN ('_','-','-','_') AND tb_zscore.zscore = '-'
                        ) AS TIDAK_DI_ANAL,
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='b' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id
                            WHERE
                                hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN  ('_','-','-','_') AND tb_zscore.zscore = '-'
                        ) AS TIDAK_DI_EVALUASI,
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='b' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN  ('_','-','-','_')
                                AND
                                (
                                    (tb_zscore.zscore > 2 AND tb_zscore.zscore <= 3)
                                    OR
                                    (tb_zscore.zscore >= -3 AND tb_zscore.zscore < -2)
                                )
                        ) AS PERINGATAN,
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='b' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id."
                                AND
                                hasil_pemeriksaan NOT IN  ('_','-','-','_')
                                AND (
                                        (tb_zscore.zscore >= -2 AND tb_zscore.zscore <= 2)
                                    )
                        ) AS MEMUASKAN,
                        (
                            SELECT
                            count(*)
                            FROM `hp_details`
                                INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='b' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                            WHERE
                                hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN  ('_','-','-','_')
                                AND
                                (
                                    (tb_zscore.zscore < -3 OR tb_zscore.zscore > 3)
                                )
                        ) AS TIDAKS_MEMUASKAN
                    ";

            $dataalata2 = "SELECT
                            (
                                SELECT
                                    count(*)
                                FROM
                                    tb_registrasi
                                WHERE
                                    tb_registrasi.bidang = 1
                            AND
                                tb_registrasi.status = 3
                            ) AS JUMLAH_PESERTA_ALAT,
                            (
                                SELECT
                                    count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='b' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id
                                WHERE
                                    hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan IN ('_','-','_') AND tb_zscore_alat.zscore = '-'
                            ) AS TIDAK_DI_ANAL_ALAT,
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='b' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id
                                WHERE
                                    hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN ('_','-','_') AND tb_zscore_alat.zscore = '-'
                            ) AS TIDAK_DI_EVALUASI_ALAT,
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='b' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN ('_','-','_')
                                    AND
                                    (
                                        (tb_zscore_alat.zscore > 2 AND tb_zscore_alat.zscore <= 3)
                                        OR
                                        (tb_zscore_alat.zscore >= -3 AND tb_zscore_alat.zscore < -2)
                                    )
                            ) AS PERINGATAN_ALAT,
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='b' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id."
                                    AND
                                    hasil_pemeriksaan NOT IN ('_','-','_')
                                    AND (
                                            (tb_zscore_alat.zscore >= -2 AND tb_zscore_alat.zscore <= 2)
                                        )
                            ) AS MEMUASKAN_ALAT,
                            (
                                SELECT
                                count(*)
                                FROM `hp_details`
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_alat ON tb_zscore_alat.parameter = hp_details.parameter_id AND tb_zscore_alat.siklus = ".$input['siklus']." AND tb_zscore_alat.tahun = ".$input['tahun']." AND tb_zscore_alat.type='b' AND tb_zscore_alat.form = '".$input['form']."' AND tb_zscore_alat.id_registrasi = tb_registrasi.id AND tb_zscore_alat.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN ('_','-','_')
                                    AND
                                    (
                                        (tb_zscore_alat.zscore < -3 OR tb_zscore_alat.zscore > 3)
                                    )
                            ) AS TIDAKS_MEMUASKAN_ALAT
                            ";

            $datametodea2 = "SELECT
                                (
                                    SELECT
                                        count(*)
                                    FROM
                                        tb_registrasi
                                    WHERE
                                        tb_registrasi.bidang = 1
                                    AND
                                tb_registrasi.status = 3
                                ) AS JUMLAH_PESERTA_METODE,
                                (
                                    SELECT
                                        count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='b' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id
                                    WHERE
                                        hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan IN ('_','-','_') AND tb_zscore_metode.zscore = '-'
                                ) AS TIDAK_DI_ANAL_METODE,
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='b' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id
                                    WHERE
                                        hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN ('_','-','_') AND tb_zscore_metode.zscore = '-'
                                ) AS TIDAK_DI_EVALUASI_METODE,
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='b' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN ('_','-','_')
                                        AND
                                        (
                                            (tb_zscore_metode.zscore > 2 AND tb_zscore_metode.zscore <= 3)
                                            OR
                                            (tb_zscore_metode.zscore >= -3 AND tb_zscore_metode.zscore < -2)
                                        )
                                ) AS PERINGATAN_METODE,
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='b' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id."
                                        AND
                                        hasil_pemeriksaan NOT IN ('_','-','_')
                                        AND (
                                                (tb_zscore_metode.zscore >= -2 AND tb_zscore_metode.zscore <= 2)
                                            )
                                ) AS MEMUASKAN_METODE,
                                (
                                    SELECT
                                    count(*)
                                    FROM `hp_details`
                                        INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='b'
                                        INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                        INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='b' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                    WHERE
                                        hp_details.parameter_id = ".$val->id." AND hasil_pemeriksaan NOT IN ('_','-','_')
                                        AND
                                        (
                                            (tb_zscore_metode.zscore < -3 OR tb_zscore_metode.zscore > 3)
                                        )
                                ) AS TIDAKS_MEMUASKAN_METODE
                    ";

            $datapesertaa1 = DB::table(DB::raw("($dataa1) t"))->select('*')->first();
            $dataalata1 = DB::table(DB::raw("($dataalata1) t"))->select('*')->first();
            $datametodea1 = DB::table(DB::raw("($datametodea1) t"))->select('*')->first();
            $datapesertaa2 = DB::table(DB::raw("($dataa2) t"))->select('*')->first();
            $dataalata2 = DB::table(DB::raw("($dataalata2) t"))->select('*')->first();
            $datametodea2 = DB::table(DB::raw("($datametodea2) t"))->select('*')->first();

            $val->datapesertaa1 = $datapesertaa1;
            $val->dataalata1 = $dataalata1;
            $val->datametodea1 = $datametodea1;
            $val->datapesertaa2 = $datapesertaa2;
            $val->dataalata2 = $dataalata2;
            $val->datametodea2 = $datametodea2;
        }
        // dd($parameter);
        // dd($dataalata);
        // return view('evaluasi.hematologi.rekapitulasihasil.view', compact('jumlahpesertaa', 'datapesertaa', 'jumlahalata', 'dataalata', 'datametodea', 'jumlahmetodea'));
        $pdf = PDF::loadview('evaluasi.hematologi.rekapitulasihasil.view', compact('parameter', 'input', 'pdf'))
            ->setPaper('a4', 'lanscape')
            ->setwarnings(false);
        return $pdf->stream('Laporan Pelaksanaan PME '.$input['form'].'.pdf');
        // Excel::create('Rekapitulasi Hasil PNPME per Parameter '.$input['form'], function($excel) use ($datapesertaa, $dataalata, $datametodea, $input, $parameter) {
        //     $excel->sheet('Wilayah', function($sheet) use ($datapesertaa, $dataalata, $datametodea, $input, $parameter) {
        //         $sheet->loadView('evaluasi.hematologi.rekapitulasihasil.view', array('datapesertaa'=>$datapesertaa,'dataalata'=>$dataalata,'datametodea'=>$datametodea,'input'=>$input,'parameter'=>$parameter) );
        //     });
        // })->download('xls');
    }

    public function testcronjob(\Illuminate\Http\Request $request)
    {
        $date = $request->get('tahun');
        $siklus = $request->get('siklus');
        $type = $request->get('type');

        $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->where('tb_registrasi.bidang','=','1')
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
            ->get();
        foreach($datas as $r){
            $r->sub_bidang =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $r->id)
                ->where('sub_bidang.id', '=', '1')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
            foreach($r->sub_bidang as $sb){
                $zscore = DB::table('tb_zscore')
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $date)
                        ->where('id_registrasi', $sb->id);
                $zscore->delete();
                $dataSave = [];
                $dataParamA = DB::table('parameter')
                    ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                    ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                    ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                    ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                    ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                    ->where('hp_headers.siklus', $siklus)
                    ->where('hp_headers.type', $type)
                    ->where('tb_registrasi.id', $sb->id)
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                    ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan',DB::raw('hp_details.id as hp_detail_id'))
                    ->orderBy('parameter.bagian', 'asc')
                    ->get();
                foreach ($dataParamA as $key => $valParam) {
                    $sd = DB::table('tb_sd_median')
                            ->where('parameter', $valParam->id)
                            ->where('siklus', $siklus)
                            ->where('tahun', $date)
                            ->where('tipe', $type)
                            ->where('form', '=', 'hematologi')
                            ->get();
                    $valParam->sd = $sd;
                    $dataSave['parameter_id'][$key] = $valParam->id;
                    $valParam->hasil_pemeriksaan = str_replace(",",".",$valParam->hasil_pemeriksaan);
                    $valParam->hasil_pemeriksaan = ($valParam->hasil_pemeriksaan == "-" ? NULL : str_replace(",",".",$valParam->hasil_pemeriksaan));
                    if (count($valParam->sd) && $valParam->hasil_pemeriksaan != NULL) {
                        $z = ($valParam->hasil_pemeriksaan - $valParam->sd[0]->median) / $valParam->sd[0]->sd;
                        $dataSave['zscore'][$key] = number_format($z, 1);
                    }else{
                        $z = "-";
                        if($valParam->hasil_pemeriksaan == NULL){
                            $dataSave['zscore'][$key] = "Tidak di analisis";
                        }elseif($z == '-'){
                            $dataSave['zscore'][$key] = "-";
                        }
                    }
                }
                $i = 0;
                if(!empty($dataSave)){
                    foreach ($dataSave['parameter_id'] as $alat) {
                        $Save = new ZScore;
                        $Save->id_registrasi = $sb->id;
                        $Save->parameter = $dataSave['parameter_id'][$i];
                        $Save->zscore = $dataSave['zscore'][$i];
                        $Save->type = $type;
                        $Save->siklus = $siklus;
                        $Save->tahun = $date;
                        $Save->form = 'hematologi';
                        $Save->save();
                        $i++;
                    }
                }
            }

        }
    }

    public function testcronjobalat(\Illuminate\Http\Request $request)
    {
        $date = $request->get('tahun');
        $siklus = $request->get('siklus');
        $type = $request->get('type');

        $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->where('tb_registrasi.bidang','=','1')
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
            ->get();

        foreach($datas as $r){
        $r->sub_bidang =  DB::table('tb_registrasi')
            ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
            ->where('tb_registrasi.perusahaan_id', $r->id)
            ->where('sub_bidang.id', '=', '1')
            ->where('tb_registrasi.status', 3)
            ->where(function($query)
                {
                    $query->where('status_data1', 2)
                        ->orwhere('status_data2', 2);
                })
            ->where(function($query)
                {
                    if ('pemeriksaan' != 'done') {
                        $query->orwhere('pemeriksaan2', 'done');
                    }
                    if ('pemeriksaan2' != 'done') {
                        $query->orwhere('pemeriksaan', 'done');
                    }
                })
            ->get();
            foreach($r->sub_bidang as $sb){
                $zscore = DB::table('tb_zscore_alat')
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $date)
                        ->where('id_registrasi', $sb->id);
                        $zscore->delete();
                $dataSave = [];
                $dataParamA = DB::table('parameter')
                        ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                        ->join('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                        ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                        ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                        ->where('hp_headers.siklus', $siklus)
                        ->where('hp_headers.type', $type)
                        ->where('tb_registrasi.id', $sb->id)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id as id_parameter', 'tb_instrumen.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                        ->orderBy('parameter.bagian', 'asc')
                        ->get();
                foreach ($dataParamA as $key => $valParam) {
                    $sd = DB::table('tb_sd_median_alat')
                            ->where('parameter', $valParam->id_parameter)
                            ->where('alat', $valParam->id)
                            ->where('siklus', $siklus)
                            ->where('tahun', $date)
                            ->where('tipe', $type)
                            ->where('form', '=', 'hematologi')
                            ->get();
                    $valParam->sd = $sd;

                    $dataSave['parameter_id'][$key] = $valParam->id_parameter;
                    $dataSave['alat'][$key] = $valParam->id;
                    $valParam->hasil_pemeriksaan = str_replace(",",".",$valParam->hasil_pemeriksaan);

                    $valParam->hasil_pemeriksaan = ($valParam->hasil_pemeriksaan == "-" ? NULL : str_replace(",",".",$valParam->hasil_pemeriksaan));
                    if (count($valParam->sd) && $valParam->hasil_pemeriksaan != NULL && $valParam->sd[0]->median != NULL) {
                        $z = ($valParam->hasil_pemeriksaan - $valParam->sd[0]->median) / $valParam->sd[0]->sd;
                        $dataSave['zscore'][$key] = number_format($z, 1);
                    }else{
                        $z = "-";
                        if($valParam->hasil_pemeriksaan == NULL){
                            $dataSave['zscore'][$key] = "Tidak di analisis";
                        }else{
                            $dataSave['zscore'][$key] = "-";
                        }
                    }

                }
                $i = 0;
                if(!empty($dataSave)){
                    foreach ($dataSave['parameter_id'] as $alat) {
                        $Save = new ZScoreAlat;
                        $Save->id_registrasi = $sb->id;
                        $Save->parameter = $dataSave['parameter_id'][$i];
                        $Save->alat = $dataSave['alat'][$i];
                        $Save->zscore = $dataSave['zscore'][$i];
                        $Save->type = $type;
                        $Save->siklus = $siklus;
                        $Save->tahun = $date;
                        $Save->form = 'hematologi';
                        $Save->save();
                        $i++;
                    }
                }
            }
        }
    }

    public function testcronjobmetode(\Illuminate\Http\Request $request)
    {
        $date = $request->get('tahun');
        $siklus = $request->get('siklus');
        $type = $request->get('type');
        
        $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
            ->where('tb_registrasi.bidang','=','1')
            ->get();

        foreach($datas as $r){
            $r->sub_bidang =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $r->id)
                ->where('sub_bidang.id', '=', '1')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
            foreach($r->sub_bidang as $sb){
                $zscore = DB::table('tb_zscore_metode')
                            ->where('siklus', $siklus)
                            ->where('type', $type)
                            ->where('tahun', $date)
                            ->where('id_registrasi', $sb->id);
                            $zscore->delete();

                $dataSave = [];

                $dataParamA = DB::table('parameter')
                        ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                        ->join('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                        ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                        ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                        ->where('hp_headers.siklus', $siklus)
                        ->where('hp_headers.type', $type)
                        ->where('tb_registrasi.id', $sb->id)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id as id_parameter', 'metode_pemeriksaan.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                        ->orderBy('parameter.bagian', 'asc')
                        ->get();
                foreach ($dataParamA as $key => $valParam) {
                    $sd = DB::table('tb_sd_median_metode')
                            ->where('parameter', $valParam->id_parameter)
                            ->where('metode', $valParam->id)
                            ->where('siklus', $siklus)
                            ->where('tahun', $date)
                            ->where('tipe', $type)
                            ->where('form', '=', 'hematologi')
                            ->get();
                    $valParam->sd = $sd;

                    $dataSave['parameter_id'][$key] = $valParam->id_parameter;
                    $dataSave['metode'][$key] = $valParam->id;
                    $valParam->hasil_pemeriksaan = str_replace(",",".",$valParam->hasil_pemeriksaan);

                    $valParam->hasil_pemeriksaan = ($valParam->hasil_pemeriksaan == "-" ? NULL : str_replace(",",".",$valParam->hasil_pemeriksaan));
                    if (count($valParam->sd) && $valParam->hasil_pemeriksaan != NULL && $valParam->sd[0]->median != 0 && $valParam->sd[0]->median != NULL) {
                        $z = ($valParam->hasil_pemeriksaan - $valParam->sd[0]->median) / $valParam->sd[0]->sd;
                        $dataSave['zscore'][$key] = number_format($z, 1);
                    }else{
                        $z = "-";
                        if($valParam->hasil_pemeriksaan == NULL){
                            $dataSave['zscore'][$key] = "Tidak di analisis";
                        }elseif($z == '-'){
                            $dataSave['zscore'][$key] = "-";
                        }
                    }
                    

                }
                $i = 0;
                if(!empty($dataSave)){
                    foreach ($dataSave['parameter_id'] as $alat) {
                        $Save = new ZScoreMetode;
                        $Save->id_registrasi = $sb->id;
                        $Save->parameter = $dataSave['parameter_id'][$i];
                        $Save->metode = $dataSave['metode'][$i];
                        $Save->zscore = $dataSave['zscore'][$i];
                        $Save->type = $type;
                        $Save->siklus = $siklus;
                        $Save->tahun = $date;
                        $Save->form = 'hematologi';
                        $Save->save();
                        $i++;
                    }
                }
            }
        }
    }
    public function cronjobkimiaklinik(\Illuminate\Http\Request $request)
    {
        $date = $request->get('tahun');
        $siklus = $request->get('siklus');
        $type = $request->get('type');
        
        $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
            ->where('tb_registrasi.bidang','=','2')
            ->get();
        foreach($datas as $r){
            $r->sub_bidang =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $r->id)
                ->where('sub_bidang.id', '=', '2')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                            $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('rpr1', 'done')
                                ->orwhere('rpr2', 'done');
                    })
                ->get();
            foreach($r->sub_bidang as $sb){
                $zscore = DB::table('tb_zscore')
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $date)
                        ->where('id_registrasi', $sb->id);
                $zscore->delete();
                $dataSave = [];
                $dataParamA = DB::table('parameter')
                    ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                    ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                    ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                    ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                    ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                    ->where('hp_headers.siklus', $siklus)
                    ->where('hp_headers.type', $type)
                    ->where('tb_registrasi.id', $sb->id)
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                    ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan',DB::raw('hp_details.id as hp_detail_id'))
                    ->orderBy('parameter.bagian', 'asc')
                    ->get();
                foreach ($dataParamA as $key => $valParam) {
                    $sd = DB::table('tb_sd_median')
                            ->where('parameter', $valParam->id)
                            ->where('siklus', $siklus)
                            ->where('tahun', $date)
                            ->where('tipe', $type)
                            ->where('form', '=', 'kimia klinik')
                            ->get();
                    $valParam->sd = $sd;
                    $dataSave['parameter_id'][$key] = $valParam->id;

                    $labelHasil = $valParam->hasil_pemeriksaan;
                    try{
                        $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan  *1;
                    }catch(\Exception $e){
                        $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan;
                    }
                    if($valParam->hasil_pemeriksaan == "-" || $valParam->hasil_pemeriksaan == null || $valParam->hasil_pemeriksaan == '0'  || $valParam->hasil_pemeriksaan == '0.00' ||  $valParam->hasil_pemeriksaan == '0.0' || empty($valParam->hasil_pemeriksaan) || is_string($valParam->hasil_pemeriksaan)){
                        if($valParam->hasil_pemeriksaan == '0' || $valParam->hasil_pemeriksaan == '0.0' || $valParam->hasil_pemeriksaan == '0.00' || is_string($valParam->hasil_pemeriksaan)){
                            $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan;
                            $dataSave['zscore'][$key] = '-';
                        }else{
                            $dataSave['zscore'][$key] = '-';
                        }
                    }else{
                        $target_parameter = (!empty($valParam->sd) ? (!empty($valParam->sd[0]->median) ? $valParam->sd[0]->median : '-') : '-');
                        $sd_parameter = (!empty($valParam->sd) ? (!empty($valParam->sd[0]->sd) ? $valParam->sd[0]->sd : '-') : '-');
                        $zscore_parameter = '-';
                        if(($target_parameter != '-' && $sd_parameter != '-')){
                            $zscore_parameter = ($valParam->hasil_pemeriksaan - $valParam->sd[0]->median) / $valParam->sd[0]->sd;
                            $dataSave['zscore'][$key] = number_format($zscore_parameter, 2);
                        }
                    }
                }
                $i = 0;
                // dd($dataSave);
                if(!empty($dataSave)){
                    foreach ($dataSave['parameter_id'] as $alat) {
                        $Save = new ZScore;
                        $Save->id_registrasi = $sb->id;
                        $Save->parameter = $dataSave['parameter_id'][$i];
                        $Save->zscore = $dataSave['zscore'][$i];
                        $Save->type = $type;
                        $Save->siklus = $siklus;
                        $Save->tahun = $date;
                        $Save->form = 'kimia klinik';
                        $Save->save();
                        $i++;
                    }
                }
            }
        }


        $datasalat = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
            ->where('tb_registrasi.bidang','=','2')
            ->get();
        foreach($datasalat as $r){
            $r->sub_bidang =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $r->id)
                ->where('sub_bidang.id', '=', '2')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                            $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('rpr1', 'done')
                                ->orwhere('rpr2', 'done');
                    })
                ->get();
            foreach($r->sub_bidang as $sb){
                $zscore = DB::table('tb_zscore_alat')
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $date)
                        ->where('id_registrasi', $sb->id);
                $zscore->delete();
                $dataSave = [];
                $dataParamA = DB::table('parameter')
                        ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                        ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                        ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                        ->leftjoin('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                        ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                        ->where('hp_headers.siklus', $siklus)
                        ->where('hp_headers.type', $type)
                        ->where('tb_registrasi.id', $sb->id)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id as id_parameter', 'tb_instrumen.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                        ->orderBy('parameter.bagian', 'asc')
                        ->get();
                foreach ($dataParamA as $key => $valParam) {
                    $sd = DB::table('tb_sd_median_alat')
                            ->where('parameter', $valParam->id_parameter)
                            ->where('alat', $valParam->id)
                            ->where('siklus', $siklus)
                            ->where('tahun', $date)
                            ->where('tipe', $type)
                            ->where('form', '=', 'kimia klinik')
                            ->get();
                    $valParam->sd = $sd;
                    $dataSave['parameter_id'][$key] = $valParam->id_parameter;
                    $dataSave['alat'][$key] = $valParam->id;

                    $labelHasil = $valParam->hasil_pemeriksaan;
                    try{
                        $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan  *1;
                    }catch(\Exception $e){
                        $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan;
                    }
                    if($valParam->hasil_pemeriksaan == "-" || $valParam->hasil_pemeriksaan == null || $valParam->hasil_pemeriksaan == '0'  || $valParam->hasil_pemeriksaan == '0.00' ||  $valParam->hasil_pemeriksaan == '0.0' || empty($valParam->hasil_pemeriksaan) || is_string($valParam->hasil_pemeriksaan)){
                        if($valParam->hasil_pemeriksaan == '0' || $valParam->hasil_pemeriksaan == '0.0' || $valParam->hasil_pemeriksaan == '0.00' || is_string($valParam->hasil_pemeriksaan)){
                            $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan;
                            $dataSave['zscore'][$key] = '-';
                        }else{
                            $dataSave['zscore'][$key] = '-';
                        }
                    }else{
                        $target_alat = (!empty($valParam->sd) ? (!empty($valParam->sd[0]->median) ? $valParam->sd[0]->median : '-') : '-');
                        $sd_alat = (!empty($valParam->sd) ? (!empty($valParam->sd[0]->sd) ? $valParam->sd[0]->sd : '-') : '-');
                        $zscore_alat = '-';
                        $dataSave['zscore'][$key] = '-';
                        if(($target_alat != '-' && $sd_alat != '-')){
                            $zscore_alat = ($valParam->hasil_pemeriksaan - $valParam->sd[0]->median) / $valParam->sd[0]->sd;
                            $dataSave['zscore'][$key] = number_format($zscore_alat, 2);
                        }
                    }
                }
                $i = 0;
                // dd($dataSave);
                if(!empty($dataSave)){
                    foreach ($dataSave['parameter_id'] as $alat) {
                        $Save = new ZScoreAlat;
                        $Save->id_registrasi = $sb->id;
                        $Save->parameter = $dataSave['parameter_id'][$i];
                        $Save->alat = $dataSave['alat'][$i];
                        $Save->zscore = $dataSave['zscore'][$i];
                        $Save->type = $type;
                        $Save->siklus = $siklus;
                        $Save->tahun = $date;
                        $Save->form = 'kimia klinik';
                        $Save->save();
                        $i++;
                    }
                }
            }
        }

        $datasmetode = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
            ->where('tb_registrasi.bidang','=','2')
            ->get();
        foreach($datasmetode as $r){
            $r->sub_bidang =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $r->id)
                ->where('sub_bidang.id', '=', '2')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                            $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('rpr1', 'done')
                                ->orwhere('rpr2', 'done');
                    })
                ->get();
            foreach($r->sub_bidang as $sb){
                $zscore = DB::table('tb_zscore_metode')
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $date)
                        ->where('id_registrasi', $sb->id);
                $zscore->delete();
                $dataSave = [];
                $dataParamA = DB::table('parameter')
                        ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                        ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                        ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                        ->leftjoin('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                        ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                        ->where('hp_headers.siklus', $siklus)
                        ->where('hp_headers.type', $type)
                        ->where('tb_registrasi.id', $sb->id)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id as id_parameter', 'metode_pemeriksaan.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                        ->orderBy('parameter.bagian', 'asc')
                        ->get();
                foreach ($dataParamA as $key => $valParam) {
                    $sd = DB::table('tb_sd_median_metode')
                            ->where('parameter', $valParam->id_parameter)
                            ->where('metode', $valParam->id)
                            ->where('siklus', $siklus)
                            ->where('tahun', $date)
                            ->where('tipe', $type)
                            ->where('form', '=', 'kimia klinik')
                            ->get();
                    $valParam->sd = $sd;
                    $dataSave['parameter_id'][$key] = $valParam->id_parameter;
                    $dataSave['metode'][$key] = $valParam->id;

                    $labelHasil = $valParam->hasil_pemeriksaan;
                    try{
                        $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan  *1;
                    }catch(\Exception $e){
                        $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan;
                    }
                    if($valParam->hasil_pemeriksaan == "-" || $valParam->hasil_pemeriksaan == null || $valParam->hasil_pemeriksaan == '0'  || $valParam->hasil_pemeriksaan == '0.00' ||  $valParam->hasil_pemeriksaan == '0.0' || empty($valParam->hasil_pemeriksaan) || is_string($valParam->hasil_pemeriksaan)){
                        if($valParam->hasil_pemeriksaan == '0' || $valParam->hasil_pemeriksaan == '0.0' || $valParam->hasil_pemeriksaan == '0.00' || is_string($valParam->hasil_pemeriksaan)){
                            $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan;
                            $dataSave['zscore'][$key] = '-';
                        }else{
                            $dataSave['zscore'][$key] = '-';
                        }
                    }else{
                        $target_metode = (!empty($valParam->sd) ? (!empty($valParam->sd[0]->median) ? $valParam->sd[0]->median : '-') : '-');
                        $sd_metode = (!empty($valParam->sd) ? (!empty($valParam->sd[0]->sd) ? $valParam->sd[0]->sd : '-') : '-');
                        $zscore_metode = '-';
                        $dataSave['zscore'][$key] = '-';
                        if(($target_metode != '-' && $sd_metode != '-')){
                            $zscore_parameter = ($valParam->hasil_pemeriksaan - $valParam->sd[0]->median) / $valParam->sd[0]->sd;
                            $dataSave['zscore'][$key] = number_format($zscore_parameter, 2);
                        }
                    }
                }
                $i = 0;
                // dd($dataSave);
                if(!empty($dataSave)){
                    foreach ($dataSave['parameter_id'] as $alat) {
                        $Save = new ZScoreMetode;
                        $Save->id_registrasi = $sb->id;
                        $Save->parameter = $dataSave['parameter_id'][$i];
                        $Save->metode = $dataSave['metode'][$i];
                        $Save->zscore = $dataSave['zscore'][$i];
                        $Save->type = $type;
                        $Save->siklus = $siklus;
                        $Save->tahun = $date;
                        $Save->form = 'kimia klinik';
                        $Save->save();
                        $i++;
                    }
                }
            }
        }
    }
}

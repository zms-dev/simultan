<?php

namespace App\Http\Controllers;
use DB;
use App\User;
use App\daftar as Daftar;
use PDF;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
class GrafikController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['apighemnilaisama','apigzhemsemua','apinilaisama','apinilaisamaalat','apinilaisamametode','apigzkimair','apigzkimairterbatas','apigzkk']]);
    }

    public function apigzkk(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        
        $type = $input['x'];
        $siklus = $input['y'];
        // dd($type);

        $instansi =  DB::table('tb_registrasi')
        ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
        ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
        ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
        ->where('tb_registrasi.id', $id)
        ->where('sub_bidang.id', '=', '2')
        ->where('tb_registrasi.status', 3)
        ->where(function($query)
            {
                $query->where('pemeriksaan2', 'done')
                    ->orwhere('pemeriksaan', 'done')
                    ->orwhere('rpr1', 'done')
                    ->orwhere('rpr2', 'done');
            })

        ->where(function($query)
            {
                $query->where('status_data1', 2)
                    ->orwhere('status_data2', 2)
                    ->orwhere('status_datarpr1', 2)
                    ->orwhere('status_datarpr2', 2);
            })
        ->first();

        // $typeHeader = $input['x'];
        $reg = DB::table('tb_registrasi')->find($id);

        if ($reg->periode == 2 && $siklus == 1) {
            $periode = "AND tb_registrasi.periode = 2";
        }else{
            $periode = "";
        }

        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();

        foreach ($parameter as $key => $param) {
            $zscore = DB::table("tb_zscore")
                        ->where('id_registrasi', $id)
                        ->where('parameter', $param->id)
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $tahun)
                        ->first();
            $param->zscore = $zscore;
            $zscore_alat = DB::table("tb_zscore_alat")
                        ->where('id_registrasi', $id)
                        ->where('parameter', $param->id)
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $tahun)
                        ->first();
            $param->zscore_alat = $zscore_alat;
            $zscore_metode = DB::table("tb_zscore_metode")
                        ->where('id_registrasi', $id)
                        ->where('parameter', $param->id)
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $tahun)
                        ->first();
            $param->zscore_metode = $zscore_metode;

            $dataReg = DB::table('tb_registrasi')->where('tb_registrasi.id',$id)
                        ->leftJoin('hp_headers',function($join){
                            $join->on('tb_registrasi.id','=','hp_headers.id_registrasi')
                            ->where('hp_headers.type','=',$type)
                            ->where('hp_headers.siklus', $siklus);
                        })
                        ->leftJoin('hp_details',function($join){
                            $join->on('hp_headers.id','=','hp_details.hp_header_id')
                            ->where('hp_details.parameter_id', $param->id);
                        })
                        ->first();
            $param->dataReg = $dataReg;

            $alatmetode = DB::table('hp_headers')
                            ->join('hp_details', 'hp_details.hp_header_id', '=', 'hp_headers.id')
                            ->where('hp_headers.id_registrasi', $id)
                            ->where('hp_headers.type','=', $type)
                            ->where('hp_headers.siklus', $siklus)
                            ->where('hp_details.parameter_id', $param->id)
                            ->first();
            // dd($alatmetode->alat);
            $alat = $alatmetode->alat;
            $param->alat = $alat;

            $metode = $alatmetode->kode_metode_pemeriksaan;
            $param->metode = $metode;

            $sql ="
                SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi where not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'kimia klinik' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION

                SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'kimia klinik' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore >= -2 and zscore <= 2, 1, 0)) as nilai1, '-2 sd 2' as nilainya, '2' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'kimia klinik' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '3' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'kimia klinik' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '4' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'kimia klinik' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter";

            $sql2 ="SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -3 UNION

                    SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3 UNION

                    SELECT count(*) as nilai1, '-2 sd 2' as nilainya, 'c' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore >= -2 AND zscore <= 2 UNION

                    SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'd' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 2 AND zscore <= 3 UNION

                    SELECT count(*) as nilai1, '> 3' as nilainya, 'e' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 3";

            $data = DB::table(DB::raw("($sql) zone"))
                    ->select('*')
                    ->where('form','=','kimia klinik')
                    ->where('tahun','=',$tahun)
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->data = $data;
            $datap = DB::table(DB::raw("($sql2) zone"))
                    ->select('*')
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->datap = $datap;



            $alatna = DB::table('tb_instrumen')->where('id', $alat)->first();
            $param->alatna = $alatna;
            $sql_alat ="
                SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi where not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'kimia klinik' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'kimia klinik' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
            
                SELECT sum(if(zscore >= -2 and zscore <= 2, 1, 0)) as nilai1, '-2 sd 2' as nilainya, '2' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'kimia klinik' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '3' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'kimia klinik' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '4' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'kimia klinik' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter";

            $sql2_alat =" 
                    SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore < -3
                    UNION
                    
                    SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                    UNION
                    
                    SELECT count(*) as nilai1, '-2 sd 2' as nilainya, 'c' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore >= -2 AND zscore <= 2
                    UNION
                    
                    SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'd' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 2 AND zscore <= 3
                    UNION
                    
                    SELECT count(*) as nilai1, '> 3' as nilainya, 'e' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 3";

            $data_alat = DB::table(DB::raw("($sql_alat) zone"))
                    ->select('*')
                    ->where('form','=','kimia klinik')
                    ->where('tahun','=',$tahun)
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->data_alat = $data_alat;
            $datap_alat = DB::table(DB::raw("($sql2_alat) zone"))
                    ->select('*')
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->datap_alat = $datap_alat;

            $metodena = DB::table('metode_pemeriksaan')->where('id', $metode)->first();
            $param->metodena = $metodena;

            $sql_metode ="
                SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi where not zscore = '-' AND tahun = '".$tahun."' AND form = 'kimia klinik' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND form = 'kimia klinik' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
              
                SELECT sum(if(zscore >= -2 and zscore <= 2, 1, 0)) as nilai1, '-2 sd 2' as nilainya, '2' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND form = 'kimia klinik' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION

                SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '3' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND form = 'kimia klinik' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION

                SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '4' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND form = 'kimia klinik' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter";

            $sql2_metode =" 
                SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore < -3 UNION

                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3 UNION
                      
                SELECT count(*) as nilai1, '-2 sd 2' as nilainya, 'd' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode_b."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore >= -2 AND zscore <= 2 UNION

                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 2 AND zscore <= 3 UNION

                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 3";

            $data_metode = DB::table(DB::raw("($sql_metode) zone"))
                    ->select('*')
                    ->where('form','=','kimia klinik')
                    ->where('tahun','=',$tahun)
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->data_metode = $data_metode;

            $datap_metode = DB::table(DB::raw("($sql2_metode) zone"))
                    ->select('*')
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->datap_metode = $datap_metode;
        }
        // dd($parameter);
        return view('evaluasi.kimiaklinik.grafiksemua.apigzkk', compact('tahun','type','siklus','input','reg','parameter','instansi'));
    }

    public function apigzkimairterbatas(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $type = $request->get('x');
        $parameter = $input['parameter'];
        $siklus = $input['siklus'];

        $param = DB::table('parameter')->where('id',$parameter)->first();

        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` where form = 'kimia air terbatas' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '5' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' GROUP BY parameter, type";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','kimia air terbatas')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('siklus','=',$siklus)
                ->get();
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        return view('evaluasi.kimiaairterbatas.grafik.apigrafik', compact('data','tahun','siklus','param','datap'));
    }

    public function apigzkimair(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $type = $request->get('x');
        $tahun = $input['tahun'];
        $parameter = $input['parameter'];
        $siklus = $input['siklus'];

        $param = DB::table('parameter')->where('id',$parameter)->first();

        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` where form = 'kimia air' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '6' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' GROUP BY parameter, type";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','kimia air')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('siklus','=',$siklus)
                ->get();
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        return view('evaluasi.kimiaair.grafik.apigrafik', compact('data','tahun','siklus','param','datap'));
    }

    public function apinilaisamametode(\Illuminate\Http\Request $request, $id)
    {
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $data =  DB::table('tb_registrasi')
        ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
        ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
        ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
        ->where('tb_registrasi.id', $id)
        ->where('sub_bidang.id', '=', '2')
        ->where('tb_registrasi.status', 3)
        ->where(function($query)
            {
                $query->where('pemeriksaan2', 'done')
                    ->orwhere('pemeriksaan', 'done')
                    ->orwhere('rpr1', 'done')
                    ->orwhere('rpr2', 'done');
            })

        ->where(function($query)
            {
                $query->where('status_data1', 2)
                    ->orwhere('status_data2', 2)
                    ->orwhere('status_datarpr1', 2)
                    ->orwhere('status_datarpr2', 2);
            })
        ->first();
        $param = DB::table('metode_pemeriksaan')->where('parameter.kategori','=','kimia klinik')
        ->join('hp_details','hp_details.kode_metode_pemeriksaan','=','metode_pemeriksaan.id')
        ->join('parameter','parameter.id','=','metode_pemeriksaan.parameter_id')
        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
        ->leftJoin('tb_sd_median_metode',function($join) use($tahun,$siklus,$type){
            $join->on('tb_sd_median_metode.metode','=','metode_pemeriksaan.id')
            ->where('tb_sd_median_metode.parameter','=',DB::raw('metode_pemeriksaan.parameter_id'))
            ->where('tb_sd_median_metode.tahun','=',$tahun)
            ->where('tb_sd_median_metode.siklus','=',$siklus)
            ->where('tb_sd_median_metode.form','=','kimia klinik')
            ->where('tb_sd_median_metode.tipe','=',$type);
        })
        ->leftJoin('tb_ring_kk_metode',function($join) use($tahun,$siklus,$type){
            $join->on('tb_ring_kk_metode.metode_id','=','metode_pemeriksaan.id')
            ->where('tb_ring_kk_metode.parameter','=',DB::raw('metode_pemeriksaan.parameter_id'))
            ->where('tb_ring_kk_metode.tahun','=',$tahun)
            ->where('tb_ring_kk_metode.siklus','=',$siklus)
            ->where('tb_ring_kk_metode.form','=','kimia klinik')
            ->where('tb_ring_kk_metode.tipe','=',$type);
        })
        ->leftJoin('tb_zscore_metode',function($join) use($tahun,$siklus,$type,$id){
            $join->on('tb_zscore_metode.metode','=','metode_pemeriksaan.id')
            ->where('tb_zscore_metode.parameter','=',DB::raw('metode_pemeriksaan.parameter_id'))
            ->where('tb_zscore_metode.tahun','=',$tahun)
            ->where('tb_zscore_metode.id_registrasi','=',$id)
            ->where('tb_zscore_metode.siklus','=',$siklus)
            ->where('tb_zscore_metode.form','=','kimia klinik')
            ->where('tb_zscore_metode.type','=',$type);
        })
        ->where('hp_headers.id_registrasi','=',$id)
        ->where('hp_headers.type','=',$type)
        ->where('hp_headers.siklus','=',$siklus)
        ->whereRaw('hp_details.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")')
        ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
        ->select(
            'parameter.nama_parameter',
            'parameter.id as id_parameter',
            'metode_pemeriksaan.metode_pemeriksaan as nama_metode',
            'metode_pemeriksaan.id as id_metode',
            'hp_details.hasil_pemeriksaan',
            'tb_sd_median_metode.sd',
            'tb_sd_median_metode.median',
            'tb_ring_kk_metode.batas_bawah',
            'tb_ring_kk_metode.batas_atas',
            'tb_ring_kk_metode.ring_1_bawah',
            'tb_ring_kk_metode.ring_1_atas',
            'tb_ring_kk_metode.ring_2_bawah',
            'tb_ring_kk_metode.ring_2_atas',
            'tb_ring_kk_metode.ring_3_bawah',
            'tb_ring_kk_metode.ring_3_atas',
            'tb_zscore_metode.zscore as zscore',
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                    AND
                HPD.parameter_id = tb_ring_kk_metode.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                HPD.hasil_pemeriksaan < tb_ring_kk_metode.batas_bawah
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as out_bawah'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                    AND
                HPD.parameter_id = tb_ring_kk_metode.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_1_bawah AND tb_ring_kk_metode.ring_1_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_1'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                    AND
                HPD.parameter_id = tb_ring_kk_metode.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_2_bawah AND tb_ring_kk_metode.ring_2_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_2'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                    AND
                HPD.parameter_id = tb_ring_kk_metode.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_3_bawah AND tb_ring_kk_metode.ring_3_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_3'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                    AND
                HPD.parameter_id = tb_ring_kk_metode.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                HPD.hasil_pemeriksaan > tb_ring_kk_metode.batas_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as out_atas'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) < -3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_3'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) < -2
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) >= -3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara23'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= -1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) >= -2
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara12'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > -1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 1
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara11'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 2
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_antara12'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 2
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_antara23'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_3')
        )
        // ->where()
        ->get();
        return view('evaluasi.kimiaklinik.grafiknilaimetode.apizscore_parameter', compact('param','tahun','type','siklus', 'id','data'));
    }

    public function apinilaisamaalat(\Illuminate\Http\Request $request, $id)
    {
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $data =  DB::table('tb_registrasi')
        ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
        ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
        ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
        ->where('tb_registrasi.id', $id)
        ->where('sub_bidang.id', '=', '2')
        ->where('tb_registrasi.status', 3)
        ->where(function($query)
            {
                $query->where('pemeriksaan2', 'done')
                    ->orwhere('pemeriksaan', 'done')
                    ->orwhere('rpr1', 'done')
                    ->orwhere('rpr2', 'done');
            })

        ->where(function($query)
            {
                $query->where('status_data1', 2)
                    ->orwhere('status_data2', 2)
                    ->orwhere('status_datarpr1', 2)
                    ->orwhere('status_datarpr2', 2);
            })
        ->first();
        $param = DB::table('tb_instrumen')->where('parameter.kategori','=','kimia klinik')
        ->join('hp_details','hp_details.alat','=','tb_instrumen.id')
        ->join('parameter','parameter.id','=','tb_instrumen.id_parameter')
        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
        ->leftJoin('tb_sd_median_alat',function($join) use($tahun,$siklus,$type){
            $join->on('tb_sd_median_alat.alat','=','tb_instrumen.id')
            ->where('tb_sd_median_alat.parameter','=',DB::raw('tb_instrumen.id_parameter'))
            ->where('tb_sd_median_alat.tahun','=',$tahun)
            ->where('tb_sd_median_alat.siklus','=',$siklus)
            ->where('tb_sd_median_alat.form','=','kimia klinik')
            ->where('tb_sd_median_alat.tipe','=',$type);
        })
        ->leftJoin('tb_ring_kk_alat',function($join) use($tahun,$siklus,$type){
            $join->on('tb_ring_kk_alat.instrumen_id','=','tb_instrumen.id')
            ->where('tb_ring_kk_alat.parameter','=',DB::raw('tb_instrumen.id_parameter'))
            ->where('tb_ring_kk_alat.tahun','=',$tahun)
            ->where('tb_ring_kk_alat.siklus','=',$siklus)
            ->where('tb_ring_kk_alat.form','=','kimia klinik')
            ->where('tb_ring_kk_alat.tipe','=',$type);
        })
        ->leftJoin('tb_zscore_alat',function($join) use($tahun,$siklus,$type,$id){
            $join->on('tb_zscore_alat.alat','=','tb_instrumen.id')
            ->where('tb_zscore_alat.parameter','=',DB::raw('tb_instrumen.id_parameter'))
            ->where('tb_zscore_alat.tahun','=',$tahun)
            ->where('tb_zscore_alat.id_registrasi','=',$id)
            ->where('tb_zscore_alat.siklus','=',$siklus)
            ->where('tb_zscore_alat.form','=','kimia klinik')
            ->where('tb_zscore_alat.type','=',$type);
        })
        ->where('hp_headers.id_registrasi','=',$id)
        ->where('hp_headers.type','=',$type)
        ->where('hp_headers.siklus','=',$siklus)
        ->whereRaw('hp_details.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")')
        ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
        ->select(
            'parameter.nama_parameter',
            'parameter.id as id_parameter',
            'tb_instrumen.instrumen as nama_instrumen',
            'tb_instrumen.id as id_instrumen',
            'hp_details.hasil_pemeriksaan',
            'tb_sd_median_alat.sd',
            'tb_sd_median_alat.median',
            'tb_ring_kk_alat.batas_bawah',
            'tb_ring_kk_alat.batas_atas',
            'tb_ring_kk_alat.ring_1_bawah',
            'tb_ring_kk_alat.ring_1_atas',
            'tb_ring_kk_alat.ring_2_bawah',
            'tb_ring_kk_alat.ring_2_atas',
            'tb_ring_kk_alat.ring_3_bawah',
            'tb_ring_kk_alat.ring_3_atas',
            'tb_zscore_alat.zscore as zscore',
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                    AND
                HPD.parameter_id = tb_ring_kk_alat.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                HPD.hasil_pemeriksaan < tb_ring_kk_alat.batas_bawah
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-")
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as out_bawah'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                    AND
                HPD.parameter_id = tb_ring_kk_alat.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_1_bawah AND tb_ring_kk_alat.ring_1_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_1'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                    AND
                HPD.parameter_id = tb_ring_kk_alat.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_2_bawah AND tb_ring_kk_alat.ring_2_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_2'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                    AND
                HPD.parameter_id = tb_ring_kk_alat.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_3_bawah AND tb_ring_kk_alat.ring_3_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_3'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                    AND
                HPD.parameter_id = tb_ring_kk_alat.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                HPD.hasil_pemeriksaan > tb_ring_kk_alat.batas_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as out_atas'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                        TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) < -3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_3'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) < -2
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) >= -3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara23'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= -1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) >= -2
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara12'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > -1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 1
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara11'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 2
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_antara12'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 2
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_antara23'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_3')
        )
        // ->where()
        ->get();
        return view('evaluasi.kimiaklinik.grafiknilaialat.apizscore_parameter', compact('param','tahun','type','siklus', 'id','data'));
    }
    public function apinilaisama(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $data =  DB::table('tb_registrasi')
            ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
            ->where('tb_registrasi.id', $id)
            ->where('sub_bidang.id', '=', '2')
            ->where('tb_registrasi.status', 3)
            ->where(function($query)
                {
                    $query->where('pemeriksaan2', 'done')
                        ->orwhere('pemeriksaan', 'done')
                        ->orwhere('rpr1', 'done')
                        ->orwhere('rpr2', 'done');
                })

            ->where(function($query)
                {
                    $query->where('status_data1', 2)
                        ->orwhere('status_data2', 2)
                        ->orwhere('status_datarpr1', 2)
                        ->orwhere('status_datarpr2', 2);
                })
            ->first();
        if ($data->periode == 2 && $siklus == 1) {
            $periode = "periode = 2";
        }else{
            $periode = "periode IS NULL";
        }
        $param = DB::table('parameter')->where('parameter.kategori','=','kimia klinik')
            ->join('hp_details','hp_details.parameter_id','=','parameter.id')
            ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
            ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
            ->leftJoin('tb_sd_median',function($join) use($tahun,$siklus,$type,$periode){
                $join->on('tb_sd_median.parameter','=','parameter.id')
                ->where('tb_sd_median.tahun','=',$tahun)
                ->where('tb_sd_median.siklus','=',$siklus)
                ->where('tb_sd_median.form','=','kimia klinik')
                ->whereRaw('tb_sd_median.'.$periode)
                ->where('tb_sd_median.tipe','=',$type);
            })
            ->leftJoin('tb_ring_kk',function($join) use($tahun,$siklus,$type){
                $join->on('tb_ring_kk.parameter','=','parameter.id')
                ->where('tb_ring_kk.tahun','=',$tahun)
                ->where('tb_ring_kk.siklus','=',$siklus)
                ->where('tb_ring_kk.form','=','kimia klinik')
                ->where('tb_ring_kk.tipe','=',$type);
            })
            ->leftJoin('tb_zscore',function($join) use($tahun,$siklus,$type,$id){
                $join->on('tb_zscore.parameter','=','parameter.id')
                ->where('tb_zscore.tahun','=',$tahun)
                ->where('tb_zscore.id_registrasi','=',$id)
                ->where('tb_zscore.siklus','=',$siklus)
                ->where('tb_zscore.form','=','kimia klinik')
                ->where('tb_zscore.type','=',$type);
            })
            ->where('hp_headers.id_registrasi','=',$id)
            ->where('hp_headers.type','=',$type)
            ->where('hp_headers.siklus','=',$siklus)
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
            ->whereRaw('hp_details.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")')
            ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
            ->select(
                'parameter.nama_parameter', 'parameter.id as id_parameter', 'hp_details.hasil_pemeriksaan', 'tb_sd_median.sd', 'tb_sd_median.median', 'tb_ring_kk.batas_bawah', 'tb_ring_kk.batas_atas', 'tb_ring_kk.ring_1_bawah', 'tb_ring_kk.ring_1_atas', 'tb_ring_kk.ring_2_bawah', 'tb_ring_kk.ring_2_atas', 'tb_ring_kk.ring_3_bawah', 'tb_ring_kk.ring_3_atas', 'tb_ring_kk.ring_4_bawah', 'tb_ring_kk.ring_4_atas', 'tb_ring_kk.ring_5_bawah', 'tb_ring_kk.ring_5_atas',
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.parameter_id = tb_ring_kk.parameter
                        AND
                    HPD.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as out_bawah'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.parameter_id = tb_ring_kk.parameter
                        AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_1_bawah AND tb_ring_kk.ring_1_atas
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_1'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.parameter_id = tb_ring_kk.parameter
                        AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_2_bawah AND tb_ring_kk.ring_2_atas
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_2'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.parameter_id = tb_ring_kk.parameter
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_3_bawah AND tb_ring_kk.ring_3_atas
                            AND
                        HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                            AND
                        HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_3'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.parameter_id = tb_ring_kk.parameter
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_4_bawah AND tb_ring_kk.ring_4_atas
                            AND
                        HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                            AND
                        HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_4'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.parameter_id = tb_ring_kk.parameter
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_5_bawah AND tb_ring_kk.ring_5_atas
                            AND
                        HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                            AND
                        HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_5'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.parameter_id = tb_ring_kk.parameter
                        AND
                    HPD.hasil_pemeriksaan > tb_ring_kk.batas_atas
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as out_atas')
            )
            ->get();
        $paramalat = DB::table('tb_instrumen')->where('parameter.kategori','=','kimia klinik')
            ->join('hp_details','hp_details.alat','=','tb_instrumen.id')
            ->join('parameter','parameter.id','=','tb_instrumen.id_parameter')
            ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
            ->leftJoin('tb_sd_median_alat',function($join) use($tahun,$siklus,$type,$periode){
                $join->on('tb_sd_median_alat.alat','=','tb_instrumen.id')
                ->where('tb_sd_median_alat.parameter','=',DB::raw('tb_instrumen.id_parameter'))
                ->where('tb_sd_median_alat.tahun','=',$tahun)
                ->where('tb_sd_median_alat.siklus','=',$siklus)
                ->where('tb_sd_median_alat.form','=','kimia klinik')
                ->whereRaw('tb_sd_median_alat.'.$periode)
                ->where('tb_sd_median_alat.tipe','=',$type);
            })
            ->leftJoin('tb_ring_kk_alat',function($join) use($tahun,$siklus,$type){
                $join->on('tb_ring_kk_alat.instrumen_id','=','tb_instrumen.id')
                ->where('tb_ring_kk_alat.parameter','=',DB::raw('tb_instrumen.id_parameter'))
                ->where('tb_ring_kk_alat.tahun','=',$tahun)
                ->where('tb_ring_kk_alat.siklus','=',$siklus)
                ->where('tb_ring_kk_alat.form','=','kimia klinik')
                ->where('tb_ring_kk_alat.tipe','=',$type);
            })
            ->leftJoin('tb_zscore_alat',function($join) use($tahun,$siklus,$type,$id){
                $join->on('tb_zscore_alat.alat','=','tb_instrumen.id')
                ->where('tb_zscore_alat.parameter','=',DB::raw('tb_instrumen.id_parameter'))
                ->where('tb_zscore_alat.tahun','=',$tahun)
                ->where('tb_zscore_alat.id_registrasi','=',$id)
                ->where('tb_zscore_alat.siklus','=',$siklus)
                ->where('tb_zscore_alat.form','=','kimia klinik')
                ->where('tb_zscore_alat.type','=',$type);
            })
            ->where('hp_headers.id_registrasi','=',$id)
            ->where('hp_headers.type','=',$type)
            ->where('hp_headers.siklus','=',$siklus)
            ->whereRaw('hp_details.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")')
            ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
            ->select(
                'parameter.nama_parameter',
                'parameter.id as id_parameter',
                'tb_instrumen.instrumen as nama_instrumen',
                'tb_instrumen.id as id_instrumen',
                'hp_details.hasil_pemeriksaan',
                'tb_sd_median_alat.sd',
                'tb_sd_median_alat.median',
                'tb_ring_kk_alat.batas_bawah',
                'tb_ring_kk_alat.batas_atas',
                'tb_ring_kk_alat.ring_1_bawah',
                'tb_ring_kk_alat.ring_1_atas',
                'tb_ring_kk_alat.ring_2_bawah',
                'tb_ring_kk_alat.ring_2_atas',
                'tb_ring_kk_alat.ring_3_bawah',
                'tb_ring_kk_alat.ring_3_atas',
                'tb_ring_kk_alat.ring_4_bawah',
                'tb_ring_kk_alat.ring_4_atas',
                'tb_ring_kk_alat.ring_5_bawah',
                'tb_ring_kk_alat.ring_5_atas',
                'tb_zscore_alat.zscore as zscore',
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                        AND
                    HPD.parameter_id = tb_ring_kk_alat.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPD.hasil_pemeriksaan < tb_ring_kk_alat.batas_bawah
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-")
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as out_bawah'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                        AND
                    HPD.parameter_id = tb_ring_kk_alat.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_1_bawah AND tb_ring_kk_alat.ring_1_atas
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_1'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                        AND
                    HPD.parameter_id = tb_ring_kk_alat.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_2_bawah AND tb_ring_kk_alat.ring_2_atas
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_2'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                        AND
                    HPD.parameter_id = tb_ring_kk_alat.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_3_bawah AND tb_ring_kk_alat.ring_3_atas
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_3'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                        AND
                    HPD.parameter_id = tb_ring_kk_alat.parameter
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_4_bawah AND tb_ring_kk_alat.ring_4_atas
                            AND
                        HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                            AND
                        HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_4'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                        AND
                    HPD.parameter_id = tb_ring_kk_alat.parameter
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_5_bawah AND tb_ring_kk_alat.ring_5_atas
                            AND
                        HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                            AND
                        HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_5'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                        AND
                    HPD.parameter_id = tb_ring_kk_alat.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPD.hasil_pemeriksaan > tb_ring_kk_alat.batas_atas
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as out_atas')
            )
            // ->where()
            ->get();
        $parammetode = DB::table('metode_pemeriksaan')->where('parameter.kategori','=','kimia klinik')
            ->join('hp_details','hp_details.kode_metode_pemeriksaan','=','metode_pemeriksaan.id')
            ->join('parameter','parameter.id','=','metode_pemeriksaan.parameter_id')
            ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
            ->leftJoin('tb_sd_median_metode',function($join) use($tahun,$siklus,$type,$periode){
                $join->on('tb_sd_median_metode.metode','=','metode_pemeriksaan.id')
                ->where('tb_sd_median_metode.parameter','=',DB::raw('metode_pemeriksaan.parameter_id'))
                ->where('tb_sd_median_metode.tahun','=',$tahun)
                ->where('tb_sd_median_metode.siklus','=',$siklus)
                ->where('tb_sd_median_metode.form','=','kimia klinik')
                ->whereRaw('tb_sd_median_metode.'.$periode)
                ->where('tb_sd_median_metode.tipe','=',$type);
            })
            ->leftJoin('tb_ring_kk_metode',function($join) use($tahun,$siklus,$type){
                $join->on('tb_ring_kk_metode.metode_id','=','metode_pemeriksaan.id')
                ->where('tb_ring_kk_metode.parameter','=',DB::raw('metode_pemeriksaan.parameter_id'))
                ->where('tb_ring_kk_metode.tahun','=',$tahun)
                ->where('tb_ring_kk_metode.siklus','=',$siklus)
                ->where('tb_ring_kk_metode.form','=','kimia klinik')
                ->where('tb_ring_kk_metode.tipe','=',$type);
            })
            ->leftJoin('tb_zscore_metode',function($join) use($tahun,$siklus,$type,$id){
                $join->on('tb_zscore_metode.metode','=','metode_pemeriksaan.id')
                ->where('tb_zscore_metode.parameter','=',DB::raw('metode_pemeriksaan.parameter_id'))
                ->where('tb_zscore_metode.tahun','=',$tahun)
                ->where('tb_zscore_metode.id_registrasi','=',$id)
                ->where('tb_zscore_metode.siklus','=',$siklus)
                ->where('tb_zscore_metode.form','=','kimia klinik')
                ->where('tb_zscore_metode.type','=',$type);
            })
            ->where('hp_headers.id_registrasi','=',$id)
            ->where('hp_headers.type','=',$type)
            ->where('hp_headers.siklus','=',$siklus)
            ->whereRaw('hp_details.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")')
            ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
            ->select(
                'parameter.nama_parameter',
                'parameter.id as id_parameter',
                'metode_pemeriksaan.metode_pemeriksaan as nama_metode',
                'metode_pemeriksaan.id as id_metode',
                'hp_details.hasil_pemeriksaan',
                'tb_sd_median_metode.sd',
                'tb_sd_median_metode.median',
                'tb_ring_kk_metode.batas_bawah',
                'tb_ring_kk_metode.batas_atas',
                'tb_ring_kk_metode.ring_1_bawah',
                'tb_ring_kk_metode.ring_1_atas',
                'tb_ring_kk_metode.ring_2_bawah',
                'tb_ring_kk_metode.ring_2_atas',
                'tb_ring_kk_metode.ring_3_bawah',
                'tb_ring_kk_metode.ring_3_atas',
                'tb_ring_kk_metode.ring_4_bawah',
                'tb_ring_kk_metode.ring_4_atas',
                'tb_ring_kk_metode.ring_5_bawah',
                'tb_ring_kk_metode.ring_5_atas',
                'tb_zscore_metode.zscore as zscore',
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                        AND
                    HPD.parameter_id = tb_ring_kk_metode.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPD.hasil_pemeriksaan < tb_ring_kk_metode.batas_bawah
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as out_bawah'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                        AND
                    HPD.parameter_id = tb_ring_kk_metode.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_1_bawah AND tb_ring_kk_metode.ring_1_atas
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_1'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                        AND
                    HPD.parameter_id = tb_ring_kk_metode.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_2_bawah AND tb_ring_kk_metode.ring_2_atas
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_2'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                        AND
                    HPD.parameter_id = tb_ring_kk_metode.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_3_bawah AND tb_ring_kk_metode.ring_3_atas
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_3'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                        AND
                    HPD.parameter_id = tb_ring_kk_metode.parameter
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_4_bawah AND tb_ring_kk_metode.ring_4_atas
                            AND
                        HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                            AND
                        HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_4'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                        AND
                    HPD.parameter_id = tb_ring_kk_metode.parameter
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_5_bawah AND tb_ring_kk_metode.ring_5_atas
                            AND
                        HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                            AND
                        HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_5'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                        AND
                    HPD.parameter_id = tb_ring_kk_metode.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPD.hasil_pemeriksaan > tb_ring_kk_metode.batas_atas
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as out_atas')
            )
            // ->where()
            ->get();
        return view('evaluasi.kimiaklinik.grafiknilai.apizscore_parameter', compact('param','paramalat','parammetode','tahun','type','siklus', 'id','data'));
    }

    public function apigzhemsemua(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        
        $type = $input['x'];
        $siklus = $input['y'];
        // dd($type);

        $instansi =  DB::table('tb_registrasi')
        ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
        ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
        ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
        ->where('tb_registrasi.id', $id)
        ->where('sub_bidang.id', '=', '1')
        ->where('tb_registrasi.status', 3)
        ->where(function($query)
            {
                $query->where('pemeriksaan2', 'done')
                    ->orwhere('pemeriksaan', 'done')
                    ->orwhere('rpr1', 'done')
                    ->orwhere('rpr2', 'done');
            })

        ->where(function($query)
            {
                $query->where('status_data1', 2)
                    ->orwhere('status_data2', 2)
                    ->orwhere('status_datarpr1', 2)
                    ->orwhere('status_datarpr2', 2);
            })
        ->first();

        // $typeHeader = $input['x'];
        $reg = DB::table('tb_registrasi')->find($id);

        if ($reg->periode == 2 && $siklus == 1) {
            $periode = "AND tb_registrasi.periode = 2";
        }else{
            $periode = "";
        }

        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->orderBy('bagian','ASC')->get();

        foreach ($parameter as $key => $param) {
            $zscore = DB::table("tb_zscore")
                        ->where('id_registrasi', $id)
                        ->where('parameter', $param->id)
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $tahun)
                        ->first();
            $param->zscore = $zscore;
            $zscore_alat = DB::table("tb_zscore_alat")
                        ->where('id_registrasi', $id)
                        ->where('parameter', $param->id)
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $tahun)
                        ->first();
            $param->zscore_alat = $zscore_alat;
            $zscore_metode = DB::table("tb_zscore_metode")
                        ->where('id_registrasi', $id)
                        ->where('parameter', $param->id)
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $tahun)
                        ->first();
            $param->zscore_metode = $zscore_metode;

            $dataReg = DB::table('tb_registrasi')->where('tb_registrasi.id',$id)
                        ->leftJoin('hp_headers',function($join){
                            $join->on('tb_registrasi.id','=','hp_headers.id_registrasi')
                            ->where('hp_headers.type','=',$type)
                            ->where('hp_headers.siklus', $siklus);
                        })
                        ->leftJoin('hp_details',function($join){
                            $join->on('hp_headers.id','=','hp_details.hp_header_id')
                            ->where('hp_details.parameter_id', $param->id);
                        })
                        ->first();
            $param->dataReg = $dataReg;

            $alatmetode = DB::table('hp_headers')
                            ->join('hp_details', 'hp_details.hp_header_id', '=', 'hp_headers.id')
                            ->where('hp_headers.id_registrasi', $id)
                            ->where('hp_headers.type','=', $type)
                            ->where('hp_headers.siklus', $siklus)
                            ->where('hp_details.parameter_id', $param->id)
                            ->first();
            // dd($alatmetode->alat);
            $alat = $alatmetode->alat;
            $param->alat = $alat;

            $metode = $alatmetode->kode_metode_pemeriksaan;
            $param->metode = $metode;

            $sql ="
                SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi where not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'hematologi' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION

                SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'hematologi' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore >= -2 and zscore <= 2, 1, 0)) as nilai1, '-2 sd 2' as nilainya, '2' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'hematologi' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '3' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'hematologi' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '4' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'hematologi' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter";

            $sql2 ="SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -3 UNION

                    SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3 UNION

                    SELECT count(*) as nilai1, '-2 sd 2' as nilainya, 'c' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore >= -2 AND zscore <= 2 UNION

                    SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'd' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 2 AND zscore <= 3 UNION

                    SELECT count(*) as nilai1, '> 3' as nilainya, 'e' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 3";

            $data = DB::table(DB::raw("($sql) zone"))
                    ->select('*')
                    ->where('form','=','hematologi')
                    ->where('tahun','=',$tahun)
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->data = $data;
            $datap = DB::table(DB::raw("($sql2) zone"))
                    ->select('*')
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->datap = $datap;



            $alatna = DB::table('tb_instrumen')->where('id', $alat)->first();
            $param->alatna = $alatna;
            $sql_alat ="
                SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi where not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'hematologi' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'hematologi' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
            
                SELECT sum(if(zscore >= -2 and zscore <= 2, 1, 0)) as nilai1, '-2 sd 2' as nilainya, '2' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'hematologi' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '3' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'hematologi' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '4' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'hematologi' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter";

            $sql2_alat =" 
                    SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore < -3
                    UNION
                    
                    SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                    UNION
                    
                    SELECT count(*) as nilai1, '-2 sd 2' as nilainya, 'c' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore >= -2 AND zscore <= 2
                    UNION
                    
                    SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'd' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 2 AND zscore <= 3
                    UNION
                    
                    SELECT count(*) as nilai1, '> 3' as nilainya, 'e' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 3";

            $data_alat = DB::table(DB::raw("($sql_alat) zone"))
                    ->select('*')
                    ->where('form','=','hematologi')
                    ->where('tahun','=',$tahun)
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->data_alat = $data_alat;
            $datap_alat = DB::table(DB::raw("($sql2_alat) zone"))
                    ->select('*')
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->datap_alat = $datap_alat;

            $metodena = DB::table('metode_pemeriksaan')->where('id', $metode)->first();
            $param->metodena = $metodena;

            $sql_metode ="
                SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi where not zscore = '-' AND tahun = '".$tahun."' AND form = 'hematologi' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND form = 'hematologi' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
              
                SELECT sum(if(zscore >= -2 and zscore <= 2, 1, 0)) as nilai1, '-2 sd 2' as nilainya, '2' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND form = 'hematologi' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION

                SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '3' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND form = 'hematologi' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION

                SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '4' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND form = 'hematologi' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter";

            $sql2_metode =" 
                SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore < -3 UNION

                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3 UNION
                      
                SELECT count(*) as nilai1, '-2 sd 2' as nilainya, 'd' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode_b."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore >= -2 AND zscore <= 2 UNION

                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 2 AND zscore <= 3 UNION

                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 3";

            $data_metode = DB::table(DB::raw("($sql_metode) zone"))
                    ->select('*')
                    ->where('form','=','hematologi')
                    ->where('tahun','=',$tahun)
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->data_metode = $data_metode;

            $datap_metode = DB::table(DB::raw("($sql2_metode) zone"))
                    ->select('*')
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->datap_metode = $datap_metode;
        }
        // dd($parameter);
        return view('evaluasi.hematologi.grafiksemua.apigrafik', compact('tahun','type','siklus','input','reg','parameter','instansi'));
    }

    public function apighemnilaisama(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        // dd($input);
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $siklus = $input['y'];
        $type = $input['x'];

        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->orderBy('bagian', 'asc')->get();
        foreach ($parameter as $key => $val) {
            $dataReg = DB::table('tb_registrasi')->where('tb_registrasi.id',$id)
                            ->leftJoin('hp_headers',function($join)use($siklus, $type){
                                $join->on('tb_registrasi.id','=','hp_headers.id_registrasi')->where('hp_headers.type',$type)->where('hp_headers.siklus',$siklus);
                            })
                            ->leftJoin('hp_details',function($join) use($val){
                                $join->on('hp_headers.id','=','hp_details.hp_header_id');
                                $join->where('hp_details.parameter_id','=',$val->id);
                            })
                            ->first();
            $val->dataReg = $dataReg;
            // dd($dataReg);

            $parametera = DB::table('parameter')->where('id','=',$val->id)->first();
            $val->parameter = $parametera;

            $instrumen = DB::table('tb_instrumen')->where('id','=',$dataReg->alat)->first();
            $val->instrumen = $instrumen;
            
            $metode = DB::table('metode_pemeriksaan')->where('id','=',$dataReg->kode_metode_pemeriksaan)->first();
            $val->metode = $metode;


            $grafik = DB::select("
            SELECT
                tb_ring.ring1,tb_ring.ring2,tb_ring.ring3,tb_ring.ring4,tb_ring.ring5,
                SUM(CASE WHEN hp_details.hasil_pemeriksaan < cast(ring1 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_bawah_param,
                SUM(CASE WHEN hp_details.hasil_pemeriksaan >= cast(ring1 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring2 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_1_param,
                SUM(CASE WHEN hp_details.hasil_pemeriksaan >= cast(ring2 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring3 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_2_param,
                SUM(CASE WHEN hp_details.hasil_pemeriksaan >= cast(ring3 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring4 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_3_param,
                SUM(CASE WHEN hp_details.hasil_pemeriksaan >= cast(ring4 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan <= cast(ring5 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_4_param,
                SUM(CASE WHEN hp_details.hasil_pemeriksaan > cast(ring5 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_atas_param,
                SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan < cast(ring1 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_bawah_alat,
                SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan >= cast(ring1 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring2 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_1_alat,
                SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan >= cast(ring2 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring3 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_2_alat,
                SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan >= cast(ring3 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring4 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_3_alat,
                SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan >= cast(ring4 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan <= cast(ring5 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_4_alat,
                SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan > cast(ring5 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_atas_alat,
                SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan < cast(ring1 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_bawah_metode,
                SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= cast(ring1 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring2 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_1_metode,
                SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= cast(ring2 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring3 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_2_metode,
                SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= cast(ring3 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring4 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_3_metode,
                SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= cast(ring4 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan <= cast(ring5 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_4_metode,
                SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan > cast(ring5 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_atas_metode
            FROM    tb_ring
            LEFT JOIN (
                SELECT hp_details.id,hp_details.parameter_id,hp_headers.type,hp_headers.siklus,tb_registrasi.created_at,hp_details.hasil_pemeriksaan,hp_details.alat,hp_details.kode_metode_pemeriksaan,hp_headers.id_registrasi 
                FROM tb_registrasi
                INNER JOIN hp_headers ON hp_headers.id_registrasi = tb_registrasi.id
                INNER JOIN hp_details ON hp_headers.id = hp_details.hp_header_id
            ) as hp_details ON hp_details.parameter_id = tb_ring.parameter
            WHERE
            tb_ring.parameter =".$dataReg->parameter_id."
            AND 
            YEAR(created_at) = ".$tahun."
            AND
            tb_ring.siklus = ".$siklus."
            AND
            tb_ring.tipe = '".$type."'
            AND
            tb_ring.form = 'hematologi'
            and
            tb_ring.tahun = ".$tahun."
            AND hp_details.type = '".$type."'
            AND hp_details.siklus = ".$siklus."
            ");

            if(!empty($grafik)){
                $grafik = $grafik[0];
            }
            $val->grafik = $grafik;

            $median = DB::table('tb_sd_median')
                    ->where('parameter', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tipe', '=', $type)
                    ->where('tahun', $tahun)
                    ->first();
            $val->median = $median;
            // dd($median);
            $medianalat = DB::table('tb_sd_median_alat')
                    ->where('parameter', $val->id)
                    ->where('alat', $dataReg->alat)
                    ->where('siklus', $siklus)
                    ->where('tipe', '=', $type)
                    ->where('tahun', $tahun)
                    ->first();
            $val->medianalat = $medianalat;

            $medianmetode = DB::table('tb_sd_median_metode')
                    ->where('parameter', $val->id)
                    ->where('metode', $dataReg->kode_metode_pemeriksaan)
                    ->where('siklus', $siklus)
                    ->where('tipe', '=', $type)
                    ->where('tahun', $tahun)
                    ->first();
            $val->medianmetode = $medianmetode;
        }
        // dd($grafik_b);
        return view('evaluasi.hematologi.grafiknilai.apigrafik', compact('parameter','tahun','siklus','input','type'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = DB::table('provinces')
        //         ->join('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
        //         ->select(DB::raw('provinces.name,count(DISTINCT perusahaan.id) AS Provinsi, (SELECT count(*) from perusahaan) as jumlah, provinces.alias as Alias'))
        //         ->groupBy('provinces.name')
        //         ->groupBy('provinces.Alias')
        //         ->get();
        // foreach ($data as $key => $r) {
        //     $r->percentage = $r->Provinsi / $r->jumlah * 100;
        // }
        // // dd($data);

        return view('dashboard.grafikwilayah.index');
    }

    public function wilayahg(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $siklus = $request->get('y');

        $data = DB::table('provinces')
                ->join('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                ->select(DB::raw('provinces.name,count(DISTINCT perusahaan.id) AS Provinsi, (SELECT count(*) from perusahaan) as jumlah, provinces.alias as Alias'))
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('tb_registrasi.siklus', $siklus)
                ->groupBy('provinces.name')
                ->groupBy('provinces.Alias')
                ->get();
        $rows = array();
        foreach ($data as $key => $r) {
            $row['y'] = $r->Provinsi / $r->jumlah * 100;
            $row['bukan'] = $r->Provinsi;
            $row['name'] = $r->name;
            array_push($rows, $row);
        }

        return response ($rows);
    }

    public function badan(){
        return view('dashboard.grafikbadan.index');
    }
    public function badang(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $siklus = $request->get('y');

        $data = DB::table('badan_usaha')
                ->join('perusahaan', 'badan_usaha.id', '=', 'perusahaan.pemerintah')
                ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                ->select(DB::raw('badan_usaha.badan_usaha,count(DISTINCT perusahaan.id) AS Badan, (SELECT count(*) from perusahaan) as jumlah, badan_usaha.alias as Alias'))
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('tb_registrasi.siklus', $siklus)
                ->groupBy('badan_usaha.badan_usaha')
                ->groupBy('badan_usaha.Alias')
                ->get();
        $rows = array();
        foreach ($data as $key => $r) {
            $row['y'] = $r->Badan / $r->jumlah * 100;
            $row['bukan'] = $r->Badan;
            $row['name'] = $r->Alias;
            array_push($rows, $row);
        }
        // dd($data);
        return response ($rows);
    }

    public function bidang()
    {
        return view('dashboard.grafikbidang.index');
    }

    public function bidangg(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $siklus = $request->get('y');
        $data = DB::table('sub_bidang')
                ->join('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->select(DB::raw('sub_bidang.alias,count(DISTINCT tb_registrasi.id) AS Bidang, (SELECT count(*) from tb_registrasi) as jumlah'))
                ->groupBy('sub_bidang.alias')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('tb_registrasi.siklus', $siklus)
                ->orderBy('sub_bidang.id', 'asc')
                ->get();
        $rows = array();
        foreach ($data as $key => $r) {
            $row['bukan'] = $r->Bidang;
            $row['y'] = $r->Bidang / $r->jumlah * 100;
            $row['name'] = $r->alias;
            array_push($rows, $row);
        }
        return response ($rows);
    }

    public function pembayaran()
    {
        return view('dashboard.grafikpembayaran.index', compact('data'));
    }
    public function typeg(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $siklus = $request->get('y');

        $data = DB::table('pembayaran')
                ->join('tb_registrasi', 'pembayaran.id', '=', 'tb_registrasi.id_pembayaran')
                ->select(DB::raw('pembayaran.type,count(DISTINCT tb_registrasi.id) AS Type, (SELECT count(*) from tb_registrasi) as jumlah'))
                ->groupBy('pembayaran.type')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('tb_registrasi.siklus', $siklus)
                ->get();
        $rows = array();
        foreach ($data as $key => $r) {
            $row['y'] = $r->Type / $r->jumlah * 100;
            $row['bukan'] = $r->Type;
            $row['name'] = $r->type;
            array_push($rows, $row);
        }
        return response ($rows);
    }


    public function perusahaanhem(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.bidang','=','1')
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="hematologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.hematologi.grafik.perusahaan');
    }

    public function perusahaanhemsemua(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.bidang','=', Auth::user()->penyelenggara)
                ->where('tb_registrasi.status','>=','2')
                ->where(function($query) use ($siklus)
                    {
                        $query->where('tb_registrasi.siklus', '12')
                            ->orwhere('tb_registrasi.siklus', $siklus);
                    })
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="hematologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Proses
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.hematologi.grafiksemua.perusahaan');
    }

    public function dataperusahaanhemsemua(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '1')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.hematologi.grafiksemua.dataperusahaan', compact('data', 'siklus', 'tahun'));
    }

    public function perusahaankksemua(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.bidang','=', Auth::user()->penyelenggara)
                ->where('tb_registrasi.status','>=','2')
                ->where(function($query) use ($siklus)
                    {
                        $query->where('tb_registrasi.siklus', '12')
                            ->orwhere('tb_registrasi.siklus', $siklus);
                    })
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="kimiaklinik/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Proses
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.kimiaklinik.grafiksemua.perusahaan');
    }

    public function dataperusahaankksemua(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '2')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.kimiaklinik.grafiksemua.dataperusahaan', compact('data', 'siklus', 'tahun'));
    }

    public function dataperusahaanhem($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '1')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.hematologi.grafik.dataperusahaan', compact('data'));
    }

    public function dataperusahaanalathem($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '1')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.hematologi.grafikalat.dataperusahaan', compact('data'));
    }

    public function dataperusahaanmetodehem($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '1')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.hematologi.grafikmetode.dataperusahaan', compact('data'));
    }

    public function perusahaankimnilai(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*,tb_registrasi.kode_lebpes,tb_registrasi.id as idregistrasi, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->where('tb_registrasi.bidang','=','2')
            ->where('tb_registrasi.status','>=','2')
            ->where(function($query) use ($siklus){
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $siklus);
                })
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
            ->orderBy('kode_lebpes','asc')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = db::table('tb_zscore')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->orderBy('type', 'desc')
                                ->orderBy('id', 'asc')
                                ->first();
                        if(!empty($evaluasi)){
                            if ($evaluasi->type == 'a') {
                                return "".'<a href="kimiaklinik/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                                <button class="btn btn-primary btn-xs">
                                                    Grafik
                                                </button>
                                            </a>';
                            }else{
                                return "".'<a href="kimiaklinik/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                                <button class="btn btn-info btn-xs">
                                                    Grafik
                                                </button>
                                            </a>';
                            }
                        }else{
                            return "".'<a href="kimiaklinik/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                Grafik
                                            </button>
                                        </a>';
                        }
                        ;})
            ->make(true);
        }
        return view('evaluasi.kimiaklinik.grafiknilai.perusahaan');
    }

    public function dataperusahaankimnilai(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '2')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.kimiaklinik.grafiknilai.dataperusahaan', compact('data','siklus'));
    }

    public function perusahaankimnilaisertifikat(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*,tb_registrasi.kode_lebpes, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
            ->where('tb_registrasi.status','>=','2')
            ->where(function($query) use ($siklus)
                {
                    $query->where('tb_registrasi.siklus', '=', '12')
                        ->orwhere('tb_registrasi.siklus', '=', $siklus);
                })
            ->orderBy('kode_lebpes','asc')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Cetak
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        $bidang = Auth::user()->penyelenggara;
        return view('evaluasi.kimiaklinik.grafiknilai.perusahaansertifikat', compact('bidang', 'siklus'));
    }

    public function dataperusahaankimnilaisertifikat(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '2')
                ->where('tb_registrasi.status', 3)
                // ->where(function($query)
                //     {
                //         $query->where('pemeriksaan2', 'done')
                //             ->orwhere('pemeriksaan', 'done')
                //             ->orwhere('rpr1', 'done')
                //             ->orwhere('rpr2', 'done');
                //     })

                // ->where(function($query)
                //     {
                //         $query->where('status_data1', 2)
                //             ->orwhere('status_data2', 2)
                //             ->orwhere('status_datarpr1', 2)
                //             ->orwhere('status_datarpr2', 2);
                //     })
                ->get();
        // dd($data);
        return view('evaluasi.kimiaklinik.grafiknilai.dataperusahaansertifikat', compact('data', 'siklus'));
    }

    public function dataperusahaanhemnilaialat($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '1')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.hematologi.grafiknilaialat.dataperusahaan', compact('data'));
    }

    public function dataperusahaankimnilaialat(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '2')
                ->where('tb_registrasi.status', 3)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.kimiaklinik.grafiknilaialat.dataperusahaan', compact('data', 'siklus'));
    }

    public function dataperusahaankimnilaimetode(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '2')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.kimiaklinik.grafiknilaimetode.dataperusahaan', compact('data', 'siklus'));
    }

    public function dataperusahaanhemnilaimetode($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '1')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.hematologi.grafiknilaimetode.dataperusahaan', compact('data'));
    }

    public function perusahaanhemnilai(\Illuminate\Http\Request $request)
    {

        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpesan'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.bidang','=','1')
                ->where('tb_registrasi.status','>=','2')
                ->where(function($query) use ($siklus)
                    {
                        $query->where('tb_registrasi.siklus', '12')
                            ->orwhere('tb_registrasi.siklus', $siklus);
                    })
                ->get();
            // dd($datas);
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="hematologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Proses
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.hematologi.grafiknilai.perusahaan');
    }

    public function dataperusahaanhemnilai(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '1')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.hematologi.grafiknilai.dataperusahaan', compact('data','siklus','tahun'));
    }

    public function perusahaankimairnilai(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','11');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="kimiaair/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.kimiaair.grafiknilai.perusahaan');
    }

    public function dataperusahaankimairnilai($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '11')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.kimiaair.grafiknilai.dataperusahaan', compact('data'));
    }

    public function perusahaankimairterbatasnilai(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','12');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="kimiaairterbatas/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.kimiaairterbatas.grafiknilai.perusahaan');
    }

    public function dataperusahaankimairterbatasnilai($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '12')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.kimiaairterbatas.grafiknilai.dataperusahaan', compact('data'));
    }

    public function perusahaankim(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','2')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="kimiaklinik/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.kimiaklinik.grafik.perusahaan');
    }

    public function dataperusahaankim($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '2')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.kimiaklinik.grafik.dataperusahaan', compact('data'));
    }

    public function dataperusahaankimalat($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '2')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.kimiaklinik.grafikalat.dataperusahaan', compact('data'));
    }

    public function dataperusahaankimmetode($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '2')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.kimiaklinik.grafikmetode.dataperusahaan', compact('data'));
    }

    public function perusahaankimair(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.id as id_registrasi'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)->where('tb_registrasi.bidang','=','11')->where('tb_registrasi.status','>=','2');
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_zscore')
                                ->where('siklus', $siklus)
                                ->where('id_registrasi', $data->id_registrasi)
                                ->get();
                    if (count($evaluasi)) {
                        return "".'<a href="kimiaair/'.$data->id.'?siklus='.$siklus.'&tahun'.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            Evaluasi
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="kimiaair/'.$data->id.'?siklus='.$siklus.'&tahun'.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            Evaluasi
                                        </button>
                                    </a>';
                    }
                })
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore')
                                ->where('id_registrasi', $data->id_registrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->id_registrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_data2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia'])
            ->make(true);
        }
        return view('evaluasi.kimiaair.grafik.perusahaan');
    }

    public function dataperusahaankimair(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '11')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.kimiaair.grafik.dataperusahaan', compact('data', 'siklus'));
    }


    public function perusahaankimairterbatas(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','12')->where('tb_registrasi.status','>=','2')->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun);
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_zscore')
                                ->where('siklus', $siklus)
                                ->where('id_registrasi', $data->id_registrasi)
                                ->get();
                    if (count($evaluasi)) {
                        return "".'<a href="kimiaairterbatas/'.$data->id.'?siklus='.$siklus.'&tahun'.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            Evaluasi
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="kimiaairterbatas/'.$data->id.'?siklus='.$siklus.'&tahun'.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            Evaluasi
                                        </button>
                                    </a>';
                    }
                })
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore')
                                ->where('id_registrasi', $data->id_registrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->id_registrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_data2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia'])
            ->make(true);
        }
        return view('evaluasi.kimiaairterbatas.grafik.perusahaan');
    }

    public function dataperusahaankimairterbatas(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '12')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.kimiaairterbatas.grafik.dataperusahaan', compact('data','siklus'));
    }

    public function zhemsemua(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $type = $request->get('x');
        $arrType    = [
            'a'=> '01',
            'b'=> '02'
        ];
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','hematologi')->get();
        $alat = DB::table('tb_instrumen')->where('id_parameter','=','hematologi')->get();
        return view('evaluasi.hematologi.grafiksemua.zscore_parameter', compact('param','tahun','type','siklus', 'id', 'alat','arrType'));
    }

    public function gzhemsemua(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        
        $type = $input['x'];
        $siklus = $input['y'];
        // dd($type);

        $instansi =  DB::table('tb_registrasi')
        ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
        ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
        ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
        ->where('tb_registrasi.id', $id)
        ->where('sub_bidang.id', '=', '1')
        ->where('tb_registrasi.status', 3)
        ->where(function($query)
            {
                $query->where('pemeriksaan2', 'done')
                    ->orwhere('pemeriksaan', 'done')
                    ->orwhere('rpr1', 'done')
                    ->orwhere('rpr2', 'done');
            })

        ->where(function($query)
            {
                $query->where('status_data1', 2)
                    ->orwhere('status_data2', 2)
                    ->orwhere('status_datarpr1', 2)
                    ->orwhere('status_datarpr2', 2);
            })
        ->first();

        // $typeHeader = $input['x'];
        $reg = DB::table('tb_registrasi')->find($id);

        if ($reg->periode == 2 && $siklus == 1) {
            $periode = "AND tb_registrasi.periode = 2";
        }else{
            $periode = "";
        }

        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->orderBy('bagian','ASC')->get();

        foreach ($parameter as $key => $param) {
            $zscore = DB::table("tb_zscore")
                        ->where('id_registrasi', $id)
                        ->where('parameter', $param->id)
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $tahun)
                        ->first();
            $param->zscore = $zscore;
            $zscore_alat = DB::table("tb_zscore_alat")
                        ->where('id_registrasi', $id)
                        ->where('parameter', $param->id)
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $tahun)
                        ->first();
            $param->zscore_alat = $zscore_alat;
            $zscore_metode = DB::table("tb_zscore_metode")
                        ->where('id_registrasi', $id)
                        ->where('parameter', $param->id)
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $tahun)
                        ->first();
            $param->zscore_metode = $zscore_metode;

            $dataReg = DB::table('tb_registrasi')->where('tb_registrasi.id',$id)
                        ->leftJoin('hp_headers',function($join){
                            $join->on('tb_registrasi.id','=','hp_headers.id_registrasi')
                            ->where('hp_headers.type','=',$type)
                            ->where('hp_headers.siklus', $siklus);
                        })
                        ->leftJoin('hp_details',function($join){
                            $join->on('hp_headers.id','=','hp_details.hp_header_id')
                            ->where('hp_details.parameter_id', $param->id);
                        })
                        ->first();
            $param->dataReg = $dataReg;

            $alatmetode = DB::table('hp_headers')
                            ->join('hp_details', 'hp_details.hp_header_id', '=', 'hp_headers.id')
                            ->where('hp_headers.id_registrasi', $id)
                            ->where('hp_headers.type','=', $type)
                            ->where('hp_headers.siklus', $siklus)
                            ->where('hp_details.parameter_id', $param->id)
                            ->first();
            // dd($alatmetode->alat);
            $alat = $alatmetode->alat;
            $param->alat = $alat;

            $metode = $alatmetode->kode_metode_pemeriksaan;
            $param->metode = $metode;

            $sql ="
                SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi where not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'hematologi' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION

                SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'hematologi' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore >= -2 and zscore <= 2, 1, 0)) as nilai1, '-2 sd 2' as nilainya, '2' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'hematologi' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '3' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'hematologi' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '4' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'hematologi' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter";

            $sql2 ="SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -3 UNION

                    SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3 UNION

                    SELECT count(*) as nilai1, '-2 sd 2' as nilainya, 'c' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore >= -2 AND zscore <= 2 UNION

                    SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'd' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 2 AND zscore <= 3 UNION

                    SELECT count(*) as nilai1, '> 3' as nilainya, 'e' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 3";

            $data = DB::table(DB::raw("($sql) zone"))
                    ->select('*')
                    ->where('form','=','hematologi')
                    ->where('tahun','=',$tahun)
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->data = $data;
            $datap = DB::table(DB::raw("($sql2) zone"))
                    ->select('*')
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->datap = $datap;



            $alatna = DB::table('tb_instrumen')->where('id', $alat)->first();
            $param->alatna = $alatna;
            $sql_alat ="
                SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi where not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'hematologi' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'hematologi' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
            
                SELECT sum(if(zscore >= -2 and zscore <= 2, 1, 0)) as nilai1, '-2 sd 2' as nilainya, '2' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'hematologi' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '3' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'hematologi' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '4' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'hematologi' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter";

            $sql2_alat =" 
                    SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore < -3
                    UNION
                    
                    SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                    UNION
                    
                    SELECT count(*) as nilai1, '-2 sd 2' as nilainya, 'c' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore >= -2 AND zscore <= 2
                    UNION
                    
                    SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'd' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 2 AND zscore <= 3
                    UNION
                    
                    SELECT count(*) as nilai1, '> 3' as nilainya, 'e' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 3";

            $data_alat = DB::table(DB::raw("($sql_alat) zone"))
                    ->select('*')
                    ->where('form','=','hematologi')
                    ->where('tahun','=',$tahun)
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->data_alat = $data_alat;
            $datap_alat = DB::table(DB::raw("($sql2_alat) zone"))
                    ->select('*')
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->datap_alat = $datap_alat;

            $metodena = DB::table('metode_pemeriksaan')->where('id', $metode)->first();
            $param->metodena = $metodena;

            $sql_metode ="
                SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi where not zscore = '-' AND tahun = '".$tahun."' AND form = 'hematologi' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND form = 'hematologi' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
              
                SELECT sum(if(zscore >= -2 and zscore <= 2, 1, 0)) as nilai1, '-2 sd 2' as nilainya, '2' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND form = 'hematologi' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION

                SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '3' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND form = 'hematologi' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION

                SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '4' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND form = 'hematologi' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter";

            $sql2_metode =" 
                SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore < -3 UNION

                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3 UNION
                      
                SELECT count(*) as nilai1, '-2 sd 2' as nilainya, 'd' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode_b."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore >= -2 AND zscore <= 2 UNION

                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 2 AND zscore <= 3 UNION

                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 3";

            $data_metode = DB::table(DB::raw("($sql_metode) zone"))
                    ->select('*')
                    ->where('form','=','hematologi')
                    ->where('tahun','=',$tahun)
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->data_metode = $data_metode;

            $datap_metode = DB::table(DB::raw("($sql2_metode) zone"))
                    ->select('*')
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->datap_metode = $datap_metode;
        }
        // dd($parameter);
        return view('evaluasi.hematologi.grafiksemua.grafik', compact('tahun','type','siklus','input','reg','parameter','instansi'));
    }

    public function zkksemua(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $type = $request->get('x');
        $arrType    = [
            'a'=> '01',
            'b'=> '02'
        ];
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        $alat = DB::table('tb_instrumen')->where('id_parameter','=','kimia klinik')->get();
        return view('evaluasi.kimiaklinik.grafiksemua.zscore_parameter', compact('param','tahun','type','siklus', 'id', 'alat','arrType'));
    }

    public function gzkksemua(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        
        $type = $input['x'];
        $siklus = $input['y'];
        // dd($type);

        $instansi =  DB::table('tb_registrasi')
            ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
            ->where('tb_registrasi.id', $id)
            ->where('sub_bidang.id', '=', '2')
            ->where('tb_registrasi.status', 3)
            ->where(function($query)
                {
                    $query->where('pemeriksaan2', 'done')
                        ->orwhere('pemeriksaan', 'done')
                        ->orwhere('rpr1', 'done')
                        ->orwhere('rpr2', 'done');
                })

            ->where(function($query)
                {
                    $query->where('status_data1', 2)
                        ->orwhere('status_data2', 2)
                        ->orwhere('status_datarpr1', 2)
                        ->orwhere('status_datarpr2', 2);
                })
            ->first();

        // $typeHeader = $input['x'];
        $reg = DB::table('tb_registrasi')->find($id);

        if ($reg->periode == 2 && $siklus == 1) {
            $periode = "AND tb_registrasi.periode = 2";
        }else{
            $periode = "";
        }

        $parameter = DB::table('parameter')
                        ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                        ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                        ->where('parameter.kategori','=','kimia klinik')
                        ->where('tb_registrasi.id', $id)
                        ->select('parameter.*')
                        ->get();

        foreach ($parameter as $key => $param) {
            $zscore = DB::table("tb_zscore")
                        ->where('id_registrasi', $id)
                        ->where('parameter', $param->id)
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $tahun)
                        ->first();
            $param->zscore = $zscore;
            $zscore_alat = DB::table("tb_zscore_alat")
                        ->where('id_registrasi', $id)
                        ->where('parameter', $param->id)
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $tahun)
                        ->first();
            $param->zscore_alat = $zscore_alat;
            $zscore_metode = DB::table("tb_zscore_metode")
                        ->where('id_registrasi', $id)
                        ->where('parameter', $param->id)
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $tahun)
                        ->first();
            $param->zscore_metode = $zscore_metode;

            $dataReg = DB::table('tb_registrasi')->where('tb_registrasi.id',$id)
                        ->leftJoin('hp_headers',function($join){
                            $join->on('tb_registrasi.id','=','hp_headers.id_registrasi')
                            ->where('hp_headers.type','=',$type)
                            ->where('hp_headers.siklus', $siklus);
                        })
                        ->leftJoin('hp_details',function($join){
                            $join->on('hp_headers.id','=','hp_details.hp_header_id')
                            ->where('hp_details.parameter_id', $param->id);
                        })
                        ->first();
            $param->dataReg = $dataReg;

            $alatmetode = DB::table('hp_headers')
                            ->join('hp_details', 'hp_details.hp_header_id', '=', 'hp_headers.id')
                            ->where('hp_headers.id_registrasi', $id)
                            ->where('hp_headers.type','=', $type)
                            ->where('hp_headers.siklus', $siklus)
                            ->where('hp_details.parameter_id', $param->id)
                            ->first();
            // dd($alatmetode->alat);
            $alat = $alatmetode->alat;
            $param->alat = $alat;

            $metode = $alatmetode->kode_metode_pemeriksaan;
            $param->metode = $metode;

            $sql ="
                SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi where not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'kimia klinik' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION

                SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'kimia klinik' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore >= -2 and zscore <= 2, 1, 0)) as nilai1, '-2 sd 2' as nilainya, '2' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'kimia klinik' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '3' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'kimia klinik' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '4' as sort, form, tahun, parameter, type, tb_zscore.siklus, id_registrasi
                FROM `tb_zscore` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak di Evaluasi' AND form = 'kimia klinik' AND type = '".$type."' AND tb_zscore.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter";

            $sql2 ="SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -3 UNION

                    SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3 UNION

                    SELECT count(*) as nilai1, '-2 sd 2' as nilainya, 'c' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore >= -2 AND zscore <= 2 UNION

                    SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'd' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 2 AND zscore <= 3 UNION

                    SELECT count(*) as nilai1, '> 3' as nilainya, 'e' as sort from tb_zscore
                    where parameter = '".$param->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 3";

            $data = DB::table(DB::raw("($sql) zone"))
                    ->select('*')
                    ->where('form','=','kimia klinik')
                    ->where('tahun','=',$tahun)
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->data = $data;
            $datap = DB::table(DB::raw("($sql2) zone"))
                    ->select('*')
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->datap = $datap;



            $alatna = DB::table('tb_instrumen')->where('id', $alat)->first();
            $param->alatna = $alatna;
            $sql_alat ="
                SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi where not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'kimia klinik' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'kimia klinik' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
            
                SELECT sum(if(zscore >= -2 and zscore <= 2, 1, 0)) as nilai1, '-2 sd 2' as nilainya, '2' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'kimia klinik' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '3' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'kimia klinik' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '4' as sort, form, tahun, alat, parameter, type, tb_zscore_alat.siklus, id_registrasi
                FROM `tb_zscore_alat` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_alat.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND not zscore = 'Tidak dapat dievaluasi' AND form = 'kimia klinik' AND alat = '".$alat."' AND type = '".$type."' AND tb_zscore_alat.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter";

            $sql2_alat =" 
                    SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore < -3
                    UNION
                    
                    SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                    UNION
                    
                    SELECT count(*) as nilai1, '-2 sd 2' as nilainya, 'c' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore >= -2 AND zscore <= 2
                    UNION
                    
                    SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'd' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 2 AND zscore <= 3
                    UNION
                    
                    SELECT count(*) as nilai1, '> 3' as nilainya, 'e' as sort from tb_zscore_alat
                    where parameter = '".$param->id."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 3";

            $data_alat = DB::table(DB::raw("($sql_alat) zone"))
                    ->select('*')
                    ->where('form','=','kimia klinik')
                    ->where('tahun','=',$tahun)
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->data_alat = $data_alat;
            $datap_alat = DB::table(DB::raw("($sql2_alat) zone"))
                    ->select('*')
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->datap_alat = $datap_alat;

            $metodena = DB::table('metode_pemeriksaan')->where('id', $metode)->first();
            $param->metodena = $metodena;

            $sql_metode ="
                SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi where not zscore = '-' AND tahun = '".$tahun."' AND form = 'kimia klinik' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
                
                SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND form = 'kimia klinik' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION
              
                SELECT sum(if(zscore >= -2 and zscore <= 2, 1, 0)) as nilai1, '-2 sd 2' as nilainya, '2' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND form = 'kimia klinik' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION

                SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '3' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND form = 'kimia klinik' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter UNION

                SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '4' as sort, form, tahun, metode, parameter, type, tb_zscore_metode.siklus, id_registrasi
                FROM `tb_zscore_metode` INNER JOIN tb_registrasi ON tb_registrasi.id = tb_zscore_metode.id_registrasi WHERE not zscore = '-' AND tahun = '".$tahun."' AND form = 'kimia klinik' AND metode = '".$metode."' AND type = '".$type."' AND tb_zscore_metode.siklus = ".$siklus." AND parameter = ".$param->id." ".$periode." GROUP BY parameter";

            $sql2_metode =" 
                SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore < -3 UNION

                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3 UNION
                      
                SELECT count(*) as nilai1, '-2 sd 2' as nilainya, 'd' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode_b."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore >= -2 AND zscore <= 2 UNION

                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 2 AND zscore <= 3 UNION

                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore_metode
                where parameter = '".$param->id."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 3";

            $data_metode = DB::table(DB::raw("($sql_metode) zone"))
                    ->select('*')
                    ->where('form','=','kimia klinik')
                    ->where('tahun','=',$tahun)
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->data_metode = $data_metode;

            $datap_metode = DB::table(DB::raw("($sql2_metode) zone"))
                    ->select('*')
                    ->orderBy('sort', 'asc')
                    ->get();
            $param->datap_metode = $datap_metode;
        }
        // dd($parameter);
        return view('evaluasi.kimiaklinik.grafiksemua.grafik', compact('tahun','type','siklus','input','reg','parameter','instansi'));
    }

    public function zhem(\Illuminate\Http\Request $request, $id)
    {
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','hematologi')->get();
        return view('evaluasi.hematologi.grafik.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function gzhem(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $parameter = $input['parameter'];
        $type = $input['type'];
        $siklus = $input['siklus'];

        $param = DB::table('parameter')->where('id',$parameter)->first();

        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` where form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '6' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'hematologi' GROUP BY parameter";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 3";

        $median ="SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_sd_median
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_sd_median
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median < -2 and median >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_sd_median
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median <= -1 AND median >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_sd_median
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > -1 AND median <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_sd_median
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 1 AND median <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_sd_median
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 2 AND median <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_sd_median
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 3";

        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','hematologi')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('type','=',$type)
                ->where('siklus','=',$siklus)
                ->orderBy('sort', 'asc')
                ->get();
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        $datam = DB::table(DB::raw("($median) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        // dd($datam);
        return view('evaluasi.hematologi.grafik.grafik', compact('data','tahun','type','siklus','param','datap','datam'));
    }

    public function alatzhem(\Illuminate\Http\Request $request, $id)
    {
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','hematologi')->get();
        $alat = DB::table('tb_instrumen')->where('id_parameter','=','hematologi')->get();
        // dd($alat);
        return view('evaluasi.hematologi.grafikalat.zscore_parameter', compact('alat','tahun','type','siklus', 'id', 'param'));
    }

    public function alatgzhem(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $alat = $input['alat'];
        $parameter = $input['parameter'];
        $type = $input['type'];
        $siklus = $input['siklus'];

        $param = DB::table('parameter')->where('id', $parameter)->first();
        $alatna = DB::table('tb_instrumen')->where('id', $alat)->first();
        // dd($alat);
        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` where form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '6' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'hematologi' GROUP BY parameter";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 3";

        $median ="SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_sd_median_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_sd_median_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median < -2 and median >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_sd_median_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median <= -1 AND median >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_sd_median_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > -1 AND median <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_sd_median_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 1 AND median <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_sd_median_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 2 AND median <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_sd_median_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 3";

        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','hematologi')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('alat','=',$alat)
                ->where('type','=',$type)
                ->where('siklus','=',$siklus)
                ->orderBy('sort', 'asc')
                ->get();
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        $datam = DB::table(DB::raw("($median) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        // dd($data);
        return view('evaluasi.hematologi.grafikalat.grafik', compact('data','tahun','type','siklus','param','datap','datam','alatna'));
    }

    public function getmetode($id){
        $data = DB::table('metode_pemeriksaan')->select('id as Kode','metode_pemeriksaan as Nama')->where('parameter_id', $id)->get();
        return response()->json(['Hasil'=>$data]);
    }

    public function getmetodesemua($id){
        $data_metode = DB::table('metode_pemeriksaan')->select('id as Kode','metode_pemeriksaan as Nama')->where('parameter_id', $id)->get();
        $data_alat = DB::table('tb_instrumen')->select('id as Kode','instrumen as Nama')->where('id_parameter', 'hematologi')->get();
        return response()->json(['Hasil'=>$data_metode, 'Data'=>$data_alat]);
    }

    public function metodezhem(\Illuminate\Http\Request $request, $id)
    {
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','hematologi')->get();
        // dd($alat);
        return view('evaluasi.hematologi.grafikmetode.zscore_parameter', compact('tahun','type','siklus', 'id', 'param'));
    }

    public function metodegzhem(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $metode = $input['metode'];
        $parameter = $input['parameter'];
        $type = $input['type'];
        $siklus = $input['siklus'];

        $param = DB::table('parameter')->where('id', $parameter)->first();
        $metodena = DB::table('metode_pemeriksaan')->where('id', $metode)->first();
        // dd($alat);
        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` where form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '6' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'hematologi' GROUP BY parameter";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 3";

        $median ="SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_sd_median_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_sd_median_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median < -2 and median >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_sd_median_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median <= -1 AND median >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_sd_median_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > -1 AND median <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_sd_median_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 1 AND median <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_sd_median_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 2 AND median <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_sd_median_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 3";

        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','hematologi')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('metode','=',$metode)
                ->where('type','=',$type)
                ->where('siklus','=',$siklus)
                ->orderBy('sort', 'asc')
                ->get();
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        $datam = DB::table(DB::raw("($median) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        // dd($metode);
        return view('evaluasi.hematologi.grafikmetode.grafik', compact('data','tahun','type','siklus','param','datap','datam','metodena'));
    }

    public function nilaisama(\Illuminate\Http\Request $request, $id)
    {
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $data =  DB::table('tb_registrasi')
            ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
            ->where('tb_registrasi.id', $id)
            ->where('sub_bidang.id', '=', '2')
            ->where('tb_registrasi.status', 3)
            ->where(function($query)
                {
                    $query->where('pemeriksaan2', 'done')
                        ->orwhere('pemeriksaan', 'done')
                        ->orwhere('rpr1', 'done')
                        ->orwhere('rpr2', 'done');
                })

            ->where(function($query)
                {
                    $query->where('status_data1', 2)
                        ->orwhere('status_data2', 2)
                        ->orwhere('status_datarpr1', 2)
                        ->orwhere('status_datarpr2', 2);
                })
            ->first();
        if ($data->periode == 2 && $siklus == 1) {
            $periode = "periode = 2";
        }else{
            $periode = "periode IS NULL";
        }
        $param = DB::table('parameter')->where('parameter.kategori','=','kimia klinik')
            ->join('hp_details','hp_details.parameter_id','=','parameter.id')
            ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
            ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
            ->leftJoin('tb_sd_median',function($join) use($tahun,$siklus,$type,$periode){
                $join->on('tb_sd_median.parameter','=','parameter.id')
                ->where('tb_sd_median.tahun','=',$tahun)
                ->where('tb_sd_median.siklus','=',$siklus)
                ->where('tb_sd_median.form','=','kimia klinik')
                ->whereRaw('tb_sd_median.'.$periode)
                ->where('tb_sd_median.tipe','=',$type);
            })
            ->leftJoin('tb_ring_kk',function($join) use($tahun,$siklus,$type){
                $join->on('tb_ring_kk.parameter','=','parameter.id')
                ->where('tb_ring_kk.tahun','=',$tahun)
                ->where('tb_ring_kk.siklus','=',$siklus)
                ->where('tb_ring_kk.form','=','kimia klinik')
                ->where('tb_ring_kk.tipe','=',$type);
            })
            ->leftJoin('tb_zscore',function($join) use($tahun,$siklus,$type,$id){
                $join->on('tb_zscore.parameter','=','parameter.id')
                ->where('tb_zscore.tahun','=',$tahun)
                ->where('tb_zscore.id_registrasi','=',$id)
                ->where('tb_zscore.siklus','=',$siklus)
                ->where('tb_zscore.form','=','kimia klinik')
                ->where('tb_zscore.type','=',$type);
            })
            ->where('hp_headers.id_registrasi','=',$id)
            ->where('hp_headers.type','=',$type)
            ->where('hp_headers.siklus','=',$siklus)
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
            ->whereRaw('hp_details.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")')
            ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
            ->select(
                'parameter.nama_parameter', 'parameter.id as id_parameter', 'hp_details.hasil_pemeriksaan', 'tb_sd_median.sd', 'tb_sd_median.median', 'tb_ring_kk.batas_bawah', 'tb_ring_kk.batas_atas', 'tb_ring_kk.ring_1_bawah', 'tb_ring_kk.ring_1_atas', 'tb_ring_kk.ring_2_bawah', 'tb_ring_kk.ring_2_atas', 'tb_ring_kk.ring_3_bawah', 'tb_ring_kk.ring_3_atas', 'tb_ring_kk.ring_4_bawah', 'tb_ring_kk.ring_4_atas', 'tb_ring_kk.ring_5_bawah', 'tb_ring_kk.ring_5_atas',
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.parameter_id = tb_ring_kk.parameter
                        AND
                    HPD.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as out_bawah'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.parameter_id = tb_ring_kk.parameter
                        AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_1_bawah AND tb_ring_kk.ring_1_atas
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_1'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.parameter_id = tb_ring_kk.parameter
                        AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_2_bawah AND tb_ring_kk.ring_2_atas
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_2'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.parameter_id = tb_ring_kk.parameter
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_3_bawah AND tb_ring_kk.ring_3_atas
                            AND
                        HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                            AND
                        HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_3'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.parameter_id = tb_ring_kk.parameter
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_4_bawah AND tb_ring_kk.ring_4_atas
                            AND
                        HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                            AND
                        HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_4'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.parameter_id = tb_ring_kk.parameter
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_5_bawah AND tb_ring_kk.ring_5_atas
                            AND
                        HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                            AND
                        HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_5'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.parameter_id = tb_ring_kk.parameter
                        AND
                    HPD.hasil_pemeriksaan > tb_ring_kk.batas_atas
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as out_atas')
            )
            ->orderBy('parameter.id', 'asc')
            ->get();
        $paramalat = DB::table('tb_instrumen')->where('parameter.kategori','=','kimia klinik')
            ->join('hp_details','hp_details.alat','=','tb_instrumen.id')
            ->join('parameter','parameter.id','=','tb_instrumen.id_parameter')
            ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
            ->leftJoin('tb_sd_median_alat',function($join) use($tahun,$siklus,$type,$periode){
                $join->on('tb_sd_median_alat.alat','=','tb_instrumen.id')
                ->where('tb_sd_median_alat.parameter','=',DB::raw('tb_instrumen.id_parameter'))
                ->where('tb_sd_median_alat.tahun','=',$tahun)
                ->where('tb_sd_median_alat.siklus','=',$siklus)
                ->where('tb_sd_median_alat.form','=','kimia klinik')
                ->whereRaw('tb_sd_median_alat.'.$periode)
                ->where('tb_sd_median_alat.tipe','=',$type);
            })
            ->leftJoin('tb_ring_kk_alat',function($join) use($tahun,$siklus,$type){
                $join->on('tb_ring_kk_alat.instrumen_id','=','tb_instrumen.id')
                ->where('tb_ring_kk_alat.parameter','=',DB::raw('tb_instrumen.id_parameter'))
                ->where('tb_ring_kk_alat.tahun','=',$tahun)
                ->where('tb_ring_kk_alat.siklus','=',$siklus)
                ->where('tb_ring_kk_alat.form','=','kimia klinik')
                ->where('tb_ring_kk_alat.tipe','=',$type);
            })
            ->leftJoin('tb_zscore_alat',function($join) use($tahun,$siklus,$type,$id){
                $join->on('tb_zscore_alat.alat','=','tb_instrumen.id')
                ->where('tb_zscore_alat.parameter','=',DB::raw('tb_instrumen.id_parameter'))
                ->where('tb_zscore_alat.tahun','=',$tahun)
                ->where('tb_zscore_alat.id_registrasi','=',$id)
                ->where('tb_zscore_alat.siklus','=',$siklus)
                ->where('tb_zscore_alat.form','=','kimia klinik')
                ->where('tb_zscore_alat.type','=',$type);
            })
            ->where('hp_headers.id_registrasi','=',$id)
            ->where('hp_headers.type','=',$type)
            ->where('hp_headers.siklus','=',$siklus)
            ->whereRaw('hp_details.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")')
            ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
            ->select(
                'parameter.nama_parameter',
                'parameter.id as id_parameter',
                'tb_instrumen.instrumen as nama_instrumen',
                'tb_instrumen.id as id_instrumen',
                'hp_details.hasil_pemeriksaan',
                'tb_sd_median_alat.sd',
                'tb_sd_median_alat.median',
                'tb_ring_kk_alat.batas_bawah',
                'tb_ring_kk_alat.batas_atas',
                'tb_ring_kk_alat.ring_1_bawah',
                'tb_ring_kk_alat.ring_1_atas',
                'tb_ring_kk_alat.ring_2_bawah',
                'tb_ring_kk_alat.ring_2_atas',
                'tb_ring_kk_alat.ring_3_bawah',
                'tb_ring_kk_alat.ring_3_atas',
                'tb_ring_kk_alat.ring_4_bawah',
                'tb_ring_kk_alat.ring_4_atas',
                'tb_ring_kk_alat.ring_5_bawah',
                'tb_ring_kk_alat.ring_5_atas',
                'tb_zscore_alat.zscore as zscore',
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                        AND
                    HPD.parameter_id = tb_ring_kk_alat.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPD.hasil_pemeriksaan < tb_ring_kk_alat.batas_bawah
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-")
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as out_bawah'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                        AND
                    HPD.parameter_id = tb_ring_kk_alat.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_1_bawah AND tb_ring_kk_alat.ring_1_atas
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_1'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                        AND
                    HPD.parameter_id = tb_ring_kk_alat.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_2_bawah AND tb_ring_kk_alat.ring_2_atas
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_2'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                        AND
                    HPD.parameter_id = tb_ring_kk_alat.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_3_bawah AND tb_ring_kk_alat.ring_3_atas
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_3'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                        AND
                    HPD.parameter_id = tb_ring_kk_alat.parameter
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_4_bawah AND tb_ring_kk_alat.ring_4_atas
                            AND
                        HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                            AND
                        HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_4'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                        AND
                    HPD.parameter_id = tb_ring_kk_alat.parameter
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_5_bawah AND tb_ring_kk_alat.ring_5_atas
                            AND
                        HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                            AND
                        HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_5'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                        AND
                    HPD.parameter_id = tb_ring_kk_alat.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPD.hasil_pemeriksaan > tb_ring_kk_alat.batas_atas
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as out_atas')
            )
            ->orderBy('parameter.id', 'asc')
            // ->where()
            ->get();
        $parammetode = DB::table('metode_pemeriksaan')->where('parameter.kategori','=','kimia klinik')
            ->join('hp_details','hp_details.kode_metode_pemeriksaan','=','metode_pemeriksaan.id')
            ->join('parameter','parameter.id','=','metode_pemeriksaan.parameter_id')
            ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
            ->leftJoin('tb_sd_median_metode',function($join) use($tahun,$siklus,$type,$periode){
                $join->on('tb_sd_median_metode.metode','=','metode_pemeriksaan.id')
                ->where('tb_sd_median_metode.parameter','=',DB::raw('metode_pemeriksaan.parameter_id'))
                ->where('tb_sd_median_metode.tahun','=',$tahun)
                ->where('tb_sd_median_metode.siklus','=',$siklus)
                ->where('tb_sd_median_metode.form','=','kimia klinik')
                ->whereRaw('tb_sd_median_metode.'.$periode)
                ->where('tb_sd_median_metode.tipe','=',$type);
            })
            ->leftJoin('tb_ring_kk_metode',function($join) use($tahun,$siklus,$type){
                $join->on('tb_ring_kk_metode.metode_id','=','metode_pemeriksaan.id')
                ->where('tb_ring_kk_metode.parameter','=',DB::raw('metode_pemeriksaan.parameter_id'))
                ->where('tb_ring_kk_metode.tahun','=',$tahun)
                ->where('tb_ring_kk_metode.siklus','=',$siklus)
                ->where('tb_ring_kk_metode.form','=','kimia klinik')
                ->where('tb_ring_kk_metode.tipe','=',$type);
            })
            ->leftJoin('tb_zscore_metode',function($join) use($tahun,$siklus,$type,$id){
                $join->on('tb_zscore_metode.metode','=','metode_pemeriksaan.id')
                ->where('tb_zscore_metode.parameter','=',DB::raw('metode_pemeriksaan.parameter_id'))
                ->where('tb_zscore_metode.tahun','=',$tahun)
                ->where('tb_zscore_metode.id_registrasi','=',$id)
                ->where('tb_zscore_metode.siklus','=',$siklus)
                ->where('tb_zscore_metode.form','=','kimia klinik')
                ->where('tb_zscore_metode.type','=',$type);
            })
            ->where('hp_headers.id_registrasi','=',$id)
            ->where('hp_headers.type','=',$type)
            ->where('hp_headers.siklus','=',$siklus)
            ->whereRaw('hp_details.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")')
            ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
            ->select(
                'parameter.nama_parameter',
                'parameter.id as id_parameter',
                'metode_pemeriksaan.metode_pemeriksaan as nama_metode',
                'metode_pemeriksaan.id as id_metode',
                'hp_details.hasil_pemeriksaan',
                'tb_sd_median_metode.sd',
                'tb_sd_median_metode.median',
                'tb_ring_kk_metode.batas_bawah',
                'tb_ring_kk_metode.batas_atas',
                'tb_ring_kk_metode.ring_1_bawah',
                'tb_ring_kk_metode.ring_1_atas',
                'tb_ring_kk_metode.ring_2_bawah',
                'tb_ring_kk_metode.ring_2_atas',
                'tb_ring_kk_metode.ring_3_bawah',
                'tb_ring_kk_metode.ring_3_atas',
                'tb_ring_kk_metode.ring_4_bawah',
                'tb_ring_kk_metode.ring_4_atas',
                'tb_ring_kk_metode.ring_5_bawah',
                'tb_ring_kk_metode.ring_5_atas',
                'tb_zscore_metode.zscore as zscore',
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                        AND
                    HPD.parameter_id = tb_ring_kk_metode.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPD.hasil_pemeriksaan < tb_ring_kk_metode.batas_bawah
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as out_bawah'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                        AND
                    HPD.parameter_id = tb_ring_kk_metode.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_1_bawah AND tb_ring_kk_metode.ring_1_atas
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_1'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                        AND
                    HPD.parameter_id = tb_ring_kk_metode.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_2_bawah AND tb_ring_kk_metode.ring_2_atas
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_2'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                        AND
                    HPD.parameter_id = tb_ring_kk_metode.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_3_bawah AND tb_ring_kk_metode.ring_3_atas
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_3'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                        AND
                    HPD.parameter_id = tb_ring_kk_metode.parameter
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_4_bawah AND tb_ring_kk_metode.ring_4_atas
                            AND
                        HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                            AND
                        HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_4'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                        AND
                    HPD.parameter_id = tb_ring_kk_metode.parameter
                        AND
                        HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_5_bawah AND tb_ring_kk_metode.ring_5_atas
                            AND
                        HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                            AND
                        HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as ring_5'),
                DB::raw('IFNULL((
                    SELECT COUNT(*) FROM hp_details as HPD
                    INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                    INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                    WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                        AND
                    HPD.parameter_id = tb_ring_kk_metode.parameter
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                        AND
                    HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                        AND
                    HPD.hasil_pemeriksaan > tb_ring_kk_metode.batas_atas
                        AND
                    HPH.siklus = "'.$siklus.'"
                        AND
                    HPH.type = "'.$type.'"
                        AND
                    YEAR(TBR.created_at) = "'.$tahun.'"
                ),0) as out_atas')
            )
            ->orderBy('parameter.id', 'asc')
            // ->where()
            ->get();
        return view('evaluasi.kimiaklinik.grafiknilai.zscore_parameter', compact('param','paramalat','parammetode','tahun','type','siklus', 'id','data'));
    }

    public function hemnilaisama(\Illuminate\Http\Request $request, $id)
    {
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $type       = $request->get('x');
        $arrType    = [
            'a'=> '01',
            'b'=> '02'
        ];
        $siklus     = $request->get('y');
        $id         = $id;
        $param      = DB::table('parameter')->where('kategori','=','hematologi')->get();
        return view('evaluasi.hematologi.grafiknilai.zscore_parameter', compact('param','tahun','type','siklus', 'id','arrType'));
    }

    public function ghemnilaisama(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        // dd($input);
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $input['y'];
        $type = $input['x'];

        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->orderBy('bagian', 'asc')->get();
        foreach ($parameter as $key => $val) {
            $dataReg = DB::table('tb_registrasi')->where('tb_registrasi.id',$id)
                            ->leftJoin('hp_headers',function($join)use($siklus, $type){
                                $join->on('tb_registrasi.id','=','hp_headers.id_registrasi')->where('hp_headers.type',$type)->where('hp_headers.siklus',$siklus);
                            })
                            ->leftJoin('hp_details',function($join) use($val){
                                $join->on('hp_headers.id','=','hp_details.hp_header_id');
                                $join->where('hp_details.parameter_id','=',$val->id);
                            })
                            ->first();
            $val->dataReg = $dataReg;
            // dd($dataReg);

            $parametera = DB::table('parameter')->where('id','=',$val->id)->first();
            $val->parameter = $parametera;

            $instrumen = DB::table('tb_instrumen')->where('id','=',$dataReg->alat)->first();
            $val->instrumen = $instrumen;
            
            $metode = DB::table('metode_pemeriksaan')->where('id','=',$dataReg->kode_metode_pemeriksaan)->first();
            $val->metode = $metode;


            $grafik = DB::select("
            SELECT
                tb_ring.ring1,tb_ring.ring2,tb_ring.ring3,tb_ring.ring4,tb_ring.ring5,
                SUM(CASE WHEN hp_details.hasil_pemeriksaan < cast(ring1 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_bawah_param,
                SUM(CASE WHEN hp_details.hasil_pemeriksaan >= cast(ring1 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring2 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_1_param,
                SUM(CASE WHEN hp_details.hasil_pemeriksaan >= cast(ring2 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring3 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_2_param,
                SUM(CASE WHEN hp_details.hasil_pemeriksaan >= cast(ring3 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring4 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_3_param,
                SUM(CASE WHEN hp_details.hasil_pemeriksaan >= cast(ring4 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan <= cast(ring5 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_4_param,
                SUM(CASE WHEN hp_details.hasil_pemeriksaan > cast(ring5 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_atas_param,
                SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan < cast(ring1 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_bawah_alat,
                SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan >= cast(ring1 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring2 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_1_alat,
                SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan >= cast(ring2 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring3 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_2_alat,
                SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan >= cast(ring3 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring4 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_3_alat,
                SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan >= cast(ring4 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan <= cast(ring5 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_4_alat,
                SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan > cast(ring5 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_atas_alat,
                SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan < cast(ring1 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_bawah_metode,
                SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= cast(ring1 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring2 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_1_metode,
                SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= cast(ring2 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring3 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_2_metode,
                SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= cast(ring3 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan < cast(ring4 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_3_metode,
                SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= cast(ring4 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan <= cast(ring5 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_4_metode,
                SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan > cast(ring5 AS DECIMAL(5,2)) AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_atas_metode
            FROM    tb_ring
            LEFT JOIN (
                SELECT hp_details.id,hp_details.parameter_id,hp_headers.type,hp_headers.siklus,tb_registrasi.created_at,hp_details.hasil_pemeriksaan,hp_details.alat,hp_details.kode_metode_pemeriksaan,hp_headers.id_registrasi 
                FROM tb_registrasi
                INNER JOIN hp_headers ON hp_headers.id_registrasi = tb_registrasi.id
                INNER JOIN hp_details ON hp_headers.id = hp_details.hp_header_id
            ) as hp_details ON hp_details.parameter_id = tb_ring.parameter
            WHERE
            tb_ring.parameter =".$dataReg->parameter_id."
            AND 
            YEAR(created_at) = ".$tahun."
            AND
            tb_ring.siklus = ".$siklus."
            AND
            tb_ring.tipe = '".$type."'
            AND
            tb_ring.form = 'hematologi'
            and
            tb_ring.tahun = ".$tahun."
            AND hp_details.type = '".$type."'
            AND hp_details.siklus = ".$siklus."
            ");

            if(!empty($grafik)){
                $grafik = $grafik[0];
            }
            $val->grafik = $grafik;

            $median = DB::table('tb_sd_median')
                    ->where('parameter', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tipe', '=', $type)
                    ->where('tahun', $tahun)
                    ->first();
            $val->median = $median;
            // dd($median);
            $medianalat = DB::table('tb_sd_median_alat')
                    ->where('parameter', $val->id)
                    ->where('alat', $dataReg->alat)
                    ->where('siklus', $siklus)
                    ->where('tipe', '=', $type)
                    ->where('tahun', $tahun)
                    ->first();
            $val->medianalat = $medianalat;

            $medianmetode = DB::table('tb_sd_median_metode')
                    ->where('parameter', $val->id)
                    ->where('metode', $dataReg->kode_metode_pemeriksaan)
                    ->where('siklus', $siklus)
                    ->where('tipe', '=', $type)
                    ->where('tahun', $tahun)
                    ->first();
            $val->medianmetode = $medianmetode;
        }
        // dd($parameter);
        // dd($grafik_b);
        return view('evaluasi.hematologi.grafiknilai.grafik', compact('parameter','tahun','siklus','input','type'));
    }

    public function kimiaairnilaisama(\Illuminate\Http\Request $request, $id)
    {
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','kimia air')->get();
        return view('evaluasi.kimiaair.grafiknilai.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function gkimiaairnilaisama(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $input = $request->all();
        $data = DB::table('parameter')
                ->where('parameter.kategori','=','kimia air')
                ->get();
        foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median')
                    ->where('parameter', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('form', '=', 'kimia air')
                    ->get();
            $val->sd = $sd;

            $zcount = DB::table('tb_zscore')
                        ->where('parameter', $val->id)
                        ->where('siklus', $siklus)
                        ->where('tahun', $date)
                        ->where('form','=','kimia air')
                        ->count();
            $val->count = $zcount;
        }

        $zscore = DB::table('tb_zscore')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('form','=','kimia air')
                    ->get();
        return view('evaluasi.kimiaair.grafiknilai.grafik', compact('data','sub','zscore','zscore1'));
    }

    public function kimiaairterbatasnilaisama(\Illuminate\Http\Request $request, $id)
    {
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','kimia air terbatas')->get();
        return view('evaluasi.kimiaairterbatas.grafiknilai.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function gkimiaairterbatasnilaisama(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $input = $request->all();
        $data = DB::table('parameter')
                ->where('parameter.kategori','=','kimia air terbatas')
                ->get();
        // dd($data);
         foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median')
                    ->where('parameter', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('form', '=', 'kimia air terbatas')
                    ->get();
            $val->sd = $sd;

            $zcount = DB::table('tb_zscore')
                        ->where('parameter', $val->id)
                        ->where('siklus', $siklus)
                        ->where('tahun', $date)
                        ->where('form','=','kimia air terbatas')
                        ->count();
            $val->count = $zcount;
        }

        $zscore = DB::table('tb_zscore')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('form','=','kimia air terbatas')
                    ->get();
        return view('evaluasi.kimiaairterbatas.grafiknilai.grafik', compact('data','sub','zscore','zscore1'));
    }

    public function gnilaisama(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $parameter = DB::table('parameter')->where('id', $input['parameter'])->first();
        $peserta = DB::table('hp_details')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_details.parameter_id', $input['parameter'])
                ->where('tb_registrasi.id', $id)
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', $input['type'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('hp_details.hasil_pemeriksaan')
                ->first();
        $ring = DB::table('tb_ring_kk')
                ->select('tb_ring_kk.*',
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_bawah'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_1_bawah AND tb_ring_kk.ring_1_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_1'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_2_bawah AND tb_ring_kk.ring_2_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_2'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_3_bawah AND tb_ring_kk.ring_3_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_bawah'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_3'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_4_bawah AND tb_ring_kk.ring_4_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_4'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_5_bawah AND tb_ring_kk.ring_5_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_5'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_atas')

                )
                ->where('parameter', $input['parameter'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $input['type'])
                ->where('tahun', $input['tahun'])
                ->first();
        $median = DB::table('tb_sd_median')
                ->where('parameter', $input['parameter'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $input['type'])
                ->where('tahun', $input['tahun'])
                ->first();
        // dd($median);
        return view('evaluasi.kimiaklinik.grafiknilai.grafik', compact('peserta','parameter','datas','ring', 'median'));
    }

    public function nilaisamaalat(\Illuminate\Http\Request $request, $id)
    {
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $data =  DB::table('tb_registrasi')
        ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
        ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
        ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
        ->where('tb_registrasi.id', $id)
        ->where('sub_bidang.id', '=', '2')
        ->where('tb_registrasi.status', 3)
        ->where(function($query)
            {
                $query->where('pemeriksaan2', 'done')
                    ->orwhere('pemeriksaan', 'done')
                    ->orwhere('rpr1', 'done')
                    ->orwhere('rpr2', 'done');
            })

        ->where(function($query)
            {
                $query->where('status_data1', 2)
                    ->orwhere('status_data2', 2)
                    ->orwhere('status_datarpr1', 2)
                    ->orwhere('status_datarpr2', 2);
            })
        ->first();
        $param = DB::table('tb_instrumen')->where('parameter.kategori','=','kimia klinik')
        ->join('hp_details','hp_details.alat','=','tb_instrumen.id')
        ->join('parameter','parameter.id','=','tb_instrumen.id_parameter')
        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
        ->leftJoin('tb_sd_median_alat',function($join) use($tahun,$siklus,$type){
            $join->on('tb_sd_median_alat.alat','=','tb_instrumen.id')
            ->where('tb_sd_median_alat.parameter','=',DB::raw('tb_instrumen.id_parameter'))
            ->where('tb_sd_median_alat.tahun','=',$tahun)
            ->where('tb_sd_median_alat.siklus','=',$siklus)
            ->where('tb_sd_median_alat.form','=','kimia klinik')
            ->where('tb_sd_median_alat.tipe','=',$type);
        })
        ->leftJoin('tb_ring_kk_alat',function($join) use($tahun,$siklus,$type){
            $join->on('tb_ring_kk_alat.instrumen_id','=','tb_instrumen.id')
            ->where('tb_ring_kk_alat.parameter','=',DB::raw('tb_instrumen.id_parameter'))
            ->where('tb_ring_kk_alat.tahun','=',$tahun)
            ->where('tb_ring_kk_alat.siklus','=',$siklus)
            ->where('tb_ring_kk_alat.form','=','kimia klinik')
            ->where('tb_ring_kk_alat.tipe','=',$type);
        })
        ->leftJoin('tb_zscore_alat',function($join) use($tahun,$siklus,$type,$id){
            $join->on('tb_zscore_alat.alat','=','tb_instrumen.id')
            ->where('tb_zscore_alat.parameter','=',DB::raw('tb_instrumen.id_parameter'))
            ->where('tb_zscore_alat.tahun','=',$tahun)
            ->where('tb_zscore_alat.id_registrasi','=',$id)
            ->where('tb_zscore_alat.siklus','=',$siklus)
            ->where('tb_zscore_alat.form','=','kimia klinik')
            ->where('tb_zscore_alat.type','=',$type);
        })
        ->where('hp_headers.id_registrasi','=',$id)
        ->where('hp_headers.type','=',$type)
        ->where('hp_headers.siklus','=',$siklus)
        ->whereRaw('hp_details.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")')
        ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
        ->select(
            'parameter.nama_parameter',
            'parameter.id as id_parameter',
            'tb_instrumen.instrumen as nama_instrumen',
            'tb_instrumen.id as id_instrumen',
            'hp_details.hasil_pemeriksaan',
            'tb_sd_median_alat.sd',
            'tb_sd_median_alat.median',
            'tb_ring_kk_alat.batas_bawah',
            'tb_ring_kk_alat.batas_atas',
            'tb_ring_kk_alat.ring_1_bawah',
            'tb_ring_kk_alat.ring_1_atas',
            'tb_ring_kk_alat.ring_2_bawah',
            'tb_ring_kk_alat.ring_2_atas',
            'tb_ring_kk_alat.ring_3_bawah',
            'tb_ring_kk_alat.ring_3_atas',
            'tb_zscore_alat.zscore as zscore',
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                    AND
                HPD.parameter_id = tb_ring_kk_alat.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                HPD.hasil_pemeriksaan < tb_ring_kk_alat.batas_bawah
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-")
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as out_bawah'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                    AND
                HPD.parameter_id = tb_ring_kk_alat.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_1_bawah AND tb_ring_kk_alat.ring_1_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_1'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                    AND
                HPD.parameter_id = tb_ring_kk_alat.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_2_bawah AND tb_ring_kk_alat.ring_2_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_2'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                    AND
                HPD.parameter_id = tb_ring_kk_alat.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_3_bawah AND tb_ring_kk_alat.ring_3_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_3'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                    AND
                HPD.parameter_id = tb_ring_kk_alat.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                HPD.hasil_pemeriksaan > tb_ring_kk_alat.batas_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as out_atas'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                        TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) < -3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_3'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) < -2
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) >= -3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara23'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= -1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) >= -2
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara12'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > -1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 1
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara11'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 2
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_antara12'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 2
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_antara23'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_3')
        )
        // ->where()
        ->get();
        return view('evaluasi.kimiaklinik.grafiknilaialat.zscore_parameter', compact('param','tahun','type','siklus', 'id','data'));
    }

    public function gnilaisamaalat(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $parameter = DB::table('parameter')->where('id', $input['parameter'])->first();
        $alat = DB::table('tb_instrumen')->where('id', $input['alat'])->first();
        $peserta = DB::table('hp_details')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_details.parameter_id', $input['parameter'])
                ->where('tb_instrumen.id', $input['alat'])
                ->where('tb_registrasi.id', $id)
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', $input['type'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('hp_details.hasil_pemeriksaan')
                ->first();
        $ring = DB::table('tb_ring_kk')
                ->select('tb_ring_kk.*',
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_instrumen ON tb_instrumen.id = hp_details.alat
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        tb_instrumen.id = "'.$input['alat'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_bawah'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_instrumen ON tb_instrumen.id = hp_details.alat
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_1_bawah AND tb_ring_kk.ring_1_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        tb_instrumen.id = "'.$input['alat'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_1'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_instrumen ON tb_instrumen.id = hp_details.alat
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_2_bawah AND tb_ring_kk.ring_2_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        tb_instrumen.id = "'.$input['alat'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_2'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_instrumen ON tb_instrumen.id = hp_details.alat
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_3_bawah AND tb_ring_kk.ring_3_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        tb_instrumen.id = "'.$input['alat'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_bawah'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_instrumen ON tb_instrumen.id = hp_details.alat
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        tb_instrumen.id = "'.$input['alat'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_3'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_instrumen ON tb_instrumen.id = hp_details.alat
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_4_bawah AND tb_ring_kk.ring_4_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        tb_instrumen.id = "'.$input['alat'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_4'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_instrumen ON tb_instrumen.id = hp_details.alat
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_5_bawah AND tb_ring_kk.ring_5_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        tb_instrumen.id = "'.$input['alat'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_5'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_instrumen ON tb_instrumen.id = hp_details.alat
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        tb_instrumen.id = "'.$input['alat'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_atas')

                )
                ->where('parameter', $input['parameter'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $input['type'])
                ->where('tahun', $input['tahun'])
                ->first();

        $median = DB::table('tb_sd_median_alat')
                ->where('parameter', $input['parameter'])
                ->where('alat', $input['alat'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $input['type'])
                ->where('tahun', $input['tahun'])
                ->first();
        // dd($median);
        return view('evaluasi.kimiaklinik.grafiknilaialat.grafik', compact('peserta','parameter','datas','ring', 'median', 'alat'));
    }

    public function hemnilaisamaalat(\Illuminate\Http\Request $request, $id)
    {
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','hematologi')->get();
        $alat = DB::table('hp_headers')
                ->join('hp_details', 'hp_details.hp_header_id','=', 'hp_headers.id')
                ->join('tb_instrumen', 'tb_instrumen.id', '=', 'hp_details.alat')
                ->where('id_parameter','=','hematologi')
                ->orderBy('bagian', 'asc')
                ->groupBy('hp_details.alat')
                ->select('hp_details.alat', 'tb_instrumen.instrumen')
                ->get();
        return view('evaluasi.hematologi.grafiknilaialat.zscore_parameter', compact('param','tahun','type','siklus', 'id', 'alat'));
    }

    public function ghemnilaisamaalat(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        // $parameter = DB::table('parameter')->where('id', $input['parameter'])->first();
        // $alat = DB::table('tb_instrumen')->where('id', $input['alat'])->first();
        $datapes = DB::table('hp_headers')
                ->join('hp_details', 'hp_details.hp_header_id','=', 'hp_headers.id')
                ->join('tb_instrumen', 'tb_instrumen.id', '=', 'hp_details.alat')
                ->leftjoin('parameter', 'parameter.id', '=', 'hp_details.parameter_id')
                ->where('id_parameter','=','hematologi')
                ->where('hp_headers.id_registrasi', $id)
                ->orderBy('parameter.bagian', 'asc')
                ->get();
        // dd($datapes);
        foreach ($datapes as $key => $val) {
            $data = DB::table('parameter')
                    ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                    ->join('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                    ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                    ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                    ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                    ->where('parameter.id', $val->parameter_id)
                    ->where('tb_instrumen.id', $val->alat)
                    ->where('hp_headers.siklus', $input['siklus'])
                    ->where('hp_headers.type', $input['type'])
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                    ->select('hp_details.id', 'hp_details.hasil_pemeriksaan', DB::raw('count(hp_details.hasil_pemeriksaan) as jumlah'))
                    ->groupBy('hp_details.id')
                    ->get();
                foreach ($data as $key => $valu) {
                    $peserta = DB::table('hp_details')
                            ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                            ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                            ->where('hp_details.id', $valu->id)
                            ->where('tb_registrasi.id', $id)
                            ->select('hp_details.hasil_pemeriksaan')
                            ->get();
                    $valu->peserta = $peserta;
                }
            $val->data = $data;
            $ring = DB::table('tb_ring')
                    ->where('parameter', $val->parameter_id)
                    ->where('siklus', $input['siklus'])
                    ->where('tipe', $input['type'])
                    ->where('tahun', $input['tahun'])
                    ->get();
            $val->ring = $ring;
            $median = DB::table('tb_sd_median_alat')
                    ->where('parameter', $val->parameter_id)
                    ->where('alat', $val->alat)
                    ->where('siklus', $input['siklus'])
                    ->where('tipe', $input['type'])
                    ->where('tahun', $input['tahun'])
                    ->first();
            $val->median = $median;
        }
             // dd($datapes);
        return view('evaluasi.hematologi.grafiknilaialat.grafik', compact('datapes','parameter','datas','ring', 'median', 'alat'));
    }

    public function hemnilaisamametode(\Illuminate\Http\Request $request, $id)
    {
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','hematologi')->get();
        return view('evaluasi.hematologi.grafiknilaimetode.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function ghemnilaisamametode(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        // $parameter = DB::table('parameter')->where('id', $input['parameter'])->first();
        // $metode = DB::table('metode_pemeriksaan')->where('id', $input['metode'])->first();

        $datapes = DB::table('hp_headers')
                ->join('hp_details', 'hp_details.hp_header_id','=', 'hp_headers.id')
                ->join('metode_pemeriksaan', 'metode_pemeriksaan.id', '=', 'hp_details.kode_metode_pemeriksaan')
                ->leftjoin('parameter', 'parameter.id', '=', 'hp_details.parameter_id')
                ->where('parameter.kategori','=','hematologi')
                ->where('hp_headers.id_registrasi', $id)
                ->orderBy('parameter.bagian', 'asc')
                ->get();
            foreach ($datapes as $key => $val) {
                $data = DB::table('parameter')
                        ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                        ->join('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                        ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                        ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                        ->where('parameter.id', $val->parameter_id)
                        ->where('metode_pemeriksaan.id', $val->kode_metode_pemeriksaan)
                        ->where('hp_headers.siklus', $input['siklus'])
                        ->where('hp_headers.type', $input['type'])
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->select('hp_details.id', 'hp_details.hasil_pemeriksaan', DB::raw('count(hp_details.hasil_pemeriksaan) as jumlah'))
                        ->groupBy('hp_details.id')
                        ->get();
                    foreach ($data as $key => $valu) {
                        $peserta = DB::table('hp_details')
                                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                                ->where('hp_details.id', $valu->id)
                                ->where('tb_registrasi.id', $id)
                                ->select('hp_details.hasil_pemeriksaan')
                                ->get();
                        $valu->peserta = $peserta;
                    }
                $val->data = $data;
                $ring = DB::table('tb_ring')
                        ->where('parameter', $val->parameter_id)
                        ->where('siklus', $input['siklus'])
                        ->where('tipe', $input['type'])
                        ->where('tahun', $input['tahun'])
                        ->get();
                $val->ring = $ring;

                $median = DB::table('tb_sd_median_metode')
                        ->where('parameter', $val->parameter_id)
                        ->where('metode', $val->kode_metode_pemeriksaan)
                        ->where('siklus', $input['siklus'])
                        ->where('tipe', $input['type'])
                        ->where('tahun', $input['tahun'])
                        ->first();
                $val->median = $median;
            }
        // dd($median);
        return view('evaluasi.hematologi.grafiknilaimetode.grafik', compact('datapes','parameter','datas','ring', 'median', 'metode'));
    }

    public function nilaisamametode(\Illuminate\Http\Request $request, $id)
    {
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $data =  DB::table('tb_registrasi')
        ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
        ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
        ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
        ->where('tb_registrasi.id', $id)
        ->where('sub_bidang.id', '=', '2')
        ->where('tb_registrasi.status', 3)
        ->where(function($query)
            {
                $query->where('pemeriksaan2', 'done')
                    ->orwhere('pemeriksaan', 'done')
                    ->orwhere('rpr1', 'done')
                    ->orwhere('rpr2', 'done');
            })

        ->where(function($query)
            {
                $query->where('status_data1', 2)
                    ->orwhere('status_data2', 2)
                    ->orwhere('status_datarpr1', 2)
                    ->orwhere('status_datarpr2', 2);
            })
        ->first();
        $param = DB::table('metode_pemeriksaan')->where('parameter.kategori','=','kimia klinik')
        ->join('hp_details','hp_details.kode_metode_pemeriksaan','=','metode_pemeriksaan.id')
        ->join('parameter','parameter.id','=','metode_pemeriksaan.parameter_id')
        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
        ->leftJoin('tb_sd_median_metode',function($join) use($tahun,$siklus,$type){
            $join->on('tb_sd_median_metode.metode','=','metode_pemeriksaan.id')
            ->where('tb_sd_median_metode.parameter','=',DB::raw('metode_pemeriksaan.parameter_id'))
            ->where('tb_sd_median_metode.tahun','=',$tahun)
            ->where('tb_sd_median_metode.siklus','=',$siklus)
            ->where('tb_sd_median_metode.form','=','kimia klinik')
            ->where('tb_sd_median_metode.tipe','=',$type);
        })
        ->leftJoin('tb_ring_kk_metode',function($join) use($tahun,$siklus,$type){
            $join->on('tb_ring_kk_metode.metode_id','=','metode_pemeriksaan.id')
            ->where('tb_ring_kk_metode.parameter','=',DB::raw('metode_pemeriksaan.parameter_id'))
            ->where('tb_ring_kk_metode.tahun','=',$tahun)
            ->where('tb_ring_kk_metode.siklus','=',$siklus)
            ->where('tb_ring_kk_metode.form','=','kimia klinik')
            ->where('tb_ring_kk_metode.tipe','=',$type);
        })
        ->leftJoin('tb_zscore_metode',function($join) use($tahun,$siklus,$type,$id){
            $join->on('tb_zscore_metode.metode','=','metode_pemeriksaan.id')
            ->where('tb_zscore_metode.parameter','=',DB::raw('metode_pemeriksaan.parameter_id'))
            ->where('tb_zscore_metode.tahun','=',$tahun)
            ->where('tb_zscore_metode.id_registrasi','=',$id)
            ->where('tb_zscore_metode.siklus','=',$siklus)
            ->where('tb_zscore_metode.form','=','kimia klinik')
            ->where('tb_zscore_metode.type','=',$type);
        })
        ->where('hp_headers.id_registrasi','=',$id)
        ->where('hp_headers.type','=',$type)
        ->where('hp_headers.siklus','=',$siklus)
        ->whereRaw('hp_details.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")')
        ->whereRaw('hp_details.hasil_pemeriksaan REGEXP \'[0-9]\'')
        ->select(
            'parameter.nama_parameter',
            'parameter.id as id_parameter',
            'metode_pemeriksaan.metode_pemeriksaan as nama_metode',
            'metode_pemeriksaan.id as id_metode',
            'hp_details.hasil_pemeriksaan',
            'tb_sd_median_metode.sd',
            'tb_sd_median_metode.median',
            'tb_ring_kk_metode.batas_bawah',
            'tb_ring_kk_metode.batas_atas',
            'tb_ring_kk_metode.ring_1_bawah',
            'tb_ring_kk_metode.ring_1_atas',
            'tb_ring_kk_metode.ring_2_bawah',
            'tb_ring_kk_metode.ring_2_atas',
            'tb_ring_kk_metode.ring_3_bawah',
            'tb_ring_kk_metode.ring_3_atas',
            'tb_zscore_metode.zscore as zscore',
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                    AND
                HPD.parameter_id = tb_ring_kk_metode.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                HPD.hasil_pemeriksaan < tb_ring_kk_metode.batas_bawah
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as out_bawah'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                    AND
                HPD.parameter_id = tb_ring_kk_metode.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_1_bawah AND tb_ring_kk_metode.ring_1_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_1'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                    AND
                HPD.parameter_id = tb_ring_kk_metode.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_2_bawah AND tb_ring_kk_metode.ring_2_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_2'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                    AND
                HPD.parameter_id = tb_ring_kk_metode.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_3_bawah AND tb_ring_kk_metode.ring_3_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_3'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                    AND
                HPD.parameter_id = tb_ring_kk_metode.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan REGEXP \'[0-9]\'
                    AND
                HPD.hasil_pemeriksaan > tb_ring_kk_metode.batas_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as out_atas'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) < -3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_3'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) < -2
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) >= -3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara23'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= -1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) >= -2
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara12'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > -1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 1
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara11'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 2
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_antara12'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 2
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_antara23'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_3')
        )
        // ->where()
        ->get();
        return view('evaluasi.kimiaklinik.grafiknilaimetode.zscore_parameter', compact('param','tahun','type','siklus', 'id','data'));
    }

    public function gnilaisamametode(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $parameter = DB::table('parameter')->where('id', $input['parameter'])->first();
        $metode = DB::table('metode_pemeriksaan')->where('id', $input['metode'])->first();
        $peserta = DB::table('hp_details')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_details.parameter_id', $input['parameter'])
                ->where('metode_pemeriksaan.id', $input['metode'])
                ->where('tb_registrasi.id', $id)
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', $input['type'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('hp_details.hasil_pemeriksaan')
                ->first();
        $ring = DB::table('tb_ring_kk')
                ->select('tb_ring_kk.*',
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN metode_pemeriksaan ON metode_pemeriksaan.id = hp_details.kode_metode_pemeriksaan
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE hp_details.parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        metode_pemeriksaan.id = "'.$input['metode'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_bawah'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN metode_pemeriksaan ON metode_pemeriksaan.id = hp_details.kode_metode_pemeriksaan
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE hp_details.parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_1_bawah AND tb_ring_kk.ring_1_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        metode_pemeriksaan.id = "'.$input['metode'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_1'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN metode_pemeriksaan ON metode_pemeriksaan.id = hp_details.kode_metode_pemeriksaan
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE hp_details.parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_2_bawah AND tb_ring_kk.ring_2_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        metode_pemeriksaan.id = "'.$input['metode'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_2'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN metode_pemeriksaan ON metode_pemeriksaan.id = hp_details.kode_metode_pemeriksaan
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE hp_details.parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_3_bawah AND tb_ring_kk.ring_3_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        metode_pemeriksaan.id = "'.$input['metode'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_bawah'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN metode_pemeriksaan ON metode_pemeriksaan.id = hp_details.kode_metode_pemeriksaan
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE hp_details.parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        metode_pemeriksaan.id = "'.$input['metode'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_3'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN metode_pemeriksaan ON metode_pemeriksaan.id = hp_details.kode_metode_pemeriksaan
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE hp_details.parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_4_bawah AND tb_ring_kk.ring_4_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        metode_pemeriksaan.id = "'.$input['metode'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_4'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN metode_pemeriksaan ON metode_pemeriksaan.id = hp_details.kode_metode_pemeriksaan
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE hp_details.parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_5_bawah AND tb_ring_kk.ring_5_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        metode_pemeriksaan.id = "'.$input['metode'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_5'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN metode_pemeriksaan ON metode_pemeriksaan.id = hp_details.kode_metode_pemeriksaan
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE hp_details.parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        metode_pemeriksaan.id = "'.$input['metode'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_atas')

                )
                ->where('parameter', $input['parameter'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $input['type'])
                ->where('tahun', $input['tahun'])
                ->first();
        $median = DB::table('tb_sd_median_metode')
                ->where('parameter', $input['parameter'])
                ->where('metode', $input['metode'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $input['type'])
                ->where('tahun', $input['tahun'])
                ->first();
        // dd($median);
        return view('evaluasi.kimiaklinik.grafiknilaimetode.grafik', compact('data','parameter','datas','ring', 'median', 'metode'));
    }

    public function zkim(\Illuminate\Http\Request $request, $id)
    {
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view('evaluasi.kimiaklinik.grafik.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function gzkim(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $parameter = $input['parameter'];
        $type = $input['type'];
        $siklus = $input['siklus'];
        $param = DB::table('parameter')->where('id',$parameter)->first();

        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` where form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '6' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia klinik' GROUP BY parameter, type";

        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','kimia klinik')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('type','=',$type)
                ->where('siklus','=',$siklus)
                ->get();
        // dd($type);
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        return view('evaluasi.kimiaklinik.grafik.grafik', compact('data','tahun','type','siklus','param','datap'));
    }

    public function alatzkim(\Illuminate\Http\Request $request, $id)
    {
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view('evaluasi.kimiaklinik.grafikalat.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function alatgzkim(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $parameter = $input['parameter'];
        $alat = $input['alat'];
        $type = $input['type'];
        $siklus = $input['siklus'];
        $param = DB::table('parameter')->where('id',$parameter)->first();
        $alatna = DB::table('tb_instrumen')->where('id',$alat)->first();

        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` where form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '6' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'kimia klinik' GROUP BY parameter, type";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','kimia klinik')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('alat','=',$alat)
                ->where('type','=',$type)
                ->where('siklus','=',$siklus)
                ->get();
        // dd($data);
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        return view('evaluasi.kimiaklinik.grafikalat.grafik', compact('data','tahun','type','siklus','param','datap', 'alatna'));
    }

    public function metodezkim(\Illuminate\Http\Request $request, $id)
    {
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view('evaluasi.kimiaklinik.grafikmetode.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function metodegzkim(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $parameter = $input['parameter'];
        $metode = $input['metode'];
        $type = $input['type'];
        $siklus = $input['siklus'];
        $param = DB::table('parameter')->where('id',$parameter)->first();
        $metodena = DB::table('metode_pemeriksaan')->where('id',$metode)->first();

        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` where form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '6' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'kimia klinik' GROUP BY parameter, type";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','kimia klinik')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('metode','=',$metode)
                ->where('type','=',$type)
                ->where('siklus','=',$siklus)
                ->get();
        // dd($data);
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        return view('evaluasi.kimiaklinik.grafikmetode.grafik', compact('data','tahun','type','siklus','param','datap', 'metodena'));
    }

    public function zkimair(\Illuminate\Http\Request $request, $id)
    {
        $data = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data->created_at );  
        
        $tahun = $tahun->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','kimia air')->get();
        return view('evaluasi.kimiaair.grafik.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function gzkimair(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $type = $request->get('x');
        $tahun = $input['tahun'];
        $parameter = $input['parameter'];
        $siklus = $input['siklus'];

        $param = DB::table('parameter')->where('id',$parameter)->first();

        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` where form = 'kimia air' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '6' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' GROUP BY parameter, type";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','kimia air')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('siklus','=',$siklus)
                ->get();
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        return view('evaluasi.kimiaair.grafik.grafik', compact('data','tahun','siklus','param','datap'));
    }

    public function zkimairterbatas(\Illuminate\Http\Request $request, $id)
    {
        $data = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data->created_at );  
        
        $tahun = $tahun->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','kimia air terbatas')->get();
        return view('evaluasi.kimiaairterbatas.grafik.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function gzkimairterbatas(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $type = $request->get('x');
        $parameter = $input['parameter'];
        $siklus = $input['siklus'];

        $param = DB::table('parameter')->where('id',$parameter)->first();

        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` where form = 'kimia air terbatas' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' AND siklus = '".$siklus."' AND tahun = '".$tahun."' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '5' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' GROUP BY parameter, type";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','kimia air terbatas')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('siklus','=',$siklus)
                ->get();
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        return view('evaluasi.kimiaairterbatas.grafik.grafik', compact('data','tahun','siklus','param','datap'));
    }

    public function zsseluruhpesertaair(){
        $param = DB::table('parameter')->where('kategori','=','kimia air')->get();
        return view('evaluasi.kimiaairterbatas.grafikseluruhpeserta.index', compact('param'));
    }

    public function gzsseluruhpesertaair(\Illuminate\Http\Request $request){
        $input = $request->all();
        $perusahaan = DB::table('tb_zscore')
                        ->select('tb_registrasi.kode_lebpes')
                        ->leftJoin('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                        ->where('tb_zscore.tahun', '=', $input['tahun'])
                        ->where('tb_zscore.parameter', '=', $input['parameter'])
                        ->where('tb_zscore.siklus', '=', $input['siklus'])
                        ->where('tb_zscore.zscore', '!=', 'Tidak di Evaluasi')
                        ->orderBy('tb_zscore.zscore', 'ASC')
                        ->get();
        $data = DB::table('tb_zscore')
                    ->select('tb_zscore.zscore')
                    ->leftJoin('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                    ->where('tb_zscore.tahun', '=', $input['tahun'])
                    ->where('tb_zscore.parameter', '=', $input['parameter'])
                    ->where('tb_zscore.siklus', '=', $input['siklus'])
                    ->where('tb_zscore.zscore', '!=', 'Tidak di Evaluasi')
                    ->orderBy('tb_zscore.zscore', 'ASC')
                    ->get();
        $subtitle = DB::table('parameter')
                    ->select('nama_parameter')
                    ->where('id', '=', $input['parameter'])
                    ->first();
        // dd($data);
        return view('evaluasi.kimiaairterbatas.grafikseluruhpeserta.grafik', compact('data', 'perusahaan', 'subtitle'));
    }

    public function zsseluruhpesertaterbatas(){
        $param = DB::table('parameter')->where('kategori','=','kimia air terbatas')->get();
        return view('evaluasi.kimiaairterbatas.grafikseluruhpeserta.index', compact('param'));
    }

    public function gzsseluruhpesertaterbatas(\Illuminate\Http\Request $request){
        $input = $request->all();
        $perusahaan = DB::table('tb_zscore')
                        ->select('tb_registrasi.kode_lebpes')
                        ->leftJoin('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                        ->where('tb_zscore.tahun', '=', $input['tahun'])
                        ->where('tb_zscore.parameter', '=', $input['parameter'])
                        ->where('tb_zscore.siklus', '=', $input['siklus'])
                        ->where('tb_zscore.zscore', '!=', 'Tidak di Evaluasi')
                        ->orderBy('tb_zscore.zscore', 'ASC')
                        ->get();
        $data = DB::table('tb_zscore')
                    ->select('tb_zscore.zscore')
                    ->leftJoin('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                    ->where('tb_zscore.tahun', '=', $input['tahun'])
                    ->where('tb_zscore.parameter', '=', $input['parameter'])
                    ->where('tb_zscore.siklus', '=', $input['siklus'])
                    ->where('tb_zscore.zscore', '!=', 'Tidak di Evaluasi')
                    ->orderBy('tb_zscore.zscore', 'ASC')
                    ->get();

        $subtitle = DB::table('parameter')
                    ->select('nama_parameter')
                    ->where('id', '=', $input['parameter'])
                    ->first();
        return view('evaluasi.kimiaairterbatas.grafikseluruhpeserta.grafik', compact('data', 'perusahaan', 'subtitle'));
    }

    public function grafiksurvey(){
        return view('laporan.grafik.survey.index');
    }

    public function grafiksurveyna(\Illuminate\Http\Request $request){
        $input = $request->all();
        for ($i=1; $i <= 20 ; $i++) { 
            $sql[$i] = "SELECT COUNT(no_".$i.") as jumlah, no_".$i." as no 
                        FROM tb_pendapat 
                        INNER JOIN tb_pendapat_responden ON tb_pendapat_responden.id = tb_pendapat.id_responden
                        where tahun = '".$input['tahun']."'
                        and siklus = '".$input['siklus']."'
                        GROUP BY no_".$i." Order By no_".$i." ASC
                    ";
            $data[$i] = DB::table(DB::raw("($sql[$i]) zone"))
                        ->get();
        }
        // dd($data);
        return view('laporan.grafik.survey.grafik', compact('data', 'input'));
    }
}

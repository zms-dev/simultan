<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\daftar as Daftar;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use App\TanggalEvaluasi;
use App\JobsEvaluasi;
use App\MetodePemeriksaan;
use App\TbReagenImunologi as ReagenImunologi;
use App\TbHp;
use App\Rujukanurinalisa;
use App\Skoringuri;

class EvaluasiimunController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function skoring()
    {
        return view('evaluasi.urinalisa.grafikskoring.index');
    }
    public function grafikskoring(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $type = $input['type'];
        $siklus = $input['siklus'];

        $sql =" SELECT count(*) as nilai1, '<= 1,00' as nilainya, 'a' as sort from tb_skoring_urinalisa
                where siklus = '".$siklus."' AND type = '".$type."' AND tahun = '".$tahun."' AND skoring <= 1
                UNION
                SELECT count(*) as nilai1, '> 1,00 - 2,00' as nilainya, 'b' as sort from tb_skoring_urinalisa
                where siklus = '".$siklus."' AND type = '".$type."' AND tahun = '".$tahun."' AND skoring > 1 and skoring <= 2
                UNION
                SELECT count(*) as nilai1, '> 2,00 - 3,00' as nilainya, 'c' as sort from tb_skoring_urinalisa
                where siklus = '".$siklus."' AND type = '".$type."' AND tahun = '".$tahun."' AND skoring > 2 AND skoring <= 3
                UNION
                SELECT count(*) as nilai1, '> 3,00' as nilainya, 'd' as sort from tb_skoring_urinalisa
                where siklus = '".$siklus."' AND type = '".$type."' AND tahun = '".$tahun."' AND skoring > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->orderBy('sort', 'desc')
                ->get();
        return view('evaluasi.urinalisa.grafikskoring.grafik', compact('data','tahun','type','siklus'));
    }

    public function perusahaan(\Illuminate\Http\Request $request)
    {
        // ->where('tb_registrasi.status_data1','=','2')
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('*, @rownum := @rownum +1 as rownum'),'perusahaan.*','tb_registrasi.bidang','tb_registrasi.kode_lebpes as kode_lebpesan', 'tb_registrasi.id as idregistrasi')
                        ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->join('users','users.id','=','tb_registrasi.created_by')
                        ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                        ->where('tb_registrasi.status','>=','2')
                        ->where(function($query){
                            if (Auth::user()->penyelenggara == '6') {
                                if (Auth::user()->badan_usaha == '9') {
                                    $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                                }else{
                                    $query->where('perusahaan.pemerintah', '!=', '9');
                                }
                            }
                        })
                        ->get();
            return Datatables::of($datas)
                ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="imunologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>';
                })
                ->addColumn('posisi', function($data) use ($siklus){
                        $evaluasi = DB::table('tb_kesimpulan_evaluasi')
                                    ->where('siklus', $siklus)
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('type', '=', 'rpr-syphilis')
                                    ->orderBy('id', 'desc')
                                    ->first();
                        if(count($evaluasi)){
                            return "1";
                        }else{
                            return "0";
                        }
                ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_kesimpulan_evaluasi')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where(DB::raw('YEAR(created_at)'), '=', $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where(function($query)
                                    {
                                        if(Auth::user()->penyelenggara == '7'){
                                            $query->where('type', '=', 'Syphilis');
                                        }
                                    })
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_data2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_kesimpulan_evaluasi')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where(DB::raw('YEAR(created_at)'), '=', $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where(function($query)
                                    {
                                        if(Auth::user()->penyelenggara == '7'){
                                            $query->where('type', '=', 'rpr-syphilis');
                                        }
                                    })
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('print', function($data) use ($siklus, $tahun){
                    return "".'<a href="cetak-imunologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Cetak
                                    </button>
                                </a>'
                        ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib', 'print' =>  'print'])
            ->make(true);
        }
        $evaluasi = TanggalEvaluasi::where('kategori','=','Imunologi')->first(); 
        return view('evaluasi.imunologi.perusahaan', compact('evaluasi'));
    }

    public function inserttanggalna(\Illuminate\Http\Request $request)
    {
        $data = $request->all();

        if ($data['simpan']== "Simpan") {
            $Save = new TanggalEvaluasi;
            $Save->tanggal = $request->tanggal;
            $Save->kategori = $request->kategori;
            $Save->save();
        }else if ($data['simpan']== "Update") {
            $evaluasi = TanggalEvaluasi::where("kategori", $request->kategori)
            ->update(["tanggal" => $request->tanggal]);
        }
        // dd($data);
        return back();
    }

    public function dataperusahaan(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        // dd($siklus);
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','perusahaan.pemerintah')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id_bidang', '=', '7')
                ->where('tb_registrasi.status', 3)
                ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                ->where(function($query){
                    if (Auth::user()->badan_usaha == '9') {
                        $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                    }
                })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        // dd($data);
        // dd($data);
        return view('evaluasi.imunologi.dataperusahaan', compact('data','siklus'));
    }

    public function perusahaancetak(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('*, @rownum := @rownum +1 as rownum'),'perusahaan.*','tb_registrasi.bidang','tb_registrasi.kode_lebpes as kode_lebpesan','tb_registrasi.id as idregistrasi')
                        ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                        ->where('tb_registrasi.status','>=','2')
                        ->where(function($query){
                            if (Auth::user()->penyelenggara == '6') {
                                if (Auth::user()->badan_usaha == '9') {
                                    $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                                }else{
                                    $query->where('perusahaan.pemerintah', '!=', '9');
                                }
                            }
                        })
                        ->groupBy('perusahaan.id')
                        ->get();
            return Datatables::of($datas)
                ->addColumn('action', function($data) use ($siklus, $tahun){
                    if(Auth::user()->penyelenggara != '7'){
                        $evaluasi = DB::table('tb_kesimpulan_evaluasi')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->orderBy('id', 'desc')
                                    ->first();
                        if(count($evaluasi)){
                            return "".'<a href="cetak-imunologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Cetak
                                            </button>
                                        </a>';
                        }else{
                            return "".'<a href="cetak-imunologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                    Cetak
                                            </button>
                                        </a>';
                        }
                    }else{
                        $evaluasi = DB::table('tb_kesimpulan_evaluasi')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->orderBy('id', 'desc')
                                    ->count('id_registrasi');
                        if ($evaluasi == '2') {
                            return "".'<a href="cetak-imunologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Cetak
                                            </button>
                                        </a>';
                        }else{
                            return "".'<a href="cetak-imunologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                Cetak
                                            </button>
                                        </a>';
                        }
                    }
                ;})

                ->addColumn('tp', function($data) use ($siklus){
                        $evaluasi = DB::table('tb_kesimpulan_evaluasi')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->where('type', '=', 'Syphilis')
                                    ->orderBy('id', 'desc')
                                    ->first();
                        if(count($evaluasi)){
                            return "".'Selesai';
                        }else{
                            return "".'-';
                        }
                ;})
                ->addColumn('rpr', function($data) use ($siklus){
                        $evaluasi = DB::table('tb_kesimpulan_evaluasi')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->where('type', '=', 'rpr-syphilis')
                                    ->orderBy('id', 'desc')
                                    ->first();
                        if(count($evaluasi)){
                            return "".'Selesai';
                        }else{
                            return "".'-';
                        }
                ;})
                ->addColumn('posisi', function($data) use ($siklus){
                        $evaluasi = DB::table('tb_kesimpulan_evaluasi')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->orderBy('id', 'desc')
                                    ->first();
                        if(count($evaluasi)){
                            return "1";
                        }else{
                            return "0";
                        }
                ;})
            ->make(true);
        }
        return view('evaluasi.imunologi.cetak.perusahaan');
    }

    public function perusahaancetaksertifikat(\Illuminate\Http\Request $request)
    {
        // ->where('tb_registrasi.status_data1','=','2')
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('*, @rownum := @rownum +1 as rownum'),'perusahaan.*','tb_registrasi.bidang','tb_registrasi.kode_lebpes as kode_lebpesan','tb_registrasi.id as idregistrasi')
                        ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                        ->where('tb_registrasi.status','>=','2')
                        ->where(function($query){
                            if (Auth::user()->penyelenggara == '6') {
                                if (Auth::user()->badan_usaha == '9') {
                                    $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                                }else{
                                    $query->where('perusahaan.pemerintah', '!=', '9');
                                }
                            }
                        })
                        ->get();
            return Datatables::of($datas)
                ->addColumn('action', function($data) use ($siklus, $tahun){
                    if(Auth::user()->penyelenggara != '7'){
                        $evaluasi = DB::table('tb_kesimpulan_evaluasi')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->orderBy('id', 'desc')
                                    ->first();
                        if(count($evaluasi)){
                            return "".'<a href="cetak-sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Cetak
                                            </button>
                                        </a>';
                        }else{
                            return "".'<a href="cetak-sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                    Cetak
                                            </button>
                                        </a>';
                        }
                    }else{
                        $evaluasi = DB::table('tb_kesimpulan_evaluasi')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->orderBy('id', 'desc')
                                    ->count('id_registrasi');
                        if ($evaluasi == '2') {
                            return "".'<a href="cetak-sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Cetak
                                            </button>
                                        </a>';
                        }else{
                            return "".'<a href="cetak-sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                Cetak
                                            </button>
                                        </a>';
                        }
                    }
                ;})

                ->addColumn('tp', function($data) use ($siklus){
                        $evaluasi = DB::table('tb_kesimpulan_evaluasi')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->where('type', '=', 'Syphilis')
                                    ->orderBy('id', 'desc')
                                    ->first();
                        if(count($evaluasi)){
                            return "".'Selesai';
                        }else{
                            return "".'-';
                        }
                ;})
                ->addColumn('rpr', function($data) use ($siklus){
                        $evaluasi = DB::table('tb_kesimpulan_evaluasi')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->where('type', '=', 'rpr-syphilis')
                                    ->orderBy('id', 'desc')
                                    ->first();
                        if(count($evaluasi)){
                            return "".'Selesai';
                        }else{
                            return "".'-';
                        }
                ;})
                ->addColumn('posisi', function($data) use ($siklus){
                        $evaluasi = DB::table('tb_kesimpulan_evaluasi')
                                    ->where('siklus', $siklus)
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('type', '=', 'rpr-syphilis')
                                    ->orderBy('id', 'desc')
                                    ->first();
                        if(count($evaluasi)){
                            return "1";
                        }else{
                            return "0";
                        }
                ;})
            ->make(true);
        }
        $bidang = Auth::user()->penyelenggara;
        return view('evaluasi.imunologi.cetak.perusahaansertifikat', compact('bidang'));
    }

    public function dataperusahaancetak(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','perusahaan.pemerintah')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id_bidang', '=', '7')
                ->where('tb_registrasi.status', 3)
                ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                ->where(function($query){
                    if (Auth::user()->badan_usaha == '9') {
                        $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                    }
                })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        // dd($data);

        return view('evaluasi.imunologi.cetak.dataperusahaan', compact('data','siklus'));
    }
     public function dataperusahaancetaksertifikat(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','perusahaan.pemerintah')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id_bidang', '=', '7')
                ->where('tb_registrasi.status', 3)
                ->wherein('tb_registrasi.bidang',[6,7,8,9])
                ->where(function($query){
                    if (Auth::user()->badan_usaha == '9') {
                        $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                    }
                })
                // ->where(function($query)
                //     {
                //         $query->where('pemeriksaan2', 'done')
                //             ->orwhere('pemeriksaan', 'done')
                //             ->orwhere('rpr1', 'done')
                //             ->orwhere('rpr2', 'done');
                //     })

                // ->where(function($query)
                //     {
                //         $query->where('status_data1', 2)
                //             ->orwhere('status_data2', 2)
                //             ->orwhere('status_datarpr1', 2)
                //             ->orwhere('status_datarpr2', 2);
                //     })
                ->take(1)
                ->get();
        // dd($data);
        return view('evaluasi.imunologi.cetak.dataperusahaansertifikat', compact('data','siklus'));
    }

    public function urinalisasertifikat(\Illuminate\Http\Request $request)
    {
      // return Auth::user();
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'),'perusahaan.*','tb_registrasi.kode_lebpes','tb_registrasi.id as idregistrasi')
                    ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                    ->where(function($query) use ($siklus)
                    {
                        $query->where('tb_registrasi.siklus', '=', '12')
                            ->orwhere('tb_registrasi.siklus', '=', $siklus);
                    })
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                    ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                    ->where('tb_registrasi.status','>=','2')
                    ->where(function($query){
                      if(Auth::user()->role != '5'){
                        $query->where('tb.registrasi.created_by',Auth::user()->id);
                      }
                    })
                    ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('siklus', $siklus)
                                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                                ->get();
                    if(count($evaluasi) == '2'){
                        return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            View
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            View
                                        </button>
                                    </a>';
                    }
                    ;})
            ->addColumn('posisi', function($data) use ($siklus){
                    $datanya = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('bahan', '=', 'Negatif')
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($datanya)){
                        return "1";
                    }else{
                        return "0";
                    }
            ;})

            ->addColumn('positif', function($data) use ($siklus){
                    $datanya = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('bahan', '=', 'Positif')
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($datanya)){
                        return "".'selesai';
                    }else{
                        return "".'-';
                    }
            ;})
            ->addColumn('negatif', function($data) use ($siklus){
                    $datanya = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('bahan', '=', 'Negatif')
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($datanya)){
                        return "".'selesai';
                    }else{
                        return "".'-';
                    }
            ;})
            ->make(true);
        }
        $bidang = Auth::user()->penyelenggara;
        return view('evaluasi.urinalisa.perusahaansertifikat', compact('bidang'));
    }

    public function dataurinalisasertifikat(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.status', 3)
                // ->where(function($query)
                //     {
                //         $query->where('pemeriksaan2', 'done')
                //             ->orwhere('pemeriksaan', 'done')
                //             ->orwhere('rpr1', 'done')
                //             ->orwhere('rpr2', 'done');
                //     })

                // ->where(function($query)
                //     {
                //         $query->where('status_data1', 2)
                //             ->orwhere('status_data2', 2)
                //             ->orwhere('status_datarpr1', 2)
                //             ->orwhere('status_datarpr2', 2);
                //     })
                ->get();
                // dd($data);
        return view('evaluasi.urinalisa.dataperusahaansertifikat', compact('data', 'siklus'));
    }

    public function urinalisa(\Illuminate\Http\Request $request)
    {
      // return Auth::user();
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'),'perusahaan.*','tb_registrasi.kode_lebpes','tb_registrasi.id as idregistrasi')
                            ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('tb_registrasi.bidang','=','3')
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                            ->where(function($query) use ($siklus){
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $siklus);
                            })
                            ->where(function($query){
                              if(Auth::user()->role != '5'){
                                $query->where('tb_registrasi.created_by',Auth::user()->id);
                              }
                            })->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('siklus', $siklus)
                                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                                ->get();
                    return "".'<a href="urinalisa/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            Proses
                                        </button>
                                    </a>';
                ;})

            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('type', '=', 'a')
                                ->where('siklus', $siklus)
                                ->where('tahun', $tahun)
                                ->orderBy('id', 'desc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('type', '=', 'b')
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisi', function($data) use ($siklus){
                    $datanya = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('type', '=', 'a')
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($datanya)){
                        return "1";
                    }else{
                        return "0";
                    }
            ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib', 'posisi' =>  'posisi'])
            ->make(true);
        }
        $jobPending = JobsEvaluasi::where('name','=','evaluasi-urinalisa')->where('status','=',0)->first();
        $showAllertPending = false;
        $showAllertSuccess = false;
        $showForm = true;
        if(!empty($jobPending)){
            $showAllertPending = true;
            $showAllertSuccess = false;
            $showForm = false;
        }else{
            $jobSuccess = JobsEvaluasi::where('name','=','evaluasi-urinalisa')->where('status','=',1)->where('isread','=',0)->first();
            if(!empty($jobSuccess)){
                $showAllertPending = false;
                $showAllertSuccess = true;
                $showForm = true;
                JobsEvaluasi::where('name','=','evaluasi-urinalisa')->where('status','=',1)->where('isread','=',0)->update(['isread'=>1]);
            }else{
                $showForm = true;
            }
        }
        return view('evaluasi.urinalisa.perusahaan', compact('showAllertPending','showAllertSuccess','showForm'));
    }

    public function dataurinalisa(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
                // dd($data);
        return view('evaluasi.urinalisa.dataperusahaan', compact('data','siklus'));
    }

    public function parameter(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, tb_registrasi.kode_lebpes, tb_registrasi.id as idregistrasi, @rownum := @rownum +1 as rownum'))
                ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.bidang','=','3')
                ->where('tb_registrasi.status','>=','2')
                ->where(function($query){
                  if(Auth::user()->role != '5'){
                    $query->where('tb.registrasi.created_by',Auth::user()->id);
                  }
                })
                ->where(function($query) use ($siklus){
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $siklus);
                })
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('siklus', $siklus)
                                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                                ->get();
                    if(count($evaluasi) == '2'){
                        return "".'<a href="parameter-urinalisa/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            View
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="parameter-urinalisa/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            View
                                        </button>
                                    </a>';
                    }
                    ;})
            
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('type', '=', 'a')
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('type', '=', 'b')
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisi', function($data) use ($siklus){
                    $datanya = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('type', '=', 'a')
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($datanya)){
                        return "1";
                    }else{
                        return "0";
                    }
            ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib', 'posisi' =>  'posisi'])
            ->make(true);
        }
        return view('evaluasi.urinalisa.grafikparameter.perusahaan');
    }

    public function dataparameter(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();

        // dd($data);
        return view('evaluasi.urinalisa.grafikparameter.dataperusahaan', compact('data', 'siklus'));
    }

    public function reagen(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, tb_registrasi.kode_lebpes, tb_registrasi.id as idregistrasi, @rownum := @rownum +1 as rownum'))
                    ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                    ->where('tb_registrasi.bidang','=','3')
                    ->where('tb_registrasi.status','>=','2')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                    ->where(function($query){
                        if(Auth::user()->role != '5'){
                            $query->where('tb.registrasi.created_by',Auth::user()->id);
                        }
                    })
                    ->where(function($query) use ($siklus){
                        $query->where('tb_registrasi.siklus', '12')
                            ->orwhere('tb_registrasi.siklus', $siklus);
                    })
                    ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('siklus', $siklus)
                                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                                ->get();
                    if(count($evaluasi) == '2'){
                        return "".'<a href="reagen-urinalisa/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            View
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="reagen-urinalisa/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            View
                                        </button>
                                    </a>';
                    }
                    ;})
            
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('type', '=', 'a')
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('type', '=', 'b')
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisi', function($data) use ($siklus){
                    $datanya = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('type', '=', 'a')
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($datanya)){
                        return "1";
                    }else{
                        return "0";
                    }
            ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib', 'posisi' =>  'posisi'])
            ->make(true);
        }
        return view('evaluasi.urinalisa.grafikreagen.perusahaan');
    }

    public function datareagen(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.urinalisa.grafikreagen.dataperusahaan', compact('data','siklus'));
    }

    public function reagenkehamilan(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))
                    ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                    ->where('tb_registrasi.status','>=','2')
                    ->where('tb_registrasi.bidang','=','3');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="reagen-kehamilan-urinalisa/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        View
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.urinalisa.grafikreagenkehamilan.perusahaan');
    }

    public function datareagenkehamilan($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        // dd($data);
        return view('evaluasi.urinalisa.grafikreagenkehamilan.dataperusahaan', compact('data'));
    }
    public function cronjoburin(\Illuminate\Http\Request $request)
    {
        $date = $request->get('tahun');
        $siklus = $request->get('siklus');
        $type = $request->get('type');

        $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->join('hp_headers', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->where('tb_registrasi.bidang','=','3')
                ->where('hp_headers.type', $type)
                ->where('hp_headers.siklus', $siklus)
                ->get();
                // dd($datas);
        foreach($datas as $r){
            $r->sub_bidang =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $r->id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->where('sub_bidang.id', '=', '3')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('rpr1', 2)
                            ->orwhere('rpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
            foreach($r->sub_bidang as $sb){
                $score = DB::table('tb_skoring_urinalisa')
                        ->where('siklus', $siklus)
                        ->where('type', $type)
                        ->where('tahun', $date)
                        ->where('id_registrasi', $sb->id);
                $score->delete();
                $dataSave = [];

                $data = DB::table('parameter')
                    ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
                    ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
                    ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
                    ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
                    ->leftjoin('tb_reagen_imunologi', 'hp_details.reagen', '=', 'tb_reagen_imunologi.id')
                    ->leftjoin('tb_instrumen','hp_details.alat', '=', 'tb_instrumen.id')
                    ->leftjoin('tb_hp','hp_details.hasil_pemeriksaan', '=', 'tb_hp.id')
                    ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.alat','tb_instrumen.id as idinstrumen', 'tb_instrumen.instrumen','hp_details.instrument_lain', 'hp_details.id as id_detail','hp_details.reagen as hp_reagen','tb_reagen_imunologi.reagen as imu_reagen','hp_details.reagen_lain', 'tb_hp.hp', 'tb_hp.parameter_id')
                    ->where('parameter.kategori', 'urinalisa')
                    ->where('hp_headers.id_registrasi', $sb->id)
                    ->where('hp_headers.type', $type)
                    ->where('hp_headers.siklus', $siklus)
                    ->get();
                    foreach ($data as $key => $value) {
                        $metode = MetodePemeriksaan::where('parameter_id', $value->id)->get();
                        $value->metode = $metode;
                        $reagen = ReagenImunologi::where('parameter_id', $value->id)->get();
                        $value->reagen = $reagen;
                        $hasilPemeriksaan = TbHp::where('parameter_id', $value->id)->get();
                        $value->hasilPemeriksaan = $hasilPemeriksaan;
                        $target = Rujukanurinalisa::where('parameter', $value->id)
                                                    ->where('siklus', $siklus)
                                                    ->where('tahun', $date)
                                                    ->where('type', $type)
                                                    ->first();
                        $value->target = $target;
                    }
                    $totalscore = 0;
                    $ikutan = 0;
                    foreach ($data as $key => $val) {
                        if($val->id <= 10){
                            if($val->id == 9){
                                if($val->Hasil != "-" ||  $val->Hasil == "Tidak Mengerjakan"){
                                    if ($val->Hasil == "Tidak Mengerjakan") {
                                    }else{
                                        if($val->hp != NULL && $val->id == $val->parameter_id){
                                            if ($val->hp >= $val->target->rujukan) {
                                                $total = $val->hp - $val->target->rujukan;
                                            }else{
                                                $total = $val->target->rujukan - $val->hp ;
                                            }
                                        }else{
                                            if ($val->Hasil >= $val->target->rujukan) {
                                                $total = $val->Hasil - $val->target->rujukan;
                                            }else{
                                                $total = $val->target->rujukan - $val->Hasil ;
                                            }
                                        }
                                        $total = number_format($total, 3);
                                        if ($total == 0) {
                                            $totalscore = $totalscore  + 4;
                                            $ikutan++;
                                        }elseif ($total > 0 && $total <= 0.005) {
                                            $totalscore = $totalscore  + 3;
                                            $ikutan++;
                                        }elseif($total > 0.005 && $total <= 0.010){
                                            $totalscore = $totalscore  + 2;
                                            $ikutan++;
                                        }elseif($total > 0.010 && $total <= 0.015) {
                                            $totalscore = $totalscore  + 1;
                                            $ikutan++;
                                        }else{
                                            $totalscore = $totalscore  + 0;
                                            $ikutan++;
                                        }
                                    }
                                }
                            }elseif($val->id == 10){
                                if($val->Hasil != "-" || $val->Hasil == "Tidak Mengerjakan"){
                                    if ($val->Hasil == "Tidak Mengerjakan") {
                                    }else{
                                        if($val->hp != NULL && $val->id == $val->parameter_id){
                                            if ($val->hp >= $val->target->rujukan) {
                                                $total = $val->hp - $val->target->rujukan;
                                            }else{
                                                $total = $val->target->rujukan - $val->hp;
                                            }
                                        }else{
                                            if ($val->Hasil >= $val->target->rujukan) {
                                                $total = $val->Hasil - $val->target->rujukan;
                                            }else{
                                                $total = $val->target->rujukan - $val->Hasil ;
                                            }
                                        }
                                        if ($total == 0) {
                                            $totalscore = $totalscore  + 4;
                                            $ikutan++;
                                        }elseif ($total > 0 && $total <= 0.5) {
                                            $totalscore = $totalscore  + 3;
                                            $ikutan++;
                                        }elseif($total > 0.5 && $total <= 1.0){
                                            $totalscore = $totalscore  + 2;
                                            $ikutan++;
                                        }elseif ($total > 1.0 && $total <= 1.5) {
                                            $totalscore = $totalscore  + 1;
                                            $ikutan++;
                                        }else{
                                            $totalscore = $totalscore  + 0;
                                            $ikutan++;
                                        }
                                    }
                                }
                            }
                        }else{
                            if(count($val->hp)){
                                if($val->target->rujukan == 'Negatif'){
                                    if ($val->hp != $val->target->rujukan) {
                                        $totalscore = $totalscore  + 0;
                                        $ikutan++;
                                    }else{
                                        $totalscore = $totalscore  + 4;
                                        $ikutan++;
                                    }
                                }elseif($val->target->rujukan == 'Positif'){
                                    if ($val->hp != $val->target->rujukan) {
                                        $totalscore = $totalscore  + 0;
                                        $ikutan++;
                                    }else{
                                        $totalscore = $totalscore  + 4;
                                        $ikutan++;
                                    }
                                }elseif($val->hp == 'Negatif'){
                                    if ($val->hp != $val->target->rujukan) {
                                        $totalscore = $totalscore  + 0;
                                        $ikutan++;
                                    }else{
                                        $totalscore = $totalscore  + 4;
                                        $ikutan++;
                                    }
                                }else{
                                    if($val->hp == $val->target->rujukan){
                                        $total = "0";
                                    }elseif ($val->hp >= $val->target->rujukan) {
                                        if($val->hp == "±"){
                                            $val->hp = "0";
                                        }elseif($val->target->rujukan == "±"){
                                            $val->target->rujukan = "0";
                                        }
                                        $total = str_replace('+', '', $val->hp) - str_replace('+', '', $val->target->rujukan);
                                    }else{
                                        if($val->hp == "±"){
                                            $val->hp = "0";
                                        }elseif($val->target->rujukan == "±"){
                                            $val->target->rujukan = "0";
                                        }
                                        $total =  str_replace('+', '', $val->target->rujukan) - str_replace('+', '', $val->hp);
                                    }
                                    if ($total == "0") {
                                        $totalscore = $totalscore  + 4;
                                        $ikutan++;
                                    }elseif($total == "1" || $total == "-1"){
                                        $totalscore = $totalscore  + 3;
                                        $ikutan++;
                                    }elseif ($total == "2" || $total == "-2") {
                                        $totalscore = $totalscore  + 2;
                                        $ikutan++;
                                    }elseif ($total == "3" || $total == "-3") {
                                        $totalscore = $totalscore  + 1;
                                        $ikutan++;
                                    }
                                }
                            }
                        }
                    }
                $totalscore = number_format($totalscore/$ikutan, 2);

                $i = 0;
                if(!empty($totalscore)){
                    $SaveData = new Skoringuri;
                    $SaveData->id_registrasi = $sb->id;
                    $SaveData->skoring = $totalscore;
                    $SaveData->bahan = $target->bahan;
                    $SaveData->siklus = $siklus;
                    $SaveData->type = $type;
                    $SaveData->tahun = $date;
                    $SaveData->catatan = "-";
                    $SaveData->save();
                }
            }

        }
    }
}
<?php

namespace App\Http\Controllers;
use DB;
use Request;
use Auth;
use Input;
use PDF;
use Carbon\Carbon;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\TanggalKirimHasil;
use App\telurcacing as Telurcacing;
use App\daftar as Daftar;
use App\EvaluasiTc;
use App\register as Register;
use App\LogInput;

use Redirect;
use Validator;
use Session;

class TelurCacingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['printevaluasipeserta','view']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;
        $pendidikan = DB::table('tb_pendidikan')->get();

        $siklus = $request->get('y');
        $register = Register::find($id);
        $perusahaan = $register->kode_lebpes;

        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->get();
                    
        if (count($validasi)) {
            return view('hasil_pemeriksaan/telur_cacing', compact('data', 'perusahaan', 'siklus','date','pendidikan'));
        }else{
            Session::flash('message', 'Harap Input Tanda Terima Bahan Telur Cacing Terlebih Dahulu.');
            Session::flash('alert-class', 'alert-danger');
            return redirect('tanda-terima');
        }
        // if ($siklus == 2) {
        //     if ($q1->siklus == 12) {
        //         if ($q1->pemeriksaan == 'done') {
        //             if (count($validasi)) {
        //                 return view('hasil_pemeriksaan/telur_cacing', compact('data', 'perusahaan', 'siklus','date','pendidikan'));
        //             }else{
        //                 Session::flash('message', 'Harap Input Tanda Terima Bahan Telur Cacing Terlebih Dahulu.');
        //                 Session::flash('alert-class', 'alert-danger');
        //                 return redirect('tanda-terima');
        //             }
        //         }else{
        //             Session::flash('message', 'Harap Input Form Siklus 1.');
        //             Session::flash('alert-class', 'alert-danger');
        //             return back();
        //         }
        //     }else{
        //         if (count($validasi)) {
        //             return view('hasil_pemeriksaan/telur_cacing', compact('data', 'perusahaan', 'siklus','date','pendidikan'));
        //         }else{
        //             Session::flash('message', 'Harap Input Tanda Terima Bahan Telur Cacing Terlebih Dahulu.');
        //             Session::flash('alert-class', 'alert-danger');
        //             return redirect('tanda-terima');
        //         }
        //     }
        // }else{
        //     if (count($validasi)) {
        //         return view('hasil_pemeriksaan/telur_cacing', compact('data', 'perusahaan', 'siklus','date','pendidikan'));
        //     }else{
        //         Session::flash('message', 'Harap Input Tanda Terima Bahan Telur Cacing Terlebih Dahulu.');
        //         Session::flash('alert-class', 'alert-danger');
        //         return redirect('tanda-terima');
        //     }
        // }
    }

    public function view(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;
        // $data = DB::table('parameter')
        //     ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
        //     ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
        //     ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
        //     ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil')
        //     ->where('parameter.kategori', 'hematologi')
        //     ->where('hp_headers.id_registrasi', $id)
        //     ->get();
        // $datas = HpHeader::where('id_registrasi', $id)
        //     ->first();
        
        $data = DB::table('tb_telurcacing')
                ->leftjoin('tb_pendidikan','tb_pendidikan.id','=','tb_telurcacing.pendidikan_petugas')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                ->select('tb_telurcacing.*','tb_pendidikan.tingkat')
                ->first();
        $datas = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                ->where('siklus', $siklus)
                ->get();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;

        // return view('cetak_hasil/telur-cacing', compact('data', 'perusahaan', 'datas', 'type'));
        $pdf = PDF::loadview('cetak_hasil/telur-cacing', compact('data', 'perusahaan','datas', 'type','date','siklus'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        $kode_pes = sprintf("%04s", Auth::user()->id_member);
        if ($siklus == 1) {
            $sikprint = 'I';
        }else{
            $sikprint = 'II';
        }
        return $pdf->stream($kode_pes.' TCC S'.$sikprint.' '.$date.'.pdf');
    }

    public function insert(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $kode_botol = $request->kode_botol;
        $reagen = $request->reagen;
        $reagen_lain = $request->reagen_lain;
        $hasil = $request->hasil;
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $years = $q2->year;

        $input = $request->all();
        // dd($input);
        $validasi = Telurcacing::where(DB::raw('YEAR(tb_telurcacing.created_at)'), '=' , $years)->where('id_registrasi','=',$id)->where('siklus','=',$siklus)->get();


        $i = 0;
        if (count($validasi)>0) {
            Session::flash('message', 'Hasil Telur Cacing Sudah Ada!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('hasil-pemeriksaan');
        }else{
            foreach ($input['kode_botol'] as $kode) {
                if($kode != ''){
                    $data = new Telurcacing;
                    $data['kode_botol'] = $input['kode_botol'][$i];
                    $data['tgl_penerimaan'] = $request->tgl_penerimaan;
                    $data['tgl_pemeriksaan'] = $request->tgl_pemeriksaan;
                    $data['reagen'] = $reagen[$i];
                    $data['kondisi'] = $request->kondisi;
                    $data['reagen_lain'] = $reagen_lain[$i];
                    $data['hasil'] = htmlentities($hasil[$i]);
                    $data['kode_peserta'] = $request->kode_peserta;
                    $data['nama_pemeriksa'] = $request->nama_pemeriksa;
                    $data['nomor_hp'] = $request->nomor_hp;
                    $data['catatan'] = $request->catatan;
                    $data['penanggung_jawab'] = $request->penanggung_jawab;
                    $data['pendidikan_petugas'] = $request->pendidikan;
                    $data['pendidikan_lain'] = $request->pendidikan_lain;
                    $data['created_by'] = Auth::user()->id;
                    $data['id_registrasi'] = $id;
                    $data['siklus'] = $siklus;
                    $data->save();
                  }
                $i++;
            }

            $log = new LogInput;
            $log->id_registrasi = $id;
            $log->status = 'Input Hasil';
            $log->created_by = Auth::user()->id;
            $log->siklus = $siklus;
            $log->type = '';
            $log->save();

            if ($siklus == '1') {
                Register::where('id',$id)->update(['siklus_1'=>'done', 'rpr1'=>'done', 'status_data1'=>'1']);
                Register::where('id',$id)->update(['pemeriksaan'=>'done']);
            }else{
                Register::where('id',$id)->update(['siklus_2'=>'done', 'rpr2'=>'done', 'status_data2'=>'1']);
                Register::where('id',$id)->update(['pemeriksaan2'=>'done']);
            }
            return redirect('hasil-pemeriksaan');
        }
    }

    public function edit(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;
        $pendidikan = DB::table('tb_pendidikan')->get();

        $data = DB::table('tb_telurcacing')
                ->leftjoin('tb_pendidikan','tb_pendidikan.id','=','tb_telurcacing.pendidikan_petugas')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                ->select('tb_telurcacing.*','tb_pendidikan.tingkat')
                ->first();
        $datas = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                ->get();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;

        return view('edit_hasil/telur_cacing', compact('data', 'perusahaan', 'datas', 'siklus','date','pendidikan'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        $register = Register::find($id);

        // dd($input);
        $i = 0;
        foreach ($input['kode_botol'] as $kode_botol) {
            if($kode_botol != ''){
                $Savedata['tgl_pemeriksaan'] = $request->tgl_pemeriksaan;
                $Savedata['tgl_penerimaan'] = $request->tgl_penerimaan;
                $Savedata['catatan'] = $request->catatan;
                $Savedata['penanggung_jawab'] = $request->penanggung_jawab;
                $Savedata['nama_pemeriksa'] = $request->nama_pemeriksa;
                $Savedata['pendidikan_petugas'] = $request->pendidikan;
                $Savedata['pendidikan_lain'] = $request->pendidikan_lain;
                $Savedata['nomor_hp'] = $request->nomor_hp;
                $Savedata['kode_botol'] = $request->kode_botol[$i];
                $Savedata['reagen'] = $request->reagen[$i];
                $Savedata['reagen_lain'] = $request->reagen_lain[$i];
                $Savedata['hasil'] = $request->hasil[$i];
                $Savedata['kondisi'] = $request->kondisi;
                Telurcacing::where('id', $request->idtc[$i])->update($Savedata);
            }
            $i++;
        }
        // dd($input);
        if ($request->simpan == "Kirim") {
            TanggalKirimHasil::where('id_registrasi', $id)
                            ->where('siklus', $siklus)
                            ->delete();

            $KirimHasil = new TanggalKirimHasil;
            $KirimHasil->id_registrasi = $id;
            $KirimHasil->siklus = $siklus;
            $KirimHasil->type = "";
            $KirimHasil->tanggal_kirim = date('Y-m-d');
            $KirimHasil->save();

            $log = new LogInput;
            $log->id_registrasi = $id;
            $log->status = 'Kirim Hasil';
            $log->created_by = Auth::user()->id;
            $log->siklus = $siklus;
            $log->type = '';
            $log->save();

            if ($siklus == '1') {
                Register::where('id',$id)->update(['status_data1'=>'2']);
            }else{
                Register::where('id',$id)->update(['status_data2'=>'2']);
            }
        }else{
            $log = new LogInput;
            $log->id_registrasi = $id;
            $log->status = 'Edit Hasil';
            $log->created_by = Auth::user()->id;
            $log->siklus = $siklus;
            $log->type = '';
            $log->save();
        }
        return redirect('edit-hasil');
    }

    public function evaluasi(\Illuminate\Http\Request $request, $id)
    {
        $input =$request->all();
        $siklus = $request->get('y');
        $data = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->first();
        $datas = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->get();
        $tahun = new Carbon( $data->created_at );   
        $rujuk = DB::table('tb_rujukan_tc')
                ->where('siklus', $siklus)
                ->where('tahun', '=', $tahun->year)
                ->get();
        $evaluasi = DB::table('tb_evaluasi_tc')->where('siklus', $siklus)->where('id_registrasi',$id)->get();
        $evaluasisaran = DB::table('tb_evaluasi_tc')->where('id_registrasi',$id)->where('siklus', $siklus)->first();
        // dd($evaluasisaran);
        $nilaievaluasi = DB::table('tb_evaluasi_tc')->where('id_registrasi',$id)->where('siklus', $siklus)->select(DB::raw('AVG(nilai) as nilai'))->first();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        // dd($evaluasi);
        return view('evaluasi.tc.penilaian.evaluasi.index', compact('data','evaluasisaran','id','siklus', 'perusahaan', 'datas', 'type','rujuk','evaluasi','nilaievaluasi'));
    }

    public function insertevaluasi(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        // dd($input);
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $saran = $input['saran'];
        $i = 0;
        if ($input['simpan'] == 'Simpan') {
            foreach ($input['kode_sediaan'] as $kode) {
                if($kode != ''){
                    $Savedata = new EvaluasiTc;
                    $Savedata->id_registrasi = $id;
                    $Savedata->kode_sediaan = $input['kode_sediaan'][$i];
                    $Savedata->nilai = $input['nilai'][$i];
                    $Savedata->siklus = $siklus;
                    $Savedata->tindakan = $input['tindakan'];
                    $Savedata->tahun = $tahun;
                    $Savedata->saran = $saran;
                    $Savedata->save();
                }
                $i++;
            }

        }
        // dd($Savedata);
        return redirect('evaluasi/telur-cacing/penilaian');
    }

    public function printevaluasi(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');

        $input =$request->all();
        $tanggal=Date('Y m d');
        $data = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->first();
        $datas = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->get();

        $tahun = new Carbon( $data->created_at );
        
        $data_peserta = DB::table('tb_telurcacing')
                ->join('tb_registrasi','tb_registrasi.id','=','tb_telurcacing.id_registrasi')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->select('perusahaan.nama_lab','perusahaan.alamat','tb_telurcacing.kode_peserta')
                ->where('id_registrasi', $id)
                ->where('tb_telurcacing.siklus', $siklus)
                ->first();


       // dd($data_peserta);
        $rujuk = DB::table('tb_rujukan_tc')
                ->where('siklus', $siklus)
                ->where('tahun', '=' , $tahun->year)
                ->get();
        $evaluasi = DB::table('tb_evaluasi_tc')->where('siklus', $siklus)->where('id_registrasi',$id)->get();
        $evaluasisaran = DB::table('tb_evaluasi_tc')->where('siklus', $siklus)->where('id_registrasi',$id)->where('tahun',$tahun->year)->first();

        $nilaievaluasi = DB::table('tb_evaluasi_tc')->where('siklus', $siklus)->where('id_registrasi',$id)->select(DB::raw('AVG(nilai) as nilai'))->first();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $i = 0;

        // return view('evaluasi/tc/penilaian/evaluasi/print', compact('data','data_peserta','evaluasisaran','tanggal','evaluasi','rujuk','nilaievaluasi','id','siklus','tanggal','tahun','register','perusahaan','datas'));

        if ($input['simpan'] == 'Print') {
            if (count($evaluasi) > 0) {
            $pdf = PDF::loadview('evaluasi/tc/penilaian/evaluasi/print', compact('data','data_peserta','evaluasisaran','tanggal','evaluasi','rujuk','nilaievaluasi','id','siklus','tanggal','tahun','register','perusahaan','datas'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Telur Cacing.pdf');
            }else{
                Session::flash('message', 'TC belum dievaluasi!');
                Session::flash('alert-class', 'alert-danger');
                return back();
            }
        }elseif($input['simpan'] == 'Update'){
        foreach ($input['kode_sediaan'] as $kode) {
                if($kode != ''){
                    $Savedata['siklus'] = $siklus;
                    $Savedata['kode_sediaan'] = $request->kode_sediaan[$i];
                    $Savedata['nilai'] = $request->nilai[$i];
                    $Savedata['saran'] = $request->saran;
                    $Savedata['tindakan'] = $request->tindakan;
                    EvaluasiTc::where('id', $request->id[$i])->update($Savedata);
                }
                $i++;
            }
        return back();
        }
    }

    public function printevaluasipeserta(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');

        $input =$request->all();
        $tanggal=Date('Y m d');
        $data = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->first();
        $datas = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->get();

        $tahun = new Carbon( $data->created_at );
        
        $data_peserta = DB::table('tb_telurcacing')
                ->join('tb_registrasi','tb_registrasi.id','=','tb_telurcacing.id_registrasi')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->select('perusahaan.nama_lab','perusahaan.alamat','tb_telurcacing.kode_peserta')
                ->where('id_registrasi', $id)
                ->where('tb_telurcacing.siklus', $siklus)
                ->first();

        $ttd = DB::table('tb_ttd_evaluasi')
            ->where('tahun', $tahun->year)
            ->where('siklus', $siklus)
            ->where('bidang', '=', '5')
            ->first();

       // dd($data_peserta);
        $rujuk = DB::table('tb_rujukan_tc')
                ->where('siklus', $siklus)
                ->where('tahun', '=' , $tahun->year)
                ->get();
        $evaluasi = DB::table('tb_evaluasi_tc')->where('siklus', $siklus)->where('id_registrasi',$id)->get();
        $evaluasisaran = DB::table('tb_evaluasi_tc')->where('siklus', $siklus)->where('id_registrasi',$id)->where('tahun',$tahun->year)->first();

        $nilaievaluasi = DB::table('tb_evaluasi_tc')->where('siklus', $siklus)->where('id_registrasi',$id)->select(DB::raw('AVG(nilai) as nilai'))->first();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $i = 0;

        // return view('evaluasi/tc/penilaian/evaluasi/print', compact('data','data_peserta','evaluasisaran','tanggal','evaluasi','rujuk','nilaievaluasi','id','siklus','tanggal','tahun','register','perusahaan','datas'));

        if (count($evaluasi) > 0) {
        $pdf = PDF::loadview('evaluasi/tc/penilaian/evaluasi/print', compact('data','data_peserta','evaluasisaran','tanggal','evaluasi','rujuk','nilaievaluasi','id','siklus','tanggal','tahun','register','perusahaan','datas','ttd'))
        ->setPaper('a4', 'potrait')
        ->setwarnings(false);
        return $pdf->stream('Telur Cacing.pdf');
        }else{
            Session::flash('message', 'TC belum dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
    }
}

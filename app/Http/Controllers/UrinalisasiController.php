<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;
use Carbon\Carbon;
use App\TanggalKirimHasil;
use App\Parameter;
use App\HpHeader;
use App\Skoringuri;
use App\MetodePemeriksaan;
use App\LogInput;
use App\TbHp;
use App\Rujukanurinalisa;
use App\TbReagenImunologi as ReagenImunologi;
use App\HpDetail;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\View;
use App\kimiaklinik as Kimiaklinik;
use App\daftar as Daftar;
use App\register as Register;
use Redirect;
use Validator;
use Session;
use App\Jobs\Evaluasi;
use App\JobsEvaluasi;

class UrinalisasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['penilaianreagen','penilaianparameter','view','apipenilaianparameter','apipenilaianreagen']]);
    }

    public function apipenilaianreagen(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $peserta = DB::table('tb_registrasi')->find($id);

        $reagen = DB::table('hp_details')
                    ->join('tb_reagen_imunologi','hp_details.reagen','=','tb_reagen_imunologi.id')
                    ->join('hp_headers','hp_details.hp_header_id','=','hp_headers.id')
                    ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                    ->select('tb_reagen_imunologi.reagen','tb_reagen_imunologi.id', 'hp_headers.kode_lab', 'tb_registrasi.kode_lebpes', DB::raw('COUNT(DISTINCT tb_registrasi.id) as jumlah'))
                    ->groupBy('hp_details.reagen')
                    ->where('hp_headers.type', $type)
                    ->where('hp_headers.siklus', $siklus)
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                    ->whereNotIn('hp_details.parameter_id', [19])
                    ->orderBy('tb_reagen_imunologi.reagen', 'asc')
                    ->get();

        $data = DB::table('hp_details')
                ->join('tb_reagen_imunologi','hp_details.reagen','=','tb_reagen_imunologi.id')
                ->join('hp_headers','hp_details.hp_header_id','=','hp_headers.id')
                ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                ->select('tb_reagen_imunologi.reagen','tb_reagen_imunologi.id', 'tb_registrasi.kode_lebpes')
                ->where('hp_headers.type', $type)
                ->where('hp_headers.siklus', $siklus)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->where('tb_registrasi.id', $id)
                ->whereNotIn('hp_details.parameter_id', [19])
                ->get();
        $kehamilan = DB::table('hp_details')
                    ->join('tb_reagen_imunologi','hp_details.reagen','=','tb_reagen_imunologi.id')
                    ->join('hp_headers','hp_details.hp_header_id','=','hp_headers.id')
                    ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                    ->select('tb_reagen_imunologi.reagen','tb_reagen_imunologi.id', 'hp_headers.kode_lab', DB::raw('COUNT(DISTINCT tb_registrasi.id) as jumlah'))
                    ->groupBy('hp_details.reagen')
                    ->where('hp_headers.type', $type)
                    ->where('hp_headers.siklus', $siklus)
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                    ->whereIn('hp_details.parameter_id', [19])
                    ->orderBy('tb_reagen_imunologi.reagen', 'asc')
                    ->get();
        $data2 = DB::table('hp_details')
                ->join('tb_reagen_imunologi','hp_details.reagen','=','tb_reagen_imunologi.id')
                ->join('hp_headers','hp_details.hp_header_id','=','hp_headers.id')
                ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                ->select('tb_reagen_imunologi.reagen','tb_reagen_imunologi.id', 'tb_registrasi.kode_lebpes')
                ->where('hp_headers.type', $type)
                ->where('hp_headers.siklus', $siklus)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->where('tb_registrasi.id', $id)
                ->whereIn('hp_details.parameter_id', [19])
                ->get();
        return view('evaluasi.urinalisa.grafikreagen.apigrafik', compact('reagen','data','kehamilan','data2','siklus','type','date', 'peserta'));
        // dd($parameter);
    }
    public function apipenilaianparameter(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $reagen = Rujukanurinalisa::where('siklus', $siklus)
                                    ->where('tahun', $date)
                                    ->where('type', $type)
                                    ->count();
        // dd($form);
        $peserta = DB::table('tb_registrasi')->where('id', $id)->first();
        $parameter = DB::table('parameter')->where('kategori', '=', 'urinalisa')->get();
        // dd($parameter);
        foreach ($parameter as $key => $val) {
            if ($val->id > 10) {
                $masing = DB::table('tb_hp')
                            ->select('tb_hp.*',
                                DB::raw('(SELECT COUNT(*) from hp_details as HPD
                                        INNER JOIN `hp_headers` on `hp_headers`.`id` = `HPD`.`hp_header_id`
                                        INNER JOIN `tb_registrasi` on `tb_registrasi`.`id` = `hp_headers`.`id_registrasi`
                                            WHERE
                                                `HPD`.`hasil_pemeriksaan` = `tb_hp`.`id`
                                            AND
                                                `HPD`.`parameter_id` = tb_hp.parameter_id
                                            AND
                                                `hp_headers`.`type` = "'.$type.'"
                                            AND
                                                `hp_headers`.`siklus` = '.$siklus.'
                                            AND
                                                YEAR(tb_registrasi.created_at) = '.$date.'
                                  ) as Jumlah'))
                            ->where('tb_hp.parameter_id', '=', $val->id)
                            ->groupBy('tb_hp.id')
                            ->orderBy(DB::raw('FIELD(hp, "±","Negatif")'), 'desc')
                            ->get();
                $hasilna = DB::table('hp_details')
                            ->join('hp_headers', 'hp_details.hp_header_id','=','hp_headers.id')
                            ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                            ->join('tb_hp','hp_details.hasil_pemeriksaan','=','tb_hp.id')
                            ->where('hp_details.parameter_id', $val->id)
                            ->where('hp_headers.type', $type)
                            ->where('hp_headers.siklus', $siklus)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                            ->where('tb_registrasi.id', $id)
                            ->select('hp_details.hasil_pemeriksaan','tb_hp.hp')
                            ->first();
            }else{
                $masing = DB::table('tb_hp')
                            ->select('tb_hp.*',
                                DB::raw('(SELECT COUNT(*) from hp_details as HPD
                                        INNER JOIN `hp_headers` on `hp_headers`.`id` = `HPD`.`hp_header_id`
                                        INNER JOIN `tb_registrasi` on `tb_registrasi`.`id` = `hp_headers`.`id_registrasi`
                                            WHERE
                                                (`HPD`.`hasil_pemeriksaan` = `tb_hp`.`hp` or `HPD`.`hasil_pemeriksaan` = `tb_hp`.`id`)
                                            AND
                                                `HPD`.`parameter_id` = tb_hp.parameter_id
                                            AND
                                                `hp_headers`.`type` = "'.$type.'"
                                            AND
                                                `hp_headers`.`siklus` = '.$siklus.'
                                            AND
                                                YEAR(tb_registrasi.created_at) = '.$date.'
                                  ) as Jumlah'))
                            ->where('tb_hp.parameter_id', '=', $val->id)
                            ->groupBy('tb_hp.id')
                            ->orderBy(DB::raw('FIELD(hp, "±","Negatif")'), 'desc')
                            ->get();

                $hasilna = DB::table('hp_details')
                            ->join('hp_headers', 'hp_details.hp_header_id','=','hp_headers.id')
                            ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                            ->leftjoin('tb_hp','hp_details.hasil_pemeriksaan','=','tb_hp.id')
                            ->where('hp_details.parameter_id', $val->id)
                            ->where('tb_hp.parameter_id', $val->id)
                            ->where('hp_headers.type', $type)
                            ->where('hp_headers.siklus', $siklus)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                            ->where('tb_registrasi.id', $id)
                            ->select('hp_details.hasil_pemeriksaan', 'tb_hp.hp')
                            ->first();
            }
            $target = Rujukanurinalisa::where('parameter', $val->id)
                                        ->where('siklus', $siklus)
                                        ->where('tahun', $date)
                                        ->where('type', $type)
                                        ->select('rujukan')
                                        ->first();
            $val->target = $target;
            $val->masing = $masing;
            $val->hasilna = $hasilna;
        }
        // dd($parameter);
        if ($reagen == '11') {
            return view('evaluasi.urinalisa.grafikparameter.apigrafik', compact('parameter', 'peserta', 'date','siklus', 'type'));
        }else{
            Session::flash('message', 'Data Rujukan Belum Lengkap!');
            Session::flash('alert-class', 'alert-danger');
            return redirect::back();
        }
        // dd($parameter);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request, $id)
    {
        $reagen = DB::table('tb_reagen_imunologi')
                    ->where('kelompok','=','Urinalisa')
                    ->where('parameter_id',null)
                    ->get();
        // dd($reagen);
        $data = Parameter::where('kategori', 'urinalisa')->get();
        $pendidikan = DB::table('tb_pendidikan')->get();
        // dd($data);
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $register = Register::find($id);
        $perusahaan = $register->kode_lebpes;

        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->first();

        if (count($validasi)) {
            return view('hasil_pemeriksaan/urinalisasi', compact('data', 'perusahaan', 'type', 'siklus','date', 'reagen','pendidikan','validasi'));
        }else{
            Session::flash('message', 'Harap Input Tanda Terima Bahan Urinalisa Terlebih Dahulu.');
            Session::flash('alert-class', 'alert-danger');
            return redirect('tanda-terima');
        }
        // if ($siklus == 2) {
        //     if ($q1->siklus == 12) {
        //         if ($q1->pemeriksaan == 'done' && $q1->pemeriksaan2 == 'done') {
        //             if (count($validasi)) {
        //                 return view('hasil_pemeriksaan/urinalisasi', compact('data', 'perusahaan', 'type', 'siklus','date', 'reagen','pendidikan','validasi'));
        //             }else{
        //                 Session::flash('message', 'Harap Input Tanda Terima Bahan Urinalisa Terlebih Dahulu.');
        //                 Session::flash('alert-class', 'alert-danger');
        //                 return redirect('tanda-terima');
        //             }
        //         }else{
        //             Session::flash('message', 'Harap Input Form Siklus 1.');
        //             Session::flash('alert-class', 'alert-danger');
        //             return back();
        //         }
        //     }else{
        //         if (count($validasi)) {
        //             return view('hasil_pemeriksaan/urinalisasi', compact('data', 'perusahaan', 'type', 'siklus','date', 'reagen','pendidikan','validasi'));
        //         }else{
        //             Session::flash('message', 'Harap Input Tanda Terima Bahan Urinalisa Terlebih Dahulu.');
        //             Session::flash('alert-class', 'alert-danger');
        //             return redirect('tanda-terima');
        //         }
        //     }
        // }else{
        //     if (count($validasi)) {
        //         return view('hasil_pemeriksaan/urinalisasi', compact('data', 'perusahaan', 'type', 'siklus','date', 'reagen','pendidikan','validasi'));
        //     }else{
        //         Session::flash('message', 'Harap Input Tanda Terima Bahan Urinalisa Terlebih Dahulu.');
        //         Session::flash('alert-class', 'alert-danger');
        //         return redirect('tanda-terima');
        //     }
        // }
    }

    public function view(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $tanggal = date('d-m-Y');
        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('tb_instrumen', 'hp_details.alat', '=', 'tb_instrumen.id')
            ->leftjoin('tb_hp', 'hp_details.hasil_pemeriksaan', '=', 'tb_hp.id')
            ->leftjoin('tb_reagen_imunologi', 'hp_details.reagen', '=', 'tb_reagen_imunologi.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->select('parameter.*','hp_details.alat as Alat', 'tb_instrumen.instrumen', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.instrument_lain', 'hp_details.hasil_pemeriksaan as Hasil','hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'tb_reagen_imunologi.reagen', 'hp_details.reagen_lain','hp_details.reagen as Reagen', 'tb_hp.hp')
            ->where('parameter.kategori', 'urinalisa')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->get();
        // dd($data);
            
        $datas = HpHeader::where('id_registrasi', $id)
            ->leftjoin('tb_pendidikan','tb_pendidikan.id','=','hp_headers.pendidikan_petugas')
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->select('hp_headers.*','tb_pendidikan.tingkat')
            ->first();

        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $tanggalkirim = DB::table('tb_kirim_hasil')
                            ->where('id_registrasi', $id)
                            ->where('siklus', $siklus)
                            ->where('type', $type)
                            ->first();

        // return view('cetak_hasil/urinalisasi', compact('data', 'perusahaan', 'datas', 'type'));
        $pdf = PDF::loadview('cetak_hasil/urinalisasi', compact('data', 'perusahaan','datas', 'type', 'siklus' ,'date','tanggal','register','tanggalkirim'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);


        $kode_pes = sprintf("%04s", Auth::user()->id_member);
        if ($type == 'a') {
            $typeprint = '01';
        }else{
            $typeprint = '02';
        }
        if ($siklus == 1) {
            $sikprint = 'I';
        }else{
            $sikprint = 'II';
        }
        return $pdf->stream($kode_pes.' URI S'.$sikprint.'-'.$typeprint.' '.$date.'.pdf');
    }

    public function insert(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        
        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;
        $type = $input['type'];
        $validasi = HpHeader::where(['id_registrasi'=>$id , 'siklus'=>$siklus , 'type'=>$type])->get();

        $rules = array(
            'kode_peserta' => 'required',
            'tanggal_penerimaan' => 'required',
            'tanggal_pemeriksaan' => 'required',
            'kualitas' => 'required'
        );

        $messages = array(
            'kode_peserta.required' => 'Kode Peserta Wajib diisi',
            'tanggal_penerimaan.required' => 'Tanggal Penerimaan wajib diisi',
            'tanggal_pemeriksaan.required' => 'Tanggal Pemeriksaan wajib diisi',
            'kualitas.required' => 'Kualitas wajib dipliih'
        );
        if (count($validasi)>0) {
            Session::flash('message', 'Hasil Urinalisasi Sudah Ada!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('hasil-pemeriksaan');
        }else{
            $saveHeader = new HpHeader;
            $saveHeader->kode_lab = $input['kode_peserta'];
            $saveHeader->kode_peserta = $perusahaanID;
            $saveHeader->tgl_pemeriksaan = date('Y-m-d', strtotime($input['tanggal_pemeriksaan']));
            $saveHeader->tgl_penerimaan = date('Y-m-d', strtotime($input['tanggal_penerimaan']));
            $saveHeader->kualitas_bahan = $input['kualitas'];
            $saveHeader->catatan = $input['catatan'];
            $saveHeader->kategori = 'urinalisa';
            $saveHeader->kode_bahan = $input['kode_bahan'];
            $saveHeader->id_registrasi = $id;
            $saveHeader->penanggung_jawab = $input['penanggung_jawab'];
            $saveHeader->type = $input['type'];
            $saveHeader->pendidikan_petugas = $input['pendidikan'];
            $saveHeader->pendidikan_lain = $input['pendidikan_lain'];
            // $saveHeader->pelaksanaan_pmi = $input['pelaksanaan_pmi'];
            $saveHeader->siklus = $siklus;
            $saveHeader->save();

            $saveHeaderID = $saveHeader->id;

            $i = 0;
            foreach ($input['parameter_id'] as $parameter_id) {
                $saveDetail = new HpDetail;
                $saveDetail->parameter_id = $input['parameter_id'][$i];
                $saveDetail->hp_header_id = $saveHeaderID;
                $saveDetail->alat = $input['alat'][$i];
                $saveDetail->kode_metode_pemeriksaan = $input['kode'][$i];
                $saveDetail->hasil_pemeriksaan = $input['hasil'][$i];
                $saveDetail->reagen = $input['reagen'][$i];
                $saveDetail->reagen_lain = $input['reagen_lain'][$i];
                $saveDetail->metode_lain = $input['metode_lain'][$i];
                $saveDetail->save();
                $i++;
            }

            if ($siklus == '1') {
                Register::where('id',$id)->update(['siklus_1'=>'done']);
                if ($input['type'] == 'a') {
                    Register::where('id',$id)->update(['pemeriksaan'=>'done', 'status_data1'=>'1']);
                }else{
                    Register::where('id',$id)->update(['pemeriksaan2'=>'done', 'status_data2'=>'1']);
                }
            }else{
                Register::where('id',$id)->update(['siklus_2'=>'done',]);
                if ($input['type'] == 'a') {
                    Register::where('id',$id)->update(['rpr1'=>'done', 'status_datarpr1'=>'1']);
                }else{
                    Register::where('id',$id)->update(['rpr2'=>'done', 'status_datarpr2'=>'1']);
                }
            }
            $log = new LogInput;
            $log->id_registrasi = $id;
            $log->status = 'Input Hasil';
            $log->created_by = Auth::user()->id;
            $log->siklus = $siklus;
            $log->type = $type;
            $log->save();
            return redirect('hasil-pemeriksaan');
        }
    }


    public function edit(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;
        $pendidikan = DB::table('tb_pendidikan')->get();

        $instrumen = DB::table('tb_instrumen')->where('id_parameter', 'urinalisa')->get();
        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->leftjoin('tb_reagen_imunologi', 'hp_details.reagen', '=', 'tb_reagen_imunologi.id')
            ->leftjoin('tb_instrumen','hp_details.alat', '=', 'tb_instrumen.id')
            ->leftjoin('tb_hp','hp_details.hasil_pemeriksaan', '=', 'tb_hp.id')
            ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.alat','tb_instrumen.id as idinstrumen', 'tb_instrumen.instrumen','hp_details.instrument_lain', 'hp_details.id as id_detail','hp_details.reagen as hp_reagen','tb_reagen_imunologi.reagen as imu_reagen','hp_details.reagen_lain', 'tb_hp.hp')
            ->where('parameter.kategori', 'urinalisa')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->get();
            foreach ($data as $key => $val) {
                $metode = MetodePemeriksaan::where('parameter_id', $val->id)->get();
                $val->metode = $metode;
                $reagen = ReagenImunologi::where('parameter_id', $val->id)->get();
                $val->reagen = $reagen;
                $hasilPemeriksaan = TbHp::where('parameter_id', $val->id)->get();
                $val->hasilPemeriksaan = $hasilPemeriksaan;
            }
            // dd($data);
        $reagen = DB::table('tb_reagen_imunologi')
                    ->where('kelompok','=','Urinalisa')
                    ->where('parameter_id',NULL)
                    ->get();
        $datas = HpHeader::where('id_registrasi', $id)
            ->leftjoin('tb_pendidikan','tb_pendidikan.id','=','hp_headers.pendidikan_petugas')
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->select('hp_headers.*','tb_pendidikan.tingkat')
            ->first();
            // dd($siklus);
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;

        return view('edit_hasil/urinalisasi', compact('data', 'perusahaan', 'datas', 'type','siklus','date','instrumen','reagen','pendidikan'));
    }


    public function update(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        $type = $request->get('x');
        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;

        // dd($input);
        $SaveHeader['tgl_penerimaan'] = $request->tanggal_penerimaan;
        $SaveHeader['tgl_pemeriksaan'] = $request->tanggal_pemeriksaan;
        $SaveHeader['kualitas_bahan'] = $request->kualitas;
        $SaveHeader['kode_bahan'] = $request->kode_bahan;
        $SaveHeader['catatan'] = $request->catatan;
        $SaveHeader['penanggung_jawab'] = $request->penanggung_jawab;
        $SaveHeader['pendidikan_petugas'] = $request->pendidikan;
        $SaveHeader['pendidikan_lain'] = $request->pendidikan_lain;
        // $SaveHeader['pelaksanaan_pmi'] = $request->pelaksanaan_pmi;

        HpHeader::where('id_registrasi',$id)->where('siklus', $siklus)->where('type', $type)->update($SaveHeader);
        // dd($input);
        $i = 0;
        foreach ($input['alat'] as $alat) {
            if($alat != ''){
                $SaveDetail['alat'] = $request->alat[$i];
                $SaveDetail['reagen'] = $request->reagen[$i];
                $SaveDetail['reagen_lain'] = $request->reagen_lain[$i];
                $SaveDetail['kode_metode_pemeriksaan'] = $request->kode[$i];
                $SaveDetail['metode_lain'] = $request->metode_lain[$i];
                $SaveDetail['hasil_pemeriksaan'] = $request->hasil[$i];
                HpDetail::where('id', $request->id_detail[$i])->update($SaveDetail);
            }
            $i++;
        }
        if ($request->simpan == "Kirim") {
            TanggalKirimHasil::where('id_registrasi', $id)
                            ->where('siklus', $siklus)
                            ->where('type', $type)
                            ->delete();

            $KirimHasil = new TanggalKirimHasil;
            $KirimHasil->id_registrasi = $id;
            $KirimHasil->siklus = $siklus;
            $KirimHasil->type = $type;
            $KirimHasil->tanggal_kirim = date('Y-m-d');
            $KirimHasil->save();

            $log = new LogInput;
            $log->id_registrasi = $id;
            $log->status = 'Kirim Hasil';
            $log->created_by = Auth::user()->id;
            $log->siklus = $siklus;
            $log->type = $type;
            $log->save();

            if ($siklus == '1') {
                Register::where('id',$id)->update(['siklus_1'=>'done']);
                if ($input['type'] == 'a') {
                    Register::where('id',$id)->update(['pemeriksaan'=>'done', 'status_data1'=>'2']);
                }else{
                    Register::where('id',$id)->update(['pemeriksaan2'=>'done', 'status_data2'=>'2']);
                }
            }else{
                Register::where('id',$id)->update(['siklus_2'=>'done']);
                if ($input['type'] == 'a') {
                    Register::where('id',$id)->update(['rpr1'=>'done', 'status_datarpr1'=>'2']);
                }else{
                    Register::where('id',$id)->update(['rpr2'=>'done', 'status_datarpr2'=>'2']);
                }
            }
        }else{
            $log = new LogInput;
            $log->id_registrasi = $id;
            $log->status = 'Edit Hasil';
            $log->created_by = Auth::user()->id;
            $log->siklus = $siklus;
            $log->type = $type;
            $log->save();
        }
        return redirect('edit-hasil');
    }

    public function evaluasi(\Illuminate\Http\Request $request, $id){
        $input = $request->all();
        // dd($input);
        // dd($input);
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $tanggal = date('Y m d');
        $instrumen = DB::table('tb_instrumen')->where('id_parameter', 'urinalisa')->get();
        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->leftjoin('tb_reagen_imunologi', 'hp_details.reagen', '=', 'tb_reagen_imunologi.id')
            ->leftjoin('tb_instrumen','hp_details.alat', '=', 'tb_instrumen.id')
            ->leftjoin('tb_hp','hp_details.hasil_pemeriksaan', '=', 'tb_hp.id')
            ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.alat','tb_instrumen.id as idinstrumen', 'tb_instrumen.instrumen','hp_details.instrument_lain', 'hp_details.id as id_detail','hp_details.reagen as hp_reagen','tb_reagen_imunologi.reagen as imu_reagen','hp_details.reagen_lain', 'tb_hp.hp', 'tb_hp.parameter_id','tb_registrasi.periode')
            ->where('parameter.kategori', 'urinalisa')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->get();
            foreach ($data as $key => $val) {
                $metode = MetodePemeriksaan::where('parameter_id', $val->id)->get();
                $val->metode = $metode;
                $reagen = ReagenImunologi::where('parameter_id', $val->id)->get();
                $val->reagen = $reagen;
                $hasilPemeriksaan = TbHp::where('parameter_id', $val->id)->get();
                $val->hasilPemeriksaan = $hasilPemeriksaan;
                $target = Rujukanurinalisa::where('parameter', $val->id)
                                            ->where('siklus', $siklus)
                                            ->where('tahun', $date)
                                            ->where('type', $type)
                                            ->where(function($query) use ($val, $tahun, $siklus){
                                                if ($val->periode == 2 && $siklus == 1) {
                                                    $query->where('periode','=', '2');
                                                }else{
                                                    $query->whereNull('periode');
                                                }
                                            })
                                            ->get();
                $val->target = $target;
            }
            $skoring = DB::table('tb_skoring_urinalisa')
                    ->where('id_registrasi', $id)
                    ->where('siklus', $siklus)
                    ->where('type', $type)
                    ->where('tahun', $date)
                    ->get();
            // dd($skoring);
            $skoringUp = DB::table('tb_skoring_urinalisa')
                    ->where('id_registrasi', $id)
                    ->where('siklus', $siklus)
                    ->where('type', $type)
                    ->where('tahun', $date)
                    ->get();

        $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();


        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        // return $data;
        if (count($data[10]->target)) {
            return view('evaluasi/urinalisa/input', compact('data','simpan', 'perusahaan', 'datas', 'type','siklus','date','instrumen', 'tanggal', 'skoring','id', 'input'));
        }else{
            Session::flash('message', 'Data Rujukan Belum Lengkap!');
            Session::flash('alert-class', 'alert-danger');
            return redirect::back();
        }
    }

    public function printevaluasi(\Illuminate\Http\Request $request, $id){
       $input = $request->all();
        // dd($input);
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $tanggal = date('Y m d');
        $instrumen = DB::table('tb_instrumen')->where('id_parameter', 'urinalisa')->get();
        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->leftjoin('tb_reagen_imunologi', 'hp_details.reagen', '=', 'tb_reagen_imunologi.id')
            ->leftjoin('tb_instrumen','hp_details.alat', '=', 'tb_instrumen.id')
            ->leftjoin('tb_hp','hp_details.hasil_pemeriksaan', '=', 'tb_hp.id')
            ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.alat','tb_instrumen.id as idinstrumen', 'tb_instrumen.instrumen','hp_details.instrument_lain', 'hp_details.id as id_detail','hp_details.reagen as hp_reagen','tb_reagen_imunologi.reagen as imu_reagen','hp_details.reagen_lain', 'tb_hp.hp', 'tb_hp.parameter_id','tb_registrasi.periode')
            ->where('parameter.kategori', 'urinalisa')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->get();
            foreach ($data as $key => $val) {
                $metode = MetodePemeriksaan::where('parameter_id', $val->id)->get();
                $val->metode = $metode;
                $reagen = ReagenImunologi::where('parameter_id', $val->id)->get();
                $val->reagen = $reagen;
                $hasilPemeriksaan = TbHp::where('parameter_id', $val->id)->get();
                $val->hasilPemeriksaan = $hasilPemeriksaan;
                $target = Rujukanurinalisa::where('parameter', $val->id)
                                            ->where('siklus', $siklus)
                                            ->where('tahun', $date)
                                            ->where('type', $type)
                                            ->where(function($query) use ($val, $siklus){
                                                if ($val->periode == 2 && $siklus == 1) {
                                                    $query->where('periode','=', '2');
                                                }else{
                                                    $query->whereNull('periode');
                                                }
                                            })
                                            ->get();
                $val->target = $target;
            }

        // dd($data);
        $skoring = DB::table('tb_skoring_urinalisa')
                    ->where('id_registrasi', $id)
                    ->where('siklus', $siklus)
                    ->where('type', $type)
                    ->where('tahun', $date)
                    ->get();
        $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();


        $skoring1 = DB::table('tb_skoring_urinalisa')
                    ->where('id_registrasi', $id)
                    ->where('siklus', $siklus)
                    ->where('type', $type)
                    ->where('tahun', $date)
                    ->first();
        $ttd = DB::table('tb_ttd_evaluasi')
            ->where('tahun', $date)
            ->where('siklus', $siklus)
            ->where('bidang', '=', '3')
            ->where(function($query) use ($q1, $date, $siklus){
                if ($q1->periode == 2 && $date == 2019 && $siklus == 1) {
                    $query->where('periode', '=', '2');
                }else{
                    $query->whereNull('periode');
                }
            })
            ->first();


        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;


        if ($input['simpan'] == 'Print') {
            if (count($data[10]->target) > 0) {
            $pdf = PDF::loadview('evaluasi/urinalisa/print/evaluasi', compact('data' ,'perusahaan', 'datas', 'type','siklus','date','instrumen', 'tanggal', 'skoring','id','ttd','q1'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Urinalisa.pdf');
            }else{
                 Session::flash('message', 'Urinalisa belum dievaluasi!');
                 Session::flash('alert-class', 'alert-danger');
                return back();
            }
        }else if($input['simpan'] == 'Update'){
            Skoringuri::where("id", $request->id)->update(["catatan" => $request->catatan, "status" => "1"]);
            return back();
        }else if($input['simpan'] == 'Batal Evaluasi'){
            Skoringuri::where("id", $request->id)->delete();
            return back();
        }

    }

    public function simpanskor(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;
        
        $tanggal = date('Y m d');
        $input = $request->all();
        if ($input['simpan'] == 'Simpan') {
            $SaveData = new Skoringuri;
            $SaveData->id_registrasi = $id;
            $SaveData->skoring = $input['skoring'];
            $SaveData->bahan = $input['bahan'];
            $SaveData->siklus = $siklus;
            $SaveData->type = $type;
            $SaveData->tahun = $date;
            $SaveData->catatan = $input['catatan'];
            $SaveData->save();
        }else{
            $instrumen = DB::table('tb_instrumen')->where('id_parameter', 'urinalisa')->get();
            $data = DB::table('parameter')
                ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
                ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
                ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
                ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
                ->leftjoin('tb_reagen_imunologi', 'hp_details.reagen', '=', 'tb_reagen_imunologi.id')
                ->leftjoin('tb_instrumen','hp_details.alat', '=', 'tb_instrumen.id')
                ->leftjoin('tb_hp','hp_details.hasil_pemeriksaan', '=', 'tb_hp.id')
                ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.alat','tb_instrumen.id as idinstrumen', 'tb_instrumen.instrumen','hp_details.instrument_lain', 'hp_details.id as id_detail','hp_details.reagen as hp_reagen','tb_reagen_imunologi.reagen as imu_reagen','hp_details.reagen_lain', 'tb_hp.hp')
                ->where('parameter.kategori', 'urinalisa')
                ->where('hp_headers.id_registrasi', $id)
                ->where('hp_headers.type', $type)
                ->where('hp_headers.siklus', $siklus)
                ->get();
                foreach ($data as $key => $val) {
                    $metode = MetodePemeriksaan::where('parameter_id', $val->id)->get();
                    $val->metode = $metode;
                    $reagen = ReagenImunologi::where('parameter_id', $val->id)->get();
                    $val->reagen = $reagen;
                    $hasilPemeriksaan = TbHp::where('parameter_id', $val->id)->get();
                    $val->hasilPemeriksaan = $hasilPemeriksaan;
                    $target = Rujukanurinalisa::where('parameter', $val->id)
                                                ->where('siklus', $siklus)
                                                ->where('tahun', $date)
                                                ->get();
                    $val->target = $target;
                }
                // dd($data);
            $skoring = DB::table('tb_skoring_urinalisa')->where('id_registrasi', $id)->where('siklus', $siklus)->where('type', $type)->where('tahun', $date)->get();
            $datas = HpHeader::where('id_registrasi', $id)
                ->where('type', $type)
                ->first();

            $register = Register::find($id);
            $perusahaan = $register->perusahaan->nama_lab;

            return view('evaluasi/urinalisa/input', compact('data', 'perusahaan', 'datas', 'type','siklus','date','instrumen', 'tanggal', 'skoring'));

        }
        return redirect::back();
    }

    public function penilaianparameter(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $peserta = DB::table('tb_registrasi')->where('id', $id)->first();
        // dd($reagen);
        if ($peserta->periode == 2 && $siklus == 1) {
            $periode = "AND tb_registrasi.periode = 2";
        }else{
            $periode = "AND tb_registrasi.periode IS NULL";
        }
        $parameter = DB::table('parameter')->where('kategori', '=', 'urinalisa')->get();
        $reagen = Rujukanurinalisa::where('siklus', $siklus)
                                    ->where('tahun', $date)
                                    ->where('type', $type)
                                    ->where(function($query) use ($peserta, $siklus){
                                        if ($peserta->periode == 2 && $siklus == 1) {
                                            $query->where('periode','=', '2');
                                        }else{
                                            $query->whereNull('periode');
                                        }
                                    })
                                    ->count();
        foreach ($parameter as $key => $val) {
            if ($val->id > 10) {
                $masing = DB::table('tb_hp')
                            ->select('tb_hp.*',
                                DB::raw('(SELECT COUNT(*) from hp_details as HPD
                                        INNER JOIN `hp_headers` on `hp_headers`.`id` = `HPD`.`hp_header_id`
                                        INNER JOIN `tb_registrasi` on `tb_registrasi`.`id` = `hp_headers`.`id_registrasi`
                                            WHERE
                                                `HPD`.`hasil_pemeriksaan` = `tb_hp`.`id`
                                            AND
                                                `HPD`.`parameter_id` = tb_hp.parameter_id
                                            AND
                                                `hp_headers`.`type` = "'.$type.'"
                                            AND
                                                `hp_headers`.`siklus` = '.$siklus.'
                                            AND
                                                YEAR(tb_registrasi.created_at) = '.$date.'
                                            '.$periode.'
                                  ) as Jumlah'))
                            ->where('tb_hp.parameter_id', '=', $val->id)
                            ->groupBy('tb_hp.id')
                            ->orderBy(DB::raw('FIELD(hp, "±","Negatif")'), 'desc')
                            ->get();
                $hasilna = DB::table('hp_details')
                            ->join('hp_headers', 'hp_details.hp_header_id','=','hp_headers.id')
                            ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                            ->join('tb_hp','hp_details.hasil_pemeriksaan','=','tb_hp.id')
                            ->where('hp_details.parameter_id', $val->id)
                            ->where('hp_headers.type', $type)
                            ->where('hp_headers.siklus', $siklus)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                            ->where('tb_registrasi.id', $id)
                            ->select('hp_details.hasil_pemeriksaan','tb_hp.hp')
                            ->first();
            }else{
                $masing = DB::table('tb_hp')
                            ->select('tb_hp.*',
                                DB::raw('(SELECT COUNT(*) from hp_details as HPD
                                        INNER JOIN `hp_headers` on `hp_headers`.`id` = `HPD`.`hp_header_id`
                                        INNER JOIN `tb_registrasi` on `tb_registrasi`.`id` = `hp_headers`.`id_registrasi`
                                            WHERE
                                                (`HPD`.`hasil_pemeriksaan` = `tb_hp`.`hp` or `HPD`.`hasil_pemeriksaan` = `tb_hp`.`id`)
                                            AND
                                                `HPD`.`parameter_id` = tb_hp.parameter_id
                                            AND
                                                `hp_headers`.`type` = "'.$type.'"
                                            AND
                                                `hp_headers`.`siklus` = '.$siklus.'
                                            AND
                                                YEAR(tb_registrasi.created_at) = '.$date.'
                                            '.$periode.'
                                  ) as Jumlah'))
                            ->where('tb_hp.parameter_id', '=', $val->id)
                            ->groupBy('tb_hp.id')
                            ->orderBy(DB::raw('FIELD(hp, "±","Negatif")'), 'desc')
                            ->get();

                $hasilna = DB::table('hp_details')
                            ->join('hp_headers', 'hp_details.hp_header_id','=','hp_headers.id')
                            ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                            ->leftjoin('tb_hp','hp_details.hasil_pemeriksaan','=','tb_hp.id')
                            ->where('hp_details.parameter_id', $val->id)
                            ->where('tb_hp.parameter_id', $val->id)
                            ->where('hp_headers.type', $type)
                            ->where('hp_headers.siklus', $siklus)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                            ->where('tb_registrasi.id', $id)
                            ->select('hp_details.hasil_pemeriksaan', 'tb_hp.hp')
                            ->first();
            }
            $target = Rujukanurinalisa::where('parameter', $val->id)
                                        ->where('siklus', $siklus)
                                        ->where('tahun', $date)
                                        ->where('type', $type)
                                        ->where(function($query) use ($peserta, $siklus){
                                            if ($peserta->periode == 2 && $siklus == 1) {
                                                $query->where('periode','=', '2');
                                            }else{
                                                $query->whereNull('periode');
                                            }
                                        })
                                        ->select('rujukan')
                                        ->first();
            $val->target = $target;
            $val->masing = $masing;
            $val->hasilna = $hasilna;
        }
        // dd($parameter);
        if ($reagen >= '11') {
            return view('evaluasi.urinalisa.grafikparameter.grafik', compact('parameter', 'peserta', 'date','siklus', 'type'));
        }else{
            Session::flash('message', 'Data Rujukan Belum Lengkap!');
            Session::flash('alert-class', 'alert-danger');
            return redirect::back();
        }
        // dd($parameter);
    }

    public function penilaianreagen(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $peserta = DB::table('tb_registrasi')->find($id);

        $reagen = DB::table('hp_details')
                    ->join('tb_reagen_imunologi','hp_details.reagen','=','tb_reagen_imunologi.id')
                    ->join('hp_headers','hp_details.hp_header_id','=','hp_headers.id')
                    ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                    ->select('tb_reagen_imunologi.reagen','tb_reagen_imunologi.id', 'hp_headers.kode_lab', 'tb_registrasi.kode_lebpes', DB::raw('COUNT(DISTINCT tb_registrasi.id) as jumlah'))
                    ->groupBy('hp_details.reagen')
                    ->where('hp_headers.type', $type)
                    ->where('hp_headers.siklus', $siklus)
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                    ->whereNotIn('hp_details.parameter_id', [19])
                    ->orderBy('tb_reagen_imunologi.reagen', 'asc')
                    ->get();

        $data = DB::table('hp_details')
                ->join('tb_reagen_imunologi','hp_details.reagen','=','tb_reagen_imunologi.id')
                ->join('hp_headers','hp_details.hp_header_id','=','hp_headers.id')
                ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                ->select('tb_reagen_imunologi.reagen','tb_reagen_imunologi.id', 'tb_registrasi.kode_lebpes')
                ->where('hp_headers.type', $type)
                ->where('hp_headers.siklus', $siklus)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->where('tb_registrasi.id', $id)
                ->whereNotIn('hp_details.parameter_id', [19])
                ->get();
        $kehamilan = DB::table('hp_details')
                    ->join('tb_reagen_imunologi','hp_details.reagen','=','tb_reagen_imunologi.id')
                    ->join('hp_headers','hp_details.hp_header_id','=','hp_headers.id')
                    ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                    ->select('tb_reagen_imunologi.reagen','tb_reagen_imunologi.id', 'hp_headers.kode_lab', DB::raw('COUNT(DISTINCT tb_registrasi.id) as jumlah'))
                    ->groupBy('hp_details.reagen')
                    ->where('hp_headers.type', $type)
                    ->where('hp_headers.siklus', $siklus)
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                    ->whereIn('hp_details.parameter_id', [19])
                    ->orderBy('tb_reagen_imunologi.reagen', 'asc')
                    ->get();
        $data2 = DB::table('hp_details')
                ->join('tb_reagen_imunologi','hp_details.reagen','=','tb_reagen_imunologi.id')
                ->join('hp_headers','hp_details.hp_header_id','=','hp_headers.id')
                ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                ->select('tb_reagen_imunologi.reagen','tb_reagen_imunologi.id', 'tb_registrasi.kode_lebpes')
                ->where('hp_headers.type', $type)
                ->where('hp_headers.siklus', $siklus)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->where('tb_registrasi.id', $id)
                ->whereIn('hp_details.parameter_id', [19])
                ->get();
        return view('evaluasi.urinalisa.grafikreagen.grafik', compact('reagen','data','kehamilan','data2','siklus','type','date', 'peserta'));
        // dd($parameter);
    }

    public function penilaianreagenkehamilan(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;
                // dd($reagen);
        return view('evaluasi.urinalisa.grafikreagenkehamilan.grafik', compact('reagen','data'));
        // dd($parameter);
    }

    public function perusahaan(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.kode_lebpes, tb_registrasi.id as idregistrasi'))
                    ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang','=','3')
                    ->where('tb_registrasi.status','>=','2')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                    ->where(function($query) use ($siklus){
                        $query->where('tb_registrasi.siklus', '12')
                            ->orwhere('tb_registrasi.siklus', $siklus);
                    })
                    ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="urinalisa/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Cetak
                                    </button>
                                </a>'
                        ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if($siklus == 1) {
                        if ($inputhasil->status_data1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }else{
                        if ($inputhasil->status_datarpr1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if($siklus == 1) {
                        if ($inputhasil->status_data1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }else{
                        if ($inputhasil->status_datarpr1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib'])
            ->make(true);
        }
        return view('evaluasi.urinalisa.rekapcetakpeserta.perusahaan');
    }

    public function dataperusahaan(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '3')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.urinalisa.rekapcetakpeserta.dataperusahaan', compact('data','siklus'));
    }

    public function autoSave(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $siklus = $input['siklus'];
        $type = $input['type'];
        $date = $input['date'];
        Session::flash('message', 'System sedang melakukan proses evaluasi.');
        Session::flash('alert-class', 'alert-info');
        $jobs = new JobsEvaluasi;
        $jobs->name = 'evaluasi-urinalisa';
        $jobs->description = 'Evaluasi Urinalisa';
        $jobs->status = 0;
        $jobs->isread = 0;
        $jobs->save();
        Evaluasi::dispatch($input,$jobs->name)->onConnection('redis');
        return redirect('evaluasi/urinalisa');
    }

}
<?php
namespace App\Http\Controllers;
use DB;
use Input;
use PDF;
use Carbon\Carbon;

use App\HpHeader;
use App\Parameter;
use App\MetodePemeriksaan;
use App\HpDetail;
use App\ZScore;
use App\ZScoreAlat;
use App\ZScoreMetode;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\kimiaklinik as Kimiaklinik;
use App\daftar as Daftar;
use App\register as Register;
use App\TbReagenImunologi as ReagenImunologi;
use App\TbHp;
use App\Rujukanurinalisa;
use Redirect;
use Validator;
use Session;
use App\User;
use App\SdMedian;
use App\SdMedianMetode;
use App\Ring;
use App\CatatanImun;
use App\SdMedianAlat;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Excel;
use App\statusbakteri;


class GrafikEvaluasiController extends Controller
{
    
    public function __construct()
    {

    }
    public function sif(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('siklus');

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;
        // dd($date);
        $grafikbidang1 = DB::table('sub_bidang')->where('id','7')->first();
        $grafikdata1 = DB::table('badan_usaha')->get();
        foreach($grafikdata1 as $skey => $r)
        {
            $semua = DB::table('tb_registrasi')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where('tb_registrasi.status','>=','2')
                        ->where('tb_registrasi.bidang','=','7')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->count();
            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang','=','7')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                            ->count();
            $r->persen = $peserta / $semua * 100;
        }

        $grafikdata2 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->groupBy('tb_reagen_imunologi.id')
                    ->orderBy('jumlah', 'desc')
                    ->get();
        $lainlain4 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->where('tb_reagen_imunologi.reagen', '=', 'Lain - lain')
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'reagen_imunologi.reagen_lain','master_imunologi.jenis_form')
                    ->groupBy('reagen_imunologi.reagen_lain')
                    ->orderBy('jumlah', 'desc')
                    ->get();
        $grafiktotal2 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->count();
    
        $grafikdata3 = DB::table('badan_usaha')->get();
        foreach ($grafikdata3 as $key => $val) {
            $baik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.type','=', 'Syphilis')
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                        ->where('tb_registrasi.bidang', '=', '7')
                        ->where('perusahaan.pemerintah',$val->id)
                        ->count();
            $tidakbaik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.type','=', 'Syphilis')
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                        ->where('tb_registrasi.bidang', '=', '7')
                        ->where('perusahaan.pemerintah',$val->id)
                        ->count();

            $baikrpr = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.type','=', 'rpr-syphilis')
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                        ->where('tb_registrasi.bidang', '=', '7')
                        ->where('perusahaan.pemerintah',$val->id)
                        ->count();
            $tidakbaikrpr = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.type','=', 'rpr-syphilis')
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                        ->where('tb_registrasi.bidang', '=', '7')
                        ->where('perusahaan.pemerintah',$val->id)
                        ->count();

            $val->baikrpr = $baikrpr;
            $val->tidakbaikrpr = $tidakbaikrpr;
            $val->baik = $baik;
            $val->tidakbaik = $tidakbaik;
        }

        $grafikdata4 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where('master_imunologi.siklus', $siklus)
                    ->groupBy('reagen_imunologi.nama_reagen', 'master_imunologi.jenis_form')
                    ->select('tb_reagen_imunologi.reagen','reagen_imunologi.nama_reagen', 'master_imunologi.jenis_form')
                    ->get();
        foreach ($grafikdata4 as $key => $val) {
            $total = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->count();
            $val->total = $total;
            $sesuai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Baik')
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->sesuai = $sesuai;
            $tidaksesuai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Kurang')
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->tidaksesuai = $tidaksesuai;
            $tidakdinilai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Tidak dapat dinilai')
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->tidakdinilai = $tidakdinilai;
        }


        $grafikbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_kesimpulan_evaluasi.type','=', 'Syphilis')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->count();
        $grafikpbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_kesimpulan_evaluasi.type','=', 'Syphilis')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->where('tb_kesimpulan_evaluasi.type', 'Syphilis')
                    ->count();
        $grafikptidakbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->where('tb_kesimpulan_evaluasi.type','=', 'Syphilis')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakdapatnilai5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->where('tb_kesimpulan_evaluasi.type', 'Syphilis')
                    ->count();
        $grafikptidakdapatnilai5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->where('tb_kesimpulan_evaluasi.type','=', 'Syphilis')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();

        $grafikbaikrpr5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_kesimpulan_evaluasi.type','=', 'rpr-syphilis')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->count();
        $grafikpbaikrpr5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_kesimpulan_evaluasi.type','=', 'rpr-syphilis')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakbaikrpr5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->where('tb_kesimpulan_evaluasi.type', 'rpr-syphilis')
                    ->count();
        $grafikptidakbaikrpr5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->where('tb_kesimpulan_evaluasi.type','=', 'rpr-syphilis')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakdapatnilairpr5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->where('tb_kesimpulan_evaluasi.type', 'rpr-syphilis')
                    ->count();
        $grafikptidakdapatnilairpr5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->where('tb_kesimpulan_evaluasi.type','=', 'rpr-syphilis')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();

        $type = $request->get('y');
        
        $tahun = $q2->year;
        // $catatan = CatatanImun::where('id_registrasi', $id)->where('siklus', $type)->where('tahun', $tahun)->get();
        // dd($catatan)->first();
        return view('data_evaluasi/grafik/sif', compact('data','tanggal','datas','id','type','siklus','tahunevaluasi', 'grafikbidang1', 'grafikdata1', 'grafikdata2', 'grafiktotal2', 'grafikdata3', 'grafikdata4', 'grafikbaik5', 'grafiktidakbaik5', 'grafiktidakdapatnilai5', 'grafikbaikrpr5', 'grafiktidakbaikrpr5', 'grafiktidakdapatnilairpr5','grafikptidakbaik5', 'grafikptidakdapatnilairpr5', 'grafikptidakbaikrpr5', 'grafikpbaikrpr5','grafikptidakdapatnilai5', 'grafikpbaik5','lainlain4'));
    }

    public function hbs(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('siklus');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;
        
        $grafikbidang1 = DB::table('sub_bidang')->where('id','8')->first();
        $grafikdata1 = DB::table('badan_usaha')->get();
        foreach($grafikdata1 as $skey => $r)
        {
            $semua = DB::table('tb_registrasi')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where('tb_registrasi.status','>=','2')
                        ->where('tb_registrasi.bidang','=','8')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->count();
            $peserta = DB::table('tb_registrasi')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where('perusahaan.pemerintah','=',$r->id)
                        ->where('tb_registrasi.status','>=','2')
                        ->where('tb_registrasi.bidang','=','8')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->count();
            $r->persen = $peserta / $semua * 100;
        }

        $grafikdata2 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->groupBy('tb_reagen_imunologi.id')
                    ->orderBy('jumlah', 'desc')
                    ->get();
        $lainlain4 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->where('tb_reagen_imunologi.reagen', '=', 'Lain - lain')
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'reagen_imunologi.reagen_lain','master_imunologi.jenis_form')
                    ->groupBy('reagen_imunologi.reagen_lain')
                    ->orderBy('jumlah', 'desc')
                    ->get();
        // dd($lainlain4);
        $grafiktotal2 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->count();

        $grafikdata3 = DB::table('badan_usaha')->get();
        foreach ($grafikdata3 as $key => $val) {
            $baik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                        ->where('tb_registrasi.bidang', '=', '8')
                        ->where('perusahaan.pemerintah', $val->id)
                        ->count();
            $tidakbaik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                        ->where('tb_registrasi.bidang', '=', '8')
                        ->where('perusahaan.pemerintah', $val->id)
                        ->count();
            $val->baik = $baik;
            $val->tidakbaik = $tidakbaik;
        }

        $grafikdata4 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where('master_imunologi.siklus', $siklus)
                    ->groupBy('reagen_imunologi.nama_reagen', 'master_imunologi.jenis_form')
                    ->select('tb_reagen_imunologi.reagen','reagen_imunologi.nama_reagen', 'master_imunologi.jenis_form')
                    ->get();
        foreach ($grafikdata4 as $key => $val) {
            $total = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->total = $total;
            $sesuai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Baik')
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->sesuai = $sesuai;
            $tidaksesuai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Kurang')
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->tidaksesuai = $tidaksesuai;
            $tidakdinilai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Tidak dapat dinilai')
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->tidakdinilai = $tidakdinilai;
        }
        $grafikbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->count();
        $grafikpbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->count();
        $grafikptidakbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakdapatnilai5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->count();
        $grafikptidakdapatnilai5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();

        // dd($grafikdata2);

        return view('data_evaluasi/grafik/hbs', compact('datas','tanggal','data','id','type','siklus','tahunevaluasi', 'grafikbidang1', 'grafikdata1', 'grafikdata2', 'grafiktotal2', 'grafikdata3', 'grafikdata4', 'grafikbaik5', 'grafiktidakbaik5', 'grafiktidakdapatnilai5', 'grafikpbaik5', 'grafikptidakbaik5', 'grafikptidakdapatnilai5','lainlain4'));
    }

    public function hcv(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('siklus');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $grafikbidang1 = DB::table('sub_bidang')->where('id','9')->first();
        $grafikdata1 = DB::table('badan_usaha')->get();
        foreach($grafikdata1 as $skey => $r)
        {
            $semua = DB::table('tb_registrasi')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where('tb_registrasi.status','>=','2')
                        ->where('tb_registrasi.bidang','=','9')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->count();
            $peserta = DB::table('tb_registrasi')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where('perusahaan.pemerintah','=',$r->id)
                        ->where('tb_registrasi.status','>=','2')
                        ->where('tb_registrasi.bidang','=','9')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->count();
            $r->persen = $peserta / $semua * 100;
        }

        $grafikdata2 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->groupBy('tb_reagen_imunologi.id')
                    ->orderBy('jumlah', 'desc')
                    ->get();
        $lainlain4 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->where('tb_reagen_imunologi.reagen', '=', 'Lain - lain')
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'reagen_imunologi.reagen_lain','master_imunologi.jenis_form')
                    ->groupBy('reagen_imunologi.reagen_lain')
                    ->orderBy('jumlah', 'desc')
                    ->get();

        $grafiktotal2 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->count();

        $grafikdata3 = DB::table('badan_usaha')->get();
        foreach ($grafikdata3 as $key => $val) {
            $baik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                        ->where('tb_registrasi.bidang', '=', '9')
                        ->where('perusahaan.pemerintah', $val->id)
                        ->count();
            $tidakbaik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                        ->where('tb_registrasi.bidang', '=', '9')
                        ->where('perusahaan.pemerintah', $val->id)
                        ->count();
            $val->baik = $baik;
            $val->tidakbaik = $tidakbaik;
        }

        $grafikdata4 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->where('master_imunologi.siklus', $siklus)
                    ->groupBy('reagen_imunologi.nama_reagen', 'master_imunologi.jenis_form')
                    ->select('tb_reagen_imunologi.reagen','reagen_imunologi.nama_reagen', 'master_imunologi.jenis_form')
                    ->get();
        foreach ($grafikdata4 as $key => $val) {
            $total = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->total = $total;
            $sesuai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Baik')
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->sesuai = $sesuai;
            $tidaksesuai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Kurang')
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->tidaksesuai = $tidaksesuai;
            $tidakdinilai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Tidak dapat dinilai')
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->tidakdinilai = $tidakdinilai;
        }

        $grafikbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->count();
        $grafikpbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->count();
        $grafikptidakbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakdapatnilai5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->count();
        $grafikptidakdapatnilai5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();

        return view('data_evaluasi/grafik/hcv', compact('data','id','type','tanggal','siklus','tahunevaluasi','datas', 'grafikbidang1', 'grafikdata1', 'grafikdata2', 'grafiktotal2', 'grafikdata3', 'grafikdata4', 'grafikbaik5', 'grafiktidakbaik5', 'grafiktidakdapatnilai5', 'grafikpbaik5', 'grafikptidakbaik5', 'grafikptidakdapatnilai5','lainlain4'));
    }

    public function hiv(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('siklus');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $perusahaan = DB::table('perusahaan')
                    ->join('tb_registrasi', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                    ->where('tb_registrasi.id', $id)
                    ->first();
        
        $grafikbidang1 = DB::table('sub_bidang')->where('id','6')->first();
        $grafikdata1 = DB::table('badan_usaha')->get();
        foreach($grafikdata1 as $skey => $r)
        {
            $semua = DB::table('tb_registrasi')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where('tb_registrasi.status','>=','2')
                        ->where('tb_registrasi.bidang','=','6')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->where(function($query) use ($perusahaan){
                            if ($perusahaan->pemerintah == '9') {
                                $query->where('perusahaan.pemerintah','=','9');
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        })
                        ->count();
            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang','=','6')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                            ->where(function($query) use ($perusahaan){
                                if ($perusahaan->pemerintah == '9') {
                                    $query->where('perusahaan.pemerintah','=','9');
                                }else{
                                    $query->where('perusahaan.pemerintah','!=','9');
                                }
                            })
                            ->count();
            $r->persen = $peserta / $semua * 100;
        }

        $grafikdata2 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query) use ($perusahaan){
                        if ($perusahaan->pemerintah == '9') {
                            $query->where('perusahaan.pemerintah','=','9');
                        }else{
                            $query->where('perusahaan.pemerintah','!=','9');
                        }
                    })
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->groupBy('tb_reagen_imunologi.id')
                    ->orderBy('jumlah', 'desc')
                    ->get();
        $lainlain4 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query) use ($perusahaan){
                        if ($perusahaan->pemerintah == '9') {
                            $query->where('perusahaan.pemerintah','=','9');
                        }else{
                            $query->where('perusahaan.pemerintah','!=','9');
                        }
                    })
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->where('tb_reagen_imunologi.reagen', '=', 'Lain - lain')
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'reagen_imunologi.reagen_lain','master_imunologi.jenis_form')
                    ->groupBy('reagen_imunologi.reagen_lain')
                    ->orderBy('jumlah', 'desc')
                    ->get();

        $grafiktotal2 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query) use ($perusahaan){
                        if ($perusahaan->pemerintah == '9') {
                            $query->where('perusahaan.pemerintah','=','9');
                        }else{
                            $query->where('perusahaan.pemerintah','!=','9');
                        }
                    })
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->count();

        $grafikdata3 = DB::table('badan_usaha')
                ->where(function($query) use ($perusahaan){
                    if ($perusahaan->pemerintah == '9') {
                        $query->where('id','=','9');
                    }else{
                        $query->where('id','!=','9');
                    }
                })
                ->get();
        foreach ($grafikdata3 as $key => $val) {
            $baik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                        ->where('tb_registrasi.bidang', '=', '6')
                        ->where('perusahaan.pemerintah', $val->id)
                        ->count();
            $tidakbaik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                        ->where('tb_registrasi.bidang', '=', '6')
                        ->where('perusahaan.pemerintah', $val->id)
                        ->count();
            $val->baik = $baik;
            $val->tidakbaik = $tidakbaik;
        }


        $grafikbaik4 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query) use ($perusahaan){
                        if ($perusahaan->pemerintah == '9') {
                            $query->where('perusahaan.pemerintah','=','9');
                        }else{
                            $query->where('perusahaan.pemerintah','!=','9');
                        }
                    })
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->count();
        $grafikpesertabaik4 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakbaik4 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where(function($query) use ($perusahaan){
                        if ($perusahaan->pemerintah == '9') {
                            $query->where('perusahaan.pemerintah','=','9');
                        }else{
                            $query->where('perusahaan.pemerintah','!=','9');
                        }
                    })
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->count();
        $grafikpesertatidakbaik4 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakdapatnilai4 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query) use ($perusahaan){
                        if ($perusahaan->pemerintah == '9') {
                            $query->where('perusahaan.pemerintah','=','9');
                        }else{
                            $query->where('perusahaan.pemerintah','!=','9');
                        }
                    })
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->count();
        $grafikpesertatidakdapatdinilai4 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();

        $grafikdata5 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query) use ($perusahaan){
                        if ($perusahaan->pemerintah == '9') {
                            $query->where('perusahaan.pemerintah','=','9');
                        }else{
                            $query->where('perusahaan.pemerintah','!=','9');
                        }
                    })
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where('master_imunologi.siklus', $siklus)
                    ->groupBy('reagen_imunologi.nama_reagen')
                    ->select('tb_reagen_imunologi.reagen','reagen_imunologi.nama_reagen')
                    ->get();

        foreach ($grafikdata5 as $key => $val) {
            $total = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                        ->where(function($query) use ($perusahaan){
                            if ($perusahaan->pemerintah == '9') {
                                $query->where('perusahaan.pemerintah','=','9');
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        })
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->total = $total;
            $sesuai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                        ->where(function($query) use ($perusahaan){
                            if ($perusahaan->pemerintah == '9') {
                                $query->where('perusahaan.pemerintah','=','9');
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        })
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Baik')
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->sesuai = $sesuai;
            $tidaksesuai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                        ->where(function($query) use ($perusahaan){
                            if ($perusahaan->pemerintah == '9') {
                                $query->where('perusahaan.pemerintah','=','9');
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        })
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Kurang')
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->tidaksesuai = $tidaksesuai;
            $tidakdinilai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                        ->where(function($query) use ($perusahaan){
                            if ($perusahaan->pemerintah == '9') {
                                $query->where('perusahaan.pemerintah','=','9');
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        })
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Tidak dapat dinilai')
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->tidakdinilai = $tidakdinilai;
        }

        return view('data_evaluasi/grafik/hiv', compact('data','tanggal','datas','id','type','siklus','tahunevaluasi', 'perusahaan','grafikbidang1','grafikdata1', 'grafikdata2', 'grafiktotal2', 'grafikdata3', 'grafikbaik4', 'grafiktidakbaik4', 'grafikpesertabaik4', 'grafikpesertatidakbaik4', 'grafikpesertatidakdapatdinilai4', 'grafiktidakdapatnilai4', 'grafikdata5','lainlain4'));
    }
}
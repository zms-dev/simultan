<?php

namespace App\Http\Controllers;
use DB;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\register as Register;
use Excel;
use PDF;
class EvaluasiUrinalisaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function rekappeserta()
    {
        return view('evaluasi.rekap.urinalisa.excel');
    }
    public function rekappesertana(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameterna = DB::table('parameter')->where('kategori', 'urinalisa')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                ->where('tb_registrasi.bidang', '=', '3')
                ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                })
                ->where(function($query) use ($input){
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $input['siklus']);
                })
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', $input['type'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $input['tahun'])
                ->orderBy('tb_registrasi.created_by', 'asc')
                ->select('hp_headers.*', 'perusahaan.nama_lab')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'urinalisa')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                            ->leftjoin('tb_reagen_imunologi','tb_reagen_imunologi.id','=','hp_details.reagen')
                            ->leftjoin('tb_hp', 'tb_hp.id', '=', 'hp_details.hasil_pemeriksaan')
                            ->where('hp_details.hp_header_id', $val->id)
                            ->where('hp_details.parameter_id', $par->id)
                            ->select('hp_details.alat', 'hp_details.parameter_id', 'hp_details.hasil_pemeriksaan', 'hp_details.metode_lain', 'hp_details.reagen_lain', 'metode_pemeriksaan.metode_pemeriksaan', 'tb_reagen_imunologi.reagen', 'tb_hp.hp')
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data);
        // return view('evaluasi.rekap.urinalisa.rekappeserta.index', compact('data'));

        Excel::create('Rekap Peserta Urinalisa', function($excel) use ($data, $input, $parameterna) {
            // $excel->sheet('PME', function($sheet) use ($data, $input, $parameterna) {
            //     $sheet->loadView('evaluasi.rekap.urinalisa.rekappeserta.index', array('data'=>$data, 'input'=>$input, 'parameterna'=>$parameterna) );
            // });
            $excel->sheet('Data', function($sheet)  use ($data, $parameterna){

                $sheet->mergeCells('C1:F1');
                $sheet->mergeCells('G1:J1');
                $sheet->mergeCells('K1:N1');
                $sheet->mergeCells('O1:R1');
                $sheet->mergeCells('S1:V1');
                $sheet->mergeCells('W1:Z1');
                $sheet->mergeCells('AA1:AD1');
                $sheet->mergeCells('AE1:AH1');
                $sheet->mergeCells('AI1:AL1');
                $sheet->mergeCells('AM1:AP1');
                $sheet->mergeCells('AQ1:AT1');
                $sheet->setMergeColumn(array(
                    'columns' => array('A','B'),
                    'rows' => array(
                        array(1,2),
                        array(1,2),
                    )
                ));
                $sheet->cell('A1', function($cell) {
                    $cell->setValue('Kode Lab');
                });
                $sheet->cell('B1', function($cell) {
                    $cell->setValue('Nama Instansi');
                });
                
                $alphas = mkRange('C', 'AZ');
                $nop = -4;
                $a = 0;
                foreach($parameterna as $val){
                    $nop++;
                    $nop++;
                    $nop++;
                    $nop++;
                    $sheet->cell($alphas[$nop].'1', function($cell) use ($val) {
                        $cell->setValue(strip_tags($val->nama_parameter));
                    });
                    $sheet->cell($alphas[$a].'2', function($cell) {
                        $cell->setValue('Hasil');
                    });
                    $a++; 
                    $sheet->cell($alphas[$a].'2', function($cell) {
                        $cell->setValue('Instrument');
                    });
                    $a++; 
                    $sheet->cell($alphas[$a].'2', function($cell) {
                        $cell->setValue('Metode');
                    });
                    $a++; 
                    $sheet->cell($alphas[$a].'2', function($cell) {
                        $cell->setValue('Reagen');
                    });
                    $a++; 
                }
                // dd($alphas[$nop]);
                
                $startRow = 3;
                foreach($data as $val){     
                    $sheet->cell('A'.$startRow, function($cell) use ($val) {
                        $cell->setValue($val->kode_lab);
                    }); 
                    $sheet->cell('B'.$startRow, function($cell) use ($val) {
                        $cell->setValue($val->nama_lab);
                    }); 
                    $a = 0; 
                    foreach($val->parameter as $par){
                        foreach($par->detail as $hasil){
                            if($hasil->parameter_id <= '10'){
                                $sheet->cell($alphas[$a].$startRow, function($cell) use ($hasil) {
                                    if($hasil->hp != NULL){
                                        $cell->setValue($hasil->hp);
                                    }else{
                                        $cell->setValue($hasil->hasil_pemeriksaan);
                                    }
                                }); 
                            }else{
                                $sheet->cell($alphas[$a].$startRow, function($cell) use ($hasil) {
                                    $cell->setValue($hasil->hp);
                                }); 
                            }
                            if($par->catatan == "Hasil pemeriksaan menggunakan 3 (tiga) desimal"){
                                $format = '0.000';
                            }elseif($par->catatan == "Hasil pemeriksaan tanpa desimal"){
                                $format = '0';
                            }elseif($par->catatan == "Hasil pemeriksaan menggunakan 1 (satu) desimal"){
                                $format = '0.0';
                            }
                            $sheet->setColumnFormat(array(
                                $alphas[$a].$startRow => $format
                            ));
                            $a++;
                            $sheet->cell($alphas[$a].$startRow, function($cell) use ($hasil) {
                                $cell->setValue($hasil->alat);
                            }); 
                            $a++;
                            $sheet->cell($alphas[$a].$startRow, function($cell) use ($hasil) {
                                $cell->setValue($hasil->metode_pemeriksaan .' '. $hasil->metode_lain);
                            }); 
                            $a++;
                            $sheet->cell($alphas[$a].$startRow, function($cell) use ($hasil) {
                                $cell->setValue($hasil->reagen .' '. $hasil->reagen_lain);
                            }); 
                            $a++;
                        }
                        
                    }
                    $startRow++;
                }
                $highestRow = $startRow - 1;
                $startRow = 2;

            });
        })->download('xls');
    }

    public function urinalisarekapitulasi()
    {
        return view('evaluasi.urinalisa.rekapitulasihasil.index');
    }

    public function urinalisarekapitulasina(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $periode = "";
        if ($input['siklus'] == 1) {
            if ($input['periode'] == 2 && $input['tahun'] == 2019) {
                $periode = "AND tb_registrasi.periode = 2";
                $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1))";
            }else{
                $periode = "AND tb_registrasi.periode IS NULL";
                $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1))";
            }
        }else{
            $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 2))";
        }
        $data = DB::table('tb_registrasi')
                ->where('bidang', '=', '3')
                ->where('status', '=', '3')
                ->where(DB::raw('YEAR(created_at)'), '=' , $input['tahun'])
                ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                })
                ->where(function($query) use ($input){
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $input['siklus']);
                })
                ->count();
        
        $kirima = DB::table('hp_headers')
                ->join('tb_registrasi', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', '=', 'a')
                ->where('tb_registrasi.bidang', '=', 3)
                ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                })
                ->where(function($query) use ($input){
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $input['siklus']);
                })
                ->count();
        $kirimb = DB::table('hp_headers')
                ->join('tb_registrasi', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', '=', 'b')
                ->where('tb_registrasi.bidang', '=', 3)
                ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                })
                ->where(function($query) use ($input){
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $input['siklus']);
                })
                ->count();
        if($input['siklus'] == '1'){
            $beluminput1 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang','=','3')
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.pemeriksaan','=', NULL)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input){
                                if ($input['siklus'] == 1) {
                                    if ($input['periode'] == 2) {
                                        $query->where('tb_registrasi.periode','=','2');
                                    }else{
                                        $query->WhereNull('tb_registrasi.periode');
                                    }
                                }
                            })
                            ->where(function($query) use ($input){
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $beluminput2 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang','=','3')
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.pemeriksaan2','=', NULL)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input){
                                if ($input['siklus'] == 1) {
                                    if ($input['periode'] == 2) {
                                        $query->where('tb_registrasi.periode','=','2');
                                    }else{
                                        $query->WhereNull('tb_registrasi.periode');
                                    }
                                }
                            })
                            ->where(function($query) use ($input){
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
        }else{
            $beluminput1 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang','=','3')
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.rpr1','=', NULL)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input){
                                if ($input['siklus'] == 1) {
                                    if ($input['periode'] == 2) {
                                        $query->where('tb_registrasi.periode','=','2');
                                    }else{
                                        $query->WhereNull('tb_registrasi.periode');
                                    }
                                }
                            })
                            ->where(function($query) use ($input){
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $beluminput2 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang','=','3')
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.rpr2','=', NULL)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input){
                                if ($input['siklus'] == 1) {
                                    if ($input['periode'] == 2) {
                                        $query->where('tb_registrasi.periode','=','2');
                                    }else{
                                        $query->WhereNull('tb_registrasi.periode');
                                    }
                                }
                            })
                            ->where(function($query) use ($input){
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
        }
        $data1 = "SELECT 
                    ( 
                        SELECT count(*) FROM `tb_skoring_urinalisa`
                        INNER JOIN tb_registrasi ON tb_skoring_urinalisa.id_registrasi = tb_registrasi.id
                        WHERE 
                            tb_skoring_urinalisa.siklus = ".$input['siklus']."
                        AND
                            type = 'a'
                        AND
                            tahun = ".$input['tahun']."
                        AND
                            ( (skoring > 3) ) 
                        ".$periode."
                        ".$datasiklus."
                    ) AS SANGAT_BAIK,
                    ( 
                        SELECT count(*) FROM `tb_skoring_urinalisa`
                        INNER JOIN tb_registrasi ON tb_skoring_urinalisa.id_registrasi = tb_registrasi.id
                        WHERE
                            tb_skoring_urinalisa.siklus = ".$input['siklus']."
                        AND
                            type = 'a'
                        AND
                            tahun = ".$input['tahun']."
                        AND
                            ( (skoring > 2 AND skoring <= 3) ) 
                        ".$periode."
                        ".$datasiklus."
                    ) AS BAIK,
                    ( 
                        SELECT count(*) FROM `tb_skoring_urinalisa`
                        INNER JOIN tb_registrasi ON tb_skoring_urinalisa.id_registrasi = tb_registrasi.id
                        WHERE 
                            tb_skoring_urinalisa.siklus = ".$input['siklus']."
                        AND
                            type = 'a'
                        AND
                            tahun = ".$input['tahun']."
                        AND
                            ( (skoring > 1 AND skoring <= 2) ) 
                        ".$periode."
                        ".$datasiklus."
                    ) AS KURANG,
                    ( 
                        SELECT count(*) FROM `tb_skoring_urinalisa`
                        INNER JOIN tb_registrasi ON tb_skoring_urinalisa.id_registrasi = tb_registrasi.id
                        WHERE 
                            tb_skoring_urinalisa.siklus = ".$input['siklus']."
                        AND
                            type = 'a'
                        AND
                            tahun = ".$input['tahun']."
                        AND
                            ( (skoring <= 1) ) 
                        ".$periode."
                        ".$datasiklus."
                    ) AS BURUK";

        $data2 = "SELECT 
                    ( 
                        SELECT count(*) FROM `tb_skoring_urinalisa`
                        INNER JOIN tb_registrasi ON tb_skoring_urinalisa.id_registrasi = tb_registrasi.id
                        WHERE 
                            tb_skoring_urinalisa.siklus = ".$input['siklus']."
                        AND
                            type = 'b'
                        AND
                            tahun = ".$input['tahun']."
                        AND
                            ( (skoring > 3) ) 
                        ".$periode."
                        ".$datasiklus."
                    ) AS SANGAT_BAIK,
                    ( 
                        SELECT count(*) FROM `tb_skoring_urinalisa`
                        INNER JOIN tb_registrasi ON tb_skoring_urinalisa.id_registrasi = tb_registrasi.id
                        WHERE
                            tb_skoring_urinalisa.siklus = ".$input['siklus']."
                        AND
                            type = 'b'
                        AND
                            tahun = ".$input['tahun']."
                        AND
                            ( (skoring > 2 AND skoring <= 3) ) 
                        ".$periode."
                        ".$datasiklus."
                    ) AS BAIK,
                    ( 
                        SELECT count(*) FROM `tb_skoring_urinalisa`
                        INNER JOIN tb_registrasi ON tb_skoring_urinalisa.id_registrasi = tb_registrasi.id
                        WHERE 
                            tb_skoring_urinalisa.siklus = ".$input['siklus']."
                        AND
                            type = 'b'
                        AND
                            tahun = ".$input['tahun']."
                        AND
                            ( (skoring > 1 AND skoring <= 2) ) 
                        ".$periode."
                        ".$datasiklus."
                    ) AS KURANG,
                    ( 
                        SELECT count(*) FROM `tb_skoring_urinalisa`
                        INNER JOIN tb_registrasi ON tb_skoring_urinalisa.id_registrasi = tb_registrasi.id
                        WHERE 
                            tb_skoring_urinalisa.siklus = ".$input['siklus']."
                        AND
                            type = 'b'
                        AND
                            tahun = ".$input['tahun']."
                        AND
                            ( (skoring <= 1) ) 
                        ".$periode."
                        ".$datasiklus."
                    ) AS BURUK";

        $dataa = DB::table(DB::raw("($data1) t"))->select('*')->first();
        $datab = DB::table(DB::raw("($data2) t"))->select('*')->first();
        // dd($data);
        // return view('evaluasi.urinalisa.rekapitulasihasil.view', compact('jumlahpesertaa', 'datapesertaa', 'jumlahalata', 'dataalata', 'datametodea', 'jumlahmetodea'));
        $pdf = PDF::loadview('evaluasi.urinalisa.rekapitulasihasil.view', compact('dataa', 'datab', 'input', 'data', 'kirima', 'kirimb', 'beluminput1', 'beluminput2'))
            ->setPaper('a4', 'lanscape')
            ->setwarnings(false);
        return $pdf->stream('Laporan Pelaksanaan PME '.$input['form'].'.pdf');
    }

    public function urinalisarekapitulasipengiriman(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $periode = "";
        if ($input['siklus'] == 1) {
            if ($input['periode'] == 2 && $input['tahun'] == 2019) {
                $periode = "AND tb_registrasi.periode = 2";
                $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1))";
            }else{
                $periode = "AND tb_registrasi.periode IS NULL";
                $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1))";
            }
        }else{
            $datasiklus = "AND((tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 2))";
        }
        $data = DB::table('tb_registrasi')
                ->where('bidang', '=', '3')
                ->where('status', '=', '3')
                ->where(DB::raw('YEAR(created_at)'), '=' , $input['tahun'])
                ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                })
                ->where(function($query) use ($input){
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $input['siklus']);
                })
                ->count();
        $kirima = DB::table('hp_headers')
                ->join('tb_registrasi', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', '=', 'a')
                ->where('tb_registrasi.bidang', '=', 3)
                ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                })
                ->where(function($query) use ($input){
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $input['siklus']);
                })
                ->count();
        $kirimb = DB::table('hp_headers')
                ->join('tb_registrasi', 'tb_registrasi.id', '=', 'hp_headers.id_registrasi')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', '=', 'b')
                ->where('tb_registrasi.bidang', '=', 3)
                ->where(function($query) use ($input){
                    if ($input['siklus'] == 1) {
                        if ($input['periode'] == 2) {
                            $query->where('tb_registrasi.periode','=','2');
                        }else{
                            $query->WhereNull('tb_registrasi.periode');
                        }
                    }
                })
                ->where(function($query) use ($input){
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $input['siklus']);
                })
                ->count();
        if($input['siklus'] == '1'){
            $beluminput1 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang','=','3')
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.pemeriksaan','=', NULL)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input){
                                if ($input['siklus'] == 1) {
                                    if ($input['periode'] == 2) {
                                        $query->where('tb_registrasi.periode','=','2');
                                    }else{
                                        $query->WhereNull('tb_registrasi.periode');
                                    }
                                }
                            })
                            ->where(function($query) use ($input){
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $beluminput2 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang','=','3')
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.pemeriksaan2','=', NULL)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input){
                                if ($input['siklus'] == 1) {
                                    if ($input['periode'] == 2) {
                                        $query->where('tb_registrasi.periode','=','2');
                                    }else{
                                        $query->WhereNull('tb_registrasi.periode');
                                    }
                                }
                            })
                            ->where(function($query) use ($input){
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
        }else{
            $beluminput1 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang','=','3')
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.rpr1','=', NULL)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input){
                                if ($input['siklus'] == 1) {
                                    if ($input['periode'] == 2) {
                                        $query->where('tb_registrasi.periode','=','2');
                                    }else{
                                        $query->WhereNull('tb_registrasi.periode');
                                    }
                                }
                            })
                            ->where(function($query) use ($input){
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $beluminput2 = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang','=','3')
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.rpr2','=', NULL)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input){
                                if ($input['siklus'] == 1) {
                                    if ($input['periode'] == 2) {
                                        $query->where('tb_registrasi.periode','=','2');
                                    }else{
                                        $query->WhereNull('tb_registrasi.periode');
                                    }
                                }
                            })
                            ->where(function($query) use ($input){
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
        }

        $pdf = PDF::loadview('evaluasi.urinalisa.rekapitulasihasil.view_kirim', compact('input', 'data', 'kirima', 'kirimb', 'beluminput1', 'beluminput2'))
            ->setPaper('a4', 'lanscape')
            ->setwarnings(false);
        return $pdf->stream('Laporan Pelaksanaan PME '.$input['form'].'.pdf');
    }
}

<?php

namespace App\Http\Controllers;
use DB;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Excel;
use stdClass;
use PDF;
class LaporanRegistrasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function peserta(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('tb_registrasi')
                ->join('users', 'tb_registrasi.created_by', '=', 'users.id')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->join('provinces','perusahaan.provinsi','=','provinces.id')
                ->join('regencies','perusahaan.kota','=','regencies.id')
                ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->join('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.status','>=','2')
                ->select('tb_registrasi.*','perusahaan.nama_lab','perusahaan.alamat','perusahaan.telp','perusahaan.penanggung_jawab','perusahaan.personal','perusahaan.email as pemail','perusahaan.no_hp','provinces.name as Provinsi', 'regencies.name as Kota','sub_bidang.alias', 'perusahaan.pemerintah', 'users.email')
                ->get();
                // dd($data);
        return view('laporan.peserta.proses', compact('data', 'input'));
    }

    public function pesertaexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('tb_registrasi')
                ->join('users', 'tb_registrasi.created_by', '=', 'users.id')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->join('provinces','perusahaan.provinsi','=','provinces.id')
                ->join('regencies','perusahaan.kota','=','regencies.id')
                ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->join('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where(function($query) use ($input)
                {
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $input['siklus']);
                })
                ->where('tb_registrasi.status','>=','2')
                ->select('tb_registrasi.*','perusahaan.nama_lab','perusahaan.alamat','perusahaan.telp','perusahaan.penanggung_jawab','perusahaan.personal','perusahaan.email as pemail','perusahaan.no_hp','provinces.name as Provinsi', 'regencies.name as Kota','sub_bidang.alias', 'perusahaan.pemerintah', 'users.email')
                ->get();
                // dd($data);
        // return view('laporan.peserta.proses', compact('data', 'input'));

        Excel::create('Peserta PME', function($excel) use ($data, $input) {
            $excel->sheet('PME', function($sheet) use ($data, $input) {
                $sheet->loadView('laporan.peserta.view', array('data'=>$data, 'input'=>$input) );
            });
        })->download('xls');
    }

    public function pesertaparameter()
    {
        $data = DB::table('sub_bidang')->get();
        return view('laporan.pesertaparameter.registrasi', compact('data'));
    }
    public function pesertaparameterp(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameter = DB::table('sub_bidang')->where('id', $input['parameter'])->first();
        $data = DB::table('tb_registrasi')
                ->join('users', 'users.id','=','tb_registrasi.created_by')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->join('provinces','perusahaan.provinsi','=','provinces.id')
                ->join('regencies','perusahaan.kota','=','regencies.id')
                ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->join('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.bidang','=', $input['parameter'])
                ->where('tb_registrasi.status','>=','2')
                ->select('tb_registrasi.*','perusahaan.nama_lab','perusahaan.alamat','perusahaan.telp','perusahaan.penanggung_jawab','perusahaan.personal','users.email','perusahaan.no_hp','provinces.name as Provinsi', 'regencies.name as Kota','sub_bidang.alias', 'perusahaan.pemerintah')
                ->get();
                // dd($data);
        return view('laporan.pesertaparameter.proses', compact('data', 'input','parameter'));
    }

    public function pesertaparameterexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameter = DB::table('sub_bidang')->get();
        // dd($parameter);
        foreach ($parameter as $key => $val) {
            $data = DB::table('tb_registrasi')
                    ->join('users', 'users.id','=','tb_registrasi.created_by')
                    ->join('perusahaan', 'tb_registrasi.perusahaan_id','=','perusahaan.id')
                    ->join('provinces','perusahaan.provinsi','=','provinces.id')
                    ->join('regencies','perusahaan.kota','=','regencies.id')
                    ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                    ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                    ->join('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                    ->where('tb_registrasi.bidang','=', $val->id)
                    ->where('tb_registrasi.status','>=','2')
                    ->select('tb_registrasi.*','perusahaan.nama_lab','perusahaan.alamat','perusahaan.telp','perusahaan.penanggung_jawab','perusahaan.personal','users.email','perusahaan.no_hp','provinces.name as Provinsi', 'regencies.name as Kota','sub_bidang.alias', 'perusahaan.pemerintah')
                    ->get();
            $val->data = $data;
        }
        // dd($parameter);
        // return view('laporan.peserta.proses', compact('data', 'input'));

        Excel::create('Peserta PME Tahun '.$input['tahun'], function($excel) use ($input, $parameter) {
            foreach ($parameter as $key => $val) {
                if($val->id == 6) {
                    $excel->sheet($val->alias.' Umum dan PMI', function($sheet) use ($input, $val) {
                        $sheet->loadView('laporan.pesertaparameter.view', array('data'=>$val->data, 'input'=>$input,'parameter'=>$val) );
                    });
                }else{
                    $excel->sheet($val->alias, function($sheet) use ($input, $val) {
                        $sheet->loadView('laporan.pesertaparameter.view', array('data'=>$val->data, 'input'=>$input,'parameter'=>$val) );
                    });
                }
            }
        })->download('xls');
    }

    public function index(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->join('users', 'perusahaan.created_by', '=', 'users.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->leftjoin('regencies', 'perusahaan.kota', '=', 'regencies.id')
                ->join('badan_usaha', 'perusahaan.pemerintah', '=', 'badan_usaha.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where(function($query) use ($input)
                {
                    if ($input['status'] == 1) {
                        $query->where('tb_registrasi.status','=','1');
                    }else{
                        $query->where('tb_registrasi.status','>=','2');
                    }
                })
                ->where(function($query) use ($input)
                {
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $input['siklus']);
                })
                ->select('tb_registrasi.perusahaan_id','tb_registrasi.siklus', 'perusahaan.alamat', 'tb_registrasi.kode_lebpes', 'tb_registrasi.no_urut', 'perusahaan.nama_lab as Nama', 'perusahaan.personal', 'perusahaan.no_hp', 'badan_usaha.badan_usaha as Jenisbadan', 'perusahaan.telp as Telp', 'provinces.name as Provinsi', 'regencies.name as Kota', 'users.email', 'perusahaan.email as pemail', 'perusahaan.pemantapan_mutu', 'perusahaan.dilaksanakan')
                ->groupBy('tb_registrasi.perusahaan_id')
                ->groupBy('perusahaan.alamat')
                ->groupBy('perusahaan.no_hp')
                ->groupBy('perusahaan.personal')
                ->groupBy('perusahaan.nama_lab')
                ->groupBy('badan_usaha.badan_usaha')
                ->groupBy('perusahaan.telp')
                ->groupBy('provinces.name')
                ->groupBy('users.email')
                ->orderBy('tb_registrasi.kode_lebpes', 'asc')
                ->get();
        // dd($data);
        foreach($data as $skey => $r)
        {
            $hematologi = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 1)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->h = $hematologi;
            $kimiaklinik = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 2)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->kk = $kimiaklinik;
            $urinalisa = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 3)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->uri = $urinalisa;
            $mbta = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 4)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->bta = $mbta;
            $mtc = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 5)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->tc = $mtc;
            $antihiv = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 6)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->ahiv = $antihiv;
            $syphilis = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 7)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->syph = $syphilis;
            $hbsag = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 8)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->hbsag = $hbsag;
            $antihcv = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 9)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->ahcv = $antihcv;
            $mal = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 10)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->mal = $mal;
            $kai = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 11)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->kai = $kai;
            $kat = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 12)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->kat = $kat;
            $bac = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 13)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->bac = $bac;
        }
        // dd($data);
        return view('laporan.registrasi.proses', compact('data', 'input'));
    }

    // public function indexpdf(\Illuminate\Http\Request $request)
    // {
    //     $input = $request->all();

    //     $data = DB::table('tb_registrasi')
    //             ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
    //             ->join('users', 'perusahaan.created_by', '=', 'users.id')
    //             ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
    //             ->join('badan_usaha', 'perusahaan.pemerintah', '=', 'badan_usaha.id')
    //             ->where('siklus', $input['siklus'])
    //             ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //             //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
    //             ->select('tb_registrasi.perusahaan_id', 'perusahaan.alamat', 'perusahaan.nama_lab as Nama', 'perusahaan.personal', 'perusahaan.no_hp', 'badan_usaha.badan_usaha as Jenisbadan', 'perusahaan.telp as Telp', 'provinces.name as Provinsi', 'users.email')
    //             ->groupBy('tb_registrasi.perusahaan_id')
    //             ->groupBy('perusahaan.alamat')
    //             ->groupBy('perusahaan.no_hp')
    //             ->groupBy('perusahaan.personal')
    //             ->groupBy('perusahaan.nama_lab')
    //             ->groupBy('badan_usaha.badan_usaha')
    //             ->groupBy('perusahaan.telp')
    //             ->groupBy('provinces.name')
    //             ->groupBy('users.email')
    //             ->get();
    //     foreach($data as $skey => $r)
    //     {
    //         $hematologi = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 1)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->h = $hematologi;
    //         $kimiaklinik = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 2)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->kk = $kimiaklinik;
    //         $urinalisa = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 3)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->uri = $urinalisa;
    //         $mbta = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 4)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->bta = $mbta;
    //         $mtc = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 5)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->tc = $mtc;
    //         $antihiv = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 6)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->ahiv = $antihiv;
    //         $syphilis = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 7)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->syph = $syphilis;
    //         $hbsag = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 8)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->hbsag = $hbsag;
    //         $antihcv = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 9)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->ahcv = $antihcv;
    //     }
    //     $pdf = PDF::loadview('laporan.registrasi.proses', compact('data', 'input'))
    //         ->setPaper('a4', 'potrait')
    //         ->setwarnings(false);

    //     return $pdf->stream('Registrasi.pdf');
    // }

    public function indexexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        if ($input['status'] == 1) {
            $status = 'Belum Bayar';
        }else{
            $status = 'Sudah Bayar';
        }
        $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->join('users', 'perusahaan.created_by', '=', 'users.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->leftjoin('regencies', 'perusahaan.kota', '=', 'regencies.id')
                ->leftjoin('badan_usaha', 'perusahaan.pemerintah', '=', 'badan_usaha.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where(function($query) use ($input)
                {
                    if ($input['status'] == 1) {
                        $query->where('tb_registrasi.status','=','1');
                    }else{
                        $query->where('tb_registrasi.status','>=','2');
                    }
                })
                ->where(function($query) use ($input)
                {
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $input['siklus']);
                })
                // ->where('tb_registrasi.status','>=','2')
                // ->where('tb_registrasi.id', '=', '1848')
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('tb_registrasi.perusahaan_id','tb_registrasi.siklus', 'tb_registrasi.kode_lebpes', 'tb_registrasi.no_urut', 'perusahaan.alamat', 'perusahaan.nama_lab as Nama', 'perusahaan.personal', 'perusahaan.no_hp', 'badan_usaha.badan_usaha as Jenisbadan', 'perusahaan.telp as Telp', 'provinces.name as Provinsi', 'regencies.name as Kota', 'users.email', 'perusahaan.email as pemail', 'perusahaan.pemantapan_mutu', 'perusahaan.dilaksanakan')
                ->groupBy('tb_registrasi.perusahaan_id')
                ->groupBy('perusahaan.alamat')
                ->groupBy('perusahaan.no_hp')
                ->groupBy('perusahaan.personal')
                ->groupBy('perusahaan.nama_lab')
                ->groupBy('badan_usaha.badan_usaha')
                ->groupBy('perusahaan.telp')
                ->groupBy('provinces.name')
                ->groupBy('users.email')
                ->orderBy('tb_registrasi.kode_lebpes', 'asc')
                ->get();
                // dd($data);
        foreach($data as $skey => $r)
        {
            $hematologi = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 1)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->h = $hematologi;
            $kimiaklinik = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 2)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->kk = $kimiaklinik;
            $urinalisa = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 3)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->uri = $urinalisa;
            $mbta = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 4)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->bta = $mbta;
            $mtc = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 5)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->tc = $mtc;
            $antihiv = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 6)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->ahiv = $antihiv;
            $syphilis = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 7)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->syph = $syphilis;
            $hbsag = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 8)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->hbsag = $hbsag;
            $antihcv = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 9)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->ahcv = $antihcv;
            $mal = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 10)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->mal = $mal;
            $kai = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 11)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->kai = $kai;
            $kat = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 12)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->kat = $kat;
            $bac = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 13)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->where(function($query) use ($input)
                            {
                                $query->where('tb_registrasi.siklus', '12')
                                    ->orwhere('tb_registrasi.siklus', $input['siklus']);
                            })
                            ->count();
            $r->bac = $bac;
        }
        Excel::create('Laporan Registrasi Tahun '.$input['tahun'].' Siklus '.$input['siklus'].' '.$status, function($excel) use ($data, $input) {
            $excel->sheet('Registrasi', function($sheet) use ($data, $input) {
                $sheet->loadView('laporan.registrasi.view', array('data'=>$data, 'input'=>$input) );
                $sheet->mergeCells('P1:R1');
                $sheet->mergeCells('S1:U1');
                $sheet->mergeCells('V1:Y1');
                $sheet->mergeCells('Z1:AA1');
                $sheet->setMergeColumn(array(
                    'columns' => array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','AB'),
                    'rows' => array(
                        array(1,2),
                        array(1,2),
                        array(1,2),
                        array(1,2),
                        array(1,2),
                        array(1,2),
                        array(1,2),
                        array(1,2),
                        array(1,2),
                        array(1,2),
                        array(1,2),
                        array(1,2),
                        array(1,2),
                        array(1,2),
                        array(1,2),
                    )
                ));
                $sheet->cell('A1', function($cell) {
                    $cell->setValue('No'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('B1', function($cell) {
                    $cell->setValue('No VA'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('C1', function($cell) {
                    $cell->setValue('Kode Instansi'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('D1', function($cell) {
                    $cell->setValue('Nama Instansi'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('E1', function($cell) {
                    $cell->setValue('Alamat'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('F1', function($cell) {
                    $cell->setValue('Alamat'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('G1', function($cell) {
                    $cell->setValue('Provinsi'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('H1', function($cell) {
                    $cell->setValue('Kabupaten / Kota'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('I1', function($cell) {
                    $cell->setValue('No. Telpon'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('J1', function($cell) {
                    $cell->setValue('Personal'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('K1', function($cell) {
                    $cell->setValue('No. Hp'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('L1', function($cell) {
                    $cell->setValue('Pemantapan Mutu Internal'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('M1', function($cell) {
                    $cell->setValue('Email Daftar PNPME'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('N1', function($cell) {
                    $cell->setValue('Email Login'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('O1', function($cell) {
                    $cell->setValue('Siklus'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('P2', function($cell) {
                    $cell->setValue('HEMA'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('Q2', function($cell) {
                    $cell->setValue('KK'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('R2', function($cell) {
                    $cell->setValue('Urinalisa'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('S2', function($cell) {
                    $cell->setValue('BTA'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('T2', function($cell) {
                    $cell->setValue('TC'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('U2', function($cell) {
                    $cell->setValue('Malaria'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('V2', function($cell) {
                    $cell->setValue('A HIV'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('W2', function($cell) {
                    $cell->setValue('Syph'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('X2', function($cell) {
                    $cell->setValue('HBSAG'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('Y2', function($cell) {
                    $cell->setValue('A HCV'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('Z2', function($cell) {
                    $cell->setValue('Kimai Air'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('AA2', function($cell) {
                    $cell->setValue('Kimia Air Terbatas'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
            });

        })->download('xls');
    }


    public function uangmasuk(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        if ($input['pembayaran'] == '1') {
            $pembayaran = NULL;
        }else {
            $pembayaran = '-';
        }
        $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->join('users', 'perusahaan.created_by', '=', 'users.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->join('badan_usaha', 'perusahaan.pemerintah', '=', 'badan_usaha.id')
                ->where('tb_registrasi.sptjm', $pembayaran)
                ->where('tb_registrasi.status', '>=', '2')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('tb_registrasi.perusahaan_id', 'perusahaan.alamat', 'perusahaan.nama_lab as Nama', 'perusahaan.personal', 'perusahaan.no_hp', 'badan_usaha.badan_usaha as Jenisbadan', 'perusahaan.telp as Telp', 'provinces.name as Provinsi', 'users.email')
                ->groupBy('tb_registrasi.perusahaan_id')
                // ->groupBy('tb_registrasi.created_at')
                ->groupBy('perusahaan.nama_lab')
                ->get();

        foreach($data as $skey => $r)
        {
            $hematologi = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 1)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->h = $hematologi;
            $kimiaklinik = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 2)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->kk = $kimiaklinik;
            $urinalisa = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 3)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->uri = $urinalisa;
            $mbta = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 4)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->bta = $mbta;
            $mtc = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 5)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->tc = $mtc;
            $antihiv = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 6)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->ahiv = $antihiv;
            $syphilis = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 7)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->syph = $syphilis;
            $hbsag = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 8)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->hbsag = $hbsag;
            $antihcv = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 9)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->ahcv = $antihcv;
            $malaria = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 10)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->mal = $malaria;
            $kimiaair = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 11)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->kai = $kimiaair;
            $kimiaterbatas = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 12)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->kat = $kimiaterbatas;
            $bakteri = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 13)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->bac = $bakteri;
            $total = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->sum('sub_bidang.tarif');
            $r->tot = $total;
        }
        // dd($data);
        return view('laporan.uangmasuk.proses', compact('data', 'input'));
    }

    public function uangmasukexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        if ($input['pembayaran'] == '1') {
            $pembayaran = NULL;
        }else {
            $pembayaran = '-';
        }
        $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->join('users', 'perusahaan.created_by', '=', 'users.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->join('badan_usaha', 'perusahaan.pemerintah', '=', 'badan_usaha.id')
                ->where('tb_registrasi.sptjm', $pembayaran)
                ->where('tb_registrasi.status', '>=', '2')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('tb_registrasi.perusahaan_id', 'perusahaan.alamat', 'perusahaan.nama_lab as Nama', 'perusahaan.personal', 'perusahaan.no_hp', 'badan_usaha.badan_usaha as Jenisbadan', 'perusahaan.telp as Telp', 'provinces.name as Provinsi', 'users.email')
                ->groupBy('tb_registrasi.perusahaan_id')
                // ->groupBy('tb_registrasi.created_at')
                ->groupBy('perusahaan.nama_lab')
                ->get();

        foreach($data as $skey => $r)
        {
            $hematologi = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 1)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->h = $hematologi;
            $kimiaklinik = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 2)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->kk = $kimiaklinik;
            $urinalisa = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 3)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->uri = $urinalisa;
            $mbta = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 4)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->bta = $mbta;
            $mtc = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 5)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->tc = $mtc;
            $antihiv = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 6)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->ahiv = $antihiv;
            $syphilis = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 7)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->syph = $syphilis;
            $hbsag = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 8)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->hbsag = $hbsag;
            $antihcv = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 9)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->ahcv = $antihcv;
            $malaria = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 10)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->mal = $malaria;
            $kimiaair = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 11)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->kai = $kimiaair;
            $kimiaterbatas = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 12)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->kat = $kimiaterbatas;
            $bakteri = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 13)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->bac = $bakteri;
            $total = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->sum('sub_bidang.tarif');
            $r->tot = $total;
        }
        // dd($data);
        Excel::create('Penerimaan Uang Masuk', function($excel) use ($data, $input) {
            $excel->sheet('Uang Masuk', function($sheet) use ($data, $input) {
                $sheet->loadView('laporan.uangmasuk.view', array('data'=>$data, 'input'=>$input) );
                $sheet->mergeCells('H1:J1');
                $sheet->mergeCells('K1:N1');
                $sheet->mergeCells('O1:R1');
                $sheet->mergeCells('S1:T1');
                $sheet->mergeCells('U1:U2');
                $sheet->cell('A1', function($cell) {
                    $cell->setValue('No'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('B1', function($cell) {
                    $cell->setValue('Tanggal'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('C1', function($cell) {
                    $cell->setValue('Nama Instansi'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('H2', function($cell) {
                    $cell->setValue('HEM (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('I2', function($cell) {
                    $cell->setValue('KKL (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('J2', function($cell) {
                    $cell->setValue('URI (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('K2', function($cell) {
                    $cell->setValue('BTA (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('L2', function($cell) {
                    $cell->setValue('TCC (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('M2', function($cell) {
                    $cell->setValue('MAL (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('N2', function($cell) {
                    $cell->setValue('BAC (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('O2', function($cell) {
                    $cell->setValue('A HIV (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('P2', function($cell) {
                    $cell->setValue('Syph (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('Q2', function($cell) {
                    $cell->setValue('HBsAg (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('R2', function($cell) {
                    $cell->setValue('A HCV (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('S2', function($cell) {
                    $cell->setValue('Kimia Air (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('T2', function($cell) {
                    $cell->setValue('Kimia Air Terbatas (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
            });

        })->download('xls');
    }

    public function uangmasukpdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->where('tb_registrasi.siklus', $input['siklus'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('tb_registrasi.perusahaan_id', 'perusahaan.nama_lab as Nama', 'tb_registrasi.created_at')
                ->groupBy('tb_registrasi.perusahaan_id')
                // ->groupBy('tb_registrasi.created_at')
                ->groupBy('perusahaan.nama_lab')
                ->get();

        foreach($data as $skey => $r)
        {
            $hematologi = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 1)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->h = $hematologi;
            $kimiaklinik = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 2)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->kk = $kimiaklinik;
            $urinalisa = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 3)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->uri = $urinalisa;
            $mbta = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 4)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->bta = $mbta;
            $mtc = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 5)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->tc = $mtc;
            $antihiv = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 6)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->ahiv = $antihiv;
            $syphilis = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 7)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->syph = $syphilis;
            $hbsag = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 8)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->hbsag = $hbsag;
            $antihcv = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 9)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->ahcv = $antihcv;
            $paket = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 10)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->paket = $paket;
        }
        $pdf = PDF::loadview('laporan.uangmasuk.proses', compact('data', 'input'))
            ->setPaper('a4', 'landscape')
            ->setwarnings(false);

        return $pdf->stream('Uang-masuk.pdf');
    }


    public function sampel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $data = DB::table('tb_kirim')
                ->join('tb_registrasi', 'tb_kirim.id_registrasi', '=', 'tb_registrasi.id')
                ->leftjoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftjoin('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->leftjoin('regencies', 'perusahaan.kota', '=', 'regencies.id')
                ->where(DB::raw('YEAR(tb_kirim.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.siklus', $input['siklus'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('tb_registrasi.*', 'perusahaan.nama_lab as Nama', 'perusahaan.pemerintah as Jenisbadan', 'perusahaan.telp as Telp', 'provinces.name as Provinsi', 'sub_bidang.parameter as Bidang', 'sub_bidang.tarif as Tarif', 'sub_bidang.siklus as Siklus', 'regencies.name as Kota', 'perusahaan.alamat as Alamat')
                ->get();
        return view('laporan.sampel.proses', compact('data', 'input'));
    }

    public function sampelexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $data = DB::table('tb_kirim')
                ->join('tb_registrasi', 'tb_kirim.id_registrasi', '=', 'tb_registrasi.id')
                ->leftjoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftjoin('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->leftjoin('regencies', 'perusahaan.kota', '=', 'regencies.id')
                ->where(DB::raw('YEAR(tb_kirim.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.siklus', $input['siklus'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('tb_registrasi.*', 'perusahaan.nama_lab as Nama', 'perusahaan.pemerintah as Jenisbadan', 'perusahaan.telp as Telp', 'provinces.name as Provinsi', 'sub_bidang.parameter as Bidang', 'sub_bidang.tarif as Tarif', 'sub_bidang.siklus as Siklus', 'regencies.name as Kota', 'perusahaan.alamat as Alamat')
                ->get();
        Excel::create('Penerimaan Sampel', function($excel) use ($data, $input) {
            $excel->sheet('Sampel', function($sheet) use ($data, $input) {
                $sheet->loadView('laporan.sampel.view', array('data'=>$data, 'input'=>$input) );
            });

        })->download('xls');
    }

    public function sampelpdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $data = DB::table('tb_kirim')
                ->join('tb_registrasi', 'tb_kirim.id_registrasi', '=', 'tb_registrasi.id')
                ->leftjoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftjoin('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->leftjoin('regencies', 'perusahaan.kota', '=', 'regencies.id')
                ->where(DB::raw('YEAR(tb_kirim.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.siklus', $input['siklus'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('tb_registrasi.*', 'perusahaan.nama_lab as Nama', 'perusahaan.pemerintah as Jenisbadan', 'perusahaan.telp as Telp', 'provinces.name as Provinsi', 'sub_bidang.parameter as Bidang', 'sub_bidang.tarif as Tarif', 'sub_bidang.siklus as Siklus', 'regencies.name as Kota', 'perusahaan.alamat as Alamat')
                ->get();

        $pdf = PDF::loadview('laporan.sampel.proses', compact('data', 'input'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Penerimaan Sampel.pdf');
    }

    public function evaluasi(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $data = DB::table('evaluasi')
                ->join('tb_registrasi', 'evaluasi.id_register', '=', 'tb_registrasi.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->join('users', 'tb_registrasi.created_by', '=', 'users.id')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->where(DB::raw('YEAR(evaluasi.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.siklus', $input['siklus'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('evaluasi.*', 'perusahaan.nama_lab', 'tb_registrasi.kode_lebpes', 'sub_bidang.parameter as Bidang')
                ->get();
        return view('laporan.evaluasi.proses', compact('data', 'input'));
    }

    public function evaluasiexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $data = DB::table('evaluasi')
                ->join('tb_registrasi', 'evaluasi.id_register', '=', 'tb_registrasi.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->join('users', 'tb_registrasi.created_by', '=', 'users.id')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->where(DB::raw('YEAR(evaluasi.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.siklus', $input['siklus'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('evaluasi.*', 'perusahaan.nama_lab', 'tb_registrasi.kode_lebpes', 'sub_bidang.parameter as Bidang')
                ->get();

        Excel::create('Evaluasi', function($excel) use ($data, $input) {
            $excel->sheet('Evaluasi', function($sheet) use ($data, $input) {
                $sheet->loadView('laporan.evaluasi.view', array('data'=>$data, 'input'=>$input) );
            });

        })->download('xls');
    }

    public function evaluasipdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $data = DB::table('evaluasi')
                ->join('tb_registrasi', 'evaluasi.id_register', '=', 'tb_registrasi.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->join('users', 'tb_registrasi.created_by', '=', 'users.id')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->where(DB::raw('YEAR(evaluasi.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.siklus', $input['siklus'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('evaluasi.*', 'perusahaan.nama_lab', 'tb_registrasi.kode_lebpes', 'sub_bidang.parameter as Bidang')
                ->get();
        $pdf = PDF::loadview('laporan.evaluasi.proses', compact('data', 'input'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Evaluasi.pdf');
    }


    public function rekappeserta($id)
    {
        $data = DB::table('sub_bidang')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->select('sub_bidang.*', 'tb_bidang.bidang')
                ->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->jumlah = $jumlah;
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function rekappesertaexcel(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $data = DB::table('sub_bidang')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->select('sub_bidang.*', 'tb_bidang.bidang')
                ->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->jumlah = $jumlah;
        }
        // dd($data);
        if ($request->export == "Export Excel") {
            Excel::create('Rekap Peserta Berdasarkan Parameter', function($excel) use ($data, $tahun) {
                $excel->sheet('Rekap', function($sheet) use ($data, $tahun) {
                    $sheet->loadView('laporan.rekap.excel.peserta_parameter', array('data'=>$data, 'tahun'=>$tahun) );
                });
            })->download('xls');
        }else{
            $pdf = PDF::loadview('laporan.rekap.excel.peserta_parameter', compact('data', 'tahun'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            return $pdf->stream('Rekap Peserta Berdasarkan Parameter.pdf');
        }
    }

    public function monitoring1(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $data = DB::table('sub_bidang')
                ->join('tb_bidang', 'tb_bidang.id', '=', 'sub_bidang.id_bidang')
                ->select('sub_bidang.id','sub_bidang.alias', 'tb_bidang.bidang')
                // ->wherein('sub_bidang.id', [1,2,3])
                ->get();
        // dd($data);
        foreach($data as $skey => $r)
        {
            if ($r->id <= 3) {
                $jumlah = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function($query) use ($siklus){
                                    if ($siklus == 1) {
                                        $query->whereNull('tb_registrasi.periode');
                                    }
                                })
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->jumlah = $jumlah;
                if ($siklus == "1") {
                    $cankirim = DB::table('tb_registrasi')
                                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','2')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->whereNull('tb_registrasi.periode');
                                        }
                                    })
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->cankirim = $cankirim;
                    $beluminput1 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','>=','2')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->whereNull('tb_registrasi.periode');
                                        }
                                    })
                                    ->where('tb_registrasi.pemeriksaan','=', NULL)
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->beluminput1 = $beluminput1;
                    $beluminput2 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','>=','2')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->whereNull('tb_registrasi.periode');
                                        }
                                    })
                                    ->where('tb_registrasi.pemeriksaan2','=', NULL)
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->beluminput2 = $beluminput2;
                    $input1 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->whereNull('tb_registrasi.periode');
                                        }
                                    })
                                    ->where('tb_registrasi.status_data1','=', '1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->input1 = $input1;
                    $input2 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->whereNull('tb_registrasi.periode');
                                        }
                                    })
                                    ->where('tb_registrasi.status_data2','=', '1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->input2 = $input2;
                    $sudahinput1 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->whereNull('tb_registrasi.periode');
                                        }
                                    })
                                    ->where('tb_registrasi.status_data1','=', '2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->sudahinput1 = $sudahinput1;
                    $sudahinput2 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->whereNull('tb_registrasi.periode');
                                        }
                                    })
                                    ->where('tb_registrasi.status_data2','=', '2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->sudahinput2 = $sudahinput2;
                }else{
                    $cankirim = DB::table('tb_registrasi')
                                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->whereNull('tb_registrasi.periode');
                                        }
                                    })
                                    ->where('tb_registrasi.status','>=','2')
                                    ->whereNull('tb_registrasi.status_2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->cankirim = $cankirim;
                    $beluminput1 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','>=','2')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->whereNull('tb_registrasi.periode');
                                        }
                                    })
                                    ->where('tb_registrasi.rpr1','=', NULL)
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->beluminput1 = $beluminput1;
                    $beluminput2 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->whereNull('tb_registrasi.periode');
                                        }
                                    })
                                    ->where('tb_registrasi.status','>=','2')
                                    ->where('tb_registrasi.rpr2','=', NULL)
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->beluminput2 = $beluminput2;
                    $input1 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->whereNull('tb_registrasi.periode');
                                        }
                                    })
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_datarpr1','=', '1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->input1 = $input1;
                    $input2 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->whereNull('tb_registrasi.periode');
                                        }
                                    })
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_datarpr2','=', '1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->input2 = $input2;
                    $sudahinput1 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->whereNull('tb_registrasi.periode');
                                        }
                                    })
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_datarpr1','=', '2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->sudahinput1 = $sudahinput1;
                    $sudahinput2 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->whereNull('tb_registrasi.periode');
                                        }
                                    })
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_datarpr2','=', '2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->sudahinput2 = $sudahinput2;
                }
                if($r->id == '3'){
                    $sudahevaluasi1 = DB::table('tb_skoring_urinalisa')
                                ->join('tb_registrasi','tb_registrasi.id','=','tb_skoring_urinalisa.id_registrasi')
                                ->where(function($query) use ($siklus){
                                    if ($siklus == 1) {
                                        $query->whereNull('tb_registrasi.periode');
                                    }
                                })
                                ->where('type','=', 'a')
                                ->where('tb_skoring_urinalisa.siklus','=', $siklus)
                                ->where('tahun', '=' , $id)
                                ->select(DB::raw('DISTINCT id_registrasi'))->get();
                    $r->sudahevaluasi1 = count($sudahevaluasi1);

                    $sudahevaluasi2 = DB::table('tb_skoring_urinalisa')
                                ->join('tb_registrasi','tb_registrasi.id','=','tb_skoring_urinalisa.id_registrasi')
                                ->where(function($query) use ($siklus){
                                    if ($siklus == 1) {
                                        $query->whereNull('tb_registrasi.periode');
                                    }
                                })
                                ->where('type','=', 'a')
                                ->where('tb_skoring_urinalisa.siklus','=', $siklus)
                                ->where('tahun', '=' , $id)
                                ->select(DB::raw('DISTINCT id_registrasi'))->get();
                    $r->sudahevaluasi2 = count($sudahevaluasi2);
                }else{
                    $sudahevaluasi1 = DB::table('tb_zscore')
                                    ->join('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->whereNull('tb_registrasi.periode');
                                        }
                                    })
                                    ->where('tb_zscore.form','=',strtolower($r->bidang))
                                    ->where('tb_zscore.type','=', 'a')
                                    ->where('tb_zscore.siklus','=', $siklus)
                                    ->where('tb_zscore.tahun', '=' , $id)
                                    ->select(DB::raw('DISTINCT id_registrasi'))->get();
                    $r->sudahevaluasi1 = count($sudahevaluasi1);


                    $sudahevaluasi2 = DB::table('tb_zscore')
                                    ->join('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->whereNull('tb_registrasi.periode');
                                        }
                                    })
                                    ->where('tb_zscore.form','=',strtolower($r->bidang))
                                    ->where('tb_zscore.type','=', 'b')
                                    ->where('tb_zscore.siklus','=', $siklus)
                                    ->where('tb_zscore.tahun', '=' , $id)
                                    ->select(DB::raw('DISTINCT id_registrasi'))->get();
                    $r->sudahevaluasi2 = count($sudahevaluasi2);
                }

                $r->belumevaluasi1 = $r->sudahinput1 - $r->sudahevaluasi1;
                $r->belumevaluasi2 = $r->sudahinput2 - $r->sudahevaluasi2;

                $jumlah2 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function($query) use ($siklus){
                                    if ($siklus == 1) {
                                        $query->where('tb_registrasi.periode','=','2');
                                    }
                                })
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->jumlah2 = $jumlah2;
                if ($siklus == "1") {
                    $cankirim2 = DB::table('tb_registrasi')
                                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','2')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->where('tb_registrasi.periode','=','2');
                                        }
                                    })
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->cankirim2 = $cankirim2;
                    $beluminput12 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','>=','2')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->where('tb_registrasi.periode','=','2');
                                        }
                                    })
                                    ->where('tb_registrasi.pemeriksaan','=', NULL)
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->beluminput12 = $beluminput12;
                    $beluminput22 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','>=','2')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->where('tb_registrasi.periode','=','2');
                                        }
                                    })
                                    ->where('tb_registrasi.pemeriksaan2','=', NULL)
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->beluminput22 = $beluminput22;
                    $input12 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->where('tb_registrasi.periode','=','2');
                                        }
                                    })
                                    ->where('tb_registrasi.status_data1','=', '1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->input12 = $input12;
                    $input22 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->where('tb_registrasi.periode','=','2');
                                        }
                                    })
                                    ->where('tb_registrasi.status_data2','=', '1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->input22 = $input22;
                    $sudahinput12 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->where('tb_registrasi.periode','=','2');
                                        }
                                    })
                                    ->where('tb_registrasi.status_data1','=', '2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->sudahinput12 = $sudahinput12;
                    $sudahinput22 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->where('tb_registrasi.periode','=','2');
                                        }
                                    })
                                    ->where('tb_registrasi.status_data2','=', '2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->sudahinput22 = $sudahinput22;
                }else{
                    $cankirim2 = DB::table('tb_registrasi')
                                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','>=','2')
                                    ->whereNull('tb_registrasi.status_2')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->where('tb_registrasi.periode','=','2');
                                        }
                                    })
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->cankirim2 = $cankirim2;
                    $beluminput12 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','>=','2')
                                    ->where('tb_registrasi.rpr1','=', NULL)
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->where('tb_registrasi.periode','=','2');
                                        }
                                    })
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->beluminput12 = $beluminput12;
                    $beluminput22 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','>=','2')
                                    ->where('tb_registrasi.rpr2','=', NULL)
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->where('tb_registrasi.periode','=','2');
                                        }
                                    })
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->beluminput22 = $beluminput22;
                    $input12 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->where('tb_registrasi.periode','=','2');
                                        }
                                    })
                                    ->where('tb_registrasi.status_datarpr1','=', '1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->input12 = $input12;
                    $input22 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->where('tb_registrasi.periode','=','2');
                                        }
                                    })
                                    ->where('tb_registrasi.status_datarpr2','=', '1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->input22 = $input22;
                    $sudahinput12 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->where('tb_registrasi.periode','=','2');
                                        }
                                    })
                                    ->where('tb_registrasi.status_datarpr1','=', '2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->sudahinput12 = $sudahinput12;
                    $sudahinput22 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->where('tb_registrasi.periode','=','2');
                                        }
                                    })
                                    ->where('tb_registrasi.status_datarpr2','=', '2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->sudahinput22 = $sudahinput22;
                }
                if($r->id == '3'){
                    $sudahevaluasi12 = DB::table('tb_skoring_urinalisa')
                                ->join('tb_registrasi','tb_registrasi.id','=','tb_skoring_urinalisa.id_registrasi')
                                ->where(function($query) use ($siklus){
                                    if ($siklus == 1) {
                                        $query->where('tb_registrasi.periode','=','2');
                                    }
                                })
                                ->where('type','=', 'a')
                                ->where('tb_skoring_urinalisa.siklus','=', $siklus)
                                ->where('tahun', '=' , $id)
                                ->select(DB::raw('DISTINCT id_registrasi'))->get();
                    $r->sudahevaluasi12 = count($sudahevaluasi12);

                    $sudahevaluasi22 = DB::table('tb_skoring_urinalisa')
                                ->join('tb_registrasi','tb_registrasi.id','=','tb_skoring_urinalisa.id_registrasi')
                                ->where(function($query) use ($siklus){
                                    if ($siklus == 1) {
                                        $query->where('tb_registrasi.periode','=','2');
                                    }
                                })
                                ->where('type','=', 'a')
                                ->where('tb_skoring_urinalisa.siklus','=', $siklus)
                                ->where('tahun', '=' , $id)
                                ->select(DB::raw('DISTINCT id_registrasi'))->get();
                    $r->sudahevaluasi22 = count($sudahevaluasi22);
                }else{
                    $sudahevaluasi12 = DB::table('tb_zscore')
                                    ->join('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->where('tb_registrasi.periode','=','2');
                                        }
                                    })
                                    ->where('tb_zscore.form','=',strtolower($r->bidang))
                                    ->where('tb_zscore.type','=', 'a')
                                    ->where('tb_zscore.siklus','=', $siklus)
                                    ->where('tb_zscore.tahun', '=' , $id)
                                    ->select(DB::raw('DISTINCT id_registrasi'))->get();
                    $r->sudahevaluasi12 = count($sudahevaluasi12);


                    $sudahevaluasi22 = DB::table('tb_zscore')
                                    ->join('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                                    ->where(function($query) use ($siklus){
                                        if ($siklus == 1) {
                                            $query->where('tb_registrasi.periode','=','2');
                                        }
                                    })
                                    ->where('tb_zscore.form','=',strtolower($r->bidang))
                                    ->where('tb_zscore.type','=', 'b')
                                    ->where('tb_zscore.siklus','=', $siklus)
                                    ->where('tb_zscore.tahun', '=' , $id)
                                    ->select(DB::raw('DISTINCT id_registrasi'))->get();
                    $r->sudahevaluasi22 = count($sudahevaluasi22);
                }

                $r->belumevaluasi12 = $r->sudahinput1 - $r->sudahevaluasi1;
                $r->belumevaluasi22 = $r->sudahinput2 - $r->sudahevaluasi2;
            }elseif ($r->id == 7) {
                $jumlah = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->jumlah = $jumlah;
                if ($siklus == "1") {
                    $cankirim = DB::table('tb_registrasi')
                                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->cankirim = $cankirim;
                    $beluminputtp = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','>=','2')
                                    ->where('tb_registrasi.pemeriksaan','=', NULL)
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->beluminputtp = $beluminputtp;
                    $beluminputrpr = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','>=','2')
                                    ->where('tb_registrasi.rpr1','=', NULL)
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->beluminputrpr = $beluminputrpr;
                    $inputtp = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_data1','=', '1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->inputtp = $inputtp;
                    $inputrpr = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_datarpr1','=', '1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->inputrpr = $inputrpr;
                    $sudahinputtp = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_data1','=', '2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->sudahinputtp = $sudahinputtp;
                    $sudahinputrpr = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_datarpr1','=', '2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->sudahinputrpr = $sudahinputrpr;
                }else{
                    $cankirim = DB::table('tb_registrasi')
                                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','>=','2')
                                    ->whereNull('tb_registrasi.status_2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->cankirim = $cankirim;
                    $beluminputtp = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_data2','=', NULL)
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->beluminputtp = $beluminputtp;
                    $beluminputrpr = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.rpr2','=', NULL)
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->beluminputrpr = $beluminputrpr;
                    $inputtp = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_data2','=', '1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->inputtp = $inputtp;
                    $inputrpr = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_datarpr2','=', '1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->inputrpr = $inputrpr;
                    $sudahinputtp = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_data2','=', '2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->sudahinputtp = $sudahinputtp;
                    $sudahinputrpr = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_datarpr2','=', '2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->sudahinputrpr = $sudahinputrpr;
                }
            }else{
                // if (Auth::user()->penyelenggara == 6) {
                //     $jumlah = DB::table('tb_registrasi')
                //                 ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                //                 // ->where(function($query)
                //                 //     {
                //                 //         if(Auth::user()->badan_usaha == '9'){
                //                 //             $query->where('perusahaan.pemerintah', '=', '9');
                //                 //         }else{
                //                 //             $query->where('perusahaan.pemerintah', '!=', '9');
                //                 //         }
                //                 //     })
                //                 ->where('tb_registrasi.bidang','=',$r->id)
                //                 ->where('tb_registrasi.status','>=','2')
                //                 ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                //                 ->count();
                //     $r->jumlah = $jumlah;
                //     if ($siklus == "1") {
                //         $cankirim = DB::table('tb_registrasi')
                //                         ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                //                         // ->where(function($query)
                //                         //     {
                //                         //         if(Auth::user()->badan_usaha == '9'){
                //                         //             $query->where('perusahaan.pemerintah', '=', '9');
                //                         //         }else{
                //                         //             $query->where('perusahaan.pemerintah', '!=', '9');
                //                         //         }
                //                         //     })
                //                         ->where('tb_registrasi.bidang','=',$r->id)
                //                         ->where('tb_registrasi.status','=','2')
                //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                //                         ->count();
                //         $r->cankirim = $cankirim;

                //         $beluminput1 = DB::table('tb_registrasi')
                //                         ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                //                         // ->where(function($query)
                //                         //     {
                //                         //         if(Auth::user()->badan_usaha == '9'){
                //                         //             $query->where('perusahaan.pemerintah', '=', '9');
                //                         //         }else{
                //                         //             $query->where('perusahaan.pemerintah', '!=', '9');
                //                         //         }
                //                         //     })
                //                         ->where('tb_registrasi.bidang','=',$r->id)
                //                         ->where('tb_registrasi.status','=','3')
                //                         ->whereNull('tb_registrasi.siklus_1')
                //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                //                         ->count();
                //         $r->beluminput1 = $beluminput1;
                //         $input1 = DB::table('tb_registrasi')
                //                         ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                //                         // ->where(function($query)
                //                         //     {
                //                         //         if(Auth::user()->badan_usaha == '9'){
                //                         //             $query->where('perusahaan.pemerintah', '=', '9');
                //                         //         }else{
                //                         //             $query->where('perusahaan.pemerintah', '!=', '9');
                //                         //         }
                //                         //     })
                //                         ->where('tb_registrasi.bidang','=',$r->id)
                //                         ->where('tb_registrasi.status','=','3')
                //                         ->where('tb_registrasi.status_data1','=', '1')
                //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                //                         ->count();
                //         $r->input1 = $input1;
                //         $sudahinput1 = DB::table('tb_registrasi')
                //                         ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                //                         // ->where(function($query)
                //                         //     {
                //                         //         if(Auth::user()->badan_usaha == '9'){
                //                         //             $query->where('perusahaan.pemerintah', '=', '9');
                //                         //         }else{
                //                         //             $query->where('perusahaan.pemerintah', '!=', '9');
                //                         //         }
                //                         //     })
                //                         ->where('tb_registrasi.bidang','=',$r->id)
                //                         ->where('tb_registrasi.status','=','3')
                //                         ->where('tb_registrasi.status_data1','=', '2')
                //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                //                         ->count();
                //         $r->sudahinput1 = $sudahinput1;
                //     }else{
                //         $cankirim = DB::table('tb_registrasi')
                //                         ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                //                         // ->where(function($query)
                //                         //     {
                //                         //         if(Auth::user()->badan_usaha == '9'){
                //                         //             $query->where('perusahaan.pemerintah', '=', '9');
                //                         //         }else{
                //                         //             $query->where('perusahaan.pemerintah', '!=', '9');
                //                         //         }
                //                         //     })
                //                         ->where('tb_registrasi.bidang','=',$r->id)
                //                         ->where('tb_registrasi.status','>=','2')
                //                         ->whereNull('tb_registrasi.status_2')
                //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                //                         ->count();
                //         $r->cankirim = $cankirim;
                //         $beluminput1 = DB::table('tb_registrasi')
                //                         ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                //                         // ->where(function($query)
                //                         //     {
                //                         //         if(Auth::user()->badan_usaha == '9'){
                //                         //             $query->where('perusahaan.pemerintah', '=', '9');
                //                         //         }else{
                //                         //             $query->where('perusahaan.pemerintah', '!=', '9');
                //                         //         }
                //                         //     })
                //                         ->where('tb_registrasi.bidang','=',$r->id)
                //                         ->where('tb_registrasi.status','=','3')
                //                         ->whereNull('tb_registrasi.siklus_2')
                //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                //                         ->count();
                //         $r->beluminput1 = $beluminput1;
                //         $input1 = DB::table('tb_registrasi')
                //                         ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                //                         // ->where(function($query)
                //                         //     {
                //                         //         if(Auth::user()->badan_usaha == '9'){
                //                         //             $query->where('perusahaan.pemerintah', '=', '9');
                //                         //         }else{
                //                         //             $query->where('perusahaan.pemerintah', '!=', '9');
                //                         //         }
                //                         //     })
                //                         ->where('tb_registrasi.bidang','=',$r->id)
                //                         ->where('tb_registrasi.status','=','3')
                //                         ->where('tb_registrasi.status_data2','=', '1')
                //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                //                         ->count();
                //         $r->input1 = $input1;
                //         $sudahinput1 = DB::table('tb_registrasi')
                //                         ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                //                         // ->where(function($query)
                //                         //     {
                //                         //         if(Auth::user()->badan_usaha == '9'){
                //                         //             $query->where('perusahaan.pemerintah', '=', '9');
                //                         //         }else{
                //                         //             $query->where('perusahaan.pemerintah', '!=', '9');
                //                         //         }
                //                         //     })
                //                         ->where('tb_registrasi.bidang','=',$r->id)
                //                         ->where('tb_registrasi.status','=','3')
                //                         ->where('tb_registrasi.status_data2','=', '2')
                //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                //                         ->count();
                //         $r->sudahinput1 = $sudahinput1;
                //     }
                // }else{
                    $jumlah = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','>=','2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->jumlah = $jumlah;
                    if ($siklus == "1") {
                        $cankirim = DB::table('tb_registrasi')
                                        ->where('tb_registrasi.bidang','=',$r->id)
                                        ->where('tb_registrasi.status','=','2')
                                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                        ->count();
                        $r->cankirim = $cankirim;

                        $beluminput1 = DB::table('tb_registrasi')
                                        ->where('tb_registrasi.bidang','=',$r->id)
                                        ->where('tb_registrasi.status','=','3')
                                        ->whereNull('tb_registrasi.siklus_1')
                                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                        ->count();
                        $r->beluminput1 = $beluminput1;
                        $input1 = DB::table('tb_registrasi')
                                        ->where('tb_registrasi.bidang','=',$r->id)
                                        ->where('tb_registrasi.status','=','3')
                                        ->where('tb_registrasi.status_data1','=', '1')
                                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                        ->count();
                        $r->input1 = $input1;
                        $sudahinput1 = DB::table('tb_registrasi')
                                        ->where('tb_registrasi.bidang','=',$r->id)
                                        ->where('tb_registrasi.status','=','3')
                                        ->where('tb_registrasi.status_data1','=', '2')
                                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                        ->count();
                        $r->sudahinput1 = $sudahinput1;
                    }else{
                        $cankirim = DB::table('tb_registrasi')
                                        ->where('tb_registrasi.bidang','=',$r->id)
                                        ->where('tb_registrasi.status','>=','2')
                                        ->whereNull('tb_registrasi.status_2')
                                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                        ->count();
                        $r->cankirim = $cankirim;
                        $beluminput1 = DB::table('tb_registrasi')
                                        ->where('tb_registrasi.bidang','=',$r->id)
                                        ->where('tb_registrasi.status','=','3')
                                        ->whereNull('tb_registrasi.siklus_2')
                                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                        ->count();
                        $r->beluminput1 = $beluminput1;
                        $input1 = DB::table('tb_registrasi')
                                        ->where('tb_registrasi.bidang','=',$r->id)
                                        ->where('tb_registrasi.status','=','3')
                                        ->where('tb_registrasi.status_data2','=', '1')
                                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                        ->count();
                        $r->input1 = $input1;
                        $sudahinput1 = DB::table('tb_registrasi')
                                        ->where('tb_registrasi.bidang','=',$r->id)
                                        ->where('tb_registrasi.status','=','3')
                                        ->where('tb_registrasi.status_data2','=', '2')
                                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                        ->count();
                        $r->sudahinput1 = $sudahinput1;
                    }
                // }
            }
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function monitoring2(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $data = DB::table('sub_bidang')->where('id', '=', Auth::user()->penyelenggara)->get();
        foreach($data as $skey => $r)
        {
            if (Auth::user()->penyelenggara == 6) {
                $jumlah = DB::table('tb_registrasi')
                            ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                            ->where(function($query)
                                {
                                    if(Auth::user()->badan_usaha == '9'){
                                        $query->where('perusahaan.pemerintah', '=', '9');
                                    }else{
                                        $query->where('perusahaan.pemerintah', '!=', '9');
                                    }
                                })
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
                $r->jumlah = $jumlah;
                if ($siklus == "1") {
                    $cankirim = DB::table('tb_registrasi')
                                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                                    ->where(function($query)
                                        {
                                            if(Auth::user()->badan_usaha == '9'){
                                                $query->where('perusahaan.pemerintah', '=', '9');
                                            }else{
                                                $query->where('perusahaan.pemerintah', '!=', '9');
                                            }
                                        })
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->cankirim = $cankirim;

                    $beluminput1 = DB::table('tb_registrasi')
                                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                                    ->where(function($query)
                                        {
                                            if(Auth::user()->badan_usaha == '9'){
                                                $query->where('perusahaan.pemerintah', '=', '9');
                                            }else{
                                                $query->where('perusahaan.pemerintah', '!=', '9');
                                            }
                                        })
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->whereNull('tb_registrasi.siklus_1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->beluminput1 = $beluminput1;
                    $input1 = DB::table('tb_registrasi')
                                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                                    ->where(function($query)
                                        {
                                            if(Auth::user()->badan_usaha == '9'){
                                                $query->where('perusahaan.pemerintah', '=', '9');
                                            }else{
                                                $query->where('perusahaan.pemerintah', '!=', '9');
                                            }
                                        })
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_data1','=', '1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->input1 = $input1;
                    $sudahinput1 = DB::table('tb_registrasi')
                                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                                    ->where(function($query)
                                        {
                                            if(Auth::user()->badan_usaha == '9'){
                                                $query->where('perusahaan.pemerintah', '=', '9');
                                            }else{
                                                $query->where('perusahaan.pemerintah', '!=', '9');
                                            }
                                        })
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_data1','=', '2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->sudahinput1 = $sudahinput1;
                }else{
                    $cankirim = DB::table('tb_registrasi')
                                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                                    ->where(function($query)
                                        {
                                            if(Auth::user()->badan_usaha == '9'){
                                                $query->where('perusahaan.pemerintah', '=', '9');
                                            }else{
                                                $query->where('perusahaan.pemerintah', '!=', '9');
                                            }
                                        })
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->cankirim = $cankirim;
                    $beluminput1 = DB::table('tb_registrasi')
                                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                                    ->where(function($query)
                                        {
                                            if(Auth::user()->badan_usaha == '9'){
                                                $query->where('perusahaan.pemerintah', '=', '9');
                                            }else{
                                                $query->where('perusahaan.pemerintah', '!=', '9');
                                            }
                                        })
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->whereNull('tb_registrasi.siklus_2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->beluminput1 = $beluminput1;
                    $input1 = DB::table('tb_registrasi')
                                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                                    ->where(function($query)
                                        {
                                            if(Auth::user()->badan_usaha == '9'){
                                                $query->where('perusahaan.pemerintah', '=', '9');
                                            }else{
                                                $query->where('perusahaan.pemerintah', '!=', '9');
                                            }
                                        })
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_data2','=', '1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->input1 = $input1;
                    $sudahinput1 = DB::table('tb_registrasi')
                                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                                    ->where(function($query)
                                        {
                                            if(Auth::user()->badan_usaha == '9'){
                                                $query->where('perusahaan.pemerintah', '=', '9');
                                            }else{
                                                $query->where('perusahaan.pemerintah', '!=', '9');
                                            }
                                        })
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_data2','=', '2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->sudahinput1 = $sudahinput1;
                }
            }else{
                $jumlah = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->jumlah = $jumlah;
                if ($siklus == "1") {
                    $cankirim = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->cankirim = $cankirim;

                    $beluminput1 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->whereNull('tb_registrasi.siklus_1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->beluminput1 = $beluminput1;
                    $input1 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_data1','=', '1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->input1 = $input1;
                    $sudahinput1 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_data1','=', '2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->sudahinput1 = $sudahinput1;
                }else{
                    $cankirim = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->cankirim = $cankirim;
                    $beluminput1 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->whereNull('tb_registrasi.siklus_2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->beluminput1 = $beluminput1;
                    $input1 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_data2','=', '1')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->input1 = $input1;
                    $sudahinput1 = DB::table('tb_registrasi')
                                    ->where('tb_registrasi.bidang','=',$r->id)
                                    ->where('tb_registrasi.status','=','3')
                                    ->where('tb_registrasi.status_data2','=', '2')
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                    ->count();
                    $r->sudahinput1 = $sudahinput1;
                }
            }
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    // TC
    public function monitoring5(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $data = DB::table('sub_bidang')->where('id', '=', Auth::user()->penyelenggara)->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->jumlah = $jumlah;
            if ($siklus == "1") {
                $cankirim = DB::table('tb_registrasi')
                                ->leftJoin('tb_evaluasi_tc','tb_registrasi.id','=','tb_evaluasi_tc.id_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->get();
                $r->cankirim = $cankirim->where('id_registrasi', null)->count('id_registrasi');

                $beluminput1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','=','3')
                                ->whereNull('tb_registrasi.siklus_1')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->beluminput1 = $beluminput1;
                $input1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.status_data1','=', '1')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->input1 = $input1;
                $sudahinput1 = DB::table('tb_registrasi')
                                ->join('tb_evaluasi_tc','tb_registrasi.id','=','tb_evaluasi_tc.id_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.status_data1','=', '2')
                                ->where('tb_evaluasi_tc.siklus','=', $siklus)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->groupby('tb_registrasi.id')
                                ->get();
                $r->sudahinput1 = count($sudahinput1);
            }else{
                $cankirim = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','=','2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->cankirim = $cankirim;
                $beluminput1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','=','3')
                                ->whereNull('tb_registrasi.siklus_2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->beluminput1 = $beluminput1;
                $input1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.status_data2','=', '1')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->input1 = $input1;
                $sudahinput1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.status_data2','=', '2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->sudahinput1 = $sudahinput1;
            }
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function monitoring3(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $data = DB::table('sub_bidang')->where('id', '=', Auth::user()->penyelenggara)->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->jumlah = $jumlah;
            if ($siklus == "1") {
                $beluminputtp = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where('tb_registrasi.pemeriksaan','=', NULL)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->beluminputtp = $beluminputtp;
                $beluminputrpr = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where('tb_registrasi.rpr1','=', NULL)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->beluminputrpr = $beluminputrpr;
                $inputtp = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.status_data1','=', '1')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->inputtp = $inputtp;
                $inputrpr = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.status_datarpr1','=', '1')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->inputrpr = $inputrpr;
                $sudahinputtp = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.status_data1','=', '2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->sudahinputtp = $sudahinputtp;
                $sudahinputrpr = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.status_datarpr1','=', '2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->sudahinputrpr = $sudahinputrpr;
            }else{
                $beluminputtp = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.status_data2','=', NULL)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->beluminputtp = $beluminputtp;
                $beluminputrpr = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.rpr2','=', NULL)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->beluminputrpr = $beluminputrpr;
                $inputtp = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.status_data2','=', '1')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->inputtp = $inputtp;
                $inputrpr = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.status_datarpr2','=', '1')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->inputrpr = $inputrpr;
                $sudahinputtp = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.status_data2','=', '2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->sudahinputtp = $sudahinputtp;
                $sudahinputrpr = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','=','3')
                                ->where('tb_registrasi.status_datarpr2','=', '2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->sudahinputrpr = $sudahinputrpr;
            }
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function rekaptransaksi($id)
    {
        $data = DB::table('sub_bidang')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->select('sub_bidang.*', 'tb_bidang.bidang')
                ->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            $r->jumlah = $jumlah;
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function rekaptransaksiexcel(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $data = DB::table('sub_bidang')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->select('sub_bidang.*', 'tb_bidang.bidang')
                ->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            $r->jumlah = $jumlah;
        }
        // dd($data);
        if ($request->export == "Export Excel") {
            Excel::create('Rekap Transaksi Berdasarkan Parameter', function($excel) use ($data, $tahun) {
                $excel->sheet('Rekap', function($sheet) use ($data, $tahun) {
                    $sheet->loadView('laporan.rekap.excel.transaksi_parameter', array('data'=>$data, 'tahun'=>$tahun) );
                });
            })->download('xls');
        }else{
            $pdf = PDF::loadview('laporan.rekap.excel.transaksi_parameter', compact('data', 'tahun'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            return $pdf->stream('Rekap Transaksi Berdasarkan Parameter.pdf');
        }
    }

    public function grafiktransaksit(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $data = DB::table('sub_bidang')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->select('sub_bidang.*', 'tb_bidang.bidang')
                ->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->first();
            $r->jumlah = $jumlah->jumlah_tarif;
        }

        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->alias;
            $row['y'] = $r->jumlah;
            array_push($rows, $row);
        }

        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }

    public function grafiktransaksip(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $data = DB::table('sub_bidang')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->select('sub_bidang.*', 'tb_bidang.bidang')
                ->get();
        foreach($data as $skey => $r)
        {
            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->count();
            $r->peserta = $peserta;
        }

        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->alias;
            $row['y'] = $r->peserta;
            array_push($rows, $row);
        }

        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }

    public function grafikmalaria(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $data = DB::select("SELECT
                            	A.*,
                            	(SELECT count(kesimpulan) FROM tb_evaluasi_malaria WHERE kesimpulan = A.name) as y
                            FROM
                            	( SELECT kesimpulan as name FROM tb_evaluasi_malaria WHERE kesimpulan != '') A");

        return($data);
        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }

    public function grafiktc(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $data = DB::select("SELECT
                            	A.*,
                            	( SELECT count( nilai ) FROM tb_evaluasi_tc WHERE nilai = A.name AND kode_sediaan LIKE '____/TCC/1%' ) AS y
                            FROM
                            	( SELECT DISTINCT ( nilai ) as name FROM tb_evaluasi_tc WHERE kode_sediaan LIKE '____/TCC/1%' ) A");

        return($data);
        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }

    public function grafikpembayaran(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $data = DB::table('pembayaran')->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('tb_registrasi.id_pembayaran','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->groupBy('tb_registrasi.id_pembayaran')
                            ->count(DB::raw('tb_registrasi.id_pembayaran'));
            $r->jumlah = $jumlah;
        }
        // dd($data);
        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->type;
            $row['y'] = $r->jumlah;
            array_push($rows, $row);
        }
        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }

    public function rekappesertaparameter($id)
    {
        $data = DB::table('sub_bidang')->get();
        foreach($data as $skey => $r)
        {
            $blk = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '1')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->blk = $blk;
            $bblk = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->bblk = $bblk;
            $rspem = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '3')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->rspem = $rspem;
            $pus = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '4')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->pus = $pus;
            $labkes = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '5')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->labkes = $labkes;
            $rsswa = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '6')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->rsswa = $rsswa;
            $labkl = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '7')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->labkl = $labkl;
            $utdrs = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '8')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->utdrs = $utdrs;
            $utdpmi = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '9')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->utdpmi = $utdpmi;
            $rstni = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '10')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->rstni = $rstni;
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function rekappesertaparameterexcel(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $data = DB::table('sub_bidang')->get();
        foreach($data as $skey => $r)
        {
            $blk = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '1')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->blk = $blk;
            $bblk = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->bblk = $bblk;
            $rspem = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '3')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->rspem = $rspem;
            $pus = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '4')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->pus = $pus;
            $labkes = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '5')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->labkes = $labkes;
            $rsswa = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '6')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->rsswa = $rsswa;
            $labkl = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '7')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->labkl = $labkl;
            $utdrs = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '8')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->utdrs = $utdrs;
            $utdpmi = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '9')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->utdpmi = $utdpmi;
            $rstni = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '10')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->rstni = $rstni;
        }
            // dd($data);
        if ($request->export == "Export Excel") {
            Excel::create('Rekapitulasi Peserta PME Berdasarkan Jenis Instansi dan Parameter', function($excel) use ($data, $tahun) {
                $excel->sheet('Rekap', function($sheet) use ($data, $tahun) {
                    $sheet->loadView('laporan.rekap.excel.peserta_instalasi', array('data'=>$data, 'tahun'=>$tahun) );
                });
            })->download('xls');
        }else{
            $pdf = PDF::loadview('laporan.rekap.excel.peserta_instalasi', compact('data', 'tahun'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            return $pdf->stream('Rekapitulasi Peserta PME Berdasarkan Jenis Instansi dan Parameter.pdf');
        }
    }


    public function rekappesertaprovinsi($id)
    {
        $data = DB::table('provinces')->get();
        foreach($data as $skey => $r)
        {
            $hem = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '1')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->hem = $hem;
            $kkl = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->kkl = $kkl;
            $uri = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '3')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->uri = $uri;
            $bta = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '4')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->bta = $bta;
            $tcc = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '5')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->tcc = $tcc;
            $hiv = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '6')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->hiv = $hiv;
            $sif = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '7')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->sif = $sif;
            $hbs = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '8')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->hbs = $hbs;
            $hcv = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '9')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->hcv = $hcv;
            $mal = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '10')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->mal = $mal;
            $kai = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '11')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->kai = $kai;
            $kat = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '12')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->kat = $kat;
            $bac = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '13')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->bac = $bac;
            $tot = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->tot = $tot;
        }
        // dd($data);
        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->name;
            $row['y'] = $r->tot;
            array_push($rows, $row);
        }
        $rowshem = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->name;
            $row['y'] = $r->hem;
            array_push($rowshem, $row);
        }
        $rowskkl = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->name;
            $row['y'] = $r->kkl;
            array_push($rowskkl, $row);
        }
        $rowsuri = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->name;
            $row['y'] = $r->uri;
            array_push($rowsuri, $row);
        }
        $rowsbta = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->name;
            $row['y'] = $r->bta;
            array_push($rowsbta, $row);
        }
        $rowstcc = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->name;
            $row['y'] = $r->tcc;
            array_push($rowstcc, $row);
        }
        $rowshiv = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->name;
            $row['y'] = $r->hiv;
            array_push($rowshiv, $row);
        }
        $rowssif = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->name;
            $row['y'] = $r->sif;
            array_push($rowssif, $row);
        }
        $rowshbs = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->name;
            $row['y'] = $r->hbs;
            array_push($rowshbs, $row);
        }
        $rowshcv = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->name;
            $row['y'] = $r->hcv;
            array_push($rowshcv, $row);
        }
        $rowsmal = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->name;
            $row['y'] = $r->mal;
            array_push($rowsmal, $row);
        }
        $rowskai = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->name;
            $row['y'] = $r->kai;
            array_push($rowskai, $row);
        }
        $rowskat = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->name;
            $row['y'] = $r->kat;
            array_push($rowskat, $row);
        }
        $rowsbac = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->name;
            $row['y'] = $r->bac;
            array_push($rowsbac, $row);
        }

        // return response ($rows);
        return response()->json([
            'Hasil'=>$data,
            'rows'=>$rows,
            'rowshem'=>$rowshem,
            'rowskkl'=>$rowskkl,
            'rowsuri'=>$rowsuri,
            'rowsbta'=>$rowsbta,
            'rowstcc'=>$rowstcc,
            'rowshiv'=>$rowshiv,
            'rowssif'=>$rowssif,
            'rowshbs'=>$rowshbs,
            'rowshcv'=>$rowshcv,
            'rowsmal'=>$rowsmal,
            'rowskai'=>$rowskai,
            'rowskat'=>$rowskat,
            'rowsbac'=>$rowsbac
        ]);

    }

    public function rekappesertaprovinsiexcel(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $bidang = $request->sub;
        // dd($bidang);
        $data = DB::table('provinces')->get();
        foreach($data as $skey => $r)
        {
            $hem = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '1')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->hem = $hem;
            $kkl = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->kkl = $kkl;
            $uri = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '3')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->uri = $uri;
            $bta = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '4')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->bta = $bta;
            $tcc = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '5')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->tcc = $tcc;
            $hiv = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '6')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->hiv = $hiv;
            $sif = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '7')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->sif = $sif;
            $hbs = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '8')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->hbs = $hbs;
            $hcv = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '9')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->hcv = $hcv;
            $mal = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '10')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->mal = $mal;
            $kai = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '11')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->kai = $kai;
            $kat = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '12')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->kat = $kat;
            $bac = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang', '=', '13')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->bac = $bac;
            $tot = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->leftjoin('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.provinsi','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->tot = $tot;
        }
        // dd($request->export);
        if ($request->export == "Download Excel") {
            Excel::create('Rekap Peserta Berdasarkan Provinsi', function($excel) use ($data, $tahun, $bidang) {
                $excel->sheet('Rekap', function($sheet) use ($data, $tahun, $bidang) {
                    $sheet->loadView('laporan.rekap.excel.peserta_provinsi_excel', array('data'=>$data, 'tahun'=>$tahun,'bidang'=>$bidang) );
                });
            })->download('xls');
        }else{
            $pdf = PDF::loadview('laporan.rekap.excel.peserta_provinsi', compact('data', 'tahun','bidang'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            return $pdf->stream('Rekap Peserta Berdasarkan Provinsi.pdf');
        }
    }
    public function rekapinstansi($id)
    {
        $data = DB::table('badan_usaha')
                ->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($jumlah[0]->jumlah_tarif == NULL) {
                $r->jumlah = 0;
            }else{
                $r->jumlah = $jumlah[0]->jumlah_tarif;
            }

            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->groupby('perusahaan.id')
                            ->get();
            $r->peserta = count($peserta);
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function rekapinstansiexcel(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $data = DB::table('badan_usaha')->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($jumlah[0]->jumlah_tarif == NULL) {
                $r->jumlah = 0;
            }else{
                $r->jumlah = $jumlah[0]->jumlah_tarif;
            }

            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->groupby('perusahaan.id')
                            ->get();
            $r->peserta = count($peserta);
        }
            // dd($data);
        if ($request->export == "Export Excel") {
            Excel::create('Rekapitulasi Peserta PME Berdasarkan Jenis Instansi', function($excel) use ($data, $tahun) {
                $excel->sheet('Rekap', function($sheet) use ($data, $tahun) {
                    $sheet->loadView('laporan.rekap.excel.peserta_instansi_excel', array('data'=>$data, 'tahun'=>$tahun) );
                });
            })->download('xls');
        }else{
            $pdf = PDF::loadview('laporan.rekap.excel.peserta_instansi', compact('data', 'tahun'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            return $pdf->stream('Rekapitulasi Peserta PME Berdasarkan Jenis Instansi.pdf');
        }
    }

    public function grafikinstansip(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $param = $request->get('a');
        $data = DB::table('badan_usaha')->get();
        $sub = DB::table('sub_bidang')->where('id_bidang', $param)->get();

        $a = 0;
        foreach ($sub as $key => $value) {
            $bidang[$a] = $value->id;
            $a++;
        }

        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->whereIn('tb_registrasi.bidang', $bidang)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($jumlah[0]->jumlah_tarif == NULL) {
                $r->jumlah = 0;
            }else{
                $r->jumlah = $jumlah[0]->jumlah_tarif;
            }

            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->whereIn('tb_registrasi.bidang',$bidang)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->count();
            $r->peserta = $peserta;
        }
        // dd($data);
        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->alias;
            $row['y'] = $r->peserta;
            array_push($rows, $row);
        }

        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }

    public function grafikinstansisemua(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $data = DB::table('badan_usaha')->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($jumlah[0]->jumlah_tarif == NULL) {
                $r->jumlah = 0;
            }else{
                $r->jumlah = $jumlah[0]->jumlah_tarif;
            }

            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->groupby('perusahaan.id')
                            ->get();
            $r->peserta = count($peserta);
        }
        // dd($data);
        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->alias;
            $row['y'] = $r->peserta;
            array_push($rows, $row);
        }

        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }

     public function grafikinstansiview(\Illuminate\Http\Request $request)
    {
        $bidang = DB::table('tb_bidang')->get();

        return view('laporan.grafik.peserta_instansi', compact('bidang'));

    }

    public function grafiktcpeserta(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $data = DB::table('badan_usaha')->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($jumlah[0]->jumlah_tarif == NULL) {
                $r->jumlah = 0;
            }else{
                $r->jumlah = $jumlah[0]->jumlah_tarif;
            }

            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('sub_bidang.id', '=', 5) //Berdasarkan sub_bidang 5 = telur cacing
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->count();
            $r->peserta = $peserta;
        }
        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->alias;
            $row['y'] = $r->peserta;
            array_push($rows, $row);
        }

        return response ($rows);
    }

    public function grafiktcpesertajawabanbaik(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $siklus = $request->get('siklus');

        $jumlahs = DB::table('tb_registrasi')
            ->select(DB::raw('SUM(tb_evaluasi_tc.nilai / 4) as predikat'))
            ->leftJoin('tb_evaluasi_tc','tb_registrasi.id','=','tb_evaluasi_tc.id_registrasi')
            ->where('bidang','=', 5)
            ->where('status','>=', 2)
            ->where('tahun','=', $tahun)
            ->where('tb_evaluasi_tc.siklus','=', $siklus)
            ->groupby('tb_registrasi.id')
            ->get();

        $cankirim = DB::table('tb_registrasi')
                            ->leftJoin('tb_evaluasi_tc','tb_registrasi.id','=','tb_evaluasi_tc.id_registrasi')
                            ->where('tb_registrasi.bidang','=', 5)
                            ->where('tb_registrasi.status','>=', 2)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->get();
        $belumEvaluasi = $cankirim->where('id_registrasi', null)->count('id_registrasi');

        $baik = $kurang = 0;
        foreach($jumlahs as $jumlah) {
            if($jumlah->predikat >= 6) {
                $baik++;
            } else {
                $kurang++;
            }
        }

        $data = [
            0 => [
                "alias" => "Baik",
                "jumlah"    => $baik
            ],
            1 => [
                "alias" => "Kurang",
                "jumlah"    => $kurang
            ],
            2 => [
                "alias" => "Belum Evaluasi",
                "jumlah"    => $belumEvaluasi
            ],
        ];

        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r['alias'];
            $row['y'] = $r['jumlah'];
            array_push($rows, $row);
        }

        return response ($rows);
    }

    public function grafikmalariapesertajawabanbaik(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $siklus = $request->get('siklus');

        $buruk = DB::table('tb_evaluasi_malaria')
                    ->select(DB::raw('COUNT(kesimpulan) as predikat'))
                    ->where('tahun','=', $tahun)
                    ->where('siklus','=', $siklus)
                    ->where('kesimpulan','=', 'Buruk')
                    ->first();
        $baik = DB::table('tb_evaluasi_malaria')
                    ->select(DB::raw('COUNT(kesimpulan) as predikat'))
                    ->where('tahun','=', $tahun)
                    ->where('siklus','=', $siklus)
                    ->where('kesimpulan','=', 'Baik')
                    ->first();

        if($buruk != null) {
            $buruk = $buruk->predikat;
        } else {
            $buruk = 0;
        }
        if($baik != null) {
            $baik = $baik->predikat;
        } else {
            $baik = 0;
        }

        $data = [
            0 => [
                "alias" => "Baik",
                "jumlah"    => $baik
            ],
            1 => [
                "alias" => "Buruk",
                "jumlah"    => $buruk
            ],
        ];

        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r['alias'];
            $row['y'] = $r['jumlah'];
            array_push($rows, $row);
        }

        return response ($rows);
    }

    public function grafikinstansij(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $param = $request->get('a');
        $data = DB::table('badan_usaha')->get();
        $sub = DB::table('sub_bidang')->where('id_bidang', $param)->get();

        $a = 0;
        foreach ($sub as $key => $value) {
            $bidang[$a] = $value->id;
            $a++;
        }

        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->whereIn('tb_registrasi.bidang',$bidang)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($jumlah[0]->jumlah_tarif == NULL) {
                $r->jumlah = 0;
            }else{
                $r->jumlah = $jumlah[0]->jumlah_tarif;
            }

            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->whereIn('tb_registrasi.bidang', $bidang)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->count();
            $r->peserta = $peserta;
        }
        // dd($data);
        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->alias;
            $row['y'] = $r->jumlah;
            array_push($rows, $row);
        }

        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }

    public function grafikinstansijsemua(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $data = DB::table('badan_usaha')->get();

        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($jumlah[0]->jumlah_tarif == NULL) {
                $r->jumlah = 0;
            }else{
                $r->jumlah = $jumlah[0]->jumlah_tarif;
            }

            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->count();
            $r->peserta = $peserta;
        }
        // dd($data);
        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->alias;
            $row['y'] = $r->jumlah;
            array_push($rows, $row);
        }

        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }

    // MALARIA
    public function grafikmalariapeserta(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $data = DB::table('badan_usaha')->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->where(function($query) use ($siklus)
                                {
                                    $query->where('tb_registrasi.siklus', '12')
                                        ->orwhere('tb_registrasi.siklus', $siklus);
                                })
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($jumlah[0]->jumlah_tarif == NULL) {
                $r->jumlah = 0;
            }else{
                $r->jumlah = $jumlah[0]->jumlah_tarif;
            }

            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('sub_bidang.id', '=', 10) //Berdasarkan sub_bidang 5 = telur cacing
                            ->where(function($query) use ($siklus)
                                {
                                    $query->where('tb_registrasi.siklus', '12')
                                        ->orwhere('tb_registrasi.siklus', $siklus);
                                })
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->count();
            $r->peserta = $peserta;
        }
        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->alias;
            $row['y'] = $r->peserta;
            array_push($rows, $row);
        }

        return response ($rows);
    }

    public function rekappesertatransaksi($id)
    {
        $data = DB::table('sub_bidang')->get();
        foreach($data as $skey => $r)
        {
            $blk = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '1')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($blk[0]->jumlah_tarif == NULL) {
                $r->blk = 0;
            }else{
                $r->blk = $blk[0]->jumlah_tarif;
            }

            $bblk = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($bblk[0]->jumlah_tarif == NULL) {
                $r->bblk = 0;
            }else{
                $r->bblk = $bblk[0]->jumlah_tarif;
            }

            $rspem = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '3')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($rspem[0]->jumlah_tarif == NULL) {
                $r->rspem = 0;
            }else{
                $r->rspem = $rspem[0]->jumlah_tarif;
            }
            $pus = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '4')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($pus[0]->jumlah_tarif == NULL) {
                $r->pus = 0;
            }else{
                $r->pus = $pus[0]->jumlah_tarif;
            }
            $labkes = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '5')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($labkes[0]->jumlah_tarif == NULL) {
                $r->labkes = 0;
            }else{
                $r->labkes = $labkes[0]->jumlah_tarif;
            }
            $rsswa = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '6')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($rsswa[0]->jumlah_tarif == NULL) {
                $r->rsswa = 0;
            }else{
                $r->rsswa = $rsswa[0]->jumlah_tarif;
            }
            $labkl = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '7')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($labkl[0]->jumlah_tarif == NULL) {
                $r->labkl = 0;
            }else{
                $r->labkl = $labkl[0]->jumlah_tarif;
            }
            $utdrs = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '8')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($utdrs[0]->jumlah_tarif == NULL) {
                $r->utdrs = 0;
            }else{
                $r->utdrs = $utdrs[0]->jumlah_tarif;
            }
            $utdpmi = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '9')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($utdpmi[0]->jumlah_tarif == NULL) {
                $r->utdpmi = 0;
            }else{
                $r->utdpmi = $utdpmi[0]->jumlah_tarif;
            }
            $rstni = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '10')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($rstni[0]->jumlah_tarif == NULL) {
                $r->rstni = 0;
            }else{
                $r->rstni = $rstni[0]->jumlah_tarif;
            }
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function rekappesertatransaksiexcel(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $data = DB::table('sub_bidang')->get();
        foreach($data as $skey => $r)
        {
            $blk = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '1')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($blk[0]->jumlah_tarif == NULL) {
                $r->blk = 0;
            }else{
                $r->blk = $blk[0]->jumlah_tarif;
            }

            $bblk = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($bblk[0]->jumlah_tarif == NULL) {
                $r->bblk = 0;
            }else{
                $r->bblk = $bblk[0]->jumlah_tarif;
            }

            $rspem = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '3')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($rspem[0]->jumlah_tarif == NULL) {
                $r->rspem = 0;
            }else{
                $r->rspem = $rspem[0]->jumlah_tarif;
            }
            $pus = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '4')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($pus[0]->jumlah_tarif == NULL) {
                $r->pus = 0;
            }else{
                $r->pus = $pus[0]->jumlah_tarif;
            }
            $labkes = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '5')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($labkes[0]->jumlah_tarif == NULL) {
                $r->labkes = 0;
            }else{
                $r->labkes = $labkes[0]->jumlah_tarif;
            }
            $rsswa = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '6')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($rsswa[0]->jumlah_tarif == NULL) {
                $r->rsswa = 0;
            }else{
                $r->rsswa = $rsswa[0]->jumlah_tarif;
            }
            $labkl = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '7')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($labkl[0]->jumlah_tarif == NULL) {
                $r->labkl = 0;
            }else{
                $r->labkl = $labkl[0]->jumlah_tarif;
            }
            $utdrs = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '8')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($utdrs[0]->jumlah_tarif == NULL) {
                $r->utdrs = 0;
            }else{
                $r->utdrs = $utdrs[0]->jumlah_tarif;
            }
            $utdpmi = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '9')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($utdpmi[0]->jumlah_tarif == NULL) {
                $r->utdpmi = 0;
            }else{
                $r->utdpmi = $utdpmi[0]->jumlah_tarif;
            }
            $rstni = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '10')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($rstni[0]->jumlah_tarif == NULL) {
                $r->rstni = 0;
            }else{
                $r->rstni = $rstni[0]->jumlah_tarif;
            }
        }
            // dd($data);
        if ($request->export == "Export Excel") {
            Excel::create('Rekapitulasi Peserta PME Berdasarkan Jenis Instansi dan Parameter', function($excel) use ($data, $tahun) {
                $excel->sheet('Rekap', function($sheet) use ($data, $tahun) {
                    $sheet->loadView('laporan.rekap.excel.tarif_parameter', array('data'=>$data, 'tahun'=>$tahun) );
                });
            })->download('xls');
        }else{
            $pdf = PDF::loadview('laporan.rekap.excel.tarif_parameter', compact('data', 'tahun'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            return $pdf->stream('Rekapitulasi Peserta PME Berdasarkan Jenis Instansi dan Parameter.pdf');
        }
    }

    public function seluruh(){
      $bidang = DB::table('sub_bidang')->get();

      return view('laporan.rekap.seluruh_peserta', compact('bidang'));
    }

    public function seluruhcetak(\Illuminate\Http\Request $request){
      $tahun = $request->tahun;
      $bidangs = $request->bidang;
      $bidang = DB::table('sub_bidang')->where('id',$bidangs)->first();
      // return $bidang;
      //hematologi


      return view('laporan.rekap.excel.seluruh_peserta', compact('bidang','tahun','bidang'));
    }


    public function laporanhasilpnpme()
    {
        $data = DB::table('sub_bidang')->get();
        $provinsi = DB::table('provinces')->get();
        return view('laporan.grafik.hasilpnpme.index', compact('data', 'provinsi'));
    }
    public function laporanhasilpnpmep(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        // dd($input);
        if ($input['parameter'] == 'Semua Parameter') {
            $parameter = new stdClass();
            $parameter->nama_parameter = 'Semua Parameter';
        }else{
            $parameter = DB::table('parameter')->where('id', $input['parameter'])->first();
        }
        $provinsi = DB::table('provinces')->where('id', $input['provinsi'])->first();
        $kota = DB::table('regencies')->where('id', $input['kota'])->first();
        // dd($provinsi);
        
        if ($input['bidang'] == 'hematologi' || $input['bidang'] == 'kimia klinik' || $input['bidang'] == 'kimia air' || $input['bidang'] == 'kimia air terbatas') {
            if($input['bidang'] == 'kimia air' || $input['bidang'] == 'kimia air terbatas'){
                    $tipe1 = DB::table('tb_zscore')
                            ->select(DB::raw("CASE 
                                WHEN zscore <= '2' AND zscore >= '-2'  THEN 'Memuaskan' 
                                WHEN zscore <= '3' AND zscore >= '-3'  THEN 'Peringatan' 
                                WHEN zscore > '3' AND zscore < '-3' THEN 'Tidak Memuaskan' 
                                ELSE 'Tidak Dievaluasi' 
                                END AS keterangan,count(1) as jumlah"))
                            ->join('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                            ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                            ->where('tb_zscore.siklus', $input['siklus'])
                            ->where('tb_zscore.tahun', $input['tahun'])
                            ->where(function ($query) use ($input) {
                                if ($input['provinsi'] == 'Semua Provinsi') {
                                }else{
                                    if ($input['kota'] == 'Semua Kota') {
                                        $query->where('provinsi', $input['provinsi']);
                                    }else{
                                        $query->where('kota', $input['kota']);
                                    }
                                }
                            })
                            ->where('tb_zscore.form', $input['bidang'])
                            ->where(function ($query) use ($input) {
                                if ($input['parameter'] == 'Semua Parameter') {
                                }else{
                                    $query->where('parameter', $input['parameter']);
                                }
                            })
                            ->groupby('keterangan')
                            ->get();
                    $count1 = 0;
                    foreach ($tipe1 as $key => $val) {
                        if($val->keterangan != 'Tidak Dievaluasi'){
                            $count1 = $count1 + $val->jumlah;
                        }
                    }
                    $tipe2 = DB::table('tb_zscore')
                            ->select(DB::raw("CASE 
                                WHEN zscore <= '2' AND zscore >= '-2'  THEN 'Memuaskan' 
                                WHEN zscore <= '3' AND zscore >= '-3'  THEN 'Peringatan' 
                                WHEN zscore > '3' AND zscore < '-3' THEN 'Tidak Memuaskan' 
                                ELSE 'Tidak Dievaluasi' 
                                END AS keterangan,count(1) as jumlah"))
                            ->join('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                            ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                            ->where('tb_zscore.siklus', $input['siklus'])
                            ->where('tb_zscore.tahun', $input['tahun'])
                            ->where(function ($query) use ($input) {
                                if ($input['provinsi'] == 'Semua Provinsi') {
                                }else{
                                    if ($input['kota'] == 'Semua Kota') {
                                        $query->where('provinsi', $input['provinsi']);
                                    }else{
                                        $query->where('kota', $input['kota']);
                                    }
                                }
                            })
                            ->where('tb_zscore.type', '=', 'a')
                            ->where('tb_zscore.form', $input['bidang'])
                            ->where(function ($query) use ($input) {
                                if ($input['parameter'] == 'Semua Parameter') {
                                }else{
                                    $query->where('parameter', $input['parameter']);
                                }
                            })
                            ->groupby('keterangan')
                            ->get();
                    $count2 = 0;
                    foreach ($tipe2 as $key => $val) {
                        if($val->keterangan != 'Tidak Dievaluasi'){
                            $count2 = $count2 + $val->jumlah;
                        }
                    }
            }else{
                    if($input['kelompok'] == 'Peserta'){
                        $table = "tb_zscore";
                    }elseif ($input['kelompok'] == 'Alat') {
                        $table = "tb_zscore_alat";
                    }elseif ($input['kelompok'] == 'Metode') {
                        $table = "tb_zscore_metode";
                    }
                    $tipe1 = DB::table($table)
                            ->select(DB::raw("CASE 
                                WHEN zscore <= '2' AND zscore >= '-2'  THEN 'Memuaskan' 
                                WHEN zscore <= '3' AND zscore >= '-3'  THEN 'Peringatan' 
                                WHEN zscore > '3' AND zscore < '-3' THEN 'Tidak Memuaskan' 
                                ELSE 'Tidak Dievaluasi' 
                                END AS keterangan,count(1) as jumlah"))
                            ->join('tb_registrasi', 'tb_registrasi.id', '=', $table.'.id_registrasi')
                            ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                            ->where($table.'.siklus', $input['siklus'])
                            ->where($table.'.tahun', $input['tahun'])
                            ->where(function ($query) use ($input) {
                                if ($input['provinsi'] == 'Semua Provinsi') {
                                }else{
                                    if ($input['kota'] == 'Semua Kota') {
                                        $query->where('provinsi', $input['provinsi']);
                                    }else{
                                        $query->where('kota', $input['kota']);
                                    }
                                }
                            })
                            ->where($table.'.type', '=', 'a')
                            ->where($table.'.form', $input['bidang'])
                            ->where(function ($query) use ($input) {
                                if ($input['parameter'] == 'Semua Parameter') {
                                }else{
                                    $query->where('parameter', $input['parameter']);
                                }
                            })
                            ->groupby('keterangan')
                            ->get();
                    $count1 = 0;
                    foreach ($tipe1 as $key => $val) {
                        if($val->keterangan != 'Tidak Dievaluasi'){
                            $count1 = $count1 + $val->jumlah;
                        }
                    }
                    // dd($tipe1);
                    $tipe2 = DB::table($table)
                            ->select(DB::raw("CASE WHEN zscore <= '2' AND zscore >= '-2'  THEN 'Memuaskan' WHEN zscore <= '3' AND zscore >= '-3'  THEN 'Peringatan' WHEN zscore > '3' AND zscore < '-3' THEN 'Tidak Memuaskan' ELSE 'Tidak Dievaluasi' END AS keterangan,count(1) as jumlah"))
                            ->join('tb_registrasi', 'tb_registrasi.id', '=', $table.'.id_registrasi')
                            ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                            ->where($table.'.siklus', $input['siklus'])
                            ->where($table.'.tahun', $input['tahun'])
                            ->where(function ($query) use ($input) {
                                if ($input['provinsi'] == 'Semua Provinsi') {
                                }else{
                                    if ($input['kota'] == 'Semua Kota') {
                                        $query->where('provinsi', $input['provinsi']);
                                    }else{
                                        $query->where('kota', $input['kota']);
                                    }
                                }
                            })
                            ->where($table.'.form', $input['bidang'])
                            ->where($table.'.type', '=', 'b')
                            ->where(function ($query) use ($input) {
                                if ($input['parameter'] == 'Semua Parameter') {
                                }else{
                                    $query->where('parameter', $input['parameter']);
                                }
                            })
                            ->groupby('keterangan')
                            ->get();
                    $count2 = 0;
                    foreach ($tipe2 as $key => $val) {
                        if($val->keterangan != 'Tidak Dievaluasi'){
                            $count2 = $count2 + $val->jumlah;
                        }
                    }
            }
            // dd($tipe1);
            return view('laporan.grafik.hasilpnpme.grafik', compact('tipe1', 'tipe2', 'input', 'parameter', 'provinsi', 'kota', 'count1', 'count2'));
        }elseif ($input['bidang'] == "3") {
            $tipe1 = DB::table('tb_skoring_urinalisa')
                    ->select(DB::raw("CASE WHEN skoring > '3' THEN 'Sangat Baik' WHEN skoring > '2' THEN 'Baik' WHEN skoring > '1' THEN 'Kurang' ELSE 'Buruk' END AS keterangan, count(1) AS jumlah"))
                    ->join('tb_registrasi', 'tb_registrasi.id', '=', 'tb_skoring_urinalisa.id_registrasi')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->where('type', '=','a')
                    ->where('tb_skoring_urinalisa.siklus', $input['siklus'])
                    ->where('tb_skoring_urinalisa.tahun', $input['tahun'])
                    ->where(function ($query) use ($input) {
                        if ($input['provinsi'] == 'Semua Provinsi') {
                        }else{
                            if ($input['kota'] == 'Semua Kota') {
                                $query->where('provinsi', $input['provinsi']);
                            }else{
                                $query->where('kota', $input['kota']);
                            }
                        }
                    })
                    ->groupby('keterangan')
                    ->get();
            $count1 = 0;
            foreach ($tipe1 as $key => $val) {
                $count1 = $count1 + $val->jumlah;
            }
            // dd($count1);
            $tipe2 = DB::table('tb_skoring_urinalisa')
                    ->select(DB::raw("CASE WHEN skoring > '3' THEN 'Sangat Baik' WHEN skoring > '2' THEN 'Baik' WHEN skoring > '1' THEN 'Kurang' ELSE 'Buruk' END AS keterangan, count(1) AS jumlah"))
                    ->join('tb_registrasi', 'tb_registrasi.id', '=', 'tb_skoring_urinalisa.id_registrasi')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->where('type', '=','b')
                    ->where('tb_skoring_urinalisa.siklus', $input['siklus'])
                    ->where('tb_skoring_urinalisa.tahun', $input['tahun'])
                    ->where(function ($query) use ($input) {
                        if ($input['provinsi'] == 'Semua Provinsi') {
                        }else{
                            if ($input['kota'] == 'Semua Kota') {
                                $query->where('provinsi', $input['provinsi']);
                            }else{
                                $query->where('kota', $input['kota']);
                            }
                        }
                    })
                    ->groupby('keterangan')
                    ->get();
            $count2 = 0;
            foreach ($tipe2 as $key => $val) {
                $count2 = $count2 + $val->jumlah;
            }
            // dd($tipe2);
            // dd($tipe2);
            return view('laporan.grafik.hasilpnpme.urin', compact('tipe1', 'tipe2', 'input', 'parameter', 'provinsi', 'kota', 'count1', 'count2'));
        }elseif ($input['bidang'] == "4") {
            $lulus = DB::table("tb_evaluasi_bta_status")
                        ->join('tb_registrasi', 'tb_registrasi.id', '=', 'tb_evaluasi_bta_status.id_registrasi')
                        ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->where('tb_evaluasi_bta_status.status','=','Lulus')
                        ->where('tb_evaluasi_bta_status.siklus', $input['siklus'])
                        ->where(function ($query) use ($input) {
                            if ($input['provinsi'] == 'Semua Provinsi') {
                            }else{
                                if ($input['kota'] == 'Semua Kota') {
                                    $query->where('provinsi', $input['provinsi']);
                                }else{
                                    $query->where('kota', $input['kota']);
                                }
                            }
                        })
                        ->where('tahun', $input['tahun'])
                        ->count();
            $tidaklulus = DB::table("tb_evaluasi_bta_status")
                        ->join('tb_registrasi', 'tb_registrasi.id', '=', 'tb_evaluasi_bta_status.id_registrasi')
                        ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->where('tb_evaluasi_bta_status.status','=','Tidak Lulus')
                        ->where('tb_evaluasi_bta_status.siklus', $input['siklus'])
                        ->where(function ($query) use ($input) {
                            if ($input['provinsi'] == 'Semua Provinsi') {
                            }else{
                                if ($input['kota'] == 'Semua Kota') {
                                    $query->where('provinsi', $input['provinsi']);
                                }else{
                                    $query->where('kota', $input['kota']);
                                }
                            }
                        })
                        ->where('tahun', $input['tahun'])
                        ->count();
            $count = $lulus + $tidaklulus;
            return view('laporan.grafik.hasilpnpme.bta', compact('lulus', 'tidaklulus', 'input', 'parameter', 'provinsi', 'count', 'kota'));
        }elseif ($input['bidang'] == "5") {
            $jumlahs = DB::table('tb_registrasi')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->leftJoin('tb_evaluasi_tc','tb_registrasi.id','=','tb_evaluasi_tc.id_registrasi')
                    ->where('bidang','=', 5)
                    ->where('status','>=', 2)
                    ->where('tahun','=', $input['tahun'])
                    ->where(function ($query) use ($input) {
                        if ($input['provinsi'] == 'Semua Provinsi') {
                        }else{
                            if ($input['kota'] == 'Semua Kota') {
                                $query->where('provinsi', $input['provinsi']);
                            }else{
                                $query->where('kota', $input['kota']);
                            }
                        }
                    })
                    ->where('tb_evaluasi_tc.siklus','=', $input['siklus'])
                    ->select(DB::raw('SUM(tb_evaluasi_tc.nilai / 4) as predikat'))
                    ->groupby('tb_registrasi.id')
                    ->get();

            $cankirim = DB::table('tb_registrasi')
                    ->leftJoin('tb_evaluasi_tc','tb_registrasi.id','=','tb_evaluasi_tc.id_registrasi')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang','=', 5)
                    ->where('tb_registrasi.status','>=', 2)
                    ->where(function ($query) use ($input) {
                        if ($input['provinsi'] == 'Semua Provinsi') {
                        }else{
                            if ($input['kota'] == 'Semua Kota') {
                                $query->where('provinsi', $input['provinsi']);
                            }else{
                                $query->where('kota', $input['kota']);
                            }
                        }
                    })
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                    ->get();
            $belumEvaluasi = $cankirim->where('id_registrasi', null)->count('id_registrasi');

            $baik = $kurang = 0;
            foreach($jumlahs as $jumlah) {
                if($jumlah->predikat >= 6) {
                    $baik++;
                } else {
                    $kurang++;
                }
            }

            $count = $baik + $kurang + $belumEvaluasi;

            $data = [
                0 => [
                    "alias" => "Baik",
                    "jumlah"    => $baik
                ],
                1 => [
                    "alias" => "Kurang",
                    "jumlah"    => $kurang
                ],
                2 => [
                    "alias" => "Belum Evaluasi",
                    "jumlah"    => $belumEvaluasi
                ],
            ];
            // dd($data[0]['alias']);
            return view('laporan.grafik.hasilpnpme.tc', compact('data', 'input', 'parameter', 'provinsi', 'count', 'kota'));
        }elseif ($input['bidang'] == "10") {
            $buruk = DB::table('tb_evaluasi_malaria')
                        ->join('tb_registrasi', 'tb_registrasi.id', '=', 'tb_evaluasi_malaria.id_registrasi')
                        ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->select(DB::raw('COUNT(kesimpulan) as predikat'))
                        ->where('tahun','=', $input['tahun'])
                        ->where('tb_evaluasi_malaria.siklus','=', $input['siklus'])
                        ->where('kesimpulan','=', 'Buruk')
                        ->where(function ($query) use ($input) {
                            if ($input['provinsi'] == 'Semua Provinsi') {
                            }else{
                                if ($input['kota'] == 'Semua Kota') {
                                    $query->where('provinsi', $input['provinsi']);
                                }else{
                                    $query->where('kota', $input['kota']);
                                }
                            }
                        })
                        ->first();
            $baik = DB::table('tb_evaluasi_malaria')
                        ->join('tb_registrasi', 'tb_registrasi.id', '=', 'tb_evaluasi_malaria.id_registrasi')
                        ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->select(DB::raw('COUNT(kesimpulan) as predikat'))
                        ->where('tahun','=', $input['tahun'])
                        ->where('tb_evaluasi_malaria.siklus','=', $input['siklus'])
                        ->where('kesimpulan','=', 'Baik')
                        ->where(function ($query) use ($input) {
                            if ($input['provinsi'] == 'Semua Provinsi') {
                            }else{
                                if ($input['kota'] == 'Semua Kota') {
                                    $query->where('provinsi', $input['provinsi']);
                                }else{
                                    $query->where('kota', $input['kota']);
                                }
                            }
                        })
                        ->first();

            if($buruk != null) {
                $buruk = $buruk->predikat;
            } else {
                $buruk = 0;
            }
            if($baik != null) {
                $baik = $baik->predikat;
            } else {
                $baik = 0;
            }
            $count = $baik + $buruk;
            $data = [
                0 => [
                    "alias" => "Baik",
                    "jumlah"    => $baik
                ],
                1 => [
                    "alias" => "Buruk",
                    "jumlah"=> $buruk
                ],
            ];
            // dd($data);
            return view('laporan.grafik.hasilpnpme.malaria', compact('data', 'input', 'parameter', 'provinsi', 'kota', 'count'));
        }elseif ($input['bidang'] == "7") {
            $baik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi', 'tb_registrasi.id','=','tb_kesimpulan_evaluasi.id_registrasi')
                        ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->where('tb_kesimpulan_evaluasi.siklus', $input['siklus'])
                        ->whereYear('tb_kesimpulan_evaluasi.created_at', $input['tahun'])
                        ->where(function ($query) use ($input) {
                            if ($input['provinsi'] == 'Semua Provinsi') {
                            }else{
                                if ($input['kota'] == 'Semua Kota') {
                                    $query->where('provinsi', $input['provinsi']);
                                }else{
                                    $query->where('kota', $input['kota']);
                                }
                            }
                        })
                        ->where('type','=','Syphilis')
                        ->where('ketepatan', '=', 'Baik')
                        ->count();
            $kurang = DB::table('tb_kesimpulan_evaluasi')
                        ->where('tb_kesimpulan_evaluasi.siklus', $input['siklus'])
                        ->join('tb_registrasi', 'tb_registrasi.id','=','tb_kesimpulan_evaluasi.id_registrasi')
                        ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->whereYear('tb_kesimpulan_evaluasi.created_at', $input['tahun'])
                        ->where(function ($query) use ($input) {
                            if ($input['provinsi'] == 'Semua Provinsi') {
                            }else{
                                if ($input['kota'] == 'Semua Kota') {
                                    $query->where('provinsi', $input['provinsi']);
                                }else{
                                    $query->where('kota', $input['kota']);
                                }
                            }
                        })
                        ->where('type','=','Syphilis')
                        ->where('ketepatan', '=', 'Kurang')
                        ->count();
            $tidak = DB::table('tb_kesimpulan_evaluasi')
                        ->where('tb_kesimpulan_evaluasi.siklus', $input['siklus'])
                        ->join('tb_registrasi', 'tb_registrasi.id','=','tb_kesimpulan_evaluasi.id_registrasi')
                        ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->whereYear('tb_kesimpulan_evaluasi.created_at', $input['tahun'])
                        ->where(function ($query) use ($input) {
                            if ($input['provinsi'] == 'Semua Provinsi') {
                            }else{
                                if ($input['kota'] == 'Semua Kota') {
                                    $query->where('provinsi', $input['provinsi']);
                                }else{
                                    $query->where('kota', $input['kota']);
                                }
                            }
                        })
                        ->where('type','=','Syphilis')
                        ->where('ketepatan', '=', 'Tidak dapat dinilai')
                        ->count();
            $counttp = $baik + $kurang + $tidak;
            $baikrpr = DB::table('tb_kesimpulan_evaluasi')
                        ->where('tb_kesimpulan_evaluasi.siklus', $input['siklus'])
                        ->join('tb_registrasi', 'tb_registrasi.id','=','tb_kesimpulan_evaluasi.id_registrasi')
                        ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->whereYear('tb_kesimpulan_evaluasi.created_at', $input['tahun'])
                        ->where(function ($query) use ($input) {
                            if ($input['provinsi'] == 'Semua Provinsi') {
                            }else{
                                if ($input['kota'] == 'Semua Kota') {
                                    $query->where('provinsi', $input['provinsi']);
                                }else{
                                    $query->where('kota', $input['kota']);
                                }
                            }
                        })
                        ->where('type','=','rpr-syphilis')
                        ->where('ketepatan', '=', 'Baik')
                        ->count();
            $kurangrpr = DB::table('tb_kesimpulan_evaluasi')
                        ->where('tb_kesimpulan_evaluasi.siklus', $input['siklus'])
                        ->join('tb_registrasi', 'tb_registrasi.id','=','tb_kesimpulan_evaluasi.id_registrasi')
                        ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->whereYear('tb_kesimpulan_evaluasi.created_at', $input['tahun'])
                        ->where(function ($query) use ($input) {
                            if ($input['provinsi'] == 'Semua Provinsi') {
                            }else{
                                if ($input['kota'] == 'Semua Kota') {
                                    $query->where('provinsi', $input['provinsi']);
                                }else{
                                    $query->where('kota', $input['kota']);
                                }
                            }
                        })
                        ->where('type','=','rpr-syphilis')
                        ->where('ketepatan', '=', 'Kurang')
                        ->count();
            $tidakrpr = DB::table('tb_kesimpulan_evaluasi')
                        ->where('tb_kesimpulan_evaluasi.siklus', $input['siklus'])
                        ->join('tb_registrasi', 'tb_registrasi.id','=','tb_kesimpulan_evaluasi.id_registrasi')
                        ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->whereYear('tb_kesimpulan_evaluasi.created_at', $input['tahun'])
                        ->where(function ($query) use ($input) {
                            if ($input['provinsi'] == 'Semua Provinsi') {
                            }else{
                                if ($input['kota'] == 'Semua Kota') {
                                    $query->where('provinsi', $input['provinsi']);
                                }else{
                                    $query->where('kota', $input['kota']);
                                }
                            }
                        })
                        ->where('type','=','rpr-syphilis')
                        ->where('ketepatan', '=', 'Tidak dapat dinilai')
                        ->count();
            $countrpr = $baikrpr + $kurangrpr + $tidakrpr;
            return view('laporan.grafik.hasilpnpme.syphilis', compact('baik','kurang','tidak','baikrpr','kurangrpr','tidakrpr', 'input', 'parameter', 'provinsi', 'kota', 'counttp', 'countrpr'));
        }else{
            $bidangna = Db::table('sub_bidang')->where('id', $input['bidang'])->first();
            $baik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi', 'tb_registrasi.id','=','tb_kesimpulan_evaluasi.id_registrasi')
                        ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->where('tb_kesimpulan_evaluasi.siklus', $input['siklus'])
                        ->whereYear('tb_kesimpulan_evaluasi.created_at', $input['tahun'])
                        ->where(function ($query) use ($input) {
                            if ($input['provinsi'] == 'Semua Provinsi') {
                            }else{
                                if ($input['kota'] == 'Semua Kota') {
                                    $query->where('provinsi', $input['provinsi']);
                                }else{
                                    $query->where('kota', $input['kota']);
                                }
                            }
                        })
                        ->where('bidang','=', $input['bidang'])
                        ->where('ketepatan', '=', 'Baik')
                        ->count();
            $kurang = DB::table('tb_kesimpulan_evaluasi')
                        ->where('tb_kesimpulan_evaluasi.siklus', $input['siklus'])
                        ->join('tb_registrasi', 'tb_registrasi.id','=','tb_kesimpulan_evaluasi.id_registrasi')
                        ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->whereYear('tb_kesimpulan_evaluasi.created_at', $input['tahun'])
                        ->where(function ($query) use ($input) {
                            if ($input['provinsi'] == 'Semua Provinsi') {
                            }else{
                                if ($input['kota'] == 'Semua Kota') {
                                    $query->where('provinsi', $input['provinsi']);
                                }else{
                                    $query->where('kota', $input['kota']);
                                }
                            }
                        })
                        ->where('bidang','=', $input['bidang'])
                        ->where('ketepatan', '=', 'Kurang')
                        ->count();
            $tidak = DB::table('tb_kesimpulan_evaluasi')
                        ->where('tb_kesimpulan_evaluasi.siklus', $input['siklus'])
                        ->join('tb_registrasi', 'tb_registrasi.id','=','tb_kesimpulan_evaluasi.id_registrasi')
                        ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->whereYear('tb_kesimpulan_evaluasi.created_at', $input['tahun'])
                        ->where(function ($query) use ($input) {
                            if ($input['provinsi'] == 'Semua Provinsi') {
                            }else{
                                if ($input['kota'] == 'Semua Kota') {
                                    $query->where('provinsi', $input['provinsi']);
                                }else{
                                    $query->where('kota', $input['kota']);
                                }
                            }
                        })
                        ->where('bidang','=', $input['bidang'])
                        ->where('ketepatan', '=', 'Tidak dapat dinilai')
                        ->count();
            $count = $baik + $kurang + $tidak;

            return view('laporan.grafik.hasilpnpme.imun', compact('baik','kurang','tidak', 'bidangna', 'input', 'parameter', 'provinsi', 'kota', 'count'));
        }
    }
}
<?php
namespace App\Http\Controllers;
use DB;
use Input;
use PDF;
use Carbon\Carbon;

use App\HpHeader;
use App\Parameter;
use App\MetodePemeriksaan;
use App\HpDetail;
use App\ZScore;
use App\ZScoreAlat;
use App\ZScoreMetode;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\kimiaklinik as Kimiaklinik;
use App\daftar as Daftar;
use App\register as Register;
use App\TbReagenImunologi as ReagenImunologi;
use App\TbHp;
use App\Rujukanurinalisa;
use Redirect;
use Validator;
use Session;
use App\User;
use App\SdMedian;
use App\SdMedianMetode;
use App\UploadLaporanIdentifikasi;
use App\Ring;
use App\CatatanImun;
use App\SdMedianAlat;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Excel;
use App\statusbakteri;
use App\UploadLaporan;


class LapAkhirEvaluasiController extends Controller
{
    public function laporanakhirhematologi(\Illuminate\Http\Request $request,$id)
    {
        if(count($request->get('tanggal'))){
            $tanggal = $request->get('tanggal');
            $siklus = $request->get('siklus');
        }else{
            $tanggal = Session::get('tanggal');
            $siklus = Session::get('siklus');
        }
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        // dd($tanggal);
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.id','=', $id)
                ->where('tb_registrasi.bidang',1)
                ->first();
        $t = date('Y');

        $ttd = DB::table('tb_ttd_evaluasi')
                ->where('tahun', $tahun)
                ->where('siklus', $siklus)
                ->where('bidang', '=', '1')
                ->where(function($query) use ($q1, $tahun, $siklus){
                    if ($q1->periode == 2 && $tahun == 2019 && $siklus == 1) {
                        $query->where('periode', '=', '2');
                    }else{
                        $query->whereNull('periode');
                    }
                })
                ->first();

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_hema', compact('data','datas','tanggal','siklus','tahun','ttd'))
            ->setPaper([0, 0, '612', '936'], 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi HEMATOLOGI.pdf');
    }

    public function datagrafikdistribusihasilpeserta($id) {
        $input = [
            'tahun' => "2018",
            'type' => "a",
            'siklus' => "1",
            '_token' => "v9qcOvPty9UqErpVGpzyNwEpkexCNLFOIhGeoLYP",
            'x' => "a",
            'y' => "1"
        ];

        $hematologis = DB::table('parameter')->where('kategori','hematologi')->get();

        foreach($hematologis as $hematologi) {
            $parameter_id = $hematologi->id;

            $siklus = $input['siklus'];
            $typeHeader = 'a';

            $dataReg[$parameter_id] = DB::table('tb_registrasi')->where('tb_registrasi.id',$id)
                        ->leftJoin('hp_headers',function($join)use($typeHeader,$siklus){
                            $join->on('tb_registrasi.id','=','hp_headers.id_registrasi')->where('hp_headers.type',$typeHeader)->where('hp_headers.siklus',$siklus);
                        })
                        ->leftJoin('hp_details',function($join) use($parameter_id){
                            $join->on('hp_headers.id','=','hp_details.hp_header_id');
                            $join->where('hp_details.parameter_id','=',$parameter_id);
                        })
                        ->where('tb_registrasi.id', '=', $id)
                        ->first();
            $parameter[$parameter_id] = DB::table('parameter')->where('id','=',$parameter_id)->first();
            $instrumen[$parameter_id] = DB::table('tb_instrumen')->where('id','=',$dataReg[$parameter_id]->alat)->first();
            $metode[$parameter_id] = DB::table('metode_pemeriksaan')->where('id','=',$dataReg[$parameter_id]->kode_metode_pemeriksaan)->first();
            $grafik[$parameter_id] = DB::select("
                SELECT
                    tb_ring.ring1,
                    tb_ring.ring2,
                    tb_ring.ring3,
                    tb_ring.ring4,
                    tb_ring.ring5,
                    SUM(CASE WHEN hp_details.hasil_pemeriksaan < ring1 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_bawah_param,
                    SUM(CASE WHEN hp_details.hasil_pemeriksaan >= ring1 AND hp_details.hasil_pemeriksaan < ring2 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_1_param,
                    SUM(CASE WHEN hp_details.hasil_pemeriksaan >= ring2 AND hp_details.hasil_pemeriksaan < ring3 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_2_param,
                    SUM(CASE WHEN hp_details.hasil_pemeriksaan >= ring3 AND hp_details.hasil_pemeriksaan < ring4 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_3_param,
                    SUM(CASE WHEN hp_details.hasil_pemeriksaan >= ring4 AND hp_details.hasil_pemeriksaan <= ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_4_param,
                    SUM(CASE WHEN hp_details.hasil_pemeriksaan > ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_atas_param,
                    SUM(CASE WHEN hp_details.alat = ".$dataReg[$parameter_id]->alat." AND  hp_details.hasil_pemeriksaan < ring1 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_bawah_alat,
                    SUM(CASE WHEN hp_details.alat = ".$dataReg[$parameter_id]->alat." AND  hp_details.hasil_pemeriksaan >= ring1 AND hp_details.hasil_pemeriksaan < ring2 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_1_alat,
                    SUM(CASE WHEN hp_details.alat = ".$dataReg[$parameter_id]->alat." AND  hp_details.hasil_pemeriksaan >= ring2 AND hp_details.hasil_pemeriksaan < ring3 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_2_alat,
                    SUM(CASE WHEN hp_details.alat = ".$dataReg[$parameter_id]->alat." AND  hp_details.hasil_pemeriksaan >= ring3 AND hp_details.hasil_pemeriksaan < ring4 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_3_alat,
                    SUM(CASE WHEN hp_details.alat = ".$dataReg[$parameter_id]->alat." AND  hp_details.hasil_pemeriksaan >= ring4 AND hp_details.hasil_pemeriksaan <= ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_4_alat,
                    SUM(CASE WHEN hp_details.alat = ".$dataReg[$parameter_id]->alat." AND  hp_details.hasil_pemeriksaan > ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_atas_alat,
                    SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg[$parameter_id]->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan < ring1 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_bawah_metode,
                    SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg[$parameter_id]->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= ring1 AND hp_details.hasil_pemeriksaan < ring2 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_1_metode,
                    SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg[$parameter_id]->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= ring2 AND hp_details.hasil_pemeriksaan < ring3 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_2_metode,
                    SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg[$parameter_id]->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= ring3 AND hp_details.hasil_pemeriksaan < ring4 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_3_metode,
                    SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg[$parameter_id]->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= ring4 AND hp_details.hasil_pemeriksaan <= ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_4_metode,
                    SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg[$parameter_id]->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan > ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_atas_metode
                FROM 	tb_ring
                LEFT JOIN (
                    SELECT hp_details.id,hp_details.parameter_id,hp_headers.type,hp_headers.siklus,tb_registrasi.created_at,hp_details.hasil_pemeriksaan,hp_details.alat,hp_details.kode_metode_pemeriksaan,hp_headers.id_registrasi FROM tb_registrasi
                    INNER JOIN hp_headers ON hp_headers.id_registrasi = tb_registrasi.id
                    INNER JOIN hp_details ON hp_headers.id = hp_details.hp_header_id
                ) as hp_details ON hp_details.parameter_id = tb_ring.parameter
                WHERE
                tb_ring.parameter =".$dataReg[$parameter_id]->parameter_id."
                AND
                tb_ring.siklus = ".$siklus."
                AND
                tb_ring.tipe = '".$typeHeader."'
                AND
                tb_ring.form = 'hematologi'
                AND hp_details.type = '".$typeHeader."'
                AND hp_details.siklus = ".$siklus."
                ");

            $median[$parameter_id] = DB::table('tb_sd_median')
                ->where('parameter', $input['parameter'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $typeHeader)
                ->where('tahun', $input['tahun'])
                ->first();

            $medianalat[$parameter_id] = DB::table('tb_sd_median_alat')
                ->where('alat', $dataReg[$parameter_id]->alat)
                ->where('siklus', $input['siklus'])
                ->where('tipe', $typeHeader)
                ->where('tahun', $input['tahun'])
                ->first();

            $medianmetode[$parameter_id] = DB::table('tb_sd_median_metode')
                ->where('parameter', $input['parameter'])
                ->where('metode', $dataReg[$parameter_id]->kode_metode_pemeriksaan)
                ->where('siklus', $input['siklus'])
                ->where('tipe', $typeHeader)
                ->where('tahun', $input['tahun'])
                ->first();

            // B
            $siklus2 = $input['siklus'];
            $typeHeader2 = 'b';
            $dataReg2[$parameter_id] = DB::table('tb_registrasi')->where('tb_registrasi.id',$id)
                        ->leftJoin('hp_headers',function($join)use($typeHeader2,$siklus){
                            $join->on('tb_registrasi.id','=','hp_headers.id_registrasi')->where('hp_headers.type',$typeHeader2)->where('hp_headers.siklus',$siklus);
                        })
                        ->leftJoin('hp_details',function($join) use($parameter_id){
                            $join->on('hp_headers.id','=','hp_details.hp_header_id');
                            $join->where('hp_details.parameter_id','=',$parameter_id);
                        })
                        ->where('tb_registrasi.id', '=', $id)
                        ->first();
            $parameter[$parameter_id] = DB::table('parameter')->where('id','=',$parameter_id)->first();
            $instrumen[$parameter_id] = DB::table('tb_instrumen')->where('id','=',$dataReg2[$parameter_id]->alat)->first();
            $metode2[$parameter_id] = DB::table('metode_pemeriksaan')->where('id','=',$dataReg2[$parameter_id]->kode_metode_pemeriksaan)->first();
            $grafik2[$parameter_id] = DB::select("
                SELECT
                    tb_ring.ring1,
                    tb_ring.ring2,
                    tb_ring.ring3,
                    tb_ring.ring4,
                    tb_ring.ring5,
                    SUM(CASE WHEN hp_details.hasil_pemeriksaan < ring1 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_bawah_param,
                    SUM(CASE WHEN hp_details.hasil_pemeriksaan >= ring1 AND hp_details.hasil_pemeriksaan < ring2 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_1_param,
                    SUM(CASE WHEN hp_details.hasil_pemeriksaan >= ring2 AND hp_details.hasil_pemeriksaan < ring3 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_2_param,
                    SUM(CASE WHEN hp_details.hasil_pemeriksaan >= ring3 AND hp_details.hasil_pemeriksaan < ring4 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_3_param,
                    SUM(CASE WHEN hp_details.hasil_pemeriksaan >= ring4 AND hp_details.hasil_pemeriksaan <= ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_4_param,
                    SUM(CASE WHEN hp_details.hasil_pemeriksaan > ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_atas_param,
                    SUM(CASE WHEN hp_details.alat = ".$dataReg2[$parameter_id]->alat." AND  hp_details.hasil_pemeriksaan < ring1 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_bawah_alat,
                    SUM(CASE WHEN hp_details.alat = ".$dataReg2[$parameter_id]->alat." AND  hp_details.hasil_pemeriksaan >= ring1 AND hp_details.hasil_pemeriksaan < ring2 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_1_alat,
                    SUM(CASE WHEN hp_details.alat = ".$dataReg2[$parameter_id]->alat." AND  hp_details.hasil_pemeriksaan >= ring2 AND hp_details.hasil_pemeriksaan < ring3 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_2_alat,
                    SUM(CASE WHEN hp_details.alat = ".$dataReg2[$parameter_id]->alat." AND  hp_details.hasil_pemeriksaan >= ring3 AND hp_details.hasil_pemeriksaan < ring4 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_3_alat,
                    SUM(CASE WHEN hp_details.alat = ".$dataReg2[$parameter_id]->alat." AND  hp_details.hasil_pemeriksaan >= ring4 AND hp_details.hasil_pemeriksaan <= ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_4_alat,
                    SUM(CASE WHEN hp_details.alat = ".$dataReg2[$parameter_id]->alat." AND  hp_details.hasil_pemeriksaan > ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_atas_alat,
                    SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg2[$parameter_id]->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan < ring1 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_bawah_metode,
                    SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg2[$parameter_id]->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= ring1 AND hp_details.hasil_pemeriksaan < ring2 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_1_metode,
                    SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg2[$parameter_id]->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= ring2 AND hp_details.hasil_pemeriksaan < ring3 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_2_metode,
                    SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg2[$parameter_id]->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= ring3 AND hp_details.hasil_pemeriksaan < ring4 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_3_metode,
                    SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg2[$parameter_id]->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= ring4 AND hp_details.hasil_pemeriksaan <= ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_4_metode,
                    SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg2[$parameter_id]->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan > ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_atas_metode
                FROM 	tb_ring
                LEFT JOIN (
                    SELECT hp_details.id,hp_details.parameter_id,hp_headers.type,hp_headers.siklus,tb_registrasi.created_at,hp_details.hasil_pemeriksaan,hp_details.alat,hp_details.kode_metode_pemeriksaan,hp_headers.id_registrasi FROM tb_registrasi
                    INNER JOIN hp_headers ON hp_headers.id_registrasi = tb_registrasi.id
                    INNER JOIN hp_details ON hp_headers.id = hp_details.hp_header_id
                ) as hp_details ON hp_details.parameter_id = tb_ring.parameter
                WHERE
                tb_ring.parameter =".$dataReg2[$parameter_id]->parameter_id."
                AND
                tb_ring.siklus = ".$siklus."
                AND
                tb_ring.tipe = '".$typeHeader2."'
                AND
                tb_ring.form = 'hematologi'
                AND hp_details.type = '".$typeHeader2."'
                AND hp_details.siklus = ".$siklus."
                ");

            $median2[$parameter_id] = DB::table('tb_sd_median')
                ->where('parameter', $input['parameter'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $typeHeader2)
                ->where('tahun', $input['tahun'])
                ->first();

            $medianalat2[$parameter_id] = DB::table('tb_sd_median_alat')
                ->where('alat', $dataReg2[$parameter_id]->alat)
                ->where('siklus', $input['siklus'])
                ->where('tipe', $typeHeader2)
                ->where('tahun', $input['tahun'])
                ->first();

            $medianmetode2[$parameter_id] = DB::table('tb_sd_median_metode')
                ->where('parameter', $input['parameter'])
                ->where('metode', $dataReg2[$parameter_id]->kode_metode_pemeriksaan)
                ->where('siklus', $input['siklus'])
                ->where('tipe', $typeHeader2)
                ->where('tahun', $input['tahun'])
                ->first();
        }

        // $pdf = PDF::loadview('laporan/akhir/data_grafik_distribusi_hasil_peserta', compact('hematologis', 'dataReg','parameter','data','ring', 'median','dataalat','ringalat', 'medianalat', 'datametode', 'ringmetode', 'medianmetode','reg','grafik','instrumen','metode', 'dataReg2','parameter2','data2','ring2', 'median2','dataalat2','ringalat2', 'medianalat2', 'datametode2', 'ringmetode2', 'medianmetode2','reg2','grafik2','instrumen2','metode2'))
        //     ->setPaper('a4', 'potrait')
        //     ->setwarnings(false);

        // return $pdf->stream('Grafik Distribusi Hasil Peserta.pdf');

        return view('laporan/akhir/data_grafik_distribusi_hasil_peserta', compact('hematologis', 'dataReg','parameter','data','ring', 'median','dataalat','ringalat', 'medianalat', 'datametode', 'ringmetode', 'medianmetode','reg','grafik','instrumen','metode', 'dataReg2','parameter2','data2','ring2', 'median2','dataalat2','ringalat2', 'medianalat2', 'datametode2', 'ringmetode2', 'medianmetode2','reg2','grafik2','instrumen2','metode2'));
    }

    public function laporanakhirhematologipenutup(\Illuminate\Http\Request $request,$id)
    {
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutuphema', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup Hema.pdf');
    }

    public function laporanakhirbacpenutup(\Illuminate\Http\Request $request,$id)
    {
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutupbac', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup BAC.pdf');
    }

    public function laporanakhirbtapenutup(\Illuminate\Http\Request $request,$id)
    {
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutupbta', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup BTA.pdf');
    }

    public function laporanakhirhbspenutup(\Illuminate\Http\Request $request,$id)
    {
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutuphbs', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup HBS.pdf');
    }

    public function laporanakhirhcvpenutup(\Illuminate\Http\Request $request,$id)
    {
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutuphcv', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup HCV.pdf');
    }

    public function laporanakhirhivpenutup(\Illuminate\Http\Request $request,$id)
    {
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutuphiv', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup HIV.pdf');
    }

    public function laporanakhirkaipenutup(\Illuminate\Http\Request $request,$id)
    {
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutupkai', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup KAI.pdf');
    }

    public function laporanakhirkatpenutup(\Illuminate\Http\Request $request,$id)
    {
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutupkat', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup KAT.pdf');
    }

    public function laporanakhirkimiaklinikpenutup(\Illuminate\Http\Request $request,$id)
    {
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutupkimiaklinik', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup KK.pdf');
    }

    public function laporanakhirmalpenutup(\Illuminate\Http\Request $request,$id)
    {
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutupmal', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup MAL.pdf');
    }

    public function laporanakhirsifpenutup(\Illuminate\Http\Request $request,$id)
    {
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutupsif', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup SIF.pdf');
    }

    public function laporanakhirtccpenutup(\Illuminate\Http\Request $request,$id)
    {
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutuptcc', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup TCC.pdf');
    }



    public function laporanakhirhematologidownload(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $dd = $request->tanggal;
            $siklus = $request->get('y');
            if (!empty($dd)) {
                $request->session()->put('tanggal', $dd);
                $request->session()->put('siklus', $siklus);
            } else {
                $dd = $request->session()->get('tanggal');
                $siklus = $request->session()->get('siklus');
            }
            DB::statement(DB::raw("set @rownum=0"));
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.siklus', $siklus)
                ->where('upload_lap_akhir.tahun', $dd)
                ->where('upload_lap_akhir.parameter','=','1');
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/hematologi');

    }

    public function laporanakhirbac(\Illuminate\Http\Request $request,$id)
    {
        if(count($request->get('tanggal'))){
            $tanggal = $request->get('tanggal');
            $siklus = $request->get('siklus');
        }else{
            $tanggal = Session::get('tanggal');
            $siklus = Session::get('siklus');
        }
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.id','=', $id)
                ->where('tb_registrasi.bidang',13)
                ->first();
        // dd($data);
        $t = date('Y');

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $ttd = DB::table('tb_ttd_evaluasi')
                ->where('tahun', $tahun)
                ->where('siklus', $siklus)
                ->where('bidang', '=', '13')
                ->first();

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_bac', compact('data','datas','tanggal','siklus','ttd'))
            ->setPaper([0, 0, '612', '936'], 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri BAC.pdf');
    }
    public function laporanakhirbacdownload(\Illuminate\Http\Request $request,$id)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = UploadLaporanIdentifikasi::select( DB::raw('upload_lap_akhir_identifikasi.*, tb_registrasi.id as idregistrasi, perusahaan.nama_lab, sub_bidang.alias, @rownum := @rownum +1 as rownum'))
                    ->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir_identifikasi.parameter')
                    ->join('tb_registrasi', 'tb_registrasi.id', '=', 'upload_lap_akhir_identifikasi.id_registrasi')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->where('upload_lap_akhir_identifikasi.id_registrasi','=', $id)
                    ->orderBy('id','ASC')->get();
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/bac');

    }

     public function laporanakhirbacdownload1(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
             $dd = $request->tanggal;
            $siklus = $request->get('y');
            if (!empty($dd)) {
                $request->session()->put('tanggal', $dd);
                $request->session()->put('siklus', $siklus);
            } else {
                $dd = $request->session()->get('tanggal');
                $siklus = $request->session()->get('siklus');
            }
            DB::statement(DB::raw("set @rownum=0")); 
           $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','13')
                ->where('upload_lap_akhir.siklus','=',$siklus)
                ->where('upload_lap_akhir.tahun','=',$dd);
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/bac');

    }

    public function laporanakhirkimiaklinikdownload(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $dd = $request->tanggal;
            $siklus = $request->get('y');
            if (!empty($dd)) {
                $request->session()->put('tanggal', $dd);
                $request->session()->put('siklus', $siklus);
            } else {
                $dd = $request->session()->get('tanggal');
                $siklus = $request->session()->get('siklus');
            }
            DB::statement(DB::raw("set @rownum=0"));
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.siklus', $siklus)
                ->where('upload_lap_akhir.tahun', $dd)
                ->where('upload_lap_akhir.parameter','=','2');
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/kimiaklinik');

    }


    public function laporanakhirbta(\Illuminate\Http\Request $request,$id)
    {
        if(count($request->get('tanggal'))){
            $tanggal = $request->get('tanggal');
            $siklus = $request->get('siklus');
        }else{
            $tanggal = Session::get('tanggal');
            $siklus = Session::get('siklus');
        }
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.id','=', $id)
                ->where('tb_registrasi.bidang',4)
                ->first();
        $t = date('Y');

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $ttd = DB::table('tb_ttd_evaluasi')
                ->where('tahun', $tahun)
                ->where('siklus', $siklus)
                ->where('bidang', '=', '4')
                ->first();

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_bta', compact('data','datas','tanggal','siklus','date'))
            ->setPaper([0, 0, '612', '936'], 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi BTA.pdf');
    }

     public function laporanakhirbtadownload(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $dd = $request->tanggal;
            $siklus = $request->get('y');
            if (!empty($dd)) {
                $request->session()->put('tanggal', $dd);
                $request->session()->put('siklus', $siklus);
            } else {
                $dd = $request->session()->get('tanggal');
                $siklus = $request->session()->get('siklus');
            }
            DB::statement(DB::raw("set @rownum=0"));
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.siklus','=', $siklus)
                ->where('upload_lap_akhir.tahun','=', $dd)
                ->where('upload_lap_akhir.parameter','=','4');
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/bta');

    }

    public function laporanakhirhbs(\Illuminate\Http\Request $request,$id)
    {
        if(count($request->get('tanggal'))){
            $tanggal = $request->get('tanggal');
            $siklus = $request->get('siklus');
        }else{
            $tanggal = Session::get('tanggal');
            $siklus = Session::get('siklus');
        }
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.id','=', $id)
                ->where('tb_registrasi.bidang',8)
                ->first();
        $t = date('Y');

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $ttd = DB::table('tb_ttd_evaluasi')
                ->where('tahun', $tahun)
                ->where('siklus', $siklus)
                ->where('bidang', '=', '8')
                ->first();

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_hbs', compact('data','datas','tanggal','siklus','ttd'))
            ->setPaper([0, 0, '612', '936'], 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi HBS.pdf');
    }

      public function laporanakhirhbsdownload(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $dd = $request->tanggal;
            $siklus = $request->get('y');
            if (!empty($dd)) {
                $request->session()->put('tanggal', $dd);
                $request->session()->put('siklus', $siklus);
            } else {
                $dd = $request->session()->get('tanggal');
                $siklus = $request->session()->get('siklus');
            }
            DB::statement(DB::raw("set @rownum=0"));
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.siklus', '=', $siklus)
                ->where('upload_lap_akhir.tahun', '=', $dd)
                ->where('upload_lap_akhir.parameter','=','8');
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/hbs');

    }

    public function laporanakhirhcv(\Illuminate\Http\Request $request,$id)
    {
        if(count($request->get('tanggal'))){
            $tanggal = $request->get('tanggal');
            $siklus = $request->get('siklus');
        }else{
            $tanggal = Session::get('tanggal');
            $siklus = Session::get('siklus');
        }
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.id','=', $id)
                ->where('tb_registrasi.bidang',9)
                ->first();

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $ttd = DB::table('tb_ttd_evaluasi')
                ->where('tahun', $tahun)
                ->where('siklus', $siklus)
                ->where('bidang', '=', '9')
                ->first();

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_hcv', compact('data','datas','tanggal','siklus','ttd'))
            ->setPaper([0, 0, '612', '936'], 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi HCV.pdf');
    }

    public function laporanakhirhcvdownload(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $dd = $request->tanggal;
            $siklus = $request->get('y');
            if (!empty($dd)) {
                $request->session()->put('tanggal', $dd);
                $request->session()->put('siklus', $siklus);
            } else {
                $dd = $request->session()->get('tanggal');
                $siklus = $request->session()->get('siklus');
            }
            DB::statement(DB::raw("set @rownum=0"));
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.siklus', '=', $siklus)
                ->where('upload_lap_akhir.tahun', '=', $dd)
                ->where('upload_lap_akhir.parameter','=','9');
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/hcv');

    }

    public function laporanakhirhiv(\Illuminate\Http\Request $request,$id)
    {
        if(count($request->get('tanggal'))){
            $tanggal = $request->get('tanggal');
            $siklus = $request->get('siklus');
        }else{
            $tanggal = Session::get('tanggal');
            $siklus = Session::get('siklus');
        }
        // dd($datas);
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.id','=', $id)
                ->where('tb_registrasi.bidang',6)
                ->first();

        $t = date('Y');

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $ttd = DB::table('tb_ttd_evaluasi')
                ->where('tahun', $tahun)
                ->where('siklus', $siklus)
                ->where('bidang', '=', '6')
                ->first();

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_hiv', compact('datas','data','tanggal','siklus','ttd'))
            ->setPaper([0, 0, '612', '936'], 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi HIV.pdf');
    }

     public function laporanakhirhivdownload(\Illuminate\Http\Request $request,$id=null)
    {
        if($request->ajax()){
            $dd = $request->tanggal;
            $siklus = $request->get('y');
            if (!empty($dd)) {
                $request->session()->put('tanggal', $dd);
                $request->session()->put('siklus', $siklus);
            } else {
                $dd = $request->session()->get('tanggal');
                $siklus = $request->session()->get('siklus');
            }
            DB::statement(DB::raw("set @rownum=0"));
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.siklus', '=', $siklus)
                ->where('upload_lap_akhir.tahun', '=', $dd)
                ->where('upload_lap_akhir.parameter','=','6');
            if ($id == '9') { 
               $datas =  $datas->where('upload_lap_akhir.badan_usaha','=',1); 
            }else{
                $datas =  $datas->where('upload_lap_akhir.badan_usaha','=',0); 
            }
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/hiv');

    }

    public function laporanakhirkai(\Illuminate\Http\Request $request,$id)
    {
        if(count($request->get('tanggal'))){
            $tanggal = $request->get('tanggal');
            $siklus = $request->get('siklus');
        }else{
            $tanggal = Session::get('tanggal');
            $siklus = Session::get('siklus');
        }
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.id','=', $id)
                ->where('tb_registrasi.bidang',11)
                ->first();
        $t = date('Y');

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $ttd = DB::table('tb_ttd_evaluasi')
                ->where('tahun', $tahun)
                ->where('siklus', $siklus)
                ->where('bidang', '=', '11')
                ->first();

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_kai', compact('data','datas','tanggal','siklus','ttd'))
            ->setPaper([0, 0, '612', '936'], 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Kimia Air.pdf');
    }

    public function laporanakhirkaidownload(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
             $dd = $request->tanggal;
            $siklus = $request->get('y');
            if (!empty($dd)) {
                $request->session()->put('tanggal', $dd);
                $request->session()->put('siklus', $siklus);
            } else {
                $dd = $request->session()->get('tanggal');
                $siklus = $request->session()->get('siklus');
            }
            DB::statement(DB::raw("set @rownum=0"));
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','11')
                ->where('upload_lap_akhir.siklus','=',$siklus)
                ->where('upload_lap_akhir.tahun','=',$dd);
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/kai');

    }

    public function laporanakhirkat(\Illuminate\Http\Request $request,$id)
    {
        if(count($request->get('tanggal'))){
            $tanggal = $request->get('tanggal');
            $siklus = $request->get('siklus');
        }else{
            $tanggal = Session::get('tanggal');
            $siklus = Session::get('siklus');
        }
        // dd($datas);
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.id','=', $id)
                ->where('tb_registrasi.bidang',12)
                ->first();
        $t = date('Y');

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $ttd = DB::table('tb_ttd_evaluasi')
                ->where('tahun', $tahun)
                ->where('siklus', $siklus)
                ->where('bidang', '=', '12')
                ->first();

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_kat', compact('data','datas','tanggal','siklus','ttd'))
            ->setPaper([0, 0, '612', '936'], 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Kimia Air Terbatas.pdf');
    }

    public function laporanakhirkatdownload(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $dd = $request->tanggal;
            $siklus = $request->get('y');
            if (!empty($dd)) {
                $request->session()->put('tanggal', $dd);
                $request->session()->put('siklus', $siklus);
            } else {
                $dd = $request->session()->get('tanggal');
                $siklus = $request->session()->get('siklus');
            }
            DB::statement(DB::raw("set @rownum=0"));
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','12')
                ->where('upload_lap_akhir.siklus','=',$siklus)
                ->where('upload_lap_akhir.tahun','=',$dd);
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/kat');

    }


    public function laporanakhirkimiaklinik(\Illuminate\Http\Request $request,$id)
    {
        if(count($request->get('tanggal'))){
            $tanggal = $request->get('tanggal');
            $siklus = $request->get('siklus');
        }else{
            $tanggal = Session::get('tanggal');
            $siklus = Session::get('siklus');
        }
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.id','=', $id)
                ->where('tb_registrasi.bidang',2)
                ->first();

        $ttd = DB::table('tb_ttd_evaluasi')
                ->where('tahun', $tahun)
                ->where('siklus', $siklus)
                ->where('bidang', '=', '2')
                ->where(function($query) use ($q1, $tahun, $siklus){
                    if ($q1->periode == 2 && $tahun == 2019 && $siklus == 1) {
                        $query->where('periode', '=', '2');
                    }else{
                        $query->whereNull('periode');
                    }
                })
                ->first();

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_kimiaklinik', compact('data','datas','tanggal','siklus','tahun','ttd'))
            ->setPaper([0, 0, '612', '936'], 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Kimia Klinik.pdf');
    }

    public function laporanakhirmal(\Illuminate\Http\Request $request,$id)
    {
        if(count($request->get('tanggal'))){
            $tanggal = $request->get('tanggal');
            $siklus = $request->get('siklus');
        }else{
            $tanggal = Session::get('tanggal');
            $siklus = Session::get('siklus');
        }
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.id','=', $id)
                ->where('tb_registrasi.bidang',10)
                ->first();
        $t = date('Y');

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $ttd = DB::table('tb_ttd_evaluasi')
                ->where('tahun', $tahun)
                ->where('siklus', $siklus)
                ->where('bidang', '=', '10')
                ->first();

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_mal', compact('data','datas','tanggal','siklus','ttd'))
            ->setPaper([0, 0, '612', '936'], 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi MAL.pdf');
    }

     public function laporanakhirmaldownload(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $dd = $request->tanggal;
            $siklus = $request->get('y');
            if (!empty($dd)) {
                $request->session()->put('tanggal', $dd);
                $request->session()->put('siklus', $siklus);
            } else {
                $dd = $request->session()->get('tanggal');
                $siklus = $request->session()->get('siklus');
            }
            DB::statement(DB::raw("set @rownum=0"));
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.siklus', '=', $siklus)
                ->where('upload_lap_akhir.tahun', '=', $dd)
                ->where('upload_lap_akhir.parameter','=','10');
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/mal');

    }

    public function laporanakhirsif(\Illuminate\Http\Request $request,$id)
    {
        if(count($request->get('tanggal'))){
            $tanggal = $request->get('tanggal');
            $siklus = $request->get('siklus');
        }else{
            $tanggal = Session::get('tanggal');
            $siklus = Session::get('siklus');
        }

        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.id','=', $id)
                ->where('tb_registrasi.bidang',7)
                ->first();
        $t = date('Y');

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $ttd = DB::table('tb_ttd_evaluasi')
                ->where('tahun', $tahun)
                ->where('siklus', $siklus)
                ->where('bidang', '=', '7')
                ->first();

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_sif', compact('data','datas','tanggal','siklus','ttd'))
            ->setPaper([0, 0, '612', '936'], 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi SIF.pdf');
    }

      public function laporanakhirsifdownload(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $dd = $request->tanggal;
            $siklus = $request->get('y');
            if (!empty($dd)) {
                $request->session()->put('tanggal', $dd);
                $request->session()->put('siklus', $siklus);
            } else {
                $dd = $request->session()->get('tanggal');
                $siklus = $request->session()->get('siklus');
            }
            DB::statement(DB::raw("set @rownum=0"));
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.siklus', '=', $siklus)
                ->where('upload_lap_akhir.tahun', '=', $dd)
                ->where('upload_lap_akhir.parameter','=','7');
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/sif');

    }

    public function laporanakhirtcc(\Illuminate\Http\Request $request,$id)
    {
        if(count($request->get('tanggal'))){
            $tanggal = $request->get('tanggal');
            $siklus = $request->get('siklus');
        }else{
            $tanggal = Session::get('tanggal');
            $siklus = Session::get('siklus');
        }

        // dd($datas);
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.id','=', $id)
                ->where('tb_registrasi.bidang',5)
                ->first();
        $t = date('Y');
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $ttd = DB::table('tb_ttd_evaluasi')
                ->where('tahun', $tahun)
                ->where('siklus', $siklus)
                ->where('bidang', '=', '5')
                ->first();

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_tcc', compact('data','datas','tanggal','siklus','ttd'))
            ->setPaper([0, 0, '612', '936'], 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi TCC.pdf');
    }

    public function laporanakhirtccdownload(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $dd = $request->tanggal;
            $siklus = $request->get('y');
            if (!empty($dd)) {
                $request->session()->put('tanggal', $dd);
                $request->session()->put('siklus', $siklus);
            } else {
                $dd = $request->session()->get('tanggal');
                $siklus = $request->session()->get('siklus');
            }
            DB::statement(DB::raw("set @rownum=0"));
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.siklus', '=', $siklus)
                ->where('upload_lap_akhir.tahun', '=', $dd)
                ->where('upload_lap_akhir.parameter','=','5');
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/tcc');

    }

    public function laporanakhiruri(\Illuminate\Http\Request $request,$id)
    {
        if(count($request->get('tanggal'))){
            $tanggal = $request->get('tanggal');
            $siklus = $request->get('siklus');
        }else{
            $tanggal = Session::get('tanggal');
            $siklus = Session::get('siklus');
        }
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.id','=', $id)
                ->where('tb_registrasi.bidang',3)
                ->first();

        $ttd = DB::table('tb_ttd_evaluasi')
                ->where('tahun', $tahun)
                ->where('siklus', $siklus)
                ->where('bidang', '=', '3')
                ->where(function($query) use ($q1, $tahun, $siklus){
                    if ($q1->periode == 2 && $tahun == 2019 && $siklus == 1) {
                        $query->where('periode', '=', '2');
                    }else{
                        $query->whereNull('periode');
                    }
                })
                ->first();
        // $t = date('Y');

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_uri', compact('data','datas','tahun','tanggal','siklus','ttd'))
            ->setPaper([0, 0, '612', '936'], 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi URI.pdf');
    }

    public function laporanakhiruridownload(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $dd = $request->tanggal;
            $siklus = $request->get('y');
            if (!empty($dd)) {
                $request->session()->put('tanggal', $dd);
                $request->session()->put('siklus', $siklus);
            } else {
                $dd = $request->session()->get('tanggal');
                $siklus = $request->session()->get('siklus');
            }
            DB::statement(DB::raw("set @rownum=0"));
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.siklus','=',$siklus)
                ->where('upload_lap_akhir.tahun','=',$dd)
                ->where('upload_lap_akhir.parameter','=','3');
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/uri');

    }

    public function test(\Illuminate\Http\Request $request)
    {
        return view('data_evaluasi/lap/penutupuri');
    }

    // public function laporanakhiruripenutup(\Illuminate\Http\Request $request,$id)
    // {
    //     $t = Date('Y-m-d');
    //     $pdf = PDF::loadview('data_evaluasi/lap/penutupuri', compact('data','datas','t'))
    //         ->setPaper('a4', 'potrait')
    //         ->setwarnings(false);
    //         return $pdf->stream('Penutup URI.pdf');
    // }
    public function laporantandaterimabahan()
    {
        $bidang = DB::table('sub_bidang')->get();
        return view('laporan/bahan/index', compact('bidang'));
    }

    public function laporantandaterimabahana(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->join('tb_tandaterima','tb_tandaterima.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('provinces','provinces.id','=','perusahaan.provinsi')
                    ->leftjoin('regencies','regencies.id','=','perusahaan.kota')
                    ->leftjoin('districts','districts.id','=','perusahaan.kecamatan')
                    ->where('tb_registrasi.bidang', $input['bidang'])
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'),'=', $input['tahun'])
                    ->where(function($query) use ($input){
                        $query->where('tb_registrasi.siklus', '=', $input['siklus'])
                            ->orwhere('tb_registrasi.siklus', '=', '12');
                    })
                    ->groupBy('tb_registrasi.id')
                    ->select('tb_registrasi.id','perusahaan.nama_lab','perusahaan.alamat','tb_registrasi.kode_lebpes','tb_tandaterima.tanggal_penerimaan','tb_tandaterima.penerima','provinces.name as Provinsi', 'regencies.name as Kota', 'districts.name as Kecamatan')
                    ->get();
        foreach ($data as $key => $val) {
            $bahan = DB::table('tb_tandaterima')
                        ->where('id_registrasi', $val->id)
                        ->where('siklus', $input['siklus'])
                        ->select('tanggal_penerimaan','kondisi_bahan','kode','keterangan')
                        ->get();
            $val->bahan = $bahan;
        }
        // dd($data);
        Excel::create('Laporan Penerimaan Bahan Tahun '.$input['tahun'].' Siklus '.$input['siklus'], function($excel) use ($data, $input) {
            $excel->sheet('Tanda Terima Bahan', function($sheet) use ($data, $input) {
                $sheet->loadView('laporan/bahan/proses', array('data'=>$data, 'input'=>$input) );
            });

        })->download('xls');

    }
    // public function laporantandaterimabahana(\Illuminate\Http\Request $request)
    // {
    //     $input = $request->all();
    //     if($input['bidang'] <= 3 || $input['bidang'] == 11 || $input['bidang'] == 12 ){
    //         $data = DB::table('tb_registrasi')
    //                 ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
    //                 ->join('hp_headers', 'hp_headers.id_registrasi','=','tb_registrasi.id')
    //                 ->join('sub_bidang', 'sub_bidang.id','=','tb_registrasi.bidang')
    //                 ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                 ->where('hp_headers.siklus', '=', $input['siklus'])
    //                 ->where('tb_registrasi.bidang', '=', $input['bidang'])
    //                 ->select('tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'perusahaan.alamat', 'sub_bidang.parameter', 'hp_headers.tgl_penerimaan','hp_headers.type')
    //                 ->get();
    //         // dd($input['bidang']);
    //         // return view('laporan/bahan/proses', compact('data','input','type'));
    //         if($input['bidang'] <= 3 ){
    //             Excel::create('Laporan Penerimaan Bahan Tahun '.$input['tahun'].' Siklus '.$input['siklus'], function($excel) use ($data, $input) {
    //                 $excel->sheet('Tipe 01', function($sheet) use ($data, $input) {
    //                     $sheet->loadView('laporan/bahan/proses', array('data'=>$data, 'input'=>$input, 'type'=>'a') );
    //                 });
    //                 $excel->sheet('Tipe 02', function($sheet) use ($data, $input) {
    //                     $sheet->loadView('laporan/bahan/proses', array('data'=>$data, 'input'=>$input, 'type'=>'b') );
    //                 });

    //             })->download('xls');
    //         }else{
    //             Excel::create('Laporan Penerimaan Bahan Tahun '.$input['tahun'].' Siklus '.$input['siklus'], function($excel) use ($data, $input) {
    //                 $excel->sheet('Data', function($sheet) use ($data, $input) {
    //                     $sheet->loadView('laporan/bahan/proses', array('data'=>$data, 'input'=>$input) );
    //                 });

    //             })->download('xls');
    //         }
    //     }elseif ($input['bidang'] == 4) {
    //         $data = DB::table('tb_registrasi')
    //                 ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
    //                 ->join('tb_bta', 'tb_bta.id_registrasi','=','tb_registrasi.id')
    //                 ->join('sub_bidang', 'sub_bidang.id','=','tb_registrasi.bidang')
    //                 ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                 ->where('tb_bta.siklus', '=', $input['siklus'])
    //                 ->where('tb_registrasi.bidang', '=', $input['bidang'])
    //                 ->groupBy('tb_registrasi.perusahaan_id')
    //                 ->select('tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'perusahaan.alamat', 'sub_bidang.parameter', 'tb_bta.tgl_penerimaan')
    //                 ->get();
    //         // dd($data);
    //         // return view('laporan/bahan/proses', compact('data'));

    //         Excel::create('Laporan Penerimaan Bahan Tahun '.$input['tahun'].' Siklus '.$input['siklus'], function($excel) use ($data, $input) {
    //             $excel->sheet('BTA', function($sheet) use ($data, $input) {
    //                 $sheet->loadView('laporan/bahan/proses', array('data'=>$data, 'input'=>$input) );
    //             });

    //         })->download('xls');
    //     }elseif ($input['bidang'] == 5) {
    //         $data = DB::table('tb_registrasi')
    //                 ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
    //                 ->join('tb_telurcacing', 'tb_telurcacing.id_registrasi','=','tb_registrasi.id')
    //                 ->join('sub_bidang', 'sub_bidang.id','=','tb_registrasi.bidang')
    //                 ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                 ->where('tb_telurcacing.siklus', '=', $input['siklus'])
    //                 ->where('tb_registrasi.bidang', '=', $input['bidang'])
    //                 ->groupBy('tb_registrasi.perusahaan_id')
    //                 ->select('tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'perusahaan.alamat', 'sub_bidang.parameter', 'tb_telurcacing.tgl_penerimaan')
    //                 ->get();
    //         // dd($data);
    //         // return view('laporan/bahan/proses', compact('data'));

    //         Excel::create('Laporan Penerimaan Bahan Tahun '.$input['tahun'].' Siklus '.$input['siklus'], function($excel) use ($data, $input) {
    //             $excel->sheet('TC', function($sheet) use ($data, $input) {
    //                 $sheet->loadView('laporan/bahan/proses', array('data'=>$data, 'input'=>$input) );
    //             });

    //         })->download('xls');
    //     }elseif ($input['bidang'] == 7) {
    //         $datatp = DB::table('tb_registrasi')
    //                 ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
    //                 ->join('master_imunologi', 'master_imunologi.id_registrasi','=','tb_registrasi.id')
    //                 ->join('bahan_imunologi', 'bahan_imunologi.id_master_imunologi','=','master_imunologi.id')
    //                 ->join('sub_bidang', 'sub_bidang.id','=','tb_registrasi.bidang')
    //                 ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                 ->where('master_imunologi.siklus', '=', $input['siklus'])
    //                 ->where('tb_registrasi.bidang', '=', $input['bidang'])
    //                 ->where('master_imunologi.jenis_form', '=', 'Syphilis')
    //                 ->groupBy('tb_registrasi.perusahaan_id')
    //                 ->select('tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'perusahaan.alamat', 'sub_bidang.parameter', 'bahan_imunologi.tgl_diterima as tgl_penerimaan')
    //                 ->get();
    //         $datarpr = DB::table('tb_registrasi')
    //                 ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
    //                 ->join('master_imunologi', 'master_imunologi.id_registrasi','=','tb_registrasi.id')
    //                 ->join('bahan_imunologi', 'bahan_imunologi.id_master_imunologi','=','master_imunologi.id')
    //                 ->join('sub_bidang', 'sub_bidang.id','=','tb_registrasi.bidang')
    //                 ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                 ->where('master_imunologi.siklus', '=', $input['siklus'])
    //                 ->where('tb_registrasi.bidang', '=', $input['bidang'])
    //                 ->where('master_imunologi.jenis_form', '=', 'rpr-syphilis')
    //                 ->groupBy('tb_registrasi.perusahaan_id')
    //                 ->select('tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'perusahaan.alamat', 'sub_bidang.parameter', 'bahan_imunologi.tgl_diterima as tgl_penerimaan')
    //                 ->get();
    //         // dd($data);
    //         // return view('laporan/bahan/proses', compact('data'));

    //         Excel::create('Laporan Penerimaan Bahan Tahun '.$input['tahun'].' Siklus '.$input['siklus'], function($excel) use ($datatp, $datarpr, $input) {
    //             $excel->sheet('TP', function($sheet) use ($datatp, $input) {
    //                 $sheet->loadView('laporan/bahan/proses', array('data'=>$datatp, 'input'=>$input) );
    //             });
    //             $excel->sheet('RPR', function($sheet) use ($datarpr, $input) {
    //                 $sheet->loadView('laporan/bahan/proses', array('data'=>$datarpr, 'input'=>$input) );
    //             });

    //         })->download('xls');
    //     }elseif ($input['bidang'] == 6 || $input['bidang'] == 8 || $input['bidang'] == 9) {
    //         $data = DB::table('tb_registrasi')
    //                 ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
    //                 ->join('master_imunologi', 'master_imunologi.id_registrasi','=','tb_registrasi.id')
    //                 ->join('bahan_imunologi', 'bahan_imunologi.id_master_imunologi','=','master_imunologi.id')
    //                 ->join('sub_bidang', 'sub_bidang.id','=','tb_registrasi.bidang')
    //                 ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                 ->where('master_imunologi.siklus', '=', $input['siklus'])
    //                 ->where('tb_registrasi.bidang', '=', $input['bidang'])
    //                 ->groupBy('tb_registrasi.perusahaan_id')
    //                 ->select('tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'perusahaan.alamat', 'sub_bidang.parameter', 'bahan_imunologi.tgl_diterima as tgl_penerimaan')
    //                 ->get();
    //         // dd($data);
    //         // return view('laporan/bahan/proses', compact('data'));

    //         Excel::create('Laporan Penerimaan Bahan Tahun '.$input['tahun'].' Siklus '.$input['siklus'], function($excel) use ($data, $input) {
    //             $excel->sheet('Imun', function($sheet) use ($data, $input) {
    //                 $sheet->loadView('laporan/bahan/proses', array('data'=>$data, 'input'=>$input) );
    //             });

    //         })->download('xls');
    //     }elseif ($input['bidang'] == 10) {
    //         $data = DB::table('tb_registrasi')
    //                 ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
    //                 ->join('tb_malaria', 'tb_malaria.id_registrasi','=','tb_registrasi.id')
    //                 ->join('sub_bidang', 'sub_bidang.id','=','tb_registrasi.bidang')
    //                 ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                 ->where('tb_malaria.siklus', '=', $input['siklus'])
    //                 ->where('tb_registrasi.bidang', '=', $input['bidang'])
    //                 ->groupBy('tb_registrasi.perusahaan_id')
    //                 ->select('tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'perusahaan.alamat', 'sub_bidang.parameter', 'tb_malaria.tgl_penerimaan')
    //                 ->get();
    //         // dd($data);
    //         // return view('laporan/bahan/proses', compact('data'));

    //         Excel::create('Laporan Penerimaan Bahan Tahun '.$input['tahun'].' Siklus '.$input['siklus'], function($excel) use ($data, $input) {
    //             $excel->sheet('Mal', function($sheet) use ($data, $input) {
    //                 $sheet->loadView('laporan/bahan/proses', array('data'=>$data, 'input'=>$input) );
    //             });

    //         })->download('xls');
    //     }elseif ($input['bidang'] == 13) {
    //         $data = DB::table('tb_registrasi')
    //                 ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
    //                 ->join('data_antibiotik', 'data_antibiotik.id_registrasi','=','tb_registrasi.id')
    //                 ->join('sub_bidang', 'sub_bidang.id','=','tb_registrasi.bidang')
    //                 ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                 ->where('data_antibiotik.siklus', '=', $input['siklus'])
    //                 ->where('tb_registrasi.bidang', '=', $input['bidang'])
    //                 ->groupBy('tb_registrasi.perusahaan_id')
    //                 ->select('tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'perusahaan.alamat', 'sub_bidang.parameter', 'data_antibiotik.tgl_diterima as tgl_penerimaan')
    //                 ->get();
    //         // dd($data);
    //         // return view('laporan/bahan/proses', compact('data'));

    //         Excel::create('Laporan Penerimaan Bahan Tahun '.$input['tahun'].' Siklus '.$input['siklus'], function($excel) use ($data, $input) {
    //             $excel->sheet('BaC', function($sheet) use ($data, $input) {
    //                 $sheet->loadView('laporan/bahan/proses', array('data'=>$data, 'input'=>$input) );
    //             });
    //         })->download('xls');
    //     }
    // }
}

<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\User;
use App\daftar as Daftar;
use Illuminate\Support\Facades\Redirect;

class DatadiriController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $id = Auth::user()->id;
        $data = DB::table('users')
                ->leftjoin('perusahaan', 'users.id', '=', 'perusahaan.created_by')
                ->leftjoin('badan_usaha', 'perusahaan.pemerintah', '=', 'badan_usaha.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->leftjoin('regencies', 'perusahaan.kota', '=', 'regencies.id')
                ->leftjoin('districts', 'perusahaan.kecamatan', '=', 'districts.id')
                ->leftjoin('villages', 'perusahaan.kelurahan', '=', 'villages.id')
                ->select('users.name', 'users.email', 'perusahaan.nama_lab', 'badan_usaha.badan_usaha as Badan', 'badan_usaha.id as Idbadan', 'perusahaan.alamat', 'provinces.name as Provinsi', 'provinces.id as Idprovinsi', 'regencies.name as Kota', 'regencies.id as Idkota', 'districts.name as Kecamatan', 'districts.id as Idkecamatan', 'villages.name as Kelurahan', 'villages.id as Idkelurahan', 'perusahaan.telp as Telp', 'perusahaan.email as Email', 'perusahaan.penanggung_jawab', 'perusahaan.personal', 'perusahaan.no_hp', 'perusahaan.no_wa', 'perusahaan.akreditasi', 'perusahaan.pemantapan_mutu', 'perusahaan.alasan_binaan', 'perusahaan.dilaksanakan')
                ->where('users.id', $id)
                ->first();

        $provinsi = DB::table('provinces')->select('id','name')->get();
        $kota = DB::table('regencies')->where('province_id','=', $data->Idprovinsi)->get();
        $kecamatan = DB::table('districts')->where('regency_id','=', $data->Idkota)->get();
        $kelurahan = DB::table('villages')->where('district_id','=', $data->Idkecamatan)->get();
        
        $badan = DB::table('badan_usaha')->get();
        $pendidikan = DB::table('tb_pendidikan')->get();
        // dd($data);
        return view('edit_data', compact('data', 'badan', 'provinsi', 'pendidikan','kota','kecamatan','kelurahan'));
    }

    public function update(\Illuminate\Http\Request $request)
    {
        $id = Auth::user()->id;
     
        $dilaksanakan = ""; 
        $data['nama_lab'] = $request->nama_lab;
        $data['pemerintah'] = $request->pemerintah;
        $data['alamat'] = $request->alamat;
        $data['akreditasi'] = $request->akreditasi;
        $data['pemantapan_mutu'] = $request->pemantapan_mutu;
        if($request->pemantapan_mutu == 'Dilaksanakan'){
            if(!empty($request->dilaksanakan)){
                $data['dilaksanakan'] = implode(", ",$request->dilaksanakan);
            }
        }else{
            $data['dilaksanakan'] = NULL;
        }
        $data['alasan_binaan'] = $request->alasan_binaan;
        $data['provinsi'] = $request->provinsi;
        $data['kota'] = $request->kota;
        $data['kecamatan'] = $request->kecamatan;
        $data['kelurahan'] = $request->kelurahan;
        $data['telp'] = $request->telp;
        $data['email'] = $request->Email;
        $data['penanggung_jawab'] = $request->penanggung_jawab;
        $data['personal'] = $request->personal;
        $data['no_hp'] = $request->no_hp;
        $data['no_wa'] = $request->no_wa;

        $user['name'] = $request->name;

        $password = bcrypt($request->password);
        if ($request->password != "") {
            $user['password'] = $password;
        }

        Daftar::where('created_by',$id)->update($data);
        User::where('id',$id)->update($user);
        Session::flash('message', 'Data diri berhasil diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('edit-data');
    }
}

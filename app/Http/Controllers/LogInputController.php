<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;
use App\Hide;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\LogInput;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class LogInputController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $user = DB::table('users')->get();
        return view('back/log/index', compact('user'));
    }

    public function proses(\Illuminate\Http\Request $request)
    {
        $data = $request->all();
        $log = DB::table('log_input')
                ->join('tb_registrasi','tb_registrasi.id','=','log_input.id_registrasi')
                ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                ->join('users','users.id','=','log_input.created_by')
                ->select('log_input.*','sub_bidang.alias','users.email')
                ->where('log_input.created_by', $data['email'])
                ->orderBy('log_input.id','desc')
                ->get();
        return view('back/log/view', compact('log'));
    }

}

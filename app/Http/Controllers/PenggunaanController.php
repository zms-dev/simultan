<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;
use App\penggunaan as Penggunaan;
use App\Instrumen;
use App\MetodePemeriksaan;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;

class PenggunaanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = DB::table('penggunaan')->get();
        return view('back/penggunaan/index', compact('data'));
    }

    public function edit($id)
    {
        $data = DB::table('penggunaan')->where('id', $id)->get();
        return view('back/penggunaan/update', compact('data'));
    }

    public function in()
    {
        return view('back/penggunaan/insert');
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $data = Request::all();
        if (Request::hasFile('video'))
        {
            $dest = public_path('asset/backend/penggunaan/');
            $name = Request::file('video')->getClientOriginalName();
            Request::file('video')->move($dest, $name);
            $data['video'] = $name;
        }
        Penggunaan::where('id',$id)->update($data);
        Session::flash('message', 'Tutorial Penggunaan Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/penggunaan');
    }    

    public function insert(\Illuminate\Http\Request $request)
    {
       if (Request::hasFile('video'))
        {
            $dest = public_path('asset/backend/penggunaan/');
            $name = Request::file('video')->getClientOriginalName();
            $ucp = Request::file('video')->move($dest, $name);
            // dd($ucp);
        }
        $data = Request::file('video');
        $data = Request::all();
        $data['video'] = $name;

        Penggunaan::create($data);
        Session::flash('message', 'Tutorial Penggunaan Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/penggunaan');
    }

    public function delete($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Penggunaan::find($id)->delete();
        return redirect("admin/penggunaan");
    }

    public function alatkimia(\Illuminate\Http\Request $request)
    {
        $id = $request->get('parameter');
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        $data = DB::table('tb_instrumen')
                ->join('parameter','parameter.id','=','tb_instrumen.id_parameter')
                ->where('parameter.kategori','=','kimia klinik')
                ->where('parameter.id', $id)
                ->select('tb_instrumen.*', 'parameter.nama_parameter')
                ->orderBy('tb_instrumen.id', 'desc')
                ->get();
                // dd($id);
        return view("back/alatinstrumen/kimiaklinik/index", compact('data','parameter'));
    }

    public function alatkimiai()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view("back/alatinstrumen/kimiaklinik/insert", compact('parameter'));
    }

    public function alatkimiain(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $Save = new Instrumen;
        $Save->instrumen = $input['instrumen'];
        $Save->status = $input['status'];
        $Save->id_parameter = $input['parameter'];
        $Save->save();
        Session::flash('message', 'Alat / Instrumen Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/alat-instrumen/kimia-klinik');
    }

    public function alatkimiae($id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        $data = DB::table('tb_instrumen')
                ->join('parameter','parameter.id','=','tb_instrumen.id_parameter')
                ->where('parameter.kategori','=','kimia klinik')
                ->where('tb_instrumen.id', $id)
                ->select('tb_instrumen.*', 'parameter.nama_parameter')
                ->first();
        // dd($data);
        return view("back/alatinstrumen/kimiaklinik/edit", compact('parameter','data'));
    }

    public function alatkimiaen(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();

        $Save['instrumen'] = $request->instrumen;
        $Save['status'] = $request->status;
        $Save['id_parameter'] = $request->parameter;
        Instrumen::where('id', $id)->update($Save);
        Session::flash('message', 'Alat / Instrumen Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/alat-instrumen/kimia-klinik');
    }


    public function metodekimia(\Illuminate\Http\Request $request)
    {
        $id = $request->get('parameter');
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        $data = DB::table('metode_pemeriksaan')
                ->join('parameter','parameter.id','=','metode_pemeriksaan.parameter_id')
                ->where('parameter.kategori','=','kimia klinik')
                ->where('parameter.id', $id)
                ->select('metode_pemeriksaan.*', 'parameter.nama_parameter')
                ->orderBy('metode_pemeriksaan.id', 'desc')
                ->get();
                // dd($id);
        return view("back/metodepemeriksaan/kimiaklinik/index", compact('data','parameter'));
    }
    public function metodekimiai()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view("back/metodepemeriksaan/kimiaklinik/insert", compact('parameter'));
    }

    public function metodekimiain(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $Save = new MetodePemeriksaan;
        $Save->metode_pemeriksaan = $input['metode_pemeriksaan'];
        $Save->status = $input['status'];
        $Save->parameter_id = $input['parameter'];
        $Save->kode = $input['kode'];
        $Save->save();
        Session::flash('message', 'Metode Pemeriksaan Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/metode-pemeriksaan/kimia-klinik');
    }

    public function metodekimiae($id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        $data = DB::table('metode_pemeriksaan')
                ->join('parameter','parameter.id','=','metode_pemeriksaan.parameter_id')
                ->where('parameter.kategori','=','kimia klinik')
                ->where('metode_pemeriksaan.id', $id)
                ->select('metode_pemeriksaan.*', 'parameter.nama_parameter')
                ->first();
        // dd($data);
        return view("back/metodepemeriksaan/kimiaklinik/edit", compact('parameter','data'));
    }

    public function metodekimiaen(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();

        $Save['metode_pemeriksaan'] = $request->metode_pemeriksaan;
        $Save['status'] = $request->status;
        $Save['parameter_id'] = $request->parameter;
        $Save['kode'] = $request->kode;
        MetodePemeriksaan::where('id', $id)->update($Save);
        Session::flash('message', 'Alat / Instrumen Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/metode-pemeriksaan/kimia-klinik');
    }
}

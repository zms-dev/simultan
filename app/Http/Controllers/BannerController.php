<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;
use App\Hide;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\banner as Banner;
use App\Berita;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class BannerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $banner = DB::table('tb_banner')->get();
        return view('back/banner/banner', compact('banner'));
    }

    public function edit($id)
    {
        $data = DB::table('tb_banner')->where('id', $id)->get();
        return view('back/banner/update', compact('data'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        if (Request::hasFile('image'))
        {
            $dest = public_path('asset/backend/banner/');
            $name = Request::file('image')->getClientOriginalName();
            Request::file('image')->move($dest, $name);
        }
        $data = Request::file('image');
        $data = Request::all();
        $data['image'] = $name;
        $data['created_by'] = Auth::user()->id;
        Banner::where('id',$id)->update($data);
        Session::flash('message', 'Banner Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/banner');
    }    

    public function insert(\Illuminate\Http\Request $request)
    {
        if (Request::hasFile('image'))
        {
            $dest = public_path('asset/backend/banner/');
            $name = Request::file('image')->getClientOriginalName();
            Request::file('image')->move($dest, $name);
        }
        $data = Request::file('image');
        $data = Request::all();
        $data['image'] = $name;
        $data['created_by'] = Auth::user()->id;
        Banner::create($data);
        Session::flash('message', 'Banner Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/banner');
    }

    public function delete($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Banner::find($id)->delete();
        return redirect("admin/banner");
    }

    public function edithasil()
    {
        return view('back.edithasil.index');
    }
    public function edithasilna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $date = date('Y');
        // dd($input);  
        $bidang = DB::table('sub_bidang')->get();
        foreach ($bidang as $key => $val) {
            if($val->id <= 3){
                if ($input['siklus'] == '1') {
                    $data1 = DB::table('tb_registrasi')
                            ->where('status_data1', '=', '1')
                            ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                            ->update(array('status_data1' => 2));
                    $data2 = DB::table('tb_registrasi')
                            ->where('status_data2', '=', '1')
                            ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                            ->update(array('status_data2' => 2));
                }else{
                    $data1 = DB::table('tb_registrasi')
                            ->where('status_datarpr1', '=', '1')
                            ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                            ->update(array('status_datarpr1' => 2));
                    $data2 = DB::table('tb_registrasi')
                            ->where('status_datarpr2', '=', '1')
                            ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                            ->update(array('status_datarpr2' => 2));
                }
            }elseif ($val->id == 7) {
                if ($input['siklus'] == '1') {
                    $data1 = DB::table('tb_registrasi')
                            ->where('status_data1', '=', '1')
                            ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                            ->update(array('status_data1' => 2));
                    $data2 = DB::table('tb_registrasi')
                            ->where('status_datarpr1', '=', '1')
                            ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                            ->update(array('status_datarpr1' => 2));
                }else{
                    $data1 = DB::table('tb_registrasi')
                            ->where('status_data2', '=', '1')
                            ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                            ->update(array('status_data2' => 2));
                    $data2 = DB::table('tb_registrasi')
                            ->where('status_datarpr2', '=', '1')
                            ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                            ->update(array('status_datarpr2' => 2));
                }
            }else{
                if ($input['siklus'] == '1') {
                    $data1 = DB::table('tb_registrasi')
                            ->where('status_data1', '=', '1')
                            ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                            ->update(array('status_data1' => 2));
                }else{
                    $data2 = DB::table('tb_registrasi')
                            ->where('status_data2', '=', '1')
                            ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                            ->update(array('status_data2' => 2));
                }
            }
        }
        Session::flash('message', 'Data Edit Peserta Telah ter Kirim!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect::back();
    }

    public function menu()
    {
        $data = DB::table('tb_hide_menu')->get();
        return view('back.edithasil.menu', compact('data'));
    }
    public function menuna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $menu = Hide::where('id', $input['id'])->first();
        // dd($menu);
        if($menu->status == 'hilang'){
            $proses = Hide::where('id', $input['id'])
                    ->update(['status'=> 'muncul']);
        }else{
            $proses = Hide::where('id', $input['id'])
                        ->update(['status'=> 'hilang']);
        }

        Session::flash('message', 'Menu Web PNPME Berhasil di Ubah!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect::back();
    }

    public function beritafront()
    {
        $data = DB::table('tb_berita')->orderBy('updated_at', 'desc')->get();
        User::where('id',Auth::user()->id)->update(['berita' => 0]);
        return view('berita/berita', compact('data'));
    }
    public function detailbertia($id)
    {
        $data = DB::table('tb_berita')->where('id', $id)->first();
        return view('berita/detail', compact('data'));
    }
    public function berita()
    {
        $berita = DB::table('tb_berita')->orderBy('updated_at', 'desc')->get();
        return view('back/edithasil/berita', compact('berita'));
    }
    public function addberita()
    {
        $data = DB::table('tb_berita')->first();
        return view('back.edithasil.insertberita', compact('data'));
    }
    public function insertberita(\Illuminate\Http\Request $request)
    {
        $input = Request::all();
        if (Request::hasFile('img'))
        {
            $dest = public_path('asset/backend/berita/');
            $name = Request::file('img')->getClientOriginalName();
            Request::file('img')->move($dest, $name);
        }
        $data['judul'] = $input['judul'];
        $data['isi'] = $input['isi'];
        $data['img'] = $name;
        $data['created_by'] = Auth::user()->id;
        Berita::create($data);
        User::where('role','=','3')->update(['berita'=> DB::raw('berita+1')]);
        Session::flash('message', 'Berita Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/berita');
    }
    public function editberita($id)
    {
        $data = DB::table('tb_berita')->where('id', $id)->first();
        return view('back.edithasil.prosesberita', compact('data'));
    }
    public function updateberita(\Illuminate\Http\Request $request, $id)
    {
        $input = Request::all();
        if (Request::hasFile('img'))
        {
            $dest = public_path('asset/backend/berita/');
            $name = Request::file('img')->getClientOriginalName();
            Request::file('img')->move($dest, $name);
            $data['img'] = $name;
        }
        $data['judul'] = $input['judul'];
        $data['isi'] = $input['isi'];
        $data['created_by'] = Auth::user()->id;
        // dd($data);
        Berita::where('id',$id)->update($data);
        User::where('role','=','3')->update(['berita'=> DB::raw('berita+1')]);
        Session::flash('message', 'Berita Berhasil di Update!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/berita');
    }
    public function deleteberita($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Berita::find($id)->delete();
        return redirect("admin/berita");
    }

    public function siklus()
    {
        $menu = DB::table('tb_siklus')->first();
        return view('back.edithasil.siklus', compact('menu'));
    }
    public function siklusna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $menu = DB::table('tb_siklus')->get();
        // dd($menu);
        if (count($menu)) {
            $proses = DB::table('tb_siklus')->update(['siklus'=> $input['siklus'], 'tahun' => $input['tahun']]);
        }else{
            $proses = DB::table('tb_siklus')->insert(['siklus'=> $input['siklus'], 'tahun' => $input['tahun']]);
        }
        Session::flash('message', 'Siklus PNPME Berhasil di Ubah!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect::back();
    }

    public function tahun()
    {
        $menu = DB::table('tb_tahun_evaluasi')->first();
        return view('back.edithasil.tahun_evaluasi', compact('menu'));
    }
    public function tahunna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $menu = DB::table('tb_tahun_evaluasi')->get();
        // dd($menu);
        if (count($menu)) {
            $proses = DB::table('tb_tahun_evaluasi')->update(['siklus'=> $input['siklus'], 'tahun' => $input['tahun']]);
        }else{
            $proses = DB::table('tb_tahun_evaluasi')->insert(['siklus'=> $input['siklus'], 'tahun' => $input['tahun']]);
        }
        Session::flash('message', 'Tahun Evaluasi PNPME Berhasil di Ubah!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect::back();
    }
}

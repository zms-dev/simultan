<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Reagen;
use Illuminate\Support\Facades\Redirect;

class ReagenController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $reagen = DB::table('tb_reagen_imunologi')->get();
        return view('back/reagen/index', compact('reagen'));
    }

    public function edit($id)
    {
        $data = DB::table('tb_reagen_imunologi')->where('id', $id)->get();
        return view('back/reagen/update', compact('data'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $SaveDetail['reagen'] = $request->reagen;
        $SaveDetail['kelompok'] = $request->kelompok;
        $SaveDetail['produsen'] = $request->produsen;
        $SaveDetail['metode'] = $request->metode;
        Reagen::where('id', $id)->update($SaveDetail);

        Session::flash('message', 'Reagen Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/reagen-imunologi');
    }    

    public function insert(\Illuminate\Http\Request $request)
    {
        $input = Request::all();

        $saveReagen = new Reagen;
        $saveReagen->reagen = $input['reagen'];
        $saveReagen->kelompok = $input['kelompok'];
        $saveReagen->produsen = $input['produsen'];
        $saveReagen->metode = $input['metode'];
        $saveReagen->save();
        Session::flash('message', 'Reagen Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/reagen-imunologi');
    }

    public function delete($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Reagen::find($id)->delete();
        return redirect("admin/reagen-imunologi");
    }
}

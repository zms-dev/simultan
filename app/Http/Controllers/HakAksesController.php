<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\akses as Akses;
use Illuminate\Support\Facades\Redirect;

class HakAksesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $akses = DB::table('users')->get();
        return view('back/akses/index', compact('akses'));
    }

    public function in()
    {
        $sub = DB::table('sub_bidang')->get();
        return view('back/akses/insert', compact('sub'));
    }

    public function edit($id)
    {
        $akses = DB::table('users')
                    ->leftjoin('sub_bidang', 'users.penyelenggara','=','sub_bidang.id')
                    ->where('users.id', $id)
                    ->select('users.*', 'sub_bidang.parameter')
                    ->get();
        $sub = DB::table('sub_bidang')->get();
        return view('back/akses/update', compact('akses','sub'));
    }

    public function insert(\Illuminate\Http\Request $request){
        $data = Request::all();
        $password = bcrypt($request->password);
        $data['id_member'] = '0';
        $data['password'] = $password;
        Akses::create($data);
        Session::flash('message', 'Data Pengurus Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/hak-akses');
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $password = bcrypt($request->password);
        if ($request->password != "") {
            $data['name'] = $request->name;
            $data['role'] = $request->role;
            $data['email'] = $request->email;
            $data['penyelenggara'] = $request->penyelenggara;
            $data['password'] = $password;
            Akses::where('id',$id)->update($data);
        }else{
            $data['name'] = $request->name;
            $data['role'] = $request->role;
            $data['penyelenggara'] = $request->penyelenggara;
            $data['email'] = $request->email;
            Akses::where('id',$id)->update($data);
        }
        Session::flash('message', 'Hak Akses Berhasil DiUbah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/hak-akses');
    }    

    public function delete($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Akses::find($id)->delete();
        return redirect("admin/hak-akses");
    }
}

<?php

namespace App\Http\Controllers;
use DB;
use PDF;
use Session;

use App\register as Register;

use App\User;
use App\TandaTerima;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
class TandaTerimaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        $tahun = date('Y');
        $user = Auth::user()->id;
        $siklus = DB::table('tb_siklus')->first();
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('tb_registrasi.status', 3)
                ->get();
        foreach ($data as $key => $val) {
            $siklus1 = DB::table('tb_tandaterima')
                        ->where('id_registrasi', $val->id)
                        ->where('siklus', '=', '1')
                        ->get();
            $siklus2 = DB::table('tb_tandaterima')
                        ->where('id_registrasi', $val->id)
                        ->where('siklus', '=', '2')
                        ->get();
            $val->siklus1 = $siklus1;
            $val->siklus2 = $siklus2;
        }
        // dd($data);
        return view('tandaterima/index', compact('data','siklus','tahun'));
    }
    public function hematologi(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();

        return view('tandaterima/input/hematologi',compact('tahun','siklus','register'));
    }
    public function hematologii(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        // dd($siklus);
        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->get();
        if (count($validasi)) {
            Session::flash('message', 'Data sudah terkirim.');
            Session::flash('alert-class', 'alert-danger');
        }else{
            $i = 0;
            foreach ($input['bahan_uji'] as $key => $val) {
                $save = new TandaTerima;
                $save->id_registrasi = $id;
                $save->tanggal_penerimaan = $input['tanggal_penerimaan'];
                $save->juklak = $input['juklak'];
                $save->penerima = $input['penerima'];
                $save->kode = $input['kode'];
                $save->bahan_uji = $input['bahan_uji'][$i];
                $save->kondisi_bahan = $input['kondisi_bahan'][$i];
                $save->keterangan = $input['keterangan'];
                $save->siklus = $siklus;
                // dd($save);
                $save->save();
                $i++;
            }
            Session::flash('message', 'Data sukses terkirim.');
            Session::flash('alert-class', 'alert-info');
        }
        return redirect('tanda-terima');
    }

    public function hematologiprint(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');
        // dd($siklus);
        $data = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->get();
        $data1 = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->first();

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();
        // dd($data);
        $pdf = PDF::loadview('tandaterima/print/hematologi', compact('data','data1', 'register','siklus','tahun'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Hematologi.pdf');
    }

    public function kimiaklinik(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();

        return view('tandaterima/input/kimiaklinik',compact('tahun','siklus','register'));
    }
    public function kimiakliniki(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        // dd($siklus);
        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->get();
        if (count($validasi)) {
            Session::flash('message', 'Data sudah terkirim.');
            Session::flash('alert-class', 'alert-danger');
        }else{
            $i = 0;
            foreach ($input['bahan_uji'] as $key => $val) {
                $save = new TandaTerima;
                $save->id_registrasi = $id;
                $save->tanggal_penerimaan = $input['tanggal_penerimaan'];
                $save->juklak = $input['juklak'];
                $save->penerima = $input['penerima'];
                $save->kode = $input['kode'];
                $save->bahan_uji = $input['bahan_uji'][$i];
                $save->kondisi_bahan = $input['kondisi_bahan'][$i];
                $save->keterangan = $input['keterangan'];
                $save->siklus = $siklus;
                // dd($save);
                $save->save();
                $i++;
            }
            Session::flash('message', 'Data sukses terkirim.');
            Session::flash('alert-class', 'alert-info');
        }
        return redirect('tanda-terima');
    }

    public function kimiaklinikprint(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');
        // dd($siklus);
        $data = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->get();
        $data1 = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->first();

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();
        // dd($data);
        $pdf = PDF::loadview('tandaterima/print/kimiaklinik', compact('data','data1', 'register','siklus','tahun'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Kimia Klinik.pdf');
    }

    public function urinalisa(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();

        return view('tandaterima/input/urinalisa',compact('tahun','siklus','register'));
    }
    public function urinalisai(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        // dd($siklus);
        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->get();
        if (count($validasi)) {
            Session::flash('message', 'Data sudah terkirim.');
            Session::flash('alert-class', 'alert-danger');
        }else{
            $i = 0;
            foreach ($input['bahan_uji'] as $key => $val) {
                $save = new TandaTerima;
                $save->id_registrasi = $id;
                $save->tanggal_penerimaan = $input['tanggal_penerimaan'];
                $save->juklak = $input['juklak'];
                $save->penerima = $input['penerima'];
                $save->kode = $input['kode'];
                $save->bahan_uji = $input['bahan_uji'][$i];
                $save->kondisi_bahan = $input['kondisi_bahan'][$i];
                $save->keterangan = $input['keterangan'];
                $save->siklus = $siklus;
                // dd($save);
                $save->save();
                $i++;
            }
            Session::flash('message', 'Data sukses terkirim.');
            Session::flash('alert-class', 'alert-info');
        }
        return redirect('tanda-terima');
    }

    public function urinalisaprint(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');
        // dd($siklus);
        $data = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->get();
        $data1 = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->first();

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();
        // dd($data);
        $pdf = PDF::loadview('tandaterima/print/urinalisa', compact('data','data1', 'register','siklus','tahun'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Urinalisa.pdf');
    }

    public function bta(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();

        return view('tandaterima/input/bta',compact('tahun','siklus','register'));
    }
    public function btai(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        // dd($siklus);
        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->get();
        if (count($validasi)) {
            Session::flash('message', 'Data sudah terkirim.');
            Session::flash('alert-class', 'alert-danger');
        }else{
            $i = 0;
            foreach ($input['kondisi_bahan'] as $key => $val) {
                $save = new TandaTerima;
                $save->id_registrasi = $id;
                $save->tanggal_penerimaan = $input['tanggal_penerimaan'];
                $save->penerima = $input['penerima'];
                $save->kode = $input['kode'][$i];
                $save->kondisi_bahan = $input['kondisi_bahan'][$i];
                $save->keterangan = $input['keterangan'][$i];
                $save->siklus = $siklus;
                // dd($save);
                $save->save();
                $i++;
            }
            Session::flash('message', 'Data sukses terkirim.');
            Session::flash('alert-class', 'alert-info');
        }
        return redirect('tanda-terima');
    }
    public function btaprint(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');
        // dd($siklus);
        $data = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->get();
        $data1 = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->first();

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();
        // dd($data);
        $pdf = PDF::loadview('tandaterima/print/bta', compact('data','data1', 'register','siklus','tahun'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Mikrobiologi BTA.pdf');
    }

    public function tc(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();

        return view('tandaterima/input/tc',compact('tahun','siklus','register'));
    }
    public function tci(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        // dd($siklus);
        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->get();
        if (count($validasi)) {
            Session::flash('message', 'Data sudah terkirim.');
            Session::flash('alert-class', 'alert-danger');
        }else{
            $i = 0;
            foreach ($input['kondisi_bahan'] as $key => $val) {
                $save = new TandaTerima;
                $save->id_registrasi = $id;
                $save->tanggal_penerimaan = $input['tanggal_penerimaan'];
                $save->penerima = $input['penerima'];
                $save->kode = $input['kode'][$i];
                $save->kondisi_bahan = $input['kondisi_bahan'][$i];
                $save->keterangan = $input['keterangan'][$i];
                $save->siklus = $siklus;
                // dd($save);
                $save->save();
                $i++;
            }
            Session::flash('message', 'Data sukses terkirim.');
            Session::flash('alert-class', 'alert-info');
        }
        return redirect('tanda-terima');
    }
    public function tcprint(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');
        // dd($siklus);
        $data = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->get();
        $data1 = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->first();

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();
        // dd($data);
        $pdf = PDF::loadview('tandaterima/print/tc', compact('data','data1', 'register','siklus','tahun'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Mikrobiologi Telur Cacinf.pdf');
    }

    public function antibiotik(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();

        return view('tandaterima/input/antibiotik',compact('tahun','siklus','register'));
    }
    public function antibiotiki(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        // dd($siklus);
        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->get();
        if (count($validasi)) {
            Session::flash('message', 'Data sudah terkirim.');
            Session::flash('alert-class', 'alert-danger');
        }else{
            $i = 0;
            foreach ($input['kondisi_bahan'] as $key => $val) {
                $save = new TandaTerima;
                $save->id_registrasi = $id;
                $save->tanggal_penerimaan = $input['tanggal_penerimaan'];
                $save->penerima = $input['penerima'];
                $save->kode = $input['kode'][$i];
                $save->kondisi_bahan = $input['kondisi_bahan'][$i];
                $save->keterangan = $input['keterangan'][$i];
                $save->siklus = $siklus;
                // dd($save);
                $save->save();
                $i++;
            }
            Session::flash('message', 'Data sukses terkirim.');
            Session::flash('alert-class', 'alert-info');
        }
        return redirect('tanda-terima');
    }
    public function antibiotikprint(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');
        // dd($siklus);
        $data = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->get();
        $data1 = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->first();

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();
        // dd($data);
        $pdf = PDF::loadview('tandaterima/print/antibiotik', compact('data','data1', 'register','siklus','tahun'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Antibiotik.pdf');
    }
    public function malaria(\Illuminate\Http\Request $request, $id)
    {
        DB::statement(DB::raw('SET @row_number = 0'));
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();

        $posisi = "SELECT
                     *
                    FROM
                        (SELECT
                            (@row_number:=@row_number + 1) as Number, kode_lebpes, nama_lab, tb_registrasi.id
                        FROM
                            `tb_registrasi`
                        INNER JOIN perusahaan ON perusahaan.id = tb_registrasi.perusahaan_id
                        WHERE
                            bidang = 10
                        AND YEAR (tb_registrasi.created_at) = 2019
                        AND (siklus = 12 OR siklus = ".$siklus.")
                        ORDER BY
                            tb_registrasi.created_by ASC) as Malaria
                    WHERE id = ".$id."";
        $posisi = DB::table(DB::raw("($posisi) t"))->select('*')->first();
// dd($posisi);
        return view('tandaterima/input/malaria',compact('tahun','siklus','register','posisi'));
    }
    public function malariai(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        // dd($siklus);
        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->get();
        if (count($validasi)) {
            Session::flash('message', 'Data sudah terkirim.');
            Session::flash('alert-class', 'alert-danger');
        }else{
            $i = 0;
            foreach ($input['kondisi_bahan'] as $key => $val) {
                $save = new TandaTerima;
                $save->id_registrasi = $id;
                $save->tanggal_penerimaan = $input['tanggal_penerimaan'];
                $save->penerima = $input['penerima'];
                $save->kode = $input['kode'][$i];
                $save->kondisi_bahan = $input['kondisi_bahan'][$i];
                $save->keterangan = $input['keterangan'][$i];
                $save->siklus = $siklus;
                // dd($save);
                $save->save();
                $i++;
            }
            Session::flash('message', 'Data sukses terkirim.');
            Session::flash('alert-class', 'alert-info');
        }
        return redirect('tanda-terima');
    }
    public function malariaprint(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');
        // dd($siklus);
        $data = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->get();
        $data1 = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->first();

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();
        // dd($data);
        $pdf = PDF::loadview('tandaterima/print/malaria', compact('data','data1', 'register','siklus','tahun'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Malaria.pdf');
    }

    public function hiv(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();

        return view('tandaterima/input/hiv',compact('tahun','siklus','register'));
    }
    public function hivi(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        // dd($siklus);
        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->get();
        if (count($validasi)) {
            Session::flash('message', 'Data sudah terkirim.');
            Session::flash('alert-class', 'alert-danger');
        }else{
            $i = 0;
            foreach ($input['kondisi_bahan'] as $key => $val) {
                $save = new TandaTerima;
                $save->id_registrasi = $id;
                $save->tanggal_penerimaan = $input['tanggal_penerimaan'];
                $save->penerima = $input['penerima'];
                $save->kode = $input['kode'][$i];
                $save->kondisi_bahan = $input['kondisi_bahan'][$i];
                $save->keterangan = $input['keterangan'][$i];
                $save->siklus = $siklus;
                // dd($save);
                $save->save();
                $i++;
            }
            Session::flash('message', 'Data sukses terkirim.');
            Session::flash('alert-class', 'alert-info');
        }
        return redirect('tanda-terima');
    }
    public function hivprint(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');
        // dd($siklus);
        $data = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->get();
        $data1 = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->first();

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();
        // dd($data);
        $pdf = PDF::loadview('tandaterima/print/hiv', compact('data','data1', 'register','siklus','tahun'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Anti HIV.pdf');
    }

    public function syphilis(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();

        return view('tandaterima/input/syphilis',compact('tahun','siklus','register'));
    }
    public function syphilisi(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        // dd($siklus);
        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->get();
        if (count($validasi)) {
            Session::flash('message', 'Data sudah terkirim.');
            Session::flash('alert-class', 'alert-danger');
        }else{
            $i = 0;
            foreach ($input['kondisi_bahan'] as $key => $val) {
                $save = new TandaTerima;
                $save->id_registrasi = $id;
                $save->tanggal_penerimaan = $input['tanggal_penerimaan'];
                $save->penerima = $input['penerima'];
                $save->kode = $input['kode'][$i];
                $save->kondisi_bahan = $input['kondisi_bahan'][$i];
                $save->keterangan = $input['keterangan'][$i];
                $save->siklus = $siklus;
                // dd($save);
                $save->save();
                $i++;
            }
            Session::flash('message', 'Data sukses terkirim.');
            Session::flash('alert-class', 'alert-info');
        }
        return redirect('tanda-terima');
    }
    public function syphilisprint(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');
        // dd($siklus);
        $data = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->get();
        $data1 = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->first();

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();
        // dd($data);
        $pdf = PDF::loadview('tandaterima/print/syphilis', compact('data','data1', 'register','siklus','tahun'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Sifilis.pdf');
    }
    public function hcv(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();

        return view('tandaterima/input/hcv',compact('tahun','siklus','register'));
    }
    public function hcvi(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        // dd($siklus);
        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->get();
        if (count($validasi)) {
            Session::flash('message', 'Data sudah terkirim.');
            Session::flash('alert-class', 'alert-danger');
        }else{
            $i = 0;
            foreach ($input['kondisi_bahan'] as $key => $val) {
                $save = new TandaTerima;
                $save->id_registrasi = $id;
                $save->tanggal_penerimaan = $input['tanggal_penerimaan'];
                $save->penerima = $input['penerima'];
                $save->kode = $input['kode'][$i];
                $save->kondisi_bahan = $input['kondisi_bahan'][$i];
                $save->keterangan = $input['keterangan'][$i];
                $save->siklus = $siklus;
                // dd($save);
                $save->save();
                $i++;
            }
            Session::flash('message', 'Data sukses terkirim.');
            Session::flash('alert-class', 'alert-info');
        }
        return redirect('tanda-terima');
    }
    public function hcvprint(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');
        // dd($siklus);
        $data = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->get();
        $data1 = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->first();

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();
        // dd($data);
        $pdf = PDF::loadview('tandaterima/print/hcv', compact('data','data1', 'register','siklus','tahun'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Anti HCV.pdf');
    }
    public function hbsag(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();

        return view('tandaterima/input/hbsag',compact('tahun','siklus','register'));
    }
    public function hbsagi(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        // dd($siklus);
        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->get();
        if (count($validasi)) {
            Session::flash('message', 'Data sudah terkirim.');
            Session::flash('alert-class', 'alert-danger');
        }else{
            $i = 0;
            foreach ($input['kondisi_bahan'] as $key => $val) {
                $save = new TandaTerima;
                $save->id_registrasi = $id;
                $save->tanggal_penerimaan = $input['tanggal_penerimaan'];
                $save->penerima = $input['penerima'];
                $save->kode = $input['kode'][$i];
                $save->kondisi_bahan = $input['kondisi_bahan'][$i];
                $save->keterangan = $input['keterangan'][$i];
                $save->siklus = $siklus;
                // dd($save);
                $save->save();
                $i++;
            }
            Session::flash('message', 'Data sukses terkirim.');
            Session::flash('alert-class', 'alert-info');
        }
        return redirect('tanda-terima');
    }
    public function hbsagprint(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');
        // dd($siklus);
        $data = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->get();
        $data1 = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->first();

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();
        // dd($data);
        $pdf = PDF::loadview('tandaterima/print/hbsag', compact('data','data1', 'register','siklus','tahun'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('HBsAg.pdf');
    }

    public function ka(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();

        return view('tandaterima/input/ka',compact('tahun','siklus','register'));
    }
    public function kai(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->get();
        if (count($validasi)) {
            Session::flash('message', 'Data sudah terkirim.');
            Session::flash('alert-class', 'alert-danger');
        }else{
            foreach ($input['bahan_uji'] as $key => $val) {
                $save = new TandaTerima;
                $save->id_registrasi = $id;
                $save->bahan_uji = $input['bahan_uji'][$key];
                $save->lain_lain = $input['lain_lain'][$key];
                $save->tanggal_penerimaan = $input['tanggal_penerimaan'];
                $save->penerima = $input['penerima'];
                if ($input['kode'][$key] != '') {
                    $save->kode = $input['botol'][$key].$input['kode'][$key];
                }else{
                    $save->kode = '';
                }
                $save->kondisi_bahan = $input['kondisi_bahan'][$key];
                $save->keterangan = $input['keterangan'][$key];
                $save->siklus = $siklus;
                // dd($save);
                $save->save();
            }

            Session::flash('message', 'Data sukses terkirim.');
            Session::flash('alert-class', 'alert-info');
        }
        return redirect('tanda-terima');
    }
    public function kaprint(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');
        // dd($siklus);
        $data = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->get();
        $data1 = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->first();

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();
        // dd($data);
        $pdf = PDF::loadview('tandaterima/print/ka', compact('data','data1', 'register','siklus','tahun'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Kimia Air.pdf');
    }

    public function kat(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();

        return view('tandaterima/input/kat',compact('tahun','siklus','register'));
    }
    public function kati(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        // dd($siklus);
        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->get();
        if (count($validasi)) {
            Session::flash('message', 'Data sudah terkirim.');
            Session::flash('alert-class', 'alert-danger');
        }else{
            $save = new TandaTerima;
            $save->id_registrasi = $id;
            $save->tanggal_penerimaan = $input['tanggal_penerimaan'];
            $save->penerima = $input['penerima'];
            $save->kode = $input['kode'];
            $save->kondisi_bahan = $input['kondisi_bahan'];
            $save->keterangan = $input['keterangan'];
            $save->siklus = $siklus;
            // dd($save);
            $save->save();

            Session::flash('message', 'Data sukses terkirim.');
            Session::flash('alert-class', 'alert-info');
        }
        return redirect('tanda-terima');
    }
    public function katprint(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        $siklus = $request->get('y');
        // dd($siklus);
        $data = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->get();
        $data1 = DB::table('tb_tandaterima')->where('siklus', $siklus)->where('id_registrasi', $id)->first();

        $register = DB::table('tb_registrasi')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.id', $id)
                    ->first();
        // dd($data);
        $pdf = PDF::loadview('tandaterima/print/kat', compact('data','data1', 'register','siklus','tahun'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Kimia Air Terbatas.pdf');
    }
}

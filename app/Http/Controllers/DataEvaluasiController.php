<?php
namespace App\Http\Controllers;
use DB;
use Input;
use PDF;
use Carbon\Carbon;

use App\HpHeader;
use App\Parameter;
use App\MetodePemeriksaan;
use App\HpDetail;
use App\ZScore;
use App\ZScoreAlat;
use App\ZScoreMetode;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\kimiaklinik as Kimiaklinik;
use App\daftar as Daftar;
use App\register as Register;
use App\TbReagenImunologi as ReagenImunologi;
use App\TbHp;
use App\Rujukanurinalisa;
use Redirect;
use Validator;
use Session;
use App\User;
use App\SdMedian;
use App\SdMedianMetode;
use App\Ring;
use App\CatatanImun;
use App\SdMedianAlat;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Excel;
use App\statusbakteri;


class DataEvaluasiController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['printevaluasi']]);
    }

    public function index(\Illuminate\Http\Request $request)
    {
        $tanggal = $request->tanggal;
        $siklus = $request->siklus;
        $user = Auth::user()->id;

        $request->session()->put('tanggal', $tanggal);
        $request->session()->put('siklus', $siklus);
        $dd = $request->session()->get('tanggal');
        $siklus = $request->session()->get('siklus');
        // return $dd;
        if ($tanggal == null) {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }else{
            $tanggal = $request->tanggal;
            $siklus = $request->siklus;
        }
        $tanggal = $dd;
        $siklus = $siklus;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('tb_registrasi.status', '>=' , 2)
                ->get();
        Session::put('data',$datas);
        Session::get('data');


        $sklSatu = DB::table('tb_siklus')->select('siklus','tahun')->first();
        
        $responSatu = DB::table('tb_pendapat_responden')->select('siklus','tahun')->where('tahun', $tanggal)->where('siklus','=', $siklus)->where('id_users', $user)->first();
        $listblok = DB::table('blok_evaluasi')->where('user_id', $user)->where('siklus', $siklus)->where('tahun', $tanggal)->get();

        // if(count($data)){
        //     if (count($responSatu)) {
        //         return view('data_evaluasi/index', compact('data', 'data2','tanggal','siklus'));
        //     }else{
        //         Session::flash('message', 'Silahkan isi survey untuk melihat data evaluasi!');
        //         Session::flash('alert-class', 'alert-danger');
        //         return redirect('pendapat/pendapat-responden'); 
        //     }
        // }else{
        // }
        if (count($listblok)) {
            Session::flash('message', 'Akun anda di Blok dari list Evaluasi. Untuk lebih lanjut, silahkan hubungi BBLK Surabaya!'); 
            Session::flash('alert-class', 'alert-success'); 
            return redirect::back();
        }else{
            return view('data_evaluasi/index', compact('datas', 'data2','tanggal','siklus','tahunevaluasi'));
        }
    }

    public function dynamicHemaChartAlat(\Illuminate\Http\Request $req){

        //$alatna = DB::table('tb_instrumen')->where('id', $alat)->first();
        // dd($alatna);
        $get = $req->all();
       /*Chart Script from GfarikController@gzhemsemua*/
             // return('kole');
        $tahun = $get['x'];
        $parameter = $get['p'];
        $type = 'a';
        $siklus = 1;
        $reg = DB::table('tb_registrasi')->where('created_by',$user = Auth::user()->id)->first();
        $alatmetode = DB::table('hp_headers')
            ->join('hp_details', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->where('hp_headers.id_registrasi', $reg->id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->where('hp_details.parameter_id', $parameter)
            ->first();
// dd($alatmetode);
        $alat = $alatmetode->alat;
        $alatna = DB::table('tb_instrumen')->where('id', $alat)->first();
        $id = $reg->id;
        $sql_alat ="
            SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_alat` where not zscore = '-' AND form = 'hematologi' AND alat = ".$alat." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION
            SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_alat` WHERE not zscore = '-' AND form = 'hematologi' AND alat = ".$alat." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION
            SELECT sum(if(zscore < -1 and zscore >= -2, 1, 0)) as nilai1, '< -1 sd -2' as nilainya, '2' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_alat` WHERE not zscore = '-' AND form = 'hematologi' AND alat = ".$alat." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION
            SELECT sum(if(zscore >= -1 and zscore < 0, 1, 0)) as nilai1, '> -1 sd < 0' as nilainya, '3' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_alat` WHERE not zscore = '-' AND form = 'hematologi' AND alat = ".$alat." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION
            SELECT sum(if(zscore = 0.0, 1, 0)) as nilai1, '0.0' as nilainya, '4' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_alat` WHERE not zscore = '-' AND form = 'hematologi' AND alat = ".$alat." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION
            SELECT sum(if(zscore > 0 and zscore <= 1, 1, 0)) as nilai1, '> 0 sd 1' as nilainya, '5' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_alat` WHERE not zscore = '-' AND form = 'hematologi' AND alat = ".$alat." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION
            SELECT sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '6' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_alat` WHERE not zscore = '-' AND form = 'hematologi' AND alat = ".$alat." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION
            SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '7' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_alat` WHERE not zscore = '-' AND form = 'hematologi' AND alat = ".$alat." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION
            SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '8' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_alat` WHERE not zscore = '-' AND form = 'hematologi' AND alat = ".$alat." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter";

        $sql2_alat =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '< -1 sd -2' as nilainya, 'c' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore < -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '>= -1 sd < 0' as nilainya, 'd' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore >= -1 AND zscore < 0
                UNION
                SELECT count(*) as nilai1, '0.0' as nilainya, 'e' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore = 0.0
                UNION
                SELECT count(*) as nilai1, '> 0 sd 1' as nilainya, 'f' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 0 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'g' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'h' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'i' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 3";
        $data_alat = DB::table(DB::raw("($sql_alat) zone"))
                ->select('*')
                ->where('form','=','hematologi')
                ->where('tahun','=',$tahun)
                ->orderBy('sort', 'asc')
                ->get();
        $datap_alat = DB::table(DB::raw("($sql2_alat) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
          $rows = [
            0 => [],
            1 => null,
          ];
        foreach($data_alat as $val){
            $row['color'] = null;
          $row['y'] =(int) $val->nilai1;
          if($val->nilainya == '0.0') {
                $row['color']= '#67ca3b';
          }
          if($val->nilainya == '< -3' || $val->nilainya == '> 3') {
                $row['color']= '#ca3b3b';
          }
          foreach($datap_alat as $valu){
            if($valu->nilainya == $val->nilainya) {
              if($valu->nilai1 == '1') {
                $row['a']= "*";
              }
            }
          }
        array_push($rows[0], $row);
        }
        $alatna->parameter = $parameter;
        $rows[1] = $alatna;
        return response ($rows);
          // dd($data_alat);
    }

    public function dynamicHemaChartMetode(\Illuminate\Http\Request $req){
          //$metodena = DB::table('metode_pemeriksaan')->where('id', $metode)->first();
          // dd($alat);
        $get = $req->all();
       /*Chart Script from GfarikController@gzhemsemua*/
             // return('kole');
        $tahun = $get['x'];
        $parameter = $get['p'];
        $type = 'a';
        $siklus = 1;
        $reg = DB::table('tb_registrasi')->where('created_by',$user = Auth::user()->id)->first();
        $alatmetode = DB::table('hp_headers')
            ->join('hp_details', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->where('hp_headers.id_registrasi', $reg->id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->where('hp_details.parameter_id', $parameter)
            ->first();
        $metode = $alatmetode->kode_metode_pemeriksaan;
        $metodena = DB::table('metode_pemeriksaan')->where('id', $metode)->first();
        $id = $reg->id;
          $sql_metode ="
            SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` where not zscore = '-' AND form = 'hematologi' AND metode = ".$metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION
            SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'hematologi' AND metode = ".$metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION
            SELECT sum(if(zscore < -1 and zscore >= -2, 1, 0)) as nilai1, '< -1 sd -2' as nilainya, '2' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'hematologi' AND metode = ".$metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION

            SELECT sum(if(zscore >= -1 and zscore < 0, 1, 0)) as nilai1, '>= -1 sd < 0' as nilainya, '3' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'hematologi' AND metode = ".$metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION

            SELECT sum(if(zscore = 0.0, 1, 0)) as nilai1, '0.0' as nilainya, '4' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'hematologi' AND metode = ".$metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION

            SELECT sum(if(zscore > 0 and zscore <= 1, 1, 0)) as nilai1, '> 0 sd 1' as nilainya, '5' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'hematologi' AND metode = ".$metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION

            SELECT sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '6' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'hematologi' AND metode = ".$metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION
            SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '7' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'hematologi' AND metode = ".$metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION
            SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '8' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'hematologi' AND metode = ".$metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter";

          $sql2_metode =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_metode
                  where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore < -3
                  UNION
                  SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_metode
                  where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                  UNION
                  SELECT count(*) as nilai1, '< -1 sd -2' as nilainya, 'c' as sort from tb_zscore_metode
                  where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore < -1 AND zscore >= -2
                  UNION
                  SELECT count(*) as nilai1, '>= -1 sd < 0' as nilainya, 'd' as sort from tb_zscore_metode
                  where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore >= -1 AND zscore < 0
                  UNION
                  SELECT count(*) as nilai1, '> 0 sd 1' as nilainya, 'e' as sort from tb_zscore_metode
                  where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 0 AND zscore <= 1
                  UNION
                  SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'f' as sort from tb_zscore_metode
                  where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > -1 AND zscore <= 1
                  UNION
                  SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'g' as sort from tb_zscore_metode
                  where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 1 AND zscore <= 2
                  UNION
                  SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'h' as sort from tb_zscore_metode
                  where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 2 AND zscore <= 3
                  UNION
                  SELECT count(*) as nilai1, '> 3' as nilainya, 'i' as sort from tb_zscore_metode
                  where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 3";

          $data_metode = DB::table(DB::raw("($sql_metode) zone"))
                  ->select('*')
                  ->where('form','=','hematologi')
                  ->where('tahun','=',$tahun)
                  ->orderBy('sort', 'asc')
                  ->get();
          $datap_metode = DB::table(DB::raw("($sql2_metode) zone"))
                  ->select('*')
                  ->orderBy('sort', 'asc')
                  ->get();
          $rows = [
            0 => [],
            1 => [],
          ];
       foreach($data_metode as $val){
        $row['color'] = null;
          $row['y']= (float)$val->nilai1;
           if($val->nilainya == '0.0') {
            $row['color']= '#67ca3b';
           }
           if($val->nilainya == '< -3' || $val->nilainya == '> 3') {
                $row['color']= '#ca3b3b';
           }
          foreach($datap_metode as $valu){
             if($valu->nilainya == $val->nilainya) {
               if($valu->nilai1 == '1') {
                $row['a']= "*";
               }
             }
          }
          array_push($rows[0], $row);
        }
        $rows[1] = $metodena;
        return response ($rows);

    }

    public function dynamicHemaChart(\Illuminate\Http\Request $req){
        $get = $req->all();
       /*Chart Script from GfarikController@gzhemsemua*/
             // return('kole');
        $tahun = $get['x'];
        $parameter = $get['p'];
        $type = 'a';
        $siklus = 1;
        $reg = DB::table('tb_registrasi')->where('created_by',$user = Auth::user()->id)->first();
        $id = $reg->id;
        $sql ="SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` where not zscore = '-' AND form = 'hematologi' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION
            SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'hematologi' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION
            SELECT sum(if(zscore < -1 and zscore >= -2, 1, 0)) as nilai1, '< -1 sd -2' as nilainya, '2' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'hematologi' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION

            SELECT sum(if(zscore >= -1 and zscore < 0, 1, 0)) as nilai1, '>= -1 sd < 0' as nilainya, '3' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'hematologi' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION

            SELECT sum(if(zscore = 0.0 , 1, 0)) as nilai1, '0.0' as nilainya, '4' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'hematologi' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION

            SELECT sum(if(zscore > 0 and zscore <= 1, 1, 0)) as nilai1, '> 0 sd 1' as nilainya, '5' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'hematologi' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION

            SELECT sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '6' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'hematologi' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION
            SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '7' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'hematologi' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter UNION
            SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '8' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'hematologi' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$parameter." GROUP BY parameter";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '< -1 sd -2' as nilainya, 'c' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '>= -1 sd < 0' as nilainya, 'e' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore >= -1 AND zscore < 0
                UNION
                SELECT count(*) as nilai1, '0.0' as nilainya, 'f' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore = 0.0
                UNION
                SELECT count(*) as nilai1, '> 0 sd 1' as nilainya, 'g' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 0 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'h' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'i' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'j' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','hematologi')
                ->where('tahun','=',$tahun)
                ->orderBy('sort', 'asc')
                ->get();
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        $rows = array();

        // dd($datam);
        foreach($data as $val){
            $row['color'] = null;
            $row['y']           = (int)$val->nilai1;
            $row['name']        = $val->nilainya;
            if($val->nilainya   == '0.0') {
                $row['color']   = '#67ca3b';
            }
            if($val->nilainya   == "< -3" || $val->nilainya == '> 3') {
                $row['color']   = '#ca3b3b';
            }
            foreach($datap as $valu){
                if($valu->nilainya == $val->nilainya) {
                    if($valu->nilai1 == '1') {
                        $row['a'] = "*";
                    }
                }
            }
            array_push($rows, $row);
        }
        return response ($rows);
       /*Enc Chart*/

    }

    public function dynamicNilaiCharts(\Illuminate\Http\Request $request){
        $input          = $request->all();
        $reg            = DB::table('tb_registrasi')->where('created_by',$user = Auth::user()->id)->first();
        $id             = $reg->id;
        $tahun          = $input['x'];
        $type           = 'a';
        $siklus         = 1;
        $parameter_id   = $input['p'];
        $arrType        = [
            '01'=> 'a',
            '02'=> 'b'
        ];
        $typeHeader     = 'a';
        $input['type']  = 'a';
        $dataReg = DB::table('tb_registrasi')->where('tb_registrasi.id',$id)
                        ->leftJoin('hp_headers',function($join)use($typeHeader,$siklus){
                            $join->on('tb_registrasi.id','=','hp_headers.id_registrasi')->where('hp_headers.type',$typeHeader)->where('hp_headers.siklus',$siklus);
                        })
                        ->leftJoin('hp_details',function($join) use($parameter_id){
                            $join->on('hp_headers.id','=','hp_details.hp_header_id');
                            $join->where('hp_details.parameter_id','=',$parameter_id);
                        })
                        ->first();
        $parameter      = DB::table('parameter')->where('id','=',$parameter_id)->first();
        $instrumen      = DB::table('tb_instrumen')->where('id','=',$dataReg->alat)->first();
        $metode         = DB::table('metode_pemeriksaan')->where('id','=',$dataReg->kode_metode_pemeriksaan)->first();
        $grafik         = DB::select("
        SELECT
            tb_ring.ring1,
            tb_ring.ring2,
            tb_ring.ring3,
            tb_ring.ring4,
            tb_ring.ring5,
            SUM(CASE WHEN hp_details.hasil_pemeriksaan < ring1 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_bawah_param,
            SUM(CASE WHEN hp_details.hasil_pemeriksaan >= ring1 AND hp_details.hasil_pemeriksaan < ring2 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_1_param,
            SUM(CASE WHEN hp_details.hasil_pemeriksaan >= ring2 AND hp_details.hasil_pemeriksaan < ring3 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_2_param,
            SUM(CASE WHEN hp_details.hasil_pemeriksaan >= ring3 AND hp_details.hasil_pemeriksaan < ring4 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_3_param,
            SUM(CASE WHEN hp_details.hasil_pemeriksaan >= ring4 AND hp_details.hasil_pemeriksaan <= ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_4_param,
            SUM(CASE WHEN hp_details.hasil_pemeriksaan > ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_atas_param,
            SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan < ring1 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_bawah_alat,
            SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan >= ring1 AND hp_details.hasil_pemeriksaan < ring2 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_1_alat,
            SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan >= ring2 AND hp_details.hasil_pemeriksaan < ring3 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_2_alat,
            SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan >= ring3 AND hp_details.hasil_pemeriksaan < ring4 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_3_alat,
            SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan >= ring4 AND hp_details.hasil_pemeriksaan <= ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_4_alat,
            SUM(CASE WHEN hp_details.alat = ".$dataReg->alat." AND  hp_details.hasil_pemeriksaan > ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_atas_alat,
            SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan < ring1 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_bawah_metode,
            SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= ring1 AND hp_details.hasil_pemeriksaan < ring2 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_1_metode,
            SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= ring2 AND hp_details.hasil_pemeriksaan < ring3 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_2_metode,
            SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= ring3 AND hp_details.hasil_pemeriksaan < ring4 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_3_metode,
            SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan >= ring4 AND hp_details.hasil_pemeriksaan <= ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as count_ring_4_metode,
            SUM(CASE WHEN hp_details.kode_metode_pemeriksaan = ".$dataReg->kode_metode_pemeriksaan." AND  hp_details.hasil_pemeriksaan > ring5 AND hp_details.hasil_pemeriksaan REGEXP '[0-9]' THEN 1 ELSE 0 END) as out_atas_metode
        FROM    tb_ring
        LEFT JOIN (
            SELECT hp_details.id,hp_details.parameter_id,hp_headers.type,hp_headers.siklus,tb_registrasi.created_at,hp_details.hasil_pemeriksaan,hp_details.alat,hp_details.kode_metode_pemeriksaan,hp_headers.id_registrasi FROM tb_registrasi
            INNER JOIN hp_headers ON hp_headers.id_registrasi = tb_registrasi.id
            INNER JOIN hp_details ON hp_headers.id = hp_details.hp_header_id
        ) as hp_details ON hp_details.parameter_id = tb_ring.parameter
        WHERE
        tb_ring.parameter =".$dataReg->parameter_id."
        AND
        tb_ring.siklus = ".$siklus."
        AND
        tb_ring.tipe = '".$typeHeader."'
        AND
        tb_ring.form = 'hematologi'
        AND hp_details.type = '".$typeHeader."'
        AND hp_details.siklus = ".$siklus."
        ");
        if(!empty($grafik)){
            $grafik = $grafik[0];
        }


        $median = DB::table('tb_sd_median')
                ->where('parameter', $parameter_id)
                ->where('siklus', $siklus)
                ->where('tipe', $type)
                ->where('tahun', $tahun)
                ->first();


        $medianalat = DB::table('tb_sd_median_alat')
                ->where('alat', $dataReg->alat)
                ->where('siklus', $siklus)
                ->where('tipe', $type)
                ->where('tahun', $tahun)
                ->first();



        $medianmetode = DB::table('tb_sd_median_metode')
                ->where('parameter', $parameter_id)
                ->where('metode', $dataReg->kode_metode_pemeriksaan)
                ->where('siklus', $siklus)
                ->where('tipe', $type)
                ->where('tahun', $tahun)
                ->first();

        $rows = [
            0 => [],
            1 => [],
            2 => []
        ];
              $row["name"]  = "< $grafik->ring1";
              if($dataReg->hasil_pemeriksaan < $grafik->ring1){
              $row["a"] = "*";
              }
              $row["color"] = '#BF0B23';
              $row["y"] = (float)$grafik->out_bawah_param;
            array_push($rows[0], $row);
            $row['color'] = null;

                $row["name"]    = ">= $grafik->ring1 < $grafik->ring2";
                if($dataReg->hasil_pemeriksaan >= $grafik->ring1 && $dataReg->hasil_pemeriksaan < $grafik->ring2){
                    $row["a"]   = "*";
                }
                if($median->median >= $grafik->ring1 && $median->median < $grafik->ring2){
                  $row["color"] = '#1bbd20';
                }
                $row["y"]   = (float)$grafik->count_ring_1_param;
            array_push($rows[0], $row);
            $row['color'] = null;

                $row["name"]    = ">= $grafik->ring2 < $grafik->ring3";
                if($dataReg->hasil_pemeriksaan >= $grafik->ring2 && $dataReg->hasil_pemeriksaan < $grafik->ring3){
                    $row["a"]   = "*";
                }
                if($median->median >= $grafik->ring2 && $median->median < $grafik->ring3){
                  $row["color"] = '#1bbd20';
                }
                $row["y"]   = (float)$grafik->count_ring_2_param;
            array_push($rows[0], $row);
            $row['color'] = null;

                $row["name"]    = ">= $grafik->ring3 < $grafik->ring4";
                if($dataReg->hasil_pemeriksaan >= $grafik->ring3 && $dataReg->hasil_pemeriksaan < $grafik->ring4){
                    $row["a"]   = "*";
                }
                if($median->median >= $grafik->ring3 && $median->median < $grafik->ring4){
                  $row["color"] = '#1bbd20';
                }
                $row["y"]   = (float)$grafik->count_ring_3_param;
            array_push($rows[0], $row);
            $row['color'] = null;

                $row["name"]    = ">= $grafik->ring4 <= $grafik->ring5";
                if($dataReg->hasil_pemeriksaan >= $grafik->ring4 && $dataReg->hasil_pemeriksaan <= $grafik->ring5){
                    $row["a"]   = "*";
                }
                if($median->median >= $grafik->ring4 && $median->median <= $grafik->ring5){
                  $row["color"] = '#1bbd20';
                }
                $row["y"]   = (float)$grafik->count_ring_4_param;
            array_push($rows[0], $row);
            $row['color'] = null;

                $row["name"]    = "> $grafik->ring5";
                if($dataReg->hasil_pemeriksaan > $grafik->ring5){
                    $row["a"]   = "*";
                }
              $row["color"] = '#BF0B23';
                $row["y"]   = (float)$grafik->out_atas_param;
            array_push($rows[0], $row);
            $row['color'] = null;
            /*Grapik 2*/

              $row["name"] = "< $grafik->ring1";
              if($dataReg->hasil_pemeriksaan < $grafik->ring1){
                $row["a"] = "*";
              }
              $row["color"] = '#BF0B23';
              $row["y"] = (float)$grafik->out_bawah_alat;
            array_push($rows[1], $row);
            $row['color'] = null;

                $row["name"] = ">= $grafik->ring1 < $grafik->ring2";
                if($dataReg->hasil_pemeriksaan >= $grafik->ring1 && $dataReg->hasil_pemeriksaan < $grafik->ring2){
                    $row["a"] = "*";
                }
                if(!empty($medianalat) && $medianalat->median >= $grafik->ring1 && $medianalat->median < $grafik->ring2){
                  $row["color"] = '#1bbd20';
                }
                $row["y"] = (float)$grafik->count_ring_1_alat;
            array_push($rows[1], $row);
            $row['color'] = null;

                $row["name"] = ">= $grafik->ring2 < $grafik->ring3";
                if($dataReg->hasil_pemeriksaan >= $grafik->ring2 && $dataReg->hasil_pemeriksaan < $grafik->ring3){
                    $row["a"] = "*";
                }
                if(!empty($medianalat) && $median->median >= $grafik->ring2 && $median->median < $grafik->ring3){
                  $row["color"] = '#1bbd20';
                }
                $row["y"] = (float)$grafik->count_ring_2_alat;
            array_push($rows[1], $row);
            $row['color'] = null;

                $row["name"] = ">= $grafik->ring3 < $grafik->ring4";
                if($dataReg->hasil_pemeriksaan >= $grafik->ring3 && $dataReg->hasil_pemeriksaan < $grafik->ring4){
                    $row["a"] = "*";
                }
                if(!empty($medianalat) && $median->median >= $grafik->ring3 && $median->median < $grafik->ring4){
                  $row["color"] = '#1bbd20';
                }
                $row["y"] = (float)$grafik->count_ring_3_alat;
            array_push($rows[1], $row);
            $row['color'] = null;

                $row["name"] = ">= $grafik->ring4 <= $grafik->ring5";
                if($dataReg->hasil_pemeriksaan >= $grafik->ring4 && $dataReg->hasil_pemeriksaan <= $grafik->ring5){
                    $row["a"] = "*";
                }
                if(!empty($medianalat) && $medianalat->median >= $grafik->ring4 && $medianalat->median <= $grafik->ring5){
                  $row["color"] = '#1bbd20';
                }
                $row["y"] = (float)$grafik->count_ring_4_alat;
            array_push($rows[1], $row);
            $row['color'] = null;

                $row["name"] = "> $grafik->ring5";
                if($dataReg->hasil_pemeriksaan > $grafik->ring5){
                    $row["a"] = "*";
                }
              $row["color"] = '#BF0B23';
                $row["y"] = (float)$grafik->out_atas_alat;
            array_push($rows[1], $row);
            $row['color'] = null;
            /*Grapik 3*/



              $row["name"]  = "< $grafik->ring1";
              if($dataReg->hasil_pemeriksaan < $grafik->ring1){
                $row["a"]     = "*";
              }
              $row["color"]     = '#BF0B23';
              $row["y"]     = (float)$grafik->out_bawah_metode;
            array_push($rows[2], $row);
            $row['color'] = null;

                $row["name"]    = ">= $grafik->ring1 < $grafik->ring2";
                if($dataReg->hasil_pemeriksaan >= $grafik->ring1 && $dataReg->hasil_pemeriksaan < $grafik->ring2){
                    $row["a"]   = "*";
                }
                if(!empty($medianmetode) && $medianmetode->median >= $grafik->ring1 && $medianmetode->median < $grafik->ring2){
                  $row["color"]     = '#1bbd20';
                }
                $row["y"]   = (float)$grafik->count_ring_1_metode;
            array_push($rows[2], $row);
            $row['color'] = null;

                $row["name"]    = ">= $grafik->ring2 < $grafik->ring3";
                if($dataReg->hasil_pemeriksaan >= $grafik->ring2 && $dataReg->hasil_pemeriksaan < $grafik->ring3){
                    $row["a"]   = "*";
                }
                if(!empty($medianmetode) && $medianmetode->median >= $grafik->ring2 && $medianmetode->median < $grafik->ring3){
                  $row["color"]     = '#1bbd20';
                }
                $row["y"]   = (float)$grafik->count_ring_2_metode;
            array_push($rows[2], $row);
            $row['color'] = null;

                $row["name"]    = ">= $grafik->ring3 < $grafik->ring4";
                if($dataReg->hasil_pemeriksaan >= $grafik->ring3 && $dataReg->hasil_pemeriksaan < $grafik->ring4){
                    $row["a"]   = "*";
                }
                if(!empty($medianmetode) && $medianmetode->median >= $grafik->ring3 && $medianmetode->median < $grafik->ring4){
                  $row["color"]     = '#1bbd20';
                }
                $row["y"]   = (float)$grafik->count_ring_3_metode;
            array_push($rows[2], $row);
            $row['color'] = null;

                $row["name"]    = ">= $grafik->ring4 <= $grafik->ring5";
                if($dataReg->hasil_pemeriksaan >= $grafik->ring4 && $dataReg->hasil_pemeriksaan <= $grafik->ring5){
                    $row["a"]   = "*";
                }
                if(!empty($medianmetode) && $medianmetode->median >= $grafik->ring4 && $medianmetode->median <= $grafik->ring5){
                  $row["color"]     = '#1bbd20';
                }
                $row["y"]   = (float)$grafik->count_ring_4_metode;
            array_push($rows[2], $row);
            $row['color'] = null;

                $row["name"]    = "> $grafik->ring5";
                if($dataReg->hasil_pemeriksaan > $grafik->ring5){
                    $row["a"]   = "*";
                }
              $row["color"]     = '#BF0B23';
                $row["y"]   = (float)$grafik->out_atas_metode;
            array_push($rows[2], $row);
            $row['color'] = null;



            return response($rows);
        // $median = DB::table('tb_sd_median')
        //         ->where('parameter', $input['parameter'])
        //         ->where('siklus', $input['siklus'])
        //         ->where('tipe', $input['type'])
        //         ->where('tahun', $input['tahun'])
        //         ->first();


        // $medianalat = DB::table('tb_sd_median_alat')
        //         ->where('alat', $dataReg->alat)
        //         ->where('siklus', $input['siklus'])
        //         ->where('tipe', $input['type'])
        //         ->where('tahun', $input['tahun'])
        //         ->first();



        // $medianmetode = DB::table('tb_sd_median_metode')
        //         ->where('parameter', $input['parameter'])
        //         ->where('metode', $dataReg->kode_metode_pemeriksaan)
        //         ->where('siklus', $input['siklus'])
        //         ->where('tipe', $input['type'])
        //         ->where('tahun', $input['tahun'])
        //         ->first();


    }

    public function hematologizscore(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $input = $request->all();
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $user = Auth::user()->id;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('tb_registrasi.status', '>=' , 2)
                ->get();

        $sik = Session::get('siklus');
        $hematologi = DB::table('parameter')->where('kategori','hematologi')->get();
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('sub_bidang.id', '=', '1')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query) use ($siklus)
                    {
                        if ($siklus == 1) {
                            $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done');
                        }else{
                            $query->where('rpr1', 'done')
                                ->orwhere('rpr2', 'done');
                        }
                    })
                ->get();

        $responSatu = DB::table('tb_pendapat_responden')->select('siklus','tahun')->where('tahun', $tanggal)->where('siklus','=', $siklus)->where('id_users', $user)->first();
        if(count($datas)){
            return view('data_evaluasi.hematologi', compact('data','dd','sik','data1','id','tanggal','type','siklus','date','datas','hematologi','tahunevaluasi','responSatu'));
        }else{
            return view('data_evaluasi/index', compact('data', 'data2','tanggal','siklus','tahunevaluasi'));
        }
        
    }

    public function kimiaklinik(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $input = $request->all();
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $user = Auth::user()->id;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('tb_registrasi.status', '>=' , 2)
                ->get();
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter','tb_registrasi.id as idregistrasi','sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('sub_bidang.id', '=', '2')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query) use ($siklus)
                    {
                        if ($siklus == 1) {
                            $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done');
                        }else{
                            $query->where('rpr1', 'done')
                                ->orwhere('rpr2', 'done');
                        }
                    })
                ->get();
        $responSatu = DB::table('tb_pendapat_responden')->select('siklus','tahun')->where('tahun', $tanggal)->where('siklus','=', $siklus)->where('id_users', $user)->first();

        if(count($datas)){
            return view('data_evaluasi.kimiaklinik', compact('skoring1','dd','tanggal','data','datas','id','type','siklus','tahunevaluasi','responSatu','date'));
        }else{
            return view('data_evaluasi/index', compact('data', 'data2','tanggal','siklus','tahunevaluasi'));
        }
    }

    public function cetak(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $input = $request->all();
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', $type)
                ->where('tb_registrasi.id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->select('tb_instrumen.id as idalat', 'metode_pemeriksaan.id as idmetode',  'hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id', 'hp_details.instrument_lain', 'hp_details.alat as Instrumen', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                ->get();
        foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median')
                    ->where('parameter', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('tipe', $type)
                    ->where('form', '=', 'kimia klinik')
                    ->get();
            $val->sd = $sd;
            $sdalat = DB::table('tb_sd_median_alat')
                    ->where('alat', $val->idalat)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('tipe', $type)
                    ->where('form', '=', 'kimia klinik')
                    ->get();
            $val->sdalat = $sdalat;
            $sdmetode = DB::table('tb_sd_median_metode')
                    ->where('metode', $val->idmetode)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('tipe', $type)
                    ->where('form', '=', 'kimia klinik')
                    ->get();
            $val->sdmetode = $sdmetode;
        }
         $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();
        $tanggal = Date('Y m d');


        $zscoremetode = DB::table('tb_zscore_metode')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('type', $type)
                    ->where('id_registrasi', $id)
                    ->get();
        $zscore = DB::table('tb_zscore')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('type', $type)
                    ->where('id_registrasi', $id)
                    ->get();
        $zscorealat = DB::table('tb_zscore_alat')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('type', $type)
                    ->where('id_registrasi', $id)
                    ->get();
         // dd($data[0]->sd[0]->median);
        $pdf = PDF::loadview('evaluasi/kimiaklinik/zscore/print/cetak', compact('data','tanggal', 'datas','input', 'date', 'type','zscore', 'zscorealat', 'zscoremetode','id','siklus'))
            ->setPaper('a4', 'landscape')
            ->setwarnings(false);
             return $pdf->stream('KimiaKlinik.pdf');
    }

    public function cetakzscore(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $tanggal = date('Y m d');


        $input = $request->all();
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', $type)
                ->where('tb_registrasi.id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                ->get();
        foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median')
                    ->where('parameter', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('tipe', $type)
                    ->where('form', '=', 'hematologi')
                    ->get();
            $val->sd = $sd;
        }
        $zscore = DB::table('tb_zscore')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('type', $type)
                    ->where('id_registrasi', $id)
                    ->get();

        $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();

         // dd($data);
        if (count($zscore) > 0) {
            $pdf = PDF::loadview('evaluasi/hematologi/zscore/print/cetak', compact('tanggal','data','datas', 'input', 'date', 'type','zscore','id','siklus'))
            ->setPaper('a4', 'potrait')
        ->setwarnings(false);
             return $pdf->stream('Hematologi.pdf');
        }else{
            Session::flash('message', 'Hematologi belum dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
    }

    public function bta(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $input = $request->all();
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $user = Auth::user()->id;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();

        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('tb_registrasi.status', '>=' , 2)
                ->get();

        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->where('sub_bidang.id', '=', '4')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2);
                    })
                ->get();

         // dd($data);
        $responSatu = DB::table('tb_pendapat_responden')->select('siklus','tahun')->where('tahun', $tanggal)->where('siklus','=', $siklus)->where('id_users', $user)->first();

        if(count($datas)){
            // if (count($responSatu)) {
                return view('data_evaluasi.bta', compact('data','tanggal','dd','datas','id','type','siklus','tahunevaluasi','responSatu'));
            // }else{
            //     Session::flash('message', 'Silahkan isi survey untuk melihat data evaluasi!');
            //     Session::flash('alert-class', 'alert-danger');
            //     return redirect('pendapat/pendapat-responden'); 
            // }
        }else{
            return view('data_evaluasi/index', compact('data', 'data2','tanggal','siklus','tahunevaluasi'));
        }
    }

    public function tcc(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $input = $request->all();
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $user = Auth::user()->id;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('tb_registrasi.status', '>=' , 2)
                ->get();

        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->where('sub_bidang.id', '=', '5')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2);
                    })
                ->get();

         // dd($data);
        $responSatu = DB::table('tb_pendapat_responden')->select('siklus','tahun')->where('tahun', $tanggal)->where('siklus','=', $siklus)->where('id_users', $user)->first();

        if(count($datas)){
            // if (count($responSatu)) {
                return view('data_evaluasi.tcc', compact('data','tanggal','datas','id','type','siklus','tahunevaluasi','responSatu'));
            // }else{
            //     Session::flash('message', 'Silahkan isi survey untuk melihat data evaluasi!');
            //     Session::flash('alert-class', 'alert-danger');
            //     return redirect('pendapat/pendapat-responden'); 
            // }
        }else{
            return view('data_evaluasi/index', compact('data', 'data2','tanggal','siklus','tahunevaluasi'));
        }
    }

    public function hiv(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $input = $request->all();
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $user = Auth::user()->id;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();

        $perusahaan = DB::table('perusahaan')
                    ->join('users', 'users.id', '=', 'perusahaan.created_by')
                    ->where('users.id', Auth::user()->id)
                    ->first();
        
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('tb_registrasi.status', '>=' , 2)
                ->get();
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','perusahaan.pemerintah')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->where('tb_registrasi.bidang','=', '6')
                ->where('sub_bidang.id_bidang', '=', '7')
                ->where('tb_registrasi.status', 3)
                ->where(function($query){
                    if (Auth::user()->badan_usaha == '9') {
                        $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                    }
                })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2);
                    })
                ->get();
        
        $grafikbidang1 = DB::table('sub_bidang')->where('id','6')->first();
        $grafikdata1 = DB::table('badan_usaha')->get();
        foreach($grafikdata1 as $skey => $r)
        {
            $semua = DB::table('tb_registrasi')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where('tb_registrasi.status','>=','2')
                        ->where('tb_registrasi.bidang','=','6')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->where(function($query) use ($perusahaan){
                            if ($perusahaan->pemerintah == '9') {
                                $query->where('perusahaan.pemerintah','=','9');
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        })
                        ->count();
            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang','=','6')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                            ->where(function($query) use ($perusahaan){
                                if ($perusahaan->pemerintah == '9') {
                                    $query->where('perusahaan.pemerintah','=','9');
                                }else{
                                    $query->where('perusahaan.pemerintah','!=','9');
                                }
                            })
                            ->count();
            $r->persen = $peserta / $semua * 100;
        }

        $grafikdata2 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query) use ($perusahaan){
                        if ($perusahaan->pemerintah == '9') {
                            $query->where('perusahaan.pemerintah','=','9');
                        }else{
                            $query->where('perusahaan.pemerintah','!=','9');
                        }
                    })
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->groupBy('tb_reagen_imunologi.id')
                    ->orderBy('jumlah', 'desc')
                    ->get();
        $lainlain4 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query) use ($perusahaan){
                        if ($perusahaan->pemerintah == '9') {
                            $query->where('perusahaan.pemerintah','=','9');
                        }else{
                            $query->where('perusahaan.pemerintah','!=','9');
                        }
                    })
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->where('tb_reagen_imunologi.reagen', '=', 'Lain - lain')
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'reagen_imunologi.reagen_lain','master_imunologi.jenis_form')
                    ->groupBy('reagen_imunologi.reagen_lain')
                    ->orderBy('jumlah', 'desc')
                    ->get();

        $grafiktotal2 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query) use ($perusahaan){
                        if ($perusahaan->pemerintah == '9') {
                            $query->where('perusahaan.pemerintah','=','9');
                        }else{
                            $query->where('perusahaan.pemerintah','!=','9');
                        }
                    })
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->count();

        $grafikdata3 = DB::table('badan_usaha')
                ->where(function($query) use ($perusahaan){
                    if ($perusahaan->pemerintah == '9') {
                        $query->where('id','=','9');
                    }else{
                        $query->where('id','!=','9');
                    }
                })
                ->get();
        foreach ($grafikdata3 as $key => $val) {
            $baik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                        ->where('tb_registrasi.bidang', '=', '6')
                        ->where('perusahaan.pemerintah', $val->id)
                        ->count();
            $tidakbaik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                        ->where('tb_registrasi.bidang', '=', '6')
                        ->where('perusahaan.pemerintah', $val->id)
                        ->count();
            $val->baik = $baik;
            $val->tidakbaik = $tidakbaik;
        }


        $grafikbaik4 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query) use ($perusahaan){
                        if ($perusahaan->pemerintah == '9') {
                            $query->where('perusahaan.pemerintah','=','9');
                        }else{
                            $query->where('perusahaan.pemerintah','!=','9');
                        }
                    })
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->count();
        $grafikpesertabaik4 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakbaik4 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where(function($query) use ($perusahaan){
                        if ($perusahaan->pemerintah == '9') {
                            $query->where('perusahaan.pemerintah','=','9');
                        }else{
                            $query->where('perusahaan.pemerintah','!=','9');
                        }
                    })
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->count();
        $grafikpesertatidakbaik4 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakdapatnilai4 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query) use ($perusahaan){
                        if ($perusahaan->pemerintah == '9') {
                            $query->where('perusahaan.pemerintah','=','9');
                        }else{
                            $query->where('perusahaan.pemerintah','!=','9');
                        }
                    })
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->count();
        $grafikpesertatidakdapatdinilai4 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();

        $grafikdata5 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query) use ($perusahaan){
                        if ($perusahaan->pemerintah == '9') {
                            $query->where('perusahaan.pemerintah','=','9');
                        }else{
                            $query->where('perusahaan.pemerintah','!=','9');
                        }
                    })
                    ->where('tb_registrasi.bidang', '=', '6')
                    ->where('master_imunologi.siklus', $siklus)
                    ->groupBy('reagen_imunologi.nama_reagen')
                    ->select('tb_reagen_imunologi.reagen','reagen_imunologi.nama_reagen')
                    ->get();

        foreach ($grafikdata5 as $key => $val) {
            $total = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                        ->where(function($query) use ($perusahaan){
                            if ($perusahaan->pemerintah == '9') {
                                $query->where('perusahaan.pemerintah','=','9');
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        })
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->total = $total;
            $sesuai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                        ->where(function($query) use ($perusahaan){
                            if ($perusahaan->pemerintah == '9') {
                                $query->where('perusahaan.pemerintah','=','9');
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        })
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Baik')
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->sesuai = $sesuai;
            $tidaksesuai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                        ->where(function($query) use ($perusahaan){
                            if ($perusahaan->pemerintah == '9') {
                                $query->where('perusahaan.pemerintah','=','9');
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        })
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Kurang')
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->tidaksesuai = $tidaksesuai;
            $tidakdinilai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                        ->where(function($query) use ($perusahaan){
                            if ($perusahaan->pemerintah == '9') {
                                $query->where('perusahaan.pemerintah','=','9');
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        })
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Tidak dapat dinilai')
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->tidakdinilai = $tidakdinilai;
        }

        $responSatu = DB::table('tb_pendapat_responden')->select('siklus','tahun')->where('tahun', $tanggal)->where('siklus','=', $siklus)->where('id_users', $user)->first();

        if(count($datas)){
            // if (count($responSatu)) {
                return view('data_evaluasi.hiv', compact('data','tanggal','datas','id','type','siklus','tahunevaluasi', 'perusahaan','grafikbidang1','grafikdata1', 'grafikdata2', 'grafiktotal2', 'grafikdata3', 'grafikbaik4', 'grafiktidakbaik4', 'grafikpesertabaik4', 'grafikpesertatidakbaik4', 'grafikpesertatidakdapatdinilai4', 'grafiktidakdapatnilai4', 'grafikdata5','lainlain4','responSatu'));
            // }else{
            //     Session::flash('message', 'Silahkan isi survey untuk melihat data evaluasi!');
            //     Session::flash('alert-class', 'alert-danger');
            //     return redirect('pendapat/pendapat-responden'); 
            // }
        }else{
            return view('data_evaluasi/index', compact('data', 'data2','tanggal','siklus','tahunevaluasi'));
        }
    }

     public function sif(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $input = $request->all();
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $user = Auth::user()->id;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('tb_registrasi.status', '>=' , 2)
                ->get();

        $data =  DB::table('tb_registrasi')
                    ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                    ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                    ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                    ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','perusahaan.pemerintah')
                    ->where('sub_bidang.id_bidang', '=', '7')
                    ->where('tb_registrasi.bidang','=','7')
                    ->where('tb_registrasi.created_by','=', Auth::user()->id)
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                    ->where('tb_registrasi.status', 3)
                    ->where(function($query)
                        {
                            $query->where('pemeriksaan2', 'done')
                                    ->orwhere('pemeriksaan', 'done')
                                    ->orwhere('siklus_1', 'done')
                                    ->orwhere('siklus_2', 'done')
                                    ->orwhere('status_data1', 2)
                                    ->orwhere('status_data2', 2);
                        })
                    ->get();
        // dd($data);
        
        $grafikbidang1 = DB::table('sub_bidang')->where('id','7')->first();
        $grafikdata1 = DB::table('badan_usaha')->get();
        foreach($grafikdata1 as $skey => $r)
        {
            $semua = DB::table('tb_registrasi')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where('tb_registrasi.status','>=','2')
                        ->where('tb_registrasi.bidang','=','7')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->count();
            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang','=','7')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                            ->count();
            $r->persen = $peserta / $semua * 100;
        }

        $grafikdata2 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->groupBy('tb_reagen_imunologi.id')
                    ->orderBy('jumlah', 'desc')
                    ->get();
        $lainlain4 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->where('tb_reagen_imunologi.reagen', '=', 'Lain - lain')
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'reagen_imunologi.reagen_lain','master_imunologi.jenis_form')
                    ->groupBy('reagen_imunologi.reagen_lain')
                    ->orderBy('jumlah', 'desc')
                    ->get();
        $grafiktotal2 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->count();
    
        $grafikdata3 = DB::table('badan_usaha')->get();
        foreach ($grafikdata3 as $key => $val) {
            $baik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.type','=', 'Syphilis')
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                        ->where('tb_registrasi.bidang', '=', '7')
                        ->where('perusahaan.pemerintah',$val->id)
                        ->count();
            $tidakbaik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.type','=', 'Syphilis')
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                        ->where('tb_registrasi.bidang', '=', '7')
                        ->where('perusahaan.pemerintah',$val->id)
                        ->count();

            $baikrpr = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.type','=', 'rpr-syphilis')
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                        ->where('tb_registrasi.bidang', '=', '7')
                        ->where('perusahaan.pemerintah',$val->id)
                        ->count();
            $tidakbaikrpr = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.type','=', 'rpr-syphilis')
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                        ->where('tb_registrasi.bidang', '=', '7')
                        ->where('perusahaan.pemerintah',$val->id)
                        ->count();

            $val->baikrpr = $baikrpr;
            $val->tidakbaikrpr = $tidakbaikrpr;
            $val->baik = $baik;
            $val->tidakbaik = $tidakbaik;
        }

        $grafikdata4 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where('master_imunologi.siklus', $siklus)
                    ->groupBy('reagen_imunologi.nama_reagen', 'master_imunologi.jenis_form')
                    ->select('tb_reagen_imunologi.reagen','reagen_imunologi.nama_reagen', 'master_imunologi.jenis_form')
                    ->get();
        foreach ($grafikdata4 as $key => $val) {
            $total = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->count();
            $val->total = $total;
            $sesuai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Baik')
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->sesuai = $sesuai;
            $tidaksesuai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Kurang')
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->tidaksesuai = $tidaksesuai;
            $tidakdinilai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Tidak dapat dinilai')
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->tidakdinilai = $tidakdinilai;
        }


        $grafikbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_kesimpulan_evaluasi.type','=', 'Syphilis')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->count();
        $grafikpbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_kesimpulan_evaluasi.type','=', 'Syphilis')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->where('tb_kesimpulan_evaluasi.type', 'Syphilis')
                    ->count();
        $grafikptidakbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->where('tb_kesimpulan_evaluasi.type','=', 'Syphilis')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakdapatnilai5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->where('tb_kesimpulan_evaluasi.type', 'Syphilis')
                    ->count();
        $grafikptidakdapatnilai5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->where('tb_kesimpulan_evaluasi.type','=', 'Syphilis')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();

        $grafikbaikrpr5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_kesimpulan_evaluasi.type','=', 'rpr-syphilis')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->count();
        $grafikpbaikrpr5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_kesimpulan_evaluasi.type','=', 'rpr-syphilis')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakbaikrpr5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->where('tb_kesimpulan_evaluasi.type', 'rpr-syphilis')
                    ->count();
        $grafikptidakbaikrpr5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->where('tb_kesimpulan_evaluasi.type','=', 'rpr-syphilis')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakdapatnilairpr5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->where('tb_kesimpulan_evaluasi.type', 'rpr-syphilis')
                    ->count();
        $grafikptidakdapatnilairpr5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->where('tb_kesimpulan_evaluasi.type','=', 'rpr-syphilis')
                    ->where('tb_registrasi.bidang', '=', '7')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();

        $type = $request->get('y');
        
        $tahun = $q2->year;
        // $catatan = CatatanImun::where('id_registrasi', $id)->where('siklus', $type)->where('tahun', $tahun)->get();
        // dd($catatan)->first();

        $responSatu = DB::table('tb_pendapat_responden')->select('siklus','tahun')->where('tahun', $tanggal)->where('siklus','=', $siklus)->where('id_users', $user)->first();

        if(count($datas)){
            // if (count($responSatu)) {
                return view('data_evaluasi.sif', compact('data','tanggal','datas','id','type','siklus','tahunevaluasi', 'grafikbidang1', 'grafikdata1', 'grafikdata2', 'grafiktotal2', 'grafikdata3', 'grafikdata4', 'grafikbaik5', 'grafiktidakbaik5', 'grafiktidakdapatnilai5', 'grafikbaikrpr5', 'grafiktidakbaikrpr5', 'grafiktidakdapatnilairpr5','grafikptidakbaik5', 'grafikptidakdapatnilairpr5', 'grafikptidakbaikrpr5', 'grafikpbaikrpr5','grafikptidakdapatnilai5', 'grafikpbaik5','lainlain4','responSatu'));
            // }else{
            //     Session::flash('message', 'Silahkan isi survey untuk melihat data evaluasi!');
            //     Session::flash('alert-class', 'alert-danger');
            //     return redirect('pendapat/pendapat-responden'); 
            // }
        }else{
            return view('data_evaluasi/index', compact('data', 'data2','tanggal','siklus','tahunevaluasi'));
        }
    }

    public function hbs(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $input = $request->all();
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $user = Auth::user()->id;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('tb_registrasi.status', '>=' , 2)
                ->get();

        $data =  DB::table('tb_registrasi')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','perusahaan.pemerintah')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->where('sub_bidang.id_bidang', '=', '7')
                ->where('tb_registrasi.status', 3)
                ->where(function($query){
                    if (Auth::user()->badan_usaha == '9') {
                        $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                    }
                })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2);
                    })
                ->get();
        
        $grafikbidang1 = DB::table('sub_bidang')->where('id','8')->first();
        $grafikdata1 = DB::table('badan_usaha')->get();
        foreach($grafikdata1 as $skey => $r)
        {
            $semua = DB::table('tb_registrasi')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where('tb_registrasi.status','>=','2')
                        ->where('tb_registrasi.bidang','=','8')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->count();
            $peserta = DB::table('tb_registrasi')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where('perusahaan.pemerintah','=',$r->id)
                        ->where('tb_registrasi.status','>=','2')
                        ->where('tb_registrasi.bidang','=','8')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->count();
            $r->persen = $peserta / $semua * 100;
        }

        $grafikdata2 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->groupBy('tb_reagen_imunologi.id')
                    ->orderBy('jumlah', 'desc')
                    ->get();
        $lainlain4 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->where('tb_reagen_imunologi.reagen', '=', 'Lain - lain')
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'reagen_imunologi.reagen_lain','master_imunologi.jenis_form')
                    ->groupBy('reagen_imunologi.reagen_lain')
                    ->orderBy('jumlah', 'desc')
                    ->get();
        // dd($lainlain4);
        $grafiktotal2 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->count();

        $grafikdata3 = DB::table('badan_usaha')->get();
        foreach ($grafikdata3 as $key => $val) {
            $baik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                        ->where('tb_registrasi.bidang', '=', '8')
                        ->where('perusahaan.pemerintah', $val->id)
                        ->count();
            $tidakbaik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                        ->where('tb_registrasi.bidang', '=', '8')
                        ->where('perusahaan.pemerintah', $val->id)
                        ->count();
            $val->baik = $baik;
            $val->tidakbaik = $tidakbaik;
        }

        $grafikdata4 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where('master_imunologi.siklus', $siklus)
                    ->groupBy('reagen_imunologi.nama_reagen', 'master_imunologi.jenis_form')
                    ->select('tb_reagen_imunologi.reagen','reagen_imunologi.nama_reagen', 'master_imunologi.jenis_form')
                    ->get();
        foreach ($grafikdata4 as $key => $val) {
            $total = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->total = $total;
            $sesuai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Baik')
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->sesuai = $sesuai;
            $tidaksesuai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Kurang')
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->tidaksesuai = $tidaksesuai;
            $tidakdinilai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Tidak dapat dinilai')
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->tidakdinilai = $tidakdinilai;
        }
        $grafikbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->count();
        $grafikpbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->count();
        $grafikptidakbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakdapatnilai5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->count();
        $grafikptidakdapatnilai5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();

        // dd($grafikdata2);

        $responSatu = DB::table('tb_pendapat_responden')->select('siklus','tahun')->where('tahun', $tanggal)->where('siklus','=', $siklus)->where('id_users', $user)->first();

        if(count($datas)){
            // if (count($responSatu)) {
                return view('data_evaluasi.hbs', compact('datas','tanggal','data','id','type','siklus','tahunevaluasi', 'grafikbidang1', 'grafikdata1', 'grafikdata2', 'grafiktotal2', 'grafikdata3', 'grafikdata4', 'grafikbaik5', 'grafiktidakbaik5', 'grafiktidakdapatnilai5', 'grafikpbaik5', 'grafikptidakbaik5', 'grafikptidakdapatnilai5','lainlain4','responSatu'));
            // }else{
            //     Session::flash('message', 'Silahkan isi survey untuk melihat data evaluasi!');
            //     Session::flash('alert-class', 'alert-danger');
            //     return redirect('pendapat/pendapat-responden'); 
            // }
        }else{
            return view('data_evaluasi/index', compact('data', 'data2','tanggal','siklus','tahunevaluasi'));
        }
    }

    public function hcv(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $input = $request->all();
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $user = Auth::user()->id;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('tb_registrasi.status', '>=' , 2)
                ->get();
        $data =  DB::table('tb_registrasi')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','perusahaan.pemerintah')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->where('sub_bidang.id_bidang', '=', '7')
                ->where('tb_registrasi.status', 3)
                ->where(function($query){
                    if (Auth::user()->badan_usaha == '9') {
                        $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                    }
                })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2);
                    })
                ->get();

        $grafikbidang1 = DB::table('sub_bidang')->where('id','9')->first();
        $grafikdata1 = DB::table('badan_usaha')->get();
        foreach($grafikdata1 as $skey => $r)
        {
            $semua = DB::table('tb_registrasi')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where('tb_registrasi.status','>=','2')
                        ->where('tb_registrasi.bidang','=','9')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->count();
            $peserta = DB::table('tb_registrasi')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where('perusahaan.pemerintah','=',$r->id)
                        ->where('tb_registrasi.status','>=','2')
                        ->where('tb_registrasi.bidang','=','9')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->count();
            $r->persen = $peserta / $semua * 100;
        }

        $grafikdata2 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->groupBy('tb_reagen_imunologi.id')
                    ->orderBy('jumlah', 'desc')
                    ->get();
        $lainlain4 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '8')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->where('tb_reagen_imunologi.reagen', '=', 'Lain - lain')
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'reagen_imunologi.reagen_lain','master_imunologi.jenis_form')
                    ->groupBy('reagen_imunologi.reagen_lain')
                    ->orderBy('jumlah', 'desc')
                    ->get();

        $grafiktotal2 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $date)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->count();

        $grafikdata3 = DB::table('badan_usaha')->get();
        foreach ($grafikdata3 as $key => $val) {
            $baik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                        ->where('tb_registrasi.bidang', '=', '9')
                        ->where('perusahaan.pemerintah', $val->id)
                        ->count();
            $tidakbaik = DB::table('tb_kesimpulan_evaluasi')
                        ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                        ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                        ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                        ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                        ->where('tb_registrasi.bidang', '=', '9')
                        ->where('perusahaan.pemerintah', $val->id)
                        ->count();
            $val->baik = $baik;
            $val->tidakbaik = $tidakbaik;
        }

        $grafikdata4 = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->where('master_imunologi.siklus', $siklus)
                    ->groupBy('reagen_imunologi.nama_reagen', 'master_imunologi.jenis_form')
                    ->select('tb_reagen_imunologi.reagen','reagen_imunologi.nama_reagen', 'master_imunologi.jenis_form')
                    ->get();
        foreach ($grafikdata4 as $key => $val) {
            $total = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->total = $total;
            $sesuai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Baik')
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->sesuai = $sesuai;
            $tidaksesuai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Kurang')
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->tidaksesuai = $tidaksesuai;
            $tidakdinilai = DB::table('master_imunologi')
                        ->join('reagen_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_kesimpulan_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Tidak dapat dinilai')
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_kesimpulan_evaluasi.siklus', $siklus)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->tidakdinilai = $tidakdinilai;
        }

        $grafikbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->count();
        $grafikpbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->count();
        $grafikptidakbaik5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();
        $grafiktidakdapatnilai5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $date)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->count();
        $grafikptidakdapatnilai5 = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->where('tb_registrasi.bidang', '=', '9')
                    ->where('tb_kesimpulan_evaluasi.id_registrasi', $id)
                    ->count();

        $responSatu = DB::table('tb_pendapat_responden')->select('siklus','tahun')->where('tahun', $tanggal)->where('siklus','=', $siklus)->where('id_users', $user)->first();

        if(count($datas)){
            // if (count($responSatu)) {
                return view('data_evaluasi.hcv', compact('data','id','type','tanggal','siklus','tahunevaluasi','datas', 'grafikbidang1', 'grafikdata1', 'grafikdata2', 'grafiktotal2', 'grafikdata3', 'grafikdata4', 'grafikbaik5', 'grafiktidakbaik5', 'grafiktidakdapatnilai5', 'grafikpbaik5', 'grafikptidakbaik5', 'grafikptidakdapatnilai5','lainlain4','responSatu'));
            // }else{
            //     Session::flash('message', 'Silahkan isi survey untuk melihat data evaluasi!');
            //     Session::flash('alert-class', 'alert-danger');
            //     return redirect('pendapat/pendapat-responden'); 
            // }
        }else{
            return view('data_evaluasi/index', compact('data', 'data2','tanggal','siklus','tahunevaluasi'));
        }
    }

     public function mal(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $input = $request->all();
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $user = Auth::user()->id;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('tb_registrasi.status', '>=' , 2)
                ->get();
        $data =  DB::table('tb_registrasi')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->where('sub_bidang.id', '=', '10')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2);
                    })
                ->get();
         // dd($data);

        $responSatu = DB::table('tb_pendapat_responden')->select('siklus','tahun')->where('tahun', $tanggal)->where('siklus','=', $siklus)->where('id_users', $user)->first();
        // dd($responSatu);
        if(count($datas)){
            // if (count($responSatu)) {
                return view('data_evaluasi.mal', compact('data','id','type','siklus','tahunevaluasi','datas','tanggal','responSatu'));
            // }else{
            //     Session::flash('message', 'Silahkan isi survey untuk melihat data evaluasi!');
            //     Session::flash('alert-class', 'alert-danger');
            //     return redirect('pendapat/pendapat-responden'); 
            // }
        }else{
            return view('data_evaluasi/index', compact('data', 'data2','tanggal','siklus','tahunevaluasi'));
        }
    }

     public function bac(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $input = $request->all();
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $user = Auth::user()->id;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        // dd($tahunevaluasi);
        
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('tb_registrasi.status', '>=' , 2)
                ->get();

        $data =  DB::table('tb_registrasi')
            ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
            ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
            ->where('tb_registrasi.created_by','=', Auth::user()->id)
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
            ->where('sub_bidang.id', '=', '13')
            ->where('tb_registrasi.status', 3)
            ->where(function($query)
                {
                    $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                })
            ->get();

        $responSatu = DB::table('tb_pendapat_responden')->select('siklus','tahun')->where('tahun', $tanggal)->where('siklus','=', $siklus)->where('id_users', $user)->first();

        if(count($datas)){
            // if (count($responSatu)) {
                return view('data_evaluasi.bac', compact('data','datas','dd','id','type','siklus','tahunevaluasi','datas','data1','tanggal','responSatu'));
            // }else{
            //     Session::flash('message', 'Silahkan isi survey untuk melihat data evaluasi!');
            //     Session::flash('alert-class', 'alert-danger');
            //     return redirect('pendapat/pendapat-responden'); 
            // }
        }else{
            return view('data_evaluasi/index', compact('data', 'data2','tanggal','siklus','tahunevaluasi'));
        }
         // dd($data);
    }
     public function uri(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $input = $request->all();
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $user = Auth::user()->id;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('tb_registrasi.status', '>=' , 2)
                ->get();
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query) use ($siklus)
                    {
                        if ($siklus == 1) {
                            $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done');
                        }else{
                            $query->where('rpr1', 'done')
                                ->orwhere('rpr2', 'done');
                        }
                    })
                ->get();
        $evaluasi1 = DB::table('tb_skoring_urinalisa')->where('id_registrasi', $id)->where('siklus', '=', $siklus)->where('type','=','a')->first();
        $evaluasi2 = DB::table('tb_skoring_urinalisa')->where('id_registrasi', $id)->where('siklus', '=', $siklus)->where('type','=','b')->first();
        // dd($evaluasi1);

        $responSatu = DB::table('tb_pendapat_responden')->select('siklus','tahun')->where('tahun', $tanggal)->where('siklus','=', $siklus)->where('id_users', $user)->first();

        if(count($datas)){
            return view('data_evaluasi.uri', compact('data','datas','tanggal','id','type','siklus','tahunevaluasi', 'evaluasi1', 'evaluasi2','responSatu'));
        }else{
            return view('data_evaluasi/index', compact('data', 'data2','tanggal','siklus','tahunevaluasi'));
        }
    }

     public function evaluasi(\Illuminate\Http\Request $request, $id){
        $input = $request->all();
        $user = Auth::user()->id;
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $dd = $request->tanggal;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
        } else {
            $dd = $request->session()->get('tanggal');
        }
        $tanggal = $dd;

        $instrumen = DB::table('tb_instrumen')->where('id_parameter', 'urinalisa')->get();
        $datas1=  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias')
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $dd)
                ->groupBy('tb_registrasi.bidang')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();

        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->leftjoin('tb_reagen_imunologi', 'hp_details.reagen', '=', 'tb_reagen_imunologi.id')
            ->leftjoin('tb_instrumen','hp_details.alat', '=', 'tb_instrumen.id')
            ->leftjoin('tb_hp','hp_details.hasil_pemeriksaan', '=', 'tb_hp.id')
            ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.alat','tb_instrumen.id as idinstrumen', 'tb_instrumen.instrumen','hp_details.instrument_lain', 'hp_details.id as id_detail','hp_details.reagen as hp_reagen','tb_reagen_imunologi.reagen as imu_reagen','hp_details.reagen_lain', 'tb_hp.hp')
            ->where('parameter.kategori', 'urinalisa')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->get();
            foreach ($data as $key => $val) {
                $metode = MetodePemeriksaan::where('parameter_id', $val->id)->get();
                $val->metode = $metode;
                $reagen = ReagenImunologi::where('parameter_id', $val->id)->get();
                $val->reagen = $reagen;
                $hasilPemeriksaan = TbHp::where('parameter_id', $val->id)->get();
                $val->hasilPemeriksaan = $hasilPemeriksaan;
                $target = Rujukanurinalisa::where('parameter', $val->id)
                                            ->where('siklus', $siklus)
                                            ->where('tahun', $date)
                                            ->where('type', $type)
                                            ->get();
                $val->target = $target;
            }

        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        if (count($data[10]->target)) {
            return view('data_evaluasi/urinput', compact('data','dd','simpan', 'perusahaan', 'datas1', 'type','siklus','date','instrumen', 'tanggal', 'skoring','id', 'input','tanggal'));
        }else{
            Session::flash('message', 'Data Rujukan Belum Lengkap!');
            Session::flash('alert-class', 'alert-danger');
            return redirect::back();
        }
    }

     public function evaluasi1(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->y;
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $years = $q2->year;

        $register = Register::find($id);
        $user = Auth::user()->id;

        $dd = $request->tanggal;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
        } else {
            $dd = $request->session()->get('tanggal');
        }
        $tanggal = $dd;


        $datas=  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias')
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $dd)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', '>=', 2)
                // ->where(function($query)
                //     {
                //         $query->where('status_data1', 2)
                //             ->orwhere('status_data2', 2)
                //             ->orwhere('status_datarpr1', 2)
                //             ->orwhere('status_datarpr2', 2);
                //     })
                // ->where(function($query)
                //     {
                //         $query->where('pemeriksaan2', '=' , 'done')
                //                 ->orwhere('pemeriksaan', '=' , 'done')
                //                 ->orwhere('siklus_1', '=' , 'done')
                //                 ->orwhere('siklus_2', '=' , 'done');
                //     })
                ->get();

        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('sub_bidang.id', '=', '13')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2)
                                ->orwhere('status_datarpr1', 2)
                                ->orwhere('status_datarpr2', 2);
                    })
                ->get();

            $data1 =  DB::table('tb_registrasi')
            ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
            ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
            ->where('tb_registrasi.created_by','=', Auth::user()->id)
            ->where('sub_bidang.id', '=', '13')
            ->where('tb_registrasi.status', 3)
            ->where(function($query)
                {
                    $query->where('status_data1', 2)
                        ->orwhere('status_data2', 2)
                        ->orwhere('status_datarpr1', 2)
                        ->orwhere('status_datarpr2', 2);
                })
            ->where(function($query)
                {
                    $query->where('pemeriksaan2', '=' , 'done')
                            ->orwhere('pemeriksaan', '=' , 'done')
                            ->orwhere('siklus_1', '=' , 'done')
                            ->orwhere('siklus_2', '=' , 'done');
                })
            ->get();
        // dd($data);
        // dd($data);
        return view('data_evaluasi.bacinput', compact('data','tanggal','data1','datas','id','siklus'));
    }

    public function printevaluasi(\Illuminate\Http\Request $request, $id){
       $input = $request->all();
        // dd($input);
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $tanggal = date('Y m d');
        $instrumen = DB::table('tb_instrumen')->where('id_parameter', 'urinalisa')->get();
        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->leftjoin('tb_reagen_imunologi', 'hp_details.reagen', '=', 'tb_reagen_imunologi.id')
            ->leftjoin('tb_instrumen','hp_details.alat', '=', 'tb_instrumen.id')
            ->leftjoin('tb_hp','hp_details.hasil_pemeriksaan', '=', 'tb_hp.id')
            ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.alat','tb_instrumen.id as idinstrumen', 'tb_instrumen.instrumen','hp_details.instrument_lain', 'hp_details.id as id_detail','hp_details.reagen as hp_reagen','tb_reagen_imunologi.reagen as imu_reagen','hp_details.reagen_lain', 'tb_hp.hp', 'tb_hp.parameter_id','tb_registrasi.periode')
            ->where('parameter.kategori', 'urinalisa')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->get();
            foreach ($data as $key => $val) {
                $metode = MetodePemeriksaan::where('parameter_id', $val->id)->get();
                $val->metode = $metode;
                $reagen = ReagenImunologi::where('parameter_id', $val->id)->get();
                $val->reagen = $reagen;
                $hasilPemeriksaan = TbHp::where('parameter_id', $val->id)->get();
                $val->hasilPemeriksaan = $hasilPemeriksaan;
                $target = Rujukanurinalisa::where('parameter', $val->id)
                                            ->where('siklus', $siklus)
                                            ->where('tahun', $date)
                                            ->where('type', $type)
                                            ->where(function($query) use ($val, $tahun, $siklus){
                                                if ($val->periode == 2 && $siklus == 1) {
                                                    $query->where('periode','=', '2');
                                                }else{
                                                    $query->whereNull('periode');
                                                }
                                            })
                                            ->get();
                $val->target = $target;
            }

        // dd($data);
        $skoring = DB::table('tb_skoring_urinalisa')
                    ->where('id_registrasi', $id)
                    ->where('siklus', $siklus)
                    ->where('type', $type)
                    ->where('tahun', $date)
                    ->get();
        $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();


        $skoring1 = DB::table('tb_skoring_urinalisa')
                    ->where('id_registrasi', $id)
                    ->where('siklus', $siklus)
                    ->where('type', $type)
                    ->where('tahun', $date)
                    ->first();
        $ttd = DB::table('tb_ttd_evaluasi')
            ->where('tahun', $date)
            ->where('siklus', $siklus)
            ->where('bidang', '=', '3')
            ->where(function($query) use ($q1, $date, $siklus){
                if ($q1->periode == 2 && $date == 2019 && $siklus == 1) {
                    $query->where('periode', '=', '2');
                }else{
                    $query->whereNull('periode');
                }
            })
            ->first();


        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;


        if (count($data[10]->target) > 0) {
            $pdf = PDF::loadview('evaluasi/urinalisa/print/evaluasi', compact('data' ,'perusahaan', 'datas', 'type','siklus','date','instrumen', 'tanggal', 'skoring','id','ttd'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Urinalisa.pdf');
        }else{
             Session::flash('message', 'Urinalisa belum dievaluasi!');
             Session::flash('alert-class', 'alert-danger');
            return back();
        }

    }

    public function kepekaan(\Illuminate\Http\Request $request, $id){
        $siklus = $request->y;
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $years = $q2->year;

        $register = Register::find($id);
        $user = Auth::user()->id;


        $dd = $request->tanggal;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
        } else {
            $dd = $request->session()->get('tanggal');
        }
        $tanggal = $dd;

        $datas=  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias')
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                // ->where('tb_registrasi.status', 3)
                // ->where(function($query)
                //     {
                //         $query->where('status_data1', 2)
                //             ->orwhere('status_data2', 2)
                //             ->orwhere('status_datarpr1', 2)
                //             ->orwhere('status_datarpr2', 2);
                //     })
                // ->where(function($query)
                //     {
                //         $query->where('pemeriksaan2', '=' , 'done')
                //                 ->orwhere('pemeriksaan', '=' , 'done')
                //                 ->orwhere('siklus_1', '=' , 'done')
                //                 ->orwhere('siklus_2', '=' , 'done');
                //     })
                ->get();

        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('sub_bidang.id', '=', '13')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2)
                                ->orwhere('status_datarpr1', 2)
                                ->orwhere('status_datarpr2', 2);
                    })
                ->get();

            $data1 =  DB::table('tb_registrasi')
            ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
            ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
            ->where('tb_registrasi.created_by','=', Auth::user()->id)
            ->where('sub_bidang.id', '=', '13')
            ->where('tb_registrasi.status', 3)
            ->where(function($query)
                {
                    $query->where('status_data1', 2)
                        ->orwhere('status_data2', 2)
                        ->orwhere('status_datarpr1', 2)
                        ->orwhere('status_datarpr2', 2);
                })
            ->where(function($query)
                {
                    $query->where('pemeriksaan2', '=' , 'done')
                            ->orwhere('pemeriksaan', '=' , 'done')
                            ->orwhere('siklus_1', '=' , 'done')
                            ->orwhere('siklus_2', '=' , 'done');
                })
            ->get();

        return view('data_evaluasi.bacinput', compact('data','tanggal','data1','datas','id','siklus'));
    }



    public function kat(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $input = $request->all();
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }

        $tanggal = $dd;
        $sik = Session::get('siklus');
        $user = Auth::user()->id;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('tb_registrasi.status', '>=' , 2)
                ->get();

        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->where('sub_bidang.id', '=', '12')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2);
                    })
                ->get();
         // dd($data);
        $responSatu = DB::table('tb_pendapat_responden')->select('siklus','tahun')->where('tahun', $tanggal)->where('siklus','=', $siklus)->where('id_users', $user)->first();

        if(count($datas)){
            // if (count($responSatu)) {
                return view('data_evaluasi.kat', compact('data','tanggal','sik','id','type','siklus','tahunevaluasi','datas','responSatu'));
            // }else{
            //     Session::flash('message', 'Silahkan isi survey untuk melihat data evaluasi!');
            //     Session::flash('alert-class', 'alert-danger');
            //     return redirect('pendapat/pendapat-responden'); 
            // }
        }else{
            return view('data_evaluasi/index', compact('data', 'data2','tanggal','siklus','tahunevaluasi'));
        }
    }

    public function kai(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $input = $request->all();
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $sik = Session::get('siklus');
        $user = Auth::user()->id;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();

        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('tb_registrasi.status', '>=' , 2)
                ->get();

        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->where('sub_bidang.id', '=', '11')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2);
                    })
                ->get();
         // dd($data);
        $responSatu = DB::table('tb_pendapat_responden')->select('siklus','tahun')->where('tahun', $tanggal)->where('siklus','=', $siklus)->where('id_users', $user)->first();

        if(count($datas)){
            // if (count($responSatu)) {
                return view('data_evaluasi.kai', compact('data','sik','id','type','siklus','tahunevaluasi','datas','tanggal','sik','responSatu'));
            // }else{
            //     Session::flash('message', 'Silahkan isi survey untuk melihat data evaluasi!');
            //     Session::flash('alert-class', 'alert-danger');
            //     return redirect('pendapat/pendapat-responden'); 
            // }
        }else{
            return view('data_evaluasi/index', compact('data', 'data2','tanggal','siklus','tahunevaluasi'));
        }
    }

    public function evaluasi_cacing(\Illuminate\Http\Request $request, $id)
    {
        $input =$request->all();
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $user = Auth::user()->id;

        $dd = $request->tanggal;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
        } else {
            $dd = $request->session()->get('tanggal');
        }
        $tanggal = $dd;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();

        $datas=  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias')
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        // dd($evaluasi);
        return view('data_evaluasi.tccinput', compact('data','datas','tanggal','user','id','siklus', 'perusahaan','type','rujuk','evaluasi','nilaievaluasi','tahunevaluasi'));
    }

    public function grafikskoringuri(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $user = Auth::user()->id;
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $dd = $request->tanggal;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
        } else {
            $dd = $request->session()->get('tanggal');
        }
        $tanggal = $dd;

        $instrumen = DB::table('tb_instrumen')->where('id_parameter', 'urinalisa')->get();
        $datas1=  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias')
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $dd)
                ->groupBy('tb_registrasi.bidang')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        
        $get = $request->all();
        $tahun1 = $dd;
        $type1 = 'a';
        $siklus1 = 1;

        $sql =" SELECT count(*) as nilai1, '<= 1,00' as nilainya, 'a' as sort from tb_skoring_urinalisa
                where siklus = '".$siklus1."' AND type = '".$type1."' AND tahun = '".$tahun1."' AND skoring <= 1
                UNION
                SELECT count(*) as nilai1, '> 1,00 - 2,00' as nilainya, 'b' as sort from tb_skoring_urinalisa
                where siklus = '".$siklus1."' AND type = '".$type1."' AND tahun = '".$tahun1."' AND skoring > 1 and skoring <= 2
                UNION
                SELECT count(*) as nilai1, '> 2,00 - 3,00' as nilainya, 'c' as sort from tb_skoring_urinalisa
                where siklus = '".$siklus1."' AND type = '".$type1."' AND tahun = '".$tahun1."' AND skoring > 2 AND skoring <= 3
                UNION
                SELECT count(*) as nilai1, '> 3,00' as nilainya, 'd' as sort from tb_skoring_urinalisa
                where siklus = '".$siklus1."' AND type = '".$type1."' AND tahun = '".$tahun1."' AND skoring > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->orderBy('sort', 'desc')
                ->get();

        return view('data_evaluasi/grafikskoringuri',compact('dd','simpan', 'perusahaan', 'datas1', 'type','siklus','date','instrumen', 'tanggal', 'skoring','id', 'input','tanggal','data'));
    }

    public function grafikskoringurina(\Illuminate\Http\Request $request)
    {
        $get = $request->all();
        $tahun = $get['tahun'];
        $type = $get['type'];
        $siklus = $get['siklus'];


        $sql =" SELECT count(*) as nilai1, '<= 1,00' as nilainya, 'a' as sort from tb_skoring_urinalisa
                where siklus = '".$siklus."' AND type = '".$type."' AND tahun = '".$tahun."' AND skoring <= 1
                UNION
                SELECT count(*) as nilai1, '> 1,00 - 2,00' as nilainya, 'b' as sort from tb_skoring_urinalisa
                where siklus = '".$siklus."' AND type = '".$type."' AND tahun = '".$tahun."' AND skoring > 1 and skoring <= 2
                UNION
                SELECT count(*) as nilai1, '> 2,00 - 3,00' as nilainya, 'c' as sort from tb_skoring_urinalisa
                where siklus = '".$siklus."' AND type = '".$type."' AND tahun = '".$tahun."' AND skoring > 2 AND skoring <= 3
                UNION
                SELECT count(*) as nilai1, '> 3,00' as nilainya, 'd' as sort from tb_skoring_urinalisa
                where siklus = '".$siklus."' AND type = '".$type."' AND tahun = '".$tahun."' AND skoring > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->orderBy('sort', 'desc')
                ->get();
        
        $rows = [
            0 => [],
          ];
        foreach($data as $val){
            $color = null;
            $row['y']=(float)$val->nilai1;
            $row['name'] =$val->nilainya;
            array_push($rows[0], $row);
        }
        return response ($rows);
    } 
}
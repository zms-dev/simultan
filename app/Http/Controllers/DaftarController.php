<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\register as Register;
use App\subbidang as Subbidang;
use App\daftar as Daftar;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Mail;

class DaftarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        $user = Auth::user()->id;
        $date = date('Y');
        $perusahaan = DB::table('perusahaan')
                    ->where('created_by', $user)
                    ->first();
        if (!empty($perusahaan) && count($perusahaan) > 0) {
            $siklus = DB::table('perusahaan')
                        ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->where('perusahaan.created_by', $user)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $date)
                        ->select('tb_registrasi.siklus')
                        ->groupBy('tb_registrasi.siklus')
                        ->get();
            $provinsi = DB::table('provinces')->select('id','name')->get();
            $bidang = DB::table('sub_bidang')->get();
            $badan = DB::table('badan_usaha')->get();
            return view('daftar_old', compact('perusahaan','provinsi', 'bidang', 'badan', 'siklus','date'));
        }else{
            $provinsi = DB::table('provinces')->select('id','name')->get();
            $bidang = DB::table('sub_bidang')->get();
            $badan = DB::table('badan_usaha')->get();
            return view('daftar', compact('provinsi', 'bidang', 'badan','date'));
        }
    }

    public function datasiklus($id){
        $user = Auth::user()->id;
        $date = date('Y');
        
        $data = DB::table('tb_bidang')
            ->join('sub_bidang', 'tb_bidang.id', '=', 'sub_bidang.id_bidang')
            ->leftjoin('tb_registrasi','tb_registrasi.bidang','=', DB::raw('sub_bidang.id AND YEAR(tb_registrasi.created_at) = '.$date.''))
            ->select('sub_bidang.id', 'sub_bidang.parameter', 'sub_bidang.id_bidang','sub_bidang.tarif', 'sub_bidang.alias', 'tb_bidang.bidang', 
                    DB::raw('(CASE
                                WHEN sub_bidang.kuota_1 != 0  THEN
                                    sub_bidang.kuota_1 - (
                                    COUNT(
                                        CASE WHEN (tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1) AND tb_registrasi.status > 1 THEN tb_registrasi.bidang END))
                            END
                            ) as sisa_kuota_1'),
                    DB::raw('(CASE
                                WHEN sub_bidang.kuota_2 != 0  THEN
                                    sub_bidang.kuota_2 - (
                                    COUNT(
                                        CASE WHEN (tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 2) AND tb_registrasi.status > 1 THEN tb_registrasi.bidang END))
                            END
                            ) as sisa_kuota_2'))
            ->groupBy('sub_bidang.id')
            ->where('sub_bidang.status','=','1')
            ->get();
            foreach ($data as $key => $val) {
                $daftar = DB::table('tb_registrasi')
                            ->where('tb_registrasi.created_by', $user)
                            ->where('tb_registrasi.bidang', $val->id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $date)
                            ->get();
                $val->daftar = $daftar;
            }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function datasiklusbaru($id){
        $date = date('Y');
        
        $data = DB::table('tb_bidang')
            ->join('sub_bidang', 'tb_bidang.id', '=', 'sub_bidang.id_bidang')
            ->leftjoin('tb_registrasi','tb_registrasi.bidang','=', DB::raw('sub_bidang.id AND YEAR(tb_registrasi.created_at) = '.$date.''))
            ->select('sub_bidang.id', 'sub_bidang.parameter', 'sub_bidang.id_bidang','sub_bidang.tarif', 'sub_bidang.alias', 'tb_bidang.bidang', 
                    DB::raw('(CASE
                                WHEN sub_bidang.kuota_1 != 0  THEN
                                    sub_bidang.kuota_1 - (
                                    COUNT(
                                        CASE WHEN (tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1) AND tb_registrasi.status > 1 THEN tb_registrasi.bidang END))
                            END
                            ) as sisa_kuota_1'),
                    DB::raw('(CASE
                                WHEN sub_bidang.kuota_2 != 0  THEN
                                    sub_bidang.kuota_2 - (
                                    COUNT(
                                        CASE WHEN (tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 2) AND tb_registrasi.status > 1 THEN tb_registrasi.bidang END))
                            END
                            ) as sisa_kuota_2'))
            ->groupBy('sub_bidang.id')
            ->where('sub_bidang.status','=','1')
            ->get();
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function daftar(\Illuminate\Http\Request $request)
    {
        $data = $request->all();
        
        $tahun = date('Y');
        $register = DB::table('tb_registrasi')
                    ->select(DB::raw('CAST(no_urut AS UNSIGNED) as no_uruts'))
                    ->join('users', 'users.id','=','tb_registrasi.created_by')
                    ->where(DB::raw('Year(tb_registrasi.created_at)'), $tahun)
                    ->whereNotNull('no_urut')
                    ->groupBy('no_uruts')
                    ->orderBy('no_uruts', 'desc')
                    ->first();
        if(count($register)){
            $cekno = (int)$register->no_uruts + 1;
            $no_urut = sprintf("%04s", $cekno);
        }else{
            $no_urut = '0001';
        }
        $cekpeserta = DB::table('tb_registrasi')
                    ->join('users', 'users.id','=','tb_registrasi.created_by')
                    ->where(DB::raw('Year(tb_registrasi.created_at)'), $tahun)
                    ->where('tb_registrasi.created_by', Auth::user()->id)
                    ->orderBy('tb_registrasi.id', 'desc')
                    ->first();
            
        
        $cekkuota = DB::table('tb_bidang')
            ->join('sub_bidang', 'tb_bidang.id', '=', 'sub_bidang.id_bidang')
            ->leftjoin('tb_registrasi','tb_registrasi.bidang','=', DB::raw('sub_bidang.id AND YEAR(tb_registrasi.created_at) = '.$tahun.''))
            ->select('sub_bidang.parameter','sub_bidang.id','sub_bidang.kuota_1','sub_bidang.kuota_2', 'sub_bidang.id_bidang','sub_bidang.tarif',  'tb_bidang.bidang as Bidang', 
                    DB::raw('(sub_bidang.kuota_1 - (COUNT(CASE WHEN (tb_registrasi.siklus = 12 || tb_registrasi.siklus = 1) AND tb_registrasi.status > 1 THEN tb_registrasi.bidang END))) as sisa_kuota_1,
                            (sub_bidang.kuota_2 - (COUNT(CASE WHEN (tb_registrasi.siklus = 12 || tb_registrasi.siklus = 2) AND tb_registrasi.status > 1 THEN tb_registrasi.bidang END))) as sisa_kuota_2'))
            ->wherein('sub_bidang.id', $data['bidang'])
            ->groupBy('sub_bidang.id')
            ->orderBy('sisa_kuota_1','asc')
            ->orderBy('sisa_kuota_2','asc')
            ->first();
        // dd($cekkuota);
        if ($data['siklus'] == 1) {
            $cekkuotasiklus = $cekkuota->sisa_kuota_1 < 1;
        }elseif($data['siklus'] == 2){
            $cekkuotasiklus = $cekkuota->sisa_kuota_2 < 1 ;
        }else{
            $cekkuotasiklus = $cekkuota->sisa_kuota_1 < 1 || $cekkuota->sisa_kuota_2 < 1 ;
        }

        if($cekkuotasiklus){
            Session::flash('message', 'Salah satu Bidang yang anda pilih Kuotanya sudah Habis!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return Redirect::back();
        }else{
            if(count($cekpeserta)) {
            }else{
                $dilaksanakan = "";

                $saveDaftar = new Daftar;
                $saveDaftar->nama_lab = $data['nama_lab'];
                $saveDaftar->pemerintah = $data['pemerintah'];
                $saveDaftar->alamat = $data['alamat'];
                $saveDaftar->provinsi = $data['provinsi'];
                $saveDaftar->kota = $data['kota'];
                $saveDaftar->kecamatan = $data['kecamatan'];
                $saveDaftar->kelurahan = $data['kelurahan'];
                $saveDaftar->telp = $data['telp'];
                $saveDaftar->email = $data['email_cp'];
                $saveDaftar->penanggung_jawab = $data['penanggung_jawab'];
                $saveDaftar->personal = $data['personal'];
                $saveDaftar->no_hp = $data['no_hp'];
                $saveDaftar->no_wa = $data['no_wa'];
                $saveDaftar->akreditasi = $data['akreditasi'];
                $saveDaftar->pemantapan_mutu = $data['pemantapan_mutu'];
                $saveDaftar->alasan_binaan = $data['alasan_binaan'];
                if(!empty($data['dilaksanakan'])){
                    $saveDaftar->dilaksanakan = implode(", ",$data['dilaksanakan']);
                }
                $saveDaftar->created_by = Auth::user()->id;
                $saveDaftar->save();

                $saveDaftarID = $saveDaftar->id;

                $i = 0;
                $a = Auth::user()->id_member;
                $kode_pes = sprintf("%04s", $a);
                $date = date('y');
                foreach ($data['bidang'] as $alat) {
                    $datas = $data;
                    if($alat != ''){
                        $saveRegister = new Register;
                        $saveRegister->perusahaan_id = $saveDaftar->id;
                        $saveRegister->bidang = $alat;
                        $saveRegister->kode_lebpes = $kode_pes.'/'.$data['alias'][$alat - 1].'/'.$data['siklus'].'/'.$date;
                        if ($data['pembayaran'] == 'transfer')
                        {
                            $saveRegister->no_urut = $no_urut;
                            $saveRegister->file = '';
                            $saveRegister->status = '1';
                            $saveRegister->id_pembayaran = '1';
                        }else{
                            $saveRegister->no_urut = '';
                            $saveRegister->sptjm = $data['sptjm'];
                            // $saveRegister->file = $name1;
                            $saveRegister->id_pembayaran = '2';
                            $saveRegister->status = '1';
                        }
                        $saveRegister->siklus = $data['siklus'];
                        $saveRegister->created_by = Auth::user()->id;
                        $saveRegister->save();

                        // $kuota = DB::table('sub_bidang')->where('id', $alat)->first();
                        // $sisa['kuota_1'] = $kuota->kuota_1 - 1;
                        // $sisa['kuota_2'] = $kuota->kuota_2 - 1;
                        
                        // Subbidang::where('id',$alat)->update($sisa);
                        
                        Session::flash('message', 'Pendaftaran berhasil. Mohon tunggu untuk dikonfirmasi!'); 
                        Session::flash('alert-class', 'alert-success'); 
                    }
                    $i++;
                }

                $sub_bidang = DB::table('sub_bidang')->whereIn('id', $data['bidang'])->get();
                $userna = DB::table('users')->where('role', 4)->first();
                $datu = [
                        'labklinik' => $data['nama_lab'],
                        'siklus' => $data['siklus'],
                        'date' => date('Y'),
                        'parameter' => $sub_bidang 
                ];
                // Mail::send('email.pendaftaran', $datu, function ($mail) use ($datas, $userna)
                // {
                //   $mail->from('pme.bblksub@gmail.com', 'BBLK Surabaya Daftar Peserta '.$datas['nama_lab']);
                //   // $mail->to('tengkufirmansyah2@gmail.com');
                //   $mail->to($userna->email);
                //   $mail->subject('Daftar PNPME Peserta '.$datas['nama_lab']);
                // });

                // if ($data['pembayaran'] == 'transfer') {
                //     $datu = [
                //         'no_urut' => $no_urut
                //     ];
                //     Mail::send('email.transfer',$datu, function ($mail) use ($data)
                //     {
                //           $mail->from('pme.bblksub@gmail.com', 'BBLK Surabaya Daftar Peserta ');
                //           $mail->to(Auth::user()->email);
                //   // $mail->to('tengkufirmansyah2@gmail.com');
                //           $mail->subject('Daftar PNPME Surabaya ');
                //     });
                // }else{
                //     Mail::send('email.sptjm',[], function ($mail) use ($data)
                //     {
                //           $mail->from('pme.bblksub@gmail.com', 'BBLK Surabaya Daftar Peserta ');
                //           $mail->to(Auth::user()->email);
                //   // $mail->to('tengkufirmansyah2@gmail.com');
                //           $mail->subject('Daftar PNPME Surabaya');
                //     });
                // }
            }
        }
        return redirect('status-pembayaran');
    }

    public function daftar_old(\Illuminate\Http\Request $request)
    {
        $data = $request->all();

        $tahun = date('Y');
        $register = DB::table('tb_registrasi')
                    ->select(DB::raw('CAST(no_urut AS UNSIGNED) as no_uruts'))
                    ->join('users', 'users.id','=','tb_registrasi.created_by')
                    ->where(DB::raw('Year(tb_registrasi.created_at)'), $tahun)
                    ->whereNotNull('no_urut')
                    ->groupBy('no_uruts')
                    ->orderBy('no_uruts', 'desc')
                    ->first();
        if(count($register)){
            $cekno = (int)$register->no_uruts + 1;
            $no_urut = sprintf("%04s", $cekno);
        }else{
            $no_urut = '0001';
        }
        $cekpeserta = DB::table('tb_registrasi')
                    ->join('users', 'users.id','=','tb_registrasi.created_by')
                    ->where(DB::raw('Year(tb_registrasi.created_at)'), $tahun)
                    ->where('tb_registrasi.created_by', Auth::user()->id)
                    ->orderBy('tb_registrasi.id', 'desc')
                    ->first();
        // dd($no_urut);
        $user = Auth::user()->id;
        $perusahaan = DB::table('perusahaan')
                    ->where('created_by', $user)
                    ->first();
        $i = 0;

        $a = Auth::user()->id_member;
        $kode_pes = sprintf("%04s", $a);
        $date = date('y');

        
        $cekkuota = DB::table('tb_bidang')
            ->join('sub_bidang', 'tb_bidang.id', '=', 'sub_bidang.id_bidang')
            ->leftjoin('tb_registrasi','tb_registrasi.bidang','=', DB::raw('sub_bidang.id AND YEAR(tb_registrasi.created_at) = '.$tahun.''))
            ->select('sub_bidang.parameter','sub_bidang.id','sub_bidang.kuota_1','sub_bidang.kuota_2', 'sub_bidang.id_bidang','sub_bidang.tarif',  'tb_bidang.bidang as Bidang', 
                    DB::raw('(sub_bidang.kuota_1 - (COUNT(CASE WHEN (tb_registrasi.siklus = 12 || tb_registrasi.siklus = 1) AND tb_registrasi.status > 1 THEN tb_registrasi.bidang END))) as sisa_kuota_1,
                            (sub_bidang.kuota_2 - (COUNT(CASE WHEN (tb_registrasi.siklus = 12 || tb_registrasi.siklus = 2) AND tb_registrasi.status > 1 THEN tb_registrasi.bidang END))) as sisa_kuota_2'))
            ->wherein('sub_bidang.id', $data['bidang'])
            ->groupBy('sub_bidang.id')
            ->orderBy('sisa_kuota_1','asc')
            ->orderBy('sisa_kuota_2','asc')
            ->first();
        if ($data['siklus'] == 1) {
            $cekkuotasiklus = $cekkuota->sisa_kuota_1 < 1;
        }elseif($data['siklus'] == 2){
            $cekkuotasiklus = $cekkuota->sisa_kuota_2 < 1 ;
        }else{
            $cekkuotasiklus = $cekkuota->sisa_kuota_1 < 1 || $cekkuota->sisa_kuota_2 < 1 ;
        }
            
        if($cekkuotasiklus){
            Session::flash('message', 'Salah satu Bidang yang anda pilih Kuotanya sudah Habis!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return Redirect::back();
        }else{
            if(count($cekpeserta)) {
                Session::flash('message', 'Anda sudah mendaftar PME di tahun ini!'); 
                Session::flash('alert-class', 'alert-danger'); 
                return Redirect::back();
            }else{
                foreach ($data['bidang'] as $alat) {
                    if($alat != ''){
                        $saveRegister = new Register;
                        $saveRegister->perusahaan_id = $perusahaan->id;
                        $saveRegister->bidang = $alat;
                        $saveRegister->kode_lebpes = $kode_pes.'/'.$data['alias'][$alat - 1].'/'.$data['siklus'].'/'.$date;
                        if ($data['pembayaran'] == 'transfer')
                        {
                            $saveRegister->no_urut = $no_urut;
                            $saveRegister->file = "";
                            $saveRegister->status = '1';
                            $saveRegister->id_pembayaran = '1';
                        }else{
                            $saveRegister->no_urut = "";
                            $saveRegister->sptjm = $data['sptjm'];
                            $saveRegister->file = "";
                            $saveRegister->id_pembayaran = '2';
                            $saveRegister->status = '1';
                        }
                        $saveRegister->siklus = $data['siklus'];
                        $saveRegister->created_by = $user;
                        $saveRegister->save();

                        // $kuota = DB::table('sub_bidang')->where('id', $alat)->first();
                        // $sisa['kuota_1'] = $kuota->kuota_1 - 1;
                        // $sisa['kuota_2'] = $kuota->kuota_2 - 1;
                        
                        // Subbidang::where('id',$alat)->update($sisa);
                        
                        Session::flash('message', 'Pendaftaran berhasil. Mohon tunggu untuk dikonfirmasi!'); 
                        Session::flash('alert-class', 'alert-success'); 
                    }
                    $i++;
                }

                $sub_bidang = DB::table('sub_bidang')->whereIn('id', $data['bidang'])->get();
                $userna = DB::table('users')->where('role', 4)->first();
                $datu = [
                        'labklinik' => $perusahaan->nama_lab,
                        'siklus' => $data['siklus'],
                        'date' => date('Y'),
                        'parameter' => $sub_bidang 
                ];
                // Mail::send('email.pendaftaran', $datu, function ($mail) use ($userna, $perusahaan)
                // {
                //   $mail->from('pme.bblksub@gmail.com', 'BBLK Surabaya Daftar Peserta '.$perusahaan->nama_lab);
                //   // $mail->to('tengkufirmansyah2@gmail.com');
                //   $mail->to($userna->email);
                //   $mail->subject('Daftar PNPME Peserta '.$perusahaan->nama_lab);
                // });

                // if ($data['pembayaran'] == 'transfer') {
                //     $datu = [
                //         'no_urut' => $no_urut
                //     ];
                //     Mail::send('email.transfer',$datu, function ($mail) use ($data)
                //     {
                //           $mail->from('pme.bblksub@gmail.com', 'BBLK Surabaya Daftar Peserta ');
                //           $mail->to(Auth::user()->email);
                //           $mail->subject('Daftar PNPME Surabaya ');
                //     });
                // }else{
                //     Mail::send('email.sptjm',[], function ($mail) use ($data)
                //     {
                //           $mail->from('pme.bblksub@gmail.com', 'BBLK Surabaya Daftar Peserta ');
                //           $mail->to(Auth::user()->email);
                //           $mail->subject('Daftar PNPME Surabaya ');
                //     });
                // }
            }
        }
        return redirect('status-pembayaran');
    }

    public function getKota($id){
        $data = DB::table('regencies')->select('id as Kode','name as Nama')->where('province_id',$id)->get();
        return response()->json(['Hasil'=>$data]);
    }
    public function getKecamatan($id){
        $data = DB::table('districts')->select('id as Kode','name as Nama')->where('regency_id',$id)->get();
        return response()->json(['Hasil'=>$data]);
    }

    public function getKelurahan($id){
        $data = DB::table('villages')->select('id as Kode','name as Nama')->where('district_id',$id)->get();
        return response()->json(['Hasil'=>$data]);
    }

    public function getReagenImun($id){
        $data = DB::table('tb_reagen_imunologi')->select('produsen as Produsen','metode as Metode')->where('id',$id)->get();
        return response()->json(['Hasil'=>$data]);
    }
}

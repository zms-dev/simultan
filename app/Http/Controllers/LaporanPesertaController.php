<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;
use Excel;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Redirect;
use Validator;
use Session;

class LaporanPesertaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function provinsiinstansi()
    {
        $bidang = DB::table('sub_bidang')->get();
        return view('dashboard.laporanprovinsiinstansi.index', compact('bidang'));
    }

    public function provinsiinstansina(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        // dd($input);
        if ($input['bidang'] == "Semua Bidang") {
            $bidangna = "Semua Bidang";
            $data = DB::table('provinces')->get();
            foreach ($data as $key => $val) {
                $blk = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '1')
                        ->count();
                $bblk = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '2')
                        ->count();
                $rspem = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '3')
                        ->count();
                $pus = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '4')
                        ->count();
                $labke = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '5')
                        ->count();
                $rssw = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '6')
                        ->count();
                $labkl = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '7')
                        ->count();
                $utdrs = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '8')
                        ->count();
                $utdpm = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '9')
                        ->count();
                $rstni = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '10')
                        ->count();
                $all = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->count();
                $val->blk = $blk;
                $val->bblk = $bblk;
                $val->rspem = $rspem;
                $val->pus = $pus;
                $val->labke = $labke;
                $val->rssw = $rssw;
                $val->labkl = $labkl;
                $val->utdrs = $utdrs;
                $val->utdpm = $utdpm;
                $val->rstni = $rstni;
                $val->all = $all;
            }
        }else{
            $bidang = DB::table('sub_bidang')->where('id', $input['bidang'])->first();
            $bidangna = $bidang->alias;
            $data = DB::table('provinces')->get();
            foreach ($data as $key => $val) {
                $blk = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('tb_registrasi.bidang', $input['bidang'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '1')
                        ->count();
                $bblk = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where('tb_registrasi.bidang', $input['bidang'])
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '2')
                        ->count();
                $rspem = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where('tb_registrasi.bidang', $input['bidang'])
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '3')
                        ->count();
                $pus = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where('tb_registrasi.bidang', $input['bidang'])
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '4')
                        ->count();
                $labke = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where('tb_registrasi.bidang', $input['bidang'])
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '5')
                        ->count();
                $rssw = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where('tb_registrasi.bidang', $input['bidang'])
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '6')
                        ->count();
                $labkl = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where('tb_registrasi.bidang', $input['bidang'])
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '7')
                        ->count();
                $utdrs = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where('tb_registrasi.bidang', $input['bidang'])
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '8')
                        ->count();
                $utdpm = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where('tb_registrasi.bidang', $input['bidang'])
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '9')
                        ->count();
                $rstni = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where('tb_registrasi.bidang', $input['bidang'])
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->where('badan_usaha.id','=', '10')
                        ->count();
                $all = DB::table('tb_registrasi')
                        ->join('perusahaan','perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                        ->where('tb_registrasi.bidang', $input['bidang'])
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->where('perusahaan.provinsi','=', $val->id)
                        ->count();
                $val->blk = $blk;
                $val->bblk = $bblk;
                $val->rspem = $rspem;
                $val->pus = $pus;
                $val->labke = $labke;
                $val->rssw = $rssw;
                $val->labkl = $labkl;
                $val->utdrs = $utdrs;
                $val->utdpm = $utdpm;
                $val->rstni = $rstni;
                $val->all = $all;
            }
        }
        // dd($bidangna);
        if($input['proses'] == 'View Data'){
            return view('dashboard.laporanprovinsiinstansi.view', compact('data', 'bidangna'));
        }else{
            Excel::create('Laporan Jumlah Peserta per Provinsi per Instansi', function($excel) use ($data, $bidangna) {
                $excel->sheet('Data', function($sheet) use ($data, $bidangna) {
                    $sheet->loadView('dashboard.laporanprovinsiinstansi.excel', array('data'=>$data,'bidangna'=>$bidangna) );
                });
            })->download('xlsx');
        }
    }

    public function monitoringparameter()
    {
        $bidang = DB::table('sub_bidang')->get();
        return view('dashboard.monitoringparameter.index', compact('bidang'));
    }
    public function monitoringparameterna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        // dd($input);
        if ($input['bidang'] == 3) {
            $data = DB::table('tb_registrasi')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->join('badan_usaha', 'badan_usaha.id', '=', 'perusahaan.pemerintah')
                    ->join('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                    ->join('regencies', 'perusahaan.kota', '=', 'regencies.id')
                    ->join('tb_skoring_urinalisa', 'tb_skoring_urinalisa.id_registrasi', '=', 'tb_registrasi.id')
                    ->join('hp_headers', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
                    ->where('tb_registrasi.bidang', '=', 3)
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                    ->select('tb_registrasi.id', 'tb_registrasi.perusahaan_id', 'perusahaan.nama_lab', 'provinces.name', 'hp_headers.type', 'regencies.name as kota', 'badan_usaha.badan_usaha')
                    ->orderBy('tb_registrasi.id', 'asc')
                    ->groupBy('perusahaan.id', 'hp_headers.type')
                    ->get();
            foreach ($data as $key => $val) {
                $no= 0;
                for ($i=$input['tahun']; $i < $input['tahun'] + 5 ; $i++) { 
                    $no++;
                    $sql = "SELECT
                                '".$val->type."' AS type, '1' AS Siklus1,
                                (SELECT
                                        skoring
                                FROM
                                        `tb_skoring_urinalisa`
                                INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_skoring_urinalisa`.`id_registrasi`
                                WHERE perusahaan_id = ".$val->perusahaan_id." AND  tahun = ".$i." AND tb_skoring_urinalisa.siklus = 1 AND  type = '".$val->type."') zscore1, 
                                '2' AS Siklus2,
                                (SELECT
                                        skoring
                                FROM
                                        `tb_skoring_urinalisa`
                                INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_skoring_urinalisa`.`id_registrasi`
                                WHERE perusahaan_id = ".$val->perusahaan_id." AND tahun = ".$i." AND tb_skoring_urinalisa.siklus = 2 AND  type = '".$val->type."') zscore2";
                    ${'data' . $no} = DB::table(DB::raw("($sql) t"))->first();
                    $val->{'data' . $no} = ${'data' . $no};
                }
            }
            // dd($data[0]);
            $myFile = Excel::create('Monitoring Evaluasi Hasil Peserta Urinalisa', function($excel) use ($data, $input) {
                $excel->sheet('Data', function($sheet) use ($data, $input) {
                    $sheet->loadView('dashboard.monitoringparameterurinalisa.view', array('data'=>$data,'input'=>$input) );
                });
            });
            $myFile   = $myFile->string('xlsx'); 
            $response = array(
                'name' => 'Monitoring Evaluasi Hasil Peserta Urinalisa', 
                'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
            );
            return response()->json($response);
        }elseif($input['bidang'] == 4 || $input['bidang'] == 5 || $input['bidang'] == 10){
            $bidang = DB::table('sub_bidang')->where('id', $input['bidang'])->first();
            if($input['bidang'] == 4){
                $data = DB::table('tb_registrasi')
                        ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                        ->join('tb_evaluasi_bta_status', 'tb_evaluasi_bta_status.id_registrasi', '=', 'tb_registrasi.id')
                        ->where('tb_registrasi.bidang', '=', 4)
                        ->where('tb_registrasi.status', '=', 3)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->select('tb_registrasi.id', 'tb_registrasi.perusahaan_id', 'perusahaan.nama_lab', 'provinces.name', 'tb_evaluasi_bta_status.status')
                        ->orderBy('tb_registrasi.id', 'asc')
                        ->get();
                foreach ($data as $key => $val) {
                    $no= 0;
                    for ($i=$input['tahun']; $i < $input['tahun'] + 5 ; $i++) { 
                        $no++;
                        $sql = "SELECT
                                '1' as Siklus1,
                                (SELECT
                                        tb_evaluasi_bta_status.`status`
                                FROM
                                        `tb_evaluasi_bta_status`
                                INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_evaluasi_bta_status`.`id_registrasi`
                                WHERE
                                        tb_registrasi.id = ".$val->id."
                                AND
                                    tb_evaluasi_bta_status.siklus = 1
                                AND
                                        tahun = ".$i.") zscore1, '2' AS Siklus2,
                                (SELECT
                                        tb_evaluasi_bta_status.`status`
                                FROM
                                        `tb_evaluasi_bta_status`
                                INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_evaluasi_bta_status`.`id_registrasi`
                                WHERE
                                        tb_registrasi.id = ".$val->id."
                                AND
                                    tb_evaluasi_bta_status.siklus = 2
                                AND
                                        tahun = ".$i.") zscore2";
                        ${'data' . $no} = DB::table(DB::raw("($sql) t"))->first();
                        $val->{'data' . $no} = ${'data' . $no};
                    }
                }
            }elseif($input['bidang'] == 5){
                $data = DB::table('tb_registrasi')
                        ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                        ->where('tb_registrasi.bidang', '=', 5)
                        ->where('tb_registrasi.status', '=', 3)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->select('tb_registrasi.id', 'tb_registrasi.perusahaan_id', 'perusahaan.nama_lab', 'provinces.name')
                        ->orderBy('tb_registrasi.id', 'asc')
                        ->groupBy('tb_registrasi.perusahaan_id')
                        ->get();
                foreach ($data as $key => $val) {
                    $no= 0;
                    for ($i=$input['tahun']; $i < $input['tahun'] + 5 ; $i++) { 
                        $no++;
                        $sql = "SELECT
                                    tb_evaluasi_tc.siklus, '1' as Siklus1,
                                    SUM(CASE tb_evaluasi_tc.siklus
                                        WHEN '1' THEN nilai
                                        ELSE NULL
                                    END) zscore1, '2' AS Siklus2,
                                    SUM(CASE tb_evaluasi_tc.siklus
                                        WHEN '2' THEN nilai
                                        ELSE NULL
                                    END) zscore2
                                FROM
                                    `tb_evaluasi_tc`
                                INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_evaluasi_tc`.`id_registrasi`
                                WHERE
                                    tb_registrasi.id = ".$val->id."
                                AND
                                    tahun = ".$i."";
                        ${'data' . $no} = DB::table(DB::raw("($sql) t"))->first();
                        $val->{'data' . $no} = ${'data' . $no};
                    }
                }
            }elseif($input['bidang'] == 10){
                $data = DB::table('tb_registrasi')
                        ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->join('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                        ->where('tb_registrasi.bidang', '=', 10)
                        ->where('tb_registrasi.status', '=', 3)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->select('tb_registrasi.id','tb_registrasi.perusahaan_id', 'perusahaan.nama_lab', 'provinces.name')
                        ->orderBy('tb_registrasi.id', 'asc')
                        ->groupBy('tb_registrasi.perusahaan_id')
                        ->get();
                foreach ($data as $key => $val) {
                    $no= 0;
                    for ($i=$input['tahun']; $i < $input['tahun'] + 3 ; $i++) { 
                        $no++;
                            $sql = "SELECT
                                    '1' as Siklus1,
                                    (SELECT
                                            tb_evaluasi_malaria.kesimpulan
                                    FROM
                                            `tb_evaluasi_malaria`
                                    INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_evaluasi_malaria`.`id_registrasi`
                                    WHERE
                                            tb_registrasi.id = ".$val->id."
                                    AND
                                            tb_evaluasi_malaria.siklus = 1
                                    AND
                                            tahun = ".$i.") zscore1, '2' AS Siklus2,
                                    (SELECT
                                            tb_evaluasi_malaria.kesimpulan
                                    FROM
                                            `tb_evaluasi_malaria`
                                    INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_evaluasi_malaria`.`id_registrasi`
                                    WHERE
                                            tb_registrasi.id = ".$val->id."
                                    AND
                                            tb_evaluasi_malaria.siklus = 2
                                    AND
                                            tahun = ".$i.") zscore2";
                        ${'data' . $no} = DB::table(DB::raw("($sql) t"))->first();
                        $val->{'data' . $no} = ${'data' . $no};
                    }
                }
            }
            // dd($data[0]);
            $myFile = Excel::create('Monitoring Evaluasi Hasil Peserta Mikrobiologi', function($excel) use ($data, $bidang, $input) {
                $excel->sheet('Data', function($sheet) use ($data, $bidang, $input) {
                    $sheet->loadView('dashboard.monitoringparametermikro.view', array('data'=>$data, 'bidang'=>$bidang, 'input'=>$input) );
                });
            });
            $myFile   = $myFile->string('xlsx'); 
            $response = array(
                'name' => 'Monitoring Evaluasi Hasil Peserta Mikrobiologi', 
                'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
            );
            return response()->json($response);
        }elseif($input['bidang'] == 6 || $input['bidang'] == 7 || $input['bidang'] == 8 || $input['bidang'] == 9){
            $bidang = DB::table('sub_bidang')->where('id', $input['bidang'])->first();
            $data = DB::table('tb_registrasi')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->join('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                    ->join('master_imunologi', 'master_imunologi.id_registrasi', '=', 'tb_registrasi.id')
                    ->where('tb_registrasi.bidang', '=', $input['bidang'])
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                    ->select('tb_registrasi.id', 'perusahaan.nama_lab', 'tb_registrasi.perusahaan_id', 'provinces.name', 'master_imunologi.siklus')
                    ->orderBy('tb_registrasi.id', 'asc')
                    ->groupBy('tb_registrasi.perusahaan_id')
                    ->get();
            foreach ($data as $key => $val) {
                $no= 0;
                for ($i=$input['tahun']; $i < $input['tahun'] + 3 ; $i++) { 
                    $no++;
                    $sql = "SELECT
                            '1' as Siklus1,
                            (SELECT
                                    tb_kesimpulan_evaluasi.ketepatan
                            FROM
                                    `tb_kesimpulan_evaluasi`
                            INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_kesimpulan_evaluasi`.`id_registrasi`
                            WHERE
                                    tb_registrasi.id = ".$val->id."
                            AND
                                tb_kesimpulan_evaluasi.siklus = 1
                            AND
                                    YEAR(tb_kesimpulan_evaluasi.created_at) = ".$i.") zscore1, '2' AS Siklus2,
                            (SELECT
                                    tb_kesimpulan_evaluasi.ketepatan
                            FROM
                                    `tb_kesimpulan_evaluasi`
                            INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_kesimpulan_evaluasi`.`id_registrasi`
                            WHERE
                                    tb_registrasi.id = ".$val->id."
                            AND
                                tb_kesimpulan_evaluasi.siklus = 2
                            AND
                                    YEAR(tb_kesimpulan_evaluasi.created_at) = ".$i.") zscore2";
                    ${'data' . $no} = DB::table(DB::raw("($sql) t"))->first();
                    $val->{'data' . $no} = ${'data' . $no};
                }
            }
            // dd($data);
            $myFile = Excel::create('Monitoring Evaluasi Hasil Peserta Imunologi', function($excel) use ($data, $bidang, $input) {
                $excel->sheet('Data', function($sheet) use ($data, $bidang, $input) {
                    $sheet->loadView('dashboard.monitoringparameterimun.view', array('data'=>$data, 'bidang'=>$bidang, 'input'=>$input) );
                });

            });
            $myFile   = $myFile->string('xlsx'); 
            $response = array(
                'name' => 'Monitoring Evaluasi Hasil Peserta Mikrobiologi', 
                'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
            );
            return response()->json($response);

        }else{
            // dd($input);
            $parameter = DB::table('parameter')->where('id', $input['parameter'])->first();
            // dd($parameter);
            $data = DB::table('tb_registrasi')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->join('badan_usaha', 'badan_usaha.id', '=', 'perusahaan.pemerintah')
                    ->join('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                    ->join('regencies', 'perusahaan.kota', '=', 'regencies.id')
                    ->join('hp_headers', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
                    ->join('hp_details', 'hp_details.hp_header_id', '=', 'hp_headers.id')
                    ->where('hp_details.parameter_id', $input['parameter'])
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                    ->select('tb_registrasi.id', 'perusahaan.nama_lab', 'tb_registrasi.perusahaan_id', 'provinces.name', 'badan_usaha.badan_usaha', 'regencies.name as kota', 'hp_headers.type')
                    ->orderBy('perusahaan.id', 'asc')
                    ->groupBy('perusahaan.id', 'hp_headers.type')
                    ->get();
            foreach ($data as $key => $val) {
                $no = 0;
                for ($i=$input['tahun']; $i < $input['tahun'] + 3 ; $i++) { 
                    $no++;
                    if($parameter->kategori == 'kimia air' || $parameter->kategori == 'kimia air terbatas'){
                        $sql = "SELECT
                                    '".$val->type."' AS type, '1' AS Siklus1,
                                    (SELECT zscore
                                    FROM `tb_zscore`
                                    INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_zscore`.`id_registrasi`
                                    WHERE perusahaan_id = ".$val->perusahaan_id."
                                    AND tb_zscore.siklus = 1
                                    AND tahun = ".$i."
                                    AND parameter = ".$input['parameter'].") zscore1, '2' AS Siklus2,
                                    (SELECT zscore
                                    FROM `tb_zscore`
                                    INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_zscore`.`id_registrasi`
                                    WHERE perusahaan_id = ".$val->perusahaan_id."
                                    AND tb_zscore.siklus = 2
                                    AND tahun = ".$i."
                                    AND parameter = ".$input['parameter'].") zscore2";
                    }else{
                        $sql = "SELECT
                                    '".$val->type."' AS type, '1' AS Siklus1,
                                    (SELECT zscore
                                    FROM `tb_zscore`
                                    INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_zscore`.`id_registrasi`
                                    WHERE perusahaan_id = ".$val->perusahaan_id."
                                    AND type = '".$val->type."'
                                    AND tb_zscore.siklus = 1
                                    AND tahun = ".$i."
                                    AND parameter = ".$input['parameter'].") zscore1, '2' AS Siklus2,
                                    (SELECT zscore
                                    FROM `tb_zscore`
                                    INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_zscore`.`id_registrasi`
                                    WHERE perusahaan_id = ".$val->perusahaan_id."
                                    AND type = '".$val->type."'
                                    AND tb_zscore.siklus = 2
                                    AND tahun = ".$i."
                                    AND parameter = ".$input['parameter'].") zscore2";

                        $sqlalat = "SELECT
                                    '".$val->type."' AS type, '1' AS Siklus1,
                                    (SELECT zscore
                                    FROM `tb_zscore_alat`
                                    INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_zscore_alat`.`id_registrasi`
                                    WHERE perusahaan_id = ".$val->perusahaan_id."
                                    AND type = '".$val->type."'
                                    AND tb_zscore_alat.siklus = 1
                                    AND tahun = ".$i."
                                    AND parameter = ".$input['parameter'].") zscore1, '2' AS Siklus2,
                                    (SELECT zscore
                                    FROM `tb_zscore_alat`
                                    INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_zscore_alat`.`id_registrasi`
                                    WHERE perusahaan_id = ".$val->perusahaan_id."
                                    AND type = '".$val->type."'
                                    AND tb_zscore_alat.siklus = 2
                                    AND tahun = ".$i."
                                    AND parameter = ".$input['parameter'].") zscore2";

                        $sqlmetode = "SELECT
                                    '".$val->type."' AS type, '1' AS Siklus1,
                                    (SELECT zscore
                                    FROM `tb_zscore_metode`
                                    INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_zscore_metode`.`id_registrasi`
                                    WHERE perusahaan_id = ".$val->perusahaan_id."
                                    AND type = '".$val->type."'
                                    AND tb_zscore_metode.siklus = 1
                                    AND tahun = ".$i."
                                    AND parameter = ".$input['parameter'].") zscore1, '2' AS Siklus2,
                                    (SELECT zscore
                                    FROM `tb_zscore_metode`
                                    INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_zscore_metode`.`id_registrasi`
                                    WHERE perusahaan_id = ".$val->perusahaan_id."
                                    AND type = '".$val->type."'
                                    AND tb_zscore_metode.siklus = 2
                                    AND tahun = ".$i."
                                    AND parameter = ".$input['parameter'].") zscore2";
                    ${'dataalat' . $no} = DB::table(DB::raw("($sqlalat) t"))->first();
                    ${'datametode' . $no} = DB::table(DB::raw("($sqlmetode) t"))->first();
                    $val->{'dataalat' . $no} = ${'dataalat' . $no};
                    $val->{'datametode' . $no} = ${'datametode' . $no};
                    }
                    ${'data' . $no} = DB::table(DB::raw("($sql) t"))->first();
                    $val->{'data' . $no} = ${'data' . $no};
                }
            }
            // dd($data[0]);
            $myFile = Excel::create('Monitoring Evaluasi Hasil Peserta per Parameter', function($excel) use ($data, $parameter, $input) {
                $shita = explode(' ', $parameter->nama_parameter)[0];
                $shit = str_replace("/"," ",$shita);
                $excel->sheet($shit, function($sheet) use ($data, $parameter, $input) {
                    $sheet->loadView('dashboard.monitoringparameter.View', array('data'=>$data,'parameter'=>$parameter,'input'=>$input) );
                });
            });
            $myFile   = $myFile->string('xlsx'); 
            $response = array(
                'name' => 'Monitoring Evaluasi Hasil Peserta', 
                'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile), 
            );

            return response()->json($response);
        }
    }
}

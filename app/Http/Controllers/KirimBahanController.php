<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\register as Register;
use App\kirimbahan as Kirim;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;

class KirimBahanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siklus = '1';
        $user = Auth::user()->penyelenggara;
        $tahun = date('Y');
        $data = DB::table('sub_bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->leftjoin('regencies', 'perusahaan.kota', '=', 'regencies.id')
                ->leftjoin('districts', 'perusahaan.kecamatan', 'districts.id')
                ->leftjoin('villages', 'perusahaan.kelurahan', 'villages.id')
                ->leftjoin('users', 'perusahaan.created_by', '=', 'users.id')
                ->select('perusahaan.*','tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.tarif as Tarif', 'tb_bidang.bidang as Bidang', 'users.name as Name', 'users.email', 'provinces.name as Provinsi', 'regencies.name as Kota', 'districts.name as Kecamatan', 'villages.name as Kelurahan')
                ->where('tb_registrasi.status', '2')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $tahun)
                ->where(function($query){
                    $query->where('tb_registrasi.siklus', '=', '1')
                        ->orwhere('tb_registrasi.siklus', '=', '12');
                })
                ->orderBy('Name', 'asc')
                ->get();
        return view('kirim_bahan', compact('data','siklus'));
    }
    public function proses(\Illuminate\Http\Request $request){

        $role = Auth::user()->role;
        if ($role == 6) {
            $bidang = $request->bidang;
            $jumlah_dipilih = count($bidang);
            $date = date('Y-m-d');
            // $datas1 = $request->get('jenis_barang');
            // $datas2 = $request->get('jumlah');
            // dd($bidang);
            for($x=0;$x<$jumlah_dipilih;$x++){
                $data['status'] = '3';
                $i= 0;
                foreach ($bidang as $key => $valb) {
                    $kirim['id_registrasi'] = $valb;
                    $kirim['created_by'] = Auth::user()->id;
                    $kirim['siklus'] = '1';
                    Register::where('id', $valb)->update($data);
                    Kirim::create($kirim);
                    $i++;
                }

                // $email = DB::table('users')
                //         ->leftjoin('tb_registrasi', 'users.id', '=', 'tb_registrasi.created_by')
                //         ->leftjoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                //         ->leftjoin('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                //         ->select('users.email', 'tb_registrasi.siklus', 'sub_bidang.parameter as parameter', 'perusahaan.nama_lab', 'tb_registrasi.kode_lebpes')
                //         ->whereIn('tb_registrasi.id', ($bidang))
                //         ->get();
                //         // dd($email); 
                // $datu = [
                //         'labklinik' => $email[0]->nama_lab,
                //         'kode_lebpes' => $email[0]->kode_lebpes,
                //         'date' => date('Y'),
                //         'siklus' => '1',
                //         'tanggal' => $date,
                //         'parameter' => $email
                //         ];
                // Mail::send('email.kirim_bahan', $datu, function ($mail) use ($email)
                // {
                //   $mail->from('pme.bblksub@gmail.com', 'BBLK Surabaya Bahan Uji '.$email[0]->nama_lab);
                //   // $mail->to('tengkufirmansyah2@gmail.com');
                //   $mail->to($email[0]->email);
                //   $mail->subject('Kirim Bahan PNPME');
                // });
                 
                return redirect('kirim-bahan');
            }
        }
    }

    public function index2()
    {
        $siklus = '2';
        $user = Auth::user()->penyelenggara;
        $tahun = date('Y');
        $data = DB::table('sub_bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->leftjoin('regencies', 'perusahaan.kota', '=', 'regencies.id')
                ->leftjoin('districts', 'perusahaan.kecamatan', 'districts.id')
                ->leftjoin('villages', 'perusahaan.kelurahan', 'villages.id')
                ->leftjoin('users', 'perusahaan.created_by', '=', 'users.id')
                ->select('perusahaan.*','tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.tarif as Tarif', 'tb_bidang.bidang as Bidang', 'users.name as Name', 'users.email', 'provinces.name as Provinsi', 'regencies.name as Kota', 'districts.name as Kecamatan', 'villages.name as Kelurahan')
                ->where('tb_registrasi.status', '>=', '2')
                ->whereNull('tb_registrasi.status_2')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $tahun)
                ->where(function($query){
                    $query->where('tb_registrasi.siklus', '=', '2')
                        ->orwhere('tb_registrasi.siklus', '=', '12');
                })
                ->orderBy('Name', 'asc')
                ->get();
        // dd($data);
        return view('kirim_bahan', compact('data','siklus'));
    }
    public function proses2(\Illuminate\Http\Request $request){

        $role = Auth::user()->role;
        if ($role == 6) {
            $bidang = $request->bidang;
            $jumlah_dipilih = count($bidang);
            $date = date('Y-m-d');
            // $datas1 = $request->get('jenis_barang');
            // $datas2 = $request->get('jumlah');
            // dd($bidang);
            for($x=0;$x<$jumlah_dipilih;$x++){
                $data['status'] = '3';
                $data['status_2'] = '3';
                $i= 0;
                foreach ($bidang as $key => $valb) {
                    $kirim['id_registrasi'] = $valb;
                    $kirim['created_by'] = Auth::user()->id;
                    $kirim['siklus'] = '2';
                    Register::where('id', $valb)->update($data);
                    Kirim::create($kirim);
                    $i++;
                }

                // $email = DB::table('users')
                //         ->leftjoin('tb_registrasi', 'users.id', '=', 'tb_registrasi.created_by')
                //         ->leftjoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                //         ->leftjoin('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                //         ->select('users.email', 'tb_registrasi.siklus', 'sub_bidang.parameter as parameter', 'perusahaan.nama_lab', 'tb_registrasi.kode_lebpes')
                //         ->whereIn('tb_registrasi.id', ($bidang))
                //         ->get();
                //         // dd($email); 
                // $datu = [
                //         'labklinik' => $email[0]->nama_lab,
                //         'kode_lebpes' => $email[0]->kode_lebpes,
                //         'date' => date('Y'),
                //         'siklus' => '2',
                //         'tanggal' => $date,
                //         'parameter' => $email
                //         ];
                // Mail::send('email.kirim_bahan', $datu, function ($mail) use ($email)
                // {
                //   $mail->from('pme.bblksub@gmail.com', 'BBLK Surabaya Bahan Uji '.$email[0]->nama_lab);
                //   // $mail->to('tengkufirmansyah2@gmail.com');
                //   $mail->to($email[0]->email);
                //   $mail->subject('Kirim Bahan PNPME');
                // });
                 
                return redirect('kirim-bahan-siklus2');
            }
        }
    }
}

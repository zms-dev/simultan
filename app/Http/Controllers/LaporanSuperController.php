<?php

namespace App\Http\Controllers;

use DB;
use Excel;
use PDF;
use Session;

use App\User;
use App\subbidang;
use App\daftar as Daftar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class LaporanSuperController extends Controller
{
    //

    public function rekapNilai(){
      $sb = subbidang::all();
      return view('laporan.rekap.indexRekapPeserta', compact('sb'));
    }

    public function RekapNilaiList(\Illuminate\Http\Request $request ){
      // return (Auth::user());
      // return ($request->all());
      $tahun = $request->tahun;
      $bidang = $request->bidang;
      $siklus = $request->siklus;

      if($bidang != 'AHIV'){
        $bidangL = DB::table('sub_bidang')
                ->where('id',$bidang)
                ->first();
        $bidangR = $bidangL->alias;
      }else{
        $bidangR = "Anti HIV PMI";
      }


      $parameterna = null;
      if($bidang == '7') { //sipilis
        $datas = Daftar::select(
                                'nama_lab',
                                'badan_usaha.badan_usaha',
                                'tb_registrasi.id',
                                DB::raw('(select ketepatan from tb_kesimpulan_evaluasi where id_registrasi = tb_registrasi.id and `siklus` = "'.$siklus.'" AND type = "Syphilis" ) as ketetapanTP'),
                                DB::raw('(select ketepatan from tb_kesimpulan_evaluasi where id_registrasi = tb_registrasi.id and `siklus` = "'.$siklus.'" AND type = "rpr-syphilis" ) as ketetapanRPR')
                                )
                ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->leftjoin('badan_usaha','perusahaan.pemerintah','badan_usaha.id')
                ->where('tb_registrasi.bidang','7')
                ->groupBy('perusahaan.id')
                ->get();
      }elseif($bidang == '2') { //KK

        $parameterna = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
        $data = DB::table('hp_headers')
                ->leftjoin('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->leftJoin('perusahaan','tb_registrasi.perusahaan_id', '=','perusahaan.id')
                ->leftjoin('badan_usaha','perusahaan.pemerintah','badan_usaha.id')
                ->where('tb_registrasi.bidang', '=', '2')
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', 'a')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->select('hp_headers.kode_lab', 'tb_registrasi.updated_at','perusahaan.nama_lab','hp_headers.id','badan_usaha.badan_usaha')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                            ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                            ->where('hp_header_id', $val->id)
                            ->where('hp_details.parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;

            }
            $val->parameter = $parameter;
        }
        // dd($data);
        // return view('laporan.rekap.kk', compact('data','parameterna'));

        Excel::create('Laporan Kimia Klinik', function($excel) use ($data, $parameterna) {
            $excel->sheet('Laporan', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('laporan.rekap.kk', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
        })->download('xls');

      }elseif($bidang == '6') { //ANti HIV
        $datas = Daftar::select(
                                'nama_lab',
                                'badan_usaha.badan_usaha',
                                'tb_registrasi.id',
                                DB::raw('(select ketepatan from tb_kesimpulan_evaluasi where id_registrasi = tb_registrasi.id and `siklus` = "'.$siklus.'" AND type = "Anti Hiv" ) as ketetapan')
                                )
                ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->leftjoin('badan_usaha','perusahaan.pemerintah','badan_usaha.id')
                ->where('tb_registrasi.bidang','6')
                ->where('perusahaan.pemerintah','!=','9')
                ->groupBy('perusahaan.id')
                ->get();
      }elseif($bidang == 'AHIV') { //antihiv pmi
        $datas = Daftar::select(
                                'nama_lab',
                                'badan_usaha.badan_usaha',
                                'tb_registrasi.id',
                                DB::raw('(select ketepatan from tb_kesimpulan_evaluasi where id_registrasi = tb_registrasi.id and `siklus` = "'.$siklus.'" AND type = "Anti Hiv" ) as ketetapan')
                                )
                ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->leftjoin('badan_usaha','perusahaan.pemerintah','badan_usaha.id')
                ->where('tb_registrasi.bidang','6')
                ->where('perusahaan.pemerintah','=','9')
                ->groupBy('perusahaan.id')
                ->get();
      }elseif($bidang == '1') { //hema
        $parameterna = DB::table('parameter')
                        ->where('kategori', 'hematologi')
                        ->orderBy('bagian', 'asc')
                        ->get();
        $datas = DB::table('hp_headers')
                      ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                      ->leftJoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                      ->leftjoin('badan_usaha','perusahaan.pemerintah','badan_usaha.id')
                      ->where('tb_registrasi.bidang', '=', '1')
                      ->where('hp_headers.siklus', $siklus)
                      ->where('hp_headers.type', 'a')
                      ->select('hp_headers.kode_lab','hp_headers.id','hp_headers.id_registrasi', 'tb_registrasi.updated_at','perusahaan.nama_lab','badan_usaha.badan_usaha')
                      ->groupBy('kode_lab')
                      ->orderBy('kode_lab')
                      ->get();
        foreach ($datas as $key => $val) {
          $parameter = DB::table('parameter')->where('kategori', 'hematologi')->orderBy('bagian', 'asc')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('tb_zscore')
                            ->select(
                                'id_registrasi',
                                'parameter',
                                'siklus',
                                'zscore as zpeserta',
                                DB::raw('(SELECT zscore FROM tb_zscore_metode WHERE id_registrasi = tb_zscore.id_registrasi AND parameter = tb_zscore.parameter AND type = "a" AND siklus = tb_zscore.siklus) as zmetode'),
                                DB::raw('(SELECT zscore FROM tb_zscore_alat WHERE id_registrasi = tb_zscore.id_registrasi AND parameter = tb_zscore.parameter AND type = "a" AND siklus = tb_zscore.siklus) as zalat')
                            )
                            ->where('id_registrasi', $val->id_registrasi)
                            ->where('parameter', $par->id)
                            ->where('type', 'a')
                            ->where('siklus', $siklus)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }


        $datas2 = DB::table('hp_headers')
                      ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                      ->leftJoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                      ->leftjoin('badan_usaha','perusahaan.pemerintah','badan_usaha.id')
                      ->where('tb_registrasi.bidang', '=', '1')
                      ->where('hp_headers.siklus', $siklus)
                      ->where('hp_headers.type', 'b')
                      ->select('hp_headers.kode_lab','hp_headers.id','hp_headers.id_registrasi', 'tb_registrasi.updated_at','perusahaan.nama_lab','badan_usaha.badan_usaha')
                      ->groupBy('kode_lab')
                      ->orderBy('kode_lab')
                      ->get();
        foreach ($datas2 as $key2 => $val2) {
          $parameter2 = DB::table('parameter')->where('kategori', 'hematologi')->orderBy('bagian', 'asc')->get();
            foreach ($parameter2 as $key2 => $par2) {
                $detail2 = DB::table('tb_zscore')
                            ->select(
                                'id_registrasi',
                                'parameter',
                                'siklus',
                                'zscore as zpeserta',
                                DB::raw('(SELECT zscore FROM tb_zscore_metode WHERE id_registrasi = tb_zscore.id_registrasi AND parameter = tb_zscore.parameter AND type = "a" AND siklus = tb_zscore.siklus) as zmetode'),
                                DB::raw('(SELECT zscore FROM tb_zscore_alat WHERE id_registrasi = tb_zscore.id_registrasi AND parameter = tb_zscore.parameter AND type = "a" AND siklus = tb_zscore.siklus) as zalat')
                            )
                            ->where('id_registrasi', $val2->id_registrasi)
                            ->where('parameter', $par2->id)
                            ->where('type', 'b')
                            ->where('siklus', $siklus)
                            ->get();
                $par2->detail = $detail2;
            }
            $val2->parameter = $parameter2;
        }
        // dd($datas);
        // return view('laporan.rekap.hema', compact('datas','tahun', 'bidang','parameterna'));

        Excel::create('Laporan Peserta Hematologi', function($excel) use ($datas, $tahun, $bidang, $parameterna, $datas2) {
            $excel->sheet('01', function($sheet) use ($datas, $parameterna, $bidang, $tahun) {
                $sheet->loadView('laporan.rekap.hema', array('datas'=>$datas, 'parameterna'=>$parameterna, 'bidang'=>$bidang, 'tahun'=>$tahun));
            });
            $excel->sheet('02', function($sheet) use ($datas2, $parameterna, $bidang, $tahun) {
                $sheet->loadView('laporan.rekap.hema', array('datas'=>$datas2, 'parameterna'=>$parameterna, 'bidang'=>$bidang, 'tahun'=>$tahun));
            });
        })->download('xls');

      }elseif($bidang == '3') { //urin
          if($siklus == '1'){
            $bahan = 'Negatif';
            $tipe = 'a';
          }else{
            $tipe = 'b';
            $bahan = 'Positif';
          }

          $datas = Daftar::select(
                                  'nama_lab',
                                  'badan_usaha.badan_usaha',
                                  'tb_registrasi.id',
                                  DB::raw('(select skoring from tb_skoring_urinalisa where id_registrasi = tb_registrasi.id and `type` = "'.$tipe.'" and `bahan` = "'.$bahan.'" ) as skoring')
                                  )
                  ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                  ->leftjoin('badan_usaha','perusahaan.pemerintah','badan_usaha.id')
                  ->where('tb_registrasi.bidang','3')
                  ->groupBy('perusahaan.id')
                  ->get();
      }elseif($bidang == '8'){ //hbsag
        $datas = Daftar::select(
                                'nama_lab',
                                'badan_usaha.badan_usaha',
                                'tb_registrasi.id',
                                DB::raw('(select ketepatan from tb_kesimpulan_evaluasi where id_registrasi = tb_registrasi.id and `siklus` = "'.$siklus.'" and `type` = "HBsAg" ) as ketetapan')
                                )
                ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->leftjoin('badan_usaha','perusahaan.pemerintah','badan_usaha.id')
                ->where('tb_registrasi.bidang','8')
                ->groupBy('perusahaan.id')
                ->get();
      }elseif($bidang == '9'){ //anti hcv
        $datas = Daftar::select(
                                'nama_lab',
                                'badan_usaha.badan_usaha',
                                'tb_registrasi.id',
                                DB::raw('(select ketepatan from tb_kesimpulan_evaluasi where id_registrasi = tb_registrasi.id and `siklus` = "'.$siklus.'" and `type` = "ANTI-HCV" ) as ketetapan')
                                )
                ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->leftjoin('badan_usaha','perusahaan.pemerintah','badan_usaha.id')
                ->where('tb_registrasi.bidang','9')
                ->groupBy('perusahaan.id')
                ->get();
      }elseif($bidang == '5'){ //tc
        $datas = Daftar::select(
                                'nama_lab',
                                'badan_usaha.badan_usaha',
                                'tb_registrasi.id',
                                DB::raw('(select AVG(nilai) as nilai from `tb_evaluasi_tc` where `id_registrasi` = tb_registrasi.id and `siklus` = "'.$siklus.'" ) as nilai')
                                )
                ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->leftjoin('badan_usaha','perusahaan.pemerintah','badan_usaha.id')
                ->where('tb_registrasi.bidang','5')
                ->groupBy('perusahaan.id')
                ->get();
      }elseif($bidang == '4'){ //bta
        $datas = Daftar::select(
                                'nama_lab',
                                'badan_usaha.badan_usaha',
                                'tb_registrasi.id',
                                DB::raw('(select sum(nilai) as nilai from `tb_evaluasi_bta` where `id_registrasi` = tb_registrasi.id and `siklus` = "'.$siklus.'" ) as nilai')
                                )
                ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->leftjoin('badan_usaha','perusahaan.pemerintah','badan_usaha.id')
                ->where('tb_registrasi.bidang','4')
                ->groupBy('perusahaan.id')
                ->get();
      }elseif($bidang == '10'){ //malaria
        $datas = Daftar::select(
                                'nama_lab',
                                'badan_usaha.badan_usaha',
                                'tb_registrasi.id',
                                DB::raw('(select total from `tb_evaluasi_malaria` where `id_registrasi` = tb_registrasi.id and `siklus` = "'.$siklus.'" ) as nilai')
                                )
                ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->leftjoin('badan_usaha','perusahaan.pemerintah','badan_usaha.id')
                ->where('tb_registrasi.bidang','10')
                ->groupBy('perusahaan.id')
                ->get();
      }elseif ($bidang == '11') { //kimiaair
        $parameterna = DB::table('parameter')->where('kategori', 'kimia air')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->leftJoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->leftjoin('badan_usaha','perusahaan.pemerintah','badan_usaha.id')
                ->where('tb_registrasi.bidang', '=', '11')
                ->where('hp_headers.siklus', $siklus)
                ->select('hp_headers.*','perusahaan.nama_lab','badan_usaha.badan_usaha')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'kimia air')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->where('hp_header_id', $val->id)
                            ->where('parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data);
        Excel::create('Laporan Kimia Air', function($excel) use ($data, $parameterna) {
            $excel->sheet('KK', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('laporan.rekap.air', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
        })->download('xls');
      }elseif($bidang == '13'){ //identifikasi
        $datas = Daftar::select(
                                'nama_lab',
                                'badan_usaha.badan_usaha',
                                'tb_registrasi.id',
                                DB::raw('(select keterangan from `tb_status_bakteri` where `id_registrasi` = tb_registrasi.id and `siklus` = "'.$siklus.'" and jenis_pemeriksa = "Identifikasi" ) as nilai')
                                )
                ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->leftjoin('badan_usaha','perusahaan.pemerintah','badan_usaha.id')
                ->where('tb_registrasi.bidang','13')
                ->groupBy('perusahaan.id')
                ->get();
      }elseif($bidang == '12'){ //KimiaAirTerbatas
        $parameterna = DB::table('parameter')->where('kategori', 'kimia air terbatas')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->leftJoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->leftjoin('badan_usaha','perusahaan.pemerintah','badan_usaha.id')
                ->where('tb_registrasi.bidang', '=', '12')
                ->where('hp_headers.siklus', $siklus)
                ->select('hp_headers.*','perusahaan.nama_lab','badan_usaha.badan_usaha')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'kimia air terbatas')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->where('hp_header_id', $val->id)
                            ->where('parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data);
        Excel::create('Laporan Hasil Kimia Air Terbatas', function($excel) use ($data, $parameterna) {
            $excel->sheet('Terbatas', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('laporan.rekap.air_terbatas', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
        })->download('xls');
      }else{
        return('kole');
      }

      Excel::create('Laporan rekap peserta', function($excel) use ($datas, $parameterna, $bidangR, $bidang) {
          $excel->sheet('Terbatas', function($sheet) use ($datas, $parameterna, $bidangR, $bidang) {
              $sheet->loadView('laporan.rekap.list', array('datas'=>$datas, 'parameterna'=>$parameterna, 'bidangR'=>$bidangR, 'bidang'=>$bidang) );
          });
      })->download('xls');
    // return view('laporan.rekap.list', compact('datas','tahun', 'bidang','parameterna','bidangR'));
  }
}

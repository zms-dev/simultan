<?php

namespace App\Http\Controllers\Auth;


use App\User;
use App\daftar as Daftar;
use App\register as Register;
use App\subbidang as Subbidang;

Use DB;
use Request;
use Session;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Redirect;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/status-pembayaran';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $userid = DB::table('users')->orderBy('id_member', 'desc')->first();
        $tahun = date('Y');
        $register = DB::table('tb_registrasi')
                    ->select(DB::raw('CAST(no_urut AS UNSIGNED) as no_uruts'))
                    ->join('users', 'users.id','=','tb_registrasi.created_by')
                    ->where(DB::raw('Year(tb_registrasi.created_at)'), $tahun)
                    ->whereNotNull('no_urut')
                    ->groupBy('no_uruts')
                    ->orderBy('no_uruts', 'desc')
                    ->first();
        if(count($register)){
            $cekno = (int)$register->no_uruts + 1;
            $no_urut = sprintf("%04s", $cekno);
        }else{
            $no_urut = '0001';
        }
        if($userid != NULL){
            $id_member = $userid->id_member + 1;
            //--Login--//
            $user = new User;
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->role = '3';
            $user->id_member = $id_member;
            $user->password = bcrypt($data['password']);
            $user->save();

            $dilaksanakan = "";

            $saveDaftar = new Daftar;
            $saveDaftar->nama_lab = $data['name'];
            $saveDaftar->pemerintah = $data['pemerintah'];
            $saveDaftar->alamat = $data['alamat'];
            $saveDaftar->provinsi = $data['provinsi'];
            $saveDaftar->kota = $data['kota'];
            $saveDaftar->kecamatan = $data['kecamatan'];
            $saveDaftar->kelurahan = $data['kelurahan'];
            $saveDaftar->telp = $data['telp'];
            $saveDaftar->email = $data['email_cp'];
            $saveDaftar->penanggung_jawab = $data['penanggung_jawab'];
            $saveDaftar->personal = $data['personal'];
            $saveDaftar->no_hp = $data['no_hp'];
            $saveDaftar->no_wa = $data['no_wa'];
            $saveDaftar->akreditasi = $data['akreditasi'];
            $saveDaftar->pemantapan_mutu = $data['pemantapan_mutu'];
            $saveDaftar->alasan_binaan = $data['alasan_binaan'];
            if(!empty($data['dilaksanakan'])){
                $saveDaftar->dilaksanakan = implode(", ",$data['dilaksanakan']);
            }
            $saveDaftar->created_by = $user->id;
            $saveDaftar->save();

            $saveDaftarID = $saveDaftar->id;

            $i = 0;
            $a = $user->id_member;
            $kode_pes = sprintf("%04s", $a);
            $date = date('y');
                foreach ($data['bidang'] as $alat) {
                    $datas = $data;
                    if($alat != ''){
                        $saveRegister = new Register;
                        $saveRegister->perusahaan_id = $saveDaftar->id;
                        $saveRegister->bidang = $alat;
                        $saveRegister->kode_lebpes = $kode_pes.'/'.$data['alias'][$alat - 1].'/'.$data['siklus'].'/'.$date;

                        if ($data['pembayaran'] == 'transfer')
                        {
                            $saveRegister->no_urut = $no_urut;
                            $saveRegister->file = '';
                            $saveRegister->status = '1';
                            $saveRegister->id_pembayaran = '1';
                        }else{
                            $saveRegister->no_urut = '';
                            $saveRegister->sptjm = $data['sptjm'];
                            // $saveRegister->file = $name1;
                            $saveRegister->id_pembayaran = '2';
                            $saveRegister->status = '1';
                        }
                        $saveRegister->siklus = $data['siklus'];
                        $saveRegister->created_by = $user->id;
                        $saveRegister->save();

                        // $kuota = DB::table('sub_bidang')->where('id', $alat)->first();
                        // $sisa['kuota_1'] = $kuota->kuota_1 - 1;
                        // $sisa['kuota_2'] = $kuota->kuota_2 - 1;
                        
                        // Subbidang::where('id',$alat)->update($sisa);
                        
                        Session::flash('message', 'Pendaftaran berhasil. Mohon tunggu untuk dikonfirmasi!'); 
                        Session::flash('alert-class', 'alert-success'); 
                    }
                    $i++;
                }

                $sub_bidang = DB::table('sub_bidang')->whereIn('id', $data['bidang'])->get();
                $userna = DB::table('users')->where('role', 4)->first();
                $datu = [
                        'labklinik' => $data['name'],
                        'siklus' => $data['siklus'],
                        'date' => date('Y'),
                        'parameter' => $sub_bidang 
                ];
                Mail::send('email.pendaftaran', $datu, function ($mail) use ($datas, $userna)
                {
                  $mail->from('pme.bblksub@gmail.com', 'BBLK Surabaya Daftar Peserta '.$datas['name']);
                  // $mail->to('tengkufirmansyah2@gmail.com');
                  $mail->to($userna->email);
                  $mail->subject('Daftar PNPME Peserta '.$datas['name']);
                });

                if ($data['pembayaran'] == 'transfer') {
                    // $datu = [
                    //     'no_urut' => $no_urut
                    // ];
                    // Mail::send('email.transfer',$datu, function ($mail) use ($data)
                    // {
                    //       $mail->from('pme.bblksub@gmail.com', 'BBLK Surabaya Daftar Peserta ');
                    //       $mail->to($data['email']);
                    //       $mail->subject('Daftar PNPME Surabaya ');
                    // });
                }else{
                    Mail::send('email.sptjm',[], function ($mail) use ($data)
                    {
                          $mail->from('pme.bblksub@gmail.com', 'BBLK Surabaya Daftar Peserta ');
                          $mail->to($data['email']);
                          $mail->subject('Daftar PNPME Surabaya');
                    });
                }
            return $user;

        }else{
    // dd($data);
        return User::create([
            'name' => $data['name'],
            'role' => '3',
            'email' => $data['email'],
            'id_member' => '0',
            'password' => bcrypt($data['password']),
        ]);
        }
    }
}

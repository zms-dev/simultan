<?php
namespace App\Http\Controllers;
use DB;
use Request;
use Auth;
use Input;
use Session;

use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Perusahaan;
use App\User;
use App\Registrasi;
use App\TandaTerima;
use Illuminate\Support\Facades\Redirect;
class DataPerusahaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(\Illuminate\Http\Request $request)
    {

        if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Perusahaan::select(DB::raw('*, @rownum := @rownum +1 as rownum'),
            'perusahaan.id',
            'perusahaan.nama_lab as nama_lab',
            'perusahaan.alamat',
            'perusahaan.telp',
            'perusahaan.created_by',
            'users.id',
            'users.name',
            'users.email',
            'users.penyelenggara')
            ->join('users', 'users.id', '=','perusahaan.created_by')
            ->where('users.role','=',3)
            ->orderBy('perusahaan.id')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="data-perusahaan/edit/'.$data->id.'" style="float: center;" >
                                    <button class="btn btn-primary btn-xs">
                                        <i class="icon-pencil"></i>
                                    </button>
                                </a>&nbsp;
                                <a href="data-perusahaan/delete'.'/'.$data->id.'" onClick="return ConfirmDelete()">
                                    <button class="btn btn-danger btn-xs">
                                        <i class="icon-trash "></i>
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('back/perusahaan/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(\Illuminate\Http\Request $request, $id)
    {
        $data = DB::table('perusahaan')
                ->where('created_by', $id)
                ->first();

        if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Registrasi::select(DB::raw(' @rownum := @rownum +1 as rownum'),
            'tb_registrasi.id',
            'tb_registrasi.status',
            'tb_registrasi.no_urut',
            'tb_registrasi.bidang',
            'sub_bidang.parameter',
            'tb_bidang.bidang as nama',
             DB::raw('YEAR(tb_registrasi.created_at) as tahun'))
            ->join('users', 'users.id', '=','tb_registrasi.created_by')
            ->join('sub_bidang', 'sub_bidang.id', '=','tb_registrasi.bidang')
            ->join('tb_bidang', 'tb_bidang.id', '=','sub_bidang.id_bidang')
            ->where('users.role','=',3)
            ->where('users.id','=',$id)
            ->orderBy('tb_registrasi.id', 'desc')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<center><a href="../../data-perusahaan/hapus'.'/'.$data->id.'" onClick="return ConfirmDelete()">
                                    <button class="btn btn-danger btn-xs">
                                        <i class="icon-trash "></i>
                                    </button>
                                </a></center>'
                        ;})
            ->make(true);
            
        }
        return view('back/perusahaan/edit',compact('id', 'data'));
    }

    public function edit1(\Illuminate\Http\Request $request, $id)
    {

        if($request->ajax()){
            DB::statement(DB::raw('set @rownum=0')); 
            $datas = Registrasi::select(DB::raw(' @rownum := @rownum +1 as rownum'),
            'tb_registrasi.id',
            'tb_registrasi.status',
            'tb_registrasi.siklus',
            'tb_registrasi.bidang',
            'tb_registrasi.status_data1',
            'tb_registrasi.status_data2',
            'tb_registrasi.status_datarpr1',
            'tb_registrasi.status_datarpr2',
            'sub_bidang.parameter',
            'sub_bidang.link',
             DB::raw('YEAR(tb_registrasi.created_at) as tahun'))
            ->join('users', 'users.id', '=','tb_registrasi.created_by')
            ->join('sub_bidang', 'sub_bidang.id', '=','tb_registrasi.bidang')
            ->where('users.role','=',3)
            ->where('users.id','=',$id)
            ->orderBy('tb_registrasi.id', 'desc')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    if ($data->bidang <= 3) {
                        if ($data->status_data1 == 1 && $data->status_data2 == 1) {
                            return "".'<center>
                                        <a href="../../..'.$data->link.'/edit'.'/'.$data->id.'?y=1&x=a" target="_blank">
                                            <button class="btn btn-danger btn-xs">
                                                <i class="icon-pencil"> Edit Hasil [01]</i>
                                            </button>
                                        </a><p></p>
                                        <a href="../../..'.$data->link.'/edit'.'/'.$data->id.'?y=1&x=b" target="_blank">
                                            <button class="btn btn-danger btn-xs">
                                                <i class="icon-pencil"> Edit Hasil [02]</i>
                                            </button>
                                        </a></center>';
                        }elseif($data->status_data1 == 2 && $data->status_data2 == 1) {
                            return "".'<center>
                                        <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=1&x=a" >
                                            <button class="btn btn-primary btn-xs">
                                                <i class="icon-book "> Kirim Hasil [01]</i>
                                            </button>
                                        </a><p></p>
                                        <a href="../../..'.$data->link.'/edit'.'/'.$data->id.'?y=1&x=b" target="_blank">
                                            <button class="btn btn-danger btn-xs">
                                                <i class="icon-pencil "> Edit Hasil [02]</i>
                                            </button>
                                        </a></center>';
                        }elseif($data->status_data1 == 1 && $data->status_data2 == 2) {
                            return "".'<center><a href="../../..'.$data->link.'/edit'.'/'.$data->id.'?y=1&x=a" target="_blank">
                                            <button class="btn btn-danger btn-xs">
                                                <i class="icon-pencil "> Edit Hasil [01]</i>
                                            </button>
                                        </a><p></p>
                                        <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=1&x=b">
                                            <button class="btn btn-primary btn-xs">
                                                <i class="icon-book "> Kirim Hasil [02]</i>
                                            </button>
                                        </a></center>';
                        }elseif($data->status_data1 == 2 && $data->status_data2 == 2) {
                            return "".'<center>
                                        <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=1&x=a">
                                            <button class="btn btn-primary btn-xs">
                                                <i class="icon-book "> Kirim Hasil [01]</i>
                                            </button>
                                        </a><p></p>
                                        <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=1&x=b">
                                            <button class="btn btn-primary btn-xs">
                                                <i class="icon-book "> Kirim Hasil [02]</i>
                                            </button>
                                        </a></center>';
                        }
                    }elseif ($data->bidang == 7) {
                        if ($data->status_data1 == 1 && $data->status_datarpr1 == 1) {
                            return "".'<center>
                                    <a href="../../..'.$data->link.'/edit'.'/'.$data->id.'?y=1" target="_blank">
                                        <button class="btn btn-danger btn-xs">
                                            <i class="icon-pencil "> Edit Hasil TP</i>
                                        </button>
                                    </a><p></p>
                                    <a href="../../../hasil-pemeriksaan/rpr-syphilis/edit'.'/'.$data->id.'?y=1" target="_blank">
                                        <button class="btn btn-danger btn-xs">
                                            <i class="icon-pencil "> Edit Hasil RPR</i>
                                        </button>
                                    </a></center>';
                        }elseif($data->status_data1 == 2 && $data->status_datarpr1 == 1) {
                            return "".'<center>
                                    <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=1&x=a">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="icon-book "> Kirim Hasil TP</i>
                                        </button>
                                    </a><p></p>
                                    <a href="../../../hasil-pemeriksaan/rpr-syphilis/edit'.'/'.$data->id.'?y=1" target="_blank">
                                        <button class="btn btn-danger btn-xs">
                                            <i class="icon-pencil "> Edit Hasil RPR</i>
                                        </button>
                                    </a></center>';
                        }elseif($data->status_data1 == 1 && $data->status_datarpr1 == 2) {
                            return "".'<center>
                                    <a href="../../..'.$data->link.'/edit'.'/'.$data->id.'?y=1" target="_blank">
                                        <button class="btn btn-danger btn-xs">
                                            <i class="icon-pencil "> Edit Hasil TP</i>
                                        </button>
                                    </a><p></p>
                                    <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=1&x=b">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="icon-book "> Kirim Hasil RPR</i>
                                        </button>
                                    </a></center>';
                        }elseif($data->status_data1 == 2 && $data->status_datarpr1 == 2) {
                            return "".'<center>
                                    <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=1&x=a">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="icon-book "> Kirim Hasil TP</i>
                                        </button>
                                    </a><p></p>
                                    <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=1&x=b">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="icon-book "> Kirim Hasil RPR</i>
                                        </button>
                                    </a></center>';
                        }elseif($data->status_data1 == 1) {
                            return "".'<center>
                                    <a href="../../..'.$data->link.'/edit'.'/'.$data->id.'?y=1" target="_blank">
                                        <button class="btn btn-danger btn-xs">
                                            <i class="icon-pencil "> Edit Hasil TP</i>
                                        </button>
                                    </a>';
                        }elseif($data->status_datarpr1 == 1) {
                            return "".'<center>
                                    <a href="../../../hasil-pemeriksaan/rpr-syphilis/edit'.'/'.$data->id.'?y=1" target="_blank">
                                        <button class="btn btn-danger btn-xs">
                                            <i class="icon-pencil "> Edit Hasil RPR</i>
                                        </button>
                                    </a>';
                        }elseif($data->status_data1 == 2) {
                            return "".'<center>
                                    <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=1&x=a">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="icon-book "> Kirim Hasil TP</i>
                                        </button>
                                    </a>';
                        }elseif($data->status_datarpr1 == 2) {
                            return "".'<center>
                                    <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=1&x=b">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="icon-book "> Kirim Hasil RPR</i>
                                        </button>
                                    </a>';
                        }
                    }else{
                        if ($data->status_data1 == 1) {
                            return "".'<center>
                                        <a href="../../..'.$data->link.'/edit'.'/'.$data->id.'?y=1&x=a" target="_blank" >
                                            <button class="btn btn-danger btn-xs">
                                                <i class="icon-book "> Edit Hasil </i>
                                            </button>
                                        </a></center>';
                        }elseif ($data->status_data1 == 2) {
                            return "".'<center>
                                        <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=1&x=a" >
                                            <button class="btn btn-primary btn-xs">
                                                <i class="icon-book "> Kirim Hasil </i>
                                            </button>
                                        </a></center>';
                        }
                        
                    }
            })
            ->addColumn('action1', function($data){
                    if ($data->bidang <= 3) {
                        if ($data->status_datarpr1 == 1 && $data->status_datarpr2 == 1) {
                            return "".'<center>
                                        <a href="../../..'.$data->link.'/edit'.'/'.$data->id.'?y=2&x=a" target="_blank">
                                            <button class="btn btn-danger btn-xs">
                                                <i class="icon-pencil"> Edit Hasil [01]</i>
                                            </button>
                                        </a><p></p>
                                        <a href="../../..'.$data->link.'/edit'.'/'.$data->id.'?y=2&x=b" target="_blank">
                                            <button class="btn btn-danger btn-xs">
                                                <i class="icon-pencil"> Edit Hasil [02]</i>
                                            </button>
                                        </a></center>';
                        }elseif($data->status_datarpr1 == 2 && $data->status_datarpr2 == 1) {
                            return "".'<center>
                                        <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=2&x=a" >
                                            <button class="btn btn-primary btn-xs">
                                                <i class="icon-book "> Kirim Hasil [01]</i>
                                            </button>
                                        </a><p></p>
                                        <a href="../../..'.$data->link.'/edit'.'/'.$data->id.'?y=2&x=b" target="_blank">
                                            <button class="btn btn-danger btn-xs">
                                                <i class="icon-pencil "> Edit Hasil [02]</i>
                                            </button>
                                        </a></center>';
                        }elseif($data->status_datarpr1 == 1 && $data->status_datarpr2 == 2) {
                            return "".'<center><a href="../../..'.$data->link.'/edit'.'/'.$data->id.'?y=2&x=a" target="_blank">
                                            <button class="btn btn-danger btn-xs">
                                                <i class="icon-pencil "> Edit Hasil [01]</i>
                                            </button>
                                        </a><p></p>
                                        <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=2&x=b">
                                            <button class="btn btn-primary btn-xs">
                                                <i class="icon-book "> Kirim Hasil [02]</i>
                                            </button>
                                        </a></center>';
                        }elseif($data->status_datarpr1 == 2 && $data->status_datarpr2 == 2) {
                            return "".'<center>
                                        <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=2&x=a">
                                            <button class="btn btn-primary btn-xs">
                                                <i class="icon-book "> Kirim Hasil [01]</i>
                                            </button>
                                        </a><p></p>
                                        <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=2&x=b">
                                            <button class="btn btn-primary btn-xs">
                                                <i class="icon-book "> Kirim Hasil [02]</i>
                                            </button>
                                        </a></center>';
                        }
                    }elseif ($data->bidang == 7) {
                        if ($data->status_data2 == 1 && $data->status_datarpr2 == 1) {
                            return "".'<center>
                                    <a href="../../..'.$data->link.'/edit'.'/'.$data->id.'?y=2" target="_blank">
                                        <button class="btn btn-danger btn-xs">
                                            <i class="icon-pencil "> Edit Hasil TP</i>
                                        </button>
                                    </a><p></p>
                                    <a href="../../../hasil-pemeriksaan/rpr-syphilis/edit'.'/'.$data->id.'?y=2" target="_blank">
                                        <button class="btn btn-danger btn-xs">
                                            <i class="icon-pencil "> Edit Hasil RPR</i>
                                        </button>
                                    </a></center>';
                        }elseif($data->status_data2 == 2 && $data->status_datarpr2 == 1) {
                            return "".'<center>
                                    <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=2&x=a">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="icon-book "> Kirim Hasil TP</i>
                                        </button>
                                    </a><p></p>
                                    <a href="../../../hasil-pemeriksaan/rpr-syphilis/edit'.'/'.$data->id.'?y=2" target="_blank">
                                        <button class="btn btn-danger btn-xs">
                                            <i class="icon-pencil "> Edit Hasil RPR</i>
                                        </button>
                                    </a></center>';
                        }elseif($data->status_data2 == 1 && $data->status_datarpr2 == 2) {
                            return "".'<center>
                                    <a href="../../..'.$data->link.'/edit'.'/'.$data->id.'?y=2" target="_blank">
                                        <button class="btn btn-danger btn-xs">
                                            <i class="icon-pencil "> Edit Hasil TP</i>
                                        </button>
                                    </a><p></p>
                                    <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=2&x=b">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="icon-book "> Kirim Hasil RPR</i>
                                        </button>
                                    </a></center>';
                        }elseif($data->status_data2 == 2 && $data->status_datarpr2 == 2) {
                            return "".'<center>
                                    <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=2&x=a">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="icon-book "> Kirim Hasil TP</i>
                                        </button>
                                    </a><p></p>
                                    <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=2&x=b">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="icon-book "> Kirim Hasil RPR</i>
                                        </button>
                                    </a></center>';
                        }elseif($data->status_data2 == 1) {
                            return "".'<center>
                                    <a href="../../..'.$data->link.'/edit'.'/'.$data->id.'?y=2" target="_blank">
                                        <button class="btn btn-danger btn-xs">
                                            <i class="icon-pencil "> Edit Hasil TP</i>
                                        </button>
                                    </a>';
                        }elseif($data->status_datarpr2 == 1) {
                            return "".'<center>
                                    <a href="../../../hasil-pemeriksaan/rpr-syphilis/edit'.'/'.$data->id.'?y=2" target="_blank">
                                        <button class="btn btn-danger btn-xs">
                                            <i class="icon-pencil "> Edit Hasil RPR</i>
                                        </button>
                                    </a>';
                        }elseif($data->status_data2 == 2) {
                            return "".'<center>
                                    <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=2&x=a">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="icon-book "> Kirim Hasil TP</i>
                                        </button>
                                    </a>';
                        }elseif($data->status_datarpr2 == 2) {
                            return "".'<center>
                                    <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=2&x=b">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="icon-book "> Kirim Hasil RPR</i>
                                        </button>
                                    </a>';
                        }
                    }else{
                        if ($data->status_data2 == 1) {
                            return "".'<center>
                                        <a href="../../..'.$data->link.'/edit'.'/'.$data->id.'?y=2&x=a" target="_blank" >
                                            <button class="btn btn-danger btn-xs">
                                                <i class="icon-book "> Edit Hasil </i>
                                            </button>
                                        </a></center>';
                        }elseif ($data->status_data2 == 2) {
                            return "".'<center>
                                        <a href="../../data-perusahaan/edit-data'.'/'.$data->id.'?y=2&x=a" >
                                            <button class="btn btn-primary btn-xs">
                                                <i class="icon-book "> Kirim Hasil </i>
                                            </button>
                                        </a></center>';
                        }
                    }
            })
            ->addColumn('bahan1', function($data){
                $bahan1 = DB::table('tb_tandaterima')
                            ->where('id_registrasi', $data->id)
                            ->where('siklus','=','1')
                            ->get();
                if (count($bahan1)) {
                    return "".'<center>
                                <a href="../../data-perusahaan/tanda-terima/hapus'.'/'.$data->id.'?siklus=1">
                                    <button class="btn btn-danger btn-xs">
                                        <i class="icon-trash"> Siklus 1</i>
                                    </button>
                                </a><p></center>';
                }
            })
            ->addColumn('bahan2', function($data){
                $bahan2 = DB::table('tb_tandaterima')
                            ->where('id_registrasi', $data->id)
                            ->where('siklus','=','2')
                            ->get();
                if (count($bahan2)) {
                    return "".'<center>
                                <a href="../../data-perusahaan/tanda-terima/hapus'.'/'.$data->id.'?siklus=2">
                                    <button class="btn btn-danger btn-xs">
                                        <i class="icon-trash"> Siklus 2</i>
                                    </button>
                                </a><p></center>';
                }
            })
            ->rawColumns(['action' => 'action', 'action1' =>  'action1', 'bahan1' =>  'bahan1', 'bahan2' =>  'bahan2'])
            ->make(true);
            
        }
        return view('back/perusahaan/edit',compact('id'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(\Illuminate\Http\Request $request, $id)
    {
        $data['nama_lab'] = $request->nama_lab;
        Perusahaan::where('created_by',$id)->update($data);
        Session::flash('message', 'Nama Instansi Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return back();
    }    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hapus($id)
    {
        $perusahaan = perusahaan::where('created_by','=',$id);
        if ($perusahaan->delete()) {
            $registrasi = Registrasi::where('created_by','=',$id);
            $registrasi->delete();
            Session::flash('message', 'Data Perusahaan Dan Registrasi Telah Dihapus!'); 
            Session::flash('alert-class', 'alert-warning');       
            return back();
        }
        return back();
    }

    public function destroy($id)
    {
        Session::flash('message', 'Data Registrasi Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Registrasi::find($id)->delete();
        return back();
    }

    public function updatedata(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $type = $request->get('x');
        $register = DB::table('tb_registrasi')->where('id', $id)->first();
        if($register->bidang <= 3){
            if ($type == 'a' && $siklus == 1) {
                Registrasi::where('id',$id)->update(['status_data1'=>'1']);
            }elseif ($type == 'b' && $siklus == 1) {
                Registrasi::where('id',$id)->update(['status_data2'=>'1']);
            }elseif ($type == 'a' && $siklus == 2) {
                Registrasi::where('id',$id)->update(['status_datarpr1'=>'1']);
            }elseif ($type == 'b' && $siklus == 2) {
                Registrasi::where('id',$id)->update(['status_datarpr2'=>'1']);
            }
        }elseif($register->bidang == 7){
            if ($type == 'a' && $siklus == 1) {
                Registrasi::where('id',$id)->update(['status_data1'=>'1']);
            }elseif ($type == 'b' && $siklus == 1) {
                Registrasi::where('id',$id)->update(['status_datarpr1'=>'1']);
            }elseif ($type == 'a' && $siklus == 2) {
                Registrasi::where('id',$id)->update(['status_data2'=>'1']);
            }elseif ($type == 'b' && $siklus == 2) {
                Registrasi::where('id',$id)->update(['status_datarpr2'=>'1']);
            }
        }else{
            if ($type == 'a' && $siklus == 1) {
                Registrasi::where('id',$id)->update(['status_data1'=>'1']);
            }elseif ($type == 'a' && $siklus == 2) {
                Registrasi::where('id',$id)->update(['status_data2'=>'1']);
            }
        }
        return back();
    }


    public function hapustanda(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        Session::flash('message', 'Data Registrasi Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        $data = TandaTerima::where('id_registrasi', $id)->where('siklus', $siklus)->delete();
        // dd($data);
        return back();
    }

    public function tambahbidang($id)
    {
        $parameter = DB::table('sub_bidang')
                        ->join('tb_bidang','tb_bidang.id','=','sub_bidang.id_bidang')
                        ->select('sub_bidang.*','tb_bidang.bidang')
                        ->get();

        return view('back/perusahaan/insert',compact('parameter'));
    }
    public function prosestb(\Illuminate\Http\Request $request, $id)
    {
        $date = date('y');
        $input = $request->all();
        $data = DB::table('tb_registrasi')
                    ->join('users','users.id','=','tb_registrasi.created_by')
                    ->where('users.id', $id)
                    ->orderBy('tb_registrasi.id', 'desc')
                    ->first();
        $bidang = DB::table('sub_bidang')->where('id', $input['bidang'])->first();

        $a = $data->id_member;
        $kode_pes = sprintf("%04s", $a);

        $save = new Registrasi;
        $save->no_urut = $data->no_urut;
        $save->sptjm = $data->sptjm;
        $save->pks = $data->pks;
        $save->perusahaan_id = $data->perusahaan_id;
        $save->bidang = $input['bidang'];
        $save->status = $data->status;
        $save->file = $data->file;
        $save->siklus = $data->siklus;
        $save->created_by = $data->created_by;
        $save->kode_lebpes = $kode_pes.'/'.$bidang->alias.'/'.$data->siklus.'/'.$date;
        $save->id_pembayaran = $data->id_pembayaran;
        
        $save->save();
        // dd($save); 
        Session::flash('message', 'Bidang berhasil ditambah!'); 
        Session::flash('alert-class', 'alert-success'); 

        return redirect('admin/data-perusahaan/edit/'.$id);
    }
}
<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;
use Carbon\Carbon;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

use App\TanggalKirimHasil;
use App\bta as Bta;
use App\EvaluasiBta;
use App\daftar as Daftar;
use App\register as Register;
use App\EvaluasiBTAStatus;
use App\LogInput;

use Redirect;
use Validator;
use Session;

class BtaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['cetak','view']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;
        $pendidikan = DB::table('tb_pendidikan')->get();

        $siklus = $request->get('y');
        $data = DB::table('parameter')->where('kategori', 'hematologi')->get();

        $register = Register::find($id);
        $perusahaan = $register->kode_lebpes;
        
        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->get();
        
        if (count($validasi)) {
            return view('hasil_pemeriksaan/bta', compact('data', 'perusahaan','siklus','date','pendidikan'));
        }else{
            Session::flash('message', 'Harap Input Tanda Terima Bahan BTA Terlebih Dahulu.');
            Session::flash('alert-class', 'alert-danger');
            return redirect('tanda-terima');
        }
        // if ($siklus == 2) {
        //     if ($q1->siklus == 12) {
        //         if ($q1->pemeriksaan == 'done') {
        //             if (count($validasi)) {
        //                 return view('hasil_pemeriksaan/bta', compact('data', 'perusahaan','siklus','date','pendidikan'));
        //             }else{
        //                 Session::flash('message', 'Harap Input Tanda Terima Bahan BTA Terlebih Dahulu.');
        //                 Session::flash('alert-class', 'alert-danger');
        //                 return redirect('tanda-terima');
        //             }
        //         }else{
        //             Session::flash('message', 'Harap Input Form Siklus 1.');
        //             Session::flash('alert-class', 'alert-danger');
        //             return back();
        //         }
        //     }else{
        //         if (count($validasi)) {
        //             return view('hasil_pemeriksaan/bta', compact('data', 'perusahaan','siklus','date','pendidikan'));
        //         }else{
        //             Session::flash('message', 'Harap Input Tanda Terima Bahan BTA Terlebih Dahulu.');
        //             Session::flash('alert-class', 'alert-danger');
        //             return redirect('tanda-terima');
        //         }
        //     }
        // }else{
        //     if (count($validasi)) {
        //         return view('hasil_pemeriksaan/bta', compact('data', 'perusahaan','siklus','date','pendidikan'));
        //     }else{
        //         Session::flash('message', 'Harap Input Tanda Terima Bahan BTA Terlebih Dahulu.');
        //         Session::flash('alert-class', 'alert-danger');
        //         return redirect('tanda-terima');
        //     }
        // }
    }

    public function view(\Illuminate\Http\Request $request, $id){
        // $data = DB::table('parameter')
        //     ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
        //     ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
        //     ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
        //     ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil')
        //     ->where('parameter.kategori', 'hematologi')
        //     ->where('hp_headers.id_registrasi', $id)
        //     ->get();
        // $datas = HpHeader::where('id_registrasi', $id)
        //     ->first();
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;

        $data = DB::table('tb_bta')
                ->leftjoin('tb_pendidikan','tb_pendidikan.id','=','tb_bta.pendidikan_petugas')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                ->select('tb_bta.*','tb_pendidikan.tingkat')
                ->first();
        $datas = DB::table('tb_bta')
                ->where('siklus', $siklus)
                ->where('id_registrasi', $id)
                ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                ->get();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $pendidikan = DB::table('tb_pendidikan')->get();
        
        // return view('cetak_hasil/telur-cacing', compact('data', 'perusahaan', 'datas', 'type'));
        if (count($data) > 0) {
         $pdf = PDF::loadview('cetak_hasil/bta', compact('data', 'perusahaan','datas', 'siklus', 'date','pendidikan'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        }else{
            Session::flash('message', 'BTA Belum dievaluasi!'); 
            Session::flash('alert-class', 'alert-danger');
            return back();
        }

        $kode_pes = sprintf("%04s", Auth::user()->id_member);
        if ($siklus == 1) {
            $sikprint = 'I';
        }else{
            $sikprint = 'II';
        }
        return $pdf->stream($kode_pes.' BTA S'.$sikprint.' '.$date.'.pdf');
    }

    public function insert(\Illuminate\Http\Request $request, $id)
    { 
        $input = $request->all();
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $years = $q2->year;

        // dd($years);
        $validasi = Bta::where(DB::raw('YEAR(tb_bta.created_at)'), '=' , $years)->where('id_registrasi','=',$id)->where('siklus','=',$siklus)->get();

        $i = 0;
          if (count($validasi)>0) {
            Session::flash('message', 'Hasil BTA Sudah Ada!'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('hasil-pemeriksaan');
        }else{
            foreach ($input['kode'] as $kode) {
                if($kode != ''){
                  
                    $Savedata = new Bta;
                    $Savedata->kode = $input['kode'][$i];
                    $Savedata->kondisi = $input['kondisi'];
                    $Savedata->hasil = htmlentities($input['hasil'][$i]);
                    $Savedata->kode_peserta = $input['kode_peserta'];
                    $Savedata->nomor_hp = $input['nomor_hp'];
                    $Savedata->nama_pemeriksa = $input['nama_pemeriksa'];
                    $Savedata->penanggung_jawab = $input['penanggung_jawab'];
                    $Savedata->catatan = $input['catatan'];
                    $Savedata->tgl_penerimaan = $input['tgl_penerimaan'];
                    $Savedata->tgl_pemeriksaan = $input['tgl_pemeriksaan'];
                    $Savedata->pendidikan_petugas = $input['pendidikan'];
                    $Savedata->pendidikan_lain = $input['pendidikan_lain'];
                    $Savedata->created_by = Auth::user()->id;
                    $Savedata->id_registrasi = $id;
                    $Savedata->siklus = $siklus;
                    $Savedata->save();
                    }
                $i++;
            }
            // dd($input);

            $log = new LogInput;
            $log->id_registrasi = $id;
            $log->status = 'Input Hasil';
            $log->created_by = Auth::user()->id;
            $log->siklus = $siklus;
            $log->type = '';
            $log->save();

            if ($siklus == '1') {
                Register::where('id',$id)->update(['siklus_1'=>'done', 'rpr1'=>'done', 'status_data1'=>'1']);
                Register::where('id',$id)->update(['pemeriksaan'=>'done']);
            }else{
                Register::where('id',$id)->update(['siklus_2'=>'done', 'rpr2'=>'done', 'status_data2'=>'1']);
                Register::where('id',$id)->update(['pemeriksaan2'=>'done']);
            }
            return redirect('hasil-pemeriksaan');
        }
    }

    public function edit(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;
        $pendidikan = DB::table('tb_pendidikan')->get();

        $data = DB::table('tb_bta')
                ->leftjoin('tb_pendidikan','tb_pendidikan.id','=','tb_bta.pendidikan_petugas')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                ->select('tb_bta.*','tb_pendidikan.tingkat')
                ->first();
        $datas = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->get();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        
        return view('edit_hasil/bta', compact('data', 'perusahaan', 'datas', 'siklus','date','pendidikan'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        $register = Register::find($id);

        // dd($input);
        $i = 0;
        foreach ($input['kode'] as $kode) {
            if($kode != ''){
                $Savedata['siklus'] = $siklus;
                $Savedata['tgl_pemeriksaan'] = $request->tgl_pemeriksaan;
                $Savedata['tgl_penerimaan'] = $request->tgl_penerimaan;
                $Savedata['catatan'] = $request->catatan;
                $Savedata['penanggung_jawab'] = $request->penanggung_jawab;
                $Savedata['nama_pemeriksa'] = $request->nama_pemeriksa;
                $Savedata['nomor_hp'] = $request->nomor_hp;
                $Savedata['hasil'] = $request->hasil[$i];
                $Savedata['kode'] = $request->kode[$i];
                $Savedata['kondisi'] = $request->kondisi;
                $Savedata['pendidikan_petugas'] = $request->pendidikan;
                $Savedata['pendidikan_lain'] = $request->pendidikan_lain;
                Bta::where('id', $request->idbta[$i])->update($Savedata);
            }
            $i++;
        }
        if ($request->simpan == "Kirim") {
            TanggalKirimHasil::where('id_registrasi', $id)
                            ->where('siklus', $siklus)
                            ->delete();

            $KirimHasil = new TanggalKirimHasil;
            $KirimHasil->id_registrasi = $id;
            $KirimHasil->siklus = $siklus;
            $KirimHasil->type = "";
            $KirimHasil->tanggal_kirim = date('Y-m-d');
            $KirimHasil->save();

            $log = new LogInput;
            $log->id_registrasi = $id;
            $log->status = 'Kirim Hasil';
            $log->created_by = Auth::user()->id;
            $log->siklus = $siklus;
            $log->type = '';
            $log->save();

            if ($siklus == '1') {
                Register::where('id',$id)->update(['status_data1'=>'2']);
            }else{
                Register::where('id',$id)->update(['status_data2'=>'2']);
            }
        }else{
            $log = new LogInput;
            $log->id_registrasi = $id;
            $log->status = 'Kirim Hasil';
            $log->created_by = Auth::user()->id;
            $log->siklus = $siklus;
            $log->type = '';
            $log->save();
        }
        return redirect('edit-hasil');
    }

    public function evaluasi(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $data = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->first();
        $datas = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->get();
        $rujuk = DB::table('tb_rujukan_bta')
                ->where('siklus', $siklus)
                ->where('tahun', '=' , $tahun)
                ->get();
        $register = Register::find($id);
        $evaluasi = DB::table('tb_evaluasi_bta')->where('siklus', $siklus)->where('tahun', $tahun)->where('id_registrasi',$id)->get();
        $perusahaan = $register->perusahaan->nama_lab;
        $status = DB::table('tb_evaluasi_bta_status')
                ->where('siklus', $siklus)
                ->where('tahun', $tahun)
                ->where('id_registrasi', $id)
                ->first();
        // dd($evaluasi);
        if ($rujuk != NULL) {
            return view('evaluasi.bta.penilaian.evaluasi.index', compact('data','siklus','perusahaan', 'datas', 'type','rujuk','evaluasi', 'status','id'));
        }else{
            return back();
        }
    }

    public function cetak(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        $tanggal = date('d-m-Y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;

        $data = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->first();
        $datas = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->get();
        $rujuk = DB::table('tb_rujukan_bta')
                ->where('siklus', $siklus)
                ->where('tahun', '=' , $tahun)
                ->get();
        $register = Register::find($id);
        $evaluasi = DB::table('tb_evaluasi_bta')->where('tahun', $tahun)->where('siklus', $siklus)->where('id_registrasi',$id)->get();
        // dd($evaluasi);
        $perusahaan = $register->perusahaan->nama_lab;
        $alamat = $register->perusahaan->alamat;

        $status = DB::table('tb_evaluasi_bta_status')
                ->where('siklus', $siklus)
                ->where('tahun', $tahun)
                ->where('id_registrasi', $id)
                ->first();
        $skor = DB::table('tb_evaluasi_bta')
                ->where('id_registrasi',$id)
                ->groupBy('id_registrasi')
                ->where('siklus', $siklus)
                ->where('tahun', $tahun)
                ->sum('nilai');
        $ttd = DB::table('tb_ttd_evaluasi')
            ->where('tahun', $tahun)
            ->where('siklus', $siklus)
            ->where('bidang', '=', '4')
            ->first();
        // dd($skor);
        if ($rujuk != NULL) {
            if (count($evaluasi)>0) {
            $pdf = PDF::loadView('evaluasi.bta.penilaian.evaluasi.cetak',compact('data','register', 'alamat','perusahaan', 'datas', 'type','rujuk','evaluasi', 'status','siklus','tahun','tanggal','skor','ttd'));
            $pdf->setPaper('a4','potrait');
            return $pdf->stream('EvaluasiBta'.rand(2,32012).'.pdf');
            }else{
                 Session::flash('message', 'BTA belum dievaluasi!');
                Session::flash('alert-class', 'alert-danger');
                return back();
            }

        }else{
            return back();
        }
    }

    public function insertevaluasi(\Illuminate\Http\Request $request, $id)
    { 
        $input = $request->all();
        $siklus = $request->get('y');
        
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $tahun = $q2->year;
        // dd($input);
        $i = 0;
        if ($input['simpan'] == 'Simpan') {
            foreach ($input['kode_sediaan'] as $kode) {
                if($kode != ''){
                    $Savedata = new EvaluasiBta;
                    $Savedata->id_registrasi = $id;
                    $Savedata->kode_sediaan = $input['kode_sediaan'][$i];
                    $Savedata->keterangan = $input['nilai'][$i];
                    if ($input['nilai'][$i] == 'Benar') {
                        $Savedata->nilai = '10';
                    }else if($input['nilai'][$i] == 'KH'){
                        $Savedata->nilai = '5';
                    }else if($input['nilai'][$i] == 'PPR'){
                        $Savedata->nilai = '5';
                    }else if($input['nilai'][$i] == 'NPR'){
                        $Savedata->nilai = '5';
                    }else if($input['nilai'][$i] == 'PPT'){
                        $Savedata->nilai = '0';
                    }else if($input['nilai'][$i] == 'NPT'){
                        $Savedata->nilai = '0';
                    }
                    $Savedata->siklus = $siklus;
                    $Savedata->tahun = $tahun;
                    $Savedata->save();
                }
                $i++;
            }
            $EvaluasiStatus = new EvaluasiBTAStatus;
            $EvaluasiStatus->id_registrasi = $id;
            $EvaluasiStatus->status = $input['status'];
            $EvaluasiStatus->siklus = $siklus;
            $EvaluasiStatus->tahun = $tahun;
            $EvaluasiStatus->save();
        }else{
            foreach ($input['kode_sediaan'] as $kode) {
                if($kode != ''){
                    $Savedata['siklus'] = $siklus;
                    $Savedata['kode_sediaan'] = $request->kode_sediaan[$i];
                    $Savedata['keterangan'] = $request->nilai[$i];
                    if ($input['nilai'][$i] == 'Benar') {
                        $Savedata['nilai'] = '10';
                    }else if($input['nilai'][$i] == 'KH'){
                        $Savedata['nilai'] = '5';
                    }else if($input['nilai'][$i] == 'PPR'){
                        $Savedata['nilai'] = '5';
                    }else if($input['nilai'][$i] == 'NPR'){
                        $Savedata['nilai'] = '5';
                    }else if($input['nilai'][$i] == 'PPT'){
                        $Savedata['nilai'] = '0';
                    }else if($input['nilai'][$i] == 'NPT'){
                        $Savedata['nilai'] = '0';
                    }
                    EvaluasiBta::where('id', $request->id[$i])->update($Savedata);
                }
                $i++;
            }   
            $EvaluasiStatus['status'] = $request->status;
            EvaluasiBTAStatus::where('id_registrasi', $id)->where('siklus', $siklus)->where('tahun', $tahun)->update($EvaluasiStatus);
        }
        // dd($Savedata);
        return back();
    }
}
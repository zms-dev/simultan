<?php

namespace App\Http\Controllers;
use DB;
use App\User;
use App\daftar as Daftar;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\register as Register;
use Excel;
use PDF;
class EvaluasiMikroController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function malariagrafik()
    {
        return view('evaluasi.malaria.grafik.rekap.index');
    }
    public function malariagrafikna(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $siklus = $request->siklus;

        $badan = DB::table('tb_registrasi')
                ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->leftjoin('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                ->where('tb_registrasi.bidang','=', '10')
                ->where(function ($query) use ($siklus) {
                    $query->where('tb_registrasi.siklus','=','12')
                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                })
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->select('badan_usaha.badan_usaha', DB::raw('count(badan_usaha.badan_usaha) as jumlah'))
                ->groupBy('badan_usaha.id')
                ->get();
        // dd($badan);
        return view('evaluasi.malaria.grafik.rekap.grafik', compact('badan', 'tahun', 'siklus'));
    }

    public function malariagrafikkesimpulan()
    {
        return view('evaluasi.malaria.grafik.kesimpulan.index');
    }
    public function malariagrafikkesimpulanna(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $siklus = $request->siklus;

        $data = DB::table('tb_evaluasi_malaria')
                ->select('kesimpulan', DB::raw('COUNT(kesimpulan) as jumlah'))
                ->where('siklus', $siklus)
                ->where('tahun', $tahun)
                ->groupBy('kesimpulan')
                ->get();
        // dd($badan);
        return view('evaluasi.malaria.grafik.kesimpulan.grafik', compact('data', 'tahun', 'siklus'));
    }

    public function malariapeserta()
    {
        return view('evaluasi.malaria.rekaphasilpeserta.index');
    }
    public function malariapesertana(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        if($input['siklus'] == '1')
        {
            $data = DB::table('tb_registrasi')
                ->where('bidang','=','10')
                ->where('tb_registrasi.status', 3)
                ->where('status_data1', 2)
                ->where('pemeriksaan', 'done')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->get();
        }else{
            $data = DB::table('tb_registrasi')
                ->where('bidang','=','10')
                ->where('tb_registrasi.status', 3)
                ->where('status_data2', 2)
                ->where('pemeriksaan2', 'done')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->get();
        }
        foreach ($data as $key => $val) {
            $bta = DB::table('tb_malaria')
                    ->where('tb_malaria.id_registrasi', $val->id)
                    ->where('tb_malaria.siklus', $input['siklus'])
                    ->where(DB::raw('YEAR(tb_malaria.created_at)'), '=' , $input['tahun'])
                    ->get();
            $val->hasil = $bta;

            $total = DB::table('tb_evaluasi_malaria')
                        ->where('id_registrasi', $val->id)
                        ->where('siklus', $input['siklus'])
                        ->where('tahun', $input['tahun'])
                        ->groupBy('id_registrasi')
                        ->sum('total');
            $val->total = $total;
        }
        // dd($data);
        Excel::create('Rekap Hasil Seluruh Peserta Malaria', function($excel) use ($data, $input) {                    
            $excel->sheet('Data', function($sheet) use ($data, $input) {
                $sheet->loadView('evaluasi.malaria.rekaphasilpeserta.view', array('data'=>$data,'input'=>$input) );
            });
        })->download('xls');
        // return view('evaluasi.bta.rekaphasilpeserta.view', compact('data', 'input'));
    }

    public function btagrafik()
    {
        return view('evaluasi.bta.grafik.rekap.index');
    }
    public function btagrafikna(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $siklus = $request->siklus;

        $badan = DB::table('tb_registrasi')
                ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->leftjoin('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                ->where('tb_registrasi.bidang','=', '4')
                ->where(function ($query) use ($siklus) {
                    $query->where('tb_registrasi.siklus','=','12')
                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                })
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->select('badan_usaha.badan_usaha', DB::raw('count(badan_usaha.badan_usaha) as jumlah'))
                ->groupBy('badan_usaha.id')
                ->get();
        // dd($badan);
        return view('evaluasi.bta.grafik.rekap.grafik', compact('badan', 'tahun', 'siklus'));
    }
    
    public function btainputpeserta()
    {
        return view('evaluasi.bta.rekapinputpeserta.index');
    }

    public function btainputpesertana(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        if($input['siklus'] == '1')
        {
            $data = DB::table('tb_registrasi')
                ->where('bidang','=','4')
                ->where('tb_registrasi.status', 3)
                ->where('status_data1', 2)
                ->where('pemeriksaan', 'done')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->get();
        }else{
            $data = DB::table('tb_registrasi')
                ->where('bidang','=','4')
                ->where('tb_registrasi.status', 3)
                ->where('status_data2', 2)
                ->where('pemeriksaan2', 'done')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->get();
        }
        foreach ($data as $key => $val) {
            $bta = DB::table('tb_bta')
                    ->where('tb_bta.id_registrasi', $val->id)
                    ->where('tb_bta.siklus', $input['siklus'])
                    ->where(DB::raw('YEAR(tb_bta.created_at)'), '=' , $input['tahun'])
                    ->get();
            $val->hasil = $bta;
        }
        // dd($data[0]);
        Excel::create('Rekap Hasil Seluruh Peserta BTA', function($excel) use ($data, $input) {                    
            $excel->sheet('BTA', function($sheet) use ($data, $input) {
                $sheet->loadView('evaluasi.bta.rekapinputpeserta.view', array('data'=>$data,'input'=>$input) );
            });
        })->download('xls');
        // return view('evaluasi.bta.rekaphasilpeserta.view', compact('data', 'input'));
    }

    public function btapeserta()
    {
        return view('evaluasi.bta.rekaphasilpeserta.index');
    }

    public function btapesertana(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        if($input['siklus'] == '1')
        {
            $data = DB::table('tb_registrasi')
                ->where('bidang','=','4')
                ->where('tb_registrasi.status', 3)
                ->where('status_data1', 2)
                ->where('pemeriksaan', 'done')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->get();
        }else{
            $data = DB::table('tb_registrasi')
                ->where('bidang','=','4')
                ->where('tb_registrasi.status', 3)
                ->where('status_data2', 2)
                ->where('pemeriksaan2', 'done')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->get();
        }
        foreach ($data as $key => $val) {
            $bta = DB::table('tb_bta')
                    ->join('tb_evaluasi_bta', 'tb_evaluasi_bta.kode_sediaan', '=', 'tb_bta.kode')
                    ->where('tb_bta.id_registrasi', $val->id)
                    ->where('tb_bta.siklus', $input['siklus'])
                    ->where('tb_evaluasi_bta.siklus', $input['siklus'])
                    ->where('tb_evaluasi_bta.tahun', $input['tahun'])
                    ->where(DB::raw('YEAR(tb_bta.created_at)'), '=' , $input['tahun'])
                    ->get();
            $val->hasil = $bta;

            $status = DB::table('tb_evaluasi_bta_status')
                        ->where('id_registrasi', $val->id)
                        ->where('siklus', $input['siklus'])
                        ->where('tahun', $input['tahun'])
                        ->get();
            $val->status = $status;

            $total = DB::table('tb_evaluasi_bta')
                        ->where('id_registrasi', $val->id)
                        ->where('siklus', $input['siklus'])
                        ->where('tahun', $input['tahun'])
                        ->groupBy('id_registrasi')
                        ->sum('nilai');
            $val->total = $total;
        }
        // dd($data);
        Excel::create('Rekap Hasil Seluruh Peserta BTA', function($excel) use ($data, $input) {                    
            $excel->sheet('BTA', function($sheet) use ($data, $input) {
                $sheet->loadView('evaluasi.bta.rekaphasilpeserta.view', array('data'=>$data,'input'=>$input) );
            });
        })->download('xls');
        // return view('evaluasi.bta.rekaphasilpeserta.view', compact('data', 'input'));
    }

    public function grafikkesimpulanpeserta()
    {
        return view('evaluasi.bta.grafik.kesimpulan.index');
    }
    public function grafikkesimpulanpesertana(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $sql = "SELECT
                    SUM(nilai) as batas
                FROM
                    `tb_evaluasi_bta`
                where siklus = ".$input['siklus']."
                AND tahun = ".$input['tahun']."
                GROUP BY
                    id_registrasi";
        $sqlk = "SELECT
                    tb_evaluasi_bta.kode_sediaan,
                    SUM(nilai) as total,
                    tb_evaluasi_bta_status.status
                FROM
                    `tb_evaluasi_bta`
                INNER JOIN  `tb_evaluasi_bta_status` ON `tb_evaluasi_bta_status`.`id_registrasi` = `tb_evaluasi_bta`.`id_registrasi`
                WHERE tb_evaluasi_bta.siklus = ".$input['siklus']."
                AND tb_evaluasi_bta_status.siklus = ".$input['siklus']."
                AND tb_evaluasi_bta.tahun = ".$input['tahun']."
                AND tb_evaluasi_bta_status.tahun = ".$input['tahun']."
                GROUP BY
                    tb_evaluasi_bta.id_registrasi";

        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->groupBy('batas')
                ->orderBy('batas', 'desc')
                ->get();
        foreach ($data as $key => $val) {
            $lulus = DB::table(DB::raw("($sqlk) zone"))
                        ->where('total', $val->batas)
                        ->where('status','=','Lulus')
                        ->groupBy('total')
                        ->count();
            $tidaklulus = DB::table(DB::raw("($sqlk) zone"))
                        ->where('total', $val->batas)
                        ->where('status','=','Tidak Lulus')
                        ->groupBy('total')
                        ->count();
            $val->lulus = $lulus;
            $val->tidaklulus = $tidaklulus;
        }
        // dd($data);
        return view('evaluasi.bta.grafik.kesimpulan.grafik', compact('data', 'input', 'tahun'));
    }

    public function grafikjawabanpeserta()
    {
        return view('evaluasi.bta.grafik.jawabanpeserta.index');
    }
    public function grafikjawabanpesertana(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $tahun = date('Y');
        $kode = (int)$input['kode_sediaan'];
        $data = DB::table('tb_bta')
                ->join('tb_evaluasi_bta', 'tb_evaluasi_bta.kode_sediaan', '=', 'tb_bta.kode')
                ->where(DB::raw('YEAR(tb_bta.created_at)'), '=' , $input['tahun'])
                ->where('tb_bta.siklus', $input['siklus'])
                ->where('tb_evaluasi_bta.siklus', $input['siklus'])
                ->where('tb_evaluasi_bta.tahun', $input['tahun'])
                ->where(DB::raw('substr(kode, 10)'), '=' , $kode)
                ->select('tb_bta.hasil', 'tb_evaluasi_bta.keterangan' , DB::raw('SUBSTR(kode, 10) AS kode_sediaan, COUNT(substring(kode, 10)) as jumlah'))
                ->groupBy('tb_bta.hasil', 'tb_evaluasi_bta.keterangan')
                ->get();
        // var_dump($data);
        // dd($data);
        return view('evaluasi.bta.grafik.jawabanpeserta.grafik', compact('data', 'input'));
    }

    public function tcgrafik()
    {
        return view('evaluasi.tc.grafik.rekap.index');
    }
    public function tcgrafikna(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $siklus = $request->siklus;

        $badan = DB::table('tb_registrasi')
                ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->leftjoin('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                ->where('tb_registrasi.bidang','=', '5')
                ->where(function ($query) use ($siklus) {
                    $query->where('tb_registrasi.siklus','=','12')
                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                })
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->select('badan_usaha.badan_usaha', DB::raw('count(badan_usaha.badan_usaha) as jumlah'))
                ->groupBy('badan_usaha.id')
                ->get();
        // dd($badan);
        return view('evaluasi.tc.grafik.rekap.grafik', compact('badan', 'tahun', 'siklus'));
    }

    public function perusahaanmalaria(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.id as id_registrasi,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)->where('tb_registrasi.bidang','=','10')->where('tb_registrasi.status','>=','2')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_evaluasi_malaria')
                                ->where('id_registrasi', $data->id_registrasi)
                                ->where('siklus', $siklus)
                                ->first();
                    if (count($evaluasi)) {
                        return "".'<a href="penilaian/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            Evaluasi
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="penilaian/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            Evaluasi
                                        </button>
                                    </a>';
                    }
                })
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_evaluasi_malaria')
                                ->where('id_registrasi', $data->id_registrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->id_registrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_data2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia'])
            ->make(true);
        }
        return view('evaluasi.malaria.penilaian.perusahaan');
    }

    public function dataperusahaanmalaria(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '10')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
        
        return view('evaluasi.malaria.penilaian.dataperusahaan', compact('data','siklus'));
    }

    public function perusahaanbta(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.id as idregistrasi,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)->where('tb_registrasi.bidang','=','4')->where('tb_registrasi.status','>=','2')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_evaluasi_bta_status')
                                ->where('siklus', $siklus)
                                ->where('id_registrasi', $data->idregistrasi)
                                ->orderBy('id', 'desc')
                                ->first();
                    return "".'<a href="penilaian/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Proses
                                    </button>
                                </a>';
            ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = DB::table('tb_evaluasi_bta_status')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_data2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisi', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_evaluasi_bta_status')
                                ->where('siklus', $siklus)
                                ->where('tahun', $tahun)
                                ->where('id_registrasi', $data->idregistrasi)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($evaluasi)){
                        return "1";
                    }else{
                        return "0";
                    }
            ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisi' =>  'posisi'])
            ->make(true);
        }
        return view('evaluasi.bta.penilaian.perusahaan');
    }

    public function perusahaanbtasertifikat(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.id as idregistrasi,tb_registrasi.kode_lebpes as kode_lebpes'))
                ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.status','>=','2')
                ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_evaluasi_bta_status')
                                ->where('siklus', $siklus)
                                ->where('id_registrasi', $data->idregistrasi)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($evaluasi)){
                        return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            Proses
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            Proses
                                        </button>
                                    </a>';
                    }
            ;})
            ->addColumn('posisi', function($data) use ($siklus){
                    $evaluasi = DB::table('tb_evaluasi_bta_status')
                                ->where('siklus', $siklus)
                                ->where('id_registrasi', $data->idregistrasi)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($evaluasi)){
                        return "1";
                    }else{
                        return "0";
                    }
            ;})
            ->make(true);
        }
        $bidang = Auth::user()->penyelenggara;
        return view('evaluasi.bta.penilaian.perusahaansertifikat', compact('bidang'));
    }

    public function dataperusahaanbta(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '4')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.bta.penilaian.dataperusahaan', compact('data','siklus'));
    }

   
    public function dataperusahaanbtasertifikat(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id_bidang', '=', '6')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.status', 3)
                ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
                // dd($data);
        return view('evaluasi.bta.penilaian.dataperusahaansertifikat', compact('data','siklus','tahun'));
    }

    public function perusahaantc(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.id as idregistrasi ,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.bidang','=','5')
                ->where('tb_registrasi.status','>=','2')
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_evaluasi_tc')
                                ->where('siklus', $siklus)
                                ->where('id_registrasi', $data->idregistrasi)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($evaluasi)){
                        return "".'<a href="penilaian/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            Evaluasi
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="penilaian/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            Evaluasi
                                        </button>
                                    </a>';
                    }
            ;})
            ->addColumn('posisi', function($data) use ($siklus){
                    $evaluasi = DB::table('tb_evaluasi_tc')
                                ->where('siklus', $siklus)
                                ->where('id_registrasi', $data->idregistrasi)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($evaluasi)){
                        return "1";
                    }else{
                        return "0";
                    }
            ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_evaluasi_tc')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_data2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisi' =>  'posisi'])
            ->make(true);
        }
        return view('evaluasi.tc.penilaian.perusahaan');
    }

    public function dataperusahaantc(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '5')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.tc.penilaian.dataperusahaan', compact('data', 'siklus'));
    }

    public function tcinputpeserta()
    {
        return view('evaluasi.tc.rekapinputpeserta.index');
    }
    public function tcinputpesertana(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        if($input['siklus'] == '1')
        {
            $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                ->leftjoin('badan_usaha','badan_usaha.id', '=', 'perusahaan.pemerintah')
                ->where('bidang','=','5')
                ->where('tb_registrasi.status', 3)
                ->where('status_data1', 2)
                ->where('pemeriksaan', 'done')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('tb_registrasi.id', 'tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'badan_usaha.badan_usaha')
                ->get();
        }else{
            $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                ->leftjoin('badan_usaha','badan_usaha.id', '=', 'perusahaan.pemerintah')
                ->where('bidang','=','5')
                ->where('tb_registrasi.status', 3)
                ->where('status_data2', 2)
                ->where('pemeriksaan2', 'done')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('tb_registrasi.id', 'tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'badan_usaha.badan_usaha')
                ->get();
        }
        foreach ($data as $key => $val) {
            $tc = DB::table('tb_telurcacing')
                    ->where('tb_telurcacing.id_registrasi', $val->id)
                    ->where('tb_telurcacing.siklus', $input['siklus'])
                    ->where(DB::raw('YEAR(tb_telurcacing.created_at)'), '=' , $input['tahun'])
                    ->get();
            $val->hasil = $tc;
        }
        // dd($data);
        Excel::create('Rekap Input Seluruh Peserta Telur Cacing', function($excel) use ($data, $input) {                    
            $excel->sheet('TC', function($sheet) use ($data, $input) {
                $sheet->loadView('evaluasi.tc.rekapinputpeserta.view', array('data'=>$data,'input'=>$input) );
            });
        })->download('xls');
        // return view('evaluasi.bta.rekaphasilpeserta.view', compact('data', 'input'));
    }
    
    public function tcpeserta()
    {
        return view('evaluasi.tc.rekaphasilpeserta.index');
    }
    public function tcpesertana(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        if($input['siklus'] == '1')
        {
            $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                ->leftjoin('badan_usaha','badan_usaha.id', '=', 'perusahaan.pemerintah')
                ->where('bidang','=','5')
                ->where('tb_registrasi.status', 3)
                ->where('status_data1', 2)
                ->where('pemeriksaan', 'done')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('tb_registrasi.id', 'tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'badan_usaha.badan_usaha')
                ->get();
        }else{
            $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                ->leftjoin('badan_usaha','badan_usaha.id', '=', 'perusahaan.pemerintah')
                ->where('bidang','=','5')
                ->where('tb_registrasi.status', 3)
                ->where('status_data2', 2)
                ->where('pemeriksaan2', 'done')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('tb_registrasi.id', 'tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'badan_usaha.badan_usaha')
                ->get();
        }
        foreach ($data as $key => $val) {
            $tc = DB::table('tb_telurcacing')
                    ->join('tb_evaluasi_tc', 'tb_evaluasi_tc.kode_sediaan', '=', 'tb_telurcacing.kode_botol')
                    ->where('tb_telurcacing.id_registrasi', $val->id)
                    ->where('tb_telurcacing.siklus', $input['siklus'])
                    ->where(DB::raw('YEAR(tb_telurcacing.created_at)'), '=' , $input['tahun'])
                    ->get();
            $val->hasil = $tc;

            $total = DB::table('tb_evaluasi_tc')->where('id_registrasi',$val->id)->where('siklus', $input['siklus'])->where('tahun', $input['tahun'])->select(DB::raw('AVG(nilai) as nilai'))->get();

            $val->total = $total;
        }
        // dd($data);
        Excel::create('Rekap Hasil Seluruh Peserta Telur Cacing', function($excel) use ($data, $input) {                    
            $excel->sheet('TC', function($sheet) use ($data, $input) {
                $sheet->loadView('evaluasi.tc.rekaphasilpeserta.view', array('data'=>$data,'input'=>$input) );
            });
        })->download('xls');
        // return view('evaluasi.bta.rekaphasilpeserta.view', compact('data', 'input'));
    }

    public function laporanevaluasibta()
    {
        return view('evaluasi.bta.laporanhasilevaluasi.index');
    }

    public function laporanevaluasibtana(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $siklus = $request['siklus'];
        $tahun = $input['tahun'];

        $peserta = DB::table('tb_evaluasi_bta')
                    ->select(['id_registrasi' => 'id_registrasi'])
                    ->where('siklus', $siklus)
                    ->where('tahun', '=' , $tahun)
                    ->groupby('id_registrasi')
                    ->get();

        foreach($peserta as $pesertaa) {
            $id = $pesertaa->id_registrasi;

            $data[$id] = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->first();
            $datas[$id] = DB::table('tb_bta')
                ->leftJoin('tb_registrasi', 'tb_registrasi.id', '=', 'tb_bta.id_registrasi')
                ->leftJoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                // ->select('kode_peserta', 'nama_lab')
                ->where('id_registrasi', $id)
                ->where('tb_bta.siklus', $siklus)
                ->where(DB::raw('YEAR(tb_bta.created_at)'), '=' , $tahun)
                ->get();
            $rujuk[$id] = DB::table('tb_rujukan_bta')
                    ->where('siklus', $siklus)
                    ->where('tahun', '=' , $tahun)
                    ->get();
            $nilai[$id] = DB::table('tb_evaluasi_bta')
                    ->where('id_registrasi', $id)
                    ->where('siklus', $siklus)
                    ->where('tahun', '=' , $tahun)
                    ->get();
            $register[$id] = Register::find($id);
            $perusahaan[$id] = Register::find($id)->perusahaan->nama_lab;
            $evaluasi[$id] = DB::table('tb_evaluasi_bta')
                        ->where('siklus', $siklus)
                        ->where('id_registrasi', $id)
                        ->groupby('id_registrasi')
                        ->SUM('nilai');
            $status[$id] = DB::table('tb_evaluasi_bta_status')
                        ->where('siklus', $siklus)
                        ->where('id_registrasi', $id)
                        ->first();
        }
        // dd($status);
        $i = 1;
        if (count($evaluasi) > 0) {
            $pdf = PDF::loadview('evaluasi.bta.laporanhasilevaluasi.pesertabta', compact('peserta', 'data','tanggal','evaluasi','rujuk','nilai','id','siklus','tahun','register','perusahaan','datas', 'i', 'status'))
            ->setPaper('a4', 'potrait')
            ->setOptions(['dpi' => 135, 'defaultFont' => 'sans-serif'])
            ->setwarnings(false);
            return $pdf->stream('Hasil Evaluasi BTA.pdf');
        }else{
            Session::flash('message', 'BTA belum dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
    }

    public function perusahaantcprint(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.id as idregistrasi ,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.bidang','=','5')
                ->where('tb_registrasi.status','>=','2')
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_evaluasi_tc')
                                ->where('siklus', $siklus)
                                ->where('id_registrasi', $data->idregistrasi)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($evaluasi)){
                        return "".'<a href="telur-cacing/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            Print
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="telur-cacing/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            Print
                                        </button>
                                    </a>';
                    }
            ;})
            ->addColumn('posisi', function($data) use ($siklus){
                    $evaluasi = DB::table('tb_evaluasi_tc')
                                ->where('siklus', $siklus)
                                ->where('id_registrasi', $data->idregistrasi)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($evaluasi)){
                        return "1";
                    }else{
                        return "0";
                    }
            ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_evaluasi_tc')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_data2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisi' =>  'posisi'])
            ->make(true);
        }

        return view('evaluasi.tc.rekapinput.perusahaan');
    }

    public function dataperusahaantcprint(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('sub_bidang.id', '=', '5')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.tc.rekapinput.dataperusahaan', compact('data','siklus'));
    }

    public function perusahaanbtaprint(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.id as idregistrasi ,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.bidang','=','4')
                ->where('tb_registrasi.status','>=','2')
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_evaluasi_tc')
                                ->where('siklus', $siklus)
                                ->where('id_registrasi', $data->idregistrasi)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($evaluasi)){
                        return "".'<a href="bta/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            Print
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="bta/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            Print
                                        </button>
                                    </a>';
                    }
            ;})
            ->addColumn('posisi', function($data) use ($siklus){
                    $evaluasi = DB::table('tb_evaluasi_tc')
                                ->where('siklus', $siklus)
                                ->where('id_registrasi', $data->idregistrasi)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($evaluasi)){
                        return "1";
                    }else{
                        return "0";
                    }
            ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if($siklus == 1) {
                        if ($inputhasil->status_data1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }else{
                        if ($inputhasil->status_data2 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisi' =>  'posisi'])
            ->make(true);
        }

        return view('evaluasi.bta.rekapinput.perusahaan');
    }

    public function dataperusahaanbtaprint(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('sub_bidang.id', '=', '4')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.bta.rekapinput.dataperusahaan', compact('data','siklus'));
    }

    public function perusahaanmalariaprint(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.id as idregistrasi ,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('tb_registrasi.bidang','=','10')
                ->where('tb_registrasi.status','>=','2')
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_evaluasi_tc')
                                ->where('siklus', $siklus)
                                ->where('id_registrasi', $data->idregistrasi)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($evaluasi)){
                        return "".'<a href="malaria/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            Print
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="malaria/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            Print
                                        </button>
                                    </a>';
                    }
            ;})
            ->addColumn('posisi', function($data) use ($siklus){
                    $evaluasi = DB::table('tb_evaluasi_tc')
                                ->where('siklus', $siklus)
                                ->where('id_registrasi', $data->idregistrasi)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($evaluasi)){
                        return "1";
                    }else{
                        return "0";
                    }
            ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if($siklus == 1) {
                        if ($inputhasil->status_data1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }else{
                        if ($inputhasil->status_data2 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisi' =>  'posisi'])
            ->make(true);
        }

        return view('evaluasi.malaria.rekapinput.perusahaan');
    }

    public function dataperusahaanmalariaprint(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('sub_bidang.id', '=', '10')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })

                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->get();
        return view('evaluasi.malaria.rekapinput.dataperusahaan', compact('data','siklus'));
    }
}

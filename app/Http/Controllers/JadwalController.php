<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\jadwal as Jadwal;
use Illuminate\Support\Facades\Redirect;

class JadwalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = DB::table('schedule')->orderBy('sort', 'asc')->get();
        return view('back/jadwal/update', compact('data'));
    }

    public function in()
    {
        return view('back/jadwal/insert');
    }

    public function update(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        foreach ($data['id'] as $key => $id) {
            $update['cycle_date_1'] = $request->cycle_date_1[$key];
            $update['cycle_date_2'] = $request->cycle_date_2[$key];

            DB::table('schedule')->where('id',$id)->update($update);
            // dd($update);        
        }
        Session::flash('message', 'Jadwal Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/jadwal');
    }    

    public function insert(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        Jadwal::create($data);
        Session::flash('message', 'Jadwal Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/jadwal');
    }

    public function delete($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Jadwal::find($id)->delete();
        return redirect("admin/jadwal");
    }
}

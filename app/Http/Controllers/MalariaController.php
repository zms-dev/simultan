<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;
use Excel;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\TanggalKirimHasil;
use App\malaria as Malaria;
use App\EvaluasiMalaria;
use App\EvaluasiNilaiMalaria;
use App\daftar as Daftar;
use App\register as Register;
use App\LogInput;
use Redirect;
use Validator;
use Session;

class MalariaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['cetakevaluasi','view']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request, $id)
    {
        DB::statement(DB::raw('SET @row_number = 0'));

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;
        $pendidikan = DB::table('tb_pendidikan')->get();
        $siklus = $request->get('y');
        $data = DB::table('parameter')->where('kategori', 'malaria')->get();

        $register = Register::find($id);
        $perusahaan = $register->kode_lebpes;

        $posisi = DB::table('tb_tandaterima')
                    ->where([
                                ['id_registrasi','=', $id],
                                ['siklus','=',$siklus]
                            ])
                    ->get();
        // dd($posisi);
        $validasi = DB::table('tb_tandaterima')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->get();

        if (count($validasi)) {
            return view('hasil_pemeriksaan/malaria', compact('data', 'perusahaan','siklus','date','pendidikan','posisi'));
        }else{
            Session::flash('message', 'Harap Input Tanda Terima Bahan Malaria Terlebih Dahulu.');
            Session::flash('alert-class', 'alert-danger');
            return redirect('tanda-terima');
        }
        // if ($siklus == 2) {
        //     if ($q1->siklus == 12) {
        //         if ($q1->pemeriksaan == 'done') {
        //             if (count($validasi)) {
        //                 return view('hasil_pemeriksaan/malaria', compact('data', 'perusahaan','siklus','date','pendidikan','posisi'));
        //             }else{
        //                 Session::flash('message', 'Harap Input Tanda Terima Bahan Malaria Terlebih Dahulu.');
        //                 Session::flash('alert-class', 'alert-danger');
        //                 return redirect('tanda-terima');
        //             }
        //         }else{
        //             Session::flash('message', 'Harap Input Form Siklus 1.');
        //             Session::flash('alert-class', 'alert-danger');
        //             return back();
        //         }
        //     }else{
        //         if (count($validasi)) {
        //             return view('hasil_pemeriksaan/malaria', compact('data', 'perusahaan','siklus','date','pendidikan','posisi'));
        //         }else{
        //             Session::flash('message', 'Harap Input Tanda Terima Bahan Malaria Terlebih Dahulu.');
        //             Session::flash('alert-class', 'alert-danger');
        //             return redirect('tanda-terima');
        //         }
        //     }
        // }else{
        //     if (count($validasi)) {
        //         return view('hasil_pemeriksaan/malaria', compact('data', 'perusahaan','siklus','date','pendidikan','posisi'));
        //     }else{
        //         Session::flash('message', 'Harap Input Tanda Terima Bahan Malaria Terlebih Dahulu.');
        //         Session::flash('alert-class', 'alert-danger');
        //         return redirect('tanda-terima');
        //     }
        // }
    }

    public function view(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;
        $data = DB::table('tb_malaria')
                ->leftJoin('tb_pendidikan','tb_pendidikan.id','=','tb_malaria.pendidikan_petugas')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->select('tb_malaria.*','tb_pendidikan.tingkat')
                ->first();
        $datas = DB::table('tb_malaria')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->get();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;

        // $date = $data->created_at->year;
        // return view('cetak_hasil/telur-cacing', compact('data', 'perusahaan', 'datas', 'type'));
        $pdf = PDF::loadview('cetak_hasil/malaria', compact('data', 'perusahaan','datas', 'type','siklus','date'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        $kode_pes = sprintf("%04s", Auth::user()->id_member);
        if ($siklus == 1) {
            $sikprint = 'I';
        }else{
            $sikprint = 'II';
        }
        return $pdf->stream($kode_pes.' MAL S'.$sikprint.' '.$date.'.pdf');
    }

    public function insert(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        // $input = implode(", ",$input['stadium']);
        $i = 1;
                // dd($input);
          $years = date('Y');

        $validasi = Malaria::where(DB::raw('YEAR(tb_malaria.created_at)'), '=' , $years)->where('id_registrasi','=',$id)->where('siklus','=',$siklus)->get();// dd($input);
        
        if (count($validasi)>0) {
            Session::flash('message', 'Hasil Malaria Sudah Ada!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('hasil-pemeriksaan');
        }else{
            foreach ($input['kode'] as $kode) {
                if($kode != ''){

                    $Savedata = new Malaria;
                    $Savedata->kode = $input['kode'][$i];
                    if(!empty($input['hasil'][$i])){
                        $Savedata->hasil = $input['hasil'][$i];
                    }

                    $spesies = "";
                    if(!empty($input['spesies'][$i])){
                        $spesies = implode(", ",$input['spesies'][$i]);
                    }

                    $stadium = "";
                    if(!empty($input['stadium'][$i])){
                        $stadium = implode(", ",$input['stadium'][$i]);
                    }
                    // dd($spesies);
                    $Savedata->stadium = $stadium;
                    $Savedata->spesies = $spesies;
                    $Savedata->kondisi = $input['kondisi'];
                    $Savedata->kode_peserta = $input['kode_peserta'];
                    $Savedata->nomor_hp = $input['nomor_hp'];
                    $Savedata->nama_pemeriksa = $input['nama_pemeriksa'];
                    $Savedata->parasit = $input['parasit'][$i];
                    $Savedata->penanggung_jawab = $input['penanggung_jawab'];
                    $Savedata->pendidikan_petugas = $input['pendidikan'];
                    $Savedata->pendidikan_lain = $input['pendidikan_lain'];
                    $Savedata->catatan = $input['catatan'];
                    $Savedata->tgl_penerimaan = $input['tgl_penerimaan'];
                    $Savedata->tgl_pemeriksaan = $input['tgl_pemeriksaan'];
                    $Savedata->created_by = Auth::user()->id;
                    $Savedata->id_registrasi = $id;
                    $Savedata->siklus = $siklus;
                    $Savedata->save();
                }
                $i++;
            }

            if ($siklus == '1') {
                Register::where('id',$id)->update(['siklus_1'=>'done', 'rpr1'=>'done', 'status_data1'=>'1']);
                Register::where('id',$id)->update(['pemeriksaan'=>'done']);
            }else{
                Register::where('id',$id)->update(['siklus_2'=>'done', 'rpr2'=>'done', 'status_data2'=>'1']);
                Register::where('id',$id)->update(['pemeriksaan2'=>'done']);
            }
            $log = new LogInput;
            $log->id_registrasi = $id;
            $log->status = 'Input Hasil';
            $log->created_by = Auth::user()->id;
            $log->siklus = $siklus;
            $log->type = '';
            $log->save();

            return redirect('hasil-pemeriksaan');
        }
    }

    public function edit(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $date = $q2->year;
        $pendidikan = DB::table('tb_pendidikan')->get();
        $data = DB::table('tb_malaria')
                ->leftJoin('tb_pendidikan','tb_pendidikan.id','=','tb_malaria.pendidikan_petugas')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->select('tb_malaria.*','tb_pendidikan.tingkat')
                ->first();
        $datas = DB::table('tb_malaria')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->get();
        // dd($datas);
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;

        return view('edit_hasil/malaria', compact('data', 'perusahaan', 'datas', 'siklus','date','pendidikan'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        // dd($input);
        $siklus = $request->get('y');
        $register = Register::find($id);

        $i = 1;
        foreach ($input['kode'] as $kode) {
            $Savedata = [];
            if($kode != ''){
                if(!empty($input['hasil'][$i])){
                    $Savedata['hasil'] = $request->hasil[$i];
                }

                if(!empty($input['parasit'][$i])){
                    $Savedata['parasit'] = $request->parasit[$i];
                }

                $spesies = "";
                if(!empty($input['spesies'][$i])){
                    $spesies = implode(",",$request->spesies[$i]);
                }

                $stadium = "";
                if(!empty($input['stadium'][$i])){
                    $stadium = implode(",",$request->stadium[$i]);
                }
                // dd($spesies);
                $Savedata['siklus'] = $siklus;
                $Savedata['tgl_pemeriksaan'] = $request->tgl_pemeriksaan;
                $Savedata['tgl_penerimaan'] = $request->tgl_penerimaan;
                $Savedata['catatan'] = $request->catatan;
                $Savedata['penanggung_jawab'] = $request->penanggung_jawab;
                $Savedata['pendidikan_petugas'] = $request->pendidikan;
                $Savedata['pendidikan_lain'] = $request->pendidikan_lain;
                $Savedata['nama_pemeriksa'] = $request->nama_pemeriksa;
                $Savedata['nomor_hp'] = $request->nomor_hp;
                $Savedata['stadium'] = $stadium;
                $Savedata['spesies'] = $spesies;
                $Savedata['kode'] = $request->kode[$i];
                $Savedata['kondisi'] = $request->kondisi;
                $wakwaw[$i] = $Savedata;
                Malaria::where('id', $request->idmal[$i])->update($Savedata);
            }
            $i++;
        }
        if ($request->simpan == "Kirim") {
            TanggalKirimHasil::where('id_registrasi', $id)
                            ->where('siklus', $siklus)
                            ->delete();

            $KirimHasil = new TanggalKirimHasil;
            $KirimHasil->id_registrasi = $id;
            $KirimHasil->siklus = $siklus;
            $KirimHasil->type = "";
            $KirimHasil->tanggal_kirim = date('Y-m-d');
            $KirimHasil->save();
            
            $log = new LogInput;
            $log->id_registrasi = $id;
            $log->status = 'Kirim Hasil';
            $log->created_by = Auth::user()->id;
            $log->siklus = $siklus;
            $log->type = '';
            $log->save();

            if ($siklus == '1') {
                Register::where('id',$id)->update(['status_data1'=>'2']);
            }else{
                Register::where('id',$id)->update(['status_data2'=>'2']);
            }
        }else{
            $log = new LogInput;
            $log->id_registrasi = $id;
            $log->status = 'Edit Hasil';
            $log->created_by = Auth::user()->id;
            $log->siklus = $siklus;
            $log->type = '';
            $log->save();
        }
        return redirect('edit-hasil');
    }

    public function evaluasi(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        $data = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data->created_at );  
        $tahun = $tahun->year;
        $data = DB::table('tb_malaria')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->first();
        $datas = DB::table('tb_malaria')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->get();
        $rujuk = DB::table('tb_rujukan_malaria')
                ->where('siklus', $siklus)
                ->where('tahun', '=' , $tahun)
                ->get();
        $nilai = DB::table('tb_evaluasi_nilai_malaria')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where('tahun', '=' , $tahun)
                ->get();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $evaluasi = DB::table('tb_evaluasi_malaria')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->first();
        // $date = $data->created_at->year;
        return view('evaluasi.malaria.penilaian.evaluasi.index', compact('data', 'perusahaan', 'datas', 'type', 'rujuk', 'evaluasi', 'id', 'siklus', 'nilai'));
    }

    public function laporanpesertamalaria()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view('evaluasi.malaria.laporanhasilevaluasi.index', compact('parameter'));
    }

    public function laporanpesertamalariapdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $siklus = $request['siklus'];
        $tahun = $input['tahun'];

        if($input['proses'] == 'Export Excel') {
            $i = 0;

            $pesertamalarias = DB::table('tb_evaluasi_malaria')
                        ->select(['id_registrasi' => 'id_registrasi'])
                        ->where('siklus', $siklus)
                        ->where('tahun', '=' , $tahun)
                        ->groupby('id_registrasi')
                        ->get();

            foreach($pesertamalarias as $pesertamalaria) {
                $id = $pesertamalaria->id_registrasi;

                $data[$id] = DB::table('tb_malaria')
                    ->leftJoin('tb_registrasi', 'tb_registrasi.kode_lebpes', '=', 'tb_malaria.kode_peserta')
                    ->leftJoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                    ->select('kode_peserta', 'nama_lab')
                    ->where('id_registrasi', $id)
                    ->where('tb_malaria.siklus', $siklus)
                    ->where(DB::raw('YEAR(tb_malaria.created_at)'), '=' , $tahun)
                    ->first();

                $datas[$id] = DB::table('tb_malaria')
                        ->where('id_registrasi', $id)
                        ->where('siklus', $siklus)
                        ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                        ->get();
                $rujuk[$id] = DB::table('tb_rujukan_malaria')
                        ->where('siklus', $siklus)
                        ->where('tahun', '=' , $tahun)
                        ->get();
                $nilai[$id] = DB::table('tb_evaluasi_nilai_malaria')
                        ->where('id_registrasi', $id)
                        ->where('siklus', $siklus)
                        ->where('tahun', '=' , $tahun)
                    ->get();
                $register[$id] = Register::find($id);
                $perusahaan[$id] = Register::find($id)->perusahaan->nama_lab;
                $evaluasi[$id] = DB::table('tb_evaluasi_malaria')
                            ->select(['kesimpulan', 'total'])
                            ->where('siklus', $siklus)
                            ->where('id_registrasi', $id)
                            ->get();
            }
            // dd($data);
            Excel::create('LAPORAN HASIL EVALUASI MALARIA ' . 'tahun', function($excel) use ($pesertamalarias,$data,$evaluasi,$rujuk,$nilai,$siklus,$tahun,$register,$perusahaan,$datas) {
                $excel->sheet('Hasil Evaluasi', function($sheet) use ($pesertamalarias,$data,$evaluasi,$rujuk,$nilai,$siklus,$tahun,$register,$perusahaan,$datas) {
                    $sheet->loadView('evaluasi/malaria/penilaian/evaluasi/pesertamalariaexcel', array('pesertamalarias' => $pesertamalarias,'data' => $data, 'evaluasi' => $evaluasi,'rujuk' => $rujuk,'nilai' => $nilai,'siklus' => $siklus,'tahun' => $tahun,'register' => $register,'perusahaan' => $perusahaan,'datas' => $datas));
                });
            })->download('xls');
        } else {
            $pesertamalarias = DB::table('tb_evaluasi_malaria')
                        ->select(['id_registrasi' => 'id_registrasi'])
                        ->where('siklus', $siklus)
                        ->where('tahun', '=' , $tahun)
                        ->groupby('id_registrasi')
                        ->get();

            foreach($pesertamalarias as $pesertamalaria) {
                $id = $pesertamalaria->id_registrasi;

                $data[$id] = DB::table('tb_malaria')
                    ->where('id_registrasi', $id)
                    ->where('siklus', $siklus)
                    ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                    ->first();
                $datas[$id] = DB::table('tb_malaria')
                    ->leftJoin('tb_registrasi', 'tb_registrasi.kode_lebpes', '=', 'tb_malaria.kode_peserta')
                    ->leftJoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                    // ->select('kode_peserta', 'nama_lab')
                    ->where('id_registrasi', $id)
                    ->where('tb_malaria.siklus', $siklus)
                    ->where(DB::raw('YEAR(tb_malaria.created_at)'), '=' , $tahun)
                    ->get();
                $rujuk[$id] = DB::table('tb_rujukan_malaria')
                        ->where('siklus', $siklus)
                        ->where('tahun', '=' , $tahun)
                        ->get();
                $nilai[$id] = DB::table('tb_evaluasi_nilai_malaria')
                        ->where('id_registrasi', $id)
                        ->where('siklus', $siklus)
                        ->where('tahun', '=' , $tahun)
                        ->get();
                $register[$id] = Register::find($id);
                $perusahaan[$id] = Register::find($id)->perusahaan->nama_lab;
                $evaluasi[$id] = DB::table('tb_evaluasi_malaria')
                            ->where('siklus', $siklus)
                            ->where('id_registrasi', $id)
                            ->first();
            }

            $i = 1;
            if (count($evaluasi) > 0) {
                $pdf = PDF::loadview('evaluasi.malaria.penilaian.evaluasi.pesertamalaria', compact('pesertamalarias', 'data','data_peserta','evaluasisaran','tanggal','evaluasi','rujuk','nilai','id','siklus','tanggal','tahun','register','perusahaan','datas', 'i'))
                ->setPaper('a4', 'potrait')
                ->setOptions(['dpi' => 135, 'defaultFont' => 'sans-serif'])
                ->setwarnings(false);
                return $pdf->stream('Telur Cacing.pdf');
            }else{
                Session::flash('message', 'TC belum dievaluasi!');
                Session::flash('alert-class', 'alert-danger');
                return back();
            }
        }
    }

    public function rekappesertamalaria()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view('evaluasi.malaria.rekapinputpeserta.index', compact('parameter'));
    }

    public function rekappesertamalariapdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $siklus = $request['siklus'];
        $tahun = $input['tahun'];
        $i = 0;

        $data = DB::table('tb_malaria')
            ->join('tb_registrasi', 'tb_registrasi.kode_lebpes', '=', 'tb_malaria.kode_peserta')
            ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
            ->select('kode_peserta', 'nama_lab','tb_registrasi.id')
            ->where(function($query) use ($siklus)
                {
                    $query->where('tb_registrasi.siklus', '12')
                        ->orwhere('tb_registrasi.siklus', $siklus);
                })
            ->where('tb_malaria.siklus', $siklus)
            ->where(function($query)
                {
                    $query->where('pemeriksaan2', 'done')
                        ->orwhere('pemeriksaan', 'done')
                        ->orwhere('rpr1', 'done')
                        ->orwhere('rpr2', 'done');
                })
            ->where(DB::raw('YEAR(tb_malaria.created_at)'), '=' , $tahun)
            ->groupby('id_registrasi')
            ->get();
        foreach ($data as $key => $val) {
            $datas = DB::table('tb_malaria')
                    ->where('id_registrasi', $val->id)
                    ->where('siklus', $siklus)
                    ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                    ->get();
            $val->datas = $datas;
        }
        
        // dd($data);
        Excel::create('LAPORAN HASIL EVALUASI MALARIA TAHUN '. $tahun, function($excel) use ($data,$siklus,$tahun,$datas) {
            $excel->sheet('Hasil Evaluasi', function($sheet) use ($data,$siklus,$tahun,$datas) {
                $sheet->loadView('evaluasi.malaria.rekapinputpeserta.view', array('data' => $data,'siklus' => $siklus,'tahun' => $tahun,'datas' => $datas));
            });
        })->download('xls');
    }

    public function cetakevaluasi(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        $data = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data->created_at );  
        $tahun = $tahun->year;
        $data = DB::table('tb_malaria')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->first();
        $datas = DB::table('tb_malaria')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->get();
        $rujuk = DB::table('tb_rujukan_malaria')
                ->where('siklus', $siklus)
                ->where('tahun', '=' , $tahun)
                ->get();
        $nilai = DB::table('tb_evaluasi_nilai_malaria')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where('tahun', '=' , $tahun)
                ->get();

        $datass = DB::table('tb_malaria')
                ->join('tb_registrasi','tb_registrasi.id','=','tb_malaria.id_registrasi')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->select('perusahaan.nama_lab','perusahaan.alamat','tb_malaria.kode_peserta')
                ->where('id_registrasi', $id)
                ->where('tb_malaria.siklus', $siklus)
                ->where(DB::raw('YEAR(tb_malaria.created_at)'), '=' , $tahun)
                ->first();

        $evaluasi = DB::table('tb_evaluasi_malaria')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->where('tb_evaluasi_malaria.tahun','=',$tahun)
                    ->first();
        $ttd = DB::table('tb_ttd_evaluasi')
            ->where('tahun', $tahun)
            ->where('siklus', $siklus)
            ->where('bidang', '=', '10')
            ->first();

        if (count($evaluasi) > 0) {
            $pdf = PDF::loadView('evaluasi.malaria.penilaian.evaluasi.cetak',compact('data','datass','alamat','data_peserta
                ', 'datas', 'type', 'rujuk', 'evaluasi', 'siklus', 'tahun', 'register','nilai','ttd'));
                $pdf->setPaper('a4','potrait');
                return $pdf->stream('Malaria.pdf');
        }else{
            Session::flash('message', 'Malaria belum dievaluasi');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
    }
    public function insertevaluasi(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        $tahun = date('Y');
        // dd($input);
        if ($input['simpan'] == 'Simpan') {
            $total = 0;
            $no = 0;
            foreach ($input['rujukan'] as $kode) {
                $Save = new EvaluasiNilaiMalaria;
                $Save->kode_sediaan = $input['kode_sediaan'][$no];
                $Save->nilai = $input['rujukan'][$no];
                $Save->id_registrasi = $id;
                $Save->siklus = $siklus;
                $Save->tahun = $tahun;
                $Save->save();
                $total = $total + $input['rujukan'][$no];
                $no++;
            }
            // dd($total);
            if ($total < 70) {
                $kesimpulan = 'Buruk';
            }elseif($total < 80){
                $kesimpulan = 'Baik';
            }elseif($total < 90){
                $kesimpulan = 'Sangat Baik';
            }elseif($total <= 100){
                $kesimpulan = 'Sempurna';
            }
            $Savedata = new EvaluasiMalaria;
            $Savedata->kesalahan = $input['kesalahan'];
            $Savedata->kesimpulan = $kesimpulan;
            $Savedata->tindakan = $input['tindakan'];
            $Savedata->total = $total;
            $Savedata->id_registrasi = $id;
            $Savedata->siklus = $siklus;
            $Savedata->tahun = $tahun;
            $Savedata->save();

        }else {
            $total = 0;
            $no = 0;
            // dd($input);
            foreach ($input['rujukan'] as $kode) {
                $Save['kode_sediaan'] = $input['kode_sediaan'][$no];
                $Save['nilai'] = $input['rujukan'][$no];
                EvaluasiNilaiMalaria::where('id', $input['idnilai'][$no])->update($Save);
                $total = $total + $input['rujukan'][$no];
                $no++;
            }
            if ($total < 70) {
                $kesimpulan = 'Buruk';
            }elseif($total < 79){
                $kesimpulan = 'Baik';
            }elseif($total < 89){
                $kesimpulan = 'Sangat Baik';
            }elseif($total < 100){
                $kesimpulan = 'Sempurna';
            }
            $Savedata['kesalahan'] = $request->kesalahan;
            $Savedata['tindakan'] = $request->tindakan;
            $Savedata['total'] = $total;
            $Savedata['kesimpulan'] = $kesimpulan;
            EvaluasiMalaria::where('id_registrasi', $id)->where('siklus', $siklus)->update($Savedata);
        }

        return redirect::back();
    }
}

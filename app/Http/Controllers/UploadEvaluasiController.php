<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\evaluasi as Evaluasi;
use App\register as Register;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;

use Yajra\Datatables\Datatables;
class UploadEvaluasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function register(\Illuminate\Http\Request $request)
    {
        // $data = DB::table('evaluasi')
        //     ->leftjoin('sub_bidang', 'evaluasi.bidang', '=', 'sub_bidang.id')
        //     ->leftjoin('users', 'evaluasi.created_by', '=', 'users.id')
        //     ->select('evaluasi.*', 'sub_bidang.parameter as Bidang', 'users.name as Name')
        //     ->get();
        // $sub_bidang = DB::table('sub_bidang')->get();
        // return view('back/evaluasi/index', compact('data', 'sub_bidang'));
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = Register::select( DB::raw('*, tb_registrasi.id as Id, tb_registrasi.siklus, tb_registrasi.evaluasi, tb_registrasi.evaluasi2, sub_bidang.parameter, perusahaan.nama_lab, @rownum := @rownum +1 as rownum'))
                                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                                ->where(function($query)
                                    {
                                        if ('tb_registrasi.evaluasi' != NULL) {
                                            $query->orwhere('evaluasi2', NULL);
                                        }
                                        if ('tb_registrasi.evaluasi2' != NULL) {
                                            $query->orwhere('evaluasi', NULL);
                                        }
                                    })
                                ->where(function($query)
                                    {
                                        $query->orwhere('pemeriksaan2', 'done');
                                        $query->orwhere('pemeriksaan', 'done');
                                    });
            return Datatables::of($datas)
            ->addColumn('actiona', function($data){
                    if ($data->evaluasi == NULL) {
                        if ($data->pemeriksaan == 'done') {
                            return "".'<a href="upload-evaluasi/insert/'.$data->Id.'?x=a" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                <i class="icon-pencil"></i>
                                            </button>
                                        </a>';
                        }
                    }
            })
            ->addColumn('actionb', function($data){
                    if ($data->evaluasi2 == NULL) {
                        if ($data->pemeriksaan2 == 'done') {
                            return "".'<a href="upload-evaluasi/insert/'.$data->Id.'?x=b" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                <i class="icon-pencil"></i>
                                            </button>
                                        </a>';
                        }
                    }
            })
            ->rawColumns(['actiona', 'actionb'])
            ->make(true);
        }
        return view('back/evaluasi/index');
    }

    public function index(\Illuminate\Http\Request $request)
    {
        // $data = DB::table('evaluasi')
        //     ->leftjoin('sub_bidang', 'evaluasi.bidang', '=', 'sub_bidang.id')
        //     ->leftjoin('users', 'evaluasi.created_by', '=', 'users.id')
        //     ->select('evaluasi.*', 'sub_bidang.parameter as Bidang', 'users.name as Name')
        //     ->get();
        // $sub_bidang = DB::table('sub_bidang')->get();
        // return view('back/evaluasi/index', compact('data', 'sub_bidang'));
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = Evaluasi::select( DB::raw('evaluasi.id as Id, evaluasi.type, tb_registrasi.siklus, sub_bidang.parameter, perusahaan.nama_lab, @rownum := @rownum +1 as rownum'))
                                ->join('tb_registrasi', 'tb_registrasi.id', '=', 'evaluasi.id_register')
                                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                                ;
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="upload-evaluasi/delete'.'/'.$data->Id.'">
                                    <button class="btn btn-danger btn-xs">
                                        <i class="icon-trash "></i>
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('back/evaluasi/index');
    }

    public function edit($id)
    {
        $data = DB::table('evaluasi')
            ->leftjoin('tb_bidang', 'evaluasi.bidang', '=', 'tb_bidang.id')
            ->leftjoin('users', 'evaluasi.id_user', '=', 'users.id')
            ->select('evaluasi.*', 'tb_bidang.bidang as Bidang', 'users.id as Id', 'users.name as Name')
            ->get();
        $sub_bidang = DB::table('sub_bidang')->get();
        return view('back/evaluasi/update', compact('data'));
    }

    public function in($id)
    {
        $data = DB::table('tb_registrasi')
                ->where('tb_registrasi.id', $id);
        return view('back/evaluasi/insert', compact('data'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $data = Request::all();
        if (Request::hasFile('file'))
        {
            $dest = public_path('asset/backend/evaluasi/');
            $name = Request::file('file')->getClientOriginalName();
            Request::file('file')->move($dest, $name);
            $data['file'] = $name;
        }
        $data['created_by'] = Auth::user()->id;
        Evaluasi::where('id',$id)->update($data);
        Session::flash('message', 'Evaluasi Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/upload-evaluasi');
    }    

    public function insert(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $bidang = DB::table('tb_registrasi')
                    ->leftjoin('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                    ->where('tb_registrasi.id', $id)
                    ->select('sub_bidang.id_bidang')
                    ->get();
        // dd($bidang);
        $data = Request::file('file');
        $data = Request::file('sertifikat');
        $data = Request::all();
        $data['created_by'] = Auth::user()->id;
        
        if (Request::hasFile('file'))
        {
            $dest = public_path('asset/backend/evaluasi/');
            $name = Request::file('file')->getClientOriginalName();
            Request::file('file')->move($dest, $name);
        }
        $data['file'] = $name;
        $data['id_register'] = $id;
        $data['type'] = $type;
        Evaluasi::create($data);
        if ($bidang[0]->id_bidang >= '5') {
                $register['evaluasi'] = 'Done';
                $register['evaluasi2'] = 'Done';
        }else{
            if ($type == 'a') {
                $register['evaluasi'] = 'Done';
            }else{
                $register['evaluasi2'] = 'Done';
            }
        }
        Register::where('id',$id)->update($register);
        Session::flash('message', 'Evaluasi Berhasil Disimpan!'); 
        Session::flash('alert-class', 'alert-success'); 

        return redirect('admin/upload-evaluasi');
    }

    public function delete($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Evaluasi::find($id)->delete();
        return redirect("admin/upload-evaluasi");
    }
}

<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use App\CatatanImun;

class HasilPemeriksaanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tahun = date('Y');
        $user = Auth::user()->id;
        $siklus = DB::table('tb_siklus')->first();
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', NULL)
                        ->orWhere('pemeriksaan', NULL)
                        ->orWhere('siklus_1',NULL)
                        ->orWhere('siklus_2',NULL)
                        ->orWhere('rpr1',NULL)
                        ->orWhere('rpr2',NULL);
                        // $query->where('pemeriksaan2', NULL)
                        //     ->orwhere('pemeriksaan', NULL);
                        // $query->where('siklus_1', NULL)
                        //     ->orwhere('siklus_2', NULL);
                        // $query->where('rpr1', NULL)
                        //     ->orwhere('rpr2', NULL);
                    })
                ->get();
        return view('hasil_pemeriksaan', compact('data','siklus'));
    }

    public function editdata()
    {
        $siklus = DB::table('tb_siklus')->first();
        $user = Auth::user()->id;
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by', $user)
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 1)
                            ->orwhere('status_data2', 1)
                            ->orwhere('status_datarpr1', 1)
                            ->orwhere('status_datarpr2', 1);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
                // dd($data);
        return view('edit_hasil', compact('data','siklus'));
    }

    public function cetak(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('tahun');
        $siklus = DB::table('tb_siklus')->first();
        $user = Auth::user()->id;
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        // dd($data);
        $data2 =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by', $user)
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 1)
                            ->orwhere('status_data2', 1)
                            ->orwhere('status_datarpr1', 1)
                            ->orwhere('status_datarpr2', 1);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        return view('cetak_hasil', compact('data', 'data2','siklus','tahun'));
    }

    public function hasilevaluasi()
    {
        $user = Auth::user()->id;
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by', $user)
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        // dd($data);
        $data2 =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by', $user)
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 1)
                            ->orwhere('status_data2', 1)
                            ->orwhere('status_datarpr1', 1)
                            ->orwhere('status_datarpr2', 1);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        return view('hasil_evaluasi', compact('data', 'data2'));
    }
    public function cetaksementara()
    {
        $user = Auth::user()->id;
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by', $user)
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 1)
                            ->orwhere('status_data2', 1)
                            ->orwhere('status_datarpr1', 1)
                            ->orwhere('status_datarpr2', 1);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        // dd($data);
        return view('cetak_sementara', compact('data'));
    }

    public function info(\Illuminate\Http\Request $request)
    {  
        $tahun = $request->get('tahun');
        $siklus = $request->get('siklus');
        // dd($siklus);
        $uss = Auth::id();
        $data = DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'tb_bidang.id', '=', 'sub_bidang.id_bidang')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('tb_registrasi.created_by', $uss)
                ->select('tb_registrasi.id','tb_registrasi.bidang as Bidang','sub_bidang.parameter','sub_bidang.id as sub_id','tb_bidang.bidang','tb_registrasi.status_data1','tb_registrasi.status_data2','tb_registrasi.status_datarpr1','tb_registrasi.status_datarpr2')
                ->get();
        foreach ($data as $key => $val) {
            $laporan = DB::table('upload_lap_akhir')
                        ->where('parameter', $val->sub_id)
                        ->where('siklus', $siklus)
                        ->where('tahun', $tahun)
                        ->groupBy('parameter')
                        ->first();
            $pembayaran = DB::table('tb_registrasi')
                            ->where('id','=',$val->id)
                            ->select('status')
                            ->first();
            $val->pembayaran = $pembayaran;
            if($siklus == 1){
                $status = DB::table('tb_registrasi')
                            ->where('id','=',$val->id)
                            ->select('status_data1 as status')
                            ->first();
                $typea = DB::table('tb_registrasi')
                            ->where('id','=',$val->id)
                            ->select('status_data1 as status')
                            ->first();
                $typeb = DB::table('tb_registrasi')
                            ->where('id','=',$val->id)
                            ->select('status_data2 as status')
                            ->first();
                $tp = DB::table('tb_registrasi')
                            ->where('id','=',$val->id)
                            ->select('status_data1 as status')
                            ->first();
                $rpr = DB::table('tb_registrasi')
                            ->where('id','=',$val->id)
                            ->select('status_datarpr1 as status')
                            ->first();
            }else{
                $status = DB::table('tb_registrasi')
                            ->where('id','=',$val->id)
                            ->select('status_data2 as status')
                            ->first();
                $typea = DB::table('tb_registrasi')
                            ->where('id','=',$val->id)
                            ->select('status_datarpr1 as status')
                            ->first();
                $typeb = DB::table('tb_registrasi')
                            ->where('id','=',$val->id)
                            ->select('status_datarpr2 as status')
                            ->first();
                $tp = DB::table('tb_registrasi')
                            ->where('id','=',$val->id)
                            ->select('status_data2 as status')
                            ->first();
                $rpr = DB::table('tb_registrasi')
                            ->where('id','=',$val->id)
                            ->select('status_datarpr2 as status')
                            ->first();
            }
            $val->status = $status;
            $val->typea = $typea;
            $val->typeb = $typeb;
            $val->tp = $tp;
            $val->rpr = $rpr;
            $val->laporan = $laporan;
        }
        // dd($data);
        return view('info/info_pendaftaran', compact('data', 'tahun', 'siklus'));
    }

    public function statuspembayaran()
    {
        $tahun = date('Y');
        $date = date('Y-m-d H:i:s');
        $data = DB::table('users')
                ->join('tb_registrasi', 'tb_registrasi.created_by', '=', 'users.id')
                ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                ->where('tb_registrasi.status','=','1')
                ->where('id_pembayaran','=','1')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'),'=', $tahun)
                ->where('tb_registrasi.created_by', Auth::user()->id)
                ->groupBy('tb_registrasi.created_by')
                ->groupBy('tb_registrasi.no_urut')
                ->select('tb_registrasi.siklus',DB::raw('(CASE WHEN tb_registrasi.siklus != 12 THEN SUM(sub_bidang.tarif / 2) ELSE SUM(sub_bidang.tarif) END)as total'), 'users.name', 'users.email','users.id_member','tb_registrasi.created_at','tb_registrasi.no_urut',DB::raw('tb_registrasi.created_at + INTERVAL 2 HOUR + INTERVAL 15 MINUTE as coutdown'), DB::raw('DATE_ADD(tb_registrasi.created_at,INTERVAL 15 MINUTE) as coutdown1jam'))
                ->get();

        $emailva = DB::table('tb_email_va')
                    ->where('user_id','=', Auth::user()->id)
                    ->where('tahun','=', $tahun)
                    ->where('siklus','=', $data[0]->siklus)
                    ->get();
        // dd($emailva);
        if (Auth::user()->role == 3) {
            return view('info/status_pembayaran', compact('data', 'date','emailva'));
        }else{
            return Redirect('index');
        }
    }

    public function cekpeserta()
    {
        $tahun = date("Y");
        $data = DB::table('users')
                ->join('tb_registrasi', 'tb_registrasi.created_by', '=', 'users.id')
                ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                ->where('tb_registrasi.status','=','1')
                ->where('id_pembayaran','=','1')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'),'=', $tahun)
                ->groupBy('tb_registrasi.created_by')
                ->groupBy('tb_registrasi.no_urut')
                ->select('users.id',DB::raw('(CASE WHEN tb_registrasi.siklus != 12 THEN SUM(sub_bidang.tarif / 2) ELSE SUM(sub_bidang.tarif) END)as total'), 'users.name', 'users.email','users.id_member','tb_registrasi.created_at','tb_registrasi.no_urut',DB::raw('DATE_ADD(tb_registrasi.created_at,INTERVAL 3 HOUR) as coutdown'), DB::raw('DATE_ADD(tb_registrasi.created_at,INTERVAL 1 HOUR) as coutdown1jam'))
                ->get();
        $waktu = date("Y-m-d H:i:s");
        // foreach ($data as $key => $val) {
        //     if ($waktu > $val->coutdown) {
        //         $satuan = DB::table('tb_registrasi')
        //                     ->where('tb_registrasi.status','=','1')
        //                     ->where('id_pembayaran','=','1')
        //                     ->where(DB::raw('YEAR(tb_registrasi.created_at)'),'=', $tahun)
        //                     ->where('tb_registrasi.created_by','=', $val->id)
        //                     ->delete();
        //     }
        // }
        return "Success";
    }
}
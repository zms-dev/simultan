<?php

namespace App\Http\Controllers;
use DB;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Excel;
use stdClass;
use PDF;
class ApiController extends Controller
{
    public function rekapinstansiexcel(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('tahun');
        $data = DB::table('sub_bidang')->get();
        foreach($data as $skey => $r)
        {
            $blk = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '1')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($blk[0]->jumlah_tarif == NULL) {
                $r->blk = 0;
            }else{
                $r->blk = $blk[0]->jumlah_tarif;
            }

            $bblk = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($bblk[0]->jumlah_tarif == NULL) {
                $r->bblk = 0;
            }else{
                $r->bblk = $bblk[0]->jumlah_tarif;
            }

            $rspem = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '3')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($rspem[0]->jumlah_tarif == NULL) {
                $r->rspem = 0;
            }else{
                $r->rspem = $rspem[0]->jumlah_tarif;
            }
            $pus = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '4')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($pus[0]->jumlah_tarif == NULL) {
                $r->pus = 0;
            }else{
                $r->pus = $pus[0]->jumlah_tarif;
            }
            $labkes = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '5')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($labkes[0]->jumlah_tarif == NULL) {
                $r->labkes = 0;
            }else{
                $r->labkes = $labkes[0]->jumlah_tarif;
            }
            $rsswa = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '6')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($rsswa[0]->jumlah_tarif == NULL) {
                $r->rsswa = 0;
            }else{
                $r->rsswa = $rsswa[0]->jumlah_tarif;
            }
            $labkl = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '7')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($labkl[0]->jumlah_tarif == NULL) {
                $r->labkl = 0;
            }else{
                $r->labkl = $labkl[0]->jumlah_tarif;
            }
            $utdrs = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '8')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($utdrs[0]->jumlah_tarif == NULL) {
                $r->utdrs = 0;
            }else{
                $r->utdrs = $utdrs[0]->jumlah_tarif;
            }
            $utdpmi = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '9')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($utdpmi[0]->jumlah_tarif == NULL) {
                $r->utdpmi = 0;
            }else{
                $r->utdpmi = $utdpmi[0]->jumlah_tarif;
            }
            $rstni = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '10')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($rstni[0]->jumlah_tarif == NULL) {
                $r->rstni = 0;
            }else{
                $r->rstni = $rstni[0]->jumlah_tarif;
            }
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }
    public function rekappeserta(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('tahun');
        $data = DB::table('sub_bidang')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->select('sub_bidang.*', 'tb_bidang.bidang')
                ->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->count();
            $r->jumlah = $jumlah;
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function grafikinstansisemua(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('tahun');
        $data = DB::table('badan_usaha')->get();
        foreach($data as $skey => $r)
        {
            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->groupby('perusahaan.id')
                            ->get();
            $r->peserta = count($peserta);

            $harga = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->orderby('perusahaan.pemerintah')
                            ->select(DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && (sub_bidang.siklus = 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && (sub_bidang.siklus = 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif 
                                            end) as total'))
                            ->first();
            $r->harga = $harga->total;
        }
        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->alias;
            $row['y'] = $r->peserta;
            $row['x'] = $r->harga;
            array_push($rows, $row);
        }

        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }

    public function rekaptransaksi(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('tahun');
        $jumlah = DB::table('tb_registrasi')
                        ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                        ->where('tb_registrasi.status','>=','2')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                        ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                        ->first();
        $peserta = DB::table('tb_registrasi')
                        ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                        ->where('tb_registrasi.status','>=','2')
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                        ->groupby('tb_registrasi.perusahaan_id')
                        ->get();
        // dd($data);
        $rows = array();
        $row['total_tarif'] = $jumlah->jumlah_tarif;
        $row['total_peserta'] = count($peserta);
        array_push($rows, $row);

        return response ($rows);
    }
}
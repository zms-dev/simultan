<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;
use App\Hide;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\TtdEvaluasi;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class TtdHasilEvaluasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = DB::table('tb_ttd_evaluasi')
                ->join('sub_bidang','sub_bidang.id','=','tb_ttd_evaluasi.bidang')
                ->select('tb_ttd_evaluasi.*','sub_bidang.parameter')
                ->orderBy('tb_ttd_evaluasi.id','desc')
                ->get();
        return view('back/ttdhasilevaluasi/index', compact('data'));
    }

    public function add()
    {
        $bidang = DB::table('sub_bidang')->get();
        return view('back/ttdhasilevaluasi/insert', compact('bidang'));
    }

    public function edit($id)
    {
        $data = DB::table('tb_ttd_evaluasi')->where('id', $id)->get();
        return view('back/ttdhasilevaluasi/update', compact('data'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        if (Request::hasFile('image'))
        {
            $dest = public_path('asset/backend/banner/');
            $name = Request::file('image')->getClientOriginalName();
            Request::file('image')->move($dest, $name);
        }
        $data = Request::file('image');
        $data = Request::all();
        $data['image'] = $name;
        $data['created_by'] = Auth::user()->id;
        Banner::where('id',$id)->update($data);
        Session::flash('message', 'Banner Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/ttd-hasil-evaluasi');
    }    

    public function insert(\Illuminate\Http\Request $request)
    {
        $data = Request::file('image');
        $data = Request::all();
        if (Request::hasFile('image'))
        {
            $dest = public_path('asset/backend/ttdevaluasi/');
            $name = $data['tahun'].''.$data['siklus'].''.$data['bidang'].''.Request::file('image')->getClientOriginalName();
            Request::file('image')->move($dest, $name);
        }
        $data['image'] = $name;
        TtdEvaluasi::create($data);
        Session::flash('message', 'Ttd Evaluasi Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/ttd-hasil-evaluasi');
    }

    public function delete($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        TtdEvaluasi::find($id)->delete();
        return redirect("admin/ttd-hasil-evaluasi");
    }

}

<?php

namespace App\Http\Controllers;
use DB;
use Excel;
use PDF;
use Session;
use View;
use Response;
use App\User;
use App\register as Register;
use App\CatatanImun;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class EvaluasiImunologiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function pesertainstansi()
    {
        $data = DB::table('sub_bidang')->where('id', Auth::user()->penyelenggara)->get();
        return view('evaluasi.rekap.pesertainstansi.rekappeserta', compact('data'));
    }
    public function rekapinstansi(\Illuminate\Http\Request $request)
    {
        $bidang = $request->bidang;
        // dd($bidang);
        $tahun = $request->tahun;
        $bidang2 = DB::table('sub_bidang')->where('id',$bidang)->first();
        $data = DB::table('badan_usaha')->get();
        foreach($data as $skey => $r)
        {
                $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang','=',$bidang)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->count();
                $r->peserta = $peserta;
            
        }
        // dd($data);
        return view('evaluasi.rekap.pesertainstansi.datapesertainstansi', compact('data','bidang2','tahun'));
        // return response()->json(['Hasil'=>$data]);
    }

    public function perseninstansi()
    {
        $data = DB::table('sub_bidang')->where('id', Auth::user()->penyelenggara)->get();
        return view('evaluasi.rekap.perseninstansi.rekappersen', compact('data'));
    }
    public function rekapperseninstansi(\Illuminate\Http\Request $request)
    {
        $bidang = $request->bidang;
        // dd($bidang);
        $tahun = $request->tahun;
        $bidang2 = DB::table('sub_bidang')->where('id',$bidang)->first();
        $data = DB::table('badan_usaha')->get();
        foreach($data as $skey => $r)
        {

                $semua = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang','=',$bidang)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->where(function($query){
                                if (Auth::user()->badan_usaha == '9') {
                                    $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                                }else{
                                    $query->where('perusahaan.pemerintah','!=','9');
                                }
                            })
                            ->count();
                $peserta = DB::table('tb_registrasi')
                                ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                                ->where('perusahaan.pemerintah','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where('tb_registrasi.bidang','=',$bidang)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                                ->where(function($query){
                                    if (Auth::user()->badan_usaha == '9') {
                                        $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                                    }
                                })
                                ->count();
                $r->persen = $peserta / $semua * 100;
            }

        // dd($data);
        return view('evaluasi.rekap.perseninstansi.dataperseninstansi', compact('data','bidang2','tahun'));
        // return response()->json(['Hasil'=>$data]);
    }

    public function rekapevaluasibelum()
    {
        $data = DB::table('sub_bidang')->where('id', Auth::user()->penyelenggara)->get();
        // return $data;
        return view('evaluasi.imunologi.rekappeserta.index', compact('data'));
    }

    public function laporanhasil()
    {
        $data = DB::table('sub_bidang')->where('id', Auth::user()->penyelenggara)->get();
        // return $data;
        return view('evaluasi.rekap.laporanhasil.index', compact('data'));
    }
    public function laporanhasilna(\Illuminate\Http\Request $request){
        $date = date('d F Y');
        $tahun = $request->tahun;
        $form = ['0','1','2','3','4','5','Anti HIV','Anti TP','hbsag','hcv'];
        $jenisform = ['0','1','2','3','4','5','Anti Hiv','Syphilis','HBsAg','ANTI HCV'];
        $bidang = $request->bidang;
        $siklus = $request->siklus;

            $data = DB::table('master_imunologi')
                ->join('tb_registrasi', 'master_imunologi.id_registrasi', '=', 'tb_registrasi.id')
                ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where(function($query){
                    if (Auth::user()->penyelenggara == '6') {
                        if (Auth::user()->badan_usaha == '9') {
                            $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                        }else{
                            $query->where('perusahaan.pemerintah','!=','9');
                        }
                    }
                })
                ->select('master_imunologi.*', 'perusahaan.pemerintah')
                ->where('master_imunologi.siklus', $siklus)
                ->where('tb_registrasi.bidang', $bidang)
                ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $tahun)
                ->orderBy('master_imunologi.jenis_form', 'desc')
                ->get();

            foreach($data as $skey => $r)
            {
                $data2 = DB::table('hp_imunologi')
                    ->join('tb_rujukan_imunologi', 'hp_imunologi.kode_bahan_kontrol', '=', 'tb_rujukan_imunologi.kode_bahan_uji')
                    ->where('hp_imunologi.id_master_imunologi', '=' , $r->id)
                    ->where(function($query) use ($r, $jenisform, $bidang){
                        if (Auth::user()->penyelenggara == '7') {
                            if ($r->jenis_form == 'rpr-syphilis') {
                                $query->where('tb_rujukan_imunologi.parameter','=', 'RPR');
                            }else{
                                $query->where('tb_rujukan_imunologi.parameter','=', 'RPR');
                            }
                        }else{
                            $query->where('tb_rujukan_imunologi.parameter','=', $jenisform[$bidang]);
                        }
                    })
                    ->where('tb_rujukan_imunologi.tahun', '=' , $tahun)
                    ->where('tb_rujukan_imunologi.siklus', $siklus)
                    ->get();
                $r->data2 = $data2;

                $data3 = DB::table('reagen_imunologi')
                            ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','tb_reagen_imunologi.id')
                            ->select('tb_reagen_imunologi.reagen','reagen_imunologi.metode','reagen_imunologi.reagen_lain')
                            ->where('reagen_imunologi.id_master_imunologi', $r->id)
                            ->get();
                $r->data3 = $data3;
            }
            // dd($data);
            // return view('evaluasi.rekap.laporanhasil.hiv.lain', compact('data'));

            if (Auth::user()->penyelenggara == '6') {
                if (Auth::user()->badan_usaha != '9') {
                    Excel::create('Laporan Hasil Seluruh Peserta', function($excel) use ($data) {
                        $excel->sheet('Data', function($sheet) use ($data) {
                            $sheet->loadView('evaluasi.rekap.laporanhasil.hiv.lain', array('data'=>$data) );
                        });
                    })->download('xls');
                }else{
                Excel::create('Laporan Hasil Seluruh Peserta', function($excel) use ($data) {
                    $excel->sheet('Data', function($sheet) use ($data) {
                        $sheet->loadView('evaluasi.rekap.laporanhasil.hiv.imun', array('data'=>$data) );
                    });
                })->download('xls');
                }
            }else{
                Excel::create('Laporan Hasil Seluruh Peserta', function($excel) use ($data) {
                    $excel->sheet('Data', function($sheet) use ($data) {
                        $sheet->loadView('evaluasi.rekap.laporanhasil.hiv.imun', array('data'=>$data) );
                    });
                })->download('xls');
            }
    }

    public function rekapreagen()
    {
        $data = DB::table('sub_bidang')->where('id', Auth::user()->penyelenggara)->get();
        return view('evaluasi.rekap.reagen.index', compact('data'));
    }
    public function rekapreagenna(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $form = ['0','1','2','3','4','5','Anti HIV','Anti TP','hbsag','hcv'];
        $jenisform = ['0','1','2','3','4','5','Anti Hiv','Syphilis','HBsAg','ANTI-HCV'];
        $bidang = $request->bidang;
        // dd($bidang);
        $siklus = $request->siklus;

                $data = DB::table('reagen_imunologi')
                        ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                        ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                        ->where(function($query){
                            if (Auth::user()->penyelenggara == '6') {
                                if (Auth::user()->badan_usaha == '9') {
                                    $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                                }else{
                                    $query->where('perusahaan.pemerintah','!=','9');
                                }
                            }
                        })
                        ->where('tb_registrasi.bidang', Auth::user()->penyelenggara)
                        ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $tahun)
                        ->where('master_imunologi.siklus', $siklus)
                        ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                        ->groupBy('tb_reagen_imunologi.id')
                        ->orderBy('jumlah', 'desc')
                        ->get();
                // dd($data);
        if ($bidang == "7") {
        return view('evaluasi.rekap.reagen.hiv.rpr', compact('data'));
        }else{
        return view('evaluasi.rekap.reagen.hiv.index', compact('data'));
        }
    }

    public function rekapreagenpersen(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $form = ['0','1','2','3','4','5','Anti HIV','Anti TP','hbsag','hcv'];
        $jenisform = ['0','1','2','3','4','5','Anti Hiv','Syphilis','HBsAg','ANTI-HCV'];
        $bidang = $request->bidang;
        $siklus = $request->siklus;


        $data = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query){
                        if (Auth::user()->penyelenggara == '6') {
                            if (Auth::user()->badan_usaha == '9') {
                                $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        }
                    })
                    ->where('tb_registrasi.bidang', Auth::user()->penyelenggara)
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $tahun)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->groupBy('tb_reagen_imunologi.id')
                    ->orderBy('jumlah', 'desc')
                    ->get();


        $total = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query){
                        if (Auth::user()->penyelenggara == '6') {
                            if (Auth::user()->badan_usaha == '9') {
                                $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        }
                    })
                    ->where('tb_registrasi.bidang', Auth::user()->penyelenggara)
                    ->where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $tahun)
                    ->where('master_imunologi.siklus', $siklus)
                    ->select(DB::raw('COUNT(nama_reagen) as jumlah'), 'tb_reagen_imunologi.reagen','master_imunologi.jenis_form')
                    ->count();

        // dd($data);
        $bidang = $request->bidang;
        if ($bidang == "7") {
            return view('evaluasi.rekap.reagen.hiv.rpr-persen', compact('data','total'));
        }else{
            return view('evaluasi.rekap.reagen.hiv.persen', compact('data','total'));
        }
    }


    public function grafikinstansi()
    {
        $data = DB::table('sub_bidang')->where('id', Auth::user()->penyelenggara)->get();
        return view('evaluasi.rekap.grafik.kirimhasil.index', compact('data'));
    }
    public function grafikinstansina(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $form = ['0','1','2','3','4','5','Anti HIV','Anti TP','hbsag','hcv'];
        $jenisform = ['0','1','2','3','4','5','Anti Hiv','Syphilis','HBsAg','ANTI-HCV'];
        $bidang = $request->bidang;
        $siklus = $request->siklus;
        $sub = DB::table('sub_bidang')->where('id', $bidang)->first();

        $data = DB::table('badan_usaha')
                ->where(function($query){
                    if (Auth::user()->penyelenggara == '6') {
                        if (Auth::user()->badan_usaha == '9') {
                            $query->where('id',Auth::user()->badan_usaha);
                        }else{
                            $query->where('id','!=','9');
                        }
                    }
                })
                ->get();
        foreach ($data as $key => $val) {

                 $peserta = DB::table('perusahaan')
                            ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->where('perusahaan.pemerintah',$val->id)
                            ->where('tb_registrasi.bidang', $bidang)
                            ->where('tb_registrasi.status','=', '3')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->count();

                if ($siklus == '1') {
                     $kirimhasil = DB::table('perusahaan')
                                ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                                ->where('perusahaan.pemerintah',$val->id)
                                ->where('tb_registrasi.bidang', $bidang)
                                ->where('tb_registrasi.status','=', '3')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                                ->where('tb_registrasi.siklus_1', '=', 'done')
                                ->count();
                    $tidakkirimhasil = DB::table('perusahaan')
                                ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                                ->where('perusahaan.pemerintah',$val->id)
                                ->where('tb_registrasi.bidang', $bidang)
                                ->where('tb_registrasi.status','=', '3')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                                ->where('tb_registrasi.siklus_1', '=', NULL)
                                ->count();
                }else{
                    $kirimhasil = DB::table('perusahaan')
                                ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                                ->where('perusahaan.pemerintah',$val->id)
                                ->where('tb_registrasi.bidang', $bidang)
                                ->where('tb_registrasi.status','=', '3')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                                ->where('tb_registrasi.siklus_2', '=', 'done')
                                ->count();
                    $tidakkirimhasil = DB::table('perusahaan')
                                ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                                ->where('perusahaan.pemerintah',$val->id)
                                ->where('tb_registrasi.bidang', $bidang)
                                ->where('tb_registrasi.status','=', '3')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                                ->where('tb_registrasi.siklus_2', '=', NULL)
                                ->count();
                }
                $val->kirimhasil = $kirimhasil;
                $val->tidakkirimhasil = $tidakkirimhasil;
                $val->peserta = $peserta;
            }
                // dd($data);
        return view('evaluasi.rekap.grafik.kirimhasil.hiv.index', compact('data','sub'));
    }

    public function rujukaninstansi()
    {
        $data = DB::table('sub_bidang')->where('id', Auth::user()->penyelenggara)->get();
        return view('evaluasi.rekap.grafik.rujukaninstansi.index', compact('data'));
    }
    public function rujukaninstansina(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $form = ['0','1','2','3','4','5','hiv','Syphilis','HBsAg','ANTI-HCV'];
        $bidang = $request->bidang;
        $siklus = $request->siklus;
        $sub = DB::table('sub_bidang')
                ->where('id', $bidang)
                ->first();
        $data = DB::table('badan_usaha')
                ->where(function($query){
                    if (Auth::user()->penyelenggara == '6') {
                        if (Auth::user()->badan_usaha == '9') {
                            $query->where('id',Auth::user()->badan_usaha);
                        }else{
                            $query->where('id','!=','9');
                        }
                    }
                })
                ->get();
        // dd($sub);
        foreach ($data as $key => $val) {
            if ($bidang == '7') {
                    $baik = DB::table('tb_kesimpulan_evaluasi')
                                ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                                ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                                ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $tahun)
                                ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                                ->where('tb_kesimpulan_evaluasi.type','=', 'Syphilis')
                                ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                                ->where('tb_registrasi.bidang', '=', '7')
                                ->where('perusahaan.pemerintah',$val->id)
                                ->count();
                    $tidakbaik = DB::table('tb_kesimpulan_evaluasi')
                                ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                                ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                                ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $tahun)
                                ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                                ->where('tb_kesimpulan_evaluasi.type','=', 'Syphilis')
                                ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                                ->where('tb_registrasi.bidang', '=', '7')
                                ->where('perusahaan.pemerintah',$val->id)
                                ->count();

                    $baikrpr = DB::table('tb_kesimpulan_evaluasi')
                                ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                                ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                                ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $tahun)
                                ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                                ->where('tb_kesimpulan_evaluasi.type','=', 'rpr-syphilis')
                                ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                                ->where('tb_registrasi.bidang', '=', '7')
                                ->where('perusahaan.pemerintah',$val->id)
                                ->count();
                    $tidakbaikrpr = DB::table('tb_kesimpulan_evaluasi')
                                ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                                ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                                ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $tahun)
                                ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                                ->where('tb_kesimpulan_evaluasi.type','=', 'rpr-syphilis')
                                ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                                ->where('tb_registrasi.bidang', '=', '7')
                                ->where('perusahaan.pemerintah',$val->id)
                                ->count();

                    $val->baikrpr = $baikrpr;
                    $val->tidakbaikrpr = $tidakbaikrpr;
            }else{
                $baik = DB::table('tb_kesimpulan_evaluasi')
                            ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $tahun)
                            ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                            ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                            ->where('tb_registrasi.bidang', Auth::user()->penyelenggara)
                            ->where('perusahaan.pemerintah', $val->id)
                            ->count();
                $tidakbaik = DB::table('tb_kesimpulan_evaluasi')
                            ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $tahun)
                            ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                            ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                            ->where('tb_registrasi.bidang', Auth::user()->penyelenggara)
                            ->where('perusahaan.pemerintah', $val->id)
                            ->count();
            }
            $val->baik = $baik;
            $val->tidakbaik = $tidakbaik;
        }
        // dd($data);
        return view('evaluasi.rekap.grafik.rujukaninstansi.hiv.index', compact('data','sub'));
    }

    public function rujukanreagen()
    {
        $data = DB::table('sub_bidang')->where('id', Auth::user()->penyelenggara)->get();
        return view('evaluasi.rekap.grafik.rujukanreagen.index', compact('data'));
    }
    public function rujukanreagenna(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $form = ['0','1','2','3','4','5','hiv','tp','hbsag','hcv'];
        $bidang = $request->bidang;
        $siklus = $request->siklus;
        $sub = DB::table('sub_bidang')->where('id', $bidang)->first();

        $data = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                    ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query){
                        if (Auth::user()->penyelenggara == '6') {
                            if (Auth::user()->badan_usaha == '9') {
                                $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        }
                    })
                    ->where('tb_registrasi.bidang', $bidang)
                    ->where('master_imunologi.siklus', $siklus)
                    ->groupBy('reagen_imunologi.nama_reagen', 'master_imunologi.jenis_form')
                    ->select('tb_reagen_imunologi.reagen','reagen_imunologi.nama_reagen', 'master_imunologi.jenis_form')
                    ->get();

        foreach ($data as $key => $val) {
            $total = DB::table('reagen_imunologi')
                        ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_registrasi.id','=','tb_kesimpulan_evaluasi.id_registrasi')
                        ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                        ->where(function($query){
                            if (Auth::user()->penyelenggara == '6') {
                                if (Auth::user()->badan_usaha == '9') {
                                    $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                                }else{
                                    $query->where('perusahaan.pemerintah','!=','9');
                                }
                            }
                        })
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->total = $total;
            $sesuai = DB::table('reagen_imunologi')
                        ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_registrasi.id','=','tb_kesimpulan_evaluasi.id_registrasi')
                        ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                        ->where(function($query){
                            if (Auth::user()->penyelenggara == '6') {
                                if (Auth::user()->badan_usaha == '9') {
                                    $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                                }else{
                                    $query->where('perusahaan.pemerintah','!=','9');
                                }
                            }
                        })
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Baik')
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->sesuai = $sesuai;
            $tidaksesuai = DB::table('reagen_imunologi')
                        ->join('master_imunologi','reagen_imunologi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->join('tb_kesimpulan_evaluasi','tb_registrasi.id','=','tb_kesimpulan_evaluasi.id_registrasi')
                        ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                        ->where(function($query){
                            if (Auth::user()->penyelenggara == '6') {
                                if (Auth::user()->badan_usaha == '9') {
                                    $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                                }else{
                                    $query->where('perusahaan.pemerintah','!=','9');
                                }
                            }
                        })
                        ->where('reagen_imunologi.nama_reagen', $val->nama_reagen)
                        ->where('tb_kesimpulan_evaluasi.ketepatan', '=', 'Kurang')
                        ->groupBy('reagen_imunologi.nama_reagen')
                        ->count();
            $val->tidaksesuai = $tidaksesuai;
        }
        // dd($data);
        return view('evaluasi.rekap.grafik.rujukanreagen.hiv.index', compact('data','sub'));
    }


    public function rujukanbahanuji()
    {
        $data = DB::table('sub_bidang')->where('id', '=', '6')->get();
        return view('evaluasi.rekap.grafik.rujukanbahanuji.index', compact('data'));
    }
    public function rujukanbahanujina(\Illuminate\Http\Request $request)
    {
        $tahun = date('Y');
        $form = ['0','1','2','3','4','5','hiv','tp','hbsag','hcv'];
        $bidang = $request->bidang;
        $siklus = $request->siklus;
        $sub = DB::table('sub_bidang')->where('id', $bidang)->first();

        $data = DB::table('tb_strategi_evaluasi')
                    ->join('master_imunologi','tb_strategi_evaluasi.id_master_imunologi','=','master_imunologi.id')
                    ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang', $bidang)
                    ->where('master_imunologi.siklus', $siklus)
                    ->where(function($query){
                              if (Auth::user()->badan_usaha == '9') {
                                  $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                              }else{
                                  $query->where('perusahaan.pemerintah','!=','9');
                              }
                        })
                    ->groupBy('tb_strategi_evaluasi.kode_bahan')
                    ->select('tb_strategi_evaluasi.kode_bahan')
                    ->get();

        foreach ($data as $key => $val) {
            $sesuai = DB::table('tb_strategi_evaluasi')
                        ->join('master_imunologi','tb_strategi_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_registrasi.bidang', $bidang)
                        ->where(function($query){
                                if (Auth::user()->badan_usaha == '9') {
                                    $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                                }else{
                                    $query->where('perusahaan.pemerintah','!=','9');
                                }
                        })
                        ->where('tb_strategi_evaluasi.kode_bahan', $val->kode_bahan)
                        ->where('tb_strategi_evaluasi.kategori', '=' , 'Sesuai')
                        ->count();
            $val->sesuai = $sesuai;
            $tidaksesuai = DB::table('tb_strategi_evaluasi')
                        ->join('master_imunologi','tb_strategi_evaluasi.id_master_imunologi','=','master_imunologi.id')
                        ->join('tb_registrasi','master_imunologi.id_registrasi','=','tb_registrasi.id')
                        ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                        ->where(function($query){
                                if (Auth::user()->badan_usaha == '9') {
                                    $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                                }else{
                                    $query->where('perusahaan.pemerintah','!=','9');
                                }
                        })
                        ->where('tb_registrasi.bidang', $bidang)
                        ->where('master_imunologi.siklus', $siklus)
                        ->where('tb_strategi_evaluasi.kode_bahan', $val->kode_bahan)
                        ->where('tb_strategi_evaluasi.kategori', '=' , 'Tidak Sesuai')
                        ->count();
            $val->tidaksesuai = $tidaksesuai;
        }
        // dd($data);
        return view('evaluasi.rekap.grafik.rujukanbahanuji.hiv.index', compact('data','sub'));
    }


    public function nilaipeserta(\Illuminate\Http\Request $request)
    {
        $data = DB::table('sub_bidang')->where('id', Auth::user()->penyelenggara)->get();
        return view('evaluasi.rekap.grafik.nilaipeserta.index', compact('data'));
    }
    public function nilaipesertana(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $form = ['0','1','2','3','4','5','Anti Hiv','Syphilis','HBsAg','ANTI-HCV'];
        $formrpr = ['0','1','2','3','4','5','Anti Hiv','rpr-syphilis','HBsAg','ANTI-HCV'];
        // dd($form);
        $bidang = $request->bidang;
        // dd($bidang);
        $siklus = $request->siklus;
        $sub = DB::table('sub_bidang')->where('id', $bidang)->first();

        $baik = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query){
                        if (Auth::user()->penyelenggara == '6') {
                            if (Auth::user()->badan_usaha == '9') {
                                $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        }
                    })
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $tahun)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_kesimpulan_evaluasi.type','=', $form[$bidang])
                    ->where('tb_registrasi.bidang', Auth::user()->penyelenggara)
                    ->count();
        // dd($baik);

        $baikrpr = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query){
                        if (Auth::user()->penyelenggara == '6') {
                            if (Auth::user()->badan_usaha == '9') {
                                $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        }
                    })
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $tahun)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Baik')
                    ->where('tb_kesimpulan_evaluasi.type','=', $formrpr[$bidang])
                    ->where('tb_registrasi.bidang', Auth::user()->penyelenggara)
                    ->count();
        // dd($baik);
        $tidakbaikrpr = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query){
                        if (Auth::user()->penyelenggara == '6') {
                            if (Auth::user()->badan_usaha == '9') {
                                $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        }
                    })
                    ->where('tb_registrasi.bidang', Auth::user()->penyelenggara)
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $tahun)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->where('tb_kesimpulan_evaluasi.type', $formrpr[$bidang])
                    ->count();

        $tidakdapatnilairpr = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query){
                        if (Auth::user()->penyelenggara == '6') {
                            if (Auth::user()->badan_usaha == '9') {
                                $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        }
                    })
                    ->where('tb_registrasi.bidang', Auth::user()->penyelenggara)
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $tahun)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->where('tb_kesimpulan_evaluasi.type', $formrpr[$bidang])
                    ->count();

        $tidakbaik = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $tahun)
                    ->where(function($query){
                        if (Auth::user()->penyelenggara == '6') {
                            if (Auth::user()->badan_usaha == '9') {
                                $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        }
                    })
                    ->where('tb_registrasi.bidang', Auth::user()->penyelenggara)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Kurang')
                    ->where('tb_kesimpulan_evaluasi.type', $form[$bidang])
                    ->count();


        $tidakdapatnilai = DB::table('tb_kesimpulan_evaluasi')
                    ->join('tb_registrasi','tb_kesimpulan_evaluasi.id_registrasi','=','tb_registrasi.id')
                    ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where(function($query){
                        if (Auth::user()->penyelenggara == '6') {
                            if (Auth::user()->badan_usaha == '9') {
                                $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                            }else{
                                $query->where('perusahaan.pemerintah','!=','9');
                            }
                        }
                    })
                    ->where('tb_registrasi.bidang', Auth::user()->penyelenggara)
                    ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $tahun)
                    ->where('tb_kesimpulan_evaluasi.siklus',$siklus)
                    ->where('tb_kesimpulan_evaluasi.ketepatan','=','Tidak dapat dinilai')
                    ->where('tb_kesimpulan_evaluasi.type', $form[$bidang])
                    ->count();
        // dd($tidakbaik);
        return view('evaluasi.rekap.grafik.nilaipeserta.hiv.index', compact('baik','tidakbaik','baikrpr','tidakbaikrpr','sub','bidang','siklus','tidakdapatnilai','tidakdapatnilairpr'));
    }

    public function rekapevaluasi()
    {
        $data = DB::table('sub_bidang')->where('id', Auth::user()->penyelenggara)->get();
        return view('evaluasi.imunologi.rekapevaluasi.index', compact('data'));
    }

    public function rekapevaluasina(\Illuminate\Http\Request $request){
        if (Auth::user()->badan_usaha == "9") {
            $rujukan = ['','','','','','','HIV PMI','Anti TP','HBsAg','Anti HCV'];
            $kode = ['','','','','','','HV','SI','HB','HC'];
        }else{
            $kode = ['','','','','','','HV','SI','HB','HC'];
            $rujukan = ['','','','','','', 'Anti HIV', 'Anti TP','HBsAg','Anti HCV'];
        }

        $input = $request->all();
        $bidang = $rujukan[$request->bidang];
        $kodebidang = $kode[$request->bidang];
        $rujuk = DB::table('tb_rujukan_imunologi')
                ->where('parameter', $bidang)
                ->where('siklus', $input['siklus'])
                ->where('tahun', $input['tahun'])
                ->get();
        if (count($rujuk)) {
            if($request->bidang == 6){
                    $dataBiasa = DB::table('tb_registrasi')
                                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                                    ->join('master_imunologi', 'master_imunologi.id_registrasi', '=', 'tb_registrasi.id')
                                    ->leftjoin('tb_kesimpulan_evaluasi', 'tb_kesimpulan_evaluasi.id_master_imunologi', '=', 'master_imunologi.id')
                                    ->where('bidang', $input['bidang'])
                                    ->where('master_imunologi.siklus', $input['siklus'])
                                    ->where('perusahaan.pemerintah','!=',9)
                                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                                    ->where(function($query)
                                        {
                                            $query->where('status_data1', 2)
                                                ->orwhere('status_data2', 2)
                                                ->orwhere('status_datarpr1', 2)
                                                ->orwhere('status_datarpr2', 2);
                                        })
                                    ->where(function($query)
                                        {
                                            $query->where('pemeriksaan2', '=' , 'done')
                                                    ->orwhere('pemeriksaan', '=' , 'done')
                                                    ->orwhere('siklus_1', '=' , 'done')
                                                    ->orwhere('siklus_2', '=' , 'done');
                                        })
                                    ->select('tb_registrasi.id', 'tb_registrasi.kode_lebpes','perusahaan.nama_lab', 'perusahaan.pemerintah', 'master_imunologi.id as id_master_imunologi', 'master_imunologi.jenis_form', 'tb_kesimpulan_evaluasi.ketepatan', 'tb_kesimpulan_evaluasi.kesesuaian')
                                    ->get();
                foreach ($dataBiasa as $key => $val) {
                    $reagen = DB::table('reagen_imunologi')
                        ->leftjoin('tb_reagen_imunologi', 'tb_reagen_imunologi.id', '=', 'reagen_imunologi.nama_reagen')
                        ->where('id_master_imunologi','=', $val->id_master_imunologi)
                        ->get();
                    $val->reagen = $reagen;
                    $saran = DB::table('tb_saran_evaluasi')
                            ->where('id_registrasi', $val->id)
                            ->where('siklus', $input['siklus'])
                            ->first();
                    $val->saran = $saran;
                    $strategi = DB::table('hp_imunologi')
                        ->leftjoin('tb_rujukan_imunologi', 'tb_rujukan_imunologi.kode_bahan_uji', '=', 'hp_imunologi.kode_bahan_kontrol')
                        ->where('hp_imunologi.id_master_imunologi','=', $val->id_master_imunologi)
                        ->where('tb_rujukan_imunologi.tahun',$input['tahun'])
                        ->where('tb_rujukan_imunologi.siklus',$input['siklus'])
                        ->where('tb_rujukan_imunologi.parameter','Anti HIV')
                        ->orderByRaw("tabung asc, id")
                        ->select('hp_imunologi.*', 'tb_rujukan_imunologi.nilai_rujukan')
                        ->get();
                    $val->strategi = $strategi;
                }

                $dataPMI = DB::table('tb_registrasi')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->join('master_imunologi', 'master_imunologi.id_registrasi', '=', 'tb_registrasi.id')
                    ->join('tb_kesimpulan_evaluasi', 'tb_kesimpulan_evaluasi.id_master_imunologi', '=', 'master_imunologi.id')
                    ->where('bidang', $input['bidang'])
                    ->where('master_imunologi.siklus', $input['siklus'])
                    ->where('perusahaan.pemerintah','=',9)
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                    ->where(function($query)
                        {
                            $query->where('status_data1', 2)
                                ->orwhere('status_data2', 2)
                                ->orwhere('status_datarpr1', 2)
                                ->orwhere('status_datarpr2', 2);
                        })
                    ->where(function($query)
                        {
                            $query->where('pemeriksaan2', '=' , 'done')
                                    ->orwhere('pemeriksaan', '=' , 'done')
                                    ->orwhere('siklus_1', '=' , 'done')
                                    ->orwhere('siklus_2', '=' , 'done');
                        })
                    ->select('tb_registrasi.id', 'tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'perusahaan.pemerintah', 'master_imunologi.id as id_master_imunologi', 'master_imunologi.jenis_form', 'tb_kesimpulan_evaluasi.ketepatan', 'tb_kesimpulan_evaluasi.kesesuaian')
                    ->get();
                foreach ($dataPMI as $key => $val) {
                    $reagen = DB::table('reagen_imunologi')
                        ->leftjoin('tb_reagen_imunologi', 'tb_reagen_imunologi.id', '=', 'reagen_imunologi.nama_reagen')
                        ->where('id_master_imunologi','=', $val->id_master_imunologi)
                        ->get();
                    $val->reagen = $reagen;
                    $saran = DB::table('tb_saran_evaluasi')
                            ->where('id_registrasi', $val->id)
                            ->where('siklus', $input['siklus'])
                            ->first();
                    $val->saran = $saran;
                    $strategi = DB::table('hp_imunologi')
                        ->join('tb_rujukan_imunologi', 'tb_rujukan_imunologi.kode_bahan_uji','=','hp_imunologi.kode_bahan_kontrol')
                        ->where('hp_imunologi.id_master_imunologi','=', $val->id_master_imunologi)
                        ->where('tb_rujukan_imunologi.siklus', $input['siklus'])
                        ->where('tb_rujukan_imunologi.parameter', '=', 'HIV PMI')
                        ->where('tb_rujukan_imunologi.tahun', '=', $input['tahun'])
                        ->orderBy('hp_imunologi.kode_bahan_kontrol', 'asc')
                        ->get();
                    $val->strategi = $strategi;

                }

                Excel::create('REKAP HASIL EVALUASI '.$bidang.'', function($excel) use ($dataBiasa,$dataPMI) {
                    if(Auth::user()->badan_usaha == '9'){
                        $excel->sheet('PMI', function($sheet) use ($dataPMI) {
                            if (count($dataPMI)) {
                                $sheet->loadView('evaluasi.imunologi.rekapevaluasi.view_imun', array('data'=>$dataPMI,'type'=>1) );
                            }
                        });
                    }else{
                        $excel->sheet('Umum', function($sheet) use ($dataBiasa) {
                            if (count($dataBiasa)) {
                                $sheet->loadView('evaluasi.imunologi.rekapevaluasi.view_imun', array('data'=>$dataBiasa,'type'=>0) );
                            }
                        });
                    }
                })->download('xls');
            }elseif ($request->bidang == 7) {
                $datarpr = DB::table('tb_registrasi')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->where('bidang', $input['bidang'])
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                    ->where(function($query)
                        {
                            $query->where('status_data1', 2)
                                ->orwhere('status_data2', 2)
                                ->orwhere('status_datarpr1', 2)
                                ->orwhere('status_datarpr2', 2);
                        })
                    ->where(function($query)
                        {
                            $query->where('pemeriksaan2', '=' , 'done')
                                    ->orwhere('pemeriksaan', '=' , 'done')
                                    ->orwhere('siklus_1', '=' , 'done')
                                    ->orwhere('siklus_2', '=' , 'done');
                        })
                    // ->where('tb_registrasi.kode_lebpes','0103/SIF/12/18')
                    ->select('tb_registrasi.id', 'tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'perusahaan.pemerintah')
                    ->get();
                // dd($datarpr);
                foreach ($datarpr as $key => $val) {
                    $kesimpulan = DB::table('tb_kesimpulan_evaluasi')
                                ->where('id_registrasi','=', $val->id)
                                ->where('siklus', $input['siklus'])
                                ->where('tb_kesimpulan_evaluasi.type','=', 'rpr-syphilis')
                                ->get();
                    $val->kesimpulan = $kesimpulan;
                    $reagen = DB::table('reagen_imunologi')
                                ->join('master_imunologi', 'master_imunologi.id', '=', 'reagen_imunologi.id_master_imunologi')
                                ->join('tb_reagen_imunologi', 'tb_reagen_imunologi.id', '=', 'reagen_imunologi.nama_reagen')
                                ->where('master_imunologi.id_registrasi','=', $val->id)
                                ->where('master_imunologi.siklus', $input['siklus'])
                                ->where('master_imunologi.jenis_form','=', 'rpr-syphilis')
                                ->get();
                    $val->reagen = $reagen;
                    $strategi = DB::table('hp_imunologi')
                                ->join('tb_rujukan_imunologi', 'hp_imunologi.kode_bahan_kontrol', '=', 'tb_rujukan_imunologi.kode_bahan_uji')
                                ->join('master_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                                ->where('tb_rujukan_imunologi.siklus', $input['siklus'])
                                ->where('tb_rujukan_imunologi.tahun', $input['tahun'])
                                ->where('tb_rujukan_imunologi.parameter', '=','RPR')
                                ->where('master_imunologi.id_registrasi','=', $val->id)
                                ->where('master_imunologi.siklus', $input['siklus'])
                                ->where('master_imunologi.jenis_form','=', 'rpr-syphilis')
                                ->get();
                    $val->strategi = $strategi;

                    $catatan = CatatanImun::where('id_registrasi', $val->id)->where('siklus', $input['siklus'])->where('form', '=', 'RPR')->where('tahun', $input['tahun'])->first();
                    $val->catatan = $catatan;

                }
                // dd($datarpr[0]->catatan->catatan);
                $datatp = DB::table('tb_registrasi')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->where('bidang', $input['bidang'])
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                    ->where(function($query)
                        {
                            $query->where('status_data1', 2)
                                ->orwhere('status_data2', 2)
                                ->orwhere('status_datarpr1', 2)
                                ->orwhere('status_datarpr2', 2);
                        })
                    ->where(function($query)
                        {
                            $query->where('pemeriksaan2', '=' , 'done')
                                    ->orwhere('pemeriksaan', '=' , 'done')
                                    ->orwhere('siklus_1', '=' , 'done')
                                    ->orwhere('siklus_2', '=' , 'done');
                        })
                    ->select('tb_registrasi.id', 'tb_registrasi.kode_lebpes','perusahaan.nama_lab', 'perusahaan.pemerintah')
                    ->get();
                foreach ($datatp as $key => $val) {
                    $kesimpulan = DB::table('tb_kesimpulan_evaluasi')
                                ->where('id_registrasi','=', $val->id)
                                ->where('siklus', $input['siklus'])
                                ->where('tb_kesimpulan_evaluasi.type','=', 'Syphilis')
                                ->get();
                    $val->kesimpulan = $kesimpulan;
                    $reagen = DB::table('reagen_imunologi')
                                ->join('master_imunologi', 'master_imunologi.id', '=', 'reagen_imunologi.id_master_imunologi')
                                ->join('tb_reagen_imunologi', 'tb_reagen_imunologi.id', '=', 'reagen_imunologi.nama_reagen')
                                ->where('master_imunologi.id_registrasi','=', $val->id)
                                ->where('master_imunologi.siklus', $input['siklus'])
                                ->where('master_imunologi.jenis_form','=', 'Syphilis')
                                ->get();
                    $val->reagen = $reagen;
                    $strategi = DB::table('hp_imunologi')
                                ->join('tb_rujukan_imunologi', 'hp_imunologi.kode_bahan_kontrol', '=', 'tb_rujukan_imunologi.kode_bahan_uji')
                                ->join('master_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                                ->where('tb_rujukan_imunologi.siklus', $input['siklus'])
                                ->where('tb_rujukan_imunologi.tahun', $input['tahun'])
                                ->where('tb_rujukan_imunologi.parameter', '=','RPR')
                                ->where('master_imunologi.id_registrasi','=', $val->id)
                                ->where('master_imunologi.siklus', $input['siklus'])
                                ->where('master_imunologi.jenis_form','=', 'Syphilis')
                                ->get();
                    $val->strategi = $strategi;

                    $catatan = CatatanImun::where('id_registrasi', $val->id)->where('siklus', $input['siklus'])->where('form', '=', 'TP')->where('tahun', $input['tahun'])->first();
                    $val->catatan = $catatan;

                }
                // return view('evaluasi.imunologi.rekapevaluasi.view', array('data'=>$datarpr, 'type'=>1));
                // dd($datarpr[0]->strategi);
                Excel::create('REKAP HASIL EVALUASI SYPHILIS', function($excel) use ($datatp,$datarpr, $kodebidang) {
                    $excel->sheet('TP', function($sheet) use ($datatp, $kodebidang) {
                        if (count($datatp)) {
                            $sheet->loadView('evaluasi.imunologi.rekapevaluasi.view', array('data'=>$datatp,'type'=>0, 'kode'=>$kodebidang) );
                        }
                    });
                    $excel->sheet('RPR', function($sheet) use ($datarpr, $kodebidang) {
                        if (count($datarpr)) {
                            $sheet->loadView('evaluasi.imunologi.rekapevaluasi.view', array('data'=>$datarpr,'type'=>1, 'kode'=>$kodebidang) );
                        }
                    });
                })->download('xls');
            }else{
                $data = DB::table('tb_registrasi')
                    ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                    ->where('bidang', $input['bidang'])
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                    ->where(function($query)
                        {
                            $query->where('status_data1', 2)
                                ->orwhere('status_data2', 2)
                                ->orwhere('status_datarpr1', 2)
                                ->orwhere('status_datarpr2', 2);
                        })
                    ->where(function($query)
                        {
                            $query->where('pemeriksaan2', '=' , 'done')
                                    ->orwhere('pemeriksaan', '=' , 'done')
                                    ->orwhere('siklus_1', '=' , 'done')
                                    ->orwhere('siklus_2', '=' , 'done');
                        })
                    ->select('tb_registrasi.id', 'tb_registrasi.kode_lebpes','perusahaan.nama_lab', 'perusahaan.pemerintah')
                    ->get();
                // dd($data);
                foreach ($data as $key => $val) {
                    $kesimpulan = DB::table('tb_kesimpulan_evaluasi')
                                ->join('master_imunologi', 'master_imunologi.id', '=', 'tb_kesimpulan_evaluasi.id_master_imunologi')
                                ->where('master_imunologi.id_registrasi','=', $val->id)
                                ->where('master_imunologi.siklus', $input['siklus'])
                                ->get();
                    $val->kesimpulan = $kesimpulan;
                    $reagen = DB::table('reagen_imunologi')
                                ->join('master_imunologi', 'master_imunologi.id', '=', 'reagen_imunologi.id_master_imunologi')
                                ->join('tb_reagen_imunologi', 'tb_reagen_imunologi.id', '=', 'reagen_imunologi.nama_reagen')
                                ->where('master_imunologi.id_registrasi','=', $val->id)
                                ->where('master_imunologi.siklus', $input['siklus'])
                                ->get();
                    $val->reagen = $reagen;
                    $strategi = DB::table('hp_imunologi')
                                ->join('tb_rujukan_imunologi', 'hp_imunologi.kode_bahan_kontrol', '=', 'tb_rujukan_imunologi.kode_bahan_uji')
                                ->join('master_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                                ->where('tb_rujukan_imunologi.siklus', $input['siklus'])
                                ->where('tb_rujukan_imunologi.tahun', $input['tahun'])
                                ->where('tb_rujukan_imunologi.parameter', $bidang)
                                ->where('master_imunologi.id_registrasi','=', $val->id)
                                ->where('master_imunologi.siklus', $input['siklus'])
                                ->get();
                    $val->strategi = $strategi;

                    $catatan = CatatanImun::where('id_registrasi', $val->id)->where('siklus', $input['siklus'])->where('tahun', $input['tahun'])->first();
                    $val->catatan = $catatan;

                }
                // dd($data);
                Excel::create('REKAP HASIL EVALUASI '.$bidang.'', function($excel) use ($data,$kodebidang) {
                    $excel->sheet('Data', function($sheet) use ($data,$kodebidang) {
                        $sheet->loadView('evaluasi.imunologi.rekapevaluasi.view', array('data'=>$data, 'type'=>0, 'kode'=>$kodebidang));
                    });
                })->download('xls');
            }

            Session::flash('message', '- Data sukses di export!');
            Session::flash('alert-class', 'alert-success');
            return redirect::back();
        }else{
            Session::flash('message', '- Harap isi data Rujukan terlebih dahulu! <br>- Harap Evaluasi Peserta sesuai Bidang!');
            Session::flash('alert-class', 'alert-danger');
            return redirect::back();
        }
    }

    public function rekappesertabelum(\Illuminate\Http\Request $request){
        if (Auth::user()->badan_usaha == "9") {
            $rujukan = ['','','','','','','HIV PMI','Anti TP','HBsAg','Anti HCV'];
            $kode = ['','','','','','','HV','SI','HB','HC'];
        }else{
            $kode = ['','','','','','','HV','SI','HB','HC'];
            $rujukan = ['','','','','','', 'Anti HIV', 'Anti TP','HBsAg','Anti HCV'];
        }
        $input = $request->all();
        $bidang = $rujukan[$request->bidang];
        $kodebidang = $kode[$request->bidang];
        // dd($kodebidang);
        if($request->bidang == 6){
            $dataBiasa = DB::table('tb_registrasi')
                ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                ->where('bidang', $input['bidang'])
                ->where('perusahaan.pemerintah','!=',9)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                // ->where(function($query)
                //     {
                //         $query->where('status_data1', '>=', 1)
                //             ->orwhere('status_data2', '>=', 1)
                //             ->orwhere('status_datarpr1', '>=', 1)
                //             ->orwhere('status_datarpr2', '>=', 1);
                //     })
                // ->where(function($query)
                //     {
                //         $query->where('pemeriksaan2', '=' , 'done')
                //                 ->orwhere('pemeriksaan', '=' , 'done')
                //                 ->orwhere('siklus_1', '=' , 'done')
                //                 ->orwhere('siklus_2', '=' , 'done');
                //     })
                ->select('tb_registrasi.id', 'tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'perusahaan.pemerintah')
                ->get();
            foreach ($dataBiasa as $key => $val) {
                $reagen = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi', 'tb_reagen_imunologi.id', '=', 'reagen_imunologi.nama_reagen')
                    ->join('master_imunologi', 'master_imunologi.id', '=', 'reagen_imunologi.id_master_imunologi')
                    ->where('master_imunologi.id_registrasi','=', $val->id)
                    ->where('master_imunologi.siklus', $input['siklus'])
                    ->get();
                $val->reagen = $reagen;

                $hpimun = DB::table('hp_imunologi')
                    ->join('master_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                    ->where('master_imunologi.id_registrasi','=', $val->id)
                    ->where('master_imunologi.siklus', $input['siklus'])
                    ->orderBy('hp_imunologi.kode_bahan_kontrol', 'asc')
                    ->get();
                $val->hpimun = $hpimun;
            }
            // dd($dataBiasa[0]);
            $dataPMI = DB::table('tb_registrasi')
                ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                ->where('bidang', $input['bidang'])
                ->where('perusahaan.pemerintah','=',9)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                // ->where(function($query)
                //     {
                //         $query->where('status_data1', '>=', 1)
                //             ->orwhere('status_data2', '>=', 1)
                //             ->orwhere('status_datarpr1', '>=', 1)
                //             ->orwhere('status_datarpr2', '>=', 1);
                //     })
                // ->where(function($query)
                //     {
                //         $query->where('pemeriksaan2', '=' , 'done')
                //                 ->orwhere('pemeriksaan', '=' , 'done')
                //                 ->orwhere('siklus_1', '=' , 'done')
                //                 ->orwhere('siklus_2', '=' , 'done');
                //     })
                ->select('tb_registrasi.id', 'tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'perusahaan.pemerintah')
                ->get();
            foreach ($dataPMI as $key => $val) {
                $reagen = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi', 'tb_reagen_imunologi.id', '=', 'reagen_imunologi.nama_reagen')
                    ->join('master_imunologi', 'master_imunologi.id', '=', 'reagen_imunologi.id_master_imunologi')
                    ->where('master_imunologi.id_registrasi','=', $val->id)
                    ->where('master_imunologi.siklus', $input['siklus'])
                    ->get();
                $val->reagen = $reagen;
                $hpimun = DB::table('hp_imunologi')
                    ->join('master_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                    ->where('master_imunologi.id_registrasi','=', $val->id)
                    ->where('master_imunologi.siklus', $input['siklus'])
                    ->orderBy('hp_imunologi.kode_bahan_kontrol', 'asc')
                    ->get();
                $val->hpimun = $hpimun;
            }
            // dd($dataPMI[10]);

            if ($input['proses'] == "Download Excel") {
                Excel::create('REKAP INPUT PESERTA '.$bidang.'', function($excel) use ($dataBiasa,$dataPMI,$kodebidang) {
                    if(Auth::user()->badan_usaha == '9'){
                        $excel->sheet('PMI', function($sheet) use ($dataPMI,$kodebidang) {
                            if (count($dataPMI)) {
                                $sheet->loadView('evaluasi.imunologi.rekappeserta.view_imun', array('data'=>$dataPMI,'type'=>1, 'kode'=>$kodebidang) );
                            }
                        });
                    }else{
                        $excel->sheet('Umum', function($sheet) use ($dataBiasa,$kodebidang) {
                            if (count($dataBiasa)) {
                                $sheet->loadView('evaluasi.imunologi.rekappeserta.view_imun', array('data'=>$dataBiasa,'type'=>0, 'kode'=>$kodebidang) );
                            }
                        });
                    }
                })->download('xls');
            }elseif ($input['proses'] == "View Tabel") {
                if(Auth::user()->badan_usaha == '9'){
                    return view('evaluasi.imunologi.rekappeserta.view_imun', array('data'=>$dataPMI,'type'=>1, 'kode'=>$kodebidang));
                }else{
                    return view('evaluasi.imunologi.rekappeserta.view_imun', array('data'=>$dataBiasa,'type'=>0, 'kode'=>$kodebidang));
                }
            }
        }elseif ($request->bidang == 7) {
            $datarpr = DB::table('tb_registrasi')
                ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                ->where('bidang', $input['bidang'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                // ->where(function($query)
                //     {
                //         $query->where('status_data1', '>=', 1)
                //             ->orwhere('status_data2', '>=', 1)
                //             ->orwhere('status_datarpr1', '>=', 1)
                //             ->orwhere('status_datarpr2', '>=', 1);
                //     })
                // ->where(function($query)
                //     {
                //         $query->where('pemeriksaan2', '=' , 'done')
                //                 ->orwhere('pemeriksaan', '=' , 'done')
                //                 ->orwhere('siklus_1', '=' , 'done')
                //                 ->orwhere('siklus_2', '=' , 'done');
                //     })
                ->select('tb_registrasi.id', 'tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'perusahaan.pemerintah')
                ->get();
            // dd($datarpr);
            foreach ($datarpr as $key => $val) {
                $reagen = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi', 'tb_reagen_imunologi.id', '=', 'reagen_imunologi.nama_reagen')
                    ->join('master_imunologi', 'master_imunologi.id', '=', 'reagen_imunologi.id_master_imunologi')
                    ->where('master_imunologi.siklus', $input['siklus'])
                    ->where('master_imunologi.jenis_form','=', 'rpr-syphilis')
                    ->where('master_imunologi.id_registrasi','=', $val->id)
                    ->get();
                $val->reagen = $reagen;
                $hpimun = DB::table('hp_imunologi')
                    ->join('master_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                    ->where('master_imunologi.siklus', $input['siklus'])
                    ->where('master_imunologi.jenis_form','=', 'rpr-syphilis')
                    ->where('master_imunologi.id_registrasi','=', $val->id)
                    ->orderBy('hp_imunologi.kode_bahan_kontrol', 'asc')
                    ->get();
                $val->hpimun = $hpimun;

            }
            $datatp = DB::table('tb_registrasi')
                ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                ->where('bidang', $input['bidang'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                // ->where(function($query)
                //     {
                //         $query->where('status_data1', '>=', 1)
                //             ->orwhere('status_data2', '>=', 1)
                //             ->orwhere('status_datarpr1', '>=', 1)
                //             ->orwhere('status_datarpr2', '>=', 1);
                //     })
                // ->where(function($query)
                //     {
                //         $query->where('pemeriksaan2', '=' , 'done')
                //                 ->orwhere('pemeriksaan', '=' , 'done')
                //                 ->orwhere('siklus_1', '=' , 'done')
                //                 ->orwhere('siklus_2', '=' , 'done');
                //     })
                ->select('tb_registrasi.id', 'tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'perusahaan.pemerintah')
                ->get();
            // dd($datarpr);
            foreach ($datatp as $key => $val) {
                $reagen = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi', 'tb_reagen_imunologi.id', '=', 'reagen_imunologi.nama_reagen')
                    ->join('master_imunologi', 'master_imunologi.id', '=', 'reagen_imunologi.id_master_imunologi')
                    ->where('master_imunologi.siklus', $input['siklus'])
                    ->where('master_imunologi.jenis_form','=', 'Syphilis')
                    ->where('master_imunologi.id_registrasi','=', $val->id)
                    ->get();
                $val->reagen = $reagen;
                $hpimun = DB::table('hp_imunologi')
                    ->join('master_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                    ->where('master_imunologi.siklus', $input['siklus'])
                    ->where('master_imunologi.jenis_form','=', 'Syphilis')
                    ->where('master_imunologi.id_registrasi','=', $val->id)
                    ->orderBy('hp_imunologi.kode_bahan_kontrol', 'asc')
                    ->get();
                $val->hpimun = $hpimun;

            }
            // return view('evaluasi.imunologi.rekapevaluasi.view', array('data'=>$datarpr, 'type'=>1));
            // dd($datatp[0]);
            if ($input['proses'] == "Download Excel") {
                Excel::create('REKAP INPUT PESERTA SYPHILIS', function($excel) use ($datatp,$datarpr, $kodebidang) {
                    $excel->sheet('TP', function($sheet) use ($datatp, $kodebidang) {
                        if (count($datatp)) {
                            $sheet->loadView('evaluasi.imunologi.rekappeserta.view', array('data'=>$datatp,'type'=>0, 'kode'=>$kodebidang) );
                        }
                    });
                    $excel->sheet('RPR', function($sheet) use ($datarpr, $kodebidang) {
                        if (count($datarpr)) {
                            $sheet->loadView('evaluasi.imunologi.rekappeserta.view', array('data'=>$datarpr,'type'=>1, 'kode'=>$kodebidang) );
                        }
                    });
                })->download('xls');
            }else{
                return view('evaluasi.imunologi.rekappeserta.view', array('data'=>$datatp,'type'=>0, 'kode'=>$kodebidang,'datarpr'=>$datarpr));
            }
        }else{
            $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                ->where('bidang', $input['bidang'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                // ->where(function($query)
                //     {
                //         $query->where('status_data1', '>=', 1)
                //             ->orwhere('status_data2', '>=', 1)
                //             ->orwhere('status_datarpr1', '>=', 1)
                //             ->orwhere('status_datarpr2', '>=', 1);
                //     })
                // ->where(function($query)
                //     {
                //         $query->where('pemeriksaan2', '=' , 'done')
                //                 ->orwhere('pemeriksaan', '=' , 'done')
                //                 ->orwhere('siklus_1', '=' , 'done')
                //                 ->orwhere('siklus_2', '=' , 'done');
                //     })
                ->select('tb_registrasi.id', 'tb_registrasi.kode_lebpes', 'perusahaan.nama_lab', 'perusahaan.pemerintah','tb_registrasi.bidang')
                ->get();
            foreach ($data as $key => $val) {
                $reagen = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi', 'tb_reagen_imunologi.id', '=', 'reagen_imunologi.nama_reagen')
                    ->join('master_imunologi', 'master_imunologi.id', '=', 'reagen_imunologi.id_master_imunologi')
                    ->where('master_imunologi.siklus', $input['siklus'])
                    ->where('master_imunologi.id_registrasi', $val->id)
                    ->get();
                $val->reagen = $reagen;
                $hpimun = DB::table('hp_imunologi')
                    ->join('master_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                    ->where('master_imunologi.siklus', $input['siklus'])
                    ->where('master_imunologi.id_registrasi', $val->id)
                    ->orderBy('hp_imunologi.kode_bahan_kontrol', 'asc')
                    ->get();
                $val->hpimun = $hpimun;
            }
            // dd($data[0]->strategi);ada
            // @if()
            // dd($input);
            if ($input['proses'] == "Download Excel") {
                Excel::create('REKAP INPUT PESERTA '.$bidang.'', function($excel) use ($data, $kodebidang) {
                    $excel->sheet('Data', function($sheet) use ($data, $kodebidang) {
                        $sheet->loadView('evaluasi.imunologi.rekappeserta.view', array('data'=>$data, 'type'=>0, 'kode'=>$kodebidang) );
                    });
                })->download('xls');
            }elseif ($input['proses'] == "View Tabel") {
                $type = 0;
                return view('evaluasi.imunologi.rekappeserta.view', compact('data','type', 'kode'));
            }
        }

        Session::flash('message', '- Data sukses di export!');
        Session::flash('alert-class', 'alert-success');
        return redirect::back();
    }
}

<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Excel;
use Input;
use Session;
use Carbon\Carbon;
use PDF;
use App\Daftar;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Rujukanbakteri;
use App\EvaluasiBakteri;
use App\EvaluasiBakteriSaran;
use App\Statusbakteri;
use App\Antibiotik;
use App\register as Register;
use Illuminate\Support\Facades\Redirect;

class EvaluasiBakteriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function laporanpesertabakteri()
    {
        return view('evaluasi/bakteri/laporanevaluasi/index');
    }

    public function laporanpesertabakteripdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('tb_evaluasi_bakteri')
                ->join('tb_registrasi','tb_registrasi.id','=','tb_evaluasi_bakteri.id_registrasi')
                ->join('users','users.id','=','tb_registrasi.created_by')
                ->join('tb_evaluasi_bakteri_saran','tb_evaluasi_bakteri_saran.id_registrasi','=','tb_registrasi.id')
                ->where('tb_evaluasi_bakteri.siklus', $input['siklus'])
                ->where('tb_evaluasi_bakteri_saran.siklus', $input['siklus'])
                ->where(DB::raw('YEAR(tb_evaluasi_bakteri.created_at)'), '=' , $input['tahun'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->orderBy('users.id_member', 'asc')
                ->get();
        foreach ($data as $key => $val) {
            $datana = DB::table('data_antibiotik')
                        ->join('tb_rujukan', 'tb_rujukan.lembar', '=', 'data_antibiotik.lembar')
                        ->where('tb_rujukan.siklus', $input['siklus'])
                        ->where('tb_rujukan.tahun', $input['tahun'])
                        ->where('tb_rujukan.jenis_pemeriksaan', '=', 'Identifikasi')
                        ->where('id_registrasi', $val->id_registrasi)
                        ->where('data_antibiotik.siklus', $input['siklus'])
                        ->where(DB::raw('YEAR(data_antibiotik.created_at)'), '=' , $input['tahun'])
                        ->select('data_antibiotik.*','tb_rujukan.rujukan')
                        ->get();
            foreach ($datana as $key => $peka) {
                $kepekaan = DB::table('tb_kepekaan')
                                    ->leftjoin('antibiotik', 'tb_kepekaan.id_jenis_antibiotik', '=', 'antibiotik.id')
                                    ->join('data_antibiotik', 'tb_kepekaan.id_antibiotik', '=', 'data_antibiotik.id')
                                    ->select('antibiotik.*','antibiotik.id as id_anti','data_antibiotik.*','tb_kepekaan.*')
                                    ->where('data_antibiotik.id', $peka->id)
                                    ->where('data_antibiotik.siklus', $input['siklus'])
                                    ->where(DB::raw('YEAR(data_antibiotik.created_at)'), '=' , $input['tahun'])
                                    ->where(function ($query) {
                                        $query->where('tb_kepekaan.hasil1','!=','NULL')
                                              ->orWhere('tb_kepekaan.hasil2','!=','NULL');
                                    })
                                    ->get();
                $peka->kepekaan = $kepekaan;
            }
            $val->datana = $datana;

            if ($input['type'] == 'Identifikasi') {
                $total = DB::table('tb_evaluasi_bakteri')
                        ->where('id_registrasi','=', $val->id_registrasi)
                        ->where('siklus', $input['siklus'])
                        ->where(DB::raw('YEAR(created_at)'), '=' , $input['tahun'])
                        ->select(DB::raw('SUM(nilai_1) as total'))
                        ->groupBy('id_registrasi')
                        ->first();
            }else{
                $total = DB::table('tb_evaluasi_bakteri')
                        ->where('id_registrasi','=', $val->id_registrasi)
                        ->where('siklus', $input['siklus'])
                        ->where(DB::raw('YEAR(created_at)'), '=' , $input['tahun'])
                        ->select(DB::raw('SUM(nilai_2) as total'))
                        ->groupBy('id_registrasi')
                        ->first();
            }
            $val->total = $total;
        }
        if ($input['type'] == 'Identifikasi') {
            $type = 0;
        }else{
            $type = 1;
        }
        // dd($data);

        // return view('evaluasi/bakteri/laporanevaluasi/pdf', compact('data'));
        // Excel::create('Hasil Evaluasi Bakteri ', function($excel) use ($data, $input, $type) {
        //     $excel->sheet('Data', function($sheet) use ($data, $input, $type) {
        //         $sheet->loadView('evaluasi/bakteri/laporanevaluasi/pdf', array('data'=>$data,'input'=>$input, 'type'=>$type) );
        //     });
        // })->download('xls');
        if ($input['type'] == 'Identifikasi') {
        $pdf = PDF::loadview('evaluasi/bakteri/laporanevaluasi/identifikasi', compact('data','type'))
        ->setPaper('a4', 'potrait')
        ->setOptions(['dpi' => 135, 'defaultFont' => 'sans-serif'])
        ->setwarnings(false);
        }else{
        $pdf = PDF::loadview('evaluasi/bakteri/laporanevaluasi/pdf', compact('data','type'))
        ->setPaper('a4', 'potrait')
        ->setOptions(['dpi' => 135, 'defaultFont' => 'sans-serif'])
        ->setwarnings(false);
        }
        return $pdf->stream('Identifikasi Bakteri.pdf');
    }

    public function laporanpesertabakteriexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('tb_evaluasi_bakteri')
                ->join('tb_registrasi', 'tb_registrasi.id','=','tb_evaluasi_bakteri.id_registrasi')
                ->join('users','users.id','=','tb_registrasi.created_by')
                ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->join('tb_evaluasi_bakteri_saran','tb_evaluasi_bakteri_saran.id_registrasi','=','tb_registrasi.id')
                ->where('tb_evaluasi_bakteri.siklus', $input['siklus'])
                ->where('tb_evaluasi_bakteri_saran.siklus', $input['siklus'])
                ->where(DB::raw('YEAR(tb_evaluasi_bakteri.created_at)'), '=' , $input['tahun'])
                ->select('tb_evaluasi_bakteri.*','tb_registrasi.kode_lebpes','perusahaan.nama_lab','tb_registrasi.id as idregister','tb_evaluasi_bakteri_saran.kesimpulan_1','tb_evaluasi_bakteri_saran.kesimpulan_2')
                ->orderBy('users.id_member', 'asc')
                ->get();
        foreach ($data as $key => $val) {
            $datana = DB::table('data_antibiotik')
                        ->join('tb_rujukan', 'tb_rujukan.lembar', '=', 'data_antibiotik.lembar')
                        ->where('tb_rujukan.siklus', $input['siklus'])
                        ->where('tb_rujukan.tahun', $input['tahun'])
                        ->where('tb_rujukan.jenis_pemeriksaan', '=', 'Identifikasi')
                        ->where('id_registrasi', $val->id_registrasi)
                        ->where('data_antibiotik.siklus', $input['siklus'])
                        ->where(DB::raw('YEAR(data_antibiotik.created_at)'), '=' , $input['tahun'])
                        ->select('data_antibiotik.*','tb_rujukan.rujukan')
                        ->get();
            foreach ($datana as $key => $peka) {
                $kepekaan = DB::table('tb_kepekaan')
                                    ->leftjoin('antibiotik', 'tb_kepekaan.id_jenis_antibiotik', '=', 'antibiotik.id')
                                    ->join('data_antibiotik', 'tb_kepekaan.id_antibiotik', '=', 'data_antibiotik.id')
                                    ->select('antibiotik.*','antibiotik.id as id_anti','data_antibiotik.*','tb_kepekaan.*')
                                    ->where('data_antibiotik.id', $peka->id)
                                    ->where('data_antibiotik.siklus', $input['siklus'])
                                    ->where(DB::raw('YEAR(data_antibiotik.created_at)'), '=' , $input['tahun'])
                                    ->where(function ($query) {
                                        $query->where('tb_kepekaan.hasil1','!=','NULL')
                                              ->orWhere('tb_kepekaan.hasil2','!=','NULL');
                                    })
                                    ->get();
                $peka->kepekaan = $kepekaan;
            }
            $val->datana = $datana;

            if ($input['type'] == 'Identifikasi') {
                $total = DB::table('tb_evaluasi_bakteri')
                        ->where('id_registrasi','=', $val->id_registrasi)
                        ->where('siklus', $input['siklus'])
                        ->where(DB::raw('YEAR(created_at)'), '=' , $input['tahun'])
                        ->select(DB::raw('SUM(nilai_1) as total'))
                        ->groupBy('id_registrasi')
                        ->first();
            }else{
                $total = DB::table('tb_evaluasi_bakteri')
                        ->where('id_registrasi','=', $val->id_registrasi)
                        ->where('siklus', $input['siklus'])
                        ->where(DB::raw('YEAR(created_at)'), '=' , $input['tahun'])
                        ->select(DB::raw('SUM(nilai_2) as total'))
                        ->groupBy('id_registrasi')
                        ->first();
            }
            $val->total = $total;
        }
        if ($input['type'] == 'Identifikasi') {
            $type = 0;
        }else{
            $type = 1;
        }
        // dd($data);

        // return view('evaluasi/bakteri/laporanevaluasi/pdf', compact('data'));
        // Excel::create('Hasil Evaluasi Bakteri ', function($excel) use ($data, $input, $type) {
        //     $excel->sheet('Data', function($sheet) use ($data, $input, $type) {
        //         $sheet->loadView('evaluasi/bakteri/laporanevaluasi/pdf', array('data'=>$data,'input'=>$input, 'type'=>$type) );
        //     });
        // })->download('xls');
        if ($input['type'] == 'Identifikasi') {
            Excel::create('Laporan Rekap Hasil Evaluasi Tahun '.$input['tahun'].' Siklus '.$input['siklus'], function($excel) use ($data, $type) {
                $excel->sheet('Mal', function($sheet) use ($data, $type) {
                    $sheet->loadView('evaluasi/bakteri/laporanevaluasi/excel/identifikasi', array('data'=>$data, 'type'=>$type) );
                });
            })->download('xls');
        }else{
            Excel::create('Laporan Rekap Hasil Evaluasi Tahun '.$input['tahun'].' Siklus '.$input['siklus'], function($excel) use ($data, $type) {
                $excel->sheet('Mal', function($sheet) use ($data, $type) {
                    $sheet->loadView('evaluasi/bakteri/laporanevaluasi/excel/antibiotik', array('data'=>$data, 'type'=>$type) );
                });
            })->download('xls');
        }
        return $pdf->stream('Identifikasi Bakteri.pdf');
    }

    public function indexbakteri(){
        $rujukan = Rujukanbakteri::orderBy('id','ASC')->paginate(11);
        return view('evaluasi/bakteri/rujukan/index', compact('rujukan'));
    }

    public function insertBakteri()
    {
        return view('evaluasi/bakteri/rujukan/insert', compact('parameter'));
    }

    public function insertBakteri2(\Illuminate\Http\Request $request)
    {
        $data = Request::all();

        $fail = Rujukanbakteri::where('tahun','=',$request['tahun'])
                ->where('siklus','=',$request['siklus'])
                ->where('lembar','=',$request['lembar'])
                ->where('jenis_pemeriksaan','=',$request['jenis_pemeriksaan'])->count();

        if ($fail == 0) {
            Rujukanbakteri::create($data);
            Session::flash('message', 'Master Rujukan Bakteri Berhasil Disimpan!');
            Session::flash('alert-class', 'alert-success');
            return redirect('evaluasi/input-rujukan/bakteri');
        }else{
            Session::flash('message', 'Master Rujukan Bakteri Sudah Ada!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('evaluasi/input-rujukan/bakteri/insert');
        }
    }
    public function editBakteri($id)
    {
        $data = DB::table('tb_rujukan')->where('tb_rujukan.id', $id)->get();
        return view('evaluasi/bakteri/rujukan/update', compact('data'));
    }

    public function deleteBakteri($id){
        Session::flash('message', 'Data Telah Dihapus!');
        Session::flash('alert-class', 'alert-warning');
        Rujukanbakteri::find($id)->delete();
        return redirect("evaluasi/input-rujukan/bakteri");
    }
     public function updateBakteri(\Illuminate\Http\Request $request, $id)
    {
        $data['lembar'] = $request->lembar;
        $data['siklus'] = $request->siklus;
        $data['tahun'] = $request->tahun;
        $data['rujukan'] = $request->rujukan;
        $data['jenis_pemeriksaan'] = $request->jenis_pemeriksaan;
        Rujukanbakteri::where('id',$id)->update($data);
        Session::flash('message', 'Master Rujukan Bakteri Berhasil Diubah!');
        Session::flash('alert-class', 'alert-success');

        return redirect('evaluasi/input-rujukan/bakteri');
    }

    public function perusahaanbakteri(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','13')->get();
            return Datatables::of($datas)
                    // dd($datas);

            ->addColumn('action', function($data){
                    return "".'<a href="penilaian/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.bakteri.penilaian.perusahaan');
    }

    public function perusahaanbakterisertifikat(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.status','>=','2')->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)->get();
            return Datatables::of($datas)
                    // dd($datas);

            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Cetak
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        $bidang = Auth::user()->penyelenggara;
        return view('evaluasi.bakteri.penilaian.perusahaansertifikat', compact('bidang'));
    }
    public function dataperusahaanbakteri1sertifikat(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '13')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        return view('evaluasi.bakteri.penilaian.dataperusahaansertifikat', compact('data','siklus','tahun'));
    }

    public function perusahaanbakteri1(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes
'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','13')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="penilaian_uji_kepekaan_antibiotik/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.bakteri.penilaian.perusahaanuji');
    }

    public function dataperusahaanbakteri($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '13')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        return view('evaluasi.bakteri.penilaian.dataperusahaan', compact('data'));
    }
    public function dataperusahaanbakteri1($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '13')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        return view('evaluasi.bakteri.penilaian.dataperusahaanuji', compact('data'));
    }

    public function evaluasi(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->y;
        $years = date('Y');
        $register = Register::find($id);
        $evaluasi = DB::table('tb_evaluasi_bakteri')
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(tb_evaluasi_bakteri.created_at)'), '=' , $years)
                ->where('tb_evaluasi_bakteri.jenis_pemeriksa','=','Identifikasi')
                ->where('tb_evaluasi_bakteri.id_registrasi','=',$id)
                ->get();

        $identifikasi = DB::table("tb_evaluasi_bakteri")
                ->select(DB::raw("SUM(skor) as skor"))
                ->where('jenis_pemeriksa','Identifikasi')
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(tb_evaluasi_bakteri.created_at)'), '=' , $years)
                ->get();
        
        $status = Statusbakteri::where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.siklus','=', $siklus)
            ->where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.jenis_pemeriksa','=','Identifikasi')
            ->where('tb_status_bakteri.id_registrasi', $id)->first();
        // dd($status);

        $data = DB::table('data_antibiotik')
            ->leftjoin('tb_rujukan','tb_rujukan.lembar','=', 'data_antibiotik.lembar')
            ->select('data_antibiotik.id','data_antibiotik.id_registrasi','data_antibiotik.kode_lab','data_antibiotik.lembar','data_antibiotik.lembar as lembar_rujuk','data_antibiotik.spesies_auto','data_antibiotik.spesies_kultur','tb_rujukan.rujukan','tb_rujukan.jenis_pemeriksaan','tb_rujukan.siklus')
            ->where('tb_rujukan.siklus','=', $siklus)
            ->where('tb_rujukan.tahun','=', $years)
            ->where('tb_rujukan.jenis_pemeriksaan','=', 'Identifikasi')
            ->where('data_antibiotik.siklus','=', $siklus)
            ->where('data_antibiotik.id_registrasi', $id)
            ->where(DB::raw('YEAR(data_antibiotik.created_at)'), '=' , $years)
            ->orderBy('data_antibiotik.lembar','asc')
            ->get();
        // dd($data);
        // dd($data);
        return view('evaluasi.bakteri.penilaian.evaluasi.index', compact('data','status','identifikasi','evaluasi','valid','rujuk','ujiKepekaan','id','siklus'));
    }

    public function kepekaan(\Illuminate\Http\Request $request, $id){
        $siklus = $request->y;
        $years = date('Y');
        $register = Register::find($id);
        $evaluasi = DB::table('tb_evaluasi_bakteri')
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(tb_evaluasi_bakteri.created_at)'), '=' , $years)
                ->where('tb_evaluasi_bakteri.jenis_pemeriksa','=','Uji Kepekaan Antibiotik')
                ->where('tb_evaluasi_bakteri.id_registrasi','=',$id)
                ->get();
        $ujiKepekaan = DB::table("tb_evaluasi_bakteri")
                ->select(DB::raw("SUM(skor) as skor"))
                ->where('siklus', $siklus)
                ->where('jenis_pemeriksa','Uji Kepekaan Antibiotik')
                ->get();

        $status =statusbakteri::where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.siklus','=', $siklus)
            ->where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.jenis_pemeriksa','=','Uji Kepekaan Antibiotik')
            ->where('tb_status_bakteri.id_registrasi', $id)->first();

        $data = DB::table('data_antibiotik')
            ->leftjoin('tb_rujukan','tb_rujukan.lembar','=', 'data_antibiotik.lembar')
            ->select('data_antibiotik.id','data_antibiotik.id_registrasi','data_antibiotik.kode_lab','data_antibiotik.lembar','data_antibiotik.lembar as lembar_rujuk','data_antibiotik.spesies_auto','data_antibiotik.spesies_kultur','tb_rujukan.rujukan','tb_rujukan.jenis_pemeriksaan','tb_rujukan.siklus')
            ->where('tb_rujukan.siklus','=', $siklus)
            ->where('tb_rujukan.tahun','=', $years)
            ->where('tb_rujukan.jenis_pemeriksaan','=', 'Uji Kepekaan Antibiotik')
            ->where('data_antibiotik.siklus','=', $siklus)
            ->where('data_antibiotik.id_registrasi', $id)
            ->where(DB::raw('YEAR(data_antibiotik.created_at)'), '=' , $years)
            ->orderBy('data_antibiotik.lembar','asc')
            ->get();
        // dd($data);
        // dd($data);

        return view('evaluasi.bakteri.penilaian.evaluasi.kepekaan', compact('data','status','identifikasi','evaluasi','valid','rujuk','ujiKepekaan','id','siklus'));
    }
    public function evaluasiInsert(\Illuminate\Http\Request $request, $id)
    {
        $data = $request->all();
        $evaluasi = DB::table('tb_evaluasi_bakteri')->get();
        $input = $request->all();
        $siklus = $request->get('y');
        $years = date('Y');

        if ($data['simpan']== "Simpan") {
            $b = new Statusbakteri;
            $b->id_registrasi = $id;
            $b->siklus = $siklus;
            $b->tahun = $years;
            $b->keterangan = $request->keterangan;
            $b->jenis_pemeriksa = 'Identifikasi';
            $b->save();

            for ($i=0; $i < count($data['skor']); $i++) {

                $a = new EvaluasiBakteri;
                $a->id_data_antibiotik = $request->id_data_antibiotik[$i];
                $a->kode_soal = $request->kode_soal[$i];
                $a->jenis_pemeriksa = $request->jenis_pemeriksaan[$i];
                $a->nilai_rujukan = $request->rujukan[$i];
                $a->jawaban_peserta = $request->jawaban_peserta1[$i];
                $a->jawaban_peserta = $request->jawaban_peserta2[$i];
                $a->skor = $request->skor[$i];
                $a->siklus = $siklus;
                $a->id_registrasi = $id;
                $a->save();
                Session::flash('message', 'Master Rujukan Bakteri Berhasil Disimpan!');
                Session::flash('alert-class', 'alert-danger');
            }
        }
        // dd($data);
        return back();
    }

    public function evaluasiPrint(\Illuminate\Http\Request $request, $id){
        $input = $request->all();       
        $siklus = $request->y;

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );  
        $years = $q2->year;

        $tanggal = date('Y m d');
        $register = Register::find($id);
        $evaluasi = DB::table('tb_evaluasi_bakteri')
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(tb_evaluasi_bakteri.created_at)'), '=' , $years)
                ->where('tb_evaluasi_bakteri.jenis_pemeriksa','=','Identifikasi')
                ->where('tb_evaluasi_bakteri.id_registrasi','=',$id)
                ->get();
        // dd($evaluasi);


        $status = Statusbakteri::where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.siklus','=', $siklus)
            ->where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.jenis_pemeriksa','=','Identifikasi')
            ->where('tb_status_bakteri.id_registrasi', $id)->first();
        // dd($status);

        $data = DB::table('data_antibiotik')
            ->leftjoin('tb_rujukan','tb_rujukan.lembar','=', 'data_antibiotik.lembar')
            ->select('data_antibiotik.id','data_antibiotik.id_registrasi','data_antibiotik.kode_lab','data_antibiotik.lembar','data_antibiotik.lembar as lembar_rujuk','data_antibiotik.spesies_auto','data_antibiotik.spesies_kultur','tb_rujukan.rujukan','tb_rujukan.jenis_pemeriksaan','tb_rujukan.siklus')
            ->where('tb_rujukan.siklus','=', $siklus)
            ->where('tb_rujukan.tahun','=', $years)
            ->where('tb_rujukan.jenis_pemeriksaan','=', 'Identifikasi')
            ->where('data_antibiotik.siklus','=', $siklus)
            ->where('data_antibiotik.id_registrasi', $id)
            ->where(DB::raw('YEAR(data_antibiotik.created_at)'), '=' , $years)
            ->orderBy('data_antibiotik.lembar','asc')
            ->get();

            $register = Register::find($id);
            $perusahaan = $register->perusahaan->nama_lab;
            $alamat = $register->perusahaan->alamat;
            
        // dd($data);
        // dd($data);

        if ($input['simpan'] == 'Print') {
            if ((count($evaluasi)AND count($status)) > 0) {
            $pdf = PDF::loadview('evaluasi/bakteri/penilaian/evaluasi/print', compact('data','register','perusahaan','alamat','status','identifikasi','evaluasi','valid','rujuk','ujiKepekaan','id','siklus','tanggal','years'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri Identifikasi.pdf');
            }else{
            session::flash('message', 'Bakteri belum dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
            }
        }else if($input['simpan'] == 'Update'){
            $status = Statusbakteri::where("tb_status_bakteri.id_registrasi", $id)
            ->where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.siklus','=', $siklus)
            ->where('tb_status_bakteri.jenis_pemeriksa','=', 'Identifikasi')
            ->update(["keterangan" => $request->keterangan]);
            // dd($status);
            for ($i=0; $i < count($input['skor']); $i++) {
                EvaluasiBakteri::where("id", $request->id[$i])->update(["skor" => $request->skor[$i]]);
                Session::flash('message', 'Master Rujukan Bakteri Berhasil Disimpan!');
                Session::flash('alert-class', 'alert-danger');
            }
            return back();
        }
    }


    public function evaluasiInsertUji(\Illuminate\Http\Request $request, $id)
    {
        $data = $request->all();
        $evaluasi = DB::table('tb_evaluasi_bakteri')->get();
        $input = $request->all();
        $siklus = $request->get('y');
        $years = date('Y');

        if ($data['simpan']== "Simpan") {
            $b = new Statusbakteri;
            $b->id_registrasi = $id;
            $b->siklus = $siklus;
            $b->tahun = $years;
            $b->keterangan = $request->keterangan;
            $b->jenis_pemeriksa = 'Uji Kepekaan Antibiotik';
            $b->save();

            for ($i=0; $i < count($data['skor']); $i++) {

                $a = new EvaluasiBakteri;
                $a->id_data_antibiotik = $request->id_data_antibiotik[$i];
                $a->kode_soal = $request->kode_soal[$i];
                $a->jenis_pemeriksa = $request->jenis_pemeriksaan[$i];
                $a->nilai_rujukan = $request->rujukan[$i];
                $a->jawaban_peserta = $request->jawaban_peserta1[$i];
                $a->jawaban_peserta = $request->jawaban_peserta2[$i];
                $a->skor = $request->skor[$i];
                $a->siklus = $siklus;
                $a->id_registrasi = $id;
                $a->save();
                Session::flash('message', 'Master Rujukan Bakteri Berhasil Disimpan!');
                Session::flash('alert-class', 'alert-danger');
            }
        }
        // dd($data);
        return back();
    }

    public function evaluasiujiPrint(\Illuminate\Http\Request $request, $id){
        $input = $request->all();       
        // dd($input);
        $siklus = $request->y;
        $years = date('Y');
        $tanggal = date('Y m d');
        $register = Register::find($id);
        $evaluasi = DB::table('tb_evaluasi_bakteri')
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(tb_evaluasi_bakteri.created_at)'), '=' , $years)
                ->where('tb_evaluasi_bakteri.jenis_pemeriksa','=','Uji Kepekaan Antibiotik')
                ->where('tb_evaluasi_bakteri.id_registrasi','=',$id)
                ->get();


        $status = Statusbakteri::where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.siklus','=', $siklus)
            ->where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.jenis_pemeriksa','=','Uji Kepekaan Antibiotik')
            ->where('tb_status_bakteri.id_registrasi', $id)->first();
        // dd($status);

        $data = DB::table('data_antibiotik')
            ->leftjoin('tb_rujukan','tb_rujukan.lembar','=', 'data_antibiotik.lembar')
            ->select('data_antibiotik.id','data_antibiotik.id_registrasi','data_antibiotik.kode_lab','data_antibiotik.lembar','data_antibiotik.lembar as lembar_rujuk','data_antibiotik.spesies_auto','data_antibiotik.spesies_kultur','tb_rujukan.rujukan','tb_rujukan.jenis_pemeriksaan','tb_rujukan.siklus')
            ->where('tb_rujukan.siklus','=', $siklus)
            ->where('tb_rujukan.tahun','=', $years)
            ->where('tb_rujukan.jenis_pemeriksaan','=', 'Uji Kepekaan Antibiotik')
            ->where('data_antibiotik.siklus','=', $siklus)
            ->where('data_antibiotik.id_registrasi', $id)
            ->where(DB::raw('YEAR(data_antibiotik.created_at)'), '=' , $years)
            ->orderBy('data_antibiotik.lembar','asc')
            ->get();
        // dd($data);
        // dd($data);
         $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $alamat = $register->perusahaan->alamat;

        if ($input['simpan'] == 'Print') {
            if ((count($status)AND count($evaluasi))>0) {
            $pdf = PDF::loadview('evaluasi/bakteri/penilaian/evaluasi/printUji', compact('data','register','perusahaan','alamat','status','identifikasi','evaluasi','valid','rujuk','ujiKepekaan','id','siklus','tanggal','years'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri Uji Kepekaan Antibiotik.pdf');
            }else{
            Session::flash('message', 'Bakteri belum dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
            }
        }else if($input['simpan'] == 'Update'){
            $status = Statusbakteri::where("tb_status_bakteri.id_registrasi", $id)
            ->where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.siklus','=', $siklus)
            ->where('tb_status_bakteri.jenis_pemeriksa','=', 'Uji Kepekaan Antibiotik')
            ->update(["keterangan" => $request->keterangan]);
            // dd($status);
            for ($i=0; $i < count($input['skor']); $i++) {
                EvaluasiBakteri::where("id", $request->id[$i])->update(["skor" => $request->skor[$i]]);
                Session::flash('message', 'Master Rujukan Bakteri Berhasil Disimpan!');
                Session::flash('alert-class', 'alert-danger');
            }
            return back();
        }

    }
    
    public function skoridentifikasi(){
        return view('evaluasi.bakteri.skoriden.index');
    }

    public function skoridentifikasina(\Illuminate\Http\Request $request){
        $input = $request->all();
        $data = DB::table('tb_registrasi')
                ->join('tb_evaluasi_bakteri', 'tb_evaluasi_bakteri.id_registrasi','=','tb_registrasi.id')
                ->where('bidang','=', '13')
                ->where('status','>=', '2')
                ->where('tb_evaluasi_bakteri.siklus', $input['siklus'])
                ->where('tb_evaluasi_bakteri.tahun', $input['tahun'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $input['tahun'])
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->select('tb_registrasi.*')
                ->groupBy('tb_registrasi.id')
                ->get();
        foreach ($data as $key => $val) {
            $bac = DB::table('tb_evaluasi_bakteri')
                    ->where('tb_evaluasi_bakteri.siklus', $input['siklus'])
                    ->where('tb_evaluasi_bakteri.tahun', $input['tahun'])
                    ->where('tb_evaluasi_bakteri.id_registrasi', $val->id)
                    ->get();
            $val->bac = $bac;
        }
        if($input['skor'] == 'Identifikasi'){
            $type = 0;
        }else{
            $type = 1;
        }
        // dd($data);
        Excel::create('Rekapitulasi Skor Hasil '.$input['skor'], function($excel) use ($data, $input, $type) {
            $excel->sheet('Data', function($sheet) use ($data, $input, $type) {
                $sheet->loadView('evaluasi.bakteri.skoriden.excel', array('data'=>$data,'input'=>$input, 'type'=>$type) );
            });
        })->download('xls');
    }

    public function hasilidentifikasi(){
        return view('evaluasi.bakteri.spesieskul.index');
    }

    public function hasilidentifikasina(\Illuminate\Http\Request $request){
        $input = $request->all();
        $data = DB::table('data_antibiotik')
                ->where('lembar', $input['lembar'])
                ->where('siklus', $input['siklus'])
                ->where('spesies_kultur', '!=', NULL)
                ->where(DB::raw('YEAR(created_at)'), '=', $input['tahun'])
                ->groupBy('spesies_kultur')
                ->select('spesies_kultur', DB::raw('count(*) as jumlah'))
                ->orderBy('jumlah','desc')
                ->get();
        // dd($data);
        return view('evaluasi.bakteri.spesieskul.excel', compact('data','input'));
        // Excel::create('Rekapitulasi Skor Hasil '.$input['skor'], function($excel) use ($data, $input, $type) {
        //     $excel->sheet('Data', function($sheet) use ($data, $input, $type) {
        //         $sheet->loadView('evaluasi.bakteri.skoriden.excel', array('data'=>$data,'input'=>$input, 'type'=>$type) );
        //     });
        // })->download('xls');
    }

    public function jawabanantibiotik(){
        return view('evaluasi.bakteri.jawaban.index');
    }

    public function jawabanantibiotikna(\Illuminate\Http\Request $request){
        $input = $request->all();
        $data = DB::select("SELECT
                                    *,
                                    SUM(S) as SS,
                                    SUM(I) as II,
                                    SUM(R) as RR
                            FROM
                                    (SELECT
                                            antibiotik.antibiotik,
                                            antibiotik.id,
                                            tb_kepekaan.lain_lain,
                                            CONCAT(IF(hasil1 = 'S', 1, IF(hasil2 = 'S', 1, 0))) as S,
                                            CONCAT(IF(hasil1 = 'I', 1, IF(hasil2 = 'I', 1, 0))) as I,
                                            CONCAT(IF(hasil1 = 'R', 1, IF(hasil2 = 'R', 1, 0))) as R
                                    FROM
                                    `tb_kepekaan`
                                    INNER JOIN `antibiotik`ON `antibiotik`.`id` = `tb_kepekaan`.`id_jenis_antibiotik`
                                    INNER JOIN `data_antibiotik` ON `data_antibiotik`.`id` = `tb_kepekaan`.`id_antibiotik`
                                    WHERE data_antibiotik.lembar = ".$input['lembar']."
                                    AND YEAR(data_antibiotik.created_at) = ".$input['tahun']."
                                    AND data_antibiotik.siklus = ".$input['siklus']."
                                    ORDER BY antibiotik.id ASC
                                    ) as total
                            GROUP BY total.id, total.lain_lain");
        // dd($data);
        return view('evaluasi.bakteri.jawaban.excel', compact('data','input'));
    }

    public function grafikketeranganpeserta(){
        return view('evaluasi.bakteri.grafik.keterangan.index');
    }

    public function grafikketeranganpesertanya(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $lulus = DB::table('tb_status_bakteri')
                    ->where('tb_status_bakteri.tahun','=',$input['tahun'])
                    ->where('tb_status_bakteri.siklus','=',$input['siklus'])
                    ->where('tb_status_bakteri.jenis_pemeriksa','=',$input['jenis_pemeriksa'])
                    ->where('tb_status_bakteri.keterangan','=', 'Lulus')
                    ->select('tb_status_bakteri.*',DB::raw('count(keterangan) as keterangan'))
                    ->get();
        $tidak = DB::table('tb_status_bakteri')
                    ->where('tb_status_bakteri.tahun','=',$input['tahun'])
                    ->where('tb_status_bakteri.siklus','=',$input['siklus'])
                    ->where('tb_status_bakteri.jenis_pemeriksa','=',$input['jenis_pemeriksa'])
                    ->where('tb_status_bakteri.keterangan','=', 'Tidak Lulus')
                    ->select('tb_status_bakteri.*',DB::raw('count(keterangan) as keterangan'))
                    ->get();
        // var_dump($data);
        // dd($data);
        return view('evaluasi.bakteri.grafik.keterangan.grafik', compact('lulus','tidak', 'input'));
    }

    public function penilaian(\Illuminate\Http\Request $request){
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes, tb_registrasi.id as idregistrasi'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','13')->where('tb_registrasi.status','>=','2')->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)->get();
            return Datatables::of($datas)
                    // dd($datas);
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = db::table('tb_evaluasi_bakteri')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->first();
                    if(!empty($evaluasi)){
                        return "".'<a href="antibiotik/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            Proses
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="antibiotik/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            Proses
                                        </button>
                                    </a>';
                    }
            ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_evaluasi_bakteri')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_data2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia'])
            ->make(true);
        }
        return view('evaluasi.bakteri.antibiotik.index');
    }



    public function penilaiandetailantibiotik(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
            ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
            ->where('tb_registrasi.perusahaan_id', $id)
            ->where('sub_bidang.id', '=', '13')
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
            ->where('tb_registrasi.status', 3)
            ->where(function($query)
                {
                    $query->where('status_data1', 2)
                        ->orwhere('status_data2', 2)
                        ->orwhere('status_datarpr1', 2)
                        ->orwhere('status_datarpr2', 2);
                })
            ->where(function($query)
                {
                    $query->where('pemeriksaan2', '=' , 'done')
                            ->orwhere('pemeriksaan', '=' , 'done')
                            ->orwhere('siklus_1', '=' , 'done')
                            ->orwhere('siklus_2', '=' , 'done');
                })
            ->get();
        return view('evaluasi.bakteri.antibiotik.penilaiandetail', compact('data','data2', 'siklus'));
    }

    public function lembar(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        $data = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data->created_at );  
        
        $years = $tahun->year;

        $data = DB::table('data_antibiotik')
                ->leftjoin('resistensi','resistensi.id','=','data_antibiotik.resistensi')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(data_antibiotik.created_at)'), '=' , $years)
                ->orderBy('lembar', 'asc')
                ->select('data_antibiotik.*','resistensi.name')
                ->get();
        // dd($data);
        foreach ($data as $key => $val) {
            $kepekaan = DB::table('tb_kepekaan')
                                ->leftjoin('antibiotik', 'tb_kepekaan.id_jenis_antibiotik', '=', 'antibiotik.id')
                                ->join('data_antibiotik', 'tb_kepekaan.id_antibiotik', '=', 'data_antibiotik.id')
                                ->select('antibiotik.*','antibiotik.id as id_anti','data_antibiotik.*','tb_kepekaan.*')
                                ->where('data_antibiotik.id', $val->id)
                                ->where(DB::raw('YEAR(data_antibiotik.created_at)'), '=' , $years)
                                ->where(function ($query) {
                                    $query->where('tb_kepekaan.hasil1','!=','NULL')
                                          ->orWhere('tb_kepekaan.hasil2','!=','NULL')
                                          ->orWhere('tb_kepekaan.kesimpulan','!=','NULL');
                                })
                                ->get();
            $val->kepekaan = $kepekaan;
            $Iden = DB::table('tb_rujukan')
                        ->where('lembar', $val->kd_bahan)
                        ->where('siklus', $siklus)
                        ->where('tahun', '=' , $years)
                        ->where('jenis_pemeriksaan','=','Identifikasi')
                        ->first();
            $val->Iden = $Iden;
            $Anti = DB::table('tb_rujukan')
                        ->where('lembar', $val->kd_bahan)
                        ->where('siklus', $siklus)
                        ->where('tahun', '=' , $years)
                        ->where('jenis_pemeriksaan','=','Uji Kepekaan Antibiotik')
                        ->first();
            $val->Anti = $Anti;
        }
        // dd($data);

        $evaluasi = DB::table('tb_evaluasi_bakteri')->where('id_registrasi', $id)->where('siklus', $siklus)->where('tahun', $years)->get();
        $saran = DB::table('tb_evaluasi_bakteri_saran')->where('id_registrasi', $id)->where('siklus', $siklus)->where('tahun', $years)->first();
        $totalnilai = DB::table('tb_evaluasi_bakteri')
                        ->where('id_registrasi', $id)
                        ->where('siklus', $siklus)
                        ->where('tahun', $years)
                        ->select(DB::raw('SUM(nilai_1) as nilai_1, SUM(nilai_2) as nilai_2'))
                        ->first();
        // dd($rujukan);
        $register = Register::find($id);
        $perusahaan = $register->perusahaan;
        return view('evaluasi.bakteri.antibiotik.lembar', compact('id','data','perusahaan', 'type','siklus','register', 'evaluasi', 'saran', 'totalnilai'));
    }

    public function printevaluasi(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        $data = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data->created_at );  
        
        $years = $tahun->year;

        $data = DB::table('data_antibiotik')
                ->leftjoin('resistensi','resistensi.id','=','data_antibiotik.resistensi')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(data_antibiotik.created_at)'), '=' , $years)
                ->select('data_antibiotik.*','resistensi.name')
                ->orderBy('lembar', 'asc')
                ->get();
        foreach ($data as $key => $val) {
            $kepekaan = DB::table('tb_kepekaan')
                                ->leftjoin('antibiotik', 'tb_kepekaan.id_jenis_antibiotik', '=', 'antibiotik.id')
                                ->join('data_antibiotik', 'tb_kepekaan.id_antibiotik', '=', 'data_antibiotik.id')
                                ->select('antibiotik.*','antibiotik.id as id_anti','data_antibiotik.*','tb_kepekaan.*')
                                ->where('data_antibiotik.id', $val->id)
                                ->where(DB::raw('YEAR(data_antibiotik.created_at)'), '=' , $years)
                                ->where(function ($query) {
                                    $query->where('tb_kepekaan.hasil1','!=','NULL')
                                          ->orWhere('tb_kepekaan.hasil2','!=','NULL');
                                })
                                ->get();
            $val->kepekaan = $kepekaan;
            $rujukan = DB::table('tb_rujukan')
                        ->where('lembar', $val->kd_bahan)
                        ->where('siklus', $siklus)
                        ->where('tahun', '=' , $years)
                        ->first();
            $val->rujukan = $rujukan;

        }

        $evaluasi = DB::table('tb_evaluasi_bakteri')->where('id_registrasi', $id)->where('siklus', $siklus)->where('tahun', $years)->get();
        $saran = DB::table('tb_evaluasi_bakteri_saran')->where('id_registrasi', $id)->where('siklus', $siklus)->where('tahun', $years)->first();
        $totalnilai = DB::table('tb_evaluasi_bakteri')
                        ->where('id_registrasi', $id)
                        ->where('siklus', $siklus)
                        ->where('tahun', $years)
                        ->select(DB::raw('SUM(nilai_1) as nilai_1, SUM(nilai_2) as nilai_2'))
                        ->first();
        // dd($totalnilai);
        $register = Register::find($id);
        $perusahaan = $register->perusahaan;
        $ttd = DB::table('tb_ttd_evaluasi')
            ->where('tahun', $tahun)
            ->where('siklus', $siklus)
            ->where('bidang', '=', '13')
            ->first();


        if (count($saran) > 0) {
        $pdf = PDF::loadview('evaluasi.bakteri.antibiotik.lembarPrint', compact('id','data','perusahaan', 'type','siklus','register', 'evaluasi', 'saran', 'totalnilai','years','ttd'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        return $pdf->stream('Uji Kepekaan Antibiotik.pdf');
        }else{
            Session::flash('message', 'Bakteri belum dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
    }
    public function cetakimonologi(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        $tanggal = date('Y m d');
        $years = date('Y');
        $lembar = $request->get('lembar');

        $ujiantibiotik = DB::table('tb_uji_kepekaan_antibiotik')
            ->where('lembar', $lembar)
            ->where(DB::raw('YEAR(tb_uji_kepekaan_antibiotik.created_at)'), '=' , $years)
            ->get();

        // dd($ujiantibiotik);

        if(empty($lembar)){
            $lembar = 1;
        }

        $data = DB::table('data_antibiotik')->where('id_registrasi', $id)->where('siklus', $siklus)->where('lembar', '=', $lembar)->get();
        $kepekaan = DB::table('tb_kepekaan')
                                ->leftjoin('antibiotik', 'tb_kepekaan.id_jenis_antibiotik', '=', 'antibiotik.id')
                                ->join('data_antibiotik', 'tb_kepekaan.id_antibiotik', '=', 'data_antibiotik.id')
                                ->select('antibiotik.*','antibiotik.id as id_anti','data_antibiotik.*','tb_kepekaan.*')
                                ->where('data_antibiotik.lembar',$lembar)
                                // ->where('data_antibiotik.id', $val->id)
                                ->get();
    
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $alamat = $register->perusahaan->alamat;
        

        if (count($ujiantibiotik) > 0) {
        $pdf = PDF::loadview('evaluasi.bakteri.antibiotik.lembarPrint', compact('id','register','alamat','tanggal','years','ujiantibiotik','lembar','data','perusahaan','peka', 'type','siklus','date','val','kepekaan'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        return $pdf->stream('Uji Kepekaan Antibiotik.pdf');
        }else{
            Session::flash('message', 'Bakteri belum dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }

    }

    public function insertAntibiotik(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        $data = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data->created_at );  
        
        $tahun = $tahun->year;
        // dd($input);
        if ($input['simpan'] == "Simpan") {
            $i = 0;
            $nilai_1 = 0;
            $nilai_2 = 0;
            foreach ($input['kode_bahan'] as $key => $value) {
                $a = new EvaluasiBakteri;
                $a->id_registrasi = $id;
                $a->siklus = $siklus;
                $a->tahun = $tahun;
                $a->kode_bahan = $input['kode_bahan'][$i];
                $a->nilai_1 = $input['nilai_1'][$i];
                $a->acuan = $input['acuan'][$i];
                $a->nilai_2 = $input['nilai_2'][$i];
                $a->save();
                $nilai_1 = $nilai_1 + $input['nilai_1'][$i];
                $nilai_2 = $nilai_2 + $input['nilai_2'][$i];
                $i++;
            }
            if ($nilai_1 <= 6) {
                $nilai_1 = 'KURANG';
            }else{
                $nilai_1 = 'BAIK';
            }
            if ($nilai_2 <= 6) {
                $nilai_2 = 'KURANG';
            }else{
                $nilai_2 = 'BAIK';
            }

            $cek = DB::table('tb_evaluasi_bakteri_saran')->where('id', $id)->where('siklus', $siklus)->where('tahun', $tahun)->get();
            if (count($cek)) {
                $b['kesalahan_1'] = $input['kesalahan_1'];
                $b['tindakan_1'] = $input['tindakan_1'];
                $b['kesalahan_2'] = $input['kesalahan_2'];
                $b['tindakan_2'] = $input['tindakan_2'];
                $b['kesimpulan_1'] = $input['kesimpulan_1'];
                $b['kesimpulan_2'] = $input['kesimpulan_2'];
                EvaluasiBakteriSaran::where('id_registrasi',$id)->where('siklus', $siklus)->where('tahun', $tahun)->update($b);
            }else{
                $b = new EvaluasiBakteriSaran;
                $b->id_registrasi = $id;
                $b->siklus = $siklus;
                $b->tahun = $tahun;
                $b->kesalahan_1 = $input['kesalahan_1'];
                $b->tindakan_1 = $input['tindakan_1'];
                $b->kesalahan_2 = $input['kesalahan_2'];
                $b->tindakan_2 = $input['tindakan_2'];
                $b->kesimpulan_1 = $nilai_1;
                $b->kesimpulan_2 = $nilai_2;
                $b->save();
            }
            Session::flash('message', 'Uji Kepekaan Antibiotik Berhasil Disimpan!');
            Session::flash('alert-class', 'alert-success');
        }elseif ($input['simpan'] == "Update") {
            $i = 0;
            $nilai_1 = 0;
            $nilai_2 = 0;
            foreach ($input['id'] as $key => $idna) {
                $a['nilai_1'] = $input['nilai_1'][$i];
                $a['nilai_2'] = $input['nilai_2'][$i];
                $a['acuan'] = $input['acuan'][$i];
                EvaluasiBakteri::where('id', $idna)->update($a);
                $nilai_1 = $nilai_1 + $input['nilai_1'][$i];
                $nilai_2 = $nilai_2 + $input['nilai_2'][$i];
                $i++;
            }

            if ($nilai_1 <= 6) {
                $nilai_1 = 'KURANG';
            }else{
                $nilai_1 = 'BAIK';
            }
            if ($nilai_2 <= 6) {
                $nilai_2 = 'KURANG';
            }else{
                $nilai_2 = 'BAIK';
            }
            $cek = DB::table('tb_evaluasi_bakteri_saran')->where('id_registrasi', $id)->where('siklus', $siklus)->where('tahun', $tahun)->get();
            if (count($cek)) {
                $b['kesalahan_1'] = $input['kesalahan_1'];
                $b['tindakan_1'] = $input['tindakan_1'];
                $b['kesalahan_2'] = $input['kesalahan_2'];
                $b['tindakan_2'] = $input['tindakan_2'];
                $b['kesimpulan_1'] = $input['kesimpulan_1'];
                $b['kesimpulan_2'] = $input['kesimpulan_2'];
                EvaluasiBakteriSaran::where('id_registrasi',$id)->where('siklus', $siklus)->where('tahun', $tahun)->update($b);
            }else{
                $b = new EvaluasiBakteriSaran;
                $b->id_registrasi = $id;
                $b->siklus = $siklus;
                $b->tahun = $tahun;
                $b->kesalahan_1 = $input['kesalahan_1'];
                $b->tindakan_1 = $input['tindakan_1'];
                $b->kesalahan_2 = $input['kesalahan_2'];
                $b->tindakan_2 = $input['tindakan_2'];
                $b->kesimpulan_1 = $nilai_1;
                $b->kesimpulan_2 = $nilai_2;
                $b->save();
            }
            
            Session::flash('message', 'Uji Kepekaan Antibiotik Berhasil Diupdate!');
            Session::flash('alert-class', 'alert-primary');
        }
        return back();
    }

    public function perusahaan(\Illuminate\Http\Request $request)
    {
      // return('asd');
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.kode_lebpes, tb_registrasi.id as idregistrasi'))
                    ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang','=','13')
                    ->where('tb_registrasi.status','>=','2')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                    ->get();
                    // dd($datas);
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="bakteri/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Lihat
                                    </button>
                                </a>'
                        ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if($siklus == 1) {
                        if ($inputhasil->status_data1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }else{
                        if ($inputhasil->status_data2 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia'])
            ->make(true);
        }
        return view('evaluasi.bakteri.rekapcetakpeserta.perusahaan');
    }

    public function dataperusahaan(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->where('sub_bidang.id', '=', '13')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
            
        return view('evaluasi.bakteri.rekapcetakpeserta.dataperusahaan', compact('data', 'siklus'));
    }

    public function rekapinputform(){
        return view('evaluasi.bakteri.rekapinput.index');
    }

    public function rekapinput(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $siklus = $input['siklus'];
        $lembar = $input['lembar'];
        $tahun = $input['tahun'];
        $fermentasi = DB::table('bakteri_fermentasi')->orderBy('id','asc')->get();
        $antibiotik = DB::table('antibiotik')->orderBy('id','asc')->get();
        $data = DB::table('data_antibiotik')
                ->leftjoin('resistensi','resistensi.id','=','data_antibiotik.resistensi')
                ->leftjoin('tb_pendidikan','tb_pendidikan.id','=','data_antibiotik.pendidikan_petugas')
                ->join('tb_registrasi','tb_registrasi.id','=','data_antibiotik.id_registrasi')
                ->where('data_antibiotik.siklus', $siklus)
                ->where('lembar', '=', $lembar)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->select('data_antibiotik.*','tb_pendidikan.tingkat','resistensi.name as name_resisten')
                ->get();
        
        foreach ($data as $key => $val) {
            $fermentasinegatif = DB::table('tb_fermentasi_negatif')
                                ->leftjoin('bakteri_fermentasi', 'tb_fermentasi_negatif.id_fermentasi', '=', 'bakteri_fermentasi.id')
                                ->join('data_antibiotik', 'tb_fermentasi_negatif.id_antibiotik', '=', 'data_antibiotik.id')
                                ->where('data_antibiotik.id', $val->id)
                                ->select('bakteri_fermentasi.fermentasi','tb_fermentasi_negatif.status')
                                ->get();
            $val->fermentasinegatif = $fermentasinegatif;

            $fermentasipositif = DB::table('tb_fermentasi_positif')
                                ->leftjoin('bakteri_fermentasi', 'tb_fermentasi_positif.id_fermentasi', '=', 'bakteri_fermentasi.id')
                                ->join('data_antibiotik', 'tb_fermentasi_positif.id_antibiotik', '=', 'data_antibiotik.id')
                                ->where('data_antibiotik.id', $val->id)
                                ->select('bakteri_fermentasi.fermentasi','tb_fermentasi_positif.status')
                                ->get();
            $val->fermentasipositif = $fermentasipositif;

            $kepekaan = DB::table('tb_kepekaan')
                                ->join('antibiotik', 'tb_kepekaan.id_jenis_antibiotik', '=', 'antibiotik.id')
                                ->join('data_antibiotik', 'tb_kepekaan.id_antibiotik', '=', 'data_antibiotik.id')
                                ->where('data_antibiotik.id', $val->id)
                                ->select('tb_kepekaan.*', 'antibiotik.antibiotik')
                                ->get();
            $val->kepekaan = $kepekaan;

            $peka = DB::table('data_peka_antibiotik')->where('id_registrasi', $val->id_registrasi)->where('siklus', $siklus)->where('lembar', '=', $lembar)->first();
            $val->peka = $peka;
        }
        // dd($data[0]);
        // return view('evaluasi.bakteri.rekapinput.proses', compact('data','fermentasi','antibiotik'));
            Excel::create('Laporan Input Hasil Tahun '.$tahun.' Siklus '.$siklus.' Lembar '.$lembar, function($excel) use ($data, $fermentasi, $antibiotik) {
                $excel->sheet('Mal', function($sheet) use ($data, $fermentasi, $antibiotik) {
                    $sheet->loadView('evaluasi/bakteri/rekapinput/proses', array('data'=>$data, 'fermentasi'=>$fermentasi, 'antibiotik'=>$antibiotik) );
                });
            })->download('xls');
    }
}

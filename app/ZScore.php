<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZScore extends Model
{
    protected $table = 'tb_zscore';
    protected $fillable = ['*'];

    public $timestamps = false;
}

<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Perusahaan extends Model{
    protected $table = 'perusahaan';
    protected $fillable = ['*']; 
}
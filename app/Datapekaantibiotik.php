<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Datapekaantibiotik extends Model{
    protected $table = 'data_peka_antibiotik';

    protected $fillable = ['*'];
    public $timestamps = false;
}
<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Berita extends Model{
    protected $table = 'tb_berita';

    protected $fillable = ['img','isi','judul','created_by'];
    // public $timestamps = false;
}
<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


use DB;
use App\Skoringuri;
use App\TbHp;
use App\Rujukanurinalisa;
use App\TbReagenImunologi as ReagenImunologi;
use App\HpHeader;
use App\Parameter;
use App\MetodePemeriksaan;
use App\HpDetail;
use App\ZScore;
use App\ZScoreAlat;
use App\ZScoreMetode;
use App\kimiaklinik as Kimiaklinik;
use App\daftar as Daftar;
use App\register as Register;
use App\JobsEvaluasi;


class Evaluasi implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $dataInput = [];
    protected $nameEvaluasi = "";
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dataInput,$nameEvaluasi)
    {
        $this->dataInput = $dataInput;
        $this->nameEvaluasi = $nameEvaluasi;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $input = $this->dataInput;
        $siklus = $input['siklus'];
        $type = $input['type'];
        $date = $input['date'];
        if($this->nameEvaluasi == "evaluasi-parameter"){
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where('tb_registrasi.bidang','=','1')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->get();
            foreach($datas as $r){
                $r->sub_bidang =  DB::table('tb_registrasi')
                    ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                    ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                    ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                    ->where('tb_registrasi.perusahaan_id', $r->id)
                    ->where('sub_bidang.id', '=', '1')
                    ->where('tb_registrasi.status', 3)
                    ->where(function($query)
                        {
                            $query->where('status_data1', 2)
                                ->orwhere('status_data2', 2);
                        })
                    ->where(function($query)
                        {
                            if ('pemeriksaan' != 'done') {
                                $query->orwhere('pemeriksaan2', 'done');
                            }
                            if ('pemeriksaan2' != 'done') {
                                $query->orwhere('pemeriksaan', 'done');
                            }
                        })
                    ->get();
                foreach($r->sub_bidang as $sb){
                    $zscore = DB::table('tb_zscore')
                            ->where('siklus', $siklus)
                            ->where('type', $type)
                            ->where('tahun', $date)
                            ->where('id_registrasi', $sb->id);
                    $zscore->delete();
                    $dataSave = [];
                    $dataParamA = DB::table('parameter')
                        ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                        ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                        ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                        ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                        ->where('hp_headers.siklus', $siklus)
                        ->where('hp_headers.type', $type)
                        ->where('tb_registrasi.id', $sb->id)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan',DB::raw('hp_details.id as hp_detail_id'))
                        ->orderBy('parameter.bagian', 'asc')
                        ->get();
                    foreach ($dataParamA as $key => $valParam) {
                        $sd = DB::table('tb_sd_median')
                                ->where('parameter', $valParam->id)
                                ->where('siklus', $siklus)
                                ->where('tahun', $date)
                                ->where('tipe', $type)
                                ->where('form', '=', 'hematologi')
                                ->get();
                        $valParam->sd = $sd;
                        $dataSave['parameter_id'][$key] = $valParam->id;
                        $valParam->hasil_pemeriksaan = str_replace(",",".",$valParam->hasil_pemeriksaan);
                        $valParam->hasil_pemeriksaan = ($valParam->hasil_pemeriksaan == "-" ? NULL : str_replace(",",".",$valParam->hasil_pemeriksaan));
                        if (count($valParam->sd) && $valParam->hasil_pemeriksaan != NULL) {
                            $z = ($valParam->hasil_pemeriksaan - $valParam->sd[0]->median) / $valParam->sd[0]->sd;
                            $dataSave['zscore'][$key] = number_format($z, 1);
                        }else{
                            $z = "-";
                            if($valParam->hasil_pemeriksaan == NULL){
                                $dataSave['zscore'][$key] = "Tidak di analisis";
                            }elseif($z == '-'){
                                $dataSave['zscore'][$key] = "-";
                            }
                        }
                    }
                    $i = 0;
                    if(!empty($dataSave)){
                        foreach ($dataSave['parameter_id'] as $alat) {
                            $Save = new ZScore;
                            $Save->id_registrasi = $sb->id;
                            $Save->parameter = $dataSave['parameter_id'][$i];
                            $Save->zscore = $dataSave['zscore'][$i];
                            $Save->type = $type;
                            $Save->siklus = $siklus;
                            $Save->tahun = $date;
                            $Save->form = 'hematologi';
                            $Save->save();
                            $i++;
                        }
                    }
                }

            }
        }elseif($this->nameEvaluasi == "evaluasi-alat"){
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where('tb_registrasi.bidang','=','1')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->get();

            foreach($datas as $r){
            $r->sub_bidang =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $r->id)
                ->where('sub_bidang.id', '=', '1')
                ->where('tb_registrasi.status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
                foreach($r->sub_bidang as $sb){
                    $zscore = DB::table('tb_zscore_alat')
                            ->where('siklus', $siklus)
                            ->where('type', $type)
                            ->where('tahun', $date)
                            ->where('id_registrasi', $sb->id);
                            $zscore->delete();
                    $dataSave = [];
                    $dataParamA = DB::table('parameter')
                            ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                            ->join('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                            ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                            ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                            ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                            ->where('hp_headers.siklus', $siklus)
                            ->where('hp_headers.type', $type)
                            ->where('tb_registrasi.id', $sb->id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                            ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id as id_parameter', 'tb_instrumen.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                            ->orderBy('parameter.bagian', 'asc')
                            ->get();
                    foreach ($dataParamA as $key => $valParam) {
                        $sd = DB::table('tb_sd_median_alat')
                                ->where('parameter', $valParam->id_parameter)
                                ->where('alat', $valParam->id)
                                ->where('siklus', $siklus)
                                ->where('tahun', $date)
                                ->where('tipe', $type)
                                ->where('form', '=', 'hematologi')
                                ->get();
                        $valParam->sd = $sd;

                        $dataSave['parameter_id'][$key] = $valParam->id_parameter;
                        $dataSave['alat'][$key] = $valParam->id;
                        $valParam->hasil_pemeriksaan = str_replace(",",".",$valParam->hasil_pemeriksaan);

                        $valParam->hasil_pemeriksaan = ($valParam->hasil_pemeriksaan == "-" ? NULL : str_replace(",",".",$valParam->hasil_pemeriksaan));
                        if (count($valParam->sd) && $valParam->hasil_pemeriksaan != NULL && $valParam->sd[0]->median != NULL) {
                            $z = ($valParam->hasil_pemeriksaan - $valParam->sd[0]->median) / $valParam->sd[0]->sd;
                            $dataSave['zscore'][$key] = number_format($z, 1);
                        }else{
                            $z = "-";
                            if($valParam->hasil_pemeriksaan == NULL){
                                $dataSave['zscore'][$key] = "Tidak di analisis";
                            }else{
                                $dataSave['zscore'][$key] = "-";
                            }
                        }

                    }
                    $i = 0;
                    if(!empty($dataSave)){
                        foreach ($dataSave['parameter_id'] as $alat) {
                            $Save = new ZScoreAlat;
                            $Save->id_registrasi = $sb->id;
                            $Save->parameter = $dataSave['parameter_id'][$i];
                            $Save->alat = $dataSave['alat'][$i];
                            $Save->zscore = $dataSave['zscore'][$i];
                            $Save->type = $type;
                            $Save->siklus = $siklus;
                            $Save->tahun = $date;
                            $Save->form = 'hematologi';
                            $Save->save();
                            $i++;
                        }
                    }
                }
            }
        }elseif($this->nameEvaluasi == "evaluasi-metode"){
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->where('tb_registrasi.bidang','=','1')
                ->get();

            foreach($datas as $r){
                $r->sub_bidang =  DB::table('tb_registrasi')
                    ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                    ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                    ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                    ->where('tb_registrasi.perusahaan_id', $r->id)
                    ->where('sub_bidang.id', '=', '1')
                    ->where('tb_registrasi.status', 3)
                    ->where(function($query)
                        {
                            $query->where('status_data1', 2)
                                ->orwhere('status_data2', 2);
                        })
                    ->where(function($query)
                        {
                            if ('pemeriksaan' != 'done') {
                                $query->orwhere('pemeriksaan2', 'done');
                            }
                            if ('pemeriksaan2' != 'done') {
                                $query->orwhere('pemeriksaan', 'done');
                            }
                        })
                    ->get();
                foreach($r->sub_bidang as $sb){
                    $zscore = DB::table('tb_zscore_metode')
                                ->where('siklus', $siklus)
                                ->where('type', $type)
                                ->where('tahun', $date)
                                ->where('id_registrasi', $sb->id);
                                $zscore->delete();

                    $dataSave = [];

                    $dataParamA = DB::table('parameter')
                            ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                            ->join('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                            ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                            ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                            ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                            ->where('hp_headers.siklus', $siklus)
                            ->where('hp_headers.type', $type)
                            ->where('tb_registrasi.id', $sb->id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                            ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id as id_parameter', 'metode_pemeriksaan.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                            ->orderBy('parameter.bagian', 'asc')
                            ->get();
                    foreach ($dataParamA as $key => $valParam) {
                        $sd = DB::table('tb_sd_median_metode')
                                ->where('parameter', $valParam->id_parameter)
                                ->where('metode', $valParam->id)
                                ->where('siklus', $siklus)
                                ->where('tahun', $date)
                                ->where('tipe', $type)
                                ->where('form', '=', 'hematologi')
                                ->get();
                        $valParam->sd = $sd;

                        $dataSave['parameter_id'][$key] = $valParam->id_parameter;
                        $dataSave['metode'][$key] = $valParam->id;
                        $valParam->hasil_pemeriksaan = str_replace(",",".",$valParam->hasil_pemeriksaan);

                        $valParam->hasil_pemeriksaan = ($valParam->hasil_pemeriksaan == "-" ? NULL : str_replace(",",".",$valParam->hasil_pemeriksaan));
                        if (count($valParam->sd) && $valParam->hasil_pemeriksaan != NULL && $valParam->sd[0]->median != 0 && $valParam->sd[0]->median != NULL) {
                            $z = ($valParam->hasil_pemeriksaan - $valParam->sd[0]->median) / $valParam->sd[0]->sd;
                            $dataSave['zscore'][$key] = number_format($z, 1);
                        }else{
                            $z = "-";
                            if($valParam->hasil_pemeriksaan == NULL){
                                $dataSave['zscore'][$key] = "Tidak di analisis";
                            }elseif($z == '-'){
                                $dataSave['zscore'][$key] = "-";
                            }
                        }
                        

                    }
                    $i = 0;
                    if(!empty($dataSave)){
                        foreach ($dataSave['parameter_id'] as $alat) {
                            $Save = new ZScoreMetode;
                            $Save->id_registrasi = $sb->id;
                            $Save->parameter = $dataSave['parameter_id'][$i];
                            $Save->metode = $dataSave['metode'][$i];
                            $Save->zscore = $dataSave['zscore'][$i];
                            $Save->type = $type;
                            $Save->siklus = $siklus;
                            $Save->tahun = $date;
                            $Save->form = 'hematologi';
                            $Save->save();
                            $i++;
                        }
                    }
                }
            }
        }elseif($this->nameEvaluasi == "evaluasi-kimia"){

            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->where('tb_registrasi.bidang','=','2')
                ->get();
            foreach($datas as $r){
                $r->sub_bidang =  DB::table('tb_registrasi')
                    ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                    ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                    ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                    ->where('tb_registrasi.perusahaan_id', $r->id)
                    ->where('sub_bidang.id', '=', '2')
                    ->where('tb_registrasi.status', 3)
                    ->where(function($query)
                        {
                            $query->where('status_data1', 2)
                                ->orwhere('status_data2', 2)
                                ->orwhere('status_datarpr1', 2)
                                ->orwhere('status_datarpr2', 2);
                        })
                    ->where(function($query)
                        {
                                $query->where('pemeriksaan2', 'done')
                                    ->orwhere('pemeriksaan', 'done')
                                    ->orwhere('rpr1', 'done')
                                    ->orwhere('rpr2', 'done');
                        })
                    ->get();
                foreach($r->sub_bidang as $sb){
                    $zscore = DB::table('tb_zscore')
                            ->where('siklus', $siklus)
                            ->where('type', $type)
                            ->where('tahun', $date)
                            ->where('id_registrasi', $sb->id);
                    $zscore->delete();
                    $dataSave = [];
                    $dataParamA = DB::table('parameter')
                        ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                        ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                        ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                        ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                        ->where('hp_headers.siklus', $siklus)
                        ->where('hp_headers.type', $type)
                        ->where('tb_registrasi.id', $sb->id)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan',DB::raw('hp_details.id as hp_detail_id'))
                        ->orderBy('parameter.bagian', 'asc')
                        ->get();
                    foreach ($dataParamA as $key => $valParam) {
                        $sd = DB::table('tb_sd_median')
                                ->where('parameter', $valParam->id)
                                ->where('siklus', $siklus)
                                ->where('tahun', $date)
                                ->where('tipe', $type)
                                ->where('form', '=', 'kimia klinik')
                                ->get();
                        $valParam->sd = $sd;
                        $dataSave['parameter_id'][$key] = $valParam->id;

                        $labelHasil = $valParam->hasil_pemeriksaan;
                        try{
                            $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan  *1;
                        }catch(\Exception $e){
                            $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan;
                        }
                        if($valParam->hasil_pemeriksaan == "-" || $valParam->hasil_pemeriksaan == null || $valParam->hasil_pemeriksaan == '0'  || $valParam->hasil_pemeriksaan == '0.00' ||  $valParam->hasil_pemeriksaan == '0.0' || empty($valParam->hasil_pemeriksaan) || is_string($valParam->hasil_pemeriksaan)){
                            if($valParam->hasil_pemeriksaan == '0' || $valParam->hasil_pemeriksaan == '0.0' || $valParam->hasil_pemeriksaan == '0.00' || is_string($valParam->hasil_pemeriksaan)){
                                $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan;
                                $dataSave['zscore'][$key] = '-';
                            }else{
                                $dataSave['zscore'][$key] = '-';
                            }
                        }else{
                            $target_parameter = (!empty($valParam->sd) ? (!empty($valParam->sd[0]->median) ? $valParam->sd[0]->median : '-') : '-');
                            $sd_parameter = (!empty($valParam->sd) ? (!empty($valParam->sd[0]->sd) ? $valParam->sd[0]->sd : '-') : '-');
                            $zscore_parameter = '-';
                            if(($target_parameter != '-' && $sd_parameter != '-')){
                                $zscore_parameter = ($valParam->hasil_pemeriksaan - $valParam->sd[0]->median) / $valParam->sd[0]->sd;
                                $dataSave['zscore'][$key] = number_format($zscore_parameter, 2);
                            }
                        }
                    }
                    $i = 0;
                    // dd($dataSave);
                    if(!empty($dataSave)){
                        foreach ($dataSave['parameter_id'] as $alat) {
                            $Save = new ZScore;
                            $Save->id_registrasi = $sb->id;
                            $Save->parameter = $dataSave['parameter_id'][$i];
                            $Save->zscore = $dataSave['zscore'][$i];
                            $Save->type = $type;
                            $Save->siklus = $siklus;
                            $Save->tahun = $date;
                            $Save->form = 'kimia klinik';
                            $Save->save();
                            $i++;
                        }
                    }
                }
            }


            $datasalat = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->where('tb_registrasi.bidang','=','2')
                ->get();
            foreach($datasalat as $r){
                $r->sub_bidang =  DB::table('tb_registrasi')
                    ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                    ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                    ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                    ->where('tb_registrasi.perusahaan_id', $r->id)
                    ->where('sub_bidang.id', '=', '2')
                    ->where('tb_registrasi.status', 3)
                    ->where(function($query)
                        {
                            $query->where('status_data1', 2)
                                ->orwhere('status_data2', 2)
                                ->orwhere('status_datarpr1', 2)
                                ->orwhere('status_datarpr2', 2);
                        })
                    ->where(function($query)
                        {
                                $query->where('pemeriksaan2', 'done')
                                    ->orwhere('pemeriksaan', 'done')
                                    ->orwhere('rpr1', 'done')
                                    ->orwhere('rpr2', 'done');
                        })
                    ->get();
                foreach($r->sub_bidang as $sb){
                    $zscore = DB::table('tb_zscore_alat')
                            ->where('siklus', $siklus)
                            ->where('type', $type)
                            ->where('tahun', $date)
                            ->where('id_registrasi', $sb->id);
                    $zscore->delete();
                    $dataSave = [];
                    $dataParamA = DB::table('parameter')
                            ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                            ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                            ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                            ->leftjoin('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                            ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                            ->where('hp_headers.siklus', $siklus)
                            ->where('hp_headers.type', $type)
                            ->where('tb_registrasi.id', $sb->id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                            ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id as id_parameter', 'tb_instrumen.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                            ->orderBy('parameter.bagian', 'asc')
                            ->get();
                    foreach ($dataParamA as $key => $valParam) {
                        $sd = DB::table('tb_sd_median_alat')
                                ->where('parameter', $valParam->id_parameter)
                                ->where('alat', $valParam->id)
                                ->where('siklus', $siklus)
                                ->where('tahun', $date)
                                ->where('tipe', $type)
                                ->where('form', '=', 'kimia klinik')
                                ->get();
                        $valParam->sd = $sd;
                        $dataSave['parameter_id'][$key] = $valParam->id_parameter;
                        $dataSave['alat'][$key] = $valParam->id;

                        $labelHasil = $valParam->hasil_pemeriksaan;
                        try{
                            $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan  *1;
                        }catch(\Exception $e){
                            $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan;
                        }
                        if($valParam->hasil_pemeriksaan == "-" || $valParam->hasil_pemeriksaan == null || $valParam->hasil_pemeriksaan == '0'  || $valParam->hasil_pemeriksaan == '0.00' ||  $valParam->hasil_pemeriksaan == '0.0' || empty($valParam->hasil_pemeriksaan) || is_string($valParam->hasil_pemeriksaan)){
                            if($valParam->hasil_pemeriksaan == '0' || $valParam->hasil_pemeriksaan == '0.0' || $valParam->hasil_pemeriksaan == '0.00' || is_string($valParam->hasil_pemeriksaan)){
                                $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan;
                                $dataSave['zscore'][$key] = '-';
                            }else{
                                $dataSave['zscore'][$key] = '-';
                            }
                        }else{
                            $target_alat = (!empty($valParam->sd) ? (!empty($valParam->sd[0]->median) ? $valParam->sd[0]->median : '-') : '-');
                            $sd_alat = (!empty($valParam->sd) ? (!empty($valParam->sd[0]->sd) ? $valParam->sd[0]->sd : '-') : '-');
                            $zscore_alat = '-';
                            $dataSave['zscore'][$key] = '-';
                            if(($target_alat != '-' && $sd_alat != '-')){
                                $zscore_alat = ($valParam->hasil_pemeriksaan - $valParam->sd[0]->median) / $valParam->sd[0]->sd;
                                $dataSave['zscore'][$key] = number_format($zscore_alat, 2);
                            }
                        }
                    }
                    $i = 0;
                    // dd($dataSave);
                    if(!empty($dataSave)){
                        foreach ($dataSave['parameter_id'] as $alat) {
                            $Save = new ZScoreAlat;
                            $Save->id_registrasi = $sb->id;
                            $Save->parameter = $dataSave['parameter_id'][$i];
                            $Save->alat = $dataSave['alat'][$i];
                            $Save->zscore = $dataSave['zscore'][$i];
                            $Save->type = $type;
                            $Save->siklus = $siklus;
                            $Save->tahun = $date;
                            $Save->form = 'kimia klinik';
                            $Save->save();
                            $i++;
                        }
                    }
                }
            }

            $datasmetode = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->where('tb_registrasi.bidang','=','2')
                ->get();
            foreach($datasmetode as $r){
                $r->sub_bidang =  DB::table('tb_registrasi')
                    ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                    ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                    ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                    ->where('tb_registrasi.perusahaan_id', $r->id)
                    ->where('sub_bidang.id', '=', '2')
                    ->where('tb_registrasi.status', 3)
                    ->where(function($query)
                        {
                            $query->where('status_data1', 2)
                                ->orwhere('status_data2', 2)
                                ->orwhere('status_datarpr1', 2)
                                ->orwhere('status_datarpr2', 2);
                        })
                    ->where(function($query)
                        {
                                $query->where('pemeriksaan2', 'done')
                                    ->orwhere('pemeriksaan', 'done')
                                    ->orwhere('rpr1', 'done')
                                    ->orwhere('rpr2', 'done');
                        })
                    ->get();
                foreach($r->sub_bidang as $sb){
                    $zscore = DB::table('tb_zscore_metode')
                            ->where('siklus', $siklus)
                            ->where('type', $type)
                            ->where('tahun', $date)
                            ->where('id_registrasi', $sb->id);
                    $zscore->delete();
                    $dataSave = [];
                    $dataParamA = DB::table('parameter')
                            ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                            ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                            ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                            ->leftjoin('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                            ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                            ->where('hp_headers.siklus', $siklus)
                            ->where('hp_headers.type', $type)
                            ->where('tb_registrasi.id', $sb->id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                            ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id as id_parameter', 'metode_pemeriksaan.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                            ->orderBy('parameter.bagian', 'asc')
                            ->get();
                    foreach ($dataParamA as $key => $valParam) {
                        $sd = DB::table('tb_sd_median_metode')
                                ->where('parameter', $valParam->id_parameter)
                                ->where('metode', $valParam->id)
                                ->where('siklus', $siklus)
                                ->where('tahun', $date)
                                ->where('tipe', $type)
                                ->where('form', '=', 'kimia klinik')
                                ->get();
                        $valParam->sd = $sd;
                        $dataSave['parameter_id'][$key] = $valParam->id_parameter;
                        $dataSave['metode'][$key] = $valParam->id;

                        $labelHasil = $valParam->hasil_pemeriksaan;
                        try{
                            $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan  *1;
                        }catch(\Exception $e){
                            $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan;
                        }
                        if($valParam->hasil_pemeriksaan == "-" || $valParam->hasil_pemeriksaan == null || $valParam->hasil_pemeriksaan == '0'  || $valParam->hasil_pemeriksaan == '0.00' ||  $valParam->hasil_pemeriksaan == '0.0' || empty($valParam->hasil_pemeriksaan) || is_string($valParam->hasil_pemeriksaan)){
                            if($valParam->hasil_pemeriksaan == '0' || $valParam->hasil_pemeriksaan == '0.0' || $valParam->hasil_pemeriksaan == '0.00' || is_string($valParam->hasil_pemeriksaan)){
                                $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan;
                                $dataSave['zscore'][$key] = '-';
                            }else{
                                $dataSave['zscore'][$key] = '-';
                            }
                        }else{
                            $target_metode = (!empty($valParam->sd) ? (!empty($valParam->sd[0]->median) ? $valParam->sd[0]->median : '-') : '-');
                            $sd_metode = (!empty($valParam->sd) ? (!empty($valParam->sd[0]->sd) ? $valParam->sd[0]->sd : '-') : '-');
                            $zscore_metode = '-';
                            $dataSave['zscore'][$key] = '-';
                            if(($target_metode != '-' && $sd_metode != '-')){
                                $zscore_parameter = ($valParam->hasil_pemeriksaan - $valParam->sd[0]->median) / $valParam->sd[0]->sd;
                                $dataSave['zscore'][$key] = number_format($zscore_parameter, 2);
                            }
                        }
                    }
                    $i = 0;
                    // dd($dataSave);
                    if(!empty($dataSave)){
                        foreach ($dataSave['parameter_id'] as $alat) {
                            $Save = new ZScoreMetode;
                            $Save->id_registrasi = $sb->id;
                            $Save->parameter = $dataSave['parameter_id'][$i];
                            $Save->metode = $dataSave['metode'][$i];
                            $Save->zscore = $dataSave['zscore'][$i];
                            $Save->type = $type;
                            $Save->siklus = $siklus;
                            $Save->tahun = $date;
                            $Save->form = 'kimia klinik';
                            $Save->save();
                            $i++;
                        }
                    }
                }
            }
        }elseif($this->nameEvaluasi == "evaluasi-urinalisa") {
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->join('hp_headers', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                    ->where('tb_registrasi.bidang','=','3')
                    ->where('hp_headers.type', $type)
                    ->where('hp_headers.siklus', $siklus)
                    ->get();
                    // dd($datas);
            foreach($datas as $r){
                $r->sub_bidang =  DB::table('tb_registrasi')
                    ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                    ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                    ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                    ->where('tb_registrasi.perusahaan_id', $r->id)
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                    ->where('sub_bidang.id', '=', '3')
                    ->where('tb_registrasi.status', 3)
                    ->where(function($query)
                        {
                            $query->where('status_data1', 2)
                                ->orwhere('status_data2', 2)
                                ->orwhere('rpr1', 2)
                                ->orwhere('rpr2', 2);
                        })
                    ->where(function($query)
                        {
                            $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('rpr1', 'done')
                                ->orwhere('rpr2', 'done');
                        })
                    ->get();
                foreach($r->sub_bidang as $sb){
                    $score = DB::table('tb_skoring_urinalisa')
                            ->where('siklus', $siklus)
                            ->where('type', $type)
                            ->where('tahun', $date)
                            ->where('id_registrasi', $sb->id);
                    $score->delete();
                    $dataSave = [];

                    $data = DB::table('parameter')
                        ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
                        ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
                        ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
                        ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
                        ->leftjoin('tb_reagen_imunologi', 'hp_details.reagen', '=', 'tb_reagen_imunologi.id')
                        ->leftjoin('tb_instrumen','hp_details.alat', '=', 'tb_instrumen.id')
                        ->leftjoin('tb_hp','hp_details.hasil_pemeriksaan', '=', 'tb_hp.id')
                        ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.alat','tb_instrumen.id as idinstrumen', 'tb_instrumen.instrumen','hp_details.instrument_lain', 'hp_details.id as id_detail','hp_details.reagen as hp_reagen','tb_reagen_imunologi.reagen as imu_reagen','hp_details.reagen_lain', 'tb_hp.hp', 'tb_hp.parameter_id')
                        ->where('parameter.kategori', 'urinalisa')
                        ->where('hp_headers.id_registrasi', $sb->id)
                        ->where('hp_headers.type', $type)
                        ->where('hp_headers.siklus', $siklus)
                        ->get();
                        foreach ($data as $key => $value) {
                            $metode = MetodePemeriksaan::where('parameter_id', $value->id)->get();
                            $value->metode = $metode;
                            $reagen = ReagenImunologi::where('parameter_id', $value->id)->get();
                            $value->reagen = $reagen;
                            $hasilPemeriksaan = TbHp::where('parameter_id', $value->id)->get();
                            $value->hasilPemeriksaan = $hasilPemeriksaan;
                            $target = Rujukanurinalisa::where('parameter', $value->id)
                                                        ->where('siklus', $siklus)
                                                        ->where('tahun', $date)
                                                        ->where('type', $type)
                                                        ->first();
                            $value->target = $target;
                        }
                        $totalscore = 0;
                        $ikutan = 0;
                        foreach ($data as $key => $val) {
                            if($val->id <= 10){
                                if($val->id == 9){
                                    if($val->Hasil != "-" ||  $val->Hasil == "Tidak Mengerjakan"){
                                        if ($val->Hasil == "Tidak Mengerjakan") {
                                        }else{
                                            if($val->hp != NULL && $val->id == $val->parameter_id){
                                                if ($val->hp >= $val->target->rujukan) {
                                                    $total = $val->hp - $val->target->rujukan;
                                                }else{
                                                    $total = $val->target->rujukan - $val->hp ;
                                                }
                                            }else{
                                                if ($val->Hasil >= $val->target->rujukan) {
                                                    $total = $val->Hasil - $val->target->rujukan;
                                                }else{
                                                    $total = $val->target->rujukan - $val->Hasil ;
                                                }
                                            }
                                            $total = number_format($total, 3);
                                            if ($total == 0) {
                                                $totalscore = $totalscore  + 4;
                                                $ikutan++;
                                            }elseif ($total > 0 && $total <= 0.005) {
                                                $totalscore = $totalscore  + 3;
                                                $ikutan++;
                                            }elseif($total > 0.005 && $total <= 0.010){
                                                $totalscore = $totalscore  + 2;
                                                $ikutan++;
                                            }elseif($total > 0.010 && $total <= 0.015) {
                                                $totalscore = $totalscore  + 1;
                                                $ikutan++;
                                            }else{
                                                $totalscore = $totalscore  + 0;
                                                $ikutan++;
                                            }
                                        }
                                    }
                                }elseif($val->id == 10){
                                    if($val->Hasil != "-" || $val->Hasil == "Tidak Mengerjakan"){
                                        if ($val->Hasil == "Tidak Mengerjakan") {
                                        }else{
                                            if($val->hp != NULL && $val->id == $val->parameter_id){
                                                if ($val->hp >= $val->target->rujukan) {
                                                    $total = $val->hp - $val->target->rujukan;
                                                }else{
                                                    $total = $val->target->rujukan - $val->hp;
                                                }
                                            }else{
                                                if ($val->Hasil >= $val->target->rujukan) {
                                                    $total = $val->Hasil - $val->target->rujukan;
                                                }else{
                                                    $total = $val->target->rujukan - $val->Hasil ;
                                                }
                                            }
                                            if ($total == 0) {
                                                $totalscore = $totalscore  + 4;
                                                $ikutan++;
                                            }elseif ($total > 0 && $total <= 0.5) {
                                                $totalscore = $totalscore  + 3;
                                                $ikutan++;
                                            }elseif($total > 0.5 && $total <= 1.0){
                                                $totalscore = $totalscore  + 2;
                                                $ikutan++;
                                            }elseif ($total > 1.0 && $total <= 1.5) {
                                                $totalscore = $totalscore  + 1;
                                                $ikutan++;
                                            }else{
                                                $totalscore = $totalscore  + 0;
                                                $ikutan++;
                                            }
                                        }
                                    }
                                }
                            }else{
                                if(count($val->hp)){
                                    if($val->target->rujukan == 'Negatif'){
                                        if ($val->hp != $val->target->rujukan) {
                                            $totalscore = $totalscore  + 0;
                                            $ikutan++;
                                        }else{
                                            $totalscore = $totalscore  + 4;
                                            $ikutan++;
                                        }
                                    }elseif($val->target->rujukan == 'Positif'){
                                        if ($val->hp != $val->target->rujukan) {
                                            $totalscore = $totalscore  + 0;
                                            $ikutan++;
                                        }else{
                                            $totalscore = $totalscore  + 4;
                                            $ikutan++;
                                        }
                                    }elseif($val->hp == 'Negatif'){
                                        if ($val->hp != $val->target->rujukan) {
                                            $totalscore = $totalscore  + 0;
                                            $ikutan++;
                                        }else{
                                            $totalscore = $totalscore  + 4;
                                            $ikutan++;
                                        }
                                    }else{
                                        if($val->hp == $val->target->rujukan){
                                            $total = "0";
                                        }elseif ($val->hp >= $val->target->rujukan) {
                                            if($val->hp == "±"){
                                                $val->hp = "0";
                                            }elseif($val->target->rujukan == "±"){
                                                $val->target->rujukan = "0";
                                            }
                                            $total = str_replace('+', '', $val->hp) - str_replace('+', '', $val->target->rujukan);
                                        }else{
                                            if($val->hp == "±"){
                                                $val->hp = "0";
                                            }elseif($val->target->rujukan == "±"){
                                                $val->target->rujukan = "0";
                                            }
                                            $total =  str_replace('+', '', $val->target->rujukan) - str_replace('+', '', $val->hp);
                                        }
                                        if ($total == "0") {
                                            $totalscore = $totalscore  + 4;
                                            $ikutan++;
                                        }elseif($total == "1" || $total == "-1"){
                                            $totalscore = $totalscore  + 3;
                                            $ikutan++;
                                        }elseif ($total == "2" || $total == "-2") {
                                            $totalscore = $totalscore  + 2;
                                            $ikutan++;
                                        }elseif ($total == "3" || $total == "-3") {
                                            $totalscore = $totalscore  + 1;
                                            $ikutan++;
                                        }
                                    }
                                }
                            }
                        }
                    $totalscore = number_format($totalscore/$ikutan, 2);

                    $i = 0;
                    if(!empty($totalscore)){
                        $SaveData = new Skoringuri;
                        $SaveData->id_registrasi = $sb->id;
                        $SaveData->skoring = $totalscore;
                        $SaveData->bahan = $target->bahan;
                        $SaveData->siklus = $siklus;
                        $SaveData->type = $type;
                        $SaveData->tahun = $date;
                        $SaveData->catatan = "-";
                        $SaveData->save();
                    }
                }

            }
        }

        JobsEvaluasi::where('name','=',$this->nameEvaluasi)->where('status','=',0)->update(['status'=>1]);
        
    }
}

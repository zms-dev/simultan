<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;


use DB;
use App\JobsEvaluasi;


class Email2 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $dataInput = [];
    protected $usr = "";
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dataInput,$usr)
    {
        $this->dataInput = $dataInput;
        $this->usr = $usr;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->dataInput;
        $usr = $this->usr;

        $sent = Mail::send('email.email_blast', $data , function ($mail) use ($usr, $data)
        {
            $mail->from('pme.bblksub@gmail.com', 'PNPME BBLK SURABAYA ');
            $mail->to($usr->email);
            // $mail->to('tengkufirmansyah2@gmail.com');
            $mail->subject($data['sub']);
        });
    }
}

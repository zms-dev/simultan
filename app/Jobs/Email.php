<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;


use DB;
use App\JobsEvaluasi;
use App\Jobs\Email2;


class Email implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $dataInput = [];
    protected $nameEvaluasi = "";
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dataInput,$nameEvaluasi)
    {
        $this->dataInput = $dataInput;
        $this->nameEvaluasi = $nameEvaluasi;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->dataInput;
        $tahun = date('Y');
        $user = array();
        if($this->nameEvaluasi == "email-kirim-hasil"){
            $bidang = (int)$data['bidang'];
            // dd($bidang);
            $users = DB::table('users')
                        ->join('tb_registrasi','tb_registrasi.created_by','=','users.id')
                        ->where(DB::raw('Year(tb_registrasi.created_at)'), $tahun)
                        ->where('tb_registrasi.bidang', $data['bidang'])
                        ->where(function($query) use ($data)
                            {
                                if ($data['bidang'] <= 3) {
                                    if ($data['siklus'] == 1) {
                                        $query->where('status_data1', '<>' ,'2')
                                            ->orwhere('status_data2', '<>', '2')
                                            ->orwhereNull('status_data1')
                                            ->orwhereNull('status_data2');
                                    }else{
                                        $query->where('status_datarpr1', '<>' ,'2')
                                            ->orwhere('status_datarpr2', '<>', '2')
                                            ->orwhereNull('status_datarpr1')
                                            ->orwhereNull('status_datarpr2');
                                    }
                                }elseif ($data['bidang'] == 7) {
                                    if ($data['siklus'] == 1) {
                                        $query->where('status_data1', '<>' ,'2')
                                            ->orwhere('status_datarpr1', '<>', '2')
                                            ->orwhereNull('status_data1')
                                            ->orwhereNull('status_datarpr1');
                                    }else{
                                        $query->where('status_data2', '<>' ,'2')
                                            ->orwhere('status_datarpr2', '<>', '2')
                                            ->orwhereNull('status_data2')
                                            ->orwhereNull('status_datarpr2');
                                    }
                                }else{
                                    if ($data['siklus'] == 1) {
                                        $query->where('status_data1', '<>' ,'2')
                                            ->orwhereNull('status_data1');
                                    }else{
                                        $query->where('status_data2', '<>' ,'2')
                                            ->orwhereNull('status_data2');
                                    }
                                }
                            })
                        ->select('users.email')
                        ->get();
            // dd($users);
            foreach($users as $usr) {
                Email2::dispatch($data,$usr)->onConnection('redis');
            }
        }else{
            $users = DB::table('users')
                    ->join('tb_registrasi','tb_registrasi.created_by','=','users.id')
                    ->groupBy('users.id')
                    ->where('tb_registrasi.status','>=','2')
                    ->where(DB::raw('Year(tb_registrasi.created_at)'), $tahun)
                    ->where('id_member', '!=', '0')
                    ->select('email')
                    ->get();
            // dd($users);
            foreach($users as $usr) {
                Email2::dispatch($data,$usr)->onConnection('redis');
            }
        }
        JobsEvaluasi::where('name','=',$this->nameEvaluasi)->where('status','=',0)->update(['status'=>1]);
    }
}

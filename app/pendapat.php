<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class pendapat extends Model{
    protected $table = 'tb_pendapat';
    protected $fillable = ['*'];
    public $timestamps = false;
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadLaporanIdentifikasi extends Model
{
    protected $table = 'upload_lap_akhir_identifikasi';
    protected $fillable = ['*'];
}

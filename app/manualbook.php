<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class manualbook extends Model{
    protected $table = 'manual_book';
    protected $fillable = ['teaser','desc','_token','created_by']; 
}
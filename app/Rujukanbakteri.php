<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rujukanbakteri extends Model
{
    protected $table = 'tb_rujukan';
    protected $fillable = ['siklus','tahun','rujukan','lembar','jenis_pemeriksaan'];
    public $timestamps = false;
}

<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class urinalisasi extends Model{
    protected $table = 'hp_urinalisasi';
    protected $fillable = ['id_registrasi','siklus','kode_peserta','tgl_penerimaan','tgl_pemeriksaan','kualitas','berat_jenis','ph','protein','glukosa','bilirubin','urobilinogen','darah','keton','nitrit','lekosit','tes_kehamilan','jenis','catatan','penanggung_jawab','created_by'];
}
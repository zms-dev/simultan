<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
    protected $table = 'parameter';

    public function metodePemeriksaan()
    {
    	return $this->hasMany('App\MetodePemeriksaan', 'parameter_id', 'id')->where('status','=','1')->orderBy(DB::raw('FIELD(metode_pemeriksaan, "Metode lain :")'))->orderBy('metode_pemeriksaan', 'asc');
    }
    public function hasilPemeriksaan()
    {
    	return $this->hasMany('App\TbHp', 'parameter_id', 'id');
    }
    public function ReagenImunologi()
    {
    	return $this->hasMany('App\TbReagenImunologi', 'parameter_id', 'id');
    }
    public function Instrumen()
    {
        return $this->hasMany('App\Instrumen', 'id_parameter', 'id')->where('status','=','1');
    }
}

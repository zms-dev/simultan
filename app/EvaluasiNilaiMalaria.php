<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EvaluasiNilaiMalaria extends Model{
    protected $table = 'tb_evaluasi_nilai_malaria';

    protected $fillable = ['*'];
    public $timestamps = false;
}
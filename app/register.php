<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class register extends Model{
    protected $table = 'tb_registrasi';
    protected $fillable = ['perusahaan_id','bidang','status','file','siklus','pemeriksaan','created_by','sptjm','kode_lebpes','pks','updated_at'];

    public function perusahaan ()
    {
    	return $this->belongsTo('App\daftar', 'perusahaan_id', 'id');
    }
}
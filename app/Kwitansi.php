<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kwitansi extends Model{
    protected $table = 'tb_kwitansi';
    protected $fillable = ['perusahaan_id','tahun','tanggal']; 
}
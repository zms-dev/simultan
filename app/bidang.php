<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bidang extends Model{
    protected $table = 'tb_bidang';
    protected $fillable = ['bidang','created_by']; 
}
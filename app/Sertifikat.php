<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sertifikat extends Model{
    protected $table = 'tb_sertifikat';

    protected $fillable = ['*'];
    public $timestamps = false;
}
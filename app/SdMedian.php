<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SdMedian extends Model{
    protected $table = 'tb_sd_median';

    protected $fillable = ['*'];
    public $timestamps = false;
}
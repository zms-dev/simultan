<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZScoreAlat extends Model
{
    protected $table = 'tb_zscore_alat';
    protected $fillable = ['*'];

    public $timestamps = false;
}

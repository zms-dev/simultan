<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EvaluasiMalaria extends Model{
    protected $table = 'tb_evaluasi_malaria';

    protected $fillable = ['*'];
    public $timestamps = false;
}
<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlokEvaluasi extends Model{
    protected $table = 'blok_evaluasi';

    protected $fillable = ['id','user_id','siklus','tahun'];
    public $timestamps = false;
}
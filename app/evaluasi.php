<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Evaluasi extends Model{
    protected $table = 'evaluasi';
    protected $fillable = ['sertifikat','file','id_register','_token','created_by', 'type']; 
}
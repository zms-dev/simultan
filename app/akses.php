<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class akses extends Model{
    protected $table = 'users';
    protected $fillable = ['role','name','_token','email','id_member','password','penyelenggara']; 
}
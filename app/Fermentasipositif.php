<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fermentasipositif extends Model{
    protected $table = 'tb_fermentasi_positif';

    protected $fillable = ['*'];
    public $timestamps = false;
}
<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class responden extends Model{
    protected $table = 'tb_pendapat_responden';
    protected $fillable = ['*'];
    public $timestamps = false;
}
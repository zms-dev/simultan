<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ring extends Model{
    protected $table = 'tb_ring';

    protected $fillable = ['*'];
    public $timestamps = false;
}
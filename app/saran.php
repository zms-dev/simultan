<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class saran extends Model{
    protected $table = 'tb_saran';
    protected $fillable = ['*'];
    public $timestamps = false;
}
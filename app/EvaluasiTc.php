<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EvaluasiTc extends Model{
    protected $table = 'tb_evaluasi_tc';

    protected $fillable = ['*'];
    public $timestamps = false;
}
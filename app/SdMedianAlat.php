<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SdMedianAlat extends Model{
    protected $table = 'tb_sd_median_alat';

    protected $fillable = ['*'];
    public $timestamps = false;
}
<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TanggalEvaluasi extends Model{
    protected $table = 'tanggal_evaluasi';
    protected $fillable = ['*']; 
    public $timestamps = false;
}
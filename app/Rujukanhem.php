<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rujukanhem extends Model{
    protected $table = 'tb_rujukan_hematologi';
    protected $fillable = ['kode','nilai','kriteria','hasil','form','siklus','tahun'];
    public $timestamps = false;
}
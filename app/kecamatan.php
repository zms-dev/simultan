<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kecamatan extends Model{
    protected $table = 'districts';
    protected $fillable = ['regency_id', 'name'];
}
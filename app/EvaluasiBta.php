<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EvaluasiBta extends Model{
    protected $table = 'tb_evaluasi_bta';

    protected $fillable = ['*'];
    public $timestamps = false;
}
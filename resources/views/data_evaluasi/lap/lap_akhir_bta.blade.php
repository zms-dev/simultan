<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}
      
</style>
<table width="100%" cellpadding="0" border="0" style="margin-top: 25px">
        <tr align="center">
            <th width="100%" >
                <span style="font-size: 20pt; "><b>LAPORAN AKHIR</b></span><br>
                <br>
                <span style="font-size: 16pt; "><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL</b></span><br>
                <span style="font-size: 16pt; "><b>MIKROSKOPIS BTA</b></span><br>
                <span style="font-size: 16pt; "><b>SIKLUS {{$siklus}} TAHUN {{ $tanggal }}</b></span><br> 
            </th>
        </tr>
       <?php $evaluasi = DB::table('tb_hide_menu')->where('status','=','muncul')->first(); ?>
<?php 
          $kode = $data->kode_lebpes;
          $kode1 = substr($kode,5,-6);
        ?>
        <tr align="center">
            <th width="100%" >
              <pre style=""></pre><br><br><br>
                <span style="font-size: 14pt; "><b>Nomor : @if(count($data) > 0)YM.01.03/XLI.3/{{ str_replace("$kode1/", "", $kode) }}@else...@endif</b></span><br>
                <span style="font-size: 14pt; "><b>Tanggal : {{$ttd->tanggal}} {{$tanggal}}</b></span><br>
            </th>
        </tr>

        <tr align="center">
            <th width="100%" >
              <pre style=""> 
                </pre><br><br>
                <?php
                  $tahun = date('Y');
                  $taun = substr($tahun, 2);

                  $kode_p = $data->kode_lebpes; 
                  $kode_p1 = substr($kode_p, 5,-6);
                  $kode_tahun = substr($kode_p, 12);
                ?>
                <span style="font-size: 14pt; "><b>Kode Peserta: @if($data){{ $kode_p }}{{-- {{ $kode_p1 }}/ --}}{{-- {{$kode_tahun}} --}}@else...@endif</b></span><br>
            </th>
        </tr>
        <tr align="center">
            <th width="100%" >
            	<pre style=""> 
                </pre>
                   <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes2.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes2.png')}}" height="334px" width="328px">            </th>
        </tr>
        <tr align="center">
            <td width="100%" >
              <pre style=""> 
                </pre><br><br><br>
                <span style="font-size: 14pt;"><b>PENYELENGGARA&nbsp;:</b></span><br>
            </td>

        </tr>
        <tr align="center">
            <th width="100%" >
              <pre style=""> 
                </pre>
                <span style="font-size: 16pt;">KEMENTERIAN KESEHATAN RI</span><br>
                <span style="font-size: 14pt;">DIREKTORAT JENDERAL PELAYANAN KESEHATAN</span><br>
                <span style="font-size: 14pt;">BALAI BESAR LABORATORIUM KESEHATAN SURABAYA</span><br>
                <span style="font-size: 14pt;">KEMENTERIAN KESEHATAN RI</span><br>
            </th>
            
        </tr>
        <tr align="center">
            <td width="100%">
                <span style="font-size: 14pt;">Jalan karangmenjangan No.18 Surabaya 60286</span><br>
                <span style="font-size: 14pt;">Telepon pelayanan : (031) 5020306, TU : (031) 5021451 Faksimili : (031) 5020388</span><br>
                <span style="font-size: 12pt;">Website : http://bblksurabaya.id Email : pme.bblksub@gmail.com</span><br>
                <span style="font-size: 12pt;">Web Online PME : simultan.bblksurabaya.id</span><br>
            </td>
            
        </tr>

        
</table>


<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>



    
@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_hasil_evaluasi')
@section('content')
<!-- Page Content  -->

<div id="content">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">

        <button type="button" id="sidebarCollapse" class="btn btn-info">
            <i class="fa fa-align-justify"></i>
            <span></span>
        </button>
    </div>
    <label style="margin: 10px 20px 0px 0px; font-size: 28px; position: absolute; top: 0; right: 0">URINALISA</label>
</nav>
<div class="line"></div>
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{url('/urinalisasi/data-evaluasi')}}/print/{{$id}}?x={{$type}}&y={{$siklus}}" target="_blank">
    <input type="hidden" value="{{$input['bahan']}}" name="bahan">
    <div class="col-md-12" style="text-align:center;">
        <p>
            Evaluasi Urinalisasi
            <center>Siklus {{ $siklus }}</center>
        </p>  
        <input type="submit" name="simpan" value="Print" class="btn btn-info">
        {{ csrf_field() }}
    </div>          
</form>
</div>
</div>
<div class="footer">
    <div class="copy"><marquee>COPYRIGHT &copy; BBLK ALL RIGHT RESERVED - POWERED BY Pilar</marquee></div>
</div>

@if(Session::has('message'))
<div id="snackbar">{{ Session::get('message') }}</div>

<script>
    $('document').ready(function(){
      var x = document.getElementById("snackbar")
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  });
</script>
@endif

<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar-peserta').toggleClass('active');
        });
    });
</script>
@endsection

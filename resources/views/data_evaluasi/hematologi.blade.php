@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_hasil_evaluasi')
@section('content')
<!-- Sidebar  -->
<!-- Page Content  -->
<div id="content">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <button type="button" id="sidebarCollapse" class="btn btn-info">
        <i class="fa fa-align-justify"></i>
        <span></span>
      </button>
    </div>
    <label style="margin: 10px 20px 0px 0px; font-size: 28px; position: absolute; top: 0; right: 0">HEMATOLOGI</label>
  </nav>
  <div class="line"></div>
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-12" style="text-align:center;">
        @if($responSatu != NULL)
        @if(count($data))
        <?php $no = 0; ?>
        @foreach($data as $val)
        <?php 
        if($val->siklus_1 == 'done'){
          $siklus1='1';
        }elseif($val->siklus_2 == 'done'){
          $siklus2='2';
        }
        ?>
        <p>
          Laporan Akhir Hematologi
        </p>
        <a href="{{url('/laporan-akhir-hematologi/data-evaluasi/')}}/{{$id}}" class="btn btn-info" target="_blank">Sampul Laporan</a>
        <button class="btn-show btn btn-info">Laporan</button>
        <!-- <a href="{{url('/laporan-akhir-kimiaklinik/data-evaluasi/penutup/')}}/{{$id}}" class="btn btn-info" target="_blank">Penutup Laporan</a> -->
        <div class="myText">
          <br>
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default">
                <div class="panel-heading">Data Upload Laporan Akhir</div>
                <div class="panel-body">
                  <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>File</th>
                        <th>Tahun</th>
                        <th>Siklus</th>
                        <th>Parameter</th>
                        <th>Bagian</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div><br>
        <!-- <a href="{{url('asset/hasil-pemeriksaan/PENDAHULUAN%20-%20HEMATOLOGI.pdf')}}" class="btn-show btn btn-info" style="margin-top: 10px;">Pendahuluan</a> -->

        <p></p>
        @if($val->bidang == '1')
        <!-- <div class="line"></div>
        <p>Rekapitulasi Hasil</p>
        <div class="btn-group">
          <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Print Data<span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            @if($sik == 1)

            <li><a href="{{url('/data-evaluasi/rekapitulasi-hasil/hematologi')}}/{{$tanggal}}?x=1" target="_blank" >Siklus I</a></li>
            @else
            <li><a href="{{url('/data-evaluasi/rekapitulasi-hasil/hematologi')}}/{{$tanggal}}?x=2" target="_blank" >Siklus II</a></li>
            @endif
          </ul>
        </div>
         -->
        <div class="line"></div>
        <p>Hasil Saudara</p>
        <div class="btn-group">
          <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Print Data <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            @if($siklus == 1)
            <li><a href="{{url('/data-evaluasi/hematologi/')}}/print/{{$id}}?x=a&y=1" target="_blank" >I-01</a></li>
            <li><a href="{{url('/data-evaluasi/hematologi/')}}/print/{{$id}}?x=b&y=1" target="_blank" >I-02</a></li>
            @else
            <li><a href="{{url('/data-evaluasi/hematologi/')}}/print/{{$id}}?x=a&y=2" target="_blank" >II-01</a></li>
            <li><a href="{{url('/data-evaluasi/hematologi/')}}/print/{{$id}}?x=b&y=2" target="_blank" >II-02</a></li>
            @endif
          </ul>
        </div>
        <div class="line"></div>
        <div class="row">
          <!-- <div class="col-md-6">
            <p>Grafik Distribusi Hasil Peserta</p>
            <div class="btn-group">
              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Print Data <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                @if($siklus == 1)
                <li><a href="{{url('/data-evaluasi/grafik-zscore-nilai-sama/')}}/{{$id}}?x=a&y=1" target="_blank" >I-01</a></li>
                <li><a href="{{url('/data-evaluasi/grafik-zscore-nilai-sama/')}}/{{$id}}?x=b&y=1" target="_blank" >I-02</a></li>
                @else
                <li><a href="{{url('/data-evaluasi/grafik-zscore-nilai-sama/')}}/{{$id}}?x=a&y=2" target="_blank" >II-01</a></li>
                <li><a href="{{url('/data-evaluasi/grafik-zscore-nilai-sama/')}}/{{$id}}?x=b&y=2" target="_blank" >II-02</a></li>
                @endif
              </ul>
            </div>
          </div> -->
          <div class="col-md-12">
            <p>Grafik Nilai ZScore</p>
            <div class="btn-group">
              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Print Data <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                @if($siklus == 1)
                <li><a href="{{url('/hasil-pemeriksaan/hematologi/grafik-zscore-semua/')}}/{{$id}}?x=a&y=1" target="_blank" >I-01</a></li>
                <li><a href="{{url('/hasil-pemeriksaan/hematologi/grafik-zscore-semua/')}}/{{$id}}?x=b&y=1" target="_blank" >I-02</a></li>
                @else
                <li><a href="{{url('/hasil-pemeriksaan/hematologi/grafik-zscore-semua/')}}/{{$id}}?x=a&y=2" target="_blank" >II-01</a></li>
                <li><a href="{{url('/hasil-pemeriksaan/hematologi/grafik-zscore-semua/')}}/{{$id}}?x=b&y=2" target="_blank" >II-02</a></li>
                @endif
              </ul>
            </div>
          </div>
        @endif
        @endforeach
        @else
        @endif
        @else
        <h4>Harap Input <a href="{{ URL('pendapat/pendapat-responden') }}">Survey Kepuasan Pelanggan</a> Untuk Melihat Hasil Evaluasi</h4>
        @endif
        <br>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<div class=" footer" style="">
  <div class="copy"><marquee>COPYRIGHT &copy; BBLK ALL RIGHT RESERVED - POWERED BY Pilar</marquee></div>
</div>

@if(Session::has('message'))
<div id="snackbar">{{ Session::get('message') }}</div>

<script>
  $('document').ready(function(){
    var x = document.getElementById("snackbar")
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  });
</script>
@endif
@stop


@section('scriptBlock')
<script type="text/javascript">
  $(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
      $('#sidebar-peserta').toggleClass('active');
    });
  });

</script>
<script>
  var table = $(".dataTables-data");
  var dataTable = table.DataTable({
    responsive:!0,
    "serverSide":true,
    "processing":true,
    "ajax":{
      url : "{{url('/data-evaluasi/data-laporan-akhir/')}}"
    },
    dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
    language:{
      paginate:{
        previous:"&laquo;",
        next:"&raquo;"
      },search:"_INPUT_",
      searchPlaceholder:"Search..."
    },
    "columns":[
    {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
    {"data":"file","name":"file","searchable":true,"orderable":true},
    {"data":"tahun","name":"tahun","searchable":true,"orderable":true},
    {"data":"siklus","name":"siklus","searchable":true,"orderable":true},
    {"data":"alias","name":"alias","searchable":true,"orderable":true},
    {"data":"bagian","name":"bagian","searchable":true,"orderable":true},
    {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
    ],
    order:[[1,"asc"]]
  });

</script>

<script type="text/javascript">

  $(".myText").hide();
  $(".btn-show").click(function() {
    var targetna = $(".myText");
    if(targetna.css('display') == 'none'){
      console.log('show');
      $(".myText").show('100');
    }else{
      console.log('hide');
      targetna.hide('100');
    }

  });
</script>
@endsection

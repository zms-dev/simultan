<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style type="text/css">
  body{
    font-family: arial;
  }
</style>
<div class="row">
  <div class="line"> </div>
  <div class="col-md-12">
    <h4 align="center" style="margin-bottom: -10px;">1. PRESENTASE JUMLAH PESERTA</h4>
    <br>
      <div id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
  </div>
</div>
<div class="row">
  <div class="line"> </div>
  <div class="col-md-12">
    <h4 align="center" style="margin-bottom: -10px;">2. REAGEN  RPR YANG DIGUNAKAN PESERTA</h4>
    <br>
      <div id="container_rpr" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
  </div>
</div>
<div class="row" style="margin-top: 20px;">
  <div class="line"> </div>
  <div class="col-md-12">
    <h4 align="center" style="margin-bottom: -10px;">3. REAGEN  ANTI TP YANG DIGUNAKAN PESERTA</h4>
    <br>
      <div id="container_tp" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
  </div>
</div>
<div class="row">
  <div class="line"> </div>
  <div class="col-md-12">
    <h4 align="center" style="margin-bottom: -10px;">4. KESESUAIAN DENGAN RUJUKAN PER INSTANSI PARAMETER RPR</h4>
    <br>
      <div id="container_rpr2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
  </div>
</div>
<div class="row" style="margin-top: 20px;">
  <div class="line"> </div>
  <div class="col-md-12">
    <h4 align="center" style="margin-bottom: -10px;">5. KESESUAIAN DENGAN RUJUKAN PER REAGEN ANTI TP<br><small>Silahkan klik Batang / Nama Reagen untuk melihat kesesuaian rujukan</small></h4>
    <br>
      <div id="container_tp3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
  </div>
</div>
<div class="row">
  <div class="line"> </div>
  <div class="col-md-12">
    <h4 align="center" style="margin-bottom: -10px;">6. KESESUAIAN DENGAN RUJUKAN PER REAGEN RPR<br><small>Silahkan klik Batang / Nama Reagen untuk melihat kesesuaian rujukan</small></h4>
    <br>
      <div id="container_rpr3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
  </div>
</div>
<div class="row">
  <div class="line"> </div>
  <div class="col-md-12">
    <h4 align="center" style="margin-bottom: -10px;">7. NILAI PESERTA PARAMETER RPR</h4>
    <br>
      <div id="container_rpr4" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
  </div>
</div>
<div class="row" style="margin-top: 20px;">
  <div class="line"> </div>
  <div class="col-md-12">
    <h4 align="center" style="margin-bottom: -10px;">8. NILAI PESERTA PARAMETER ANTI TP</h4>
    <br>
      <div id="container_tp4" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
  </div>
</div>

<script type="text/javascript">

Highcharts.chart('container_tp4', {
    chart: {
        type: 'column'
    },
  title: {
    text: 'Grafik Nilai Peserta'
  },
  subtitle: {
    text: '(Hijau) Posisi Peserta'
  },
  xAxis: {
    categories: ['Nilai Baik', 'Nilai Kurang', 'Tidak Dapat Nilai']
  },
  yAxis: {
    title: {
      text: 'Jumlah Peserta (Anti TP)'
    }
  },
  legend: {
      enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  series: [{
    name : 'Kelompok',
    data: [{
              y: {{$grafikbaik5}},
              color: '@if($grafikpbaik5 > 0) #90ed7d @endif'
          },{
              y: {{$grafiktidakbaik5}},
              color: '@if($grafikptidakbaik5 > 0) #90ed7d @endif'
          },{
              y: {{$grafiktidakdapatnilai5}},
              color: '@if($grafikptidakdapatnilai5 > 0) #90ed7d @endif'
          }]
  }]
});

Highcharts.chart('container_rpr4', {
    chart: {
        type: 'column'
    },
  title: {
    text: 'Grafik Nilai Peserta'
  },
  subtitle: {
    text: '(Hijau) Posisi Peserta'
  },
  xAxis: {
    categories: ['Nilai Baik', 'Nilai Kurang', 'Tidak Dapat Nilai']
  },
  yAxis: {
    title: {
      text: 'Jumlah Peserta (RPR)'
    }
  },
  legend: {
      enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },
  series: [{
    name : 'Kelompok',
    data: [{
              y: {{$grafikbaikrpr5}},
              color: '@if($grafikpbaikrpr5 > 0) #90ed7d @endif'
          },{
              y: {{$grafiktidakbaikrpr5}},
              color: '@if($grafikptidakbaikrpr5 > 0) #90ed7d @endif'
          },{
              y: {{$grafiktidakdapatnilairpr5}},
              color: '@if($grafikptidakdapatnilairpr5 > 0) #90ed7d @endif'
          }]
  }]
});

Highcharts.chart('container_tp3', {
  chart: {
    type: 'column'
  },
  title: {
    text: ''
  },
  subtitle: {
    text: 'KESESUAIAN DENGAN RUJUKAN PER REAGEN ANTI TP'
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Jumlah'
    }
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
  },

  "series": [
    {
      "name": "Reagen",
      "colorByPoint": true,
      "data": [
        @foreach($grafikdata4 as $val)
        @if($val->jenis_form == "Syphilis")
        {
          "name": "{{$val->reagen}}",
          "y": {{$val->total}},
          "drilldown": "{{$val->reagen}}"
        },
        @endif
        @endforeach
      ]
    }
  ],
  "drilldown": {
    "series": [
      @foreach($grafikdata4 as $val)
      {
        "colorByPoint": true,
        "name": "{{$val->reagen}}",
        "id": "{{$val->reagen}}",
        "data": [
          [
            "Total",
            {{$val->total}}
          ],
          [
            "Sesuai",
            {{$val->sesuai}}
          ],
          [
            "Tidak Sesuai",
            {{$val->tidaksesuai}}
          ],
          [
            "Tidak Dinilai",
            {{$val->tidakdinilai}}
          ],
        ]
      },
      @endforeach
    ]
  }
});

Highcharts.chart('container_rpr3', {
  chart: {
    type: 'column'
  },
  title: {
    text: ''
  },
  subtitle: {
    text: 'KESESUAIAN DENGAN RUJUKAN PER REAGEN Anti TP'
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Jumlah'
    }
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
  },

  "series": [
    {
      "name": "Reagen",
      "colorByPoint": true,
      "data": [
        @foreach($grafikdata4 as $val)
        @if($val->jenis_form == "rpr-syphilis")
        {
          "name": "{{$val->reagen}}",
          "y": {{$val->total}},
          "drilldown": "{{$val->reagen}}"
        },
        @endif
        @endforeach
      ]
    }
  ],
  "drilldown": {
    "series": [
      @foreach($grafikdata4 as $val)
      {
        "colorByPoint": true,
        "name": "{{$val->reagen}}",
        "id": "{{$val->reagen}}",
        "data": [
          [
            "Total",
            {{$val->total}}
          ],
          [
            "Sesuai",
            {{$val->sesuai}}
          ],
          [
            "Tidak Sesuai",
            {{$val->tidaksesuai}}
          ],
          [
            "Tidak Dinilai",
            {{$val->tidakdinilai}}
          ],
        ]
      },
      @endforeach
    ]
  }
});
Highcharts.chart('container1', {
    chart: {
        type: 'pie'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: 'Rekap Peserta Instansi {{$grafikbidang1->alias}}'
    },
    tooltip: {
        pointFormat: 'Peserta : <b>{point.y}%</b>'
    },

    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [
        @foreach($grafikdata1 as $val)
          {
            name: '{{$val->badan_usaha}}',
            y: {{number_format($val->persen, 2)}},
          },
        @endforeach
        ],
        dataLabels: {
            enabled: true,
            align: 'right',
            format: '{point.y}%',
            style: {
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});

Highcharts.chart('container_rpr', {
  chart: {
      renderTo: 'container',
      defaultSeriesType: 'column',
      zoomType: 'xy'
    },
  title: {
    text: ''
  },
  subtitle: {
      text: 'KESESUAIAN DENGAN RUJUKAN PER REAGEN'
  },
  xAxis: {
    type: 'category',
    max: 5,
  },
  scrollbar: {
    enabled: true
  },
  yAxis: {
    title: {
      text: 'Jumlah'
    }
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
  },

  "series": [
    {
      "name": "Reagen",
      "colorByPoint": true,
      "data": [
        @foreach($grafikdata2 as $val)
        @if($val->jenis_form == "rpr-syphilis")
        {
          "name": "{{$val->reagen}}",
          "y": <?php $persen = $val->jumlah / $grafiktotal2 * 100 ?> {{number_format($persen, 2)}},
          @if($val->reagen == "Lain - lain")
            "drilldown": "{{$val->reagen}}"
          @endif
        },
        @endif
        @endforeach
      ],
      dataLabels: {
          enabled: true,
          format: '{point.y}%',
          style: {
              fontFamily: 'Verdana, sans-serif'
          }
      }
    }
  ],
  "drilldown": {
    "series": [
      {
        "colorByPoint": true,
        "name": "Lain - lain",
        "id": "Lain - lain",
        "data": [
        @foreach($lainlain4 as $val)
        @if($val->jenis_form == "rpr-syphilis")
          [
            "{{$val->reagen_lain}}",
            {{$val->jumlah}}
          ],
        @endif
        @endforeach
        ]
      },
    ]
  },
  scrollbar: {
    enabled:true,
    barBackgroundColor: 'gray',
    barBorderRadius: 7,
    barBorderWidth: 0,
    buttonBackgroundColor: 'gray',
    buttonBorderWidth: 0,
    buttonArrowColor: 'yellow',
    buttonBorderRadius: 7,
    rifleColor: 'yellow',
    trackBackgroundColor: 'white',
    trackBorderWidth: 1,
    trackBorderColor: 'silver',
    trackBorderRadius: 7
  }
}); 

Highcharts.chart('container_tp', {
  chart: {
      renderTo: 'container',
      defaultSeriesType: 'column',
      zoomType: 'xy'
    },
  title: {
    text: ''
  },
  subtitle: {
      text: 'KESESUAIAN DENGAN RUJUKAN PER REAGEN'
  },
  xAxis: {
    type: 'category',
    max: 10,
  },
  scrollbar: {
    enabled: true
  },
  yAxis: {
    title: {
      text: 'Jumlah'
    }
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
  },

  "series": [
    {
      "name": "Reagen",
      "colorByPoint": true,
      "data": [
        @foreach($grafikdata2 as $val)
        @if($val->jenis_form == "Syphilis")
        {
          "name": "{{$val->reagen}}",
          "y": <?php $persen = $val->jumlah / $grafiktotal2 * 100 ?> {{number_format($persen, 2)}},
          @if($val->reagen == "Lain - lain")
            "drilldown": "{{$val->reagen}}"
          @endif
        },
        @endif
        @endforeach
      ],
      dataLabels: {
          enabled: true,
          format: '{point.y}%',
          style: {
              fontFamily: 'Verdana, sans-serif'
          }
      }
    }
  ],
  "drilldown": {
    "series": [
      {
        "colorByPoint": true,
        "name": "Lain - lain",
        "id": "Lain - lain",
        "data": [
        @foreach($lainlain4 as $val)
        @if($val->jenis_form == "Syphilis")
          [
            "{{$val->reagen_lain}}",
            {{$val->jumlah}}
          ],
        @endif
        @endforeach
        ]
      },
    ]
  },
  scrollbar: {
    enabled:true,
    barBackgroundColor: 'gray',
    barBorderRadius: 7,
    barBorderWidth: 0,
    buttonBackgroundColor: 'gray',
    buttonBorderWidth: 0,
    buttonArrowColor: 'yellow',
    buttonBorderRadius: 7,
    rifleColor: 'yellow',
    trackBackgroundColor: 'white',
    trackBorderWidth: 1,
    trackBorderColor: 'silver',
    trackBorderRadius: 7
  }
});

Highcharts.chart('container_rpr2', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
        @foreach($grafikdata3 as $val)
            '{{$val->badan_usaha}}',
        @endforeach
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Peserta (SIF RPR)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            dataLabels: {
                    enabled: true,
                    crop: false,
                    overflow: 'none'
                }
        }
    },
    series: [{
        name: 'Sesuai Rujukan',
        data: [
                @foreach($grafikdata3 as $val)
                {{$val->baikrpr}},
                @endforeach
                ]

    }, {
        name: 'Tidak Sesuai Rujukan',
        data: [
                @foreach($grafikdata3 as $val)
                {{$val->tidakbaikrpr}},
                @endforeach
                ]

    }]
});

Highcharts.chart('container_tp2', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
        @foreach($grafikdata3 as $val)
            '{{$val->badan_usaha}}',
        @endforeach
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
          text: 'Peserta (Anti TP)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            dataLabels: {
                    enabled: true,
                    crop: false,
                    overflow: 'none'
                }
        }
    },
    series: [{
        name: 'Sesuai Rujukan',
        data: [
                @foreach($grafikdata3 as $val)
                {{$val->baik}},
                @endforeach
                ]

    }, {
        name: 'Tidak Sesuai Rujukan',
        data: [
                @foreach($grafikdata3 as $val)
                {{$val->tidakbaik}},
                @endforeach
                ]

    }]
});
</script>
@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')
@section('content')
   <div class="wrapper" style="margin-top: -40px;">
        <!-- Sidebar  -->
        <nav id="sidebar-peserta" style="text-align: center;">
            <div class="sidebar-header">
                <a href="{{ url('data-evaluasi') }}"><h3>Laporan Akhir</h3></a> 
                    <form method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">  
                        <select name="tanggal" class="form-control" required>
                            <option readonly>Pilih Tahun</option>
                            @for ($i=2018; $i <= date('Y'); $i++)
                                <option {{ $tanggal=="$i"?'selected':'' }} value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                      <input type="submit" name="cari" required="" class="btn btn-info" value="cari" style="margin-top: 8px;">
                    </form>
            </div>

            <ul class="list-unstyled components" style="text-align: center">        
                <li>
                 
                    @foreach($datas1 as $val)
                    <a href=" {{URL('').$val->Link}}/data-evaluasi/{{$val->id}}">{{ $val->alias }}</a>
                    @endforeach
                </li>
              </ul>                    
        </nav>
        <!-- Page Content  -->
        
    <div id="content">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fa fa-align-justify"></i>
                    <span></span>
                </button>
            </div>
            <label style="margin: 10px 20px 0px 0px; font-size: 28px; position: absolute; top: 0; right: 0">URINALISA</label>
        </nav>
        <div class="line"></div>
            <form class="form-horizontal" method="post" enctype="multipart/form-data" action="" target="_blank">            
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Skoring Hasil PNPME Seluruh Peserta I-01</div>
                            <a style="margin: 10px" onclick="javascript:printDiv('datana')"><li class="glyphicon glyphicon-print"></li></a>
                            
                            <div class="panel-body" id="datana">
                                <div id="container"></div>
                            </div>
                        </div>
                    </div>
                
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Skoring Hasil PNPME Seluruh Peserta I-02</div>
                            <a style="margin: 10px" onclick="javascript:printDiv1('datana1')"><li class="glyphicon glyphicon-print"></li></a>
                            
                            <div class="panel-body" id="datana1">
                                <div id="container1"></div>
                            </div>
                        </div>
                    </div>
                </div>      
            </form>
        </div>
    </div>
    <div class="footer">
        <div class="copy"><marquee>COPYRIGHT &copy; BBLK ALL RIGHT RESERVED - POWERED BY Pilar</marquee></div>
    </div>

@if(Session::has('message'))
    <div id="snackbar">{{ Session::get('message') }}</div>

    <script>
    $('document').ready(function(){
          var x = document.getElementById("snackbar")
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    });
    </script>
@endif
 
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar-peserta').toggleClass('active');
            });
        });
    </script>

<script type="text/javascript">
function printDiv(divID) {
    //Get the HTML of div
    var divElements = document.getElementById(divID).innerHTML;
    //Get the HTML of whole page
    var oldPage = document.body.innerHTML;

    //Reset the page's HTML with div's HTML only
    document.body.innerHTML =
      "<html><head><title></title></head><body>" +
      divElements + "</body>";

    //Print Page
    window.print();

    //Restore orignal HTML
    document.body.innerHTML = oldPage;


}
var chart = Highcharts.chart('container', {
  title: {text: 'GRAFIK SCORING HASIL PNPME BIDANG URINALISA <br>BERDASARKAN KELOMPOK SELURUH PESERTA SIKLUS 1 - 01 Tahun {{$dd}}'},
  subtitle: {text: ''},
  xAxis: {categories: [
      @foreach($data as $val)
       '{{$val->nilainya}}',
      @endforeach]},
  yAxis: {
    title: {
      text : 'Jumlah Peserta'
    }
  },
  plotOptions: {
      series: {
          borderWidth: 0,dataLabels: { 
              enabled: true,
              format:'{point.y}'
          }
      }
  },
  series: [{type: 'column',
      name : 'Kriteria Nilai',
      data: [],
      showInLegend: true}]
});

$.getJSON("{{ URL('data-evaluasi/skoring/urinalisa/peserta-c/')}}",{siklus:1, tahun:{{$dd}}, type:'a'}, function(json){
      chart.series[0].setData(json[0],true)
    });

function printDiv1(divID1) {
    //Get the HTML of div
    var divElements = document.getElementById(divID1).innerHTML;
    //Get the HTML of whole page
    var oldPage = document.body.innerHTML;

    //Reset the page's HTML with div's HTML only
    document.body.innerHTML =
      "<html><head><title></title></head><body>" +
      divElements + "</body>";

    //Print Page
    window.print();

    //Restore orignal HTML
    document.body.innerHTML = oldPage;


}
var chart1 = Highcharts.chart('container1', {
  title: {text: 'GRAFIK SCORING HASIL PNPME BIDANG URINALISA <br>BERDASARKAN KELOMPOK SELURUH PESERTA SIKLUS 1 - 02 Tahun {{$dd}}'},
  subtitle: {text: ''},
  xAxis: {categories: [
      @foreach($data as $val)
       '{{$val->nilainya}}',
      @endforeach]},
  yAxis: {
    title: {
      text : 'Jumlah Peserta'
    }
  },
  plotOptions: {
      series: {
          borderWidth: 0,dataLabels: { 
              enabled: true,
              format:'{point.y}'
          }
      }
  },
  series: [{type: 'column',
      name : 'Kriteria Nilai',
      data: [],
      showInLegend: true}]
});

$.getJSON("{{ URL('data-evaluasi/skoring/urinalisa/peserta-c/')}}",{siklus:1, tahun:{{$dd}}, type:'b'}, function(json){
      chart1.series[0].setData(json[0],true)
    });
</script>
@endsection

<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}
          {{--   <th>
                <img alt="" src="http://webdesign.zamasco.com/img/logo-kemenkes.png" height="120px">
            </th> --}}
</style>
<table width="100%" cellpadding="0" border="0" style="margin-top: 25px">
        <tr align="center">
            <th width="100%" >
                <span style="font-size: 25px; "><b>LAPORAN AKHIR</b></span><br>
                <br>
                <span style="font-size: 18px; "><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL</b></span><br>
                <span style="font-size: 18px; "><b>HEMATOLOGI</b></span><br>
                <span style="font-size: 18px; "><b>SIKLUS 1 TAHUN 2018</b></span><br> 
            </th>
        </tr>
       
        <tr align="center">
            <th width="100%" >
            	<pre style=""></pre><br><br><br>
                <span style="font-size: 17px; "><b>Nomor :YM.01.03/XLI.3/001-sik2/2017</b></span><br>
                <span style="font-size: 17px; "><b>Tanggal :30 Nopember 2017</b></span><br>
            </th>
        </tr>

        <tr align="center">
            <th width="100%" >
            	<pre style=""> 
                </pre><br><br>
                <span style="font-size: 15px; "><b>Kode Peserta: 001/HEM1/17</b></span><br>
            </th>
        </tr>
        <tr align="center">
            <th width="100%" >
            	<pre style=""> 
                </pre>
                    <img alt="" src="{{ asset('asset/img/kimkes1.jpg') }}" height="330px">
            </th>
        </tr>
        <tr align="center">
            <td width="100%" >
            	<pre style=""> 
                </pre><br><br><br>
                <span style="font-size: 17px;">PENYELENGGARA&nbsp;:</span><br>
            </td>

        </tr>
        



        <tr align="center">
            <th width="100%" >
            	<pre style=""> 
                </pre>
                <span style="font-size: 15px;">KEMENTERIAN KESEHATAN RI</span><br>
                <span style="font-size: 15px;">DIREKTORAT JENDERAL PELAYANAN KESEHATAN</span><br>
				<span style="font-size: 15px;">BALAI BESAR LABORATORIUM KESEHATAN SURABAYA</span><br>
				<span style="font-size: 15px;">KEMENTERIAN KESEHATAN RI</span><br>
            </th>
            
        </tr>
        <tr align="center">
            <td width="100%" >
                <span style="font-size: 13px;">Jalan karangmenjangan No.18 Surabaya60286</span><br>
                <span style="font-size: 13px;">Telepon pelayanan : (031) 5020306, TU : (031) 5021451 Faksimili : (031) 5020388</span><br>
				<span style="font-size: 13px;">Website : http://www.bbksurabaya.com Email : pme.bblksub@gmail.com</span><br>
            </td>
            
        </tr>

        
</table>


<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>



    
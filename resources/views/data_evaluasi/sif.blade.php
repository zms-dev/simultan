@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_hasil_evaluasi')
@section('content')
<!-- Page Content  -->
<div id="content">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">

      <button type="button" id="sidebarCollapse" class="btn btn-info">
        <i class="fa fa-align-justify"></i>
        <span></span>
      </button>
    </div>
    <label style="margin: 10px 20px 0px 0px; font-size: 28px; position: absolute; top: 0; right: 0">SIFILIS</label>
  </nav>
  <div class="line"></div>
  @if($responSatu != NULL)
  @if(count($data)>0)
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-12" style="text-align:center;">
        <p>
          Laporan Akhir Sifilis
        </p>
        <a href="{{url('/laporan-akhir-sif/data-evaluasi/')}}/{{$id}}" class="btn btn-info" target="_blank">Sampul Laporan</a>
        <button class="btn-show btn btn-info">Laporan</button>
        {{-- <a href="{{url('/laporan-akhir-sif/data-evaluasi/penutup/')}}/{{$id}}" class="btn btn-info" target="_blank">Penutup Laporan</a> --}}

        <br><br>
        <div class="myText">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default">
                <div class="panel-heading">Data Upload Laporan Akhir</div>
                <div class="panel-body">

                  <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>File</th>
                        <th>Tahun</th>
                        <th>Siklus</th>
                        <th>Parameter</th>
                        <th>Bagian</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>  
  <div class="line"></div>
  @foreach($data as $val)
  <?php 
  if($val->siklus_1 == 'done'){
    $siklus1=1;
  }elseif($val->siklus_2 == 'done'){
    $siklus2=2;
  }
  ?>
  @if($val->bidang > '5')
  @if($responSatu != NULL)
  <div class="row">
    <div class="col-md-12" >
      <div class="col-md-12" style="text-align:center;">
        <p>
          Hasil Saudara
        </p>

        <div class="btn-group">
          <button type="button"  class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Print Data <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            @if($val->bidang == '7')
            @if($siklus == 1)
            @if($val->siklus_1 == 'done')
            @if($val->status_data1 == 2)
            <li><a href="{{url('syphilis/data-evaluasi/print/')}}/{{$id}}?y=1" target="_blank">Anti TP</a></li>
            @endif
            @if($val->status_datarpr1 == 2)
            <li><a href="{{url('syphilis/data-evaluasi/rpr/print/')}}/{{$id}}?y=1" target="_blank">RPR</a></li>
            @endif
            @else
            @if($val->rpr1 == 'done')
            @if($val->status_data1 == 2)
            <li><a href="{{url('syphilis/data-evaluasi/print/')}}/{{$id}}?y=1" target="_blank">Anti TP</a></li>
            @endif
            @if($val->status_datarpr1 == 2)
            <li><a href="{{url('syphilis/data-evaluasi/rpr/print/')}}/{{$id}}?y=1" target="_blank">RPR</a></li>
            @endif
            @endif
            @endif
            @elseif($siklus == 2)
            @if($val->siklus_2 == 'done')
            @if($val->rpr2 == 'done')
            @if($val->status_data2 == 2)
            <li><a href="{{url('syphilis/data-evaluasi/print/')}}/{{$id}}?y=2" target="_blank">Anti TP</a></li>
            @endif
            @if($val->status_datarpr2 == 2)
            <li><a href="{{url('syphilis/data-evaluasi/rpr/print/')}}/{{$id}}?y=2" target="_blank">RPR</a></li>
            @endif
            @else
            @if($val->status_data2 == 2)
            <li><a href="{{url('syphilis/data-evaluasi/print/')}}/{{$id}}?y=2" target="_blank">Anti TP</a></li>
            @endif
            @endif
            @else
            @if($val->rpr2 == 'done')
            @if($val->status_data2 == 2)
            <li><a href="{{url('syphilis/data-evaluasi/print/')}}/{{$id}}?y=2" target="_blank">Anti TP</a></li>
            @endif
            @if($val->status_datarpr2 == 2)
            <li><a href="{{url('syphilis/data-evaluasi/rpr/print/')}}/{{$id}}?y=2" target="_blank">RPR</a></li>
            @endif
            @else
            @if($val->status_data2 == 2)
            <li><a href="{{url('syphilis/data-evaluasi/print/')}}/{{$id}}?y=2" target="_blank">Anti TP</a></li>
            @endif
            @endif
            @endif
            @endif
            @endif
          </ul>
        </div>  
      </div>
    </div>
  </div>
  @endif
  @endif
  @endforeach
  <div class="row">
    <div class="line"> </div>
    <div class="col-md-12">
      <h4 align="center" style="margin-bottom: -10px;">1. PRESENTASE JUMLAH PESERTA</h4>
      <br>
      <div id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
  </div>
  <div class="row">
    <div class="line"> </div>
    <div class="col-md-12">
      <h4 align="center" style="margin-bottom: -10px;">2. REAGEN  RPR YANG DIGUNAKAN PESERTA</h4>
      <br>
      <div id="container_rpr" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
  </div>
  <div class="row" style="margin-top: 20px;">
    <div class="line"> </div>
    <div class="col-md-12">
      <h4 align="center" style="margin-bottom: -10px;">3. REAGEN  ANTI TP YANG DIGUNAKAN PESERTA</h4>
      <br>
      <div id="container_tp" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
  </div>
  <div class="row">
    <div class="line"> </div>
    <div class="col-md-12">
      <h4 align="center" style="margin-bottom: -10px;">4. KESESUAIAN DENGAN RUJUKAN PER INSTANSI PARAMETER RPR</h4>
      <br>
      <div id="container_rpr2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
  </div>
  <div class="row" style="margin-top: 20px;">
    <div class="line"> </div>
    <div class="col-md-12">
      <h4 align="center" style="margin-bottom: -10px;">5. KESESUAIAN DENGAN RUJUKAN PER REAGEN ANTI TP<br><small>Silahkan klik Batang / Nama Reagen untuk melihat kesesuaian rujukan</small></h4>
      <br>
      <div id="container_tp3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
  </div>
  <div class="row">
    <div class="line"> </div>
    <div class="col-md-12">
      <h4 align="center" style="margin-bottom: -10px;">6. KESESUAIAN DENGAN RUJUKAN PER REAGEN RPR<br><small>Silahkan klik Batang / Nama Reagen untuk melihat kesesuaian rujukan</small></h4>
      <br>
      <div id="container_rpr3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
  </div>
  <div class="row">
    <div class="line"> </div>
    <div class="col-md-12">
      <h4 align="center" style="margin-bottom: -10px;">7. NILAI PESERTA PARAMETER RPR</h4>
      <br>
      <div id="container_rpr4" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
  </div>
  <div class="row" style="margin-top: 20px;">
    <div class="line"> </div>
    <div class="col-md-12">
      <h4 align="center" style="margin-bottom: -10px;">8. NILAI PESERTA PARAMETER ANTI TP</h4>
      <br>
      <div id="container_tp4" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
  </div>
  @else
  <p style="font-size: 30px; text-align: center;">Tidak Ada Data</p>
  @endif   
  @else
  <h4>Harap Input <a href="{{ URL('pendapat/pendapat-responden') }}">Survey Kepuasan Pelanggan</a> Untuk Melihat Hasil Evaluasi</h4>
  @endif     
</div>
</div>
<!-- Footer -->
<div class="footer">
  <div class=" copy"><marquee>COPYRIGHT &copy; BBLK ALL RIGHT RESERVED - POWERED BY Pilar</marquee></div>
</div>
@if(Session::has('message'))
<div id="snackbar">{{ Session::get('message') }}</div>

<script>
  $('document').ready(function(){
    var x = document.getElementById("snackbar")
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  });
</script>
@endif

@endsection
@section('scriptBlock')
<script type="text/javascript">
  $(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
      $('#sidebar-peserta').toggleClass('active');
    });
  });
</script>
<script>
  var table = $(".dataTables-data");
  var dataTable = table.DataTable({
    responsive:!0,
    "serverSide":true,
    "processing":true,
    "ajax":{
      url : "{{url('/data-evaluasi/data-laporan-akhir/sif')}}"
    },
    dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
    language:{
      paginate:{
        previous:"&laquo;",
        next:"&raquo;"
      },search:"_INPUT_",
      searchPlaceholder:"Search..."
    },
    "columns":[
    {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
    {"data":"file","name":"file","searchable":true,"orderable":true},
    {"data":"tahun","name":"tahun","searchable":true,"orderable":true},
    {"data":"siklus","name":"siklus","searchable":true,"orderable":true},
    {"data":"alias","name":"alias","searchable":true,"orderable":true},
    {"data":"bagian","name":"bagian","searchable":true,"orderable":true},
    {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
    ],
    order:[[1,"asc"]]
  });
</script>
<script type="text/javascript">

  $(".myText").hide();
  $(".btn-show").click(function() {
    var targetna = $(".myText");
    if(targetna.css('display') == 'none'){
      console.log('show');
      $(".myText").show('100');
    }else{
      console.log('hide');
      targetna.hide('100');
    }
  });

  Highcharts.chart('container_tp4', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Grafik Nilai Peserta'
    },
    subtitle: {
      text: '(Hijau) Posisi Peserta'
    },
    xAxis: {
      categories: ['Nilai Baik', 'Nilai Kurang', 'Tidak Dapat Nilai']
    },
    yAxis: {
      title: {
        text: 'Jumlah Peserta (Anti TP)'
      }
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: '{point.y}'
        }
      }
    },
    series: [{
      name : 'Kelompok',
      data: [{
        y: {{$grafikbaik5}},
        color: '@if($grafikpbaik5 > 0) #90ed7d @endif'
      },{
        y: {{$grafiktidakbaik5}},
        color: '@if($grafikptidakbaik5 > 0) #90ed7d @endif'
      },{
        y: {{$grafiktidakdapatnilai5}},
        color: '@if($grafikptidakdapatnilai5 > 0) #90ed7d @endif'
      }]
    }]
  });

  Highcharts.chart('container_rpr4', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Grafik Nilai Peserta'
    },
    subtitle: {
      text: '(Hijau) Posisi Peserta'
    },
    xAxis: {
      categories: ['Nilai Baik', 'Nilai Kurang', 'Tidak Dapat Nilai']
    },
    yAxis: {
      title: {
        text: 'Jumlah Peserta (RPR)'
      }
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: '{point.y}'
        }
      }
    },
    series: [{
      name : 'Kelompok',
      data: [{
        y: {{$grafikbaikrpr5}},
        color: '@if($grafikpbaikrpr5 > 0) #90ed7d @endif'
      },{
        y: {{$grafiktidakbaikrpr5}},
        color: '@if($grafikptidakbaikrpr5 > 0) #90ed7d @endif'
      },{
        y: {{$grafiktidakdapatnilairpr5}},
        color: '@if($grafikptidakdapatnilairpr5 > 0) #90ed7d @endif'
      }]
    }]
  });

  Highcharts.chart('container_tp3', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    subtitle: {
      text: 'KESESUAIAN DENGAN RUJUKAN PER REAGEN ANTI TP'
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Jumlah'
      }
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: '{point.y}'
        }
      }
    },

    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
    },

    "series": [
    {
      "name": "Reagen",
      "colorByPoint": true,
      "data": [
      @foreach($grafikdata4 as $val)
      @if($val->jenis_form == "Syphilis")
      {
        "name": "{{$val->reagen}}",
        "y": {{$val->total}},
        "drilldown": "{{$val->reagen}}"
      },
      @endif
      @endforeach
      ]
    }
    ],
    "drilldown": {
      "series": [
      @foreach($grafikdata4 as $val)
      {
        "colorByPoint": true,
        "name": "{{$val->reagen}}",
        "id": "{{$val->reagen}}",
        "data": [
        [
        "Total",
        {{$val->total}}
        ],
        [
        "Sesuai",
        {{$val->sesuai}}
        ],
        [
        "Tidak Sesuai",
        {{$val->tidaksesuai}}
        ],
        [
        "Tidak Dinilai",
        {{$val->tidakdinilai}}
        ],
        ]
      },
      @endforeach
      ]
    }
  });

  Highcharts.chart('container_rpr3', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    subtitle: {
      text: 'KESESUAIAN DENGAN RUJUKAN PER REAGEN Anti TP'
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Jumlah'
      }
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: '{point.y}'
        }
      }
    },

    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
    },

    "series": [
    {
      "name": "Reagen",
      "colorByPoint": true,
      "data": [
      @foreach($grafikdata4 as $val)
      @if($val->jenis_form == "rpr-syphilis")
      {
        "name": "{{$val->reagen}}",
        "y": {{$val->total}},
        "drilldown": "{{$val->reagen}}"
      },
      @endif
      @endforeach
      ]
    }
    ],
    "drilldown": {
      "series": [
      @foreach($grafikdata4 as $val)
      {
        "colorByPoint": true,
        "name": "{{$val->reagen}}",
        "id": "{{$val->reagen}}",
        "data": [
        [
        "Total",
        {{$val->total}}
        ],
        [
        "Sesuai",
        {{$val->sesuai}}
        ],
        [
        "Tidak Sesuai",
        {{$val->tidaksesuai}}
        ],
        [
        "Tidak Dinilai",
        {{$val->tidakdinilai}}
        ],
        ]
      },
      @endforeach
      ]
    }
  });
  Highcharts.chart('container1', {
    chart: {
      type: 'pie'
    },
    title: {
      text: ''
    },
    subtitle: {
      text: 'Rekap Peserta Instansi {{$grafikbidang1->alias}}'
    },
    tooltip: {
      pointFormat: 'Peserta : <b>{point.y}%</b>'
    },

    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false
        },
        showInLegend: true
      }
    },
    series: [{
      name: 'Brands',
      colorByPoint: true,
      data: [
      @foreach($grafikdata1 as $val)
      {
        name: '{{$val->badan_usaha}}',
        y: {{number_format($val->persen, 2)}},
      },
      @endforeach
      ],
      dataLabels: {
        enabled: true,
        align: 'right',
        format: '{point.y}%',
        style: {
          fontFamily: 'Verdana, sans-serif'
        }
      }
    }]
  });

  Highcharts.chart('container_rpr', {
    chart: {
      renderTo: 'container',
      defaultSeriesType: 'column',
      zoomType: 'xy'
    },
    title: {
      text: ''
    },
    subtitle: {
      text: 'KESESUAIAN DENGAN RUJUKAN PER REAGEN'
    },
    xAxis: {
      type: 'category',
      max: 5,
    },
    scrollbar: {
      enabled: true
    },
    yAxis: {
      title: {
        text: 'Jumlah'
      }
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: '{point.y}'
        }
      }
    },

    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
    },

    "series": [
    {
      "name": "Reagen",
      "colorByPoint": true,
      "data": [
      @foreach($grafikdata2 as $val)
      @if($val->jenis_form == "rpr-syphilis")
      {
        "name": "{{$val->reagen}}",
        "y": <?php $persen = $val->jumlah / $grafiktotal2 * 100 ?> {{number_format($persen, 2)}},
        @if($val->reagen == "Lain - lain")
        "drilldown": "{{$val->reagen}}"
        @endif
      },
      @endif
      @endforeach
      ],
      dataLabels: {
        enabled: true,
        format: '{point.y}%',
        style: {
          fontFamily: 'Verdana, sans-serif'
        }
      }
    }
    ],
    "drilldown": {
      "series": [
      {
        "colorByPoint": true,
        "name": "Lain - lain",
        "id": "Lain - lain",
        "data": [
        @foreach($lainlain4 as $val)
        @if($val->jenis_form == "rpr-syphilis")
        [
        "{{$val->reagen_lain}}",
        {{$val->jumlah}}
        ],
        @endif
        @endforeach
        ]
      },
      ]
    },
    scrollbar: {
      enabled:true,
      barBackgroundColor: 'gray',
      barBorderRadius: 7,
      barBorderWidth: 0,
      buttonBackgroundColor: 'gray',
      buttonBorderWidth: 0,
      buttonArrowColor: 'yellow',
      buttonBorderRadius: 7,
      rifleColor: 'yellow',
      trackBackgroundColor: 'white',
      trackBorderWidth: 1,
      trackBorderColor: 'silver',
      trackBorderRadius: 7
    }
  }); 

  Highcharts.chart('container_tp', {
    chart: {
      renderTo: 'container',
      defaultSeriesType: 'column',
      zoomType: 'xy'
    },
    title: {
      text: ''
    },
    subtitle: {
      text: 'KESESUAIAN DENGAN RUJUKAN PER REAGEN'
    },
    xAxis: {
      type: 'category',
      max: 10,
    },
    scrollbar: {
      enabled: true
    },
    yAxis: {
      title: {
        text: 'Jumlah'
      }
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: '{point.y}'
        }
      }
    },

    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
    },

    "series": [
    {
      "name": "Reagen",
      "colorByPoint": true,
      "data": [
      @foreach($grafikdata2 as $val)
      @if($val->jenis_form == "Syphilis")
      {
        "name": "{{$val->reagen}}",
        "y": <?php $persen = $val->jumlah / $grafiktotal2 * 100 ?> {{number_format($persen, 2)}},
        @if($val->reagen == "Lain - lain")
        "drilldown": "{{$val->reagen}}"
        @endif
      },
      @endif
      @endforeach
      ],
      dataLabels: {
        enabled: true,
        format: '{point.y}%',
        style: {
          fontFamily: 'Verdana, sans-serif'
        }
      }
    }
    ],
    "drilldown": {
      "series": [
      {
        "colorByPoint": true,
        "name": "Lain - lain",
        "id": "Lain - lain",
        "data": [
        @foreach($lainlain4 as $val)
        @if($val->jenis_form == "Syphilis")
        [
        "{{$val->reagen_lain}}",
        {{$val->jumlah}}
        ],
        @endif
        @endforeach
        ]
      },
      ]
    },
    scrollbar: {
      enabled:true,
      barBackgroundColor: 'gray',
      barBorderRadius: 7,
      barBorderWidth: 0,
      buttonBackgroundColor: 'gray',
      buttonBorderWidth: 0,
      buttonArrowColor: 'yellow',
      buttonBorderRadius: 7,
      rifleColor: 'yellow',
      trackBackgroundColor: 'white',
      trackBorderWidth: 1,
      trackBorderColor: 'silver',
      trackBorderRadius: 7
    }
  });

  Highcharts.chart('container_rpr2', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    subtitle: {
      text: ''
    },
    xAxis: {
      categories: [
      @foreach($grafikdata3 as $val)
      '{{$val->badan_usaha}}',
      @endforeach
      ],
      crosshair: true
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Peserta (SIF RPR)'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y}</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        dataLabels: {
          enabled: true,
          crop: false,
          overflow: 'none'
        }
      }
    },
    series: [{
      name: 'Sesuai Rujukan',
      data: [
      @foreach($grafikdata3 as $val)
      {{$val->baikrpr}},
      @endforeach
      ]

    }, {
      name: 'Tidak Sesuai Rujukan',
      data: [
      @foreach($grafikdata3 as $val)
      {{$val->tidakbaikrpr}},
      @endforeach
      ]

    }]
  });

  Highcharts.chart('container_tp2', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    subtitle: {
      text: ''
    },
    xAxis: {
      categories: [
      @foreach($grafikdata3 as $val)
      '{{$val->badan_usaha}}',
      @endforeach
      ],
      crosshair: true
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Peserta (Anti TP)'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y}</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        dataLabels: {
          enabled: true,
          crop: false,
          overflow: 'none'
        }
      }
    },
    series: [{
      name: 'Sesuai Rujukan',
      data: [
      @foreach($grafikdata3 as $val)
      {{$val->baik}},
      @endforeach
      ]

    }, {
      name: 'Tidak Sesuai Rujukan',
      data: [
      @foreach($grafikdata3 as $val)
      {{$val->tidakbaik}},
      @endforeach
      ]

    }]
  });
</script>
@endsection
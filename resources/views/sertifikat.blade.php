<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
	@page { margin: 0px; }
	body { 
		margin: 0px; 
    	font-family: Helvetica;
    	color: #333;
	}
</style>
<body>
<div style="margin-top: 10px; position: absolute;">
	<img src="{{URL::asset('asset/img/serifikat.jpg')}}" width="100%" height="750px">
</div>
<div style="z-index: 1; position: absolute; width: 100%; padding-top: 250px">
	<p style="font-size: 18px; margin-bottom: 40px; text-align: center;">Nomor : {{$data->kode_lebpes}}</p>
	<center><h1 style="margin-bottom: 60px">{{$data->nama_lab}}</h1></center>
	<center><h3>Siklus {{$data->siklus}} Tahun {{$data->tahun}}<br>Bidang Pengujian {{$data->bidang}}<br></h3><h4><div style="width: 100%; padding: 0px 370px; position: fixed; bottom: 265px">{{$data->parameter}}</div></h4></center>
	<div style="width: 100%; text-align: right; margin-right: 175px; top: 0; padding-top: 555px; position: fixed;"><p style="margin-top: 0px"><b>{{ date('d F Y', strtotime($data->created_at)) }}</b></p></div>
</div>
</body>
</html>
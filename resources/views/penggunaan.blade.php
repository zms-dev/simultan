@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Download Tutorial Penggunaan</div>

                <div class="panel-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Video</th>
                            <th>Download</th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>
                        <tr>
                            <td>{{$no}}</td>
                            <td>{{$val->video}}</td>
                            <td><center>
                                <a href="{{URL::asset('asset/backend/penggunaan').'/'.$val->video}}" target="_blank">
                                    <i class="glyphicon glyphicon-cloud-download"></i>
                                </a>
                                </center>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="3">Null</td>
                        </tr>
                        @endif

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
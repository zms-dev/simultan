@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container-fluid">
<!-- Form Login -->
    <div id="carousel-example-generic" class="row carousel slide" data-ride="carousel" data-pause="none" style="margin-top: -20px">
      <!-- Indicators -->
      <ol class="carousel-indicators">
      @if(count($banner))
      <?php $i = 0;?>
      @foreach($banner as $val)
      <?php $i++;?>
        @if($i == 1)
        <li data-target="#carousel-example-generic" data-slide-to="{{$i}}" class="active"></li>
        @else
        <li data-target="#carousel-example-generic" data-slide-to="{{$i}}" class=""></li>
        @endif
      @endforeach
      @endif
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">      
      @if(count($banner))
      <?php $i = 0;?>
      @foreach($banner as $val)
      <?php $i++;?>
      @if($i == 1)
        <div class="item active">
          <img src="{{URL::asset('asset/backend/banner').'/'.$val->image}}" align="middle">
          <div class="carousel-caption">
          </div>
        </div>
        @else
        <div class="item">
          <img src="{{URL::asset('asset/backend/banner').'/'.$val->image}}" align="middle">
          <div class="carousel-caption">
          </div>
        </div>
        @endif
      @endforeach
      @endif
      </div>
      <!-- Controls -->
      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
<!-- Footer --><!-- Large modal -->
    <div class="row footer">
        <div class="col-md-12 copy"><marquee>COPYRIGHT &copy; BBLK ALL RIGHT RESERVED - POWERED BY Pilar</marquee></div>
    </div>
</div>

<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="margin-top: 15%">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3><center><b>
          UNTUK MENGAKSES MENU HASIL EVALUASI SILAHKAN TERLEBIH DAHULU MENGISI MENU<br>
          KOMENTAR DAN SARAN -> PENDAPAT RESPONDEN
        </b></center></h3>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
   // $('document').ready(function(){        
   // $('#myModal').modal('show');
    // }); 
</script>
@if(Session::has('message'))
    <div id="snackbar">{{ Session::get('message') }}</div>

    <script>
    $('document').ready(function(){
          var x = document.getElementById("snackbar")
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    });
    </script>
@endif
@endsection
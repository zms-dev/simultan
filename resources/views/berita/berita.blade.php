  @extends('layouts.app')
  @extends('layouts.menu')
  @extends('layouts.menu_dashboard')
  @extends('layouts.menu_laporan')
  @extends('layouts.menu_evaluasi')


  @section('content')
  <div class="container">
      <div class="row">
          <div class="col-md-12">
              <div class="panel panel-default">
                  <div class="panel-heading">Berita PNPME</div>
                  <div class="panel-body">
                    @foreach($data as $val)
                    <div class="media row">
                      <div class="col-md-4" style="max-height: 150px">
                        <a href="#">
                          <img class="media-object" src="{{URL::asset('asset/backend/berita').'/'.$val->img}}" alt="{{URL::asset('asset/backend/berita').'/'.$val->img}}" width="100%">
                        </a>
                      </div>
                      <div class="col-md-8">
                        <a href="{{url('berita').'/'.$val->id}}"><h2 class="media-heading">{{$val->judul}}</h2></a>
                        <b>{{date("Y M d - H:i", strtotime($val->updated_at))}}</b><br>
                        {!! strip_tags(str_limit($val->isi, 400)) !!}
                      </div>
                    </div>
                    @endforeach
                  </div>
              </div>
          </div>
      </div>
  </div>
  @endsection
  @extends('layouts.app')
  @extends('layouts.menu')
  @extends('layouts.menu_dashboard')
  @extends('layouts.menu_laporan')
  @extends('layouts.menu_evaluasi')


  @section('content')
  <div class="container">
      <div class="row">
          <div class="col-md-12">
              <div class="panel panel-default">
                  <div class="panel-heading">{{$data->judul}} <font style="float: right;">{{date("Y M d - H:i", strtotime($data->updated_at))}}</font></div>
                  <div class="panel-body">
                    <div class="media row">
                      <div class="col-md-12" style="max-height: 300px; overflow: hidden;">
                        <a href="#">
                          <img class="media-object" src="{{URL::asset('asset/backend/berita').'/'.$data->img}}" alt="{{URL::asset('asset/backend/berita').'/'.$data->img}}" width="100%">
                        </a>
                      </div>
                      <div class="col-md-12">
                        <br>
                        {!! $data->isi !!}
                      </div>
                    </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  @endsection
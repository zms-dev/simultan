@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Formulir Pendaftaran</div>

                <div class="panel-body">
                    @if(Session::has('message'))
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ Session::get('message') }}
                      </div>
                    @endif
                    <form class="form-horizontal" id="myform" action="{{url('daftar')}}" method="post" enctype="multipart/form-data"  >
                      <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Laboratorium</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="nama_lab" name="nama_lab" placeholder="Nama Laboratorium" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="pemerintah" class="col-sm-3 control-label">Jenis FasYanKes</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="pemerintah" id="pemerintah" required <!-- onchange="pmi()" -->>
                                <option></option>
                                @if(count($badan))
                                @foreach($badan as $val)
                                <option value="{{$val->id}}">{{$val->badan_usaha}}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="akreditasi" class="col-sm-3 control-label">Status Akreditasi</label>
                        <div class="col-sm-9">
                            <input id="status_akreditasi" type="text" class="form-control" name="akreditasi" value="" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="pemantapan_mutu" class="col-sm-3 control-label">Pemantapan Mutu Internal</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="pemantapan_mutu" id="pemantapan_mutu" required onchange="pemantapanmutu()">
                                <option></option>
                                <option value="Dilaksanakan">Dilaksanakan</option>
                                <option value="Tidak Dilaksanakan">Tidak Dilaksanakan</option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group" id="dilaksanakandiv">
                        <label for="dilaksanakan" class="col-sm-3 control-label">Pilih Parameter</label>
                        <div class="col-sm-9">
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" value="Hematologi">Hematologi<br>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" value="Kimia Klinik">Kimia Klinik<br>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" value="Urinalisa">Urinalisa<br>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" value="Mikroskopis BTA">Mikroskopis BTA<br>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" value="Mikroskopis Telur Cacing">Mikroskopis Telur Cacing<br>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" value="Anti HIV">Anti HIV<br>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" value="Sifilis">Sifilis<br>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" value="HBsAg">HBsAg<br>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" value="Anti HCV">Anti HCV<br>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" value="Mikroskopis Malaria">Mikroskopis Malaria<br>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" value="Kimia Air">Kimia Air Minum<br>
                          <!-- <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" value="Kimia Air Terbatas">Kimia Air Terbatas<br> -->
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" value="Identifikasi Bakteri dan Uji Kepekaan Antibiotik">Identifikasi Bakteri dan Uji Kepekaan Antibiotik<br>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="alamat" class="col-sm-3 control-label">Alamat</label>
                        <div class="col-sm-9">
                          <textarea class="form-control" id="alamat" name="alamat" required></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="provinsi" class="col-sm-3 control-label">Provinsi</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="provinsi" id="provinsi" required onchange="Binaan()">
                                <option></option>
                                @foreach($provinsi as $val)
                                <option value="{{$val->id}}">{{$val->name}}</option>
                                @endforeach
                            </select>
                        </div>
                      </div>
                      <div class="form-group" id="alasanbinaan">
                        <label for="alasan_binaan" class="col-sm-3 control-label">Alasan Binaan</label>
                        <div class="col-sm-9">
                          <input type="text" name="alasan_binaan" class="form-control" id="alasan_binaan" placeholder="Alasan Peserta Binaan">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="kota" class="col-sm-3 control-label">Kota / Kabupaten</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="kota" id="kota" required>
                                <option></option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="kecamatan" class="col-sm-3 control-label">Kecamatan</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="kecamatan" id="kecamatan" required>
                                <option></option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="kelurahan" class="col-sm-3 control-label">Kelurahan / Desa</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="kelurahan" id="kelurahan" required>
                                <option></option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="telp" class="col-sm-3 control-label">Telp / Fax</label>
                        <div class="col-sm-9">
                          <input type="text" name="telp" class="form-control numberdoang" id="telp" placeholder="Telp / Fax" required maxlength="15">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="penanggung" class="col-sm-3 control-label">Penanggung Jawab</label>
                        <div class="col-sm-9">
                          <input type="text" name="penanggung_jawab" class="form-control hurufdoangs" id="penanggung" placeholder="Penanggung Jawab" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="personal" class="col-sm-3 control-label">Nama Kontak Person</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control hurufdoangs" name="personal" id="personal" placeholder="Personal" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">Email Kontak Person</label>
                        <div class="col-sm-9">
                          <input type="email" name="email_cp" data-validation="email" class="form-control" id="email_cp" placeholder="Email" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="hp" class="col-sm-3 control-label">Telp Kontak Person</label>
                        <div class="col-sm-9">
                          <input type="text" name="no_hp" class="form-control numberdoang" id="hp" placeholder="Telp Kontak Person" required maxlength="15">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="wa" class="col-sm-3 control-label">No. Whatsapp</label>
                        <div class="col-sm-9">
                          <input type="text" name="no_wa" class="form-control numberdoang" id="wa" placeholder="No. Whatsapp" required maxlength="15">
                        </div>
                      </div>
                      <!-- <div class="form-group pmi">
                        <center>
                          <label for="siklus" class="col-sm-12">* UTD - PMI Sudah otomatis terdaftar Paket Imunologi Paket Imunologi (Anti HIV, Sifilis, HBsAg, Anti HCV)</label>
                        </center>
                      </div> -->
                      <div class="form-group bukan-pmi">
                        <label for="siklus" class="col-sm-3 control-label">Siklus</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="siklus" id="siklus" required>
                                <option></option>
                                <?php
                                  $date = date('Y-m');
                                  $siklus1 = date('Y').'-01';
                                  $siklus2 = date('Y').'-06';
                                ?>
                                @if($date >= $siklus1 && $date < $siklus2)
                                <!-- <option class="1" value="1">Siklus 1</option> -->
                                <option class="12" value="12">Siklus 1 & 2</option>
                                @else
                                <!-- <option class="2" value="2">Siklus 2</option> -->
                                @endif
                            </select>
                        </div>
                      </div>
                    <div class="table-responsive bukan-pmi"  id="bidangpengujian">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                              <th>No</th>
                              <th colspan="2" style="text-align: center;">Bidang Pengujian Yang Dipilih</th>
                              <th>Tarif (Rp)</th>
                          </tr>
                        </thead>
                        <tbody class="body-siklus">

                        </tbody>
                          <!-- <tr id="datasiklus">
                            <td id="nosiklus"></td>
                            <td id="paramsiklus"></td>
                            <td id="bidangsiklus"></td>
                            <td id="tarifsiklus"></td>
                          </tr> -->
                      </table>
                    </div>
                    <label>Total Pembayaran:</label><br>
                    <div class="total"></div>
                    <label>Catatan :</label>
                    <p>*) Pilih metode pembayaran anda dengan benar.</p>
                    <label for="telp" class="control-label">Metode Pembayaran</label>
                    <select id="pembayaran" class="form-control" name="pembayaran" onchange="Notifpembayaran()">
                      <option></option>
                      <option value="transfer">Transfer via virtual account</option>
                      <option value="sptjm">Perjanjian Kerjasama (PKS)</option>
                    </select>
                    <div id="sptjm" class="notifpembayaran" style="display:none;">
                      <p>Perjanjian kerjasama hanya dapat dilakukan oleh instansi yang  membuat MOU mendaftar dan membayar secara kolektif.<br>
                      Pastikan instansi Anda telah memiliki MOU dengan BBLK Surabaya.</p>
                    </div>
                    <div id="transfer" class="notifpembayaran" style="display:none;">
                      <p>Kode virtual account anda 89160XXXX (xxxx merupakan nomor urut peserta)</p>

                      <p>1. Silahkan melakukan pembayaran minimal 1 (satu) jam setelah anda menerima kode virtual account<br>
                      2. Kode virtual account berlaku 1x24 jam<br>
                      3. Tata cara pembayaran via virtual account klik disini (link)<br>
                      3. Jika membutuhkan bantuan dapat menghubungi (031)5021451</p>
                    </div>
                    <!-- <div id="transfer" class="pembayaran" style="display:none">
                      <label for="telp" class="control-label">File</label><small> *Bukti Transfer</small>
                      <input type="file" class="myFile" placeholder="Nama Laboratorium" name="file" accept="image/*">
                    </div>
                    <br> -->
                    <textarea class="form-control" name="sptjm" style="display:none;">-</textarea>
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="tombolna">
                    </div>
                    <div id="sptjm" class="pembayaran" style="display:none">
                      <input type="submit" id="Daftar" name="simpan" value="Kirim" class="btn btn-submit">
                    </div>
                    @foreach($bidang as $val)
                    <input type="hidden" name="alias[]" value="{{$val->alias}}">
                    @endforeach
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"> 

$("#alasan_binaan").prop('type', 'hidden').val(''); 
$("#dilaksanakandiv").hide(); 
$("#alasanbinaan").hide(); 

$('.myFile').bind('change', function() {
  if(this.files[0].size >= 5120000){
      alert('File bukti pembayaran melebihi 5Mb!');
      $(".tombolna").html("");
  }else{
      $(".tombolna").html("");
        var exportna = "<input type=\"submit\" name=\"simpan\" value=\"Kirim\" class=\"btn btn-submit\">";
        $(".tombolna").append(exportna);
  }
});


// $(".pmi").hide(); 
// function pmi() {
//   var x = document.getElementById("pemerintah").value;
//   console.log(x);
//   if (x == '9') {
//     $("#siklus").prop('required', false); 
//     $(".bukan-pmi").hide(); 
//     $(".pmi").show(); 
//     $(".total").html("Rp. 5,400,000");
//   }else{
//     $("#siklus").prop('required', true); 
//     $(".bukan-pmi").show(); 
//     $(".pmi").hide(); 
//     $(".total").html("");
//   }
// }

function Binaan() {
  var x = document.getElementById("provinsi").value;
  console.log(x);
  if (x == '35' || x == '51' || x == '52' || x == '53' || x == '63' || x == '64' || x == '65') {
    $("#alasan_binaan").prop('type', 'text'); 
    $("#alasanbinaan").show();
  }else{
    $("#alasan_binaan").prop('type', 'hidden').val(''); 
    $("#alasanbinaan").hide();
  }
}

function Notifpembayaran() {
  var x = document.getElementById("pembayaran").value;
  console.log(x);
  if (x == 'transfer') {
    $("#transfer").show();
    $("#sptjm").hide(); 
    $("#Daftar").val('Daftar & Bayar'); 
  }else{
    $("#transfer").hide();
    $("#sptjm").show();
    $("#Daftar").val('Daftar'); 
  }
}

$(function() {
    $('#pembayaran').change(function(){
        $('.pembayaran').show();
        $('#' + $(this).val()).show();
    });
});

function formatNumber (num, currency) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

$("#siklus").change(function(){
  var val = $(this).val(), i, no = 0;
  var y = document.getElementById('datasiklus')
  $(".body-siklus").html("<tr><td colspan='4'>Please Wait... </td></tr>")
  $.ajax({
    type: "GET",
    url : "{{url('datasiklus').'/'}}"+val,
    success: function(addr){
      // y.innerHTML = '<tr id="datasiklus"><td id="nosiklus"></td><td id="paramsiklus"></td><td id="bidangsiklus"></td><td id="tarifsiklus"></td></tr>';javascript
      $(".body-siklus").html("");
      if(addr.Hasil != undefined){
        var no = 1;
        $.each(addr.Hasil,function(e,item){
          var tarif2 = formatNumber(item.tarif / 2);
          var tarif1 = formatNumber(item.tarif);
          var tarif3 = formatNumber(item.tarif * 2);
          if (val != '12') {
            if (val == '1') {
              if (item.sisa_kuota_1 > '0') {
                var html = "<tr><td class=\"nosiklus\"><center>"+no+"</center></td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif2+"</td>";
                $(".body-siklus").append(html);
              }else{
                var html = "<tr><td class=\"nosiklus\"><center>"+no+"</center></td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif2+"</td>";
                $(".body-siklus").append(html);
              }
            }
            if (val == '2') {
              if (item.sisa_kuota_2 > '0') {
                var html = "<tr><td class=\"nosiklus\"><center>"+no+"</center></td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif2+"</td>";
                $(".body-siklus").append(html);
              }else{
                var html = "<tr><td class=\"nosiklus\"><center>"+no+"</center></td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif2+"</td>";
                $(".body-siklus").append(html);
              }
            }
          }else{
            if (item.sisa_kuota_1 > '0') {
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif1+"</td>";
              $(".body-siklus").append(html);
            }else{
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif1+"</td>";
              $(".body-siklus").append(html);
            }
          }
          no++;
        })

        $("input[type=checkbox]").click(function(){
            var cekbok  = $("input[type=checkbox]"),
                total   = 0;
            cekbok.each(function(){
              var e = $(this).parents('tr').find('.tarifsiklus').html().replace(/,/g,'');
              if($(this).prop('checked') == true){
                total = total + parseInt(e);
              }
            });
            $(".total").html(formatNumber(total));
        });
      }
      return false;
    }
  });
});

$("#provinsi").change(function(){
  var val = $(this).val(), i;
  var y = document.getElementById('kota');
  $("#kecamatan").val("");
  $("#kelurahan").val("");
  y.innerHTML = "<option>Please Wait... </option>";
  $.ajax({
    type: "GET",
    url : "{{url('getkota').'/'}}"+val,
    success: function(addr){
      y.innerHTML = "<option> </option>"
      for(i = 0; i < addr.Hasil.length; i++){
        var option = document.createElement("option");
        option.value = addr.Hasil[i].Kode;
        option.text = addr.Hasil[i].Nama;
        y.add(option);
      }
      return false;
    }
  });
});

$("#kota").change(function(){
  var val = $(this).val(), i;
  var y = document.getElementById('kecamatan');
  $("#kelurahan").val("");
  y.innerHTML = "<option>Please Wait... </option>";
  $.ajax({
    type: "GET",
    url : "{{url('getkecamatan').'/'}}"+val,
    success: function(addr){
      y.innerHTML = "<option> </option>"
      for(i = 0; i < addr.Hasil.length; i++){
        var option = document.createElement("option");
        option.value = addr.Hasil[i].Kode;
        option.text = addr.Hasil[i].Nama;
        y.add(option);
      }
      return false;
    }
  });
});


$("#kecamatan").change(function(){
  var val = $(this).val(), i;
  var y = document.getElementById('kelurahan');
  y.innerHTML = "<option>Please Wait... </option>";
  $.ajax({
    type: "GET",
    url : "{{url('getkelurahan').'/'}}"+val,
    success: function(addr){
      y.innerHTML = "<option> </option>"
      for(i = 0; i < addr.Hasil.length; i++){
        var option = document.createElement("option");
        option.value = addr.Hasil[i].Kode;
        option.text = addr.Hasil[i].Nama;
        y.add(option);
      }
      return false;
    }
  });
});

$.validate({
  modules : 'location, date, security, file',
  onModulesLoaded : function() {
    $('#country').suggestCountry();
  }
});

function pemantapanmutu() {
  var x = document.getElementById("pemantapan_mutu").value;
  console.log(x);
  if (x == 'Dilaksanakan') {
    $("#dilaksanakan").prop('required', true);
    $("#dilaksanakandiv").show(); 
  }else{
    $("#dilaksanakan").prop('required', false);
    $("#dilaksanakandiv").hide(); 
    $('#dilaksanakan').val('default');
    $('#dilaksanakan').selectpicker("refresh");
  }
}

$('#myform').submit(function() {
  var x = document.getElementById("pemantapan_mutu").value;
  if($('.checkbidang:checkbox:checked').length == 0){
    alert('Pilih minimal 1 Bidang Pengujian');
    $('html, body').animate({ 
      scrollTop: $('#bidangpengujian').offset().top 
    }, 'slow');
    return false;
  }
  if(x == 'Dilaksanakan'){
    if($('.dilaksanakan:checkbox:checked').length == 0){
      alert('Pilih minimal 1 Parameter');
      $('html, body').animate({ 
        scrollTop: $('#dilaksanakandiv').offset().top 
      }, 'slow');
      return false;
    }
  }
  $('#Daftar').button('loading')
});
</script>
@endsection
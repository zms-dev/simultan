@section('menu')
@if (Auth::guest())
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-136609270-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-136609270-2');
    </script>
    <?php 
        $daftar = DB::table('tb_hide_menu')->where('menu','=','Daftar')->first();
    ?>
    <li><a href="{{ route('login') }}">Login</a></li>
    @if(count($daftar))
        @if($daftar->status == 'hilang')
        @else
            <li><a href="{{ route('register') }}">Register</a></li>
        @endif
    @endif
    <li><a href="{{ URL('tarif') }}">Info Tarif & Kuota</a></li>
    <li><a href="{{ URL('download-sptjm') }}">Download Dokumen PNPME</a></li>
    <li><a href="{{ URL('jadwal') }}">Jadwal PNPME</a></li>
    <!-- <li><a href="{{ URL('juklak') }}">Juklak</a></li>     -->
@else
    @if(Auth::user()->role == '1')
        <li><a href="{{ URL('admin/banner') }}">Master</a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Informasi <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <!-- <li><a href="{{ URL('download_evaluasi') }}">Download Evaluasi</a></li> -->
                <li><a href="{{ URL('download-manual-book') }}">Download Manual Book dan Juknis</a></li>
                <li><a href="{{ URL('download-penggunaan') }}">Download Turorial Penggunaan</a></li>
                <li><a href="{{ URL('edit-data') }}">Edit Data Personal</a></li>
                <li><a href="{{ URL('download-sptjm') }}">Download Dokumen PNPME</a></li>
                <li><a href="{{ URL('tarif') }}">Info Tarif & Kuota</a></li>
            </ul>
        </li>
        <!-- <li><a href="{{ URL('daftar') }}">Daftar</a></li> -->
        <!-- <li><a href="{{ URL('kirim-bahan') }}">Kirim Bahan</a></li> -->
        <!-- <li><a href="{{ URL('juklak') }}">Juklak</a></li> -->
        <!-- <li><a href="{{ URL('cek_transfer') }}">Cek Transfer</a></li> -->
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Email <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('email-blast') }}">Email Blast Seluruh Peserta</a></li>
                <li><a href="{{ URL('email-blast-kirim-hasil') }}">Email Blast Kirim Hasil</a></li>
            </ul>
        </li>
        <!-- <li><a href="{{ URL('sptjm') }}">Download Dokumen PNPME</a></li> -->
        <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Hasil Pemeriksaan <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('hasil-pemeriksaan') }}">Input Hasil</a></li>
                <li><a href="{{ URL('cetak-hasil') }}">Cetak Hasil</a></li>
            </ul>
        </li> -->
        <li class="dropdown">
            @yield('menudashboard')
        </li>
        <li class="dropdown">
            @yield('menulaporan')
        </li>
        <!-- <li><a href="{{ URL('#') }}">Upload Evaluasi</a></li> -->
        <li class="dropdown pisahin">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif
    @if(Auth::user()->role == '2')
        <li><a href="{{ URL('tarif') }}">Info Tarif & Kuota</a></li>
        <li><a href="{{ URL('download-manual-book') }}">Download Manual Book dan Juknis</a></li>
        <li><a href="{{ URL('download-penggunaan') }}">Download Tutorial Penggunaan</a></li>
        <li><a href="{{ URL('download-sptjm') }}">Download Dokumen PNPME</a></li>
        <li class="dropdown">
            @yield('menulaporan')
        </li>
        <li class="dropdown pisahin">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif
    @if(Auth::user()->role == '3')
        <?php
            $input = DB::table('tb_hide_menu')->where('menu','=','Input Hasil')->first();
            $inputevaluasi = DB::table('tb_hide_menu')->where('menu','=','Hasil Evaluasi')->first();
            $evaluasi = DB::table('tb_pendapat_responden')->where('id_users','=', Auth::user()->id)->first();
            $siklus = '1';
            $daftar = DB::table('tb_hide_menu')->where('menu','=','Daftar')->first();
            $register = DB::table('tb_registrasi')->where('created_by', '=', Auth::user()->id)->get();
            $berita = DB::table('users')->where('id', '=', Auth::user()->id)->select('berita')->first();
        ?>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Informasi @if($berita->berita != 0)<span class="badge">{{$berita->berita}}</span>@endif<span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('tarif') }}">Info Tarif & Kuota</a></li>
                <li><a href="{{ URL('jadwal') }}">Jadwal PNPME</a></li>
                <li><a href="{{ URL('info-pendaftaran') }}">Status PNPME</a></li>
                <li><a href="{{ URL('status-pembayaran') }}">Status Pembayaran</a></li>
                <li><a href="{{ URL('berita') }}">Berita  @if($berita->berita != 0)<span class="badge">{{$berita->berita}}</span>@endif</a></li>
            </ul>
        </li>
        @if(count($daftar))
            @if($daftar->status == 'hilang')
            @else
                <li><a href="{{ URL('daftar') }}">Daftar</a></li>
            @endif
        @endif
        @if(count($register))

        @if(count($inputevaluasi))
            @if($inputevaluasi->status == 'muncul')
            <li><a href="{{ URL('data-evaluasi') }}">Hasil Evaluasi</a></li>
            @else
            @endif
        @endif

        
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Hasil Pemeriksaan <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('tanda-terima') }}">Tanda Terima Bahan</a></li>
                @if(count($input))
                    @if($input->status == 'hilang')
                    @else
                        <li><a href="{{ URL('hasil-pemeriksaan') }}">Input Hasil</a></li>
                        <li><a href="{{ URL('edit-hasil') }}">Edit Hasil</a></li>
                    @endif
                @else
                    <li><a href="{{ URL('hasil-pemeriksaan') }}">Input Hasil</a></li>
                    <li><a href="{{ URL('edit-hasil') }}">Edit Hasil</a></li>
                @endif
                <!-- <li><a href="{{ URL('cetak-sementara') }}">Cetak Hasil Sementara</a></li> -->
                <li><a href="{{ URL('cetak-hasil') }}">Cetak Hasil</a></li>
              
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Komentar dan Saran <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('pendapat/pendapat-responden') }}">Survey Kepuasan Pelanggan</a></li>
                <li><a href="{{ URL('pendapat/keluh-saran') }}">Keluhan / Saran</a></li>
                <li><a href="{{ URL('pendapat/jawab/keluh-saran') }}">Jawaban Keluhan / Saran</a></li>
            </ul>
        </li>
        @endif
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Download <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <!-- <li><a href="{{ URL('download_evaluasi') }}">Download Evaluasi</a></li> -->
                <li><a href="{{ URL('download-manual-book') }}">Download Manual Book dan Juknis</a></li>
                <li><a href="{{ URL('download-penggunaan') }}">Download Tutorial Penggunaan</a></li>
                <li><a href="{{ URL('download-sptjm') }}">Download Dokumen PNPME</a></li>
            </ul>
        </li>
        <li class="dropdown pisahin">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('edit-data') }}">Profile</a></li>
                <li role="separator" class="divider"></li>
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif
    @if(Auth::user()->role == '4')
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Informasi <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('download-penggunaan') }}">Tutorial Penggunaan</a></li>
                <li><a href="{{ URL('tarif') }}">Info Tarif & Kuota</a></li>
                <li><a href="{{ URL('jadwal') }}">Jadwal PNPME</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Virtual Account <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('generate-va') }}">Generate Virtual Account</a></li>
                <li><a href="{{ URL('data-peserta-va') }}">Data Peserta Virtual Account</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Pembayaran <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('cek_transfer') }}">Cek Transfer</a></li>
                <li><a href="{{ URL('sptjm') }}">Perjanjian Kerjasama</a></li>
                <li><a href="{{ URL('update-pembayaran') }}">Update Pembayaran</a></li>
                <li><a href="{{ URL('cek-kwitansi') }}">Cek Kwitansi Transfer</a></li>
                <li><a href="{{ URL('cek-kwitansi/pks') }}">Cek Kwitansi PKS</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Komentar dan Saran <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('pendapat/keluh-saran/jawab') }}">Jawab Keluhan / Saran</a></li>
                <li><a href="{{ URL('pendapat/survey-pelanggan') }}">Survey Pelanggan</a></li>
            </ul>
        </li>
        <li class="dropdown">
            @yield('menulaporan')
        </li>
        <li class="dropdown">
            @yield('menudashboard')
        </li>
        <li class="dropdown pisahin">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif
    @if(Auth::user()->role == '5')

        <li><a href="{{ URL('admin/bidang') }}">Master</a></li>
        <li><a href="{{ URL('tarif') }}">Info Tarif & Kuota</a></li>
        <li><a href="{{ URL('jadwal') }}">Jadwal PNPME</a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Download <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <!-- <li><a href="{{ URL('download_evaluasi') }}">Download Evaluasi</a></li> -->
                <li><a href="{{ URL('download-manual-book') }}">Download Manual Book dan Juknis</a></li>
                <li><a href="{{ URL('download-penggunaan') }}">Download Tutorial Penggunaan</a></li>
                <li><a href="{{ URL('download-sptjm') }}">Download Dokumen PNPME</a></li>
            </ul>
        </li>
        <li class="dropdown">
            @yield('menuevaluasi')
        </li>
        <li class="dropdown">
            @yield('menudashboard')
        </li>
        <li class="dropdown">
            @yield('menulaporan')
        </li>
        <li class="dropdown pisahin">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif
    @if(Auth::user()->role == '6')
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Kirim Bahan <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('kirim-bahan') }}">Siklus 1</a></li>
                <li><a href="{{ URL('kirim-bahan-siklus2') }}">Siklus 2</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Email <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('email-blast') }}">Email Blast Seluruh Peserta</a></li>
                <li><a href="{{ URL('email-blast-kirim-hasil') }}">Email Blast Kirim Hasil</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Komentar dan Saran <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('pendapat/keluh-saran/jawab') }}">Jawab Keluhan / Saran</a></li>
            </ul>
        </li>
        <li class="dropdown">
            @yield('menudashboard')
        </li>
        <li class="dropdown">
            @yield('menulaporan')
        </li>
        <!-- <li><a href="{{ URL('#') }}">Upload Evaluasi</a></li> -->
        <li class="dropdown pisahin">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif
    @if(Auth::user()->role == '7')
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Informasi <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <!-- <li><a href="{{ URL('download_evaluasi') }}">Download Evaluasi</a></li> -->
                <li><a href="{{ URL('download-manual-book') }}">Download Manual Book dan Juknis</a></li>
                <li><a href="{{ URL('download-penggunaan') }}">Download Tutorial Penggunaan</a></li>
                <li><a href="{{ URL('download-sptjm') }}">Download Dokumen PNPME</a></li>
                <li><a href="{{ URL('tarif') }}">Info Tarif & Kuota</a></li>
            </ul>
        </li>
        <!-- <li><a href="{{ URL('juklak') }}">Juklak</a></li> -->
        <li><a href="{{ URL('cek_transfer') }}">Cek Transfer</a></li>
        <li class="dropdown">
            @yield('menulaporan')
        </li>
        <li class="dropdown pisahin">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif
@endif
@endsection

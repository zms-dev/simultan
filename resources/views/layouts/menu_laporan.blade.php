@section('menulaporan')
<a href="#" class="dropdown-toggle" data-toggle="dropdown">Laporan<b class="caret"></b></a>
<ul class="dropdown-menu">
    <li>
        <a href="{{url('/laporan-tk-nasional')}}">Laporan PME TK. Nasional</a>
    </li>
@if (Auth::guest())
@else
    @if(Auth::user()->role == '4')
    @elseif(Auth::user()->role == '6')
    @else
    <!-- <li>
        <a href="{{url('dashboard/pnpme')}}">SIMULTAN</a>
    </li> -->
    @endif
    @if(Auth::user()->role == '5')
    <li>
        <a href="{{url('/monitoring-laporan-hasil')}}">Monitoring Laporan Hasil</a>
    </li>
    @elseif(Auth::user()->role == '1')
      <li>
          <a href="{{url('/rekap-nilai-peserta')}}">Rekap Nilai Peserta</a>
      </li>
    @else
    <!-- <li>
        <a href="{{url('monitoring-simultan')}}">Monitoring Simultan</a>
    </li> -->
    @endif

@endif

    <li>
        <a href="{{url('/laporan-tanda-terima-bahan')}}">Laporan Tanda Terima Bahan</a>
    </li>
    <li>
        <a href="{{url('/laporan-survey-pelanggan')}}">Laporan Survey Pelanggan</a>
    </li>
    <li>
        <a href="{{url('/laporan-saran-komentar')}}">Laporan Saran dan Komentar</a>
    </li>
    <li>
        <a href="{{url('laporan-registrasi')}}">Laporan Seluruh Peserta</a>
    </li>
    <li>
        <a href="{{url('laporan-uang-masuk')}}">Laporan Penerimaan uang masuk</a>
    </li>
    <li>
        <a href="{{url('laporan-peserta')}}">Laporan Perserta PNPME</a>
    </li>
    <li>
        <a href="{{url('laporan-peserta-parameter')}}">Laporan Pengiriman Hasil PNPME</a>
    </li>
    <!-- <li>
        <a href="{{url('laporan-penerimaan-sampel')}}">Laporan Penerimaan Sampel</a>
    </li> -->
    <!-- <li>
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Laporan Hasil<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li style="padding-left: 13px"><a href="#">Laporan Hasil</a></li>
            <li style="padding-left: 13px"><a href="#">Laporan Hasil Siklus 1</a></li>
            <li style="padding-left: 13px"><a href="#">Laporan Hasil Siklus 2</a></li>
        </ul>
    </li> -->
    <li>

@if (Auth::guest())
@else
    @if(Auth::user()->role == '4')
    @elseif(Auth::user()->role == '6')
    @else
        <!-- <a href="{{url('laporan-evaluasi')}}">Laporan Evaluasi</a> -->
    @endif
@endif
    </li>
</ul>
@endsection

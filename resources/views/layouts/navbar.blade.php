@extends('layouts.header')
@section('header_t')

<!--sidebar start-->
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
    @if(Auth::user()->role == '1')
        <ul class="sidebar-menu" id="nav-accordion">
            <li>
                <a href="{{URL('admin/banner')}}">
                    <i class="icon-dashboard"></i>
                    <span>Banner</span>
                </a>
            </li>

            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="icon-tags"></i>
                    <span>Pendaftaran</span>
                </a>
                <ul class="sub">
                    <li><a href="{{URL('admin/manual-book')}}">Manual Book</a></li>
                    <li><a href="{{URL('admin/juklak')}}">Juklak</a></li>
                    <li><a href="{{URL('admin/penggunaan')}}">Tutorial Penggunaan</a></li>
                    <li><a href="{{URL('admin/sptjm')}}">Dokumen PNPME</a></li>
                    <li><a href="{{URL('admin/pks')}}">Perjanjian Kerja Sama</a></li>
                    <li><a href="{{URL('admin/parameter')}}">Parameter dan Kuota</a></li>
                    <li><a href="{{URL('admin/menu')}}">Seting Menu Pendaftaran</a></li>
                </ul>
            </li>

            <li>
                <a href="{{URL('admin/data-perusahaan')}}">
                    <i class="icon-tag"></i>
                    <span>Data Perusahaan</span>
                </a>
            </li>
            
            <li>
                <a href="{{URL('admin/nomor-sertifikat')}}">
                    <i class="icon-certificate"></i>
                    <span>Nomor Sertifikat</span>
                </a>
            </li>
            <li>
                <a href="{{URL('admin/ttd-hasil-evaluasi')}}">
                    <i class="icon-pencil"></i>
                    <span>TTD Hasil Evaluasi</span>
                </a>
            </li>

            <li class="sub-menu" hidden>
                <a href="javascript:;">
                    <i class="icon-book"></i>
                    <span>Rujukan</span>
                </a>
                <ul class="sub">
                    <li><a href="{{URL('admin/rujukan-bta')}}">BTA</a></li>
                    <li><a href="{{URL('admin/rujukan-tc')}}">TC</a></li>
                    <li><a href="{{URL('admin/rujukan-malaria')}}">Malaria</a></li>
                    <li><a href="{{URL('admin/rujukan-hematologi')}}">Hematologi</a></li>
                    <li><a href="{{URL('admin/rujukan-kimiaklinik')}}">Kimia Klinik</a></li>
                    <li><a href="{{URL('admin/rujukan-urinalisa')}}">Urinalisa</a></li>
                    <li><a href="{{URL('admin/rujukan-kimiakesehatan')}}">Kimia Kesehatan</a></li>
                </ul>
            </li>

            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="icon-laptop"></i>
                    <span>Wilayah</span>
                </a>
                <ul class="sub">
                    <li><a href="{{URL('admin/provinsi')}}">Provinsi</a></li>
                    <li><a href="{{URL('admin/kabupaten-kota')}}">Kabupaten / Kota</a></li>
                    <li><a href="{{URL('admin/kecamatan')}}">Kecamatan</a></li>
                    <li><a href="{{URL('admin/kelurahan')}}">Kelurahan</a></li>
                </ul>
            </li>
            <!-- <li>
                <a href="{{URL('admin/sptjm')}}">
                    <i class="icon-file-text-alt"></i>
                    <span>Dokumen PNPME</span>
                </a>
            </li> -->
            <!-- <li>
                <a href="{{URL('admin/upload-evaluasi')}}">
                    <i class=" icon-columns"></i>
                    <span>Upload Evaluasi</span>
                </a>
            </li> -->
            <li>
                <a href="{{URL('admin/berita')}}">
                    <i class="icon-share"></i>
                    <span>Berita</span>
                </a>
            </li>
            <li>
                <a href="{{URL('admin/jadwal')}}">
                    <i class="icon-list-ol"></i>
                    <span>Jadwal</span>
                </a>
            </li>
            <li>
                <a href="{{URL('admin/hak-akses')}}">
                    <i class="icon-user"></i>
                    <span>Hak Akses</span>
                </a>
            </li>
            <li>
                <a href="{{URL('admin/log-input')}}">
                    <i class="icon-key"></i>
                    <span>Log Aktivitas</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="icon-gear"></i>
                    <span>Setting</span>
                </a>
                <ul class="sub">
                    <li><a href="{{URL('admin/edit-hasil')}}">Kirim Edit Hasil</a></li>
                    <li><a href="{{URL('admin/menu')}}">Menu Web PNPME</a></li>
                    <li><a href="{{URL('admin/siklus')}}">Siklus PNPME</a></li>
                    <li><a href="{{URL('admin/tahun-evaluasi')}}">Tahun Evaluasi</a></li>
                    <li><a href="{{URL('admin/blok-evaluasi-peserta')}}">Blok Evaluasi Peserta</a></li>
                </ul>
            </li>
        </ul>
    @endif
    @if(Auth::user()->role == '5')
        <ul class="sidebar-menu" id="nav-accordion">
            <li>
                <a href="{{URL('admin/juklak')}}">
                    <i class="icon-dashboard"></i>
                    <span>Juklak</span>
                </a>
            </li>
            <li>
                <a href="{{URL('admin/manual-book')}}">
                    <i class="icon-dashboard"></i>
                    <span>Manual Book</span>
                </a>
            </li>

            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="icon-dropbox"></i>
                    <span>Alat</span>
                </a>
                <ul class="sub">
                    <!-- <li><a href="{{URL('admin/bidang')}}">Bidang</a></li>
                    <li><a href="{{URL('admin/parameter')}}">Parameter</a></li> -->
                    <li><a href="{{URL('admin/reagen-imunologi')}}">Reagen Imunologi</a></li>
                </ul>
            </li>
            <!-- <li>
                <a href="{{URL('admin/upload-evaluasi')}}">
                    <i class="icon-dashboard"></i>
                    <span>Upload Evaluasi</span>
                </a>
            </li> -->
        @if(Auth::user()->penyelenggara == '2')
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="icon-gear"></i>
                    <span>Setting Input Peserta</span>
                </a>
                <ul class="sub">
                    <!-- <li><a href="{{URL('admin/edit-hasil')}}">Kirim Edit Hasil</a></li> -->
                    <li><a href="{{URL('admin/alat-instrumen/kimia-klinik')}}">Alat / Instrumen</a></li>
                    <li><a href="{{URL('admin/metode-pemeriksaan/kimia-klinik')}}">Metode Pemeriksaan</a></li>
                </ul>
            </li>
        @endif
        </ul>
    @endif
        <!-- sidebar menu end-->
    </div>
</aside>
@yield('content')
@endsection
@section('menudashboard')
<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dashboard<b class="caret"></b></a>
<ul class="dropdown-menu">
    <li>
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Grafik<b class="caret"></b></a>
        <ul class="dropdown-menu" style="top: 0px">
            <!-- <li><a href="{{url('grafik/rekap-survey-pelanggan')}}">Rekap Survey Pelanggan</a></li> -->
            <li><a href="{{url('grafik/rekap-transaksi-parameter')}}">Rekap Transaksi berdasarkan Parameter</a></li>
            <li><a href="{{url('grafik/rekap-peserta-instansi')}}">Rekap Peserta berdasarkan Instansi</a></li>
            <li><a href="{{url('grafik/rekap-peserta-pembayaran')}}">Rekap Peserta berdasarkan Pembayaran</a></li>
            <li><a href="{{url('grafik/laporan-hasil-pnpme')}}">Laporan Hasil PNPME Per Wilayah</a></li>
        </ul>
    </li>

@if (Auth::guest())
@else
    @if(Auth::user()->role == '5')
    <li>
        <a href="{{url('laporan-peserta-provinsi-instansi')}}">Laporan Jumlah Peserta per Provinsi per Instansi </a>
    </li>
    @endif
@endif
    <li>
        <a href="{{url('monitoring-evaluasi-parameter')}}">Monitoring Evaluasi Hasil Peserta per Parameter</a>
    </li>
   <!--  <li>
        <a href="{{url('rekap-peserta-bidang')}}">Rekap Seluruh Perserta dan Bidang</a>
    </li> -->
    <li>
        <a href="{{url('rekap-peserta-provinsi')}}">Laporan Jumlah Peserta per Provinsi per Bidang</a>
    </li>
    <li>
        <a href="{{url('rekap-jumlah-peserta-parameter')}}">Rekap Peserta berdasarkan Parameter</a>
    </li>
    <li>
        <a href="{{url('rekap-transaksi-parameter')}}">Rekap Transaksi berdasarkan Parameter</a>
    </li>
    <li>
        <a href="{{url('rekap-peserta-instansi')}}">Rekap Peserta berdasarkan Instansi</a>
    </li>
    <li>
        <a href="{{url('rekap-transaksi-peserta-parameter')}}">Rekap Tarif Peserta berdasarkan Parameter</a>
    </li>
    <li>
        <a href="{{url('rekap-peserta-parameter')}}">Rekap Peserta berdasarkan Instansi dan Parameter</a>
    </li>
</ul>
@endsection

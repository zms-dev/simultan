@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Tanda Terima Bahan</div>
                <div class="panel-body">
                    @if(Session::has('message'))
                      <div class="alert {{ Session::get('alert-class') }}" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ Session::get('message') }}
                      </div>
                    @endif
                    <label>
                        <!-- - Pengisian Hasil Online PNPME Siklus 1 diperpanjang sampai tanggal 24 Mei 2018 -->
                    </label>
                    <table class="table table-bordered">
                        <tr>
                            <th width="5%"><center>No</center></th>
                            <th><center>Bidang</center></th>
                            <th colspan="2" width="15%"><center>Siklus&nbsp;1</center></th>
                            <th colspan="2" width="15%"><center>Siklus&nbsp;2</center></th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                <!-- IF BIDANG SIPILIS -->
                                <td colspan="2" class="siklus1{{$no}}">
                                    @if(count($val->siklus1))
                                    <a href="{{URL('').$val->Link}}/tanda-terima/print/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-print"></i></center></a>
                                    @else
                                    <a href="{{URL('').$val->Link}}/tanda-terima/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                    @endif
                                </td>
                                <td colspan="2" class="siklus2{{$no}}">
                                    @if(count($val->siklus2))
                                    <a href="{{URL('').$val->Link}}/tanda-terima/print/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-print"></i></center></a>
                                    @else
                                    <a href="{{URL('').$val->Link}}/tanda-terima/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6">Bahan Uji belum / sedang dalam peroses pengiriman</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

$(document).ready(function() {
    <?php $no = 0; ?>
    @foreach($data as $val)
    <?php $no++ ?>
        @if($val->status_2 != 3)
            $('.siklus2{{$no}}').empty();
        @endif
    @endforeach
});

(function (global) {

    if(typeof (global) === "undefined")
    {
        throw new Error("window is undefined");
    }

    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";
        global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };
    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };
    global.onload = function () {
        noBackPlease();
        document.body.onkeydown = function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                e.preventDefault();
            }
            e.stopPropagation();
        };  
    };
})(window);
</script>
@endsection
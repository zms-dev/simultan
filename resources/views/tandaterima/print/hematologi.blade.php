<style type="text/css">
@page {
margin: 150px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 5px;
    text-align: left;
    border: 1px solid #ddd;
}

#header { 
    position: fixed; 
    border-bottom:1px solid gray;
    padding-top: -140px
}
#footer { 
    position: fixed; 
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}

.table-bordered tbody td{
    height:30px;

}
</style>
<div id="header">
    <table width="100%" cellpadding="0" border="0" >
        <thead>
            <tr>
                <th>
                   <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px"> 
                </th>
                <td width="80%" style="padding-left: 70px;" >
                    <span style="font-size: 12px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 10px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL HEMATOLOGI</b></span><br>
                    <span style="font-size: 10px">Penyelenggara : <b>Balai Besar Laboratorium Kesehatan Surabaya</b></span>
                    <div style="padding-left: 75px; margin-top: -15px; font-size: 10px">
                        <span><br/>Jalan Karangmenjangan No, 18 Surabaya - 60286<br>Telepon : (031) 5021451 | Fax : (031) 5020388<br>Web Online : simultan.bblksurabaya.id <br>Email : pme.bblksub@gmail.com</span>
                    </div>
                </td>
                <th width="%"></th>
                <th>
                    <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" height="90px">
                </th>

                
            </tr>
            <tr>
                <th colspan="6"><hr></th>
            </tr>
        </thead>
    </table>
</div>
<center>
    <h3>TANDA TERIMA BAHAN UJI<br>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL<br>HEMATOLOGI SIKLUS {{$siklus}} TAHUN {{$tahun}}</h3>
</center><br>
<table>
    <tr>
        <th>Nama Instansi</th>
        <td>:</td>
        <td>{{$register->nama_lab}}</td>
    </tr>
    <tr>
        <th>Tanggal Diterima</th>
        <td>:</td>
        <td>{{$data1->tanggal_penerimaan}}</td>
    </tr>
</table>
<br>
<table class="utama">
    <tr>
        <th><center>BIDANG PEMERIKSAAN</center></th>
        <th><center>JUKLAK</center></th>
        <th colspan="2"><center>BAHAN UJI</center></th>
        <th><center>KONDISI BAHAN UJI</center></th>
        <th><center>KODE PESERTA</center></th>
        <th><center>KETERANGAN</center></th>
    </tr>
    <?php $no = 0; ?>
    @foreach($data as $val)
    <?php $no++; ?>
    <tr>
        @if($no == 1)
        <td rowspan="2"><center>HEMATOLOGI</center></td>
        <td rowspan="2">
            <center>{{$data1->juklak}}</center>
        </td>
        @endif
        <td><center>0{{$no}}</center></td>
        <td>
            <center>{{$val->bahan_uji}}</center>
        </td>
        <td>
            <center>{{$val->kondisi_bahan}}</center>
        </td>
        @if($no == 1)
        <td rowspan="2"><center>{{$register->kode_lebpes}}</center></td>
        <td rowspan="2">{{$data1->keterangan}}</td>
        @endif
    </tr>
    @endforeach
</table>
<p>Keterangan : </p>
<ul>
    <li><b>Laik</b> : bahan uji diterima dalam keadaan dingin, tidak Klot / Menggumpal dan tidak pecah / tumpah</li>
    <li><b>Kurang Laik</b> : bahan uji diterima dalam keadaan tidak dingin, tidak Klot / Menggumpal,  dan tidak  pecah / tumpah</li>
    <li><b>Tidak Laik</b> : bahan uji diterima dalam keadaan Klot / Menggumpal atau kondisi pecah / tumpah</li>
    <li>Perhatikan ketersediaan juklak, kesesuaian kode bahan uji yang diterima dan kondisi bahan uji ketika diterima</li>
    <li>Jika bahan uji yang diterima rusak/tidak laik segera laporkan kepada penyelenggara</li>
</ul>
<div style="float: right; text-align: center; margin: 30px 20px 20px 20px;">
    <label><b>Yang Menerima :</b></label><br><br><br><br><br><br>
    <label>{{$data1->penerima}}</label>
</div>
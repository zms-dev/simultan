<style type="text/css">
@page {
margin: 150px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 5px;
    text-align: left;
    border: 1px solid #ddd;
}

#header { 
    position: fixed; 
    border-bottom:1px solid gray;
    padding-top: -140px
}
#footer { 
    position: fixed; 
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}

.table-bordered tbody td{
    height:30px;

}
</style>
<div id="header">
    <table width="100%" cellpadding="0" border="0" >
        <thead>
            <tr>
                <th width="11%"></th>
                <th>
                   <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px"> 
                </th>
                <td width="50%" style="padding-left: 30px;" >
                    <span style="font-size: 12px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 10px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL <br>KIMIA AIR TERBATAS</b></span><br>
                    <span style="font-size: 10px">Penyelenggara : <b>Balai Besar Laboratorium Kesehatan Surabaya</b></span>
                    <div style="padding-left: 75px; margin-top: -15px; font-size: 10px">
                        <span><br/>Jalan Karangmenjangan No, 18 Surabaya - 60286<br>Telepon : (031) 5021451 | Fax : (031) 5020388<br>Web Online : simultan.bblksurabaya.id <br>Email : pme.bblksub@gmail.com</span>
                    </div>
                </td>
                <th width="%"></th>
                <th>
                    <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" height="90px">
                </th>

                
            </tr>
            <tr>
                <th colspan="6"><hr></th>
            </tr>
        </thead>
    </table>
</div>
<center>
    <h3>TANDA TERIMA BAHAN UJI<br>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL<br>KIMIA AIR TERBATAS SIKLUS {{$siklus}} TAHUN {{$tahun}}</h3>
</center><br>
<table>
    <tr>
        <th>Nomor Peserta</th>
        <td>:</td>
        <td>{{$register->kode_lebpes}}</td>
    </tr>
    <tr>
        <th>Nama Instansi</th>
        <td>:</td>
        <td>{{$register->nama_lab}}</td>
    </tr>
    <tr>
        <th>Tanggal Diterima</th>
        <td>:</td>
        <td>{{$data1->tanggal_penerimaan}}</td>
    </tr>
</table>
<br>
<table class="utama">
    <tr>
        <th><center>KETERANGAN BARANG</center></th>
        <th><center>JUMLAH</center></th>
        <th><center>KODE BOTOL</center></th>
        <th><center>KONDISI</center></th>
        <th><center>KETERANGAN</center></th>
    </tr>
    <?php $no = 0; ?>
    @foreach($data as $val)
    <?php $no++; ?>
    <tr>
        <td>Bahan Uji PNPME- KAT Siklus {{$siklus}} <br>Etiket warna @if($siklus == 1) Biru @else Hijau @endif</td>
        <td><center>1 botol @ 50 mL</center></td>
        <td>
            <center>KAT-{{$val->kode}}</center>
        </td>
        <td>
            <center>{{$val->kondisi_bahan}}</center>
        </td>
        <td>{{$data1->keterangan}}</td>
    </tr>
    @endforeach
</table>
<div style="float: right; text-align: center; margin: 30px 20px 20px 20px;">
    <label><b>Yang Menerima :</b></label><br><br><br><br><br><br>
    <label>{{$data1->penerima}}</label>
</div>
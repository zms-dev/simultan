@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Form Tanda Terima Bahan Uji</div>

                <div class="panel-body">
                 
                <form class="form-horizontal" id="form_d" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <center><label>
                        TANDA TERIMA BAHAN UJI<br>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL<br>KIMIA AIR TERBATAS SIKLUS {{$siklus}} TAHUN {{$tahun}}</label></center><br>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nomor Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="nama_instansi" value="{{$register->kode_lebpes}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Instansi </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="nama_instansi" value="{{$register->nama_lab}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Diterima </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" required class="form_datetime form-control validate-tanggal" name="tanggal_penerimaan" autocomplete="off">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Penerima </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="penerima" name="penerima" placeholder="Nama Penerima" required>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <tr>
                            <th><center>KETERANGAN BARANG</center></th>
                            <th><center>JUMLAH</center></th>
                            <th><center>KODE BOTOL</center></th>
                            <th><center>KONDISI</center></th>
                            <th><center>KETERANGAN</center></th>
                        </tr>
                        <tr>
                            <td>Bahan Uji PNPME- KAT Siklus {{$siklus}} <br>Etiket warna @if($siklus == 1) Biru @else Hijau @endif</td>
                            <td><center>1 botol @ 50 mL</center></td>
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">KAT-</span>
                                    <input type="text" class="form-control" placeholder="Kode Botol" name="kode" required>
                                </div>
                            </td>
                            <td>
                                <select name="kondisi_bahan" class="form-control" required>
                                    <option></option>
                                    <option value="Baik">Baik</option>
                                    <option value="Pecah / Tumpah">Pecah / Tumpah</option>
                                    <option value="Lain-lain">Lain-lain</option>
                                </select>
                            </td>
                            <td>
                                <textarea name="keterangan" class="form-control"></textarea>
                            </td>
                        </tr>
                    </table>
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin: 15px 0px 0px 15px;">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});
</script>
@endsection

@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Form Tanda Terima Bahan Uji</div>

                <div class="panel-body">
                 
                <form class="form-horizontal" id="form_d" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <center><label>
                        TANDA TERIMA BAHAN UJI<br>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL<br>MIKROBIOLOGI MALARIA SIKLUS {{$siklus}} TAHUN {{$tahun}}</label></center><br>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Instansi </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="nama_instansi" value="{{$register->nama_lab}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" placeholder="Nama Penerima" value="{{$register->kode_lebpes}}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Diterima </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" required class="form_datetime form-control validate-tanggal" name="tanggal_penerimaan" autocomplete="off">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Penerima </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="penerima" name="penerima" placeholder="Nama Penerima" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Bahan Uji </label>
                        <div class="col-sm-9">
                            <select class="form-control" id="kode_bahan" required>
                                <option></option>
                                <option value="1">{{substr($register->kode_lebpes,0,-5)}}1/{{$siklus}}{{substr($register->kode_lebpes, 11)}} - {{substr($register->kode_lebpes,0,-5)}}10/{{$siklus}}{{substr($register->kode_lebpes, 11)}}</option>
                                <option value="11">{{substr($register->kode_lebpes,0,-5)}}11/{{$siklus}}{{substr($register->kode_lebpes, 11)}} - {{substr($register->kode_lebpes,0,-5)}}20/{{$siklus}}{{substr($register->kode_lebpes, 11)}}</option>
                                <option value="21">{{substr($register->kode_lebpes,0,-5)}}21/{{$siklus}}{{substr($register->kode_lebpes, 11)}} - {{substr($register->kode_lebpes,0,-5)}}30/{{$siklus}}{{substr($register->kode_lebpes, 11)}}</option>
                                <option value="31">{{substr($register->kode_lebpes,0,-5)}}31/{{$siklus}}{{substr($register->kode_lebpes, 11)}} - {{substr($register->kode_lebpes,0,-5)}}40/{{$siklus}}{{substr($register->kode_lebpes, 11)}}</option>
                            </select>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <tr>
                            <th><center>KODE BAHAN UJI</center></th>
                            <th><center>KONDISI BAHAN UJI</center></th>
                            <th><center>KETERANGAN</center></th>
                        </tr>
                        <?php
                            $no =0; 
                            for ($i=1; $i <= 10 ; $i++) {
                                $no++;
                        ?>
                        <tr>
                            <td>
                                <input type="hidden" name="kode[]" id="kode{{$i}}" value="">
                                <center>
                                    <label id="label_kode{{$i}}"></label>
                                </center>
                            </td>
                            <td>
                                <select name="kondisi_bahan[]" class="form-control" required>
                                    <option></option>
                                    <option value="Baik">Baik</option>
                                    <option value="Pecah / Tumpah">Pecah / Tumpah</option>
                                    <option value="Lain-lain">Lain-lain</option>
                                </select>
                            </td>
                            <td><textarea class="form-control" name="keterangan[]"></textarea></td>
                        </tr>
                    <?php } ?>
                    </table>
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin: 15px 0px 0px 15px;">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});

$(document).ready(function(){
  $("#kode_bahan").change(function(){
    var kode = $("#kode_bahan").val();
    <?php 
        $no = 1; 
        for ($i=0; $i < 10 ; $i++){
    ?>
      $("#kode{{$no}}").val("{{substr($register->kode_lebpes,0, -5)}}"+ (parseInt(kode)+{{$i}}) +"/{{$siklus}}{{substr($register->kode_lebpes, 11)}}");
      $("#label_kode{{$no}}").text("{{substr($register->kode_lebpes,0, -5)}}"+ (parseInt(kode)+{{$i}}) +"/{{$siklus}}{{substr($register->kode_lebpes, 11)}}");
    <?php 
            $no++; 
        }
    ?>
  });
});
</script>
@endsection

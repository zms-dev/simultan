@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Cek Kwitansi {{$judul}}</div>
                <div class="panel-body">
                  <form class="form-inline">
                    <div class="form-group">
                      <label for="exampleInputEmail3">Tahun : </label>
                    </div>
                    <div class="form-group">
                      <div class="controls input-append date form_datetime tahun" data-link-field="dtp_input1">
                          @if(count($tahun))
                            <input size="16" type="text" value="{{$tahun}}" readonly class="form-control" name="tahun">
                          @else
                            <input size="16" type="text" value="2019" readonly class="form-control" name="tahun">
                          @endif
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-default">Proses</button>
                  </form>
                  <br>
                  <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                          <th>Tahun</th>
                          <th>Laboratoruim</th>
                          <th>Bidang</th>
                          <th>Parameter</th>
                          <th>Tarif</th>
                          <th>No&nbsp;VA</th>
                          <th>Tanggal</th>
                          <th>PDF</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(count($data))
                      <?php $no = 0; ?>
                      @foreach($data as $val)
                      <?php $no++;?>
                      <tr>
                      <form class="form-horizontal" action="{{url('cetak-kwitansi').'/'.$val->id}}" method="get" enctype="multipart/form-data" >
                          <td>{{date('Y', strtotime($val->created_at))}}</td>
                          <td>{{$val->nama_lab}}</td>
                          <td>{!!str_replace('|','<br/><br/>',$val->bidang) !!}</td>
                          <td>{!!str_replace('|','<br/><br/>',$val->parameter) !!}</td>
                          <td>{{number_format($val->jumlah_tarif)}}</td>
                          <td>@if($val->no_urut == NULL || $val->no_urut == '') PKS @else {{$val->no_urut}} @endif</td>
                          <input type="hidden" name="tahun" value="{{date('Y', strtotime($val->created_at))}}">
                          <input type="hidden" name="siklus" value="{{$val->siklus}}">
                          <td>
                            <div class="controls input-append date" data-link-field="dtp_input1">
                              @if(count($val->tanggal))
                                <input size="16" type="text" value="{{$val->tanggal->tanggal}}" required class="form_datetime form-control" name="tanggal" autocomplete="off">
                              @else
                                <input size="16" type="text" value="{{$val->updated_at}}" required class="form_datetime form-control" name="tanggal" autocomplete="off">
                              @endif
                                <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                          </td>
                          <td>
                            <button type="submit" class="btn btn-primary" data-toggle="modal">Cetak</button>
                          </td>
                        </form>
                      </tr>
                      @endforeach
                      @endif
                    </tbody>
                  </table>
                  <br>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('#example').DataTable( {
    "order": false
  } );
  $(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
  });
});

$(".tahun").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});

$("#checkAll").click(function () {
  $('input:checkbox').not(this).prop('checked', this.checked);
});

</script>
@endsection
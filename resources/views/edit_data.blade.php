@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Data Peserta</div>
                <div class="panel-body">
                    <form class="form-horizontal" action="{{url('edit-data')}}" method="post" enctype="multipart/form-data"  >
                      <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Instansi</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="nama_lab" name="nama_lab" placeholder="Nama Laboratorium" required value="{{$data->nama_lab}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="pemerintah" class="col-sm-3 control-label">Jenis Instansi</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="pemerintah" required readonly>
                                <option value="{{$data->Idbadan}}">{{$data->Badan}}</option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="pemerintah" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                          <small><b>Fasyankes BLK</b> hanya untuk semua balai laboratorium kesehatan pemerintah</small><br>
                          <small><b>Fasyankes BBLK</b> hanya untuk BBLK Palembang, BBLK Surabaya, BBLK Jakarta, dan BBLK Makassar</small>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="akreditasi" class="col-sm-3 control-label">Status Akreditasi</label>
                        <div class="col-sm-9">
                            <input id="status_akreditasi" type="text" class="form-control" name="akreditasi" value="{{$data->akreditasi}}" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="pemantapan_mutu" class="col-sm-3 control-label">Pemantapan Mutu Internal</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="pemantapan_mutu" id="pemantapan_mutu" required onchange="pemantapanmutu()">
                                <option value="{{$data->pemantapan_mutu}}">{{$data->pemantapan_mutu}}</option>
                                <option value="Dilaksanakan">Dilaksanakan</option>
                                <option value="Tidak Dilaksanakan">Tidak Dilaksanakan</option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group" id="dilaksanakandiv">
                        <label for="dilaksanakan" class="col-sm-3 control-label">Pilih Parameter</label>
                        <div class="col-sm-9">
                          <?php
                            $dilaksanakan = explode(",", $data->dilaksanakan);
                            $selected = "";
                            foreach ($dilaksanakan as $value) {
                                if($selected == ""){
                                    if(strpos($value, "Hematologi") !== false) {
                                        $selected = 'checked';
                                    }
                                }
                            }
                          ?>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" {{$selected}} value="Hematologi">Hematologi<br>
                          <?php
                            $selected = "";
                            foreach ($dilaksanakan as $value) {
                                if($selected == ""){
                                    if(strpos($value, "Kimia Klinik") !== false) {
                                        $selected = 'checked';
                                    }
                                }
                            }
                          ?>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" {{$selected}} value="Kimia Klinik">Kimia Klinik<br>
                          <?php
                            $selected = "";
                            foreach ($dilaksanakan as $value) {
                                if($selected == ""){
                                    if(strpos($value, "Urinalisa") !== false) {
                                        $selected = 'checked';
                                    }
                                }
                            }
                          ?>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" {{$selected}} value="Urinalisa">Urinalisa<br>
                          <?php
                            $selected = "";
                            foreach ($dilaksanakan as $value) {
                                if($selected == ""){
                                    if(strpos($value, "Mikroskopis BTA") !== false) {
                                        $selected = 'checked';
                                    }
                                }
                            }
                          ?>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" {{$selected}} value="Mikroskopis BTA">Mikroskopis BTA<br><?php
                            $selected = "";
                            foreach ($dilaksanakan as $value) {
                                if($selected == ""){
                                    if(strpos($value, "Mikroskopis Telur Cacing") !== false) {
                                        $selected = 'checked';
                                    }
                                }
                            }
                          ?>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" {{$selected}} value="Mikroskopis Telur Cacing">Mikroskopis Telur Cacing<br><?php
                            $selected = "";
                            foreach ($dilaksanakan as $value) {
                                if($selected == ""){
                                    if(strpos($value, "Anti HIV") !== false) {
                                        $selected = 'checked';
                                    }
                                }
                            }
                          ?>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" {{$selected}} value="Anti HIV">Anti HIV<br><?php
                            $selected = "";
                            foreach ($dilaksanakan as $value) {
                                if($selected == ""){
                                    if(strpos($value, "Sifilis") !== false) {
                                        $selected = 'checked';
                                    }
                                }
                            }
                          ?>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" {{$selected}} value="Sifilis">Sifilis<br><?php
                            $selected = "";
                            foreach ($dilaksanakan as $value) {
                                if($selected == ""){
                                    if(strpos($value, "HBsAg") !== false) {
                                        $selected = 'checked';
                                    }
                                }
                            }
                          ?>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" {{$selected}} value="HBsAg">HBsAg<br><?php
                            $selected = "";
                            foreach ($dilaksanakan as $value) {
                                if($selected == ""){
                                    if(strpos($value, "Anti HCV") !== false) {
                                        $selected = 'checked';
                                    }
                                }
                            }
                          ?>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" {{$selected}} value="Anti HCV">Anti HCV<br><?php
                            $selected = "";
                            foreach ($dilaksanakan as $value) {
                                if($selected == ""){
                                    if(strpos($value, "Mikroskopis Malaria") !== false) {
                                        $selected = 'checked';
                                    }
                                }
                            }
                          ?>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" {{$selected}} value="Mikroskopis Malaria">Mikroskopis Malaria<br><?php
                            $selected = "";
                            foreach ($dilaksanakan as $value) {
                                if($selected == ""){
                                    if(strpos($value, "Kimia Air") !== false) {
                                        $selected = 'checked';
                                    }
                                }
                            }
                          ?>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" {{$selected}} value="Kimia Air">Kimia Air Minum<br>
                          <?php
                            $selected = "";
                            foreach ($dilaksanakan as $value) {
                                if($selected == ""){
                                    if(strpos($value, "Identifikasi Bakteri dan Uji Kepekaan Antibiotik") !== false) {
                                        $selected = 'checked';
                                    }
                                }
                            }
                          ?>
                          <input type="checkbox" name="dilaksanakan[]" class="dilaksanakan" {{$selected}} value="Identifikasi Bakteri dan Uji Kepekaan Antibiotik">Identifikasi Bakteri dan Uji Kepekaan Antibiotik<br>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="alamat" class="col-sm-3 control-label">Alamat</label>
                        <div class="col-sm-9">
                          <textarea class="form-control" id="alamat" name="alamat" required>{{$data->alamat}}</textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="provinsi" class="col-sm-3 control-label">Provinsi</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="provinsi" id="provinsi" required onchange="Binaan()">
                                <option value="{{$data->Idprovinsi}}">{{$data->Provinsi}}</option>
                                @foreach($provinsi as $val)
                                <option value="{{$val->id}}">{{$val->name}}</option>
                                @endforeach
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="alasan_binaan" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                          <input type="text" name="alasan_binaan" class="form-control" value="{{$data->alasan_binaan}}" id="alasan_binaan" placeholder="Alasan Peserta Binaan">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="kota" class="col-sm-3 control-label">Kota / Kabupaten</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="kota" id="kota" required>
                                <option value="{{$data->Idkota}}" selected>{{$data->Kota}}</option>
                                @foreach($kota as $val)
                                <option value="{{$val->id}}">{{$val->name}}</option>
                                @endforeach
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="kecamatan" class="col-sm-3 control-label">Kecamatan</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="kecamatan" id="kecamatan" required>
                                <option value="{{$data->Idkecamatan}}" selected>{{$data->Kecamatan}}</option>
                                @foreach($kecamatan as $val)
                                <option value="{{$val->id}}">{{$val->name}}</option>
                                @endforeach
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="kelurahan" class="col-sm-3 control-label">Kelurahan / Desa</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="kelurahan" id="kelurahan" required>
                                <option value="{{$data->Idkelurahan}}" selected>{{$data->Kelurahan}}</option>
                                @foreach($kelurahan as $val)
                                <option value="{{$val->id}}">{{$val->name}}</option>
                                @endforeach
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="telp" class="col-sm-3 control-label">Telp / Fax</label>
                        <div class="col-sm-9">
                          <input type="text" name="telp" class="form-control numberdoang" id="telp" placeholder="Telp / Fax / Email" required value="{{$data->Telp}}" maxlength="15">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="penanggung" class="col-sm-3 control-label">Penanggung Jawab</label>
                        <div class="col-sm-9">
                          <input type="text" name="penanggung_jawab" class="form-control hurufdoangs" id="penanggung" placeholder="Penanggung Jawab" required value="{{$data->penanggung_jawab}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="personal" class="col-sm-3 control-label">Kontak Person</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control hurufdoangs" name="personal" id="personal" placeholder="Personal" required value="{{$data->personal}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="hp" class="col-sm-3 control-label">Email Kontak Person</label>
                        <div class="col-sm-9">
                          <input type="text" name="Email" class="form-control" data-validation="email" id="Email" placeholder="Email" required value="{{$data->Email}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="hp" class="col-sm-3 control-label">No. HP</label>
                        <div class="col-sm-9">
                          <input type="text" name="no_hp" class="form-control numberdoang" id="hp" placeholder="No. HP" required value="{{$data->no_hp}}" maxlength="15">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="wa" class="col-sm-3 control-label">No. Whatsapp</label>
                        <div class="col-sm-9">
                          <input type="text" name="no_wa" class="form-control numberdoang" id="wa" placeholder="No. Whatsapp" required value="{{$data->no_wa}}" maxlength="15">
                        </div>
                      </div>
                      <hr>
                      <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Peserta :</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control hurufdoang" id="name" name="name" required value="{{$data->name}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">Email Login:</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="email" data-validation="email" name="email" required value="{{$data->email}}" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="password" class="col-sm-3 control-label">Reset Password :</label>
                        <div class="col-sm-9">
                          <input type="password" class="form-control" id="password" name="password">
                        </div>
                      </div>
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                      <input type="submit" value="Simpan" class="btn btn-submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
@if ($data->alasan_binaan == NULL || $data->alasan_binaan == '') 
  $("#alasan_binaan").prop('type', 'hidden').val(''); 
@endif
function Binaan() {
  var x = document.getElementById("provinsi").value;
  console.log(x);
  if (x == '35' || x == '51' || x == '52' || x == '53' || x == '63' || x == '64' || x == '65') {
    $("#alasan_binaan").prop('type', 'text'); 
  }else{
    $("#alasan_binaan").prop('type', 'hidden').val(''); 
  }
}


(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  };
}(jQuery));

$(".numberdoang").inputFilter(function(value) {
  return /^-?\d*$/.test(value); });

$( ".hurufdoang" ).keypress(function(e) {
  var key = e.keyCode;
  console.log(key);
  if (key >= 65 && key <= 122 || key == 32) {
  }else{
    e.preventDefault();
  }
});

$( ".hurufdoangs" ).keypress(function(e) {
  var key = e.keyCode;
  console.log(key);
  if (key >= 48 && key <= 57) {
      e.preventDefault();
  }
});

$("#provinsi").change(function(){
  var val = $(this).val(), i;
  var y = document.getElementById('kota');
  $("#kecamatan").val("");
  $("#kelurahan").val("");
  y.innerHTML = "<option>Please Wait... </option>";
  $.ajax({
    type: "GET",
    url : "{{url('getkota').'/'}}"+val,
    success: function(addr){
      y.innerHTML = "<option> </option>"
      for(i = 0; i < addr.Hasil.length; i++){
        var option = document.createElement("option");
        option.value = addr.Hasil[i].Kode;
        option.text = addr.Hasil[i].Nama;
        y.add(option);
      }
      return false;
    }
  });
});

$("#kota").change(function(){
  var val = $(this).val(), i;
  var y = document.getElementById('kecamatan');
  $("#kelurahan").val("");
  y.innerHTML = "<option>Please Wait... </option>";
  $.ajax({
    type: "GET",
    url : "{{url('getkecamatan').'/'}}"+val,
    success: function(addr){
      y.innerHTML = "<option> </option>"
      for(i = 0; i < addr.Hasil.length; i++){
        var option = document.createElement("option");
        option.value = addr.Hasil[i].Kode;
        option.text = addr.Hasil[i].Nama;
        y.add(option);
      }
      return false;
    }
  });
});


$("#kecamatan").change(function(){
  var val = $(this).val(), i;
  var y = document.getElementById('kelurahan');
  y.innerHTML = "<option>Please Wait... </option>";
  $.ajax({
    type: "GET",
    url : "{{url('getkelurahan').'/'}}"+val,
    success: function(addr){
      y.innerHTML = "<option> </option>"
      for(i = 0; i < addr.Hasil.length; i++){
        var option = document.createElement("option");
        option.value = addr.Hasil[i].Kode;
        option.text = addr.Hasil[i].Nama;
        y.add(option);
      }
      return false;
    }
  });
});

$.validate({
  modules : 'location, date, security, file',
  onModulesLoaded : function() {
    $('#country').suggestCountry();
  }
});
</script>
@if(Session::has('message'))
    <div id="snackbar">{{ Session::get('message') }}</div>

    <script>
    $('document').ready(function(){
          var x = document.getElementById("snackbar")
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    });
    </script>
@endif
@endsection

@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Jadwal PNPME</div>

                <div class="panel-body">
                  <!-- {!!$jadwal->isi!!} -->
                  <br>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th><center>Kegiatan</center></th>
                                <th><center>Siklus 1</center></th>
                                <th><center>Siklus 2</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($schedule as $val)
                            <tr>
                                <td>{{$val->activity}}</td>
                                <td>{{$val->cycle_date_1}}</td>
                                <td>{{$val->cycle_date_2}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
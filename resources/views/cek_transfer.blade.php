  @extends('layouts.app')
  @extends('layouts.menu')
  @extends('layouts.menu_dashboard')
  @extends('layouts.menu_laporan')
  @extends('layouts.menu_evaluasi')


  @section('content')
  <div class="container">
      <div class="row">
          <div class="col-md-12">
              <div class="panel panel-default">
                  <div class="panel-heading">Cek Transfer</div>

                  <div class="panel-body">
                      <form class="form-horizontal" action="{{url('cek_transfer')}}" method="post" enctype="multipart/form-data" >
                        {{ csrf_field() }}
                      <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                              <th>Tahun</th>
                              <th>Laboratoruim</th>
                              <th>No. Hp</th>
                              <th>Siklus</th>
                              <th>Pembayaran</th>
                              <th>Bidang</th>
                              <th>Tarif</th>
                              <th>No&nbsp;VA</th>
                              <th>Cek<center><input type="checkbox" id="checkAll"></center></th>
                          </tr>
                        </thead>
                        <tbody>
                          @if(count($data))
                          <?php $no = 0; ?>
                          @foreach($data as $val)
                          <?php $no++;?>
                          <tr>
                            <td>{{date('Y', strtotime($val->created_at))}}<input type="hidden" name="email[]" value="{{$val->email}}" ></td>
                            <td>{{$val->nama_lab}}<input type="hidden" name="nama_lab[]" value="{{$val->nama_lab}}" ></td>
                            <td>{{$val->no_hp}}</td>
                            <td>{{$val->siklus}}</td>
                            <td>{{$val->type}}</td>
                            <td>
                              <table style="width: 100%">
                                @foreach($d as $bidang)
                                <tr>
                                  <td style="vertical-align: top;">{{$bidang->bidang}}</td>
                                  <td style="vertical-align: top;" width="10px"><center>:</center></td>
                                  <td style="vertical-align: top;">{{$bidang->parameter}}</td>
                                </tr>
                                @endforeach
                              </table>
                              <input type="hidden" name="parameter[]" value="{{$val->parameter}}" >
                            </td>
                            <input type="hidden" name="siklus[]" value="{{$val->siklus}}" >
                            <td>{{number_format($val->jumlah_tarif)}}</td>
                            <td style="text-align: center;">
                              {{$val->no_urut}}
                            </td>
                            <td>
                              <center><input type="checkbox" name="bidang[]" value="{{$val->id}}"></center>
                            </td>
                          </tr>
                          @endforeach
                          @endif
                        </tbody>
                      </table>
                      <label>Catatan :</label>
                      <p>*) Beri tanda &#10003; pada bidang yang sudah membayar</p>
                      <br>
                      <button type="submit" class="btn btn-info">Simpan</button>
                      <!-- <a href="cek-kwitansi"><button type="button" class="btn btn-info">Cetak Kwitansi</button></a> -->
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <script type="text/javascript">
  $(document).ready(function() {
     $('#example').DataTable( {
          "order": false
      } );
  });

  $("#checkAll").click(function () {
    $('input:checkbox').not(this).prop('checked', this.checked);
  });
  </script>
  @endsection
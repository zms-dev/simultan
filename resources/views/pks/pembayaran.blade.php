@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Perjanjian Kerjasama</div>

                <div class="panel-body">
                    <form class="form-horizontal" action="{{url('sptjm')}}" method="post" >
                    <label>PKS :</label>
                    <select name="pks" class="form-control" required>
                      <option></option>
                      @foreach($pks as $val)
                      <option value="{{$val->id}}">{{$val->pks}}</option>
                      @endforeach
                    </select>
                    <br>
                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                            <th>Tahun</th>
                            <th>Laboratorium</th>
                            <th>No Kontak Person</th>
                            <th>Bidang</th>
                            <th>Parameter</th>
                            <th>Tarif</th>
                            <th>Cek</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++;?>
                        <tr>
                          <td>{{date('Y', strtotime($val->created_at))}}<input type="hidden" name="email[]" value="{{$val->email}}" ></td>
                          <td>{{$val->nama_lab}}<input type="hidden" name="nama_lab[]" value="{{$val->nama_lab}}" ></td>
                          <td>{{$val->no_hp}}</td>
                          <td>{!!str_replace('|','<br/><br/>',$val->bidang) !!}</td>
                          <td>{!!str_replace('|','<br/><br/>',$val->parameter) !!}<input type="hidden" name="parameter[]" value="{{$val->parameter}}" ></td>
                          <input type="hidden" name="siklus[]" value="{{$val->siklus}}" >
                          <td>{{number_format($val->jumlah_tarif)}}</td>
                          <td>
                            <center><input type="checkbox" name="bidang[]" value="{{$val->id}}"></center>
                          </td>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>
                    <label>Catatan :</label>
                    <p>*) Beri tanda &#10003; pada bidang yang sudah dikirim</p>
                    <br>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Kirim</button>
                      <a href="kuitansi-pks"><button type="button" class="btn btn-info">Cetak Kwitansi</button></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $('#example').DataTable( {
    "order": false
  } );
  $(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
  });
});
</script>
@endsection
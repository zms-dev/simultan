@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Kwitansi Perjanjian Kerjasama</div>

                <div class="panel-body">
                  <form class="form-horizontal" method="post" enctype="multipart/form-data" >
                  {{ csrf_field() }}
                    <div class="form-group">
                      <label for="telp" class="col-sm-3">Tahun</label>
                      <div class="col-sm-9 controls input-append date" data-link-field="dtp_input1">
                          <input size="16" type="text" required class="form_datetime form-control" name="tahun" autocomplete="off">
                          <span class="add-on"><i class="icon-th"></i></span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="telp" class="col-sm-3">Perjanjian Kerjasama</label>
                      <div class="col-sm-9">
                        <select class="form-control" name="pks" required>
                          <option></option>
                          @foreach($pks as $val)
                          <option value="{{$val->id}}">{{$val->pks}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-info">Proses</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('#example').DataTable( {
    "order": false
  } );
  $(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    minView: 4,
    startView: 4
  });
});
</script>
@endsection
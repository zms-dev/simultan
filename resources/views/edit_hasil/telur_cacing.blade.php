@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><label>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL MIKROBIOLOGI MIKROSKOPIS TELUR CACING SIKLUS {{$siklus}} TAHUN {{$date}}</label></center><br>
                    
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$data->kode_peserta}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Penerimaan </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-link-field="dtp_input1">
                              <input size="16" type="text" value="{{$data->tgl_penerimaan}}" readonly class="form-control form_datetime" name="tgl_penerimaan">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kondisi Bahan Uji </label>
                        <div class="col-sm-9">  
                          <input type="radio" name="kondisi" required value="baik" {{ ($data->kondisi == 'baik') ? 'checked' : '' }}> Baik
                          <input type="radio" name="kondisi" value="pecah/tumpah" {{ ($data->kondisi == 'pecah/tumpah') ? 'checked' : '' }}> Pecah / Tumpah
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Pemeriksaan </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-link-field="dtp_input1">
                              <input size="16" type="text" value="{{$data->tgl_pemeriksaan}}" readonly class="form-control form_datetime" name="tgl_pemeriksaan">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Pemeriksa </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_pemeriksa" placeholder="Nama Pemeriksa" required value="{{$data->nama_pemeriksa}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nomor Hp </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nomor_hp" placeholder="Nomor HP" required value="{{$data->nomor_hp}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Pendidikan Pelaksana PME</label>
                        <div class="col-sm-5">
                            <select id="pendidikan" class="form-control" name="pendidikan">
                                <option value="{{$data->pendidikan_petugas}}">{{$data->tingkat}}</option>
                                @foreach($pendidikan as $pen)
                                    <option value="{{$pen->id}}">{{$pen->tingkat}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <div id="pendidikan_lain" class="pendidikan_lain">
                                <input id="inputpendidikan_lain" class="form-control" type="text" name="pendidikan_lain" placeholder="Pendidikan Lainnya" value="{{$data->pendidikan_lain}}" />
                            </div>
                        </div>
                    </div>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Bahan Uji</th>
                                <th>Reagen Yang Digunakan</th>
                                <th>Hasil Pemeriksaan</th>
                            </tr>
                        </thead>
                        <tbody id="wrapper">
                            <?php $i = 0; ?>
                            @foreach($datas as $val)
                            <?php $i++;?>
                            <tr>
                                <td>{{$i}}<input type="hidden" name="idtc[]" value="{{$val->id}}"></td>
                                <td><input type="text" class="form-control" size="16" name="kode_botol[]"  value="{{$val->kode_botol}}"></td>
                                <td>
                                    <select id="reagen{{$i}}" class="form-control" name="reagen[]" class="form-control" >
                                        <option value="{{$val->reagen}}">{{$val->reagen}}</option>
                                        <option value="Lugol">Lugol</option>
                                        <option value="Eosin">Eosin</option>
                                        <option value="PZ (NaCl 0,9 %)">PZ (NaCl 0,9 %)</option>
                                        <option value="Metode lain">Metode lain</option>
                                    </select>
                                    <div id="row_alat{{$i}}" class="inpualat{{$i}}">
                                        <input id="inpualat{{$i}}" class="form-control" type="text" name="reagen_lain[]" value="{{$val->reagen_lain}}" />
                                    </div>
                                </td>
                                <td>
                                    <input type="text" name="hasil[]" class="form-control hasil" value="{{$val->hasil}}" required>
                                    <!-- <textarea name="hasil[]" id="editor{{$i}}" rows="10" cols="80">{{$val->hasil}}</textarea> -->
                                </td>
                            </tr>
<script>                                 
$(function() {
    $('#row_alat{{$i}}').hide(); 
    $('#reagen{{$i}}').click(function(){
    var setan  = $("#reagen{{$i}} option:selected").text();
        if(setan.match('Metode lain.*')) {
            $('#row_alat{{$i}}').show(); 
        } else {
            $('#row_alat{{$i}}').hide(); 
        } 
    });
});
</script>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- <div><input type="button" id="more_fields" onclick="add_fields();" value="Tambah Baris" /></div> -->
                    <div class="col-sm-6">
                        <label>Catatan :</label>
                        <textarea class="form-control" name="catatan">{{$data->catatan}}</textarea>
                    </div>
                    <div class="col-sm-6">
                        <label>Nama Penanggung jawab lab :</label>
                        <input type="text" name="penanggung_jawab" class="form-control" value="{{$data->penanggung_jawab}}" required>
                    </div><br>

                      {{ csrf_field() }}
                    <div class="clearfix"></div>
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin-top: 20px"> &nbsp;
                    <input type="submit" name="simpan" value="Kirim" class="btn btn-submit" style="margin-top: 20px">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

$(function() {
    @if($data->pendidikan_lain != '' || $data->pendidikan_lain != NULL)
        $('#pendidikan_lain').show(); 
    @else
        $('#pendidikan_lain').hide(); 
    @endif
    $('#pendidikan').change(function(){
    var setan  = $("#pendidikan option:selected").text();
        if(setan.match('Lain - lain.*')) {
            $('#pendidikan_lain').show(); 
            $("#inputpendidikan_lain").prop('required',true);
        } else {
            $('#pendidikan_lain').hide(); 
            $("#inputpendidikan_lain").prop('required',false);
            $("#inputpendidikan_lain").val('');
        } 
    });
});
$("form input").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Tidak boleh kosong, atau isi dengan "-"');
    }
});
if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

$( function() {
    var availableTags = [
        "Ascaris lumbricoides",
        "Brugiya malayi",
        "Clonorchis sinensis",
        "Diphyllobothrium latum",
        "Enterobius vermicularis",
        "Echinococcus granulosus",
        "Fasciolopsis buski",
        "Fasciola hepatica",
        "Heligmosomoides polygyrus",
        "Hyminolepis diminuta",
        "Hymenolepis nana",
        "Hookworm",
        "Paragonimus westermani",
        "Schistosoma japonicum",
        "Schistosoma mansoni",
        "Trichuris trichiura",
        "Taenia solium",
        "Taenia saginata",
        "Trichinella spiralis",
        "Wuchereria brancofti",
    ];
    function split( val ) {
        return val.split( /,\s*/ );
    }
    function extractLast( term ) {
        return split( term ).pop();
    }
 
    $( ".hasil" )
    .on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
        event.preventDefault();
        }
    })
    .autocomplete({
        minLength: 0,
        source: function( request, response ) {
            response( $.ui.autocomplete.filter(
                availableTags, extractLast( request.term ) ) );
        },
        focus: function() {
            return false;
        },
        select: function( event, ui ) {
            var terms = split( this.value );
            terms.pop();
            terms.push( ui.item.value );
            terms.push( "" );
            this.value = terms.join( ", " );
            return false;
        }
    });
} );
</script>

<script type="text/javascript" >
    /*CKEDITOR.replace( 'editor1' );
    CKEDITOR.replace( 'editor2' );
    CKEDITOR.replace( 'editor3' );
    CKEDITOR.replace( 'editor4' );*/

$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});

function add_fields() {
    document.getElementById('wrapper').innerHTML += '<tr><td><input type="text" class="form-control" size="16" name="kode_botol[]"></td><td><input type="text" class="form-control" size="16" name="reagen[]"></td><td><input type="text" class="form-control" size="16" name="hasil[]"></td></tr>';
}
</script>
@endsection

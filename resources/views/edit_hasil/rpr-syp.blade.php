@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><label>FORMULIR HASIL PEMERIKSAAN PROGRAM  NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI SIKLUS {{$siklus}} TAHUN 2018<br> HASIL PEMERIKSAAN RPR</label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    @foreach($data as $val)
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$val->kode_lab}}" placeholder="Kode Peserta" readonly>
                        </div>
                        <input type="hidden" name="idmaster" value="{{$val->id}}">
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Pendidikan Pelaksana PME</label>
                        <div class="col-sm-5">
                            <select id="pendidikan" class="form-control" name="pendidikan">
                                <option value="{{$val->pendidikan_petugas}}">{{$val->tingkat}}</option>
                                @foreach($pendidikan as $pen)
                                    <option value="{{$pen->id}}">{{$pen->tingkat}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <div id="pendidikan_lain" class="pendidikan_lain">
                                <input id="inputpendidikan_lain" class="form-control" type="text" name="pendidikan_lain" placeholder="Pendidikan Lainnya" value="{{$val->pendidikan_lain}}" />
                            </div>
                        </div>
                    </div>
                    @break
                    @endforeach
                    <label>2. BAHAN UJI :</label>
                    <table class="table-bordered table">
                            <tr>
                                <td>Diterima tanggal :</td>
                                <td>
                                @foreach($data as $val)
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="{{$val->tgl_diterima}}" readonly class="form_datetime form-control" name="tgl_diterima">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                @break
                                @endforeach
                                </td>
                                <td>Diperiksa tanggal :</td>
                                <td>
                                @foreach($data as $val)
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="{{$val->tgl_diperiksa}}" readonly class="form_datetime form-control" name="tgl_diperiksa">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                @break
                                @endforeach
                                </td>
                            </tr>
                    </table>

                    <table class="table-bordered table">
                            <tr>
                                <td rowspan="6" width="20%"> Kondisi bahan Uji saat diterima</td>
                                <td>Kode Bahan Uji</td>
                                <td>Baik/Jernih </td>
                                <td>Keruh </td>
                                <td>Lain-lain</td>
                            </tr>
                            <?php  
                                $no = 0;
                                $idtabunga = ['1','2','3','4','5'];
                            ?>
                            @foreach($data as $val)
                            <tr>
                                <input type="hidden" value="{{$val->idimunologi}}" name="idbahan[]">
                                 <td>
                                    <select name="no_tabung[]" id="tabung{{$idtabunga[$no]}}1" class="form-control tabung1 badan{{$no}}" >
                                        <option value="{{$val->no_tabung}}">{{$val->no_tabung}}</option>
                                        @foreach($rujukan as $ruj)
                                            @if($ruj->group == $idtabunga[$no])
                                                <option value="{{$ruj->kode_bahan_uji}}">{{$ruj->kode_bahan_uji}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </td>
                                <td><input class="badan{{$no}}" type="checkbox" name="jenis[]" value="baik/jernih" {{ ($val->jenis == 'baik/jernih') ? 'checked' : '' }}></td>
                                <td><input class="badan{{$no}}" type="checkbox" name="jenis[]" value="keruh" {{ ($val->jenis == 'keruh') ? 'checked' : '' }}></td>
                                <td>
                                    <input type="checkbox" name="jenis[]" class="lain2{{$val->idimunologi}} badan{{$no}}" value="lain-lain" {{ ($val->jenis == 'lain-lain') ? 'checked' : '' }}>
                                    <div id="row_dim{{$no}}">
                                        <input type="text" name="lain[]" class="laintext1 form-control" value="{{$val->lain}}">
                                    </div>
                                </td>
                            </tr>
                            <script type="text/javascript">
                            $(function() {
                              enable_cb{{$no}}();
                              $(".lain2{{$val->idimunologi}}").click(enable_cb{{$no}});
                            });
                            function enable_cb{{$no}}() {
                              if (this.checked) {
                                $('#row_dim{{$no}}').show(); 
                              } else {
                                $('#row_dim{{$no}}').hide(); 
                              }
                            }
                            </script>
                            <?php $no++;?>
                            @endforeach
                    </table>
                    <label>3. REAGEN</label>
                    <table class="table table-bordered">
                        <tr>
                            <td>Nama Reagen</td>
                            @foreach($data1 as $val)
                            <input type="hidden" value="{{$val->id}}" name="idreagen[]">
                            <td>
                                <select id="alat1" class="form-control" name="nama_reagen[]" idx='1' class="form-control" required>
                                    <option value="{{$val->Idreagen}}">{{$val->namareagen}}</option>
                                    @foreach($reagen as $valu)
                                      <option value="{{$valu->id}}">{{$valu->reagen}}</option>
                                    @endforeach
                                </select>
                                <div id="row_alat1" class="inpualat1">
                                    <input id="inpualat1" class="form-control" type="text" name="reagen_lain[]" value="{{$val->reagen_lain}}" />
                                </div>
                            </td>
<script>                            
$(function() {
    var setan  = $("#alat1 option:selected").text();
    if(setan.match('Lain - lain.*')) {
        $('#inpualat1').prop('required', true).show(); 
    } else {
        $('#inpualat1').prop('required', false).hide(); 
    }

    $('#alat1').change(function(){
    var setan  = $("#alat1 option:selected").text();

    if(setan.match('Lain - lain.*')) {
    $('#inpualat1').prop('required', true).show(); 
    } else {
        $('#inpualat1').prop('required', false).hide(); 
    } 
    });


});
</script>                            
                            @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>Metode Pemeriksaan </td>
                            @foreach($data1 as $val)
                            <td>
                                <input type="text" name="metode[]" value="{{$val->metode}}" id="re1" required="" class="form-control" readonly>
                            </td>
                            @endforeach
                        </tr>
                        @foreach($data1 as $val)
                        <tr>
                            <td>Nama Produsen</td>
                            <td><input type="text" value="{{$val->nama_produsen}}" name="nama_produsen[]" class="form-control" id="produsen1" readonly=""></td>
                        </tr>
                        <tr>
                            <td>Nomor Lot/Batch</td>
                            <td><input type="text" name="nomor_lot[]" class="form-control" required value="{{$val->nomor_lot}}" id="lot1"></td>
                        </tr>
                        <tr>
                            <td>Tanggal Kadaluarsa</td>
                            <td>
                              <div class="controls input-append date form_datemo" data-link-field="dtp_input1">
                                  <input size="16" type="text"  value="{{$val->tgl_kadaluarsa}}" readonly class="form-control" name="tgl_kadaluarsa[]" required="tanggal1">
                                  <span class="add-on"><i class="icon-th"></i></span>
                              </div>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    <label>4. HASIL PEMERIKSAAN </label>
                    <table class="table table-bordered">
                        <tr>
                            <th width="17%">Kode Bahan Uji</th>
                            <td>Interpretasi hasil </td>
                            <td>Titer (Bila Aglutinasi)</td>
                        </tr>
                        <?php $no = 0;   
                              $n = 0;
                              $idtabung= ['1','2','3','4','5'];?>
                        @foreach($data2 as $hasil)
                        <input type="hidden" name="idhp[]" value="{{$hasil->id}}">
                        <tr>
                            <td><input type="text" name="kode_bahan_kontrol[]" id="tabung{{$idtabung[$no]}}2" class="form-control" required  value="{{$hasil->kode_bahan_kontrol}}" readonly></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control " id="non{{$no}}">
                                    <option value="{{$hasil->interpretasi}}">{{$hasil->interpretasi}}</option>
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                            <td>
                                <div class="pilihtiter{{$no}}">
                                    <select name="titer[]" class="form-control" id="tit{{$no}}" >
                                        <option value="{{$hasil->titer}}">{{$hasil->titer}}</option>
                                        <option value="-">-</option>
                                        <option value="1:1">1:1</option><option value="1:2">1:2</option><option value="1:4">1:4</option><option value="1:8">1:8</option><option value="1:16">1:16</option><option value="1:32">1:32</option><option value="1:64">1:64</option><option value="1:128">1:128</option><option value="1:256">1:256</option><option value="1:512">1:512</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
@for ($i = 0; $i < 6; $i++)
<script type="text/javascript">         
$(document).ready(function(){         
    if ($("#non{{$i}}").val() == 'Tanpa test') {
        console.log($("#non{{$i}}").val());
        $('.pilihtiter{{$i}}').hide();
        $('#tit{{$i}} option[value="-"]').show();
        $('#tit{{$i}} option[value="-"]').attr('selected','selected');
        // $('#tit{{$i}} option[value!="-"]').prop('disabled', true);
        $('#tit{{$i}}').attr('readonly','readonly').val('-').show();
    }else if($("#non{{$i}}").val() == 'Non Reaktif'){
        console.log($("#non{{$i}}").val());
        $('.pilihtiter{{$i}}').hide();
        $('#tit{{$i}} option[value="-"]').show();
        $('#tit{{$i}} option[value="-"]').attr('selected','selected');
        // $('#tit{{$i}} option[value!="-"]').prop('disabled', true);
        $('#tit{{$i}}').attr('readonly','readonly').val('-').show();
    }else{
        $('#tit{{$i}} option[value="-"]').hide();
    }
    
    $('#non{{$i}}').change(function(){
       if ($(this).val() == 'Tanpa test') {
        console.log($(this).val());
        $('.pilihtiter{{$i}}').hide();
        $('#tit{{$i}} option[value="-"]').show();
        $('#tit{{$i}} option[value="-"]').attr('selected','selected');
        // $('#tit{{$i}} option[value="-"]').prop('disabled', false);
        // $('#tit{{$i}} option[value!="-"]').prop('disabled', true);
        $('#tit{{$i}}').attr('readonly','readonly').val('-').show();
      }else if($(this).val() == 'Non Reaktif'){
        console.log($(this).val());
        $('.pilihtiter{{$i}}').hide();
        $('#tit{{$i}} option[value="-"]').show();
        $('#tit{{$i}} option[value="-"]').attr('selected','selected');
        // $('#tit{{$i}} option[value="-"]').prop('disabled', false);
        // $('#tit{{$i}} option[value!="-"]').prop('disabled', true);
        $('#tit{{$i}}').attr('readonly','readonly').val('-').show();
      }else if($(this).val() == 'Reaktif'){
        console.log($(this).val());
        $('.pilihtiter{{$i}}').show();
        $('#tit{{$i}} option[value="-"]').hide();
        $('#tit{{$i}} option').prop('disabled', false);
        // $('#tit{{$i}} option[value="-"]').prop('disabled', true);
        $('#tit{{$i}}').removeAttr('readonly').val('');
      }else{
        console.log($(this).val());
        $('#tit{{$i}} option[value="-"]').hide();
        $('.pilihtiter{{$i}}').show();
        $('#tit{{$i}} option').prop('disabled', false);
        $('#tit{{$i}}').removeAttr('readonly').val('');
      }
    });
});
</script>
@endfor                        
                        <?php $no++; ?>
                        @endforeach
                    </table>
                    @foreach($data as $val)
                    <div class="col-sm-6">
                        <label>Alasan bila tidak melakukan pemeriksaan :</label>
                        <textarea name="keterangan" class="form-control">{{$val->keterangan}}</textarea>
                    </div>
                    <div class="col-sm-6">
                        <label>Petugas yang melakukan pemeriksaan :</label>
                        <input type="text" name="petugas_pemeriksaan" class="form-control" value="{{$val->petugas_pemeriksaan}}" required>
                    </div><br>
                    @break
                    @endforeach
                      {{ csrf_field() }}
                    <div class="clearfix"></div>
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin-top: 20px"> &nbsp;
                    <input type="submit" name="simpan" value="Kirim" class="btn btn-submit" style="margin-top: 20px">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
@foreach($data as $val)
$(function() {
    @if($val->pendidikan_lain != '' || $val->pendidikan_lain != NULL)
        $('#pendidikan_lain').show(); 
    @else
        $('#pendidikan_lain').hide(); 
    @endif
    $('#pendidikan').change(function(){
    var setan  = $("#pendidikan option:selected").text();
        if(setan.match('Lain - lain.*')) {
            $('#pendidikan_lain').show(); 
            $("#inputpendidikan_lain").prop('required',true);
        } else {
            $('#pendidikan_lain').hide(); 
            $("#inputpendidikan_lain").prop('required',false);
            $("#inputpendidikan_lain").val('');
        } 
    });
});
@break
@endforeach
$('.decimal').keyup(function(){
    var val = $(this).val();
    if(isNaN(val)){
        val = val.replace(/[^0-9\.\<\>\:\/]/g,'');
        if(val.split('.').length>2) 
            val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = mm + '/' + dd + '/' + yyyy;
document.getElementById("demo").innerHTML = today;
</script>
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});

$(".form_datemo").datetimepicker({
    todayBtn: true,
    autoclose: true,
    format: "yyyy-mm-dd",
    minView: 3
});

$(document).ready(function(){
    $("#tabung11").change(function(){
        console.log($(this).val());
        $val = $(this).val();
        $('#tabung12').val($val);
    });
    $("#tabung21").change(function(){
        console.log($(this).val());
        $val1 = $(this).val();
        $('#tabung22').val($val1);
    });
    $("#tabung31").change(function(){
        $val2 = $(this).val();
        $('#tabung32').val($val2);
    });
    $("#tabung41").change(function(){
        $val1 = $(this).val();
        $('#tabung42').val($val1);
    });
    $("#tabung51").change(function(){
        $val2 = $(this).val();
        $('#tabung52').val($val2);
    });
    
    var e       = $(".tabung1"),
        val     = e.val(),
        arr     = [],
        tabung  = $(".tabung1");
        tabung.find('option').css("display", "");
    tabung.each(function(){
        var t = $(this);
        arr.push(t.val());
    });
    $.each(arr,function(idx, value){
        tabung.find('option[value="'+value+'"]').css("display", "none");
    });
});

$("input:checkbox").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input:checkbox[class='" + $box.attr("class") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});

$("select[name='nama_reagen[]']").change(function(){
  var val = $(this);
  console.log(val);
  var y = $('#re'+val.attr('idx'));
  var x = $('#produsen'+val.attr('idx'));
  var n = $('#lot'+val.attr('idx'));
  var t = $('#tanggal'+val.attr('idx'));
  $.ajax({
    type: "GET",
    url : "{{url('getreagenimun').'/'}}"+val.val(),
    success: function(addr){
        y.val(addr.Hasil[0].Metode);
        x.val(addr.Hasil[0].Produsen);
        if (y.val() == '-') {
            n.attr("required",false);
            t.attr("required",false);
        }else{
            n.attr("required",true);
            t.attr("required",true);
        }
    }
  });
});

$(".tabung1").change(function(){
    var e       = $(this),
        val     = e.val(),
        arr     = [],
        tabung  = $(".tabung1");
        tabung.find('option').css("display", "");
    tabung.each(function(){
        var t = $(this);
        arr.push(t.val());
    });
    $.each(arr,function(idx, value){
        tabung.find('option[value="'+value+'"]').css("display", "none");
    });
});
</script>
@endsection

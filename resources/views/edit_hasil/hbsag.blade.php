@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><label>FORMULIR HASIL PEMERIKSAAN PROGRAM  NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI SIKLUS {{$siklus}} TAHUN {{$date}}<br> HASIL PEMERIKSAAN HBsAg</label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    @foreach($data as $val)
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$val->kode_lab}}" placeholder="Kode Peserta" readonly>
                        </div>
                        <input type="hidden" name="idmaster" value="{{$val->id}}">
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Pendidikan Pelaksana PME</label>
                        <div class="col-sm-5">
                            <select id="pendidikan" class="form-control" name="pendidikan">
                                <option value="{{$val->pendidikan_petugas}}">{{$val->tingkat}}</option>
                                @foreach($pendidikan as $pen)
                                    <option value="{{$pen->id}}">{{$pen->tingkat}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <div id="pendidikan_lain" class="pendidikan_lain">
                                <input id="inputpendidikan_lain" class="form-control" type="text" name="pendidikan_lain" placeholder="Pendidikan Lainnya" value="{{$val->pendidikan_lain}}" />
                            </div>
                        </div>
                    </div>
                    @break
                    @endforeach
                    <label>2. BAHAN UJI :</label>
                    <table class="table-bordered table">
                            <tr>
                                <td>Diterima tanggal :</td>
                                <td>
                                @foreach($data as $val)
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="{{$val->tgl_diterima}}" readonly class="form_datetime form-control" name="tgl_diterima">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                @break
                                @endforeach
                                </td>
                                <td>Diperiksa tanggal :</td>
                                <td>
                                @foreach($data as $val)
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="{{$val->tgl_diperiksa}}" readonly class="form_datetime form-control" name="tgl_diperiksa">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                @break
                                @endforeach
                                </td>
                            </tr>
                    </table>

                    <table class="table-bordered table">
                            <tr>
                                <td rowspan="6" width="20%"> Kondisi bahan uji saat diterima</td>
                                <td>Kode Bahan Uji</td>
                                <td>Baik/Jernih </td>
                                <td>Keruh </td>
                                <td>Lain-lain</td>
                            </tr>
                            <?php  
                                $no = 0;
                                $idtabunga = ['1','2','3','4','5'];
                            ?>
                            @foreach($data as $val)
                            <tr>
                                <input type="hidden" value="{{$val->idimunologi}}" name="idbahan[]">
                                <td>
                                    <select name="no_tabung[]" id="tabung{{$idtabunga[$no]}}1" class="form-control tabung1 barang{{$no}}">
                                        <option value="{{$val->no_tabung}}">{{$val->no_tabung}}</option>
                                        @foreach($rujukan as $ruj)
                                            @if($ruj->group == $idtabunga[$no])
                                                <option value="{{$ruj->kode_bahan_uji}}">{{$ruj->kode_bahan_uji}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    {{-- <input type="text"  value="{{$val->no_tabung}}"> --}}</td>
                                <td><input class="barang{{$no}}" type="checkbox" name="jenis[]" value="baik/jernih" {{ ($val->jenis == 'baik/jernih') ? 'checked' : '' }}></td>
                                <td><input class="barang{{$no}}" type="checkbox" name="jenis[]" value="keruh" {{ ($val->jenis == 'keruh') ? 'checked' : '' }}></td>
                                <td><input type="checkbox" name="jenis[]" class="lain2{{$val->idimunologi}} barang{{$no}}" value="lain-lain" {{ ($val->jenis == 'lain-lain') ? 'checked' : '' }}>
                                </td>
                            </tr>
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $("#tabung{{$idtabunga[$no]}}1").attr('maxlength','5');
                                });
                                $("body").on("keypress","#tabung{{$idtabunga[$no]}}1", function(evt) {
                                    console.log(evt.keyCode)
                                    var charCode = (evt.which) ? evt.which : event.keyCode
                                    if ((charCode < 48 || charCode > 57) && (charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)
                                    return false;
                                });
                            </script>
                            <?php $no++;?>
                            @endforeach
                    </table>
                    <label>3. REAGEN</label>
                    <table class="table table-bordered">
                        <tr>
                            <td>Nama Reagen</td>
                            @foreach($data1 as $val)
                            <input type="hidden" value="{{$val->id}}" name="idreagen[]">
                            <td>
                                <select id="alat1" class="form-control" name="nama_reagen[]" idx='1' class="form-control" required>
                                    <option value="{{$val->Idreagen}}">{{$val->namareagen}}</option>
                                    @foreach($reagen as $valu)
                                      <option value="{{$valu->id}}">{{$valu->reagen}}</option>
                                    @endforeach
                                </select>
                                <div id="row_alat1" class="inpualat1">
                                    <input id="inpualat1" class="form-control" type="text" name="reagen_lain[]" value="{{$val->reagen_lain}}" />
                                </div>
                            </td>
<script>                            
$(function() {
    var setan  = $("#alat1 option:selected").text();
    if(setan.match('Lain - lain.*')) {
        $('#inpualat1').prop('required', true).show(); 
    } else {
        $('#inpualat1').prop('required', false).hide(); 
    }

    $('#alat1').change(function(){
    var setan  = $("#alat1 option:selected").text();

    if(setan.match('Lain - lain.*')) {
    $('#inpualat1').prop('required', true).show(); 
    } else {
        $('#inpualat1').prop('required', false).hide(); 
    } 
    });


});
</script> 
                        </td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Metode Pemeriksaan </td>
                            @foreach($data1 as $val)
                            <td>
                                <input type="text" name="metode[]" value="{{$val->metode}}" id="re1" required="" class="form-control" readonly>
                            </td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nama Produsen</td>
                            @foreach($data1 as $val)
                            <td><input type="text" name="nama_produsen[]" class="form-control" value="{{$val->nama_produsen}}" id="produsen1" readonly=""></td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nomor Lot/Batch</td>
                            @foreach($data1 as $val)
                            <td><input type="text" name="nomor_lot[]" class="form-control" value="{{$val->nomor_lot}}" required id="lot1"></td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Tanggal Kadaluarsa</td>
                            @foreach($data1 as $val)
                            <td>
                              <div class="controls input-append date" data-link-field="dtp_input1">
                                  <input size="16" type="text" value="{{$val->tgl_kadaluarsa}}" readonly class="form_datetime form-control" name="tgl_kadaluarsa[]" required id="tanggal1">
                                  <span class="add-on"><i class="icon-th"></i></span>
                              </div>
                            </td>
                            @endforeach
                        </tr>
                    </table>
                    <label>4. HASIL PEMERIKSAAN </label>
                    <table class="table table-bordered">
                        <tr>
                            <th>Kode Bahan Uji</th>
                            <td>Abs atau OD (A) (Bila dengan  EIA / Setara)</td>
                            <td>Cut Off (B) (Bila dengan EIA / Setara)</td>
                            <td>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA / Setara)</td>
                            <td>Interpretasi&nbsp;hasil&nbsp;</td>
                        </tr>
                        <?php $no = 0; $idtabunga = ['1','2','3','4','5']; ?>
                        @foreach($data2 as $hasil)
                        @if($hasil->tabung == '1')
                        <tr>
                            <input type="hidden" name="idhp[]" value="{{$hasil->id}}">
                            <td><input type="text" name="kode_bahan_kontrol[]" value="{{$hasil->kode_bahan_kontrol}}" id="tabung{{$idtabunga[$no]}}2" class="form-control" readonly></td>
                            <td><input type="text" class="decimal form-control" value="{{$hasil->abs_od}}" name="abs_od1[]"/></td>
                            <td><input type="text" class="decimal form-control" value="{{$hasil->cut_off}}" name="cut_off1[]"/></td>
                            <td><input type="text" class="decimal form-control" value="{{$hasil->sco}}" name="sco1[]"/></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non5">
                                    <option value="{{$hasil->interpretasi}}">{{$hasil->interpretasi}}</option>
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        @endif
                        <?php $no++; ?>
                        @endforeach
                    </table>
                    @foreach($data as $val)
                    <div class="col-sm-6">
                        <label>Alasan bila tidak melakukan pemeriksaan :</label>
                        <textarea name="keterangan" class="form-control">{{$val->keterangan}}</textarea>
                    </div>
                    <div class="col-sm-6">
                        <label>Petugas yang melakukan pemeriksaan :</label>
                        <input type="text" name="petugas_pemeriksaan" class="form-control" value="{{$val->petugas_pemeriksaan}}" required>
                    </div><br>
                    @break
                    @endforeach

                      {{ csrf_field() }}
                    <div class="clearfix"></div>
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin-top: 20px"> &nbsp;
                    <input type="submit" name="simpan" value="Kirim" class="btn btn-submit" style="margin-top: 20px">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
@foreach($data as $val)
$(function() {
    @if($val->pendidikan_lain != '' || $val->pendidikan_lain != NULL)
        $('#pendidikan_lain').show(); 
    @else
        $('#pendidikan_lain').hide(); 
    @endif
    $('#pendidikan').change(function(){
    var setan  = $("#pendidikan option:selected").text();
        if(setan.match('Lain - lain.*')) {
            $('#pendidikan_lain').show(); 
            $("#inputpendidikan_lain").prop('required',true);
        } else {
            $('#pendidikan_lain').hide(); 
            $("#inputpendidikan_lain").prop('required',false);
            $("#inputpendidikan_lain").val('');
        } 
    });
});
@break
@endforeach

$('.decimal').keyup(function(){
    var val = $(this).val();
    if(isNaN(val)){
        val = val.replace(/[^0-9\.\<\>\:\/]/g,'');
        if(val.split('.').length>2) 
            val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});

$(document).ready(function(){
    $("#tabung11").change(function(){
        $val = $(this).val();
        $('#tabung12').val($val);
    });
    $("#tabung21").change(function(){
        $val1 = $(this).val();
        $('#tabung22').val($val1);
    });
    $("#tabung31").change(function(){
        $val2 = $(this).val();
        $('#tabung32').val($val2);
    });
    $("#tabung41").change(function(){
        $val1 = $(this).val();
        $('#tabung42').val($val1);
    });
    $("#tabung51").change(function(){
        $val2 = $(this).val();
        $('#tabung52').val($val2);
    });
    
    var e       = $(".tabung1"),
        val     = e.val(),
        arr     = [],
        tabung  = $(".tabung1");
        tabung.find('option').css("display", "");
    tabung.each(function(){
        var t = $(this);
        arr.push(t.val());
    });
    $.each(arr,function(idx, value){
        tabung.find('option[value="'+value+'"]').css("display", "none");
    });
});
$("input:checkbox").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input:checkbox[class='" + $box.attr("class") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});

$("select[name='nama_reagen[]']").change(function(){
  var val = $(this);
  console.log(val);
  var y = $('#re'+val.attr('idx'));
  var x = $('#produsen'+val.attr('idx'));
  var n = $('#lot'+val.attr('idx'));
  var t = $('#tanggal'+val.attr('idx'));
  $.ajax({
    type: "GET",
    url : "{{url('getreagenimun').'/'}}"+val.val(),
    success: function(addr){
        y.val(addr.Hasil[0].Metode);
        x.val(addr.Hasil[0].Produsen);
        if (y.val() == '-') {
            n.attr("required",false);
            t.attr("required",false);
        }else{
            n.attr("required",true);
            t.attr("required",true);
        }
    }
  });
});

$(".tabung1").change(function(){
    var e       = $(this),
        val     = e.val(),
        arr     = [],
        tabung  = $(".tabung1");
        tabung.find('option').css("display", "");
    tabung.each(function(){
        var t = $(this);
        arr.push(t.val());
    });
    $.each(arr,function(idx, value){
        tabung.find('option[value="'+value+'"]').css("display", "none");
    });
});
</script>

<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: true,
        minView: 2
    });
</script>
@endsection

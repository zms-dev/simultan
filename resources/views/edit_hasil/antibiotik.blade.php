@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" method="post" enctype="multipart/form-data">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Hasil 
                    <input type="submit" name="simpan" value="Lembar 1" class="btn btn-default">
                    <input type="submit" name="simpan" value="Lembar 2" class="btn btn-default">
                    <input type="submit" name="simpan" value="Lembar 3" class="btn btn-default">
                    <!-- <a href="{{url('hasil-pemeriksaan/antibiotik/edit')}}/{{$peka->id_registrasi}}?y={{$siklus}}&lembar=1"><button type="button" class="btn btn-default" style="border-radius: 0px">Lembar 1</button></a>
                    <a href="{{url('hasil-pemeriksaan/antibiotik/edit')}}/{{$peka->id_registrasi}}?y={{$siklus}}&lembar=2"><button type="button" class="btn btn-default" style="border-radius: 0px">Lembar 2</button></a>
                    <a href="{{url('hasil-pemeriksaan/antibiotik/edit')}}/{{$peka->id_registrasi}}?y={{$siklus}}&lembar=3"><button type="button" class="btn btn-default" style="border-radius: 0px">Lembar 3</button></a> -->
                </div>
                <div class="panel-body">
                @foreach($data as $val)                    
                    <center><label> FORMULIR HASIL PEMERIKSAAN
                                    PENYELENGGARAAN NASIONAL PEMANTAPAN MUTU EKSTERNAL Identifikasi Bakteri dan Uji Kepekaan Antibiotik
                                    SIKLUS {{$siklus}}  TAHUN {{$date}}
                    </label></center><br>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Lembar <input type="hidden" name="id_bakteri" value="{{$val->id}}"></label>
                        <div class="col-sm-9">
                            <input type="text" name="lembar" class="form-control" value="{{$val->lembar}}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$val->kode_lab}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Instansi </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_instansi" value="{{$val->nama_instansi}}" placeholder="Nama Instansi">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Diterima Tanggal </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-link-field="dtp_input1">
                              <input size="16" type="text" value="{{$val->tgl_diterima}}" readonly class="form_datetime form-control" name="tgl_diterima">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kondisi Bahan </label>
                        <div class="col-sm-9">  
                          <input type="radio" required name="kondisi" value="baik" {{ ($val->kondisi == 'baik') ? 'checked' : '' }}> Baik
                          <input type="checkbox" id="checkbox1"/> Kurang Baik
                          <div id="autoUpdate" class="autoUpdate">
                              <input type="radio" name="kondisi" value="rusak" {{ ($val->kondisi == 'rusak') ? 'checked' : '' }}> Rusak
                              <input type="radio" name="kondisi" value="cair" {{ ($val->kondisi == 'cair') ? 'checked' : '' }}> Cair
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Alamat </label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="alamat">{{$val->alamat}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Pemeriksa </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_pemeriksa" value="{{$val->nama_pemeriksa}}" placeholder="Nama Pemeriksaan">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nomor HP Pemeriksa </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="no_hp" value="{{$val->no_hp}}" placeholder="No HP Pemeriksa">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Pendidikan Pelaksana PME</label>
                        <div class="col-sm-5">
                            <select id="pendidikan" class="form-control" name="pendidikan" required>
                                <option value="{{$val->pendidikan_petugas}}">{{$val->tingkat}}</option>
                                @foreach($pendidikan as $pen)
                                    <option value="{{$pen->id}}">{{$pen->tingkat}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <div id="pendidikan_lain" class="pendidikan_lain">
                                <input id="inputpendidikan_lain" value="{{$val->pendidikan_lain}}" class="form-control" type="text" name="pendidikan_lain" placeholder="Pendidikan Lainnya" />
                            </div>
                        </div>
                    </div>
                    <p> 
                    1. Masukan hasil mikroskopis dan identifikasi untuk spesimen pada tempat yang telah disediakan dibawah ini. Hasil identifikasi ditulis pada lembar hasil yang sudah disediakan <br>
                    2. Tuliskan jumlah item dan catatan bila ada kesalahan penulisan
                    </p><br>
                    <table class="table table-bordered">
                        <tr>
                            <th>Kode Bahan :</th>
                            <td>
                                <input type="text" class="form-control" value="{{substr($val->kode_lab,0, -5)}}{{$val->kd_bahan}}/{{$siklus}}{{substr($val->kode_lab, 11)}}" readonly>
                                <input type="hidden" name="kode_bahan" class="form-control" value="{{$val->kd_bahan}}" readonly>
                            </td>
                            <th>Jenis Bahan :</th>
                            <td><input type="text" class="form-control" name="jenis_bahan" value="{{$val->jenis_bahan}}"></td>
                        </tr>
                        <tr>
                            <th>Siklus :</th>
                            <td><input type="text" class="form-control" name="siklus" value="{{$val->siklus}}" readonly></td>
                            <th>Tanggal Pelaksanaan :</th>
                            <td>
                              <div class="controls input-append date" data-link-field="dtp_input1">
                                  <input size="16" type="text" value="{{$val->tgl_pelaksanaan}}" readonly class="form-control form_datetime" name="tgl_pelaksanaan">
                                  <span class="add-on"><i class="icon-th"></i></span>
                              </div>
                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <th colspan="2"><center>Metode Identifikasi Bakteri</center></th>
                        </tr>
                        <tr>
                            <td>
                                <p><select name="metode_konvensional"><option value="{{$val->metode_konvensional}}">{{$val->metode_konvensional}}</option><option value="V">V</option></select> Konvensional</p>
                                <p><select name="metode_otomatis"><option value="{{$val->metode_otomatis}}">{{$val->metode_otomatis}}</option><option value="V">V</option></select> Otomatis (sebutkan)</p>
                                <select name="metode_lainnya"><option value="{{$val->metode_lainnya}}">{{$val->metode_lainnya}}</option><option value="V">V</option></select> Lainnya (sebutkan)
                            </td>
                            <td>
                                <input type="text" name="metodetext1" class="form-control" value="{{$val->test_metode_identifikasi}}">
                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <th><center>Media yang digunakan</center></th>
                            <th><center>Buatan Sendiri</center></th>
                            <th><center>Media Komersial (Sebutkan)</center></th>
                        </tr>
                        <tr>
                            <td>
                                <select name="media_digunakan[]" class="selectpicker form-control" multiple>
                                    <option selected>{{$val->media_digunakan}}</option>
                                    <option>Blood agar</option>
                                    <option>Mac Conkey</option>
                                    <option>CLED</option>
                                    <option>Chromogenic Agar</option>
                                    <option>Lainnya</option>
                                </select>
                            </td>
                            <td>
                                <input type="checkbox" name="media_buatan" class="1 form-control" value="a" checked style="display: none;">
                                <input type="checkbox" name="media_buatan" class="1 form-control" value="v" {{ ($val->buatan_sendiri == 'v') ? 'checked' : '' }}>
                            </td>
                            <td>
                                <input type="text" name="media_komersial" class="form-control" value="{{$val->media_komersial}}">
                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <th width="30%"><center>Hasil Kultur</center></th>
                            <th><center>Pilih Salah satu</center></th>
                        </tr>
                        <tr>
                            <td>Tidak terdapat pertumbuhan bakteri patogen</td>
                            <td>
                                <input type="radio" name="hasil_kultur" value="tidak" {{ ($val->hasil_kultur == 'tidak') ? 'checked' : '' }}>
                            </td>
                        </tr>
                        <tr>
                            <td>Terdapat pertumbuhan bakteri patogen</td>
                            <td>
                                <input type="radio" name="hasil_kultur" value="ada" {{ ($val->hasil_kultur == 'ada') ? 'checked' : '' }}>
                            </td>
                        </tr>
                        <tr>
                            <td>Spesies <small>(Tanda "," untuk memisahkan)</small></td>
                            <td>
                                <input type="text" name="spesies_kultur" class="form-control hasil" value="{!!$val->spesies_kultur!!}">
                                <!-- <textarea name="spesies_kultur" id="editor1" rows="10" cols="80"></textarea> -->
                            </td>
                        </tr>
                    </table>

                    <h5>Pewarnaan</h5>
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2">Pewarnaan Gram</th>
                            <th>Gram Positif</th>
                            <th>Gram Negatif</th>
                            <th>Yeast</th>
                        </tr>
                        <tr>
                            <td>
                                <select name="pewarnaan_gram_p">
                                    <option value="{{$val->pewarnaan_gram_p}}">{{$val->pewarnaan_gram_p}}</option>
                                    <option value=""></option>
                                    <option value="Coccus">Coccus</option>
                                    <option value="Batang">Batang</option>
                                    <option value="Cocobacil">Cocobacil</option>
                                </select>
                            </td>
                            <td>
                                <select name="pewarnaan_gram_n">
                                    <option value="{{$val->pewarnaan_gram_n}}">{{$val->pewarnaan_gram_n}}</option>
                                    <option value=""></option>
                                    <option value="Coccus">Coccus</option>
                                    <option value="Batang">Batang</option>
                                    <option value="Cocobacil">Cocobacil</option>
                                </select>
                            </td>
                            <td>
                                <select name="pewarnaan_gram_y">
                                    <option value="{{$val->pewarnaan_gram_y}}">{{$val->pewarnaan_gram_y}}</option>
                                    <option value=""></option>
                                    <option value="Yeast">Yeast</option>
                                </select>
                            </td>
                        </tr>
                    </table>

                    <h5>Uji Biokimia Konvensional Untuk Golongan Gram Negatif Batang</h5>
                    <table class="table table-bordered">
                        <tr>
                            <th>Kebutuhan Oksidasi</th>
                            <th colspan="4">
                                <select class="form-control" name="kebutuhan_oksidasi">
                                    <option value="{{$val->kebutuhan_oksidasi}}">{{$val->kebutuhan_oksidasi}}</option>
                                    <option value=""></option>
                                    <option value="Strict aerob">Strict aerob</option>
                                    <option value="Fakultatif">Fakultatif</option>
                                    <option value="Mikroaerofilik">Mikroaerofilik</option>
                                    <option value="Capnophilic">Capnophilic</option>
                                    <option value="Aerotolerant">Aerotolerant</option>
                                </select>
                            </th>
                        <tr>
                            <th colspan="6">Fermentasi Karbohidrat dan uji biokimia lainnya (pilih salah satu)</th>
                        </tr>
                        <tr>
                        <?php $i = 1;?>
                        @foreach($val->fermentasinegatif as $fer)
                            <td>{{$fer->fermentasi}}<input type="hidden" name="id_fermentasi[]" value="{{$fer->id_fermentasi}}"></td>
                            <td colspan="2">
                                <select class="form-control" name="fermentasinegatif[]">
                                    <option value="{{$fer->status}}">{{$fer->status}}</option>
                                    <option value="Tanpa Test">Tanpa Test</option>
                                    @if($fer->id_fermentasi == 37 || $fer->id_fermentasi == 38 || $fer->id_fermentasi == 39)
                                    <option value="Resisten">Resisten</option>
                                    <option value="Sensitif">Sensitif</option>
                                    @else
                                    <option value="Positif">Positif</option>
                                    <option value="Negatif">Negatif</option>
                                    @endif
                                </select>
                            </td>
                        <?php 
                          if($i == 2){
                            $i =1;
                            echo "</tr><tr>";
                          }else{
                            $i++;
                          }
                        ?>
                        @endforeach
                            <td>Lain-lain</td>
                            <td colspan="2"><input type="text" name="fermentasilainn" class="form-control"></td>
                        </tr>
                    </table>

                    <h5>Uji Biokimia Konvensional Untuk Golongan Gram Positif Coccus</h5>
                    <table class="table table-bordered">
                        <tr>
                            <th>Hemolisa</th>
                            <th colspan="4">
                                <select class="form-control" name="hemolisa">
                                    <option value="{{$val->hemolisa}}">{{$val->hemolisa}}</option>
                                    <option value=""></option>
                                    <option value="Alfa Hemolisa">Alfa Hemolisa</option>
                                    <option value="Beta Hemolisa">Beta Hemolisa</option>
                                    <option value="Gamma Hemolisa">Gamma Hemolisa</option>
                                </select>
                            </th>
                        </tr>
                        <tr>
                            <th>Kebutuhan terhadap faktor X + V</th>
                            <th colspan="4">
                                <select class="form-control" name="faktorxv">
                                    <option value="{{$val->faktorxv}}">{{$val->faktorxv}}</option>
                                    <option value=""></option>
                                    <option value="Faktor X">Faktor X</option>
                                    <option value="Faktor V">Faktor V</option>
                                    <option value="Faktor X + V">Faktor X + V</option>
                                </select>
                            </th>
                        </tr>
                        <tr>
                            <th>Kebutuhan Oksigen</th>
                            <th colspan="4">
                                <select class="form-control" name="kebutuhan_oksigen">
                                    <option value="{{$val->kebutuhan_oksigen}}">{{$val->kebutuhan_oksigen}}</option>
                                    <option value=""></option>
                                    <option>Strict aero</option>
                                    <option>Akultatif aero</option>
                                    <option>Mikroaerofilik</option>
                                    <option>Capnophilic</option>
                                    <option>Aerotolerant</option>
                                </select>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="6">Fermentasi Karbohidrat dan uji biokimia lainnya (pilih salah satu)</th>
                        </tr>
                        <tr>
                        <?php $i = 1;?>
                        @foreach($val->fermentasipositif as $fer)
                            <td>{{$fer->fermentasi}}<input type="hidden" name="id_fermentasi_positif[]" value="{{$fer->id_fermentasi}}"></td>
                            <td colspan="2">
                                <select class="form-control" name="fermentasipositif[]">
                                    <option value="{{$fer->status}}">{{$fer->status}}</option>
                                    <option value="Tanpa Test">Tanpa Test</option>
                                    @if($fer->id_fermentasi == 37 || $fer->id_fermentasi == 38 || $fer->id_fermentasi == 39)
                                    <option value="Resisten">Resisten</option>
                                    <option value="Sensitif">Sensitif</option>
                                    @else
                                    <option value="Positif">Positif</option>
                                    <option value="Negatif">Negatif</option>
                                    @endif
                                </select>
                            </td>
                        <?php 
                          if($i == 2){
                            $i =1;
                            echo "</tr><tr>";
                          }else{
                            $i++;
                          }
                        ?>
                        @endforeach
                            <td>Lain-lain</td>
                            <td colspan="2"><input type="text" name="fermentasilainp" class="form-control"></td>
                        </tr>
                    </table>

                    <h5>Uji Biokimia Automatic</h5>
                    <table class="table table-bordered">
                        <tr>
                            <th>Vitek 2</th>
                            <td>
                                <select name="vitek[]" class="selectpicker form-control" multiple>
                                    <option selected value="{{$val->vitek}}">{{$val->vitek}}</option>
                                    <option value="GN Card">GN Card</option>
                                    <option value="GP Card">GP Card</option>
                                    <option value="BCL Card">BCL Card</option>
                                    <option value="NH Card">NH Card</option>
                                    <option value="ANC Card">ANC Card</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>API</th>
                            <td>
                                <select name="api[]" class="selectpicker form-control" multiple>
                                    <option selected value="{{$val->api}}">{{$val->api}}</option>
                                    <option value="API NH">API NH</option>
                                    <option value="API CANDIDA">API CANDIDA</option>
                                    <option value="API 20 E">API 20 E</option>
                                    <option value="API 20 NE">API 20 NE</option>
                                    <option value="API STAPH">API STAPH</option>
                                    <option value="API 20 STREP">API 20 STREP</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>API</th>
                            <td>
                                <select name="api2[]" class="selectpicker form-control" multiple>
                                    <option selected value="{{$val->api2}}">{{$val->api2}}</option>
                                    <option>API 20 A</option>
                                    <option>API CAMPY</option>
                                    <option>API CORYNE</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>BD Phoenik</th>
                            <td>
                                <select name="bd_phoenik[]" class="selectpicker form-control" multiple>
                                    <option selected value="{{$val->bd_phoenik}}">{{$val->bd_phoenik}}</option>
                                    <option value="NID panel">NID panel</option>
                                    <option value="PID panel">PID panel</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Lain - lain</th>
                            <td>
                                <input type="text" name="data_lain" class="form-control" value="{{$val->data_lain}}">
                            </td>
                        </tr>
                        <tr>
                            <td><b>Spesies <small>(Tanda "," untuk memisahkan)</small></b></td>
                            <td>
                                <input type="text" name="spesies_auto" class="form-control hasil" value="{!!$val->spesies_auto!!}">
                            </td>
                        </tr>
                    </table>

                    <center><h2>UJI KEPEKAAN ANTIBIOTIK</h2></center>

                    <table class="table table-bordered">
                        <tr>
                            <th>Hasil Identifikasi<input type="hidden" name="id_peka" value="{{$peka->id}}"></th>
                            <td><input type="text" name="hasil_identifikasi" value="{{$peka->hasil_identifikasi}}" class="form-control"></td>
                            <th><center>Metode/automatisasi</center></th>
                        </tr>
                        <tr>
                            <th>Standart</th>
                            <td>
                                <select name="standart[]" class="selectpicker form-control" multiple>
                                    <option value="{{$peka->standart}}" selected>{{$peka->standart}}</option>
                                    <option value="CDS">CDS</option>
                                    <option value="CLSI">CLSI</option>  
                                    <option value="EUCAST">EUCAST</option>
                                </select>
                            </td>
                            <td>
                                <select name="metode[]" class="selectpicker form-control" multiple>
                                    <option value="{{$peka->metode}}" selected>{{$peka->metode}}</option>
                                    <option value="Disk Difusi">Disk Difusi</option>
                                    <option value="Vitex 2">Vitex 2</option>
                                    <option value="Phoenix">Phoenix</option>
                                    <option value="Agar Dilusi">Agar Dilusi</option>
                                    <option value="Lainnya">Lainnya</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <th>Jenis Antibiotik</th>
                            <th>Disk difusi</th>
                            <th>Interpretasi hasil</th>
                            <th>M.I.C</th>
                            <th>Interpretasi hasil</th>
                            <th>Kesimpulan</th>
                        </tr>
                        @foreach($val->kepekaan as $peka)
                        <tr><input type="hidden" name="id_jenis_antibiotik[]" value="{{$peka->id_jenis_antibiotik}}">
                            <td>
                                @if($peka->antibiotik != 'Lain - lain')
                                    {{$peka->antibiotik}}
                                    <input type="hidden" class="form-control" name="lain_lain[]" value="{{$peka->lain_lain}}">
                                @else
                                    <input type="text" class="form-control" name="lain_lain[]" value="{{$peka->lain_lain}}" placeholder="Lain-lain">
                                @endif
                            </td>
                            <td><input type="text" name="disk[]" value="{{$peka->disk}}" class="form-control"></td>
                            <td>
                                <select name="hasil1[]" class="form-control">
                                    <option value="{{$peka->hasil1}}">{{$peka->hasil1}}</option>
                                    @if($peka->hasil1 == "")
                                    @else
                                    <option value=""></option>
                                    @endif
                                    <option value="S">S</option>
                                    <option value="I">I</option>
                                    <option value="R">R</option>
                                </select>
                            </td>
                            <td><input type="text" name="mic[]" class="form-control" value="{{$peka->mic}}"></td>
                            <td>
                                <select name="hasil2[]" class="form-control">
                                    <option value="{{$peka->hasil2}}">{{$peka->hasil2}}</option>
                                    @if($peka->hasil2 == "")
                                    @else
                                    <option value=""></option>
                                    @endif
                                    <option value="S">S</option>
                                    <option value="I">I</option>
                                    <option value="R">R</option>
                                </select>
                            </td>
                            <td>
                                <select name="kesimpulan[]" class="form-control">
                                    <option value="{{$peka->kesimpulan}}">{{$peka->kesimpulan}}</option>
                                    @if($peka->kesimpulan == "")
                                    @else
                                    <option value=""></option>
                                    @endif
                                    <option value="S">S</option>
                                    <option value="I">I</option>
                                    <option value="R">R</option>
                                </select>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    <table class="table table-bordered">
                        <!-- <tr>
                            <th>Golongan MRSA</th>
                            <td>
                                <select class="form-control" name="mrsa">
                                    <option value="{{$val->mrsa}}">{{$val->mrsa}}</option>
                                    <option value=""></option>
                                    <option value="YA">YA</option>
                                    <option value="TIDAK">TIDAK</option>
                                </selec>
                            </td>
                        </tr>
                        <tr>
                            <th>Golongan ESBL</th>
                            <td>
                                <select class="form-control" name="esbl">
                                    <option value="{{$val->esbl}}">{{$val->esbl}}</option>
                                    <option value=""></option>
                                    <option value="YA">YA</option>
                                    <option value="TIDAK">TIDAK</option>
                                </selec>
                            </td>
                        </tr> -->
                        <tr>
                            <th>
                                Resistensi
                            </th>
                            <td>
                                <select name="resistensi" id="resistensi" class="form-control" required>
                                    <option value="{{$val->resistensi}}">{{$val->name_resistensi}}</option>
                                    @foreach($resistensi as $res)
                                    <option value="{{$res->id}}">{{$res->name}}</option>
                                    @endforeach
                                </select>
                                <div id="row_dim" class="inputlain">
                                    <input id="inputlain" class="form-control" type="text" name="resistensi_lain" value="{{$val->resistensi_lain}}" />
                                </div>
                            </td>
                        </tr>
                        <script type="text/javascript">
                            $(function() {
                                @if($val->resistensi_lain == '')
                                $('#row_dim').hide();
                                @else
                                $('#row_dim').show();
                                $("#inputlain").prop('required',true);
                                @endif
                                $('#resistensi').change(function(){
                                var setan  = $("#resistensi option:selected").text();
                                    if(setan.match('Lain - Lain.*')) {
                                        $('#row_dim').show(); 
                                        $("#inputlain").prop('required',true);
                                    } else {
                                        $('#row_dim{{$val->id}}').hide(); 
                                        $("#inputlain").prop('required',false);
                                        $("#inputlain").val('');
                                    } 
                                });
                            });
                        </script>
                    </table>
                    <div class="col-sm-12">
                        <label>Nama Penanggung jawab lab :</label>
                        <input type="text" name="penanggung_jawab" value="{{$val->penanggung_jawab}}" class="form-control" required>
                    </div><br>
                @endforeach
                {{ csrf_field() }}
                <div class="clearfix"></div>
                <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin-top: 20px"> &nbsp;
                @if($val->lembar == '3')
                <input type="submit" name="simpan" value="Kirim" class="btn btn-submit" style="margin-top: 20px">
                @endif
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<script>

//CKEDITOR.replace( 'editor1' );
//CKEDITOR.replace( 'editor2' );
$(document).ready(function(){

    $('#autoUpdate').fadeOut('slow');
    $('#checkbox1').change(function(){
    if(this.checked)
        $('#autoUpdate').fadeIn('slow');
    else
        $('#autoUpdate').fadeOut('slow');

    });
});

@foreach($data as $val)
    $(function() {
        @if($val->pendidikan_lain != '' || $val->pendidikan_lain != NULL)
            $('#pendidikan_lain').show(); 
        @else
            $('#pendidikan_lain').hide(); 
        @endif
        $('#pendidikan').change(function(){
        var setan  = $("#pendidikan option:selected").text();
            if(setan.match('Lain - lain.*')) {
                $('#pendidikan_lain').show(); 
                $("#inputpendidikan_lain").prop('required',true);
            } else {
                $('#pendidikan_lain').hide(); 
                $("#inputpendidikan_lain").prop('required',false);
                $("#inputpendidikan_lain").val('');
            } 
        });
    });
@endforeach

$("input:checkbox").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input:checkbox[class='" + $box.attr("class") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});


var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

function dots(){
var dateBox = document.getElementById('hasil22')
    var chars = dateBox.value.length;
    var text = dateBox.value;

    if (chars == 1) {
        dateBox.value = text + ".";
    }

var dateBox = document.getElementById('hasil25')
    var chars = dateBox.value.length;
    var text = dateBox.value;

    if (chars == 2) {
        dateBox.value = text + ".";
    }

var dateBox = document.getElementById('hasil26')
    var chars = dateBox.value.length;
    var text = dateBox.value;

    if (chars == 2) {
        dateBox.value = text + ".";
    }

var dateBox = document.getElementById('hasil30')
    var chars = dateBox.value.length;
    var text = dateBox.value;

    if (chars == 1) {
        dateBox.value = text + ".";
    }
        
var dateBox = document.getElementById('hasil31')
    var chars = dateBox.value.length;
    var text = dateBox.value;

    if (chars == 2) {
        dateBox.value = text + ".";
    }
}

function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode

  // CHECK IF NUMBER
  if((charCode >=48 && charCode<=57) || (charCode>=96 && charCode<=105)){
     //IF KEY IS A NUMBER CALL DOTS
    dots();
  }
  else if(charCode==37||charCode==39||charCode==46 || charCode==8){
      // LET LEFT RIGHT BACKSPACE AND DEL PASS
}
    else{
    // BLOCK ALL OTHER KEYS
    evt.preventDefault();
    }
}

$( function() {
    var availableTags = [
"Actinomyces radingae","Arcanobacterium haemolyticum","Arthrobacter cumminsii","Brevibacterium casei","Brevibacterium epidermidis","Brevibacterium iodinum","Brevibacterium linens","Brevibacterium luteolum","Cellulosimicrobium cellulans","Clavibacter michiganensis","Corynebacterium accolens/ tuberculostearicum","Corynebacterium afermentans","Corynebacterium amycolatum/ xerosis","Corynebacterium argentoratense","Corynebacterium aurimucosum","Corynebacterium auris","Corynebacterium bovis","Corynebacterium confusum","Corynebacterium coyleae","Corynebacterium cystitidis","Corynebacterium diphtheriae","Corynebacterium freneyi","Corynebacterium glucuronolyticum","Corynebacterium glutamicum","Corynebacterium group F-1","Corynebacterium jeikeium","Corynebacterium kroppenstedtii","Corynebacterium kutscheri","Corynebacterium macginleyi","Corynebacterium mastitidis","Corynebacterium minutissimum","Corynebacterium mucifaciens","Corynebacterium propinquum","Corynebacterium pseudodiphtheriticum","Corynebacterium pseudotuberculosis","Corynebacterium renale","Corynebacterium simulans","Corynebacterium striatum","Corynebacterium ulcerans","Corynebacterium urealyticum","Dermabacter hominis","Dietzia spp","Gordonia spp","Lactobacillus acidophilus","Lactobacillus gasseri","Lactobacillus paracasei","Lactobacillus plantarum","Abiotrophia defectiva","Aerococcus urinae","Aerococcus viridans","Alloiococcus otitis","Dermacoccus nishinomiyaensis/Kytococcus sedentarius","Enterococcus avium","Enterococcus casseliflavus","Enterococcus cecorum","Enterococcus columbae","Enterococcus durans","Enterococcus faecalis","Enterococcus faecium","Enterococcus gallinarum","Enterococcus hirae","Enterococcus raffinosus","Enterococcus saccharolyticus","Erysipelothrix rhusiopahiae","Facklamia hominis","Gardnerella vaginalis","Gemella bergeri","Gemella haemolysans","Gemella morbillorum","Gemella sanguinis","Globicatella sanguinis","Globicatella sulfidifaciens","Granulicatella adiacens","Granulicatella elegans","Helcococcus kunzii","Kocuria kristinae","Kocuria rhizophila","Kocuria rosea","Kocuria varians","Lactococcus garvieae","Lactococcus lactis ssp. cremoris","Lactococcus lactis ssp. lactis","Lactococcus raffinolactis","Leuconostoc citreum","Leuconostoc lactis","Leuconostoc mesenteroides ssp. cremoris","Leuconostoc mesenteroides ssp. dextranicum","Leuconostoc mesenteroides ssp. mesenteroides","Leuconostoc pseudomesenteroides","Listeria grayi","Listeria innocua","Listeria ivanovii","Listeria monocytogenes","Listeria seeligeri","Listeria welshimeri","Micrococcus luteus/lylae","Pediococcus acidilactici","Pediococcus pentosaceus","Rothia dentocariosa","Rothia mucilaginosa","Staphylococcus arlettae","Staphylococcus aureus ","Staphylococcus auricularis","Staphylococcus capitis","Staphylococcus caprae","Staphylococcus carnosus ssp. carnosus","Staphylococcus chromogenes","Staphylococcus cohnii ssp. cohnii","Staphylococcus cohnii ssp. urealyticus","Staphylococcus epidermidis","Staphylococcus equorum","Staphylococcus gallinarum","Staphylococcus haemolyticus","Staphylococcus hominis ssp. hominis","Staphylococcus hominis ssp. novobiosepticus","Staphylococcus hyicus","Staphylococcus intermedius","Staphylococcus kloosii","Staphylococcus lentus","Staphylococcus lugdunensis","Staphylococcus pseudintermedius","Staphylococcus saprophyticus","Staphylococcus schleiferi","Staphylococcus sciuri","Staphylococcus simulans","Staphylococcus warneri","Staphylococcus xylosus","Streptococcus agalactiae","Streptococcus alactolyticus","Streptococcus anginosus","Streptococcus canis","Streptococcus constellatus ssp. constellatus","Streptococcus constellatus ssp. pharyngis","Streptococcus cristatus","Streptococcus downei","Streptococcus dysgalactiae ssp. dysgalactiae","Streptococcus dysgalactiae ssp. equisimilis","Streptococcus equi ssp. equi","Streptococcus equi ssp. zooepidemicus","Streptococcus equinus","Streptococcus gallolyticus ssp. gallolyticus","Streptococcus gallolyticus ssp. pasteurianus","Streptococcus gordonii","Streptococcus hyointestinalis","Streptococcus infantarius ssp. coli (Str. lutetiensis)","Streptococcus infantarius ssp. infantarius","Streptococcus intermedius","Streptococcus mitis/Streptococcus oralis","Streptococcus mutans","Streptococcus ovis","Streptococcus parasanguinis","Streptococcus pluranimalium","Streptococcus pneumoniae","Streptococcus porcinus","Streptococcus pseudoporcinus","Streptococcus pyogenes","Streptococcus salivarius ssp. salivarius","Streptococcus salivarius ssp. thermophilus","Streptococcus sanguinis","Streptococcus sobrinus","Streptococcus suis I","Streptococcus suis II","Streptococcus thoraltensis","Streptococcus uberis","Streptococcus vestibularis","Vagococcus fluvialis","Lactobacillus rhamnosus","Lactobacillus sakei ssp sakei","Leifsonia aquatica","Microbacterium lacticum","Microbacterium spp","Rhodococcus coprophilus/ erythropolis/ globerulus","Rhodococcus equi","Rhodococcus fascians","Rhodococcus opacus","Rhodococcus rhodnii","Rhodococcus rhodochrous","Rhodococcus ruber","Trueperella bernardiae (Arcanobacterium bernardiae)","Trueperella pyogenes (Arcanobacterium pyogenes)","Turicella otitidis","Budvicia aquatica","Buttiauxella agrestis","Cedecea davisae","Cedecea lapagei","Citrobacter amalonaticus","Citrobacter braakii","Citrobacter farmeri","Citrobacter freundii","Citrobacter koseri","Citrobacter sedlakii","Citrobacter youngae","Cronobacter sakazakii group","Edwardsiella hoshinae","Edwardsiella tarda","Enterobacter aerogenes","Enterobacter amnigenus 1","Enterobacter amnigenus 2","Enterobacter asburiae","Enterobacter cancerogenus","Enterobacter cloacae complex","Enterobacter gergoviae","Escherichia coli","Escherichia coli O157","Escherichia fergusonii","Escherichia hermannii","Escherichia vulneris","Ewingella americana ","Hafnia alvei","Klebsiella oxytoca ","Klebsiella pneumoniae ssp. ozaenae","Klebsiella pneumoniae ssp. pneumoniae","Klebsiella pneumoniae ssp. rhinoscleromatis","Kluyvera ascorbata","Kluyvera cryocrescens","Kluyvera intermedia (formerly known as Enterobacter intermedius)","Leclercia adecarboxylata","Moellerella wisconsensis","Morganella morganii ssp. morganii","Morganella morganii ssp. sibonii","Pantoea agglomerans ","Pantoea spp.","Plesiomonas shigelloides","Proteus hauseri","Proteus mirabilis","Proteus penneri","Proteus vulgaris","Providencia alcalifaciens","Providencia rettgeri","Providencia rustigianii","Providencia stuartii","Rahnella aquatilis","Raoultella ornithinolytica","Raoultella planticola","Roseomonas gilardii","Salmonella enterica ssp. arizonae","Salmonella enterica ssp. diarizonae","Salmonella group","Salmonella ser. Gallinarum","Salmonella ser. Paratyphi A","Salmonella ser. Typhi","Serratia ficaria","Serratia fonticola","Serratia liquefaciens group","Serratia marcescens ","Serratia odorifera ","Serratia plymuthica ","Serratia rubidaea","Shigella group","Shigella sonnei","Yersinia aldovae","Yersinia enterocolitica/frederiksenii","Yersinia intermedia","Yersinia kristensenii","Yersinia pestis","Yersinia pseudotuberculosis","Yersinia ruckeri","Yokenella regensburgei","Achromobacter denitrificans","Achromobacter xylosoxidans","Acinetobacter baumannii complex","Acinetobacter haemolyticus","Acinetobacter junii","Acinetobacter lwoffii","Acinetobacter radioresistens","Acinetobacter ursingii","Actinobacillus ureae","Aeromonas hydrophila/Aeromonas caviae","Aeromonas salmonicida","Aeromonas sobria","Aeromonas veronii","Alcaligenes faecalis ssp. faecalis","Bordetella bronchiseptica","Bordatella hinzii","Bordetella trematum","Brevundimonas diminuta/vesicularis","Brucella melitensis","Burkholderia cepacia group","Burkholderia gladioli","Burkholderia mallei","Burkholderia pseudomallei","Chromobacterium violaceum","Chryseobacterium gleum","Chryseobacterium indologenes","Comamonas testosteroni","Cupriavidus pauculus","Delftia acidovorans","Elizabethkingia meningoseptica","Francisella tularensis","Grimontia hollisae","Mannheimia haemolytica","Methylobacterium spp.","Moraxella group","Myroides spp.","Neisseria animaloris/zoodegmatis","Ochrobactrum anthropi","Oligella ureolytica","Paracoccus yeei","Pasteurella aerogenes","Pasteurella canis","Pasteurella dagmatis","Pasteurella multocida","Pasteurella pneumotropica","Pasteurella testudinis","Photobacterium damselae","Pseudomonas aeruginosa","Pseudomonas alcaligenes","Pseudomonas fluorescens","Pseudomonas luteola","Pseudomonas mendocina","Pseudomonas oleovorans","Pseudomonas oryzihabitans","Pseudomonas putida","Pseudomonas stutzeri","Ralstonia mannitolilytica","Ralstonia pickettii","Rhizobium radiobacter","Roseomonas gilardii","Shewanella algae","Shewanella putrefaciens","Sphingobacterium multivorum","Sphingobacterium spiritivorum","Sphingobacterium thalpophilum","Sphingomonas paucimobilis","Stenotrophomonas maltophilia","Vibrio alginolyticus","Vibrio cholerae","Vibrio fluvialis","Vibrio metschnikovii","Vibrio mimicus","Vibrio parahaemolyticus","Vibrio vulnificus","Actinobacillus ureae","Aggregatibacter actinomycetemcomitans","Aggregatibacter aphrophilus","Aggregatibacter segnis","Campylobacter coli","Campylobacter fetus ssp. fetus","Campylobacter jejuni ssp. jejuni","Capnocytophaga spp.","Cardiobacterium hominis","Eikenella corrodens","Gardnerella vaginalis","Haemophilus haemolyticus","Haemophilus influenzae","Haemophilus parahaemolyticus","Haemophilus parainfluenzae","Kingella denitrificans","Kingella kingae","Moraxella (Branhamella) catarrhalis","Neisseria cinerea","Neisseria elongata","Neisseria gonorrhoeae","Neisseria lactamica","Neisseria meningitidis","Neisseria sicca","Oligella urethralis","Suttonella indologenes",
    ];
    function split( val ) {
        return val.split( /,\s*/ );
    }
    function extractLast( term ) {
        return split( term ).pop();
    }
 
    $( ".hasil" )
    .on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
        event.preventDefault();
        }
    })
    .autocomplete({
        minLength: 0,
        source: function( request, response ) {
            response( $.ui.autocomplete.filter(
                availableTags, extractLast( request.term ) ) );
        },
        focus: function() {
            return false;
        },
        select: function( event, ui ) {
            var terms = split( this.value );
            terms.pop();
            terms.push( ui.item.value );
            terms.push( "" );
            this.value = terms.join( ", " );
            return false;
        }
    });
} );
</script>
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});
$('.selectpicker').selectpicker({
  style: 'btn-info',
  size: 2
});
</script>
@endsection

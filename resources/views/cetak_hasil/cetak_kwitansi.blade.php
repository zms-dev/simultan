<title>Cetak Kwitansi</title>
<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
    padding: 50px
}
table, td {    
    text-align: left;
}

.inti,.inti td, .inti th {    
    border: 1px solid #333;
    text-align: left;
    border-collapse: collapse;
}
.inti td, .inti th{
    padding: 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}
</style>
<body style="padding-bottom: 100px;padding-left: 0;padding-right: 0;padding-top: 0;">
<table width="100%" cellpadding="0" border="0">
    <thead>
        <tr>
            <th>
                <img alt="" src="{{URL::asset('asset/img/kimkes2.png')}}" height="120px">
            </th>
            <th align="center" width="100%">
                <span style="font-size: 32px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                <span style="font-size: 20px">DIREKTORAT JENDERAL PELAYANAN KESEHATAN <br>BALAI BESAR LABORATORIUM KESEHATAN SURABAYA</span>
                <span><br/>Jalan Karangmenjangan No, 18 Surabaya - 60286<br>Telepon Pelayanan : (031) 5021451 ; Faksimili : (031) 5020388<br>Website : bblksurabaya.id ; Surat elektronik : bblksub@yahoo.co.id</span>
            </th>
        </tr>
        <tr>
            <th colspan="2"><hr></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="100%">
                <table width="100%">
                    <tr>
                        <th width="45%">Nomor</th>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <td><b>Pendaftaran :</b>&nbsp;
                            @if($tahun == '2018')
                                {{substr($data->kode_lebpes,0, -9)}}PME/{{substr($data->kode_lebpes, 9)}}
                            @else 
                                {{$data->no_urut}}/PME/{{substr($data->kode_lebpes, 9)}}
                            @endif
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <table style="margin-top: 100px">
                    <tr>
                        <td>Telah diterima dari</td>
                        <td> : </td>
                        <th>Direktur / Kepala {{$data->nama_lab}}</th>
                    </tr>
                    <tr>
                        <td>Untuk Pembayaran</td>
                        <td> : </td>
                        <th> Keikutsertaan Program Nasional Pemantapan Mutu Eksternal</th>
                    </tr>
                    <tr>
                        <td>Dengan Jumlah</td>
                        <td> : </td>
                        <th>Rp. {{number_format($data->jumlah_tarif)}},-</th>
                    </tr>
                    <tr>
                        <td>Terbilang</td>
                        <td> : </td>
                        <th>{{ucwords(numbToWords($data->jumlah_tarif))}} Rupiah</th>
                    </tr>
                </table>
                <table width="100%" style="margin-top: 50px">
                    <tr>
                        <td></td>
                        <td>Surabaya, {{ _dateIndo($tanggal->tanggal) }}</td>
                    </tr>
                    <tr>
                        <td width="50%"></td>
                        <td>Bendahara Penerimaan</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Balai Besar Laboratorium Kesehatan Surabaya</td>
                    </tr>
                    <tr>
                        <td height="100px"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Sutiani Rahayu, SE</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>NIP 197903052007012023</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr nobr="true">
            <td colspan="100%">
                <div style="page-break-after: always;"></div>
                <table width="100%">
                    <tr>
                        <th width="45%">Nomor</th>
                        <td></td>
                        <th></th>
                    </tr>
                    <tr>
                        <td><b>Pendaftaran :</b>&nbsp;
                            @if($tahun == '2018')
                                {{substr($data->kode_lebpes,0, -9)}}PME/{{substr($data->kode_lebpes, 9)}}
                            @else 
                                {{$data->no_urut}}/PME/{{substr($data->kode_lebpes, 9)}}
                            @endif
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <table style="margin-top: 100px;">
                    <tr>
                        <td>Kepada</td>
                        <td> :</td>
                        <th>{{$data->nama_lab}}</th>
                    </tr>
                    <tr>
                        <td>Perihal</td>
                        <td> :</td>
                        <th>Keikutsertaan Program Nasional Pemantapan Mutu Eksternal</th>
                    </tr>
                </table>
                <table class="inti" style="margin-top: 50px">
                    <thead>
                        <tr nobr="true">
                            <th><center>No</center></th>
                            <th><center>Nama Instansi</center></th>
                            <th><center>Bidang</center></th>
                            <th><center>Siklus</center></th>
                            <th><center>Harga (Rp)</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 0; ?>
                        @foreach($d as $key => $val)
                        <?php $no++; ?>
                        <tr nobr="true" valign="top">
                            @if($key == 0)
                            <td rowspan="{{count($d)}}">1</td>
                            <td style="border-bottom: none;">{!!$data->nama_lab!!}</td>
                            @else
                            <td style="border: none;"></td>
                            @endif
                            <td>
                                {{$val[0]}}
                            </td>
                            <td style="text-align: center;">
                                @if($val[2] == 12)
                                    1 & 2
                                @else
                                    {{$val[2]}}
                                @endif
                            </td>
                            <td style="text-align: right;">
                                {{number_format($val[1])}}
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="4" style="text-align: right;">Total (Rp) :</td>
                            <td style="text-align: right;">{{number_format($data->jumlah_tarif)}}</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
</body>
<!-- <div class="header">
<table>
    <tr nobr="true">
        <td>
            <img alt="" src="http://webdesign.zamasco.com/img/logo-kemenkes.png" height="120px">
        </td>
        <td style="text-align: center;">
            <font style="font-size: 32px"><b>KEMENTERIAN KESEHATAN RI</b></font><br>
            <font style="font-size: 20px">DIREKTORAT JENDERAL PELAYANAN KESEHATAN <br>BALAI BESAR LABORATORIUM KESEHATAN SURABAYA</font>
            <font>Jalan Karangmenjangan No, 18 Surabaya - 60286<br>Telepon Pelayanan : (031) 5021451 ; Faksimili : (031) 5020388<br>Website : bblksurabaya.id ; Surat elektronik : bblksub@yahoo.co.id</font>
        </td>
    </tr>
</table>
<hr>
</div> -->
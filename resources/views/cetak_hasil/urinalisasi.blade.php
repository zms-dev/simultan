<style type="text/css">
@page {
margin: 140px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 5px;
    text-align: left;
    border: 1px solid #333;
}

#header {
    position: fixed;
    border-bottom:1px solid gray;
    padding-top: -130px;
}
#footer {
    position: fixed;
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
</style>
<div id="header">
    <table width="100%" cellpadding="0" border="0" >
        <thead>
            <tr>
                <th style="vertical-align: top; padding-top: -5px">
                    <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="60px">
                </th>
                <td width="100%" style="padding-left: 20px">
                    <span style="font-size: 12px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 12px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL URINALISA SIKLUS {{$siklus}} TAHUN {{$date}}</b></span><br>
                    <span style="font-size: 10px">Penyelenggara : <b>Balai Besar Laboratorium Kesehatan Surabaya</b></span>
                    <div style="padding-left: 75px; margin-top: -13px; font-size: 10px">
                        <span><br/>Jalan Karangmenjangan No, 18 Surabaya - 60286<br>Telepon : (031) 5021451 | Fax : (031) 5020388<br>Web Online : simultan.bblksurabaya.id <br>Email : pme.bblksub@gmail.com</span>
                    </div>
                </td>
                <th style="vertical-align: top;">
                    <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" height="60px">
                </th>
            </tr>
            <tr>
                <th colspan="3">&nbsp;</th>
            </tr>
            <tr>
                <th colspan="3"><hr></th>
            </tr>
        </thead>
    </table>
</div>
<div id="footer">
    PNPME URINALISA {{$datas->kode_bahan}} TAHUN {{$date}}
</div>
<label>
    <center>
        <font size="10px">
            FORMULIR HASIL PEMERIKSAAN PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL URINALISA <br>SIKLUS {{$siklus}}  TAHUN {{$date}}
        </font>
    </center>
</label><br>
<table>
    <tr>
        <th>Kode Peserta </th>
        <td>: </td>
        <td>{{ $datas->kode_lab }} &nbsp;</td>
        <th>Kode Bahan </th>
        <td>: </td>
        <td>{{ $datas->kode_bahan }}</td>
    </tr>
    <tr>
        <th>Tanggal Penerimaan </th>
        <td>: </td>
        <td>{{ $datas->tgl_penerimaan }}</td>
        <th>Kualitas Bahan </th>
        <td>: </td>
        <td><font style="text-transform: capitalize;">{{$datas->kualitas_bahan}}</font></td>
    </tr>
    <tr>
        <th>Tanggal Pemeriksaan </th>
        <td>: </td>
        <td>{{ $datas->tgl_pemeriksaan }}</td>
        <th>Pendidikan Pelaksana PME </th>
        <td>: </td>
        <td>@if($datas->tingkat == 'Lain - lain'){{ $datas->pendidikan_lain }}@else {{$datas->tingkat}} @endif</td>
    </tr>
    <!-- <tr>
        <th>Pelaksanaan PMI </th>
        <td>: </td>
        <td>{{ $datas->pelaksanaan_pmi }}</td>
    </tr> -->
</table>

<table  class="table utama" style="margin-top: 5px" >
    <thead>
        <tr>
            <th width="5%">No</th>
            <th><center>Parameter</center></th>
            <th><center>Alat</center></th>
            <th><center>Reagen/Kit Carik Celup</center></th>
            <th><center>Metode Pemeriksaan</center></th>
            <th><center>Hasil</center></th>
        </tr>
    </thead>
    <tbody>
        @if(count($data))
        <?php $no = 0; ?>
        @foreach($data as $val)
        <?php $no++; ?>
        <tr>
            <td>{{$no}}</td>
            <td>{!!$val->nama_parameter!!}</td>
            <td>{{ $val->Alat }}</td>
            <td>@if(count($val->reagen)){{ $val->reagen }} {{ $val->reagen_lain }}@else{{ $val->Reagen }}@endif</td>
            <td>@if(count($val->metode_pemeriksaan)){!! $val->metode_pemeriksaan !!} {{ $val->metode_lain }}@else{{ $val->Kode }}@endif</td>
            @if($siklus == 1)
                @if($val->id == '9' || $val->id == '10')
                <td>
                    @if(count($val->hp))
                        {{ $val->hp }}
                    @else
                        {{ $val->Hasil }}
                    @endif
                </td>
                @else
                <td>@if(count($val->hp)){{ $val->hp}}@else{{ $val->Hasil}}@endif</td>
                @endif
            @else
                <td>@if(count($val->hp)){{ $val->hp}}@else{{ $val->Hasil}}@endif</td>
            @endif
        </tr>
        @endforeach
        @endif

    </tbody>
    <tfoot>
        <tr>
            <td colspan="4"><label>Catatan :</label></td>
            <td colspan="2"><label>Nama Penanggung Jawab :</label></td>
        </tr>
        <tr>
            <td colspan="4">
                {{$datas->catatan}}
            </td>
            <td colspan="2">
                {{$datas->penanggung_jawab}}
            </td>
        </tr>
    </tfoot>
</table>
<table width="100%">
    @if($tanggalkirim != NULL)
    <tr>
        <td width="70%">&nbsp;</td>
        <td>Hasil dikirim tanggal </td>
        <td>: {{$tanggalkirim->tanggal_kirim}}</td>
    </tr>
    @endif
    <tr>
        <td width="70%">&nbsp;</td>
        <td>Dicetak tanggal </td>
        <td>: {{date("Y-m-d")}}</td>
    </tr>
</table>
<br><br>
<table width="100%" border="0">
    <tr>
        <td width="70%">Mengetahui,</td>
        <td>.......................... , ...................</td>
    </tr>
    <tr>
        <td>Pimpinan Laboratorium</td>
        <td></td>
    </tr>
    <tr>
        <td>{{$perusahaan}}</td>
        <td>Penanggungjawab Pemeriksaan</td>
    </tr>
    <tr>
        <td height="50px"></td>
        <td></td>
    </tr>
    <tr>
        <td>Nama ...................................</td>
        <td>Nama ...................................</td>
    </tr>
    <tr>
        <td>NIP .......................................</td>
        <td>NIP .......................................</td>
    </tr>
</table>

<script type="text/php">
  if (isset($pdf)) {
    $font = $fontMetrics->getFont("Arial", "bold");
    $pdf->page_text(515, 783, "Hal. {PAGE_NUM} dari {PAGE_COUNT}", $font, 8, array(0, 0, 0));
  }
</script>
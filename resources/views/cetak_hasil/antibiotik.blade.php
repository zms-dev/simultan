<style type="text/css">
@page {
margin: 100px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama, table.utama td, table.utama th {    
    border: 1px solid #333;
    text-align: left;
}

table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td {
    padding: 5px;
}

#header { 
    position: fixed; 
    border-bottom:1px solid gray;
    padding-top: -50px;
}
</style>
<div id="header">
    <center><label> FORMULIR HASIL PEMERIKSAAN
                    PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL IDENTIFIKASI BAKTERI DAN UJI KEPEKAAN ANTIBIOTIK
                    SIKLUS {{$siklus}} LEMBAR {{$lembar}} TAHUN {{$date}}
    </label></center><br>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                @foreach($data as $val)                    
                    <table>
                        <tr>
                            <td>Kode Peserta </td>
                            <td>:</td>
                            <td>{{$val->kode_lab}}</td>
                        </tr>
                        <tr>
                            <td>Nama Instansi </td>
                            <td>:</td>
                            <td>{{$val->nama_instansi}}</td>
                        </tr>
                        <tr>
                            <td>Alamat </td>
                            <td>:</td>
                            <td>{{$val->alamat}}</td>
                        </tr>
                        <tr>
                            <td>Nama Pemeriksa </td>
                            <td>:</td>
                            <td>{{$val->nama_pemeriksa}}</td>
                        </tr>
                        <tr>
                            <td>Nomor HP Pemeriksa </td>
                            <td>:</td>
                            <td>{{$val->no_hp}}</td>
                        </tr>
                        <tr>
                            <td>Pendidikan Pelaksana PME </td>
                            <td>:</td>
                            <td>@if($val->tingkat == 'Lain - lain'){{ $val->pendidikan_lain }}@else {{$val->tingkat}} @endif</td>
                        </tr>
                    </table>
                    <p> 
                    1. Masukan hasil mikroskopis dan identifikasi untuk spesimen pada tempat yang telah disediakan dibawah ini. Hasil identifikasi ditulis pada lembar hasil yang sudah disediakan <br>
                    2. Tuliskan jumlah item dan catatan bila ada kesalahan penulisan
                    </p><br>
                    <table class="utama">
                        <tr>
                            <th>Kode Bahan :</th>
                            <td>{{substr($val->kode_lab,0, -5)}}{{$data[0]->kd_bahan}}/{{$siklus}}{{substr($val->kode_lab, 11)}}</td>
                            <th>Jenis Bahan :</th>
                            <td>{{$val->jenis_bahan}}</td>
                        </tr>
                        <tr>
                            <th>Siklus :</th>
                            <td>{{$val->siklus}}</td>
                            <th>Tanggal Pelaksanaan :</th>
                            <td>
                                {{$val->tgl_pelaksanaan}}
                            </td>
                        </tr>
                    </table>
                    <table class="utama">
                        <tr>
                            <th colspan="2"><center>Metode Identifikasi Bakteri</center></th>
                        </tr>
                        <tr>
                            <td>
                                <p>Konvensional : {{$val->metode_konvensional}}</p>
                                <p>Otomatis (sebutkan) : {{$val->metode_otomatis}}</p>
                                <p>Lainnya (sebutkan) : {{$val->metode_lainnya}}</p>
                            </td>
                            <td>
                                {{$val->test_metode_identifikasi}}
                            </td>
                        </tr>
                    </table>
                    <table class="utama">
                        <tr>
                            <th><center>Media yang digunakan</center></th>
                            <th><center>Buatan Sendiri</center></th>
                            <th><center>Media Komersial (Sebutkan)</center></th>
                        </tr>
                        <tr>
                            <td>
                                {{$val->media_digunakan}}
                            </td>
                            <td>
                                @if($val->buatan_sendiri == "v")
                                V
                                @endif
                            </td>
                            <td>
                                {{$val->media_komersial}}
                            </td>
                        </tr>
                    </table>
                    <table class="utama">
                        <tr>
                            <th width="30%"><center>Hasil Kultur</center></th>
                            <th><center>Pilih Salah satu</center></th>
                        </tr>
                        <tr>
                            <td>Tidak terdapat pertumbuhan bakteri patogen</td>
                            <td>
                                <input type="radio" name="hasil_kultur" value="tidak" {{ ($val->hasil_kultur == 'tidak') ? 'checked' : '' }}>
                            </td>
                        </tr>
                        <tr>
                            <td>Terdapat pertumbuhan bakteri patogen</td>
                            <td>
                                <input type="radio" name="hasil_kultur" value="ada" {{ ($val->hasil_kultur == 'ada') ? 'checked' : '' }}>
                            </td>
                        </tr>
                        <tr>
                            <td>Spesies <br><small>(Tanda "," untuk memisahkan)</small></td>
                            <td>
                                {!!$val->spesies_kultur!!}
                            </td>
                        </tr>
                    </table>

                    <p style="font-weight: bold;">Pewarnaan</p>
                    <table class="utama">
                        <tr>
                            <th rowspan="2">Pewarnaan Gram</th>
                            <th>Gram Positif</th>
                            <th>Gram Negatif</th>
                            <th>Yeast</th>
                        </tr>
                        <tr>
                            <td>{{$val->pewarnaan_gram_p}}</td>
                            <td>{{$val->pewarnaan_gram_n}}</td>
                            <td>{{$val->pewarnaan_gram_y}}</td>
                        </tr>
                    </table>

                    <p style="page-break-before: always; font-weight: bold;">Uji Biokimia Konvensional Untuk Golongan Gram Negatif Batang</p>
                    <table class="utama">
                        <tr>
                            <th>Kebutuhan Oksidasi</th>
                            <th colspan="5">{{$val->kebutuhan_oksidasi}}</th>
                        </tr>
                        <tr>
                            <th colspan="6">Fermentasi Karbohidrat dan uji biokimia lainnya (pilih salah satu)</th>
                        </tr>
                        <tr>
                        <?php $i = 1;?>
                        @foreach($val->fermentasinegatif as $fer)
                            <td>{{$fer->fermentasi}}</td>
                            <td colspan="2">
                                {{$fer->status}}
                            </td>
                        <?php 
                          if($i == 2){
                            $i =1;
                            echo "</tr><tr>";
                          }else{
                            $i++;
                          }
                        ?>
                        @endforeach
                            <td>Lain-lain</td>
                            <td colspan="2">{{$val->fermentasilainn}}</td>
                        </tr>
                    </table>

                    <p style="font-weight: bold;">Uji Biokimia Konvensional Untuk Golongan Gram Positif Coccus</p>
                    <table class="utama">
                        <tr>
                            <th colspan="2">Hemolisa</th>
                            <th colspan="4">{{$val->hemolisa}}</th>
                        </tr>
                        <tr>
                            <th colspan="2">Kebutuhan terhadap faktor X + V</th>
                            <th colspan="4">{{$val->faktorxv}}</th>
                        </tr>
                        <tr>
                            <th colspan="2">Kebutuhan Oksigen</th>
                            <th colspan="4">{{$val->kebutuhan_oksigen}}</th>
                        </tr>
                        <tr>
                            <th colspan="6">Fermentasi Karbohidrat dan uji biokimia lainnya (pilih salah satu)</th>
                        </tr>
                        <tr>
                        <?php $i = 1;?>
                        @foreach($val->fermentasipositif as $fer)
                            <td>{{$fer->fermentasi}}<input type="hidden" name="id_fermentasi_positif[]" value="{{$fer->id}}"></td>
                            <td colspan="2">
                                {{$fer->status}}
                            </td>
                        <?php 
                          if($i == 2){
                            $i =1;
                            echo "</tr><tr>";
                          }else{
                            $i++;
                          }
                        ?>
                        @endforeach
                            <td>Lain-lain</td>
                            <td colspan="2">{{$val->fermentasilainp}}</td>
                        </tr>
                    </table>

                    <p style="page-break-before: always; font-weight: bold;">Uji Biokimia Automatic</p>
                    <table class="utama">
                        <tr>
                            <th>Vitek 2</th>
                            <th>{{$val->vitek}}</th>
                        </tr>
                        <tr>
                            <th>API</th>
                            <th>{{$val->api}}</th>
                        </tr>
                        <tr>
                            <th>API</th>
                            <th>{{$val->api2}}</th>
                        </tr>
                        <tr>
                            <th>BD Phoenik</th>
                            <th>{{$val->bd_phoenik}}</th>
                        </tr>
                        <tr>
                            <th>Lain - lain</th>
                            <th>{{$val->data_lain}}</th>
                        </tr>
                        <tr>
                            <td>Spesies <br><small>(Tanda "," untuk memisahkan)</small></td>
                            <td>
                                {!!$val->spesies_auto!!}
                            </td>
                        </tr>
                    </table>

                    <center><h2>UJI KEPEKAAN ANTIBIOTIK</h2></center>

                    <table class="utama">
                        <tr>
                            <th>Hasil Identifikasi</th>
                            <td>{{$peka->hasil_identifikasi}}</td>
                            <th><center>Metode/automatisasi</center></th>
                        </tr>
                        <tr>
                            <th>Standart</th>
                            <td>{{$peka->standart}}</td>
                            <td>{{$peka->metode}}</td>
                        </tr>
                    </table>
                    <table class="utama">
                        <tr>
                            <th>Jenis Antibiotik</th>
                            <th>Disk difusi</th>
                            <th>Interpretasi hasil</th>
                            <th>M.I.C</th>
                            <th>Interpretasi hasil</th>
                            <th>Kesimpulan</th>
                        </tr>
                        @foreach($val->kepekaan as $peka)
                        <tr>
                            <td>{{$peka->antibiotik}} @if($peka->lain_lain != ''): {{$peka->lain_lain}}@endif</td>
                            <td>{{$peka->disk}}</td>
                            <td>{{$peka->hasil1}}</td>
                            <td>{{$peka->mic}}</td>
                            <td>{{$peka->hasil2}}</td>
                            <td>{{$peka->kesimpulan}}</td>
                        </tr>
                        @endforeach
                    </table>
                    <table class="utama">
                        @if($val->resistensi == NULL || $val->resistensi == '')
                        <tr>
                            <th>Golongan MRSA</th>
                            <td>{{$val->mrsa}}</td>
                        </tr>
                        <tr>
                            <th>Golongan ESBL</th>
                            <td>{{$val->esbl}}</td>
                        </tr>
                        @else
                        <tr>
                            <th>Resistensi</th>
                            <td>
                                @if($val->resistensi_lain != '')
                                    {{$val->resistensi_lain}}
                                @else
                                    {{$val->name_resistensi}}
                                @endif
                            </td>
                        </tr>
                        @endif
                    </table>
                @endforeach
                    <br><br>
                    <table width="100%" border="0">
                        <tr>
                            <td width="70%">Mengetahui,</td>
                            <td>.......................... , ...................</td>
                        </tr>
                        <tr>
                            <td>Pimpinan Laboratorium</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>{{$perusahaan}}</td>
                            <td>Penanggungjawab Pemeriksaan</td>
                        </tr>
                        <tr>
                            <td height="50px"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Nama ...................................</td>
                            <td>Nama ...................................</td>
                        </tr>
                        <tr>
                            <td>NIP .......................................</td>
                            <td>NIP .......................................</td>
                        </tr>
                    </table>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
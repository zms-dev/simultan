<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 14px;
}
table, td, th {    
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 15px;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><label>FORMULIR HASIL PEMERIKSAAN PENYELENGGARAAN NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI TAHUN 2017 <br> HASIL PEMERIKSAAN ANTI HIV</label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Kode Peserta : {{ $perusahaan }}</label>
                    </div><br>
                    <label>2. BAHAN KONTROL :</label>
                    <table class="table-bordered table">
                            @foreach($datahiv as $val)
                            <tr>
                                <td>Diterima tanggal :</td>
                                <td>{{$val->tgl_diterima}}</td>
                                <td>Diperiksa tanggal :</td>
                                <td>{{$val->tgl_diperiksa}}</td>
                            </tr>
                            @break
                            @endforeach
                    </table>

                    <table class="table-bordered table">
                            <tr>
                                <td rowspan="4" width="20%"> Kondisi bahan kontrol saat diterima</td>
                                <td>NO Tabung</td>
                                <td>Baik/Jernih *)</td>
                                <td>Keruh *)</td>
                                <td>Lain-lain</td>
                            </tr>
                            @foreach($datahiv as $val)
                            <tr>
                                <td>{{$val->no_tabung}}</td>
                                <td><input type="checkbox" name="jenis[]" value="baik/jernih" {{ ($val->jenis == 'baik/jernih') ? 'checked' : '' }}></td>
                                <td><input type="checkbox" name="jenis[]" value="keruh" {{ ($val->jenis == 'keruh') ? 'checked' : '' }}></td>
                                <td><input type="checkbox" name="jenis[]" value="lain-lain" {{ ($val->jenis == 'lain-lain') ? 'checked' : '' }}></td>
                            </tr>
                            @endforeach
                    </table>
                    <label>3. REAGEN</label>
                    <table class="table table-bordered">
                        <tr>
                            <td>Keterangan</td>
                            <td>Tes I</td>
                            <td>Tes II</td>
                            <td>Tes III</td>
                        </tr>
                        <tr>
                            <td>Metode Pemeriksaan *)</td>
                            @foreach($datahiv1 as $reagen)
                            <td><input type="checkbox" name="metode[]" value="rapid" {{ ($reagen->metode == 'rapid') ? 'checked' : '' }}> Rapid <input type="checkbox" name="metode[]" value="eia" {{ ($reagen->metode == 'eia') ? 'checked' : '' }}> EIA </td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nama Reagen</td>
                            @foreach($datahiv1 as $reagen)
                            <td>{{$reagen->nama_reagen}}</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nama Produsen</td>
                            @foreach($datahiv1 as $reagen)
                            <td>{{$reagen->nama_produsen}}</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nomor Lot/Batch</td>
                            @foreach($datahiv1 as $reagen)
                            <td>{{$reagen->nomor_lot}}</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Tanggal Kadaluarsa</td>
                            @foreach($datahiv1 as $reagen)
                            <td>{{$reagen->tgl_kadaluarsa}}</td>
                            @endforeach
                        </tr>
                    </table>
                    <label>4. HASIL PEMERIKSAAN *)</label>
                    <table class="table table-bordered">
                        <tr>
                            <td>TABUNG I</td>
                            @foreach($datahiv2 as $hasil)
                            @if($hasil->tabung == '1')
                            <td colspan="4">Kode Bahan Kontrol : {{$hasil->kode_bahan_kontrol}}</td>
                            @break
                            @endif
                            @endforeach
                        </tr>
                        <tr>
                            <th>Tes</th>
                            <th>Abs atau OD (A) (Bila dengan  EIA)</th>
                            <th>Cut Off (B) (Bila dengan EIA)</th>
                            <th>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA)</th>
                            <th>Interpretasi hasil *)</th>
                        </tr>
                        <?php  
                            $no = 0;
                            $val = ['I','II','III','IV'];
                        ?>
                        @foreach($datahiv2 as $hasil)
                        @if($hasil->tabung == '1')
                        <tr>
                            <td>{{$val[$no]}}</td>
                            <td>{{$hasil->abs_od}}</td>
                            <td>{{$hasil->cut_off}}</td>
                            <td>{{$hasil->sco}}</td>
                            <td><input type="checkbox" name="interpretasi1[]" value="reaktif" {{ ($hasil->interpretasi == 'reaktif') ? 'checked' : '' }}> Reaktif <br><input type="checkbox" name="interpretasi1[]" value="non-reaktif" {{ ($hasil->interpretasi == 'non-reaktif') ? 'checked' : '' }}> Non Reaktif</td>
                        </tr>
                        <?php $no++; ?>
                        @endif
                        @endforeach
                    </table>

                    <table class="table table-bordered">
                        <tr>
                            <td>TABUNG II</td>
                            @foreach($datahiv2 as $hasil)
                            @if($hasil->tabung == '2')
                            <td colspan="4">Kode Bahan Kontrol : {{$hasil->kode_bahan_kontrol}}</td>
                            @break
                            @endif
                            @endforeach
                        </tr>
                        <tr>
                            <th>Tes</th>
                            <th>Abs atau OD (A) (Bila dengan  EIA)</th>
                            <th>Cut Off (B) (Bila dengan EIA)</th>
                            <th>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA)</th>
                            <th>Interpretasi hasil *)</th>
                        </tr>
                        <?php  
                            $no = 0;
                            $val = ['I','II','III','IV'];
                        ?>
                        @foreach($datahiv2 as $hasil)
                        @if($hasil->tabung == '2')
                        <tr>
                            <td>{{$val[$no]}}</td>
                            <td>{{$hasil->abs_od}}</td>
                            <td>{{$hasil->cut_off}}</td>
                            <td>{{$hasil->sco}}</td>
                            <td><input type="checkbox" name="interpretasi1[]" value="reaktif" {{ ($hasil->interpretasi == 'reaktif') ? 'checked' : '' }}> Reaktif <br><input type="checkbox" name="interpretasi1[]" value="non-reaktif" {{ ($hasil->interpretasi == 'non-reaktif') ? 'checked' : '' }}> Non Reaktif</td>
                        </tr>
                        <?php $no++; ?>
                        @endif
                        @endforeach
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <td>TABUNG III</td>
                            @foreach($datahiv2 as $hasil)
                            @if($hasil->tabung == '3')
                            <td colspan="4">Kode Bahan Kontrol : {{$hasil->kode_bahan_kontrol}}</td>
                            @break
                            @endif
                            @endforeach
                        </tr>
                        <tr>
                            <th>Tes</th>
                            <th>Abs atau OD (A) (Bila dengan  EIA)</th>
                            <th>Cut Off (B) (Bila dengan EIA)</th>
                            <th>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA)</th>
                            <th>Interpretasi hasil *)</th>
                        </tr>
                        <?php  
                            $no = 0;
                            $val = ['I','II','III','IV'];
                        ?>
                        @foreach($datahiv2 as $hasil)
                        @if($hasil->tabung == '3')
                        <tr>
                            <td>{{$val[$no]}}</td>
                            <td>{{$hasil->abs_od}}</td>
                            <td>{{$hasil->cut_off}}</td>
                            <td>{{$hasil->sco}}</td>
                            <td><input type="checkbox" name="interpretasi1[]" value="reaktif" {{ ($hasil->interpretasi == 'reaktif') ? 'checked' : '' }}> Reaktif <br><input type="checkbox" name="interpretasi1[]" value="non-reaktif" {{ ($hasil->interpretasi == 'non-reaktif') ? 'checked' : '' }}> Non Reaktif</td>
                        </tr>
                        <?php $no++; ?>
                        @endif
                        @endforeach
                    </table>
                    <div class="col-sm-6">
                    @foreach($datahiv as $val)
                        <p>Apakah Saudara mengeluarkan hasil pemeriksaan <input type="radio" name="hasil_pemeriksaan[]" value="ya" {{ ($val->hasil_pemeriksaan == 'ya') ? 'checked' : '' }}> Ya <input type="radio" name="hasil_pemeriksaan[]" value="tidak" {{ ($val->hasil_pemeriksaan == 'tidak') ? 'checked' : '' }}> Tidak</p>
                    </div>
                    <div class="col-sm-6">
                        <label>Petugas yang melakukan pemeriksaan :</label>
                        {{$val->petugas_pemeriksaan}}
                    @break
                    @endforeach
                    </div><br>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><label>FORMULIR HASIL PEMERIKSAAN PENYELENGGARAAN NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI TAHUN 2017 <br> HASIL PEMERIKSAAN Syphilis</label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Kode Peserta : {{ $perusahaan }}</label>
                    </div><br>
                    <label>2. BAHAN KONTROL :</label>
                    <table class="table-bordered table">
                            @foreach($datatp as $val)
                            <tr>
                                <td>Diterima tanggal :</td>
                                <td>{{$val->tgl_diterima}}</td>
                                <td>Diperiksa tanggal :</td>
                                <td>{{$val->tgl_diperiksa}}</td>
                            </tr>
                            @break
                            @endforeach
                    </table>

                    <table class="table-bordered table">
                            <tr>
                                <td rowspan="4" width="20%"> Kondisi bahan kontrol saat diterima</td>
                                <td>NO Tabung</td>
                                <td>Baik/Jernih *)</td>
                                <td>Keruh *)</td>
                                <td>Lain-lain</td>
                            </tr>
                            @foreach($datatp as $val)
                            <tr>
                                <td>{{$val->no_tabung}}</td>
                                <td><input type="checkbox" name="jenis[]" value="baik/jernih" {{ ($val->jenis == 'baik/jernih') ? 'checked' : '' }}></td>
                                <td><input type="checkbox" name="jenis[]" value="keruh" {{ ($val->jenis == 'keruh') ? 'checked' : '' }}></td>
                                <td><input type="checkbox" name="jenis[]" value="lain-lain" {{ ($val->jenis == 'lain-lain') ? 'checked' : '' }}></td>
                            </tr>
                            @endforeach
                    </table>
                    <label>3. REAGEN</label>
                    <table class="table table-bordered">
                        <tr>
                            <td>Metode Pemeriksaan *)</td>
                            @foreach($datatp1 as $reagen)
                            <td><input type="checkbox" name="metode[]" value="rapid" {{ ($reagen->metode == 'rapid') ? 'checked' : '' }}> Rapid <input type="checkbox" name="metode[]" value="eia" {{ ($reagen->metode == 'eia') ? 'checked' : '' }}> EIA </td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nama Reagen</td>
                            @foreach($datatp1 as $reagen)
                            <td>{{$reagen->nama_reagen}}</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nama Produsen</td>
                            @foreach($datatp1 as $reagen)
                            <td>{{$reagen->nama_produsen}}</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nomor Lot/Batch</td>
                            @foreach($datatp1 as $reagen)
                            <td>{{$reagen->nomor_lot}}</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Tanggal Kadaluarsa</td>
                            @foreach($datatp1 as $reagen)
                            <td>{{$reagen->tgl_kadaluarsa}}</td>
                            @endforeach
                        </tr>
                    </table>
                    <label>4. HASIL PEMERIKSAAN *)</label>
                    <table class="table table-bordered">
                        <tr>
                            <th>Nomor Tabung</th>
                            <th>Kode Bahan Kontrol</th>
                            <th>Abs atau OD (A) (Bila dengan  EIA)</th>
                            <th>Cut Off (B) (Bila dengan EIA)</th>
                            <th>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA)</th>
                            <th>Interpretasi hasil *)</th>
                        </tr>
                        <?php  
                            $no = 0;
                            $val = ['I','II','III','IV'];
                        ?>
                        @foreach($datatp2 as $hasil)
                        @if($hasil->tabung == '1')
                        <tr>
                            <td>{{$val[$no]}}</td>
                            <td>{{$hasil->kode_bahan_kontrol}}</td>
                            <td>{{$hasil->abs_od}}</td>
                            <td>{{$hasil->cut_off}}</td>
                            <td>{{$hasil->sco}}</td>
                            <td><input type="checkbox" name="interpretasi1[]" value="reaktif" {{ ($hasil->interpretasi == 'reaktif') ? 'checked' : '' }}> Reaktif <br><input type="checkbox" name="interpretasi1[]" value="non-reaktif" {{ ($hasil->interpretasi == 'non-reaktif') ? 'checked' : '' }}> Non Reaktif</td>
                        </tr>
                        <?php $no++; ?>
                        @endif
                        @endforeach
                    </table>
                    <div class="col-sm-12">
                    @foreach($datatp as $val)
                        <label>Petugas yang melakukan pemeriksaan :</label>
                        {{$val->petugas_pemeriksaan}}
                    @break
                    @endforeach
                    </div><br>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><label>FORMULIR HASIL PEMERIKSAAN PENYELENGGARAAN NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI TAHUN 2017 <br> HASIL PEMERIKSAAN HBsAg</label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Kode Peserta : {{ $perusahaan }}</label>
                    </div><br>
                    <label>2. BAHAN KONTROL :</label>
                    <table class="table-bordered table">
                            @foreach($datahbsag as $val)
                            <tr>
                                <td>Diterima tanggal :</td>
                                <td>{{$val->tgl_diterima}}</td>
                                <td>Diperiksa tanggal :</td>
                                <td>{{$val->tgl_diperiksa}}</td>
                            </tr>
                            @break
                            @endforeach
                    </table>

                    <table class="table-bordered table">
                            <tr>
                                <td rowspan="4" width="20%"> Kondisi bahan kontrol saat diterima</td>
                                <td>NO Tabung</td>
                                <td>Baik/Jernih *)</td>
                                <td>Keruh *)</td>
                                <td>Lain-lain</td>
                            </tr>
                            @foreach($datahbsag as $val)
                            <tr>
                                <td>{{$val->no_tabung}}</td>
                                <td><input type="checkbox" name="jenis[]" value="baik/jernih" {{ ($val->jenis == 'baik/jernih') ? 'checked' : '' }}></td>
                                <td><input type="checkbox" name="jenis[]" value="keruh" {{ ($val->jenis == 'keruh') ? 'checked' : '' }}></td>
                                <td><input type="checkbox" name="jenis[]" value="lain-lain" {{ ($val->jenis == 'lain-lain') ? 'checked' : '' }}></td>
                            </tr>
                            @endforeach
                    </table>
                    <label>3. REAGEN</label>
                    <table class="table table-bordered">
                        <tr>
                            <td>Metode Pemeriksaan *)</td>
                            @foreach($datahbsag1 as $reagen)
                            <td><input type="checkbox" name="metode[]" value="rapid" {{ ($reagen->metode == 'rapid') ? 'checked' : '' }}> Rapid <input type="checkbox" name="metode[]" value="eia" {{ ($reagen->metode == 'eia') ? 'checked' : '' }}> EIA </td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nama Reagen</td>
                            @foreach($datahbsag1 as $reagen)
                            <td>{{$reagen->nama_reagen}}</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nama Produsen</td>
                            @foreach($datahbsag1 as $reagen)
                            <td>{{$reagen->nama_produsen}}</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nomor Lot/Batch</td>
                            @foreach($datahbsag1 as $reagen)
                            <td>{{$reagen->nomor_lot}}</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Tanggal Kadaluarsa</td>
                            @foreach($datahbsag1 as $reagen)
                            <td>{{$reagen->tgl_kadaluarsa}}</td>
                            @endforeach
                        </tr>
                    </table>
                    <label>4. HASIL PEMERIKSAAN *)</label>
                    <table class="table table-bordered">
                        <tr>
                            <th>Nomor Tabung</th>
                            <th>Kode Bahan Kontrol</th>
                            <th>Abs atau OD (A) (Bila dengan  EIA)</th>
                            <th>Cut Off (B) (Bila dengan EIA)</th>
                            <th>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA)</th>
                            <th>Interpretasi hasil *)</th>
                        </tr>
                        <?php  
                            $no = 0;
                            $val = ['I','II','III','IV'];
                        ?>
                        @foreach($datahbsag2 as $hasil)
                        @if($hasil->tabung == '1')
                        <tr>
                            <td>{{$val[$no]}}</td>
                            <td>{{$hasil->kode_bahan_kontrol}}</td>
                            <td>{{$hasil->abs_od}}</td>
                            <td>{{$hasil->cut_off}}</td>
                            <td>{{$hasil->sco}}</td>
                            <td><input type="checkbox" name="interpretasi1[]" value="reaktif" {{ ($hasil->interpretasi == 'reaktif') ? 'checked' : '' }}> Reaktif <br><input type="checkbox" name="interpretasi1[]" value="non-reaktif" {{ ($hasil->interpretasi == 'non-reaktif') ? 'checked' : '' }}> Non Reaktif</td>
                        </tr>
                        <?php $no++; ?>
                        @endif
                        @endforeach
                    </table>
                    <div class="col-sm-12">
                    @foreach($datahbsag as $val)
                        <label>Petugas yang melakukan pemeriksaan :</label>
                        {{$val->petugas_pemeriksaan}}
                    @break
                    @endforeach
                    </div><br>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><label>FORMULIR HASIL PEMERIKSAAN PENYELENGGARAAN NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI TAHUN 2017 <br> HASIL PEMERIKSAAN ANTI-HCV</label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Kode Peserta : {{ $perusahaan }}</label>
                    </div><br>
                    <label>2. BAHAN KONTROL :</label>
                    <table class="table-bordered table">
                            @foreach($datahcv as $val)
                            <tr>
                                <td>Diterima tanggal :</td>
                                <td>{{$val->tgl_diterima}}</td>
                                <td>Diperiksa tanggal :</td>
                                <td>{{$val->tgl_diperiksa}}</td>
                            </tr>
                            @break
                            @endforeach
                    </table>

                    <table class="table-bordered table">
                            <tr>
                                <td rowspan="4" width="20%"> Kondisi bahan kontrol saat diterima</td>
                                <td>NO Tabung</td>
                                <td>Baik/Jernih *)</td>
                                <td>Keruh *)</td>
                                <td>Lain-lain</td>
                            </tr>
                            @foreach($datahcv as $val)
                            <tr>
                                <td>{{$val->no_tabung}}</td>
                                <td><input type="checkbox" name="jenis[]" value="baik/jernih" {{ ($val->jenis == 'baik/jernih') ? 'checked' : '' }}></td>
                                <td><input type="checkbox" name="jenis[]" value="keruh" {{ ($val->jenis == 'keruh') ? 'checked' : '' }}></td>
                                <td><input type="checkbox" name="jenis[]" value="lain-lain" {{ ($val->jenis == 'lain-lain') ? 'checked' : '' }}></td>
                            </tr>
                            @endforeach
                    </table>
                    <label>3. REAGEN</label>
                    <table class="table table-bordered">
                        <tr>
                            <td>Metode Pemeriksaan *)</td>
                            @foreach($datahcv1 as $reagen)
                            <td><input type="checkbox" name="metode[]" value="rapid" {{ ($reagen->metode == 'rapid') ? 'checked' : '' }}> Rapid <input type="checkbox" name="metode[]" value="eia" {{ ($reagen->metode == 'eia') ? 'checked' : '' }}> EIA </td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nama Reagen</td>
                            @foreach($datahcv1 as $reagen)
                            <td>{{$reagen->nama_reagen}}</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nama Produsen</td>
                            @foreach($datahcv1 as $reagen)
                            <td>{{$reagen->nama_produsen}}</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nomor Lot/Batch</td>
                            @foreach($datahcv1 as $reagen)
                            <td>{{$reagen->nomor_lot}}</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Tanggal Kadaluarsa</td>
                            @foreach($datahcv1 as $reagen)
                            <td>{{$reagen->tgl_kadaluarsa}}</td>
                            @endforeach
                        </tr>
                    </table>
                    <label>4. HASIL PEMERIKSAAN *)</label>
                    <table class="table table-bordered">
                        <tr>
                            <th>Nomor Tabung</th>
                            <th>Kode Bahan Kontrol</th>
                            <th>Abs atau OD (A) (Bila dengan  EIA)</th>
                            <th>Cut Off (B) (Bila dengan EIA)</th>
                            <th>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA)</th>
                            <th>Interpretasi hasil *)</th>
                        </tr>
                        <?php  
                            $no = 0;
                            $val = ['I','II','III','IV'];
                        ?>
                        @foreach($datahcv2 as $hasil)
                        @if($hasil->tabung == '1')
                        <tr>
                            <td>{{$val[$no]}}</td>
                            <td>{{$hasil->kode_bahan_kontrol}}</td>
                            <td>{{$hasil->abs_od}}</td>
                            <td>{{$hasil->cut_off}}</td>
                            <td>{{$hasil->sco}}</td>
                            <td><input type="checkbox" name="interpretasi1[]" value="reaktif" {{ ($hasil->interpretasi == 'reaktif') ? 'checked' : '' }}> Reaktif <br><input type="checkbox" name="interpretasi1[]" value="non-reaktif" {{ ($hasil->interpretasi == 'non-reaktif') ? 'checked' : '' }}> Non Reaktif</td>
                        </tr>
                        <?php $no++; ?>
                        @endif
                        @endforeach
                    </table>
                    <div class="col-sm-12">
                    @foreach($datahcv as $val)
                        <label>Petugas yang melakukan pemeriksaan :</label>
                        {{$val->petugas_pemeriksaan}}
                    @break
                    @endforeach
                    </div><br>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.print();
</script>
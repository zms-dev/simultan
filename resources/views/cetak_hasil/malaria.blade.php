<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama {
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td {
    padding: 5px;
    text-align: left;
    border: 1px solid #333;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body">
                <label><center>PROGRAM PEMANTAPAN MUTU EKSTERNAL MIKROSKOPIS MALARIA SIKLUS {{$siklus}} TAHUN {{$date}}</center></label><br>
                <table>
                    <tr>
                        <td>Kode Peserta </td>
                        <td>: </td>
                        <td>{{ $data->kode_peserta }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Penerimaan </td>
                        <td>: </td>
                        <td>{{ $data->tgl_penerimaan }}</td>
                    </tr>
                    <tr>
                        <td>Kualitas Bahan </td>
                        <td>: </td>
                        <td><font style="text-transform: capitalize;">{{$data->kondisi}}</font></td>
                    </tr>
                    <tr>
                        <td>Tanggal Pemeriksaan </td>
                        <td>: </td>
                        <td>{{ $data->tgl_pemeriksaan }}</td>
                    </tr>
                    <tr>
                        <td>Nama Pemeriksa </td>
                        <td>: </td>
                        <td>{{ $data->nama_pemeriksa }}</td>
                    </tr>
                    <tr>
                        <td>Nomor HP </td>
                        <td>: </td>
                        <td>{{ $data->nomor_hp }}</td>
                    </tr>
                    <tr>
                        <td>Pendidikan Pelaksana PME </td>
                        <td>: </td>
                        <td>@if($data->tingkat == 'Lain - lain'){{ $data->pendidikan_lain }}@else {{$data->tingkat}} @endif</td>
                    </tr>
                </table>

                <table class="utama">
                    <thead>
                        <tr>
                            <th>Kode Sediaan</th>
                            <th><center>Hasil Pemeriksaan</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($datas as $val)
                        <?php $no++; ?>
                        <tr>
                            <td>{!!$val->kode!!}</td>
                            <td>
                                @if($val->hasil == 'Positif')
                                {{ $val->hasil }}<br>{{str_replace(',',', ',$val->spesies)}}<br>{{$val->stadium}}<br>{{$val->parasit}} parasit/&#181;l darah
                                @else
                                {{ $val->hasil }}
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        
                    </tbody>
                </table>
                <table width="100%" class="utama">
                    <tr>
                        <td width="70%"><label>Catatan :</label></td>
                        <td width="30%"><label>Nama Penanggung Jawab :</label></td>
                    </tr>
                    <tr>
                        <td>
                            <div style="width: 100%; border: 1px solid #ddd; margin-right: 20px; padding: 5px">
                                {{$data->catatan}}
                            </div>
                        </td>
                        <td>
                            {{$data->penanggung_jawab}}
                        </td>
                    </tr>
                </table>
                <br><br>
                <table width="100%" border="0">
                    <tr>
                        <td width="70%">Mengetahui,</td>
                        <td>.......................... , ...................</td>
                    </tr>
                    <tr>
                        <td>Pimpinan Laboratorium</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>{{$perusahaan}}</td>
                        <td>Penanggungjawab Pemeriksaan</td>
                    </tr>
                    <tr>
                        <td height="50px"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Nama ...................................</td>
                        <td>Nama ...................................</td>
                    </tr>
                    <tr>
                        <td>NIP .......................................</td>
                        <td>NIP .......................................</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = mm + '/' + dd + '/' + yyyy;
document.getElementById("demo").innerHTML = today;
</script>
<script type="text/javascript">
$('.datepick').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});

</script>
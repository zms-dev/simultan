<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
    padding: 50px
}
p{
    font-size: 12px;
}
table, td, th {  
    font-size: 12px;  
    text-align: left;
}

.inti,.inti td, .inti th {    
    border: 1px solid #333;
    text-align: left;
    border-collapse: collapse;
}
.inti td, .inti th{
    padding: 5px;
}
</style>
<title>Cetak Kuitansi</title>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <table style="margin-top: 100px" width="100%">
                        <tr>
                            <td width="50px">Nomor</td>
                            <td width="5px">:</td>
                            <td width="350px">{{$detail->kode_lebpes}}</td>
                            <td style="text-align: right;">Tanggal :</td>
                            <td>{{$date}}</td>
                        </tr>
                        <tr>
                            <td>Lampiran</td>
                            <td>:</td>
                            <td>1 (satu) Paket</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Perihal</td>
                            <td>:</td>
                            <td>Pengiriman 1 (satu) kotak Sampel Kontrol PNPME - {{$detail->Parameter}} Siklus I tahun 2018</td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table><br>
                    <p>Yang terhormat,<br>
                    Kepala {{$detail->nama_lab}}<br>
                    {{$detail->Provinsi}} - {{$detail->Kota}} - {{$detail->Kecamatan}} - {{$detail->Kelurahan}}<br>
                    {{$detail->alamat}}</p>

                    <p>Bersama ini kami kirimkan 1 (satu) kotak Sampel Kontrol Program Nasional Pemantapan Mutu Eksternal {{$detail->Parameter}} I tahun 2018 yang terdiri dari :</p>

                    <table class="inti" width="100%">
                        <tr>
                            <th><center>No</center></th>
                            <th><center>Jenis Barang</center></th>
                            <th><center>Jumlah</center></th>
                            <th><center>Keterangan</center></th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Sampel Kontrol PNPME - {{$detail->Parameter}} Siklus I<br>kode : {{$datas1}}</td>
                            <td>{{$datas2}}</td>
                            <td>Mohon segera dimasukkan ke dalam lemari es pada suhu 4 &deg;C</td>
                        </tr>
                    </table>
                    <p>Petunjuk pengujian dan format laporan yang digunakan dapat dilihat pada Petunjuk Pelaksanaan terlampir.</p>
                    <p>Atas perhatian dan kerjasamanya kami ucapkan terimakasih.</p>
                    <table width="100%">
                        <tr>
                            <td width="55%" style="text-align: right; vertical-align: top;">a.n.</td>
                            <td>Kepala Balai Besar Laboratorium Kesehatan<br>S u r a b a y a<br>Kepala Bidang Pengendali Mutu,</td>
                        </tr>
                        <tr>
                            <td height="50px"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><b>Dahana Trikorianti, S.Si.</b><br>NIP 196211151987032002</td>
                        </tr>
                    </table>
                    <small><i>Segera kirimkan kembali potongan ini ke alamat penyelenggara</i></small>
                    <hr style="border: dashed 1px;">
                    <p>{{$detail->nama_lab}}</p>
                    <center><b>TANDA TERIMA</b></center>

                    <table class="inti" width="100%">
                        <tr>
                            <th><center>No</center></th>
                            <th><center>Jenis Barang</center></th>
                            <th><center>Jumlah</center></th>
                            <th><center>Keterangan</center></th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Sampel Kontrol PNPME - {{$detail->Parameter}} Siklus I<br>kode : {{$datas1}}</td>
                            <td>{{$datas2}}</td>
                            <td><input type="checkbox"> Baik<br><input type="checkbox"> Pecah/tumpah</td>
                        </tr>
                    </table>
                    ( Beri tanda ✓ pada <input type="checkbox"> )
                    <table width="100%">
                        <tr>
                            <td width="55%"></td>
                            <td>Tanggal diterima :<br><br>Yang Menerima,</td>
                        </tr>
                        <tr>
                            <td height="50px"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>(.............................................................)</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
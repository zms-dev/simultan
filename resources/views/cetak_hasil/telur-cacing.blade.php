<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama {
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td {
    padding: 5px;
    text-align: left;
    border: 1px solid #333;
}
</style>
<div>
    <label><center>PROGRAM PEMANTAPAN MUTU EKSTERNAL MIKROSKOPIS TELUR CACING SIKLUS {{$siklus}} TAHUN {{$date}}</center></label><br>
</div>
<table>
    <tr>
        <td>Kode Peserta </td>
        <td>: </td>
        <td>{{ $data->kode_peserta }}</td>
    </tr>
    <tr>
        <td>Tanggal Penerimaan </td>
        <td>: </td>
        <td>{{ $data->tgl_penerimaan }}</td>
    </tr>
    <tr>
        <td>Kualitas Bahan </td>
        <td>: </td>
        <td><font style="text-transform: capitalize;">{{$data->kondisi}}</font></td>
    </tr>
    <tr>
        <td>Tanggal Pemeriksaan </td>
        <td>: </td>
        <td>{{ $data->tgl_pemeriksaan }}</td>
    </tr>
    <tr>
        <td>Nama Pemeriksa </td>
        <td>: </td>
        <td>{{ $data->nama_pemeriksa }}</td>
    </tr>
    <tr>
        <td>Nomor HP </td>
        <td>: </td>
        <td>{{ $data->nomor_hp }}</td>
    </tr>
    <tr>
        <td>Pendidikan Pelaksana PME </td>
        <td>: </td>
        <td>@if($data->tingkat == 'Lain - lain'){{ $data->pendidikan_lain }}@else {{$data->tingkat}} @endif</td>
    </tr>
</table>

<table class="utama">
    <tr>
        <th>Kode Botol</th>
        <th><center>Reagen Yang Digunakan</center></th>
        <th><center>Hasil Pemeriksaan</center></th>
    </tr>
    @if(count($data))
    <?php $no = 0; ?>
    @foreach($datas as $val)
    <?php $no++; ?>
    <tr>
        <td>{!!$val->kode_botol!!}</td>
        <td><center>{{ $val->reagen }}<br>{{$val->reagen_lain}}</center></td>
        <td><center>{!! $val->hasil !!}</center></td>
    </tr>
    @endforeach 
    @endif
</table>
<table width="100%" class="utama">
    <tr>
        <td width="70%"><label>Catatan :</label></td>
        <td width="30%"><label>Nama Penanggung Jawab :</label></td>
    </tr>
    <tr>
        <td>
            <div style="width: 100%; margin-right: 20px; padding: 5px">
                {{$data->catatan}}
            </div>
        </td>
        <td>
            {{$data->penanggung_jawab}}
        </td>
    </tr>
</table>
<br><br>
<table width="100%" border="0">
    <tr>
        <td width="70%">Mengetahui,</td>
        <td>.......................... , ...................</td>
    </tr>
    <tr>
        <td>Pimpinan Laboratorium</td>
        <td></td>
    </tr>
    <tr>
        <td>{{$perusahaan}}</td>
        <td>Penanggungjawab Pemeriksaan</td>
    </tr>
    <tr>
        <td height="50px"></td>
        <td></td>
    </tr>
    <tr>
        <td>Nama ...................................</td>
        <td>Nama ...................................</td>
    </tr>
    <tr>
        <td>NIP .......................................</td>
        <td>NIP .......................................</td>
    </tr>
</table>
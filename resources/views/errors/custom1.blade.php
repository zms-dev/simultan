@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="">
            <div class="" style="margin-top: 2%">
            	<h1 style="font-size: 40px;" align="center">Sedang Dalam Proses Evaluasi</h1><br>
            	<table class="table table-bordered">
            		<tr>
            			<th width="2%">No</th>
            			<th>Parameter</th>
            			<th width="10%"><center>Download</center></th>
            		</tr>
            		<tr>
            			<td>01</td>
            			<td>Hematologi</td>
            			<td><center><a href="{{URL::asset('asset/input-hasil/Hematologi.pdf')}}" download="Hematologi"><i class="fas fa-download"></i></a></center></td>
            		</tr>
            		<tr>
            			<td>02</td>
            			<td>Kimia Klinik</td>
            			<td><center><a href="{{URL::asset('asset/input-hasil/Kimia-Klinik.pdf')}}" download="Kimia-Klinik"><i class="fas fa-download"></i></a></center></td>
            		</tr>
            		<tr>
            			<td>03</td>
            			<td>Urinalisa</td>
            			<td><center><a href="{{URL::asset('asset/input-hasil/Urinalisa.pdf')}}" download="Urinalisa"><i class="fas fa-download"></i></a></center></td>
            		</tr>
            		<tr>
            			<td>04</td>
            			<td>Mikrobiologi BTA</td>
            			<td><center><a href="{{URL::asset('asset/input-hasil/BTA.pdf')}}" download="BTA"><i class="fas fa-download"></i></a></center></td>
            		</tr>
            		<tr>
            			<td>05</td>
            			<td>Mikrobiologi Telur Cacing</td>
            			<td><center><a href="{{URL::asset('asset/input-hasil/Telur Cacing.pdf')}}" download="Telur Cacing"><i class="fas fa-download"></i></a></center></td>
            		</tr>
            		<tr>
            			<td>06</td>
            			<td>Imunologi Anti HIV</td>
            			<td><center><a href="{{URL::asset('asset/input-hasil/Anti Hiv.pdf')}}" download="Anti Hiv">Umum<i class="fas fa-download"></i></a></center><center><a href="{{URL::asset('asset/input-hasil/Anti Hiv PMI.pdf')}}" download="Anti Hiv PMI">PMI<i class="fas fa-download"></i></a></center></td>
            		</tr>
            		<tr>
            			<td>07</td>
            			<td>Imunologi Sifilis</td>
            			<td><center><a href="{{URL::asset('asset/input-hasil/Syphilis TP.pdf')}}" download="Syphilis TP">TP<i class="fas fa-download"></i></a></center><center><a href="{{URL::asset('asset/input-hasil/RPR-Syphilis.pdf')}}" download="RPR-Syphilis">RPR<i class="fas fa-download"></i></a></center></td>
            		</tr>
            		<tr>
            			<td>08</td>
            			<td>Imunologi HBsAg</td>
            			<td><center><a href="{{URL::asset('asset/input-hasil/HBsAg.pdf')}}" download="HBsAg"><i class="fas fa-download"></i></a></center></td>
            		</tr>
            		<tr>
            			<td>09</td>
            			<td>Imunologi Anti HCV</td>
            			<td><center><a href="{{URL::asset('asset/input-hasil/ANTI-HCV.pdf')}}" download="ANTI-HCV"><i class="fas fa-download"></i></a></center></td>
            		</tr>
            		<tr>
            			<td>10</td>
            			<td>Mikrobiologi Malaria</td>
            			<td><center><a href="{{URL::asset('asset/input-hasil/Malaria.pdf')}}" download="Malaria"><i class="fas fa-download"></i></a></center></td>
            		</tr>
            		<tr>
            			<td>11</td>
            			<td>Kimia Air (Besi (Fe), Tembaga (Cu), Kadmium (Cd), Kromium (Cr))</td>
            			<td><center><a href="{{URL::asset('asset/input-hasil/Kimia-Air.pdf')}}" download="Kimia-Air"><i class="fas fa-download"></i></a></center></td>
            		</tr>
            		<tr>
            			<td>12</td>
            			<td>Kimia Air Terbatas (Besi (Fe), Tembaga (Cu))</td>
            			<td><center><a href="{{URL::asset('asset/input-hasil/Kimia Air terbatas.pdf')}}" download="Kimia Air terbatas"><i class="fas fa-download"></i></a></center></td>
            		</tr>
            		<tr>
            			<td>13</td>
            			<td>Identifikasi Bakteri dan Uji Kepekaan Antibiotik</td>
            			<td><center><a href="{{URL::asset('asset/input-hasil/Identifikasi Bakteri.pdf')}}" download="Identifikasi Bakteri"><i class="fas fa-download"></i></a></center></td>
            		</tr>
            	</table>
            </div>
        </div>
    </div>
</div>
@endsection
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <title>Evaluasi</title>
  </head>
  <style type="text/css">
    body{
        height: 100vh;
        background-position: 50% 100%;
        background-color: #22a5dd;
        font-family: arial;
    }
    .wrapper{
        padding:30px 0px;
        text-align: center;
        color: #fff;
    }
    .wrapper h1 b{
      background-color: #fff;
      color: #22a5dd;
      padding: 2px 5px;
      border-radius: 4px;
    }
    table td{
      text-align: left;
    }
    a {
      text-decoration: none;
      color: inherit;
    }
  </style>
  <body>
    <div class="wrapper">
        <h4>D a l a m &nbsp; P r o s e s</h4>
        <h1><b>EVALUASI</b></h1>
        <a href="" data-toggle="modal" data-target="#exampleModal">
          Download Form Hasil
        </a><br>
        <img src="{{URL::asset('asset/img/lab-bg.gif')}}" width="100%" style="max-width: 900px">
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><font style="color: #333;">Download Form Hasil</font></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <table class="table table-bordered">
                  <tr>
                    <th width="2%">No</th>
                    <th>Parameter</th>
                    <th colspan="2"><center>Download</center></th>
                  </tr>
                  <tr>
                    <td>01</td>
                    <td>Hematologi</td>
                    <td colspan="2"><center><a href="{{URL::asset('asset/input-hasil/Hematologi.pdf')}}" target="_blank"><i class="fas fa-download"></i></a></center></td>
                  </tr>
                  <tr>
                    <td>02</td>
                    <td>Kimia Klinik</td>
                    <td colspan="2"><center><a href="{{URL::asset('asset/input-hasil/Kimia-Klinik.pdf')}}" target="_blank"><i class="fas fa-download"></i></a></center></td>
                  </tr>
                  <tr>
                    <td>03</td>
                    <td>Urinalisa</td>
                    <td colspan="2"><center><a href="{{URL::asset('asset/input-hasil/Urinalisa.pdf')}}" target="_blank"><i class="fas fa-download"></i></a></center></td>
                  </tr>
                  <tr>
                    <td>04</td>
                    <td>Mikrobiologi BTA</td>
                    <td colspan="2"><center><a href="{{URL::asset('asset/input-hasil/BTA.pdf')}}" target="_blank"><i class="fas fa-download"></i></a></center></td>
                  </tr>
                  <tr>
                    <td>05</td>
                    <td>Mikrobiologi Telur Cacing</td>
                    <td colspan="2"><center><a href="{{URL::asset('asset/input-hasil/Telur Cacing.pdf')}}" target="_blank"><i class="fas fa-download"></i></a></center></td>
                  </tr>
                  <tr>
                    <td>06</td>
                    <td>Imunologi Anti HIV</td>
                    <td>
                      <center><a href="{{URL::asset('asset/input-hasil/Anti Hiv.pdf')}}" target="_blank">Umum&nbsp;<i class="fas fa-download"></i></a></center>
                    </td>
                    <td>
                      <center><a href="{{URL::asset('asset/input-hasil/Anti Hiv PMI.pdf')}}" target="_blank">PMI&nbsp;<i class="fas fa-download"></i></a></center>
                    </td>
                  </tr>
                  <tr>
                    <td>07</td>
                    <td>Imunologi Sifilis</td>
                    <td>
                      <center><a href="{{URL::asset('asset/input-hasil/Syphilis TP.pdf')}}" target="_blank">TP&nbsp;<i class="fas fa-download"></i></a></center>
                    </td>
                    <td>
                      <center><a href="{{URL::asset('asset/input-hasil/RPR-Syphilis.pdf')}}" target="_blank">RPR&nbsp;<i class="fas fa-download"></i></a></center>
                    </td>
                  </tr>
                  <tr>
                    <td>08</td>
                    <td>Imunologi HBsAg</td>
                    <td colspan="2"><center><a href="{{URL::asset('asset/input-hasil/HBsAg.pdf')}}" target="_blank"><i class="fas fa-download"></i></a></center></td>
                  </tr>
                  <tr>
                    <td>09</td>
                    <td>Imunologi Anti HCV</td>
                    <td colspan="2"><center><a href="{{URL::asset('asset/input-hasil/ANTI-HCV.pdf')}}" dotarget="_blank"><i class="fas fa-download"></i></a></center></td>
                  </tr>
                  <tr>
                    <td>10</td>
                    <td>Mikrobiologi Malaria</td>
                    <td colspan="2"><center><a href="{{URL::asset('asset/input-hasil/Malaria.pdf')}}" target="_blank"><i class="fas fa-download"></i></a></center></td>
                  </tr>
                  <tr>
                    <td>11</td>
                    <td>Kimia Air (Besi (Fe), Tembaga (Cu), Kadmium (Cd), Kromium (Cr))</td>
                    <td colspan="2"><center><a href="{{URL::asset('asset/input-hasil/Kimia-Air.pdf')}}" target="_blank"><i class="fas fa-download"></i></a></center></td>
                  </tr>
                  <tr>
                    <td>12</td>
                    <td>Kimia Air Terbatas (Besi (Fe), Tembaga (Cu))</td>
                    <td colspan="2"><center><a href="{{URL::asset('asset/input-hasil/Kimia Air terbatas.pdf')}}" target="_blank"><i class="fas fa-download"></i></a></center></td>
                  </tr>
                  <tr>
                    <td>13</td>
                    <td>Identifikasi Bakteri dan Uji Kepekaan Antibiotik</td>
                    <td colspan="2"><center><a href="{{URL::asset('asset/input-hasil/Identifikasi Bakteri.pdf')}}" target="_blank"><i class="fas fa-download"></i></a></center></td>
                  </tr>
                </table>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">Monitoring Evaluasi Hasil Peserta per Parameter</div>
                <div class="panel-body">  
                  <table class="table table-bordered">
                    <tr>
                      <th colspan="5" style="text-transform: capitalize;">Bidang {{$bidang->parameter}} Tahun {{$input['tahun']}}</th>
                    </tr>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Nama Instansi</center></th>
                      <th><center>Provinsi</center></th>
                      <?php for ($i=$input['tahun']; $i < $input['tahun'] + 3; $i++) { ?>
                      <th><center>Tahun {{$i}} Siklus 1</center></th>
                      <th><center>Tahun {{$i}} Siklus 2</center></th>
                      <?php } ?>
                    </tr>
                    @if(count($data))
                    <?php $no = 0; ?>
                    @foreach($data as $val)
                    <?php $no++; ?>
                    <tr>
                      <td style="text-align: center;">{{$no}}</td>
                      <td style="text-transform: capitalize;">{{$val->nama_lab}}</td>
                      <td>{{$val->name}}</td>
                      @if($bidang->id == 4)
                      <?php for ($i=1; $i < 3; $i++) { ?>
                        <?php if ($val->{'data' . $i} != NULL) {?>
                        <td>{{$val->{'data' . $i}->zscore1}}</td>
                        <td>{{$val->{'data' . $i}->zscore2}}</td>
                        <?php }else{ ?>
                        <td></td>
                        <td></td>
                        <?php } ?>
                      <?php } ?>
                      @elseif($bidang->id == 5)
                      <?php for ($i=1; $i < 3; $i++) { ?>
                        <td>
                          @if($val->{'data' . $i}->zscore1 != NULL)
                            @if($val->{'data' . $i}->zscore1 >= 6)
                                Baik
                            @else
                                Kurang
                            @endif
                          @endif
                        </td>
                        <td>
                          @if($val->{'data' . $i}->zscore2 != NULL)
                            @if($val->{'data' . $i}->zscore2 >= 6)
                                Baik
                            @else
                                Kurang
                            @endif
                          @endif
                        </td>
                      <?php } ?>
                      @elseif($bidang->id == 10)
                      <?php for ($i=1; $i < 3; $i++) { ?>
                        @if($val->{'data' . $i} != NULL)
                          <td>{{$val->{'data' . $i}->zscore1}}</td>
                          <td>{{$val->{'data' . $i}->zscore2}}</td>
                        @else
                          <td></td>
                          <td></td>
                        @endif
                      <?php } ?>
                      @endif
                    </tr>
                    @endforeach
                    @endif
                  </table><br>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
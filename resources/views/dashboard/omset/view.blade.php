 <table border="1" width="100%">
  <thead>
    <tr>
      <th>No</th>
      <th>Pembayaran</th>
      <th>Siklus 1</th>
      <th>Siklus 2</th>
      <th>Siklus 1 dan 2</th>
      <th>Total</th>
    </tr>
  </thead>
  <tbody class="body-siklus">
    @if(count($data))
    <?php $no = 0; ?>
    @foreach($data as $val)
    <?php 
    $total = $val->siklus1 + $val->siklus2 + $val->siklus12;
    $no++; 
    ?>
    <tr>
      <td>{{$no}}</td>
      <td>{{$val->type}}</td>
      <td>{{$val->siklus1}}</td>
      <td>{{$val->siklus2}}</td>
      <td>{{$val->siklus12}}</td>
      <td>{{$total}}</td>
    </tr>
    @endforeach
    @endif   
  </tbody>
</table>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">Monitoring Evaluasi Hasil Peserta per Parameter</div>
                <div class="panel-body">  
                  <table class="table table-bordered">
                    <tr>
                      <th colspan="5" style="text-transform: capitalize;">Bidang {{$bidang->parameter}} Tahun {{$input['tahun']}}</th>
                    </tr>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Nama Instansi</center></th>
                      <th><center>Provinsi</center></th>
                      <?php for ($i=$input['tahun']; $i < $input['tahun'] + 3; $i++) { ?>
                      <th><center>Tahun {{$i}} Siklus 1</center></th>
                      <th><center>Tahun {{$i}} Siklus 2</center></th>
                      <?php } ?>
                    </tr>
                    @if(count($data))
                    <?php $no = 0; ?>
                    @foreach($data as $val)
                    <?php $no++; ?>
                    <tr>
                      <td style="text-align: center;">{{$no}}</td>
                      <td style="text-transform: capitalize;">{{$val->nama_lab}}</td>
                      <td>{{$val->name}}</td>
                      <?php for ($i=1; $i < 3; $i++) {?>
                      <td>
                        @if($val->{'data' . $i} != NULL)
                          {{$val->{'data' . $i}->zscore1}}
                        @endif
                      </td>
                      <td>
                        @if($val->{'data' . $i} != NULL)
                          {{$val->{'data' . $i}->zscore2}}
                        @endif
                      </td>
                    <?php } ?>
                    </tr>
                    @endforeach
                    @endif
                  </table><br>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<table>
  <tr>
    <th colspan="12">Laporan Jumlah Peserta per Provinsi per Instansi Bidang {{$bidangna}}</th>
  </tr>
  <tr>
    <th><center>No</center></th>
    <th><center>Provinsi</center></th>
    <th><center>BLK</center></th>
    <th><center>BBLK</center></th>
    <th><center>Rumah Sakit - Pemerintah</center></th>
    <th><center>Puskesmas</center></th>
    <th><center>Labkesda</center></th>
    <th><center>Rumah Sakit - Swasta</center></th>
    <th><center>LabKlinik Swasta</center></th>
    <th><center>UTD - RS</center></th>
    <th><center>UTD - PMI</center></th>
    <th><center>RS TNI/Polri</center></th>
  </tr>
  @if(count($data))
  <?php $no = 0; ?>
  @foreach($data as $val)
  <?php $no++; ?>
  <tr>
    <td style="text-align: center;">{{$no}}</td>
    <td>{{$val->name}}</td>
    <td style="text-align: right;">{{$val->blk}}</td>
    <td style="text-align: right;">{{$val->bblk}}</td>
    <td style="text-align: right;">{{$val->rspem}}</td>
    <td style="text-align: right;">{{$val->pus}}</td>
    <td style="text-align: right;">{{$val->labke}}</td>
    <td style="text-align: right;">{{$val->rssw}}</td>
    <td style="text-align: right;">{{$val->labkl}}</td>
    <td style="text-align: right;">{{$val->utdrs}}</td>
    <td style="text-align: right;">{{$val->utdpm}}</td>
    <td style="text-align: right;">{{$val->rstni}}</td>
  </tr>
  @endforeach
  @endif
</table>
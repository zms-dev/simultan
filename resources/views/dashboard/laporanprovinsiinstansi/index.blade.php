@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
              <div class="panel-heading">Laporan Jumlah Peserta per Provinsi per Instansi</div>
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                <div class="panel-body">
                  <div>
                      <label for="exampleInputEmail1">Tahun</label>
                      <div class="controls input-append date form_datetime" data-date="2017" data-link-field="dtp_input1">
                          <input size="16" type="text" value="" readonly class="form-control coba" name="tahun" id="tahun" required>
                          <span class="add-on"><i class="icon-th"></i></span>
                      </div>
                  </div><br>
                  <div>
                      <label for="exampleInputEmail1">Bidang</label>
                      <select class="form-control" name="bidang" required>
                        <option value=""></option>
                        <option value="Semua Bidang">Semua Bidang</option>
                        @foreach($bidang as $val)
                        <option value="{{$val->id}}">{{$val->alias}}</option>
                        @endforeach
                      </select>
                  </div><br>
                  {{ csrf_field() }}
                  <input type="submit" name="proses" value="View Data" class="btn btn-submit">
                  <input type="submit" name="proses" value="Download Excel" class="btn btn-submit">
                </form>
              </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "yyyy",
        autoclose: true,
        todayBtn: true,
        pickerPosition: "bottom-right",
        minView: 4,
        startView: 4
    });
</script>
@endsection
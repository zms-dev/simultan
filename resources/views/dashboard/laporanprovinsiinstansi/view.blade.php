@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">Laporan Jumlah Peserta per Provinsi per Instansi Bidang {{$bidangna}}</div>
                <div class="panel-body">
                  <table class="table table-bordered">
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Provinsi</center></th>
                      <th><center>BLK</center></th>
                      <th><center>BBLK</center></th>
                      <th><center>Rumah Sakit - Pemerintah</center></th>
                      <th><center>Puskesmas</center></th>
                      <th><center>Labkesda</center></th>
                      <th><center>Rumah Sakit - Swasta</center></th>
                      <th><center>LabKlinik Swasta</center></th>
                      <th><center>UTD - RS</center></th>
                      <th><center>UTD - PMI</center></th>
                      <th><center>RS TNI/Polri</center></th>
                    </tr>
                    @if(count($data))
                    <?php $no = 0; ?>
                    @foreach($data as $val)
                    <?php $no++; ?>
                    <tr>
                      <td style="text-align: center;">{{$no}}</td>
                      <td>{{$val->name}}</td>
                      <td style="text-align: right;">{{$val->blk}}</td>
                      <td style="text-align: right;">{{$val->bblk}}</td>
                      <td style="text-align: right;">{{$val->rspem}}</td>
                      <td style="text-align: right;">{{$val->pus}}</td>
                      <td style="text-align: right;">{{$val->labke}}</td>
                      <td style="text-align: right;">{{$val->rssw}}</td>
                      <td style="text-align: right;">{{$val->labkl}}</td>
                      <td style="text-align: right;">{{$val->utdrs}}</td>
                      <td style="text-align: right;">{{$val->utdpm}}</td>
                      <td style="text-align: right;">{{$val->rstni}}</td>
                    </tr>
                    @endforeach
                    @endif
                  </table><br>
                  <div id="container" style="height: 1000px"></div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  
var chart = Highcharts.chart('container', {
    chart: {
        inverted: true,
        polar: false
    },
    subtitle: {
        text: 'Inverted'
    },

    yAxis: {
      title: {
        text : 'Jumlah'
      }
    },
    title: {
        text: 'Laporan Jumlah Peserta per Provinsi per Instansi Bidang {{$bidangna}}'
    },

    subtitle: {
        text: ''
    },

    xAxis: {
        categories: [
                    @if(count($data))
                    @foreach($data as $val)
                    '{{$val->name}}',
                    @endforeach
                    @endif
                    ]
    },
    plotOptions: {
      series : {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format : '{point.y:.0f}'
        }
      }
    },

    series: [{
        type: 'column',
        colorByPoint: true,
        data: [
                @if(count($data))
                @foreach($data as $val)
                {{$val->all}}, 
                @endforeach
                @endif
              ],
        showInLegend: false
    }]

});
</script>
@endsection
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">Monitoring Evaluasi Hasil Peserta per Parameter</div>
                <div class="panel-body">  
                  <table class="table table-bordered">
                    <tr>
                      <th colspan="5" style="text-transform: capitalize;">Bidang Urinalisa Tahun {{$input['tahun']}}</th>
                    </tr>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Nama Instansi</center></th>
                      <th><center>Badan Usaha</center></th>
                      <th><center>Provinsi</center></th>
                      <th><center>Kabupaten / Kota</center></th>
                      <th><center>Bahan</center></th>
                      <?php for ($i=$input['tahun']; $i < $input['tahun'] + 5; $i++) { ?>
                      <th><center>Tahun {{$i}} Siklus 1</center></th>
                      <th><center>Tahun {{$i}} Siklus 2</center></th>
                      <?php } ?>
                    </tr>
                    @if(count($data))
                    <?php $no = 0; ?>
                    @foreach($data as $val)
                    <?php $no++; ?>
                    <tr>
                      <td style="text-align: center;">{{$no}}</td>
                      <td style="text-transform: capitalize;">{{$val->nama_lab}}</td>
                      <td>{{$val->badan_usaha}}</td>
                      <td>{{$val->name}}</td>
                      <td>{{$val->kota}}</td>
                      <td style="text-align: center;">@if($val->type == 'a') 01 @else 02 @endif</td>
                      <?php 
                      for ($i=1; $i < 5; $i++) {
                        if ($val->{'data' . $i} != NULL) {
                          if($val->{'data' . $i}->zscore1 == NULL){
                              $keteranganParameter = '';
                          }else if($val->{'data' . $i}->zscore1 > 3){
                              $keteranganParameter = 'Sangat Baik';
                          }else if($val->{'data' . $i}->zscore1 > 2 && $val->{'data' . $i}->zscore1 <= 3){
                              $keteranganParameter = 'Baik';
                          }else if($val->{'data' . $i}->zscore1 > 1 && $val->{'data' . $i}->zscore1 <= 2){
                              $keteranganParameter = 'Kurang';
                          }else if($val->{'data' . $i}->zscore1 <= 1){
                              $keteranganParameter = 'Buruk';
                          }
                          if($val->{'data' . $i}->zscore2 == NULL){
                              $keteranganParameter1 = '';
                          }else if($val->{'data' . $i}->zscore2 > 3){
                              $keteranganParameter1 = 'Sangat Baik';
                          }else if($val->{'data' . $i}->zscore2 > 2 && $val->{'data' . $i}->zscore2 <= 3){
                              $keteranganParameter1 = 'Baik';
                          }else if($val->{'data' . $i}->zscore2 > 1 && $val->{'data' . $i}->zscore2 <= 2){
                              $keteranganParameter1 = 'Kurang';
                          }else if($val->{'data' . $i}->zscore2 <= 1){
                              $keteranganParameter1 = 'Buruk';
                          }
                        }else{
                          $keteranganParameter1 = '';
                          $keteranganParameter = '';
                        }
                      ?>
                      <td>
                        {{$keteranganParameter}}
                      </td>
                      <td>
                        {{$keteranganParameter1}}
                      </td>
                    <?php } ?>
                    </tr>
                    @endforeach
                    @endif
                  </table><br>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
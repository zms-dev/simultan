@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Rekapitulasi Peserta Badan Usaha Berdasarkan Provinsi </div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" id="badanusaha" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-date="2017" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control coba" name="tahun" id="tahun" required>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div>
                      <div>
                          <label for="exampleInputEmail1">Provinsi</label>
                          <select name="provinsi" id="provinsi" class="form-control coba">
                            <option value=""></option>
                            @if(count($provinsi))
                            @foreach($provinsi as $val)
                            <option value="{{$val->id}}">{{$val->name}}</option>
                            @endforeach
                            @endif
                          </select>
                      </div>
                      <br>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                            <th>No</th>
                            <th>Badan Usaha</th>
                            <th>Siklus 1</th>
                            <th>Siklus 2</th>
                            <th>Siklus 1 dan 2</th>
                            <th>Total</th>
                        </tr>
                      </thead>
                      <tbody class="body-siklus">
                        
                      </tbody>
                    </table>
                      {{ csrf_field() }}
                    <div class="export">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$(".coba").change(function(){
  var val = $('#tahun').val(), i, no = 0;
  var provinsi = $('#provinsi').val();
  var y = document.getElementById('datasiklus')
  $(".body-siklus").html("<tr><td colspan='6'>Please Wait... </td></tr>")
  $.ajax({
    type: "GET",
    data: {
          tahun:val,
          provinsi:provinsi
        },
    url : "{{url('badanusahaprovinsi')}}",
    success: function(addr){
      // y.innerHTML = '<tr id="datasiklus"><td id="nosiklus"></td><td id="paramsiklus"></td><td id="bidangsiklus"></td><td id="tarifsiklus"></td></tr>';
      $(".body-siklus").html("");
      $(".export").html("");
      if(addr.Hasil != undefined){
        var no = 1;
        $.each(addr.Hasil,function(e,item){
            var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.badan_usaha+"</td><td class=\"bidangsiklus\">"+item.siklus1+"</td><td class='tarifsiklus'>"+item.siklus2+"</td><td class='tarifsiklus'>"+item.siklus12+"</td><td class='tarifsiklus'>"+item.jumlah+"</td>";
            $(".body-siklus").append(html);
          no++;
        })
        var exportna = "<button type=\"submit\" name=\"print\" class=\"btn btn-info\" onclick=\"printpdf()\">Print PDF</button> <button type=\"submit\" name=\"print\" class=\"btn btn-info\" onclick=\"printexcel()\">Print Excel</button>";
        $(".export").append(exportna);
      }
      return false;
    }
  });
});

$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});

form=document.getElementById("badanusaha");
function printpdf() {
        form.action="{{ url('/dashboard/badanusahaprovinsi/pdf')}}";
        form.submit();
}
function printexcel() {
        form.action="{{ url('/dashboard/badanusahaprovinsi/excel')}}";
        form.submit();
}
</script>
@endsection
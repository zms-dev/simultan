@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
              <div class="panel-heading">Monitoring Evaluasi Hasil Peserta per Parameter</div>
                <form id="someForm" action="" method="POST">
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-md-12">
                        <label for="exampleInputEmail1">Bidang</label>
                        <select class="form-control" name="bidang" id="bidang" required>
                          <option value=""></option>
                          <option value="hematologi">Hematologi</option>
                          <option value="kimia klinik">Kimia Klinik</option>
                          <option value="3">Urinalisa</option>
                          <option value="4">Mikroskopis BTA</option>
                          <option value="5">Mikroskopis TC</option>
                          <option value="6">Anti HIV</option>
                          <option value="7">Syphilis</option>
                          <option value="8">HBsAg</option>
                          <option value="9">Anti HCV</option>
                          <option value="10">Mikroskopis Malaria</option>
                          <option value="kimia air">Kimia Air</option>
                          <option value="kimia air terbatas">Kimia Air Terbatas</option>
                        </select>
                      </div>
                      <div class="col-md-12">
                        <div id="parametern">
                          <label for="exampleInputEmail1">Parameter</label>
                          <select class="form-control" name="parameter" id="parameter">
                            <option value=""></option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                      <label for="exampleInputEmail1">Tahun Awal</label>
                        <div class="controls input-append date form_datetime" data-date="2017" data-link-field="dtp_input1">
                            <input size="16" type="text" value="" readonly class="form-control coba" name="tahun" id="tahun" required>
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                      </div>
                      <div class="col-md-6">
                      <label for="exampleInputEmail1">Tahun Akhir <small>*(Auto)</small></label>
                        <input type="text" id="akhir" disabled class="form-control">
                      </div>
                    </div><br>
                  {{ csrf_field() }}
                  <input type="button" value="Export Excel" class="btn btn-primary" name="finished" id="Export" />
                  </div>
                </form>
              </div>
            </div>
        </div>
    </div>
</div>

<div id="loader">
  <div class="text">Loading</div>
  <div class="dot"></div>
  <div class="dot"></div>
  <div class="dot"></div>
  <div class="dot"></div>
  <div class="dot"></div>
  <div class="dot"></div>
  <div class="dot"></div>
  <div class="dot"></div>
  <div class="lading"></div>
</div>
<audio controls hidden><source src="{{url('asset/img/alarm.mp3')}}" type='audio/mpeg'></audio>
<script type="text/javascript">
$( "#tahun" ).change(function() {
    var value = $( this ).val();
    akhir = parseInt(value) + 2;
    $( "#akhir" ).val(akhir);
});

form=document.getElementById("someForm");
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});

$('#loader').hide();

$('#Export').click(function() {
  var bidang = $("#bidang option:selected").val();
  var parameter = $("#parameter option:selected").val();
  var tahun = $("input[name=tahun]").val();
  if(bidang == ''){
    alert("Bidang Wajib Diisi.");
    return false;
  }
  if($("#parameter").attr('required')){
    if(parameter == ''){
      alert("Parameter Wajib Diisi.");
      return false;
    }
  }
  if(tahun == ''){
    alert("Tahun Wajib Diisi.");
    return false;
  }
  $('#loader').show();
  $.ajax({
     type:"POST",
     url:"{{URL('monitoring-evaluasi-parameter')}}",
     timeout : 0,
     data:{bidang:bidang, tahun:tahun, parameter:parameter},
     success:function(response){
        var a = document.createElement("a");
        a.href = response.file;
        a.download = response.name;
        document.body.appendChild(a);
        a.click();
        a.remove();
        $('#loader').hide();
        $('audio')[0].play();
     }
  });
});
</script>
@endsection
@section('scriptBlock')
<script>                            
$(function() {
    $('#parametern').hide(); 
    $('#bidang').change(function(){
    var setan  = $("#bidang option:selected").text();
        if(setan == 'Hematologi' || setan == 'Kimia Klinik' || setan == 'Kimia Air' || setan == 'Kimia Air Terbatas' ) {
            $('#parameter').prop('required', true);
            $('#parametern').show(); 
            var val = $(this).val(), i;
            var y = document.getElementById('parameter');
            $("#parameter").val("");
            y.innerHTML = "<option>Please Wait... </option>";
            $.ajax({
              type: "GET",
              url : "{{url('getparameter').'/'}}"+val,
              success: function(addr){
                y.innerHTML = "<option> </option>"
                for(i = 0; i < addr.Hasil.length; i++){
                  var option = document.createElement("option");
                  option.value = addr.Hasil[i].Kode;
                  option.text = addr.Hasil[i].Parameter;
                  y.add(option);
                }
                return false;
              }
            });
        } else {
            $('#parameter').removeAttr('required'); 
            $('#parametern').hide(); 
        } 
    });
});

</script>
@endsection
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">Monitoring Evaluasi Hasil Peserta per Parameter</div>
                <div class="panel-body">  
                  <table class="table table-bordered">
                    <tr>
                      <th colspan="5" style="text-transform: capitalize;">Bidang {{$input['bidang']}} Parameter {{$parameter->nama_parameter}} Tahun {{$input['tahun']}}</th>
                    </tr>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Nama Instansi</center></th>
                      <th><center>Badan Usaha</center></th>
                      <th><center>Provinsi</center></th>
                      <th><center>Kabupaten / Kota</center></th>
                      <th><center>Bahan</center></th>
                      <?php for ($i=$input['tahun']; $i < $input['tahun'] + 3; $i++) { ?>
                      @if($parameter->kategori == 'kimia air' || $parameter->kategori == 'kimia air terbatas')
                      <th><center>Tahun {{$i}} Siklus 1</center></th>
                      <th><center>Tahun {{$i}} Siklus 2</center></th>
                      @else
                      <th><center>Tahun {{$i}} Siklus 1</center></th>
                      <th><center>Tahun {{$i}} Siklus 1</center></th>
                      <th><center>Tahun {{$i}} Siklus 1</center></th>
                      <th><center>Tahun {{$i}} Siklus 2</center></th>
                      <th><center>Tahun {{$i}} Siklus 2</center></th>
                      <th><center>Tahun {{$i}} Siklus 2</center></th>
                      @endif
                      <?php } ?>
                    </tr>
                    <tr>
                      <th><center></center></th>
                      <th><center></center></th>
                      <th><center></center></th>
                      <th><center></center></th>
                      <th><center></center></th>
                      <th><center></center></th>
                      <?php for ($i=$input['tahun']; $i < $input['tahun'] + 3; $i++) { ?>
                      @if($parameter->kategori == 'kimia air' || $parameter->kategori == 'kimia air terbatas')
                      <th><center></center></th>
                      <th><center></center></th>
                      @else
                      <th><center>Kel. Peserta</center></th>
                      <th><center>Kel. Metode</center></th>
                      <th><center>Kel. Alat</center></th>
                      <th><center>Kel. Peserta</center></th>
                      <th><center>Kel. Metode</center></th>
                      <th><center>Kel. Alat</center></th>
                      @endif
                      <?php } ?>
                    </tr>
                    @if(count($data))
                    <?php $no = 0; ?>
                    @foreach($data as $val)
                    <?php $no++; ?>
                    <tr>
                      <td style="text-align: center;">{{$no}}</td>
                      <td style="text-transform: capitalize;">{{$val->nama_lab}}</td>
                      <td style="text-transform: capitalize;">{{$val->badan_usaha}}</td>
                      <td>{{$val->name}}</td>
                      <td>{{$val->kota}}</td>
                      <td style="text-align: center;">@if($val->type == 'a') 01 @else 02 @endif</td>
                      <?php 
                      for ($i=1; $i < 3; $i++) {
                        if($parameter->kategori != 'kimia air' && $parameter->kategori != 'kimia air terbatas'){
                          if ($val->{'dataalat' . $i} != NULL) {
                            if($val->{'dataalat' . $i}->zscore1 == "-"){
                                $keteranganAlat = "Tidak Analisis";
                            }else if($val->{'dataalat' . $i}->zscore1 <= 2 && $val->{'dataalat' . $i}->zscore1 >= -2){
                                $keteranganAlat = "Memuaskan";
                            }else if($val->{'dataalat' . $i}->zscore1 <= 3 && $val->{'dataalat' . $i}->zscore1 >= -3){
                                $keteranganAlat = "Peringatan";
                            }else if($val->{'dataalat' . $i}->zscore1 > 3 && $val->{'dataalat' . $i}->zscore1 < -3){
                                $keteranganAlat = "Tidak Memuaskan";
                            }else if($val->{'dataalat' . $i}->zscore1 == NULL){
                                $keteranganAlat = "";
                            }else{
                                $keteranganAlat = "Tidak Memuaskan";
                            }
                            if($val->{'dataalat' . $i}->zscore2 == "-"){
                                $keteranganAlat1 = "Tidak Analisis";
                            }else if($val->{'dataalat' . $i}->zscore2 <= 2 && $val->{'dataalat' . $i}->zscore2 >= -2){
                                $keteranganAlat1 = "Memuaskan";
                            }else if($val->{'dataalat' . $i}->zscore2 <= 3 && $val->{'dataalat' . $i}->zscore2 >= -3){
                                $keteranganAlat1 = "Peringatan";
                            }else if($val->{'dataalat' . $i}->zscore2 > 3 && $val->{'dataalat' . $i}->zscore2 < -3){
                                $keteranganAlat1 = "Tidak Memuaskan";
                            }else if($val->{'dataalat' . $i}->zscore2 == NULL){
                                $keteranganAlat1 = "";
                            }else{
                                $keteranganAlat1 = "Tidak Memuaskan";
                            }
                          }else{
                            $keteranganAlat = "";
                            $keteranganAlat1 = "";
                          }
                          if ($val->{'datametode' . $i} != NULL) {
                            if($val->{'datametode' . $i}->zscore1 == "-"){
                                $keteranganMetode = "Tidak Analisis";
                            }else if($val->{'datametode' . $i}->zscore1 <= 2 && $val->{'datametode' . $i}->zscore1 >= -2){
                                $keteranganMetode = "Memuaskan";
                            }else if($val->{'datametode' . $i}->zscore1 <= 3 && $val->{'datametode' . $i}->zscore1 >= -3){
                                $keteranganMetode = "Peringatan";
                            }else if($val->{'datametode' . $i}->zscore1 > 3 && $val->{'datametode' . $i}->zscore1 < -3){
                                $keteranganMetode = "Tidak Memuaskan";
                            }else if($val->{'datametode' . $i}->zscore1 == NULL){
                                $keteranganMetode = "";
                            }else{
                                $keteranganMetode = "Tidak Memuaskan";
                            }
                            if($val->{'datametode' . $i}->zscore2 == "-"){
                                $keteranganMetode1 = "Tidak Analisis";
                            }else if($val->{'datametode' . $i}->zscore2 <= 2 && $val->{'datametode' . $i}->zscore2 >= -2){
                                $keteranganMetode1 = "Memuaskan";
                            }else if($val->{'datametode' . $i}->zscore2 <= 3 && $val->{'datametode' . $i}->zscore2 >= -3){
                                $keteranganMetode1 = "Peringatan";
                            }else if($val->{'datametode' . $i}->zscore2 > 3 && $val->{'datametode' . $i}->zscore2 < -3){
                                $keteranganMetode1 = "Tidak Memuaskan";
                            }else if($val->{'datametode' . $i}->zscore2 == NULL){
                                $keteranganMetode1 = "";
                            }else{
                                $keteranganMetode1 = "Tidak Memuaskan";
                            }
                          }else{
                            $keteranganMetode = "";
                            $keteranganMetode1 = "";
                          }
                        }
                        if ($val->{'data' . $i} != NULL) {
                          if($val->{'data' . $i}->zscore1 == "-"){
                              $keteranganParameter = "Tidak Analisis";
                          }else if($val->{'data' . $i}->zscore1 <= 2 && $val->{'data' . $i}->zscore1 >= -2){
                              $keteranganParameter = "Memuaskan";
                          }else if($val->{'data' . $i}->zscore1 <= 3 && $val->{'data' . $i}->zscore1 >= -3){
                              $keteranganParameter = "Peringatan";
                          }else if($val->{'data' . $i}->zscore1 > 3 && $val->{'data' . $i}->zscore1 < -3){
                              $keteranganParameter = "Tidak Memuaskan";
                          }else if($val->{'data' . $i}->zscore1 == NULL){
                              $keteranganParameter = "";
                          }elseif($val->{'data' . $i}->zscore2 == "Tidak di Evaluasi"){
                              $keteranganParameter = "Tidak di Evaluasi";
                          }else{
                              $keteranganParameter = "Tidak Memuaskan";
                          }
                          if($val->{'data' . $i}->zscore2 == "-"){
                              $keteranganParameter1 = "Tidak Analisis";
                          }else if($val->{'data' . $i}->zscore2 <= 2 && $val->{'data' . $i}->zscore2 >= -2){
                              $keteranganParameter1 = "Memuaskan";
                          }else if($val->{'data' . $i}->zscore2 <= 3 && $val->{'data' . $i}->zscore2 >= -3){
                              $keteranganParameter1 = "Peringatan";
                          }else if($val->{'data' . $i}->zscore2 > 3 && $val->{'data' . $i}->zscore2 < -3){
                              $keteranganParameter1 = "Tidak Memuaskan";
                          }else if($val->{'data' . $i}->zscore2 == NULL){
                              $keteranganParameter1 = "";
                          }elseif($val->{'data' . $i}->zscore2 == "Tidak di Evaluasi"){
                              $keteranganParameter1 = "Tidak di Evaluasi";
                          }else{
                              $keteranganParameter1 = "Tidak Memuaskan";
                          }
                        }else{
                          $keteranganParameter = "";
                          $keteranganParameter1 = "";
                        }
                      ?>
                      @if($keteranganParameter == 'Peringatan')
                      <td style="background-color: #fcff44">
                      @elseif($keteranganParameter == 'Tidak Memuaskan')
                      <td style="background-color: #f44">
                      @elseif($keteranganParameter == 'Tidak Analisis')
                      <td style="background-color: #000; color: #fff">
                      @elseif($keteranganParameter == 'Tidak di Evaluasi')
                      <td style="background-color: #000; color: #fff">
                      @else
                      <td>
                      @endif
                        {{$keteranganParameter}}
                      </td>

                      @if($parameter->kategori != 'kimia air' && $parameter->kategori != 'kimia air terbatas')
                        @if($keteranganMetode == 'Peringatan')
                        <td style="background-color: #fcff44">
                        @elseif($keteranganMetode == 'Tidak Memuaskan')
                        <td style="background-color: #f44">
                        @elseif($keteranganMetode == 'Tidak Analisis')
                        <td style="background-color: #000; color: #fff">
                        @else
                        <td>
                        @endif
                          {{$keteranganMetode}}
                        </td>

                        @if($keteranganAlat == 'Peringatan')
                        <td style="background-color: #fcff44">
                        @elseif($keteranganAlat == 'Tidak Memuaskan')
                        <td style="background-color: #f44">
                        @elseif($keteranganAlat == 'Tidak Analisis')
                        <td style="background-color: #000; color: #fff">
                        @else
                        <td>
                        @endif
                          {{$keteranganAlat}}
                        </td>
                      @endif

                      @if($keteranganParameter1 == 'Peringatan')
                      <td style="background-color: #fcff44">
                      @elseif($keteranganParameter1 == 'Tidak Memuaskan')
                      <td style="background-color: #f44">
                      @elseif($keteranganParameter1 == 'Tidak Analisis')
                      <td style="background-color: #000; color: #fff">
                      @elseif($keteranganParameter == 'Tidak di Evaluasi')
                      <td style="background-color: #000; color: #fff">
                      @else
                      <td>
                      @endif
                        {{$keteranganParameter1}}
                      </td>

                      @if($parameter->kategori != 'kimia air' && $parameter->kategori != 'kimia air terbatas')
                        @if($keteranganMetode1 == 'Peringatan')
                        <td style="background-color: #fcff44">
                        @elseif($keteranganMetode1 == 'Tidak Memuaskan')
                        <td style="background-color: #f44">
                        @elseif($keteranganMetode1 == 'Tidak Analisis')
                        <td style="background-color: #000; color: #fff">
                        @else
                        <td>
                        @endif
                          {{$keteranganMetode1}}
                        </td>
                        @if($keteranganAlat1 == 'Peringatan')
                        <td style="background-color: #fcff44">
                        @elseif($keteranganAlat1 == 'Tidak Memuaskan')
                        <td style="background-color: #f44">
                        @elseif($keteranganAlat1 == 'Tidak Analisis')
                        <td style="background-color: #000; color: #fff">
                        @else
                        <td>
                        @endif
                          {{$keteranganAlat1}}
                        </td>
                      @endif
                    <?php } ?>
                    </tr>
                    @endforeach
                    @endif
                  </table><br>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
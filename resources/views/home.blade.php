@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Info Kuota dan Tarif</div>

                <div class="panel-body table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Bidang</th>
                            <th>Parameter</th>
                            <!-- <th>Kuota</th> -->
                            <th>Sisa Kuota Siklus 1</th>
                            <th>Sisa Kuota Siklus 2</th>
                            <th>Tarif (Rp)</th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++; ?>
                        <tr>
                            <td>{{$no}}</td>
                            <td>{{$val->Bidang}}</td>
                            <td>{{$val->parameter}}</td>
                            <td>{{$val->sisa_kuota_1}}</td>
                            <td>{{$val->sisa_kuota_2}}</td>
                            <td style="text-align: right;">{{number_format($val->tarif)}}</td>
                        </tr>
                        @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

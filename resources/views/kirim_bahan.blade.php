@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Kirim Bahan Siklus {{$siklus}}</div>

                <div class="panel-body">
                    <!-- <form class="form-horizontal" onsubmit="setTimeout(function () { window.location.reload(); }, 10)" action="{{url('kirim-bahan')}}" method="post" target="_blank"> -->
                    <form class="form-horizontal" method="post">
                      {{ csrf_field() }}
                      <!-- <input type="text" name="jenis_barang" class="form-control" placeholder="Jenis Barang"><br>
                      <input type="text" name="jumlah" class="form-control" placeholder="Jumlah"> -->
                    <table id="example" class="display table table-bordered" style="width:100%">
                      <thead>
                        <tr>
                            <th>No</th>
                            <th>Laboratoruim</th>
                            <th><center>Bidang</center></th>
                            <th><center>Parameter</center></th>
                            <th><center>Alamat</center></th>
                            <th><center>No VA</center></th>
                            <th>Cek<center><input type="checkbox" id="checkAll"></center></th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++;?>
                        <tr>
                          <td>{{$no}}</td>
                          <td>{{$val->nama_lab}}<input type="hidden" name="nama_lab[]" value="{{$val->nama_lab}}" ></td>
                          <td>{{$val->Bidang}}<input type="hidden" name="email[]" value="{{$val->email}}" ></td>
                          <td>{{$val->Parameter}}<input type="hidden" name="parameter[]" value="{{$val->Parameter}}" ></td>
                          <td>{{$val->Provinsi}} - {{$val->Kota}} - {{$val->Kecamatan}} - {{$val->Kelurahan}}</td>
                          <td>@if($val->no_urut == NULL || $val->no_urut == '') PKS @else {{$val->no_urut}} @endif</td>
                          <td>
                            <center><input type="checkbox" name="bidang[]" value="{{$val->id}}"></center>
                          </td>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>
                    <label>Catatan :</label>
                    <p>*) Beri tanda &#10003; pada bidang yang sudah dikirim</p>
                    <br>
                      <button type="submit" class="btn btn-info">Kirim</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
$("#checkAll").click(function () {
    $('input:checkbox').not(this).prop('checked', this.checked);
});
</script>
@endsection
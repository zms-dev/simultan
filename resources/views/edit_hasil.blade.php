@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit data</div>
                <div class="panel-body">
                <label>
                    <!-- - Pengisian Hasil Online PNPME Siklus 1 diperpanjang sampai tanggal 24 Mei 2018 -->
                </label>
                    <table class="table table-bordered">
                        <tr>
                            <th width="5%">No</th>
                            <th><center>Bidang</center></th>
                            <th colspan="2" class="siklus1"><center>Siklus&nbsp;1</center></th>
                            <th colspan="2" class="siklus2"><center>Siklus&nbsp;2</center></th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>

                        @if($val->id_bidang > '5')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                @if($val->bidang == '7')
                                    @if($val->siklus_1 == 'done')
                                        @if($val->pemeriksaan == 'done')
                                            @if($val->status_data1 == 1)
                                            <td class="siklus1">
                                                <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                            </td>
                                            @else
                                            <td class="siklus1"></td>
                                            @endif
                                        @else
                                            <td class="siklus1"></td>
                                        @endif
                                        @if($val->rpr1 == 'done')
                                            @if($val->status_datarpr1 == 1)
                                            <td class="siklus1">
                                                <a href="{{URL('hasil-pemeriksaan/rpr-syphilis/edit')}}/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>RPR</center></a>
                                            </td>
                                            @else
                                            <td class="siklus1"></td>
                                            @endif
                                        @else
                                            <td class="siklus1"></td>
                                        @endif
                                    @else
                                    <td colspan="2"  class="siklus1"></td>                           
                                    @endif
                                    @if($val->siklus_2 == 'done')
                                        @if($val->pemeriksaan2 == 'done')
                                            @if($val->status_data2 == 1)
                                            <td class="siklus2">
                                                <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                            </td>
                                            @endif
                                        @else
                                            <td class="siklus2"></td>
                                        @endif
                                        @if($val->rpr2 == 'done')
                                            @if($val->status_datarpr2 == 1)
                                            <td class="siklus2">
                                                <a href="{{URL('hasil-pemeriksaan/rpr-syphilis/edit')}}/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>RPR</center></a>
                                            </td>
                                            @endif
                                        @else
                                            <td class="siklus2"></td>
                                        @endif
                                    @else
                                    <td colspan="2" class="siklus2"></td>
                                    @endif
                                @else
                                    <td colspan="2" class="siklus1">
                                        @if($val->siklus_1 == 'done')
                                            @if($val->status_data1 == 1)
                                            <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                            @endif
                                        @else
                                        @endif
                                    </td>
                                    <td colspan="2" class="siklus2">
                                        @if($val->siklus_2 == 'done')
                                            @if($val->status_data2 == 1)
                                            <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                            @endif
                                        @else
                                        @endif
                                    </td>
                                @endif
                            </tr>
                        @elseif($val->id_bidang < '5')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                @if($val->siklus_1 == 'done')
                                    @if($val->pemeriksaan == 'done')
                                        @if($val->status_data1 == 1)
                                            <td class="siklus1">
                                                <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?x=a&y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>I-01</center></a>
                                            </td>
                                        @else
                                        <td class="siklus1"></td>
                                        @endif
                                    @else
                                        <td class="siklus1"></td>
                                    @endif
                                    @if($val->pemeriksaan2 == 'done')
                                        @if($val->status_data2 == 1)
                                            <td class="siklus1">
                                                <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?x=b&y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>I-02</center></a>
                                            </td>
                                        @else
                                        <td class="siklus1"></td>
                                        @endif
                                    @else
                                        <td class="siklus1"></td>
                                    @endif
                                @else
                                <td colspan="2" class="siklus1"></td>
                                @endif
                                @if($val->siklus_2 == 'done')
                                    @if($val->rpr1 == 'done')
                                        @if($val->status_datarpr1 == 1)
                                            <td class="siklus2">
                                                <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?x=a&y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>II-01</center></a>
                                            </td>
                                        @else
                                        <td class="siklus2"></td>
                                        @endif
                                    @else
                                        <td class="siklus2"></td>
                                    @endif
                                    @if($val->rpr2 == 'done')
                                        @if($val->status_datarpr2 == 1)
                                            <td class="siklus2">
                                                <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?x=b&y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>II-02</center></a>
                                            </td>
                                        @else
                                        <td class="siklus2"></td>
                                        @endif
                                    @else
                                        <td class="siklus2"></td>
                                    @endif
                                @else
                                <td colspan="2" class="siklus2"></td>
                                @endif
                            </tr>
                        @else
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                <td colspan="2" class="siklus1">
                                    @if(($val->pemeriksaan == 'done') && ($val->siklus_1 == 'done'))
                                        @if($val->status_data1 == 1)
                                            <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?x=a&y=1"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                        @endif
                                    @else
                                    @endif
                                </td>
                                <td colspan="2" class="siklus2">
                                    @if(($val->pemeriksaan == 'done') && ($val->siklus_2 == 'done'))
                                        @if($val->status_data2 == 1)
                                            <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?x=a&y=2"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                        @endif
                                    @else
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
// $(document).ready(function() {
//     @if($siklus->siklus == 1)
//     $('.siklus2').hide();
//     @else
//     $('.siklus1').hide();
//     @endif
// });
    (function (global) {

    if(typeof (global) === "undefined")
    {
        throw new Error("window is undefined");
    }

    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";

        // making sure we have the fruit available for juice....
        // 50 milliseconds for just once do not cost much (^__^)
        global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };
    
    // Earlier we had setInerval here....
    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () {
        
        noBackPlease();

        // disables backspace on page except on input fields and textarea..
        document.body.onkeydown = function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                e.preventDefault();
            }
            // stopping event bubbling up the DOM tree..
            e.stopPropagation();
        };
        
    };

})(window);
</script>
@endsection
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
<HEAD>
	<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<TITLE>privacy-policy</TITLE>
	<META name="generator" content="BCL easyConverter SDK 5.0.210">
	<STYLE type="text/css">
		body {margin-top: 0px;margin-left: 0px;}
		#page_1 {position:relative; overflow: hidden;margin: 96px 0px 121px 96px;padding: 0px;border: none;}
		.ft0{font: bold 18px 'Segoe UI';color: #363636;line-height: 15px;}
		.ft1{font: 16px 'Segoe UI';color: #4a4a4a;line-height: 16px;}
		.ft2{font: 16px 'Segoe UI';color: #4a4a4a;line-height: 15px;}
		.ft3{font: 13px 'Times New Roman';color: #4a4a4a;line-height: 15px;}
		.ft4{font: 16px 'Segoe UI';text-decoration: underline;color: #3273dc;margin-left: 18px;line-height: 15px;}
		.ft5{font: 16px 'Segoe UI';text-decoration: underline;color: #3273dc;line-height: 15px;}
		.ft6{font: 16px 'Segoe UI';color: #4a4a4a;margin-left: 18px;line-height: 15px;}
		.ft7{font: 16px 'Segoe UI';text-decoration: underline;color: #0000ff;line-height: 15px;}
		.ft8{font: 16px 'Segoe UI';color: #0000ff;line-height: 15px;}

		.p0{text-align: left;margin-top: 0px;margin-bottom: 0px;}
		.p1{text-align: left;padding-right: 133px;margin-top: 1px;margin-bottom: 0px;}
		.p2{text-align: left;padding-right: 148px;margin-top: 0px;margin-bottom: 0px;}
		.p3{text-align: left;padding-right: 100px;margin-top: 2px;margin-bottom: 0px;}
		.p4{text-align: left;padding-right: 110px;margin-top: 0px;margin-bottom: 0px;}
		.p5{text-align: left;margin-top: 2px;margin-bottom: 0px;}
		.p6{text-align: left;padding-right: 103px;margin-top: 1px;margin-bottom: 0px;}
		.p7{text-align: left;padding-right: 260px;margin-top: 2px;margin-bottom: 0px;}
		.p8{text-align: left;padding-left: 16px;margin-top: 16px;margin-bottom: 0px;}
		.p9{text-align: left;margin-top: 11px;margin-bottom: 0px;}
		.p10{text-align: left;padding-right: 108px;margin-top: 1px;margin-bottom: 0px;}
		.p11{text-align: left;padding-right: 120px;margin-top: 1px;margin-bottom: 0px;}
		.p12{text-align: left;padding-right: 106px;margin-top: 2px;margin-bottom: 0px;}
		.p13{text-align: left;margin-top: 3px;margin-bottom: 0px;}
		.p14{text-align: left;margin-top: 1px;margin-bottom: 0px;}
		.p15{text-align: left;padding-left: 16px;margin-top: 11px;margin-bottom: 0px;}
		.p16{text-align: left;padding-right: 97px;margin-top: 11px;margin-bottom: 0px;}
		.p17{text-align: left;padding-right: 119px;margin-top: 1px;margin-bottom: 0px;}
	</STYLE>
</HEAD>

<BODY>
	<DIV id="page_1">
		<P class="p0 ft0">SIMULTAN Privacy Policy</P>
		<P class="p1 ft1">PT ZAMASCO MITRA SOLUSINDO built the Simultan Android app as a Free app. This SERVICE is provided by PT ZAMASCO MITRA SOLUSINDO is intended for use as is.</P>
		<P class="p2 ft2">This page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.</P>
		<P class="p3 ft1">If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. we will not use or share your information with anyone except as described in this Privacy Policy.</P>
		<P class="p4 ft2">The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Simultan Android unless otherwise defined in this Privacy Policy.</P>
		<br>
		<P class="p5 ft0">Information Collection and Use</P>
		<P class="p6 ft2">For a better experience, while using our Service, we may require you to provide us with certain personally identifiable information. The information that we request will be retained by us and used as described in this privacy policy.</P>
		<P class="p7 ft2">The app does use third party services that may collect information used to identify you. Link to privacy policy of third party service providers used by the app</P>
		<P class="p8 ft5"><SPAN class="ft3">•</SPAN><A href="https://www.google.com/policies/privacy/"><SPAN class="ft4">Google Play Services</SPAN></A></P>
		<P class="p9 ft0">Log Data</P>
		<P class="p10 ft1">we want to inform you that whenever you use our Service, in a case of an error in the app we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing our Service, the time and date of your use of the Service, and other statistics.</P>
		<br>
		<P class="p0 ft0">Cookies</P>
		<P class="p11 ft2">Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device's internal memory.</P>
		<P class="p12 ft2">This Service does not use these “cookies” explicitly. However, the app may use third party code and libraries that use “cookies” to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.</P>
		<br>
		<P class="p13 ft0">Service Providers</P>
		<P class="p14 ft2">we may employ <NOBR>third-party</NOBR> companies and individuals due to the following reasons:</P>
		<P class="p15 ft2"><SPAN class="ft3">•</SPAN><SPAN class="ft6">To facilitate our Service;</SPAN></P>
		<P class="p8 ft2"><SPAN class="ft3">•</SPAN><SPAN class="ft6">To provide the Service on our behalf;</SPAN></P>
		<P class="p15 ft2"><SPAN class="ft3">•</SPAN><SPAN class="ft6">To perform </SPAN><NOBR>Service-related</NOBR> services; or</P>
		<P class="p8 ft2"><SPAN class="ft3">•</SPAN><SPAN class="ft6">To assist us in analyzing how our Service is used.</SPAN></P>
		<P class="p16 ft1">we want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.</P>
		<br>
		<P class="p0 ft0">Security</P>
		<P class="p17 ft1">we value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.</P>
		<br>
		<P class="p0 ft0">Changes to This Privacy Policy</P>
		<P class="p17 ft2">we may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. we will notify you of any changes by posting the new Privacy Policy on this page.</P>
		<P class="p5 ft2">This policy is effective as of <NOBR>2020-04-23</NOBR></P>
		<br>
		<P class="p14 ft0">Contact Us</P>
		<P class="p14 ft2">If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at</P>
		<P class="p5 ft8"><A href="mailto:info@zamasco.co.id"><SPAN class="ft7">info@zamasco.co.id</SPAN></A><SPAN class="ft2"> .</SPAN></P>
	</DIV>
</BODY>
</HTML>

<table border="1">
	<tr>
		<th>No</th>
		<th>Nama Instansi</th>
		<th>Bagaimana pemahaman Saudara tentang kemudahan prosedur pelayanan PNPME</th>
		<th>Bagaimana pendapat Saudara tentang kesesuaian persyaratan pelayanan dengan penerapannya saat mengikuti PNPME</th>
		<th>Bagaimana pendapat Saudara tentang kejelasan informasi kegiatan PNPME di BBLK Surabaya secara umum</th>
		<th>Bagaimana pendapat Saudara dalam kemudahan melengkapi administrasi kegiatan PNPME</th>
		<th>Bagaimana pendapat Saudara tentang penjelasan petugas dalam mengikuti kegiatan PNPME</th>
		<th>Bagaimana pendapat Saudara tantang kecepatan petugas dalam menjawab atau merespon permasalahan kegiatan PNPME</th>
		<th>Bagaimana pendapat Saudara tentang kegiatan PNPME kami secara umum</th>
		<th>Bagaimana pendapat Saudara tantang kesopanan dan kemarahan petugas dalam memberikan pelayanan</th>
		<th>Bagaimana pendapat Saudara tentang kewajaran biaya untuk mendapatkan pelayanan</th>
		<th>Bagaimana pendapat Saudara tantang kesesuaian antara biaya yang dibayarkan dengan biaya yang telah di tetapkan</th>
		<th>Bagaimana pendapat Saudara tentang metode pembayaran yang kami terapkan</th>
		<th>Bagaimana pendapat Saudara tantang rencana program meningkatkan jumlah peserta maupun jumlah parameter</th>
		<th>Bagaimana pendapat Saudara tentang merekomendasikan PNPME BBLK Surabaya kepada pihak lain</th>
		<th>Bagaimana pendapat Saudara tantang ketepatan pelaksanaan keseluruhan PNPME terhadap jadual yang telah kami infokan</th>
		<th>Bagaimana pendapat Saudara tentang ketepatan jadual pengiriman bahan uji PNPME</th>
		<th>Bagaimana pendapat Saudara tantang kemasan bahan uji PNPME BBLK Surabaya</th>
		<th>Bagaimana bahan uji PNPME saat datang di tempat Saudara (Sebelum disiapkan sesuai suhu penyimpanan)</th>
		<th>Bagaimana pendapat Saudara tantang kualitas bahan uji PNPME pada saat sebelum dilakukan pengujian atau pemeriksaan</th>
		<th>Bagaimana pendapat Saudara, apakah penjelasan Laporan Akhir atau Evaluasi PNPME sudah lengkap dan jelas</th>
		<th>Bagaimana pendapat Saudara, apakah sesuai rekomendasi yang kami berikan terhadap hasil pemeriksaan peserta</th>
	</tr>
	<?php $no = 1; ?>
	@foreach($data as $val)
	<tr>
		<td>{{$no++}}</td>
		<td>{{$val->name}}</td>
		<td>
			<b>
	 			@if($val->no_1 == 'a') A. Tidak mudah
				@elseif($val->no_1 == 'b')B. Kurang mudah
				@elseif($val->no_1 == 'c')C. Mudah
				@elseif($val->no_1 == 'd')D. Sangat mudah @endif
			</b>
		</td>
		<td>
			<b>
				@if($val->no_2 == 'a') A. Tidak sesuai
				@elseif($val->no_2 == 'b') B. Kurang sesuai
				@elseif($val->no_2 == 'c') C. Sesuai
				@elseif($val->no_2 == 'd') D. Sangat sesuai @endif
			</b>
		</td>
		<td>
			<b>
				@if($val->no_3 == 'a') A. Tidak jelas
				@elseif($val->no_3 == 'b') B. Kurang jelas
				@elseif($val->no_3 == 'c') C. Jelas
				@elseif($val->no_3 == 'd') D. Sangat jelas @endif
			</b>
		</td>
		<td>
			<b>
				@if($val->no_4 == 'a') A. Tidak mudah
				@elseif($val->no_4 == 'b') B. Kurang mudah
				@elseif($val->no_4 == 'c') C. Mudah
				@elseif($val->no_4 == 'd') D. Sangat mudah @endif
			</b>
		</td>
		<td>
			<b>
			 	@if($val->no_5 == 'a')A. Tidak jelas
			 	@elseif($val->no_5 == 'b')B. Kurang jelas
			 	@elseif($val->no_5 == 'c')C. Jelas
			 	@elseif($val->no_5 == 'd')D. Sangat jelas @endif
			 </b>
		</td>
		<td>
			<b>
				@if($val->no_6 == 'a') A. Tidak cepat
				@elseif($val->no_6 == 'b') B. Kurang cepat
				@elseif($val->no_6 == 'c') C. Cepat
				@elseif($val->no_6 == 'd') D. Sangat cepat @endif
			</b>
		</td>
		<td>
			<b>
				@if($val->no_7 == 'a') A. Tidak baik
				@elseif($val->no_7 == 'b') B. Kurang baik
				@elseif($val->no_7 == 'c') C. Baik
				@elseif($val->no_7 == 'd') D. Sangat baik @endif
			</b>
		</td>
		<td>
			<b>
				@if($val->no_8 == 'a') A. Tidak sopan dan ramah
				@elseif($val->no_8 == 'b') B. Kurang sopan dan ramah
				@elseif($val->no_8 == 'c') C. Sopan dan ramah
				@elseif($val->no_8 == 'd') D. Sangat sopan dan ramah @endif
			</b>
		</td>
		<td>
			<b>
			 	@if($val->no_9 == 'a') A. Tidak wajar
			 	@elseif($val->no_9 == 'b') B. Kurang wajar
			 	@elseif($val->no_9 == 'c') C. Wajar
			 	@elseif($val->no_9 == 'd') D. Sangat wajar @endif
			 </b>
		</td>
		<td>
			<b>
				@if($val->no_10 == 'a') A. Tidak sesuai
				@elseif($val->no_10 == 'b') B. Kurang sesuai
				@elseif($val->no_10 == 'c') C. Sesuai
				@elseif($val->no_10 == 'd') D. Sangat sesuai @endif
			</b>
		</td>
		<td>
			<b>
				@if($val->no_11 == 'a') A. Tidak rumit
				@elseif($val->no_11 == 'b') B. Kurang rumit
				@elseif($val->no_11 == 'c') C. Rumit
				@elseif($val->no_11 == 'd') D. Sangat rumit @endif
			</b>
		</td>
		<td>
			<b>
				@if($val->no_12 == 'a') A. Tidak setuju
				@elseif($val->no_12 == 'b') B. Kurang setuju
				@elseif($val->no_12 == 'c') C. Setuju
				@elseif($val->no_12 == 'd') D. Sangat setuju @endif
			</b>
		</td>
		<td>
			<b>
				@if($val->no_13 == 'a') A. Tidak setuju
				@elseif($val->no_13 == 'b') B. Kurang setuju
				@elseif($val->no_13 == 'c') C. Setuju
				@elseif($val->no_13 == 'd') D. Sangat setuju @endif
			</b>
		</td>
		<td>
			<b>
				@if($val->no_14 == 'a') A. Tidak tepat
				@elseif($val->no_14 == 'b') B. Kurang tepat
				@elseif($val->no_14 == 'c') C. Tepat
				@elseif($val->no_14 == 'd') D. Sangat tepat @endif
			</b>
		</td>
		<td>
			<b>
				@if($val->no_15 == 'a') A. Sangat terlambat
				@elseif($val->no_15 == 'b') B. Terlambat
				@elseif($val->no_15 == 'c') C. Tepat waktu
				@elseif($val->no_15 == 'd') D. Sangat Cepat @endif
			</b>
		</td>
		<td>
			<b>
				@if($val->no_16 == 'a') A. Tidak baik
				@elseif($val->no_16 == 'b') B. Kurang baik
				@elseif($val->no_16 == 'c') C. Baik
				@elseif($val->no_16 == 'd') D. Sangat baik @endif
			</b>
		</td>
		<td>
		 	<b>
		 		@if($val->no_17 == 'a') A. Tidak baik, rusak berat
			 	@elseif($val->no_17 == 'b') B. Kurang baik
			 	@elseif($val->no_17 == 'c') C. baik
			 	@elseif($val->no_17 == 'd') D. Sangat baik @endif
			</b>
		</td>
		<td>
			<b>
			 	@if($val->no_18 == 'a') A. Tidak baik
			 	@elseif($val->no_18 == 'b') B. Kurang baik
			 	@elseif($val->no_18 == 'c') C. Baik
			 	@elseif($val->no_18 == 'd') D. Sangat baik @endif 
			</b>
		</td>
		<td>
			<b>
			 	@if($val->no_19 == 'a') A. Tidak lengkap dan jelas
			 	@elseif($val->no_19 == 'b') B. Kurang lengkap dan jelas
			 	@elseif($val->no_19 == 'c') C. Lengkap dan jelas
			 	@elseif($val->no_19 == 'd') D. Sangat lengkap dan jelas @endif
			</b>
		</td>
		<td>
			<b>
			 	@if($val->no_20 == 'a') A. Tidak sesuai
			 	@elseif($val->no_20 == 'b') B. Kurang sesuai
			 	@elseif($val->no_20 == 'c') C. Sesuai
			 	@elseif($val->no_20 == 'd') D. Sangat sesuai @endif
			</b>
		</td>
	</tr>
	@endforeach
</table>
<style type="text/css">
@page {
margin: 50px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 12px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 5px;
    text-align: left;
    border: 1px solid #ddd;
    vertical-align: top;
}
.dot {
    height: 10px;
    width: 10px;
    background-color: #333;
    display: inline-block;
}
.tidak {
    height: 10px;
    width: 10px;
    display: inline-block;
}
</style>
<center><h3><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL (PNPME) <br>
    SIKLUS {{$pendapat->siklus}} TAHUN {{$tahun}}</b></h3></center><br>
<table>
    <tr>
        <th style="text-align: center;" colspan="2">DATA PESERTA (RESPONDEN)</th>
    </tr>
    <tr>    
        <td>Nama Instansi :</td>
        <td>
            {{$pendapat->nama_lab}}
        </td>
    </tr>
</table>
<center><h4><b>HASIL SURVEY KEPUASAN PELANGGAN TENTANG PELAYANAN PUBLIK</b><br><i>(Pilih salah satu huruf sesuai jawaban responden)</i></h4></center><br>
<table class="utama">
    <tr>
        <th>No</th>
        <th>Parameter</th>
        <th>No</th>
        <th>Parameter</th>
    </tr>
    <tr>
        <td>1.</td>
        <td>
            Bagaimana pemahaman Saudara tentang kemudahan prosedur pelayanan PNPME<br>
            <i class="{{ ($pendapat->no_1 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak mudah<br>
            <i class="{{ ($pendapat->no_1 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang mudah<br>
            <i class="{{ ($pendapat->no_1 == 'c') ? 'dot' : 'tidak' }}"></i> C. Mudah<br>
            <i class="{{ ($pendapat->no_1 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat mudah
        </td>
        <td>2.</td>
        <td>
            Bagaimana pendapat Saudara tentang kesesuaian persyaratan pelayanan dengan penerapannya saat mengikuti PNPME<br>
            <i class="{{ ($pendapat->no_2 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak sesuai<br>
            <i class="{{ ($pendapat->no_2 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang sesuai<br>
            <i class="{{ ($pendapat->no_2 == 'c') ? 'dot' : 'tidak' }}"></i> C. Sesuai<br>
            <i class="{{ ($pendapat->no_2 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat sesuai
        </td>
    </tr>
    <tr>
        <td>3.</td>
        <td>
            Bagaimana pendapat Saudara tentang kejelasan informasi kegiatan PNPME di BBLK Surabaya secara umum<br>
            <i class="{{ ($pendapat->no_3 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak jelas<br>
            <i class="{{ ($pendapat->no_3 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang jelas<br>
            <i class="{{ ($pendapat->no_3 == 'c') ? 'dot' : 'tidak' }}"></i> C. Jelas<br>
            <i class="{{ ($pendapat->no_3 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat jelas
        </td>
        <td>4.</td>
        <td>
            Bagaimana pendapat Saudara dalam kemudahan melengkapi administrasi kegiatan PNPME<br>
            <i class="{{ ($pendapat->no_4 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak mudah<br>
            <i class="{{ ($pendapat->no_4 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang mudah<br>
            <i class="{{ ($pendapat->no_4 == 'c') ? 'dot' : 'tidak' }}"></i> C. Mudah<br>
            <i class="{{ ($pendapat->no_4 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat mudah
        </td>
    </tr>
    <tr>
        <td>5.</td>
        <td>
            Bagaimana pendapat Saudara tentang penjelasan petugas dalam mengikuti kegiatan PNPME<br>
            <i class="{{ ($pendapat->no_5 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak jelas<br>
            <i class="{{ ($pendapat->no_5 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang jelas<br>
            <i class="{{ ($pendapat->no_5 == 'c') ? 'dot' : 'tidak' }}"></i> C. Jelas<br>
            <i class="{{ ($pendapat->no_5 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat jelas
        </td>
        <td>6.</td>
        <td>
            Bagaimana pendapat Saudara tantang kecepatan petugas dalam menjawab atau merespon permasalahan kegiatan PNPME<br>
            <i class="{{ ($pendapat->no_6 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak cepat<br>
            <i class="{{ ($pendapat->no_6 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang cepat<br>
            <i class="{{ ($pendapat->no_6 == 'c') ? 'dot' : 'tidak' }}"></i> C. Cepat<br>
            <i class="{{ ($pendapat->no_6 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat cepat
        </td>
    </tr>
    <tr>
        <td>7.</td>
        <td>
            Bagaimana pendapat Saudara tentang kegiatan PNPME kami secara umum<br>
            <i class="{{ ($pendapat->no_7 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak baik<br>
            <i class="{{ ($pendapat->no_7 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang baik<br>
            <i class="{{ ($pendapat->no_7 == 'c') ? 'dot' : 'tidak' }}"></i> C. Baik<br>
            <i class="{{ ($pendapat->no_7 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat baik
        </td>
        <td>8.</td>
        <td>
            Bagaimana pendapat Saudara tantang kesopanan dan keramahan petugas dalam memberikan pelayanan<br>
            <i class="{{ ($pendapat->no_8 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak sopan dan ramah<br>
            <i class="{{ ($pendapat->no_8 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang sopan dan ramah<br>
            <i class="{{ ($pendapat->no_8 == 'c') ? 'dot' : 'tidak' }}"></i> C. Sopan dan ramah<br>
            <i class="{{ ($pendapat->no_8 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat sopan dan ramah
        </td>
    </tr>
    <tr>
        <td>9.</td>
        <td>
            Bagaimana pendapat Saudara tentang kewajaran biaya untuk mendapatkan pelayanan<br>
            <i class="{{ ($pendapat->no_9 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak wajar<br>
            <i class="{{ ($pendapat->no_9 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang wajar<br>
            <i class="{{ ($pendapat->no_9 == 'c') ? 'dot' : 'tidak' }}"></i> C. Wajar<br>
            <i class="{{ ($pendapat->no_9 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat wajar
        </td>
        <td>10.</td>
        <td>
            Bagaimana pendapat Saudara tantang kesesuaian antara biaya yang dibayarkan dengan biaya yang telah di tetapkan<br>
            <i class="{{ ($pendapat->no_10 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak sesuai<br>
            <i class="{{ ($pendapat->no_10 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang sesuai<br>
            <i class="{{ ($pendapat->no_10 == 'c') ? 'dot' : 'tidak' }}"></i> C. Sesuai<br>
            <i class="{{ ($pendapat->no_10 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat sesuai
        </td>
    </tr>
    <tr>
        <td>11.</td>
        <td>
            Bagaimana pendapat Saudara tentang metode pembayaran yang kami terapkan<br>
            <i class="{{ ($pendapat->no_11 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak rumit<br>
            <i class="{{ ($pendapat->no_11 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang rumit<br>
            <i class="{{ ($pendapat->no_11 == 'c') ? 'dot' : 'tidak' }}"></i> C. Rumit<br>
            <i class="{{ ($pendapat->no_11 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat rumit
        </td>
        <td>12.</td>
        <td>
            Bagaimana pendapat Saudara tantang rencana program meningkatkan jumlah peserta maupun jumlah parameter<br>
            <i class="{{ ($pendapat->no_12 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak setuju<br>
            <i class="{{ ($pendapat->no_12 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang setuju<br>
            <i class="{{ ($pendapat->no_12 == 'c') ? 'dot' : 'tidak' }}"></i> C. Setuju<br>
            <i class="{{ ($pendapat->no_12 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat setuju
        </td>
    </tr>
    <tr>
        <td>13.</td>
        <td>
            Bagaimana pendapat Saudara tentang merekomendasikan PNPME BBLK Surabaya kepada pihak lain<br>
            <i class="{{ ($pendapat->no_13 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak setuju<br>
            <i class="{{ ($pendapat->no_13 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang setuju<br>
            <i class="{{ ($pendapat->no_13 == 'c') ? 'dot' : 'tidak' }}"></i> C. Setuju<br>
            <i class="{{ ($pendapat->no_13 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat setuju
        </td>
        <td>14.</td>
        <td>
            Bagaimana pendapat Saudara tantang ketepatan pelaksanaan keseluruhan PNPME terhadap jadual yang telah kami infokan<br>
            <i class="{{ ($pendapat->no_14 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak tepat<br>
            <i class="{{ ($pendapat->no_14 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang tepat<br>
            <i class="{{ ($pendapat->no_14 == 'c') ? 'dot' : 'tidak' }}"></i> C. Tepat<br>
            <i class="{{ ($pendapat->no_14 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat tepat
        </td>
    </tr>
    <tr>
        <td>15.</td>
        <td>
            Bagaimana pendapat Saudara tentang ketepatan jadual pengiriman bahan uji PNPME<br>
            <i class="{{ ($pendapat->no_15 == 'a') ? 'dot' : 'tidak' }}"></i> A. Sangat terlambat<br>
            <i class="{{ ($pendapat->no_15 == 'b') ? 'dot' : 'tidak' }}"></i> B. Terlambat<br>
            <i class="{{ ($pendapat->no_15 == 'c') ? 'dot' : 'tidak' }}"></i> C. Tepat waktu<br>
            <i class="{{ ($pendapat->no_15 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat Cepat
        </td>
        <td>16.</td>
        <td>
            Bagaimana pendapat Saudara tantang kemasan bahan uji PNPME BBLK Surabaya<br>
            <i class="{{ ($pendapat->no_16 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak baik<br>
            <i class="{{ ($pendapat->no_16 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang baik<br>
            <i class="{{ ($pendapat->no_16 == 'c') ? 'dot' : 'tidak' }}"></i> C. Baik<br>
            <i class="{{ ($pendapat->no_16 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat baik
        </td>
    </tr>
    <tr>
        <td>17.</td>
        <td>
            Bagaimana bahan uji PNPME saat datang di tempat Saudara (Sebelum disiapkan sesuai suhu penyimpanan)<br>
            <i class="{{ ($pendapat->no_17 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak baik, rusak berat<br>
            <i class="{{ ($pendapat->no_17 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang baik<br>
            <i class="{{ ($pendapat->no_17 == 'c') ? 'dot' : 'tidak' }}"></i> C. baik<br>
            <i class="{{ ($pendapat->no_17 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat baik
        </td>
        <td>18.</td>
        <td>
            Bagaimana pendapat Saudara tantang kualitas bahan uji PNPME pada saat sebelum dilakukan pengujian atau pemeriksaan<br>
            <i class="{{ ($pendapat->no_18 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak baik<br>
            <i class="{{ ($pendapat->no_18 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang baik<br>
            <i class="{{ ($pendapat->no_18 == 'c') ? 'dot' : 'tidak' }}"></i> C. Baik<br>
            <i class="{{ ($pendapat->no_18 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat baik
        </td>
    </tr>
    <tr>
        <td>19.</td>
        <td>
            Bagaimana pendapat Saudara, apakah penjelasan Laporan Akhir atau Evaluasi PNPME sudah lengkap dan jelas<br>
            <i class="{{ ($pendapat->no_19 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak lengkap dan jelas<br>
            <i class="{{ ($pendapat->no_19 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang lengkap dan jelas<br>
            <i class="{{ ($pendapat->no_19 == 'c') ? 'dot' : 'tidak' }}"></i> C. Lengkap dan jelas<br>
            <i class="{{ ($pendapat->no_19 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat lengkap dan jelas
        </td>
        <td>20.</td>
        <td>
            Bagaimana pendapat Saudara, apakah sesuai rekomendasi yang kami berikan terhadap hasil pemeriksaan peserta <br>
            <i class="{{ ($pendapat->no_20 == 'a') ? 'dot' : 'tidak' }}"></i> A. Tidak sesuai<br>
            <i class="{{ ($pendapat->no_20 == 'b') ? 'dot' : 'tidak' }}"></i> B. Kurang sesuai<br>
            <i class="{{ ($pendapat->no_20 == 'c') ? 'dot' : 'tidak' }}"></i> C. Sesuai<br>
            <i class="{{ ($pendapat->no_20 == 'd') ? 'dot' : 'tidak' }}"></i> D. Sangat sesuai
        </td>
    </tr>
</table>
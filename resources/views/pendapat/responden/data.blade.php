@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Survey Pelanggan</div>

                <div class="panel-body">
                    @if(Session::has('message'))
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ Session::get('message') }}
                      </div>
                    @endif
                    <form class="form-horizontal" method="get" enctype="multipart/form-data">
                        <table class="table table-bordered">
                            <tr>
                                <th width="10px"><center>No</center></th>
                                <th>Perusahaan</th>
                                <th width="10px"><center>Siklus</center></th>
                                <th width="10px"><center>View</center></th>
                            </tr>
                            @if(count($pendapat))
                            <?php $no = 0;?>
                            @foreach($pendapat as $val)
                            <?php $no++;?>
                            <tr>
                                <td style="text-align: right;">{{$no}}</td>
                                <td>{{$val->nama_lab}}</td>
                                <td><center>{{$val->siklus}}</center></td>
                                <td><a href="{{url('/pendapat/survey-pelanggan/view/')}}/{{$val->id}}"><center>[ <i class="glyphicon glyphicon-print"></i> ]</center></a></td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="4">Data tidak ditemukan</td>
                            </tr>
                            @endif
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(".go-link").on("click",function(e){
        e.preventDefault()
        var x = $(this).data("x");
        var y = $(this).data("y");
        var idLink = $(this).data("id");
        var link = $(this).data("link");
        var tanggal = $("#tanggal").val();
        var baseUrl = "{{URL('')}}"+link+"/sertifikat/print/"+idLink+"?x="+x+"&y="+y+"&tanggal="+tanggal+"&download=true";
        var url = baseUrl;
        console.log(url);
        window.location.href = url;
    })
</script>
@endsection
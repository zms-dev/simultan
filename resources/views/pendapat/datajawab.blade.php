@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <center><h4><b>JAWABAN PENYELENGGARA KELUHAN / SARAN PESERTA PNPME <br>
                        SIKLUS {{$pendapat->siklus}} TAHUN {{$pendapat->tahun}}</b></h4></center><br>
                    <table class="table">
                        <tr>    
                            <td width="40%">Nama Instansi</td>
                            <td>:
                                {{$perusahaan->nama_lab}}
                            </td>
                        </tr>
                        <tr>    
                            <td>Alamat Instansi</td>
                            <td>:
                                {{$perusahaan->alamat}}
                            </td>
                        </tr>
                        <tr>    
                            <td>Nama Penanggung Jawab Laboratorium</td>
                            <td>:
                                {{$perusahaan->penanggung_jawab}}
                            </td>
                        </tr>
                        <tr>    
                            <td>No. HP / Telp. Penanggung Jawab Laboratorium</td>
                            <td>:
                                {{$perusahaan->no_hp}}
                            </td>
                        </tr>
                        <tr>    
                            <td>Email</td>
                            <td>:
                                {{$perusahaan->email}}
                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <th>Parameter</th>
                            <th><center>Keluhan / Saran</center></th>
                        </tr>
                        <tr>
                            <td>
                                {!!$pendapat->parameter!!}
                            </td>
                            <td>
                                {!!$pendapat->saran!!}
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">Jawaban</th>
                        </tr>
                        <tr>
                            <td colspan="2">
                                {!!$pendapat->jawaban!!}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

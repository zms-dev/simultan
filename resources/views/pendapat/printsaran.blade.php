<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;
}


table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}
</style>
<center><h4><b>KELUHAN / SARAN PESERTA PNPME <br>
    SIKLUS {{$pendapat->siklus}} TAHUN {{$pendapat->tahun}}</b></h4></center><br>
<table class="table">
    <tr>    
        <td width="40%">Nama Instansi</td>
        <td>:
            {{$perusahaan->nama_lab}}
        </td>
    </tr>
    <tr>    
        <td>Alamat Instansi</td>
        <td>:
            {{$perusahaan->alamat}}
        </td>
    </tr>
    <tr>    
        <td>Nama Penanggung Jawab Laboratorium</td>
        <td>:
            {{$perusahaan->penanggung_jawab}}
        </td>
    </tr>
    <tr>    
        <td>No. HP / Telp. Penanggung Jawab Laboratorium</td>
        <td>:
            {{$perusahaan->no_hp}}
        </td>
    </tr>
    <tr>    
        <td>Email</td>
        <td>:
            {{$perusahaan->email}}
        </td>
    </tr>
</table>
<br>
<table id="peserta_pme">
    <tr>
        <th>Parameter</th>
        <th><center>Keluhan / Saran</center></th>
    </tr>
    <tr>
        <td>
            {!!$pendapat->parameter!!}
        </td>
        <td>
            {!!$pendapat->saran!!}
        </td>
    </tr>
    <tr>
        <th colspan="2"><center>Jawaban</center></th>
    </tr>
    <tr>
        <td colspan="2">
            {!!$pendapat->jawaban!!}
        </td>
    </tr>
</table>
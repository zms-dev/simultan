<table border="1">
	<tr>
		<th>No</th>
		<th>Nama Instansi</th>
		<th>Parameter</th>
		<th>Saran</th>
		<th>Jawaban</th>
	</tr>
	<?php $no = 1; ?>
	@foreach($data as $val)
	<tr>
		<td>{{$no++}}</td>
		<td>{{$val->name}}</td>
		<td>{!!$val->parameter!!}</td>
		<td>{!!$val->saran!!}</td>
		<td>{!!$val->jawaban!!}</td>
	</tr>
	@endforeach
</table>
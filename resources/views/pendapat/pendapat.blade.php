@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ Session::get('message') }}
                    </p>
                @endif
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><h4><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL (PNPME) <br>
                        SIKLUS 
                        <select name="siklus" required>
                            <option></option>
                            <option value="1" class="1">1</option>
                            <option value="2" class="2">2</option>
                        </select> 
                        TAHUN 
                        <select name="tahun" required>
                            <option></option>
                            @foreach($data as $tahun)
                                <option value="{{$tahun->tahun}}" class="{{$tahun->tahun}}">{{$tahun->tahun}}</option>
                            @endforeach
                        </select></b></h4></center><br>
                    <table class="table-bordered table">
                        <tr>
                            <th style="text-align: center;" colspan="2">DATA PESERTA (RESPONDEN)</th>
                        </tr>
                        <tr>    
                           
                            <td>
                                <input size="16" type="text" value="{{$perusahaan->nama_lab}}" readonly class="form-control" name="nama_lab">
                            </td>
                        </tr>
                    </table>
                    <center><h5><b>SURVEY KEPUASAN PELANGGAN TENTANG PELAYANAN PUBLIK</b><br><i>(Pilih salah satu huruf sesuai jawaban responden)</i></h5></center><br>
                    <table class="table-bordered table">
                        <tr>
                            <th>No</th>
                            <th>Pertanyaan</th>
                            <th>No</th>
                            <th>Pertanyaan</th>
                        </tr>
                        <tr>
                            <td>1.</td>
                            <td>
                                Bagaimana pemahaman Saudara tentang kemudahan prosedur pelayanan PNPME
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_1" value="a" required> A. Tidak mudah
                                            &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_1" value="b"> B. Kurang mudah</td>   
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_1" value="c"> C. Mudah</td>
                                        <td><input type="radio" name="no_1" value="d"> D. Sangat mudah</td>
                                    </tr>
                                </table>
                            </td>
                            <td>2.</td>
                            <td>
                                Bagaimana pendapat Saudara tentang kesesuaian persyaratan pelayanan dengan penerapannya saat mengikuti PNPME<br>
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_2" value="a" required> A. Tidak sesuai
                                         &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_2" value="b"> B. Kurang sesuai<br></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_2" value="c"> C. Sesuai</td>
                                        <td><input type="radio" name="no_2" value="d"> D. Sangat sesuai</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>3.</td>
                            <td>
                                Bagaimana pendapat Saudara tentang kejelasan informasi kegiatan PNPME di BBLK Surabaya secara umum<br>
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_3" value="a" required> A. Tidak jelas &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_3" value="b"> B. Kurang jelas<br></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_3" value="c"> C. Jelas</td>
                                        <td><input type="radio" name="no_3" value="d"> D. Sangat jelas</td>
                                    </tr>
                                </table>
                            </td>
                            <td>4.</td>
                            <td>Bagaimana pendapat Saudara dalam kemudahan melengkapi administrasi kegiatan PNPME<br>
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_4" value="a" required> A. Tidak mudah &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_4" value="b"> B. Kurang mudah<br></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_4" value="c"> C. Mudah</td>
                                        <td><input type="radio" name="no_4" value="d"> D. Sangat mudah</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>5.</td>
                            <td>
                                Bagaimana pendapat Saudara tentang penjelasan petugas dalam mengikuti kegiatan PNPME<br>
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_5" value="a" required> A. Tidak jelas &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_5" value="b"> B. Kurang jelas<br></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_5" value="c"> C. Jelas</td>
                                        <td><input type="radio" name="no_5" value="d"> D. Sangat jelas</td>
                                    </tr>
                                </table>
                            </td>
                            <td>6.</td>
                            <td>
                                 Bagaimana pendapat Saudara tantang kecepatan petugas dalam menjawab atau merespon permasalahan kegiatan PNPME<br>
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_6" value="a" required> A. Tidak cepat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_6" value="b"> B. Kurang cepat<br></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_6" value="c"> C. Cepat</td>
                                        <td><input type="radio" name="no_6" value="d"> D. Sangat cepat</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>7.</td>
                            <td>
                                Bagaimana pendapat Saudara tentang kegiatan PNPME kami secara umum<br>
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_7" value="a" required> A. Tidak baik &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_7" value="b"> B. Kurang baik<br></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_7" value="c"> C. Baik</td>
                                        <td><input type="radio" name="no_7" value="d"> D. Sangat baik</td>
                                    </tr>
                                </table>
                            </td>
                            <td>8.</td>
                            <td> Bagaimana pendapat Saudara tantang kesopanan dan keramahan petugas dalam memberikan pelayanan<br>
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_8" value="a" required> A. Tidak sopan dan ramah  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_8" value="b"> B. Kurang sopan dan ramah<br></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_8" value="c"> C. Sopan dan ramah</td>
                                        <td><input type="radio" name="no_8" value="d"> D. Sangat sopan dan ramah</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>9.</td>
                            <td>Bagaimana pendapat Saudara tentang kewajaran biaya untuk mendapatkan pelayanan<br>
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_9" value="a" required> A. Tidak wajar &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_9" value="b"> B. Kurang wajar<br></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_9" value="c"> C. Wajar</td>
                                        <td> <input type="radio" name="no_9" value="d"> D. Sangat wajar</td>
                                    </tr>
                                </table>
                            </td>
                            <td>10.</td>
                            <td>
                                Bagaimana pendapat Saudara tantang kesesuaian antara biaya yang dibayarkan dengan biaya yang telah di tetapkan<br>
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_10" value="a" required> A. Tidak sesuai &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td> <input type="radio" name="no_10" value="b"> B. Kurang sesuai<br></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_10" value="c"> C. Sesuai</td>
                                        <td><input type="radio" name="no_10" value="d"> D. Sangat sesuai</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>11.</td>
                            <td>
                                Bagaimana pendapat Saudara tentang metode pembayaran yang kami terapkan<br>
                                <table>
                                    <tr>
                                        <td> <input type="radio" name="no_11" value="a" required> A. Tidak rumit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_11" value="b"> B. Kurang rumit<br></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_11" value="c"> C. Rumit</td>
                                        <td><input type="radio" name="no_11" value="d"> D. Sangat rumit</td>
                                    </tr>
                                </table>
                            </td>
                            <td>12.</td>
                            <td> Bagaimana pendapat Saudara tantang rencana program meningkatkan jumlah peserta maupun jumlah parameter<br>
                                <table>
                                    <tr>
                                        <td>  <input type="radio" name="no_12" value="a" required> A. Tidak setuju &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_12" value="b"> B. Kurang setuju<br></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_12" value="c"> C. Setuju</td>
                                        <td><input type="radio" name="no_12" value="d"> D. Sangat setuju</td>
                                    </tr>
                                </table>
                                
                            </td>
                        </tr>
                        <tr>
                            <td>13.</td>
                            <td>Bagaimana pendapat Saudara tentang merekomendasikan PNPME BBLK Surabaya kepada pihak lain<br>
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_13" value="a" required> A. Tidak setuju &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_13" value="b"> B. Kurang setuju<br></td>
                                    </tr>
                                    <tr>
                                        <td> <input type="radio" name="no_13" value="c"> C. Setuju</td>
                                        <td><input type="radio" name="no_13" value="d"> D. Sangat setuju</td>
                                    </tr>
                                </table>
                            </td>
                            <td>14.</td>
                            <td> Bagaimana pendapat Saudara tantang ketepatan pelaksanaan keseluruhan PNPME terhadap jadual yang telah kami infokan<br>
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_14" value="a" required> A. Tidak tepat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_14" value="b"> B. Kurang tepat<br></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_14" value="c"> C. Tepat</td>
                                        <td> <input type="radio" name="no_14" value="d"> D. Sangat tepat</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>15.</td>
                            <td>
                                Bagaimana pendapat Saudara tentang ketepatan jadual pengiriman bahan uji PNPME<br>
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_15" value="a" required> A. Sangat terlambat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_15" value="b"> B. Terlambat<br></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_15" value="c"> C. Tepat waktu</td>
                                        <td><input type="radio" name="no_15" value="d"> D. Sangat Cepat</td>
                                    </tr>
                                </table>                                
                            </td>
                            <td>16.</td>
                            <td> Bagaimana pendapat Saudara tantang kemasan bahan uji PNPME BBLK Surabaya<br>
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_16" value="a" required> A. Tidak baik &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_16" value="b"> B. Kurang baik<br></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_16" value="c"> C. Baik</td>
                                        <td><input type="radio" name="no_16" value="d"> D. Sangat baik</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>17.</td>
                            <td>Bagaimana bahan uji PNPME saat datang di tempat Saudara (Sebelum disiapkan sesuai suhu penyimpanan)<br>
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_17" value="a" required> A. Tidak baik, rusak berat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_17" value="b"> B. Kurang baik<br></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_17" value="c"> C. baik</td>
                                        <td><input type="radio" name="no_17" value="d"> D. Sangat baik</td>
                                    </tr>
                                </table>
                            </td>
                            <td>18.</td>
                            <td>Bagaimana pendapat Saudara tantang kualitas bahan uji PNPME pada saat sebelum dilakukan pengujian atau pemeriksaan<br>
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_18" value="a" required> A. Tidak baik &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_18" value="b"> B. Kurang baik<br></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_18" value="c"> C. Baik</td>
                                        <td> <input type="radio" name="no_18" value="d"> D. Sangat baik</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>19.</td>
                            <td>
                                Bagaimana pendapat Saudara, apakah penjelasan Laporan Akhir atau Evaluasi PNPME sudah lengkap dan jelas<br>
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_19" value="a" required> A. Tidak lengkap dan jelas &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_19" value="b"> B. Kurang lengkap dan jelas<br></td>
                                    </tr>
                                    <tr>
                                        <td> <input type="radio" name="no_19" value="c"> C. Lengkap dan jelas &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td><input type="radio" name="no_19" value="d"> D. Sangat lengkap dan jelas</td>
                                    </tr>
                                </table>
                            </td>
                            <td>20.</td>
                            <td>
                                Bagaimana pendapat Saudara, apakah sesuai rekomendasi yang kami berikan terhadap hasil pemeriksaan peserta <br>
                                <table>
                                    <tr>
                                        <td><input type="radio" name="no_20" value="a" required> A. Tidak sesuai &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td> <input type="radio" name="no_20" value="b"> B. Kurang sesuai<br></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" name="no_20" value="c"> C. Sesuai</td>
                                        <td><input type="radio" name="no_20" value="d"> D. Sangat sesuai</td>
                                    </tr>
                                </table>
                                
                            </td>
                        </tr>
                    </table>
                      {{ csrf_field() }}
                    <input type="submit" name="simpan" value="Kirim" class="btn btn-submit">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<h2>Pengiriman Bahan Kontrol Siklus {{$siklus}}</h2>
<p>Nama Lab : {{$labklinik}} </p>
<p>Kami telah mengirimkan bahan kontrol dengan, Bidang Parameter :<br>@foreach($parameter as $par) {{$par->parameter}}<br> @endforeach</p>
<p>
Mohon untuk berkoordinasi dengan bagian penerimaan paket/dokumen masuk di Instansi Bapak/Ibu, dan mohon untuk segera menyimpan sampel sesuai dengan Juklak yang kami upload.
</p>
<p>
Regards,<br>
Penyelenggara PNPME
BBLK Surabaya
</p>
@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Persentase Survey Kepuasan Pelanggan</div>
                <div class="panel-body">
                	<?php $no =0;?>
					@foreach($data as $val)
					<?php $no++ ;?>
                	<div class="col-md-4">
                	<div id="container{{$no}}"></div>
                	</div>
                	@endforeach
                </div>
            </div>	
        </div>
    </div>
</div>
<script type="text/javascript">
<?php $no =0;?>
@foreach($data as $val)
<?php 
$no++;
$soal = ['','Bagaimana pemahaman Saudara tentang kemudahan prosedur pelayanan PNPME',
        'Bagaimana pendapat Saudara tentang kesesuaian persyaratan pelayanan dengan penerapannya saat mengikuti PNPME',
        'Bagaimana pendapat Saudara tentang kejelasan informasi kegiatan PNPME di BBLK Surabaya secara umum',
        'Bagaimana pendapat Saudara dalam kemudahan melengkapi administrasi kegiatan PNPME',
        'Bagaimana pendapat Saudara tentang penjelasan petugas dalam mengikuti kegiatan PNPME',
        'Bagaimana pendapat Saudara tantang kecepatan petugas dalam menjawab atau merespon permasalahan kegiatan PNPME',
        'Bagaimana pendapat Saudara tentang kegiatan PNPME kami secara umum',
        'Bagaimana pendapat Saudara tantang kesopanan dan keramahan petugas dalam memberikan pelayanan',
        'Bagaimana pendapat Saudara tentang kewajaran biaya untuk mendapatkan pelayanan',
        'Bagaimana pendapat Saudara tantang kesesuaian antara biaya yang dibayarkan dengan biaya yang telah di tetapkan',
        'Bagaimana pendapat Saudara tentang metode pembayaran yang kami terapkan',
        'Bagaimana pendapat Saudara tantang rencana program meningkatkan jumlah peserta maupun jumlah parameter',
        'Bagaimana pendapat Saudara tentang merekomendasikan PNPME BBLK Surabaya kepada pihak lain',
        'Bagaimana pendapat Saudara tantang ketepatan pelaksanaan keseluruhan PNPME terhadap jadual yang telah kami infokan',
        'Bagaimana pendapat Saudara tentang ketepatan jadual pengiriman bahan uji PNPME',
        'Bagaimana pendapat Saudara tantang kemasan bahan uji PNPME BBLK Surabaya',
        'Bagaimana bahan uji PNPME saat datang di tempat Saudara (Sebelum disiapkan sesuai suhu penyimpanan)',
        'Bagaimana pendapat Saudara tantang kualitas bahan uji PNPME pada saat sebelum dilakukan pengujian atau pemeriksaan',
        'Bagaimana pendapat Saudara, apakah penjelasan Laporan Akhir atau Evaluasi PNPME sudah lengkap dan jelas',
        'Bagaimana pendapat Saudara, apakah sesuai rekomendasi yang kami berikan terhadap hasil pemeriksaan peserta'];

$no1 = ['Tidak mudah','Kurang Mudah','Mudah','Sangat Mudah'];
$no2 = ['Tidak sesuai','Kurang Sesuai','Sesuai','Sa'];

?>

Highcharts.chart('container{{$no}}', {
    chart: {
        // plotBackgroundColor: null,
        // plotBorderWidth: null,
        // plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Persentase Survey Pelanggan No {{$no}}. {{$soal[$no]}} <br>Siklus {{$input["siklus"]}} Tahun {{$input["tahun"]}}',
        style: {
            fontSize: '12px'
        }
    },
    tooltip: {
        pointFormat: 'Menjawab {point.name} : <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            size:'80%',
            cursor: 'pointer',
            animation: true,
            showInLegend: true,
            dataLabels: {
                enabled: false,
                // distance: -30,
                // format: '<b style="text-transform: uppercase;">{point.name}</b> : {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            } 
        }
    },
    legend: {
        labelFormatter: function() {
            return  this.name + " (" + Highcharts.numberFormat(this.percentage,1) + "%)";
        }
    },

    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [
        @foreach($val as $vul)
            // no 1
            @if($no == '1' || $no == '4' AND $vul->no == 'a')
        		{ 
		            name: 'Tidak mudah',
		            y: {{$vul->jumlah}},
		        },
            @elseif($no == '1' || $no == '4' AND $vul->no == 'b')
                { 
                    name: 'Kurang mudah',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '1' || $no == '4' AND $vul->no == 'c')
                { 
                    name: 'Mudah',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '1' || $no == '4' AND $vul->no == 'd')
                { 
                    name: 'Sangat mudah',
                    y: {{$vul->jumlah}},
                },
            // no 2
            @elseif($no == '2' || $no == '10' || $no == '20' AND $vul->no == 'a')
                { 
                    name: 'Tidak sesuai',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '2' || $no == '10' || $no == '20' AND $vul->no == 'b')
                { 
                    name: 'Kurang sesuai',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '2' || $no == '10' || $no == '20' AND $vul->no == 'c')
                { 
                    name: 'Sesuai',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '2' || $no == '10' || $no == '20' AND $vul->no == 'd')
                { 
                    name: 'Sangat jelas',
                    y: {{$vul->jumlah}},
                },
            // no 3
            @elseif($no == '3' || $no == '5' AND $vul->no == 'a')
                { 
                    name: 'Tidak jelas',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '3' || $no == '5' AND $vul->no == 'b')
                { 
                    name: 'Kurang jelas',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '3' || $no == '5' AND $vul->no == 'c')
                { 
                    name: 'Jelas',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '3' || $no == '5' AND $vul->no == 'd')
                { 
                    name: 'Sangat jelas',
                    y: {{$vul->jumlah}},
                },
            // no 4
            @elseif($no == '6' AND $vul->no == 'a')
                { 
                    name: 'Tidak cepat',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '6' AND $vul->no == 'b')
                { 
                    name: 'Kurang cepat',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '6' AND $vul->no == 'c')
                { 
                    name: 'Cepat',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '6' AND $vul->no == 'd')
                { 
                    name: 'Sangat cepat',
                    y: {{$vul->jumlah}},
                },
            // no 5
            @elseif($no == '7' || $no == '16' || $no == '18' AND $vul->no == 'a')
                { 
                    name: 'Tidak Baik',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '7' || $no == '16' || $no == '18' AND $vul->no == 'b')
                { 
                    name: 'Kurang Baik',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '7' || $no == '16' || $no == '18' AND $vul->no == 'c')
                { 
                    name: 'Baik',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '7' || $no == '16' || $no == '18' AND $vul->no == 'd')
                { 
                    name: 'Sangat Baik',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '8' AND $vul->no == 'a')
                { 
                    name: 'Tidak sopan dan ramah',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '8' AND $vul->no == 'b')
                { 
                    name: 'Kurang sopan dan ramah',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '8' AND $vul->no == 'c')
                { 
                    name: 'Sopan dan ramah',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '8' AND $vul->no == 'd')
                { 
                    name: 'Sangat sopan dan ramah',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '9' AND $vul->no == 'a')
                { 
                    name: 'Tidak wajar',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '9' AND $vul->no == 'b')
                { 
                    name: 'Kurang wajar',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '9' AND $vul->no == 'c')
                { 
                    name: 'Wajar',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '9' AND $vul->no == 'd')
                { 
                    name: 'Sangat wajar',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '11' AND $vul->no == 'a')
                { 
                    name: 'Tidak rumit',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '11' AND $vul->no == 'b')
                { 
                    name: 'Kurang rumit',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '11' AND $vul->no == 'c')
                { 
                    name: 'Rumit',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '11' AND $vul->no == 'd')
                { 
                    name: 'Sangat rumit',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '12' || $no == '13' AND $vul->no == 'a')
                { 
                    name: 'Tidak setuju',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '12' || $no == '13' AND $vul->no == 'b')
                { 
                    name: 'Kurang setuju',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '12' || $no == '13' AND $vul->no == 'c')
                { 
                    name: 'Setuju',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '12' || $no == '13' AND $vul->no == 'd')
                { 
                    name: 'Sangat setuju',
                    y: {{$vul->jumlah}},
                },

            @elseif($no == '14'  AND $vul->no == 'a')
                { 
                    name: 'Tidak tepat',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '14'  AND $vul->no == 'b')
                { 
                    name: 'Kurang tepat',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '14'  AND $vul->no == 'c')
                { 
                    name: 'Tepat',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '14'  AND $vul->no == 'd')
                { 
                    name: 'Sangat tepat',
                    y: {{$vul->jumlah}},
                },

            @elseif($no == '15'  AND $vul->no == 'a')
                { 
                    name: 'Sangat terlambat',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '15'  AND $vul->no == 'b')
                { 
                    name: 'Terlambat',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '15'  AND $vul->no == 'c')
                { 
                    name: 'Tepat waktu',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '15'  AND $vul->no == 'd')
                { 
                    name: 'Sangat cepat',
                    y: {{$vul->jumlah}},
                },

            @elseif($no == '17'  AND $vul->no == 'a')
                { 
                    name: 'Tidak baik, rusak berat',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '17'  AND $vul->no == 'b')
                { 
                    name: 'Kurang baik',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '17'  AND $vul->no == 'c')
                { 
                    name: 'Baik',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '17'  AND $vul->no == 'd')
                { 
                    name: 'Sangat baik',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '19'  AND $vul->no == 'a')
                { 
                    name: 'Tidak lengkap dan jelas',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '19'  AND $vul->no == 'b')
                { 
                    name: 'Kurang lengkap dan jelas',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '19'  AND $vul->no == 'c')
                { 
                    name: 'Lengkap dan jelas',
                    y: {{$vul->jumlah}},
                },
            @elseif($no == '19'  AND $vul->no == 'd')
                { 
                    name: 'Sangat lengkap dan jelas',
                    y: {{$vul->jumlah}},
                },
            @else
                { 
                    name: '{{$vul->no}}',
                    y: {{$vul->jumlah}},
                },
            @endif
		@endforeach
        ]
    }]
});
@endforeach
</script>
@endsection
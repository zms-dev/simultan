@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Laporan Hasil PNPME</div>
                <a style="margin: 10px" onclick="javascript:printDiv('datana')" class="btn btn-primary">Print</a>
                <div class="panel-body" id="datana">
                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto;"></div>
                </div>
            </div>	
        </div>
    </div>
<script src="{{URL('/js/jquery.printarea.js')}}"></script>
<script src="https://code.highcharts.com/modules/pattern-fill.js"></script>
<script type="text/javascript">
function printDiv(divID) {
    var headElements = '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>,<meta name="viewport" content="width=device-width, initial-scale=1">';
    var options = { mode : "popup", popClose : true,extraHead : headElements };
    $( '#'+divID ).printArea( options );    
}

Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: "Hasil Urinalisa Siklus {{$input['siklus']}} Tahun {{$input['tahun']}}",
        style: {
            textTransform: 'capitalize'
        }
    },
    subtitle: {
        text: '@if($provinsi == NULL) SEMUA PROVINSI @else @if($kota == NULL) PROVINSI {{$provinsi->name}}, SEMUA KOTA @else PROVINSI {{$provinsi->name}},{{$kota->name}} @endif @endif'
    },
    xAxis: {
        categories: [
            'Bahan 01',
            'Bahan 02'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:.1f}%</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}%',
            }
        }
    },
    series: [
        {
            name: 'Sangat Baik',
            color: '#90ed7d',
            data: 
                [
                    0
                    @foreach($tipe1 as $mem1)
                    @if($mem1->keterangan == "Sangat Baik")
                        +{{($mem1->jumlah / $count1) *100 }}
                    @endif
                    @endforeach
                    ,
                    0
                    @foreach($tipe2 as $mem2)
                    @if($mem2->keterangan == "Sangat Baik")
                        +{{($mem2->jumlah / $count2) *100 }}
                    @endif
                    @endforeach
                ]
        }, {
            name: 'Baik',
            color: '#f9ed50',
            data: [
                    0
                    @foreach($tipe1 as $per1)
                    @if($per1->keterangan == "Baik")
                        +{{($per1->jumlah / $count1) *100 }}
                    @endif
                    @endforeach
                    ,
                    0
                    @foreach($tipe2 as $per2)
                    @if($per2->keterangan == "Baik")
                        +{{($per2->jumlah / $count2) *100 }}
                    @endif
                    @endforeach
                    ]
        }, {
            name: 'Buruk',
            color: '#f13333',
            data: [
                    0
                    @foreach($tipe1 as $tid1)
                    @if($tid1->keterangan == "Buruk")
                        +{{($tid1->jumlah / $count1) *100 }}
                    @endif
                    @endforeach
                    ,
                    0
                    @foreach($tipe2 as $tid2)
                    @if($tid2->keterangan == "Buruk")
                        +{{($tid2->jumlah / $count2) *100 }}
                    @endif
                    @endforeach
                    ]
    }]
});
</script>
@endsection
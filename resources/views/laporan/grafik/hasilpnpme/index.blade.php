@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Laporan Hasil PNPME Per Wilayah</div>
                <div class="panel-body">
					<form class="form-horizontal" method="post" enctype="multipart/form-data" target="_blank">
	                    <div>
	                        <label for="exampleInputEmail1">Bidang</label>
	                        <select class="form-control" name="bidang" id="bidang" required>
	                          <option value=""></option>
	                          <option value="hematologi">Hematologi</option>
	                          <option value="kimia klinik">Kimia Klinik</option>
	                          <option value="3">Urinalisa</option>
	                          <option value="4">Mikroskopis BTA</option>
	                          <option value="5">Mikroskopis TC</option>
	                          <option value="6">Anti HIV</option>
	                          <option value="7">Syphilis</option>
	                          <option value="8">HBsAg</option>
	                          <option value="9">Anti HCV</option>
	                          <option value="10">Mikroskopis Malaria</option>
	                          <option value="kimia air">Kimia Air</option>
	                          <option value="kimia air terbatas">Kimia Air Terbatas</option>
	                        </select>
	                    </div>
	                    <div>
	                        <div id="parametern">
	                          	<label for="exampleInputEmail1">Parameter</label>
	                          	<select class="form-control" name="parameter" id="parameter" required>
	                            	<option value=""></option>
	                          	</select>
	                        </div>
	                    </div>
	                    <div>
	                        <div id="kelompokn">
	                          	<label for="exampleInputEmail1">Kelompok</label>
	                          	<select class="form-control" name="kelompok" id="kelompok" required>
	                            	<option value=""></option>
	                            	<option value="Peserta">Peserta</option>
	                            	<option value="Alat">Alat</option>
	                            	<option value="Metode">Metode</option>
	                          	</select>
	                        </div>
	                    </div>
	                	<div>
		                    <label for="provinsi">Provinsi</label>
		                    <select name="provinsi" class="form-control" id="provinsi" required>
		                    	<option value=""></option>
		                    	<option value="Semua Provinsi">Semua Provinsi</option>
		                    	@foreach($provinsi as $pro)
		                    	<option value="{{$pro->id}}">{{$pro->name}}</option>
		                    	@endforeach
		                    </select>
		                </div>
	                    <div>
	                        <label for="kota">Kota / Kabupaten</label>
                            <select class="form-control" name="kota" id="kota" required>
                                <option value="-">-</option>
                            </select>
	                    </div>
	                	<div>
		                    <label for="exampleInputEmail1">Siklus</label>
		                    <select name="siklus" id="" class="form-control" required>
		                    	<option value=""></option>
		                    	<option value="1">1</option>
		                    	<option value="2">2</option>
		                    </select>
		                </div>
	                    <div>
	                        <label for="exampleInputEmail1">Tahun</label>
	                        <select class="form-control" name="tahun" required>
	                    	    <option></option>
	                            <option value="2018">2018</option>
	                            <option value="2019">2019</option>
	                            <option value="2020">2020</option>
	                            <option value="2021">2021</option>
	                            <option value="2022">2022</option>
	                            <option value="2023">2023</option>
	                            <option value="2024">2024</option>
	                            <option value="2025">2025</option>
	                        </select>
	                    </div><br>
		                <br>
		          	    <input type="submit" name="proses" class="btn btn-primary" value="Proses">
						{{ csrf_field() }}
		                </div>
					</form>
                </div>
            </div>	
        </div>
    </div>
</div>
@endsection
@section('scriptBlock')
<script>                            
$(function() {
    $('#parametern').hide(); 
    $('#kelompokn').hide(); 
    $('#bidang').change(function(){
    var setan  = $("#bidang option:selected").text();
        if(setan == 'Hematologi' || setan == 'Kimia Klinik' || setan == 'Kimia Air' || setan == 'Kimia Air Terbatas' ) {
        	if(setan == 'Hematologi' || setan == 'Kimia Klinik') {
	            $('#kelompok').prop('required', true);
	            $('#kelompokn').show(); 
        	}else{
	            $('#kelompok').removeAttr('required'); 
	            $('#kelompokn').hide(); 
            	$("#kelompok").val("");
	        }
            $('#parameter').prop('required', true);
            $('#parametern').show(); 
            var val = $(this).val(), i;
            var y = document.getElementById('parameter');
            $("#parameter").val("");
            $("#kelompok").val("");
            y.innerHTML = "<option>Please Wait... </option>";
            $.ajax({
              type: "GET",
              url : "{{url('getparameter').'/'}}"+val,
              success: function(addr){
                y.innerHTML = "<option> </option><option value='Semua Parameter'>Semua Parameter</option>"
                for(i = 0; i < addr.Hasil.length; i++){
                  var option = document.createElement("option");
                  option.value = addr.Hasil[i].Kode;
                  option.text = addr.Hasil[i].Parameter;
                  y.add(option);
                }
                return false;
              }
            });
        } else {
            $('#kelompok').removeAttr('required'); 
            $('#kelompokn').hide(); 
        	$("#kelompok").val("");
            $('#parameter').removeAttr('required'); 
            $('#parametern').hide(); 
        } 
    });
});

$("#provinsi").change(function(){
  // console.log('ada');
  	var val = $(this).val(), i;
  	var y = document.getElementById('kota');
  	y.innerHTML = "<option>Please Wait... </option>";
  	if (val == 'Semua Provinsi') {
  		y.innerHTML = "<option value='-'>-</option>";
  	}else{
	  $.ajax({
	    type: "GET",
	    url : "{{url('getkota').'/'}}"+val,
	    success: function(addr){
	      y.innerHTML = "<option> </option><option value='Semua Kota'>Semua Kota</option>"
	      for(i = 0; i < addr.Hasil.length; i++){
	        var option = document.createElement("option");
	        option.value = addr.Hasil[i].Kode;
	        option.text = addr.Hasil[i].Nama;
	        y.add(option);
	      }
	      return false;
	    }
	  });
	}
});
</script>
@endsection
@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Laporan Hasil PNPME</div>
                <a style="margin: 10px" onclick="javascript:printDiv('datana')" class="btn btn-primary">Print</a>
                <div class="panel-body" id="datana">
                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto;"></div>
                </div>
            </div>	
        </div>
    </div>
<script src="{{URL('/js/jquery.printarea.js')}}"></script>
<script src="https://code.highcharts.com/modules/pattern-fill.js"></script>
<script type="text/javascript">
function printDiv(divID) {
    var headElements = '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>,<meta name="viewport" content="width=device-width, initial-scale=1">';
    var options = { mode : "popup", popClose : true,extraHead : headElements };
    $( '#'+divID ).printArea( options );    
}

Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: "Hasil Imunologi {{$bidangna->parameter}} Siklus {{$input['siklus']}} Tahun {{$input['tahun']}}",
        style: {
            textTransform: 'capitalize'
        }
    },
    subtitle: {
        text: '@if($provinsi == NULL) SEMUA PROVINSI @else @if($kota == NULL) PROVINSI {{$provinsi->name}}, SEMUA KOTA @else PROVINSI {{$provinsi->name}},{{$kota->name}} @endif @endif'
    },
    xAxis: {
        categories: [
            '',
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:.1f}%</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}%',
            }
        }
    },
    series: [
        {
            name: 'Baik',
            color: '#90ed7d',
            data: 
                [
                    {{($baik / $count) * 100}},
                ]
        }, {
            name: 'Kurang',
            color: '#f9ed50',
            data: 
                [
                    {{($kurang / $count) * 100}},
                ]
        }, {
            name: 'Tidak dapat dinilai',
            color: '#f13333',
            data: 
                [
                    {{($tidak / $count) * 100}},
                ]
    }]
});
</script>
@endsection
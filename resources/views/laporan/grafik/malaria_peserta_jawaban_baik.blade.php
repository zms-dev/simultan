@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Jawaban Peserta</div>

                <div class="panel-body">
                  <div>
                      <label for="exampleInputEmail1">Siklus</label>
                          <select class="form-control" name="siklus" id="siklus">
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div>
                  <div>
                      <label for="exampleInputEmail1">Tahun</label>
                      <div class="controls input-append date form_datetime" data-link-field="dtp_input1">
                          <input size="16" type="text" value="" readonly class="form-control coba" name="tahun" id="tahun" required>
                          <span class="add-on"><i class="icon-th"></i></span>
                      </div>
                  </div>
                  <div id="chart1"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});

$(".coba").change(function(){
  var val = $('#tahun').val(), siklus = $('#siklus').val(), i, no = 0;

  $(function(){
    var chart1 = new Highcharts.Chart({
      chart: {
        renderTo: 'chart1',
        type: 'column',
        events: {
          load: requestDataC1
        }
      },
      title: {
        text : 'Grafik Jawaban Peserta ' + val
      },
      xAxis: {
        type: 'category'
      },
      yAxis: {
        title: {
          text : 'Jumlah Peserta'
        }
      },
      legend: {
        enabled : false
      },
      plotOptions: {
        series : {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format : '{point.y}'
          }
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br>'
      },
      series: [{
        name: 'Golongan',
        colorByPoint: true,
        data: []
      }]
    });

    function requestDataC1(){
      $.getJSON("{{ URL('/grafik/grafikmalariapesertajawabanbaik')}}",{x:val, siklus:siklus}, function(json){
        chart1.series[0].setData(json,true)
        console.log(json);
      });
    }
  });
});
</script>
@endsection
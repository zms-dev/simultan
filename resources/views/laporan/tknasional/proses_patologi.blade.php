<style>
body{
  	font-family: Arial, Helvetica, sans-serif;
}
.utama td,.utama th {  
  	border: 1px solid #ddd;
  	text-align: left;
	font-size: 10px;
}
td, th {  
  	text-align: left;
	font-size: 12px;
}

.utama {
  	border-collapse: collapse;
  	width: 100%;
}

.utama, th, td {
  	padding: 5px;
}
</style>
<div id="header">
    <table width="100%" cellpadding="0" border="0" >
        <thead>
            <tr>
                <th width="15%"></th>
                <th>
                   <img alt="{{URL::asset('asset/img/kimkes.png')}}" src="{{URL::asset('asset/img/kimkes.png')}}" height="150px"> 
                </th>
                <td>
                    <span style="font-size: 18px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 16px"><b>DIREKTORAT JENDRAL PELAYANAN KESEHATAN </b></span><br>
                    <span style="font-size: 16px"><b>BALAI BESAR LABORATORIUM KESEHATAN SURABAYA</b></span><br>
                    <div style="padding-left: 75px; margin-top: -15px; font-size: 14px">
                        <span><br/>Jalan Karangmenjangan No, 18 Surabaya - 60286<br>Telepon : (031) 5021451 | Fax : (031) 5020388<br>Website : bblksurabaya.id | Email : bblksub@yahoo.co.id</span>
                    </div>
                </td>
                <th width="%"></th>
            </tr>
            <tr>
                <th colspan="4"><hr></th>
            </tr>
        </thead>
    </table>
</div>
<table>
	<thead>
		<tr>
			<th>Institusi Penyelenggara</th>
			<td>: Balai Besar Laboratorium Kesehatan Surabaya</td>
		</tr>
		<tr>
			<th>Siklus / Tahun</th>
			<td>: {{$input['siklus']}} / {{$input['tahun']}}</td>
		</tr>
		<tr>
			<th>Bidang PME</th>
			<td>: {{$bidang->bidang}}</td>
		</tr>
		<tr>
			<th>Jumlah Peserta</th>
			<td>: {{count($data)}}</td>
		</tr>
	</thead>
</table>
<table class="utama" border="1">
	<thead>
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">Nama Labkes</th>
			<th rowspan="2">Prov</th>
			<th rowspan="2">Kab/Kota</th>
			<th rowspan="2">Status Akreditasi</th>
			<th rowspan="2">Pendidikan Petugas Pelaksanaan PME</th>
			<th colspan="2">PMI Labkes</th>
			<th colspan="2">Hasil PME Bidang Parameter</th>
		</tr>
		<tr>
			<th>Dilaksanakan</th>
			<th>Tidak Dilaksanakan</th>
			<th>In Lier</th>
			<th>Outlier</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; ?>
		@foreach($data as $val)
			<?php $bagian = 0;?>
			@if($input['bidang'] <= 2)
				@foreach($val->hasil as $hasil)
	        		@if ($hasil->hasil_pemeriksaan != '-' && $hasil->hasil_pemeriksaan != '0' && $hasil->hasil_pemeriksaan != '0.0' && $hasil->hasil_pemeriksaan != '0.00' )
					<tr>
						@if($bagian == 0)
						<td>{{$no++}}</td>
						<td>{{$val->nama_lab}}</td>
						<td>{{$val->provinsi}}</td>
						<td>{{$val->kota}}</td>
						<td></td>
						@else
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						@endif
						<td>@if($hasil->tingkat == 'Lain - lain'){{ $hasil->pendidikan_lain }}@else {{$hasil->tingkat}} @endif</td>
						<td>
							@if($val->pemantapan_mutu == 'Dilaksanakan')
								√
							@endif
						</td>
						<td>
							@if($val->pemantapan_mutu == 'Tidak Dilaksanakan')
								√
							@endif
						</td>
						<td>
	                        <?php
	                        	if(count($hasil->zscore) && count($hasil->zscoreb)){
		                            if($hasil->zscore->zscore == 'Tidak di analisis' || $hasil->zscoreb->zscore == 'Tidak di analisis'){
		                            }else if($hasil->zscore->zscore <= 3 && $hasil->zscore->zscore >= -3 && $hasil->zscoreb->zscore <= 3 && $hasil->zscoreb->zscore >= -3){
		                                echo $hasil->zscore->nama_parameter;
		                            }else{
		                            }
		                        }elseif(count($hasil->zscore)){
		                            if($hasil->zscore->zscore == 'Tidak di analisis'){
		                            }else if($hasil->zscore->zscore <= 3 && $hasil->zscore->zscore >= -3){
		                                echo $hasil->zscore->nama_parameter;
		                            }else{
		                            }
		                        }elseif(count($hasil->zscoreb)){
		                            if($hasil->zscoreb->zscore == 'Tidak di analisis'){
		                            }else if($hasil->zscoreb->zscore <= 3 && $hasil->zscoreb->zscore >= -3){
		                                echo $hasil->zscoreb->nama_parameter;
		                            }else{
		                            }
		                        }
	                        ?>
						</td>
						<td>
	                        <?php
	                        	if(count($hasil->zscore) && count($hasil->zscoreb)){
		                            if($hasil->zscore->zscore == 'Tidak di analisis' || $hasil->zscoreb->zscore == 'Tidak di analisis'){
		                                echo $hasil->zscore->nama_parameter;
		                            }else if($hasil->zscore->zscore <= 3 && $hasil->zscore->zscore >= -3 && $hasil->zscoreb->zscore <= 3 && $hasil->zscoreb->zscore >= -3){
		                            }else{
		                                echo $hasil->zscore->nama_parameter;
		                            }
		                        }elseif(count($hasil->zscore)){
		                            if($hasil->zscore->zscore == 'Tidak di analisis'){
		                                echo $hasil->zscore->nama_parameter;
		                            }else if($hasil->zscore->zscore <= 3 && $hasil->zscore->zscore >= -3){
		                            }else{
		                                echo $hasil->zscore->nama_parameter;
		                            }
		                        }elseif(count($hasil->zscoreb)){
		                            if($hasil->zscoreb->zscore == 'Tidak di analisis'){
		                                echo $hasil->zscore->nama_parameter;
		                            }else if($hasil->zscoreb->zscore <= 3 && $hasil->zscoreb->zscore >= -3){
		                            }else{
		                                echo $hasil->zscoreb->nama_parameter;
		                            }
		                        }
	                        ?>
	                    </td>
					</tr>
					<?php $bagian++; ?>
					@endif
				@endforeach
			@endif
		@endforeach
	</tbody>
</table>
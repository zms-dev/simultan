<style>
body{
  	font-family: Arial, Helvetica, sans-serif;
}
.utama td,.utama th {  
  	border: 1px solid #ddd;
  	text-align: left;
	font-size: 10px;
}
td, th {  
  	text-align: left;
	font-size: 12px;
}

.utama {
  	border-collapse: collapse;
  	width: 100%;
}

.utama, th, td {
  	padding: 5px;
}
</style>
<div id="header">
    <table width="100%" cellpadding="0" border="0" >
        <thead>
            <tr>
                <th width="15%"></th>
                <th>
                   <img alt="{{URL::asset('asset/img/kimkes.png')}}" src="{{URL::asset('asset/img/kimkes.png')}}" height="150px"> 
                </th>
                <td>
                    <span style="font-size: 18px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 16px"><b>DIREKTORAT JENDRAL PELAYANAN KESEHATAN </b></span><br>
                    <span style="font-size: 16px"><b>BALAI BESAR LABORATORIUM KESEHATAN SURABAYA</b></span><br>
                    <div style="padding-left: 75px; margin-top: -15px; font-size: 14px">
                        <span><br/>Jalan Karangmenjangan No, 18 Surabaya - 60286<br>Telepon : (031) 5021451 | Fax : (031) 5020388<br>Website : bblksurabaya.id | Email : bblksub@yahoo.co.id</span>
                    </div>
                </td>
                <th width="%"></th>
            </tr>
            <tr>
                <th colspan="4"><hr></th>
            </tr>
        </thead>
    </table>
</div>
<table>
	<thead>
		<tr>
			<th>Institusi Penyelenggara</th>
			<td>: Balai Besar Laboratorium Kesehatan Surabaya</td>
		</tr>
		<tr>
			<th>Siklus / Tahun</th>
			<td>: {{$input['siklus']}} / {{$input['tahun']}}</td>
		</tr>
		<tr>
			<th>Bidang PME</th>
			<td>: @if($input['bidang'] == 4) BTA @elseif($input['bidang'] == 5) Telur Cacing @elseif($input['bidang'] == 10) Malaria @elseif($input['bidang'] == 13) Identifikasi Bakteri @endif</td>
		</tr>
		<tr>
			<th>Jumlah Peserta</th>
			<td>: {{count($data)}}</td>
		</tr>
	</thead>
</table>
<table class="utama" border="1">
	<thead>
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">Nama Labkes</th>
			<th rowspan="2">Prov</th>
			<th rowspan="2">Kab/Kota</th>
			<th rowspan="2">Status Akreditasi</th>
			<th rowspan="2">Pendidikan Petugas Pelaksanaan PME</th>
			<th colspan="2">PMI Labkes</th>
			<th colspan="2">Hasil PME Bidang Parameter</th>
		</tr>
		<tr>
			<th>Dilaksanakan</th>
			<th>Tidak Dilaksanakan</th>
			<th>In Lier</th>
			<th>Outlier</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; ?>
		@foreach($data as $val)
			<?php $bagian = 0;?>
			@foreach($val->evaluasi as $has)
				<tr>
					@if($bagian == 0)
					<td>{{$no++}}</td>
					<td>{{$val->nama_lab}}</td>
					<td>{{$val->provinsi}}</td>
					<td>{{$val->kota}}</td>
					<td></td>
					@else
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					@endif
					<td>@if($has->tingkat == 'Lain - lain'){{ $has->pendidikan_lain }}@else {{$has->tingkat}} @endif</td>
					<td>
						@if($has->pmi_labkes == 'dilaksanakan')
							√
						@endif
					</td>
					<td>
						@if($has->pmi_labkes == 'tidak dilaksanakan')
							√
						@endif
					</td>
					@if($input['bidang'] == 13)
					<td>
						<?php
                            if ($has->nilai >= 3) {
                            	echo $has->kode_sediaan;
                            }else{
                            }
						?>
					</td>
					<td>
						<?php 
	                        if ($has->nilai >= 3) {
	                        }else{
	                            echo $has->kode_sediaan;
	                        }
                        ?>
                    </td>
					@else
					<td>
						<?php
                            if ($has->nilai > 5) {
                            	echo $has->kode_sediaan;
                            }else{
                            }
						?>
					</td>
					<td>
						<?php 
	                        if ($has->nilai > 5) {
	                        }else{
	                            echo $has->kode_sediaan;
	                        }
                        ?>
                    </td>
                    @endif
				</tr>
				<?php $bagian++; ?>
			@endforeach
		@endforeach
	</tbody>
</table>
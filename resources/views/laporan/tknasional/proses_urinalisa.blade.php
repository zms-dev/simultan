<style>
body{
  	font-family: Arial, Helvetica, sans-serif;
}
.utama td,.utama th {  
  	border: 1px solid #ddd;
  	text-align: left;
	font-size: 10px;
}
td, th {  
  	text-align: left;
	font-size: 12px;
}

.utama {
  	border-collapse: collapse;
  	width: 100%;
}

.utama, th, td {
  	padding: 5px;
}
</style>
<div id="header">
    <table width="100%" cellpadding="0" border="0" >
        <thead>
            <tr>
                <th width="15%"></th>
                <th>
                   <img alt="{{URL::asset('asset/img/kimkes.png')}}" src="{{URL::asset('asset/img/kimkes.png')}}" height="150px"> 
                </th>
                <td>
                    <span style="font-size: 18px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 16px"><b>DIREKTORAT JENDRAL PELAYANAN KESEHATAN </b></span><br>
                    <span style="font-size: 16px"><b>BALAI BESAR LABORATORIUM KESEHATAN SURABAYA</b></span><br>
                    <div style="padding-left: 75px; margin-top: -15px; font-size: 14px">
                        <span><br/>Jalan Karangmenjangan No, 18 Surabaya - 60286<br>Telepon : (031) 5021451 | Fax : (031) 5020388<br>Website : bblksurabaya.id | Email : bblksub@yahoo.co.id</span>
                    </div>
                </td>
                <th width="%"></th>
            </tr>
            <tr>
                <th colspan="4"><hr></th>
            </tr>
        </thead>
    </table>
</div>
<table>
	<thead>
		<tr>
			<th>Institusi Penyelenggara</th>
			<td>: Balai Besar Laboratorium Kesehatan Surabaya</td>
		</tr>
		<tr>
			<th>Siklus / Tahun</th>
			<td>: {{$input['siklus']}} / {{$input['tahun']}}</td>
		</tr>
		<tr>
			<th>Bidang PME</th>
			<td>: {{$bidang->bidang}}</td>
		</tr>
		<tr>
			<th>Jumlah Peserta</th>
			<td>: {{count($data)}}</td>
		</tr>
	</thead>
</table>
<table class="utama" border="1">
	<thead>
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">Nama Labkes</th>
			<th rowspan="2">Prov</th>
			<th rowspan="2">Kab/Kota</th>
			<th rowspan="2">Status Akreditasi</th>
			<th rowspan="2">Pendidikan Petugas Pelaksanaan PME</th>
			<th colspan="2">PMI Labkes</th>
			<th colspan="2">Hasil PME Bidang Parameter</th>
		</tr>
		<tr>
			<th>Dilaksanakan</th>
			<th>Tidak Dilaksanakan</th>
			<th>In Lier</th>
			<th>Outlier</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; ?>
		@foreach($data as $val)
			<?php $bagian = 0;
				if(count($val->hasila) >= count($val->hasilb)){
					$hasil1 = $val->hasila;
					$hasil2 = $val->hasilb;
				}else{
					$hasil1 = $val->hasilb;
					$hasil2 = $val->hasila;
				}
			?>
			@foreach($hasil1 as $has)
				<tr>
					@if($bagian == 0)
					<td>{{$no++}}</td>
					<td>{{$val->nama_lab}}</td>
					<td>{{$val->provinsi}}</td>
					<td>{{$val->kota}}</td>
					<td></td>
					@else
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					@endif
					<td>@if($hasil->tingkat == 'Lain - lain'){{ $hasil->pendidikan_lain }}@else {{$hasil->tingkat}} @endif</td>
					<td>
						@if($val->pemantapan_mutu == 'Dilaksanakan')
							√
						@endif
					</td>
					<td>
						@if($val->pemantapan_mutu == 'Tidak Dilaksanakan')
							√
						@endif
					</td>
	        		<?php
		        		if ($has->parameter <= 10) {
	                        if ($has->parameter == 9) {
	                            if ($has->hasil_pemeriksaan != '-') {
	                                if($has->parameter == $has->parameter_id){
	                                    $pemu = $has->hp;
	                                }else{
	                                    $pemu = $has->hasil_pemeriksaan;
	                                }
	                                if ($pemu == "Tidak Mengerjakan") {
	                                  $skor = "-";
	                                }else{
	                                    if ($pemu >= $has->target) {
	                                        $total = $pemu - $has->target;
	                                    }else{
	                                        $total = $has->target - $pemu ;
	                                    }
	                                    $total = number_format($total, 3);
	                                    if ($total == 0) {
	                                        $skor = "4";
	                                    }elseif ($total > 0 && $total <= 0.005) {
	                                        $skor = "3";
	                                    }elseif($total > 0.005 && $total <= 0.010){
	                                        $skor = "2";
	                                    }elseif($total > 0.010 && $total <= 0.015) {
	                                        $skor = "1";
	                                    }else{
	                                        $skor = "0";
	                                    }
	                                }
	                            }else{
	                                $skor = "-";
	                            }
	                        }elseif($has->parameter == 10){   
	                            if ($has->hasil_pemeriksaan != '-') {
	                                if($has->parameter == $has->parameter_id){
	                                    $pemu = $has->hp;
	                                }else{
	                                    $pemu = $has->hasil_pemeriksaan;
	                                }
	                                if ($pemu == "Tidak Mengerjakan") {
	                                  $skor = "-";
	                                }else{
	                                    if ($pemu >= $has->target) {
	                                        $total = $pemu*1 - $has->target*1;
	                                    }else{
	                                        $total =  str_replace('+', '', $has->target) - str_replace('+', '', $pemu);
	                                    }
	                                    if ($total == 0) {
	                                        $skor = "4";
	                                    }elseif ($total > 0 && $total <= 0.5) {
	                                        $skor = "3";
	                                    }elseif($total > 0.5 && $total <= 1.0){
	                                        $skor = "2";
	                                    }elseif ($total > 1.0 && $total <= 1.5) {
	                                        $skor = "1";
	                                    }else{
	                                        $skor = "0";
	                                    }
	                                }
	                            }else{
	                                $skor = "-";
	                            }
	                        }
	                    }else{
	                        if(count($has->hp)){
	                            if($has->target == 'Negatif'){
	                                if ($has->hp != $has->target) {
	                                    $skor = "0";
	                                }else{
	                                    $skor = "4";
	                                }
	                            }elseif($has->target == 'Positif'){
	                                if ($has->hp != $has->target) {
	                                    $skor = "0";
	                                }else{
	                                    $skor = "4";
	                                }
	                            }elseif($has->hp == 'Negatif'){
	                                if ($has->hp != $has->target) {
	                                    $skor = "0";
	                                }else{
	                                    $skor = "4";
	                                }
	                            }else{
	                                if($has->hp == $has->target){
	                                    $total = "0";
	                                }elseif ($has->hp >= $has->target) {
	                                    $total = str_replace('+', '', $has->hp) - str_replace('+', '', $has->target) ;
	                                }else{
	                                    $total =  str_replace('+', '', $has->target) - str_replace('+', '', $has->hp);
	                                }
	                                if ($total == "0") {
	                                    $skor = "4";
	                                }elseif($total == "1" || $total == "-1"){
	                                    $skor = "3";
	                                }elseif ($total == "2" || $total == "-2") {
	                                    $skor = "2";
	                                }elseif ($total == "3" || $total == "-3") {
	                                    $skor = "1";
	                                }
	                            }
	                        }else{
	                            $skor = '-';
	                        }
	                    }

		        		if ($hasil2[$bagian]->parameter <= 10) {
	                        if ($hasil2[$bagian]->parameter == 9) {
	                            if ($hasil2[$bagian]->hasil_pemeriksaan != '-') {
	                                if($hasil2[$bagian]->parameter == $hasil2[$bagian]->parameter_id){
	                                    $pemu2 = $hasil2[$bagian]->hp;
	                                }else{
	                                    $pemu2 = $hasil2[$bagian]->hasil_pemeriksaan;
	                                }
	                                if ($pemu2 == "Tidak Mengerjakan") {
	                                  $skor2 = "-";
	                                }else{
	                                    if ($pemu2 >= $hasil2[$bagian]->target) {
	                                        $total2 = $pemu2 - $hasil2[$bagian]->target;
	                                    }else{
	                                        $total2 = $hasil2[$bagian]->target - $pemu2 ;
	                                    }
	                                    $total2 = number_format($total2, 3);
	                                    if ($total2 == 0) {
	                                        $skor2 = "4";
	                                    }elseif ($total2 > 0 && $total2 <= 0.005) {
	                                        $skor2 = "3";
	                                    }elseif($total2 > 0.005 && $total2 <= 0.010){
	                                        $skor2 = "2";
	                                    }elseif($total2 > 0.010 && $total2 <= 0.015) {
	                                        $skor2 = "1";
	                                    }else{
	                                        $skor2 = "0";
	                                    }
	                                }
	                            }else{
	                                $skor2 = "-";
	                            }
	                        }elseif($hasil2[$bagian]->parameter == 10){   
	                            if ($hasil2[$bagian]->hasil_pemeriksaan != '-') {
	                                if($hasil2[$bagian]->parameter == $hasil2[$bagian]->parameter_id){
	                                    $pemu2 = $hasil2[$bagian]->hp;
	                                }else{
	                                    $pemu2 = $hasil2[$bagian]->hasil_pemeriksaan;
	                                }
	                                if ($pemu2 == "Tidak Mengerjakan") {
	                                  $skor2 = "-";
	                                }else{
	                                    if ($pemu2 >= $hasil2[$bagian]->target) {
	                                        $total2 = $pemu2*1 - $hasil2[$bagian]->target*1;
	                                    }else{
	                                        $total2 =  str_replace('+', '', $hasil2[$bagian]->target) - str_replace('+', '', $pemu2);
	                                    }
	                                    if ($total2 == 0) {
	                                        $skor2 = "4";
	                                    }elseif ($total2 > 0 && $total2 <= 0.5) {
	                                        $skor2 = "3";
	                                    }elseif($total2 > 0.5 && $total2 <= 1.0){
	                                        $skor2 = "2";
	                                    }elseif ($total2 > 1.0 && $total2 <= 1.5) {
	                                        $skor2 = "1";
	                                    }else{
	                                        $skor2 = "0";
	                                    }
	                                }
	                            }else{
	                                $skor2 = "-";
	                            }
	                        }
	                    }else{
	                        if(count($hasil2[$bagian]->hp)){
	                            if($hasil2[$bagian]->target == 'Negatif'){
	                                if ($hasil2[$bagian]->hp != $hasil2[$bagian]->target) {
	                                    $skor2 = "0";
	                                }else{
	                                    $skor2 = "4";
	                                }
	                            }elseif($hasil2[$bagian]->target == 'Positif'){
	                                if ($hasil2[$bagian]->hp != $hasil2[$bagian]->target) {
	                                    $skor2 = "0";
	                                }else{
	                                    $skor2 = "4";
	                                }
	                            }elseif($hasil2[$bagian]->hp == 'Negatif'){
	                                if ($hasil2[$bagian]->hp != $hasil2[$bagian]->target) {
	                                    $skor2 = "0";
	                                }else{
	                                    $skor2 = "4";
	                                }
	                            }else{
                                	if ($hasil2[$bagian]->hp == '±') {
                                		$hp = 1;
                                	}else{
                                		$hp = str_replace('+', '', $hasil2[$bagian]->hp);
                                	}
                                	if ($hasil2[$bagian]->target == '±') {
                                		$target = 1;
                                	}else{
                                		$target = str_replace('+', '', $hasil2[$bagian]->target);
                                	}

	                                if($hasil2[$bagian]->hp == $hasil2[$bagian]->target){
	                                    $total2 = "0";
	                                }elseif ($hasil2[$bagian]->hp >= $hasil2[$bagian]->target) {	
	                                    $total2 = $hp - $target ;
	                                }else{
	                                    $total2 = $target - $hp;
	                                }
	                                if ($total2 == "0") {
	                                    $skor2 = "4";
	                                }elseif($total2 == "1" || $total2 == "-1"){
	                                    $skor2 = "3";
	                                }elseif ($total2 == "2" || $total2 == "-2") {
	                                    $skor2 = "2";
	                                }elseif ($total2 == "3" || $total2 == "-3") {
	                                    $skor2 = "1";
	                                }
	                            }
	                        }else{
	                            $skor2 = '-';
	                        }
	                    }
	        		?>
					<td>
                        <?php
                            if($skor > 2 && $skor2 > 2){
                                echo $has->nama_parameter;
                            }else{
                            }
                        ?>
					</td>
					<td>
						<?php
                            if($skor <= 2 || $skor2 <= 2){
                                echo $has->nama_parameter;
                            }else{
                            }
                        ?>
                    </td>
				</tr>
				<?php $bagian++; ?>
			@endforeach
		@endforeach
	</tbody>
</table>
@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Laporan PME TK. Nasional</div>
                <div class="panel-body">
                  <form method="post" enctype="multipart/form-data" target="_blank">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Bidang</label>
                      <select name="bidang" class="form-control" required id="bidang">
                        <option></option>
                        <option value="1">Hematologi</option>
                        <option value="2">Kimia Klinik</option>
                        <option value="3">Urinalisa</option>
                        <option value="4">BTA</option>
                        <option value="5">Telur Cacing</option>
                        <option value="10">Malaria</option>
                        <option value="13">Identifikasi Bakteri</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Siklus</label>
                      <select name="siklus" class="form-control" required>
                        <option value="1">1</option>
                        <option value="2">2</option>
                      </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tahun</label>
                        <div class="controls input-append date form_datetime" data-link-field="dtp_input1">
                            <input size="16" type="text" value="" readonly class="form-control coba" name="tahun" id="tahun" required>
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                    </div>
                    <input type="submit" name="proses" value="Proses" class="btn btn-primary">
                    {{ csrf_field() }}
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});
</script>
@endsection
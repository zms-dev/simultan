<script src="{{URL::asset('asset/js/jquery.min.js')}}"></script>
    <script src="{{URL::asset('asset/js/highcharts.js')}}"></script>
    <style>
        .row {
            width: 100%;
        }
        .col-md-4 {
            width: 50%;
            margin-bottom: 15%;
        }
    </style>
</script>

@foreach($hematologis as $hematologi)
    <div class="row">
        <div class="col-md-4" style="float: left">
            <div id="container{{ $hematologi->id }}"></div>
        </div>
        <div class="col-md-4" style="float: right">
            <div id="containerb{{ $hematologi->id }}"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4" style="float: left">
            <div id="containeralat{{ $hematologi->id }}"></div>
        </div>
        <div class="col-md-4" style="float: right">
            <div id="containeralatb{{ $hematologi->id }}"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4" style="float: left">
            <div id="containermetode{{ $hematologi->id }}"></div>
        </div>
        <div class="col-md-4" style="float: right">
            <div id="containermetodeb{{ $hematologi->id }}"></div>
        </div>
    </div>

    <script type="text/javascript">
        window.print();

        var hg1 = Highcharts.chart('container{{ $hematologi->id }}', {
            exporting: {
                enabled: true // hide button
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Grafik Distribusi Hasil Parameter {!!$parameter[$hematologi->id]->nama_parameter!!} Kelompok Seluruh Peserta',
                style : {
                    'font-size' : "10px",
                    'font-weight' : "bold",
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
            title: {
                text : 'Jumlah Peserta'
            }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y} {point.a}',
                        crop: false,
                        overflow: 'none'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
            },

            "series": [
                {
                    "name": "Nilai yang Sama",
                    "colorByPoint": false,
                    "data": [
                        {
                        "name": "< {{$grafik[$hematologi->id][0]->ring1}}",
                        @if($dataReg[$hematologi->id]->hasil_pemeriksaan < $grafik[$hematologi->id][0]->ring1)
                        "a": "*",
                        @endif
                        "color": '#BF0B23',
                        "y": {{$grafik[$hematologi->id][0]->out_bawah_param}},
                        },
                        {
                            "name": ">= {{$grafik[$hematologi->id][0]->ring1}} < {{$grafik[$hematologi->id][0]->ring2}}",
                            @if($dataReg[$hematologi->id]->hasil_pemeriksaan >= $grafik[$hematologi->id][0]->ring1 && $dataReg[$hematologi->id]->hasil_pemeriksaan < $grafik[$hematologi->id][0]->ring2)
                                "a": "*",
                            @endif
                            @if($median2[$hematologi->id]->median >= $grafik[$hematologi->id][0]->ring1 && $median2[$hematologi->id]->median < $grafik[$hematologi->id][0]->ring2)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik[$hematologi->id][0]->count_ring_1_param}},
                        },
                        {
                            "name": ">= {{$grafik[$hematologi->id][0]->ring2}} < {{$grafik[$hematologi->id][0]->ring3}}",
                            @if($dataReg[$hematologi->id]->hasil_pemeriksaan >= $grafik[$hematologi->id][0]->ring2 && $dataReg[$hematologi->id]->hasil_pemeriksaan < $grafik[$hematologi->id][0]->ring3)
                                "a": "*",
                            @endif
                            @if($median2[$hematologi->id]->median >= $grafik[$hematologi->id][0]->ring2 && $median2[$hematologi->id]->median < $grafik[$hematologi->id][0]->ring3)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik[$hematologi->id][0]->count_ring_2_param}},
                        },
                        {
                            "name": ">= {{$grafik[$hematologi->id][0]->ring3}} < {{$grafik[$hematologi->id][0]->ring4}}",
                            @if($dataReg[$hematologi->id]->hasil_pemeriksaan >= $grafik[$hematologi->id][0]->ring3 && $dataReg[$hematologi->id]->hasil_pemeriksaan < $grafik[$hematologi->id][0]->ring4)
                                "a": "*",
                            @endif
                            @if($median2[$hematologi->id]->median >= $grafik[$hematologi->id][0]->ring3 && $median2[$hematologi->id]->median < $grafik[$hematologi->id][0]->ring4)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik[$hematologi->id][0]->count_ring_3_param}},
                        },
                        {
                            "name": ">= {{$grafik[$hematologi->id][0]->ring4}} <= {{$grafik[$hematologi->id][0]->ring5}}",
                            @if($dataReg[$hematologi->id]->hasil_pemeriksaan >= $grafik[$hematologi->id][0]->ring4 && $dataReg[$hematologi->id]->hasil_pemeriksaan <= $grafik[$hematologi->id][0]->ring5)
                                "a": "*",
                            @endif
                            @if($median2[$hematologi->id]->median >= $grafik[$hematologi->id][0]->ring4 && $median2[$hematologi->id]->median <= $grafik[$hematologi->id][0]->ring5)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik[$hematologi->id][0]->count_ring_4_param}},
                        },
                        {
                            "name": "> {{$grafik[$hematologi->id][0]->ring5}}",
                            @if($dataReg[$hematologi->id]->hasil_pemeriksaan > $grafik[$hematologi->id][0]->ring5)
                                "a": "*",
                            @endif
                        "color": '#BF0B23',
                            "y": {{$grafik[$hematologi->id][0]->out_atas_param}},
                        },
                    ]
                }
            ],
        });
        var hg2 = Highcharts.chart('containeralat{{ $hematologi->id }}', {
            exporting: {
                enabled: true // hide button
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Grafik Distribusi Hasil Parameter {!!$parameter[$hematologi->id]->nama_parameter!!} <br> Kelompok Alat {{$instrumen->instrumen}} {{$dataReg->instrument_lain}}',
                style : {
                    'font-size' : "10px",
                    'font-weight' : "bold",
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
            title: {
                text : 'Jumlah Peserta'
            }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y} {point.a}',
                        crop: false,
                        overflow: 'none'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
            },

            "series": [
                {
                    "name": "Nilai yang Sama",
                    "colorByPoint": false,
                    "data": [
                        {
                        "name": "< {{$grafik[$hematologi->id][0]->ring1}}",
                        @if($dataReg[$hematologi->id]->hasil_pemeriksaan < $grafik[$hematologi->id][0]->ring1)
                        "a": "*",
                        @endif
                        "color": '#BF0B23',
                        "y": {{$grafik[$hematologi->id][0]->out_bawah_alat}},
                        },
                        {
                            "name": ">= {{$grafik[$hematologi->id][0]->ring1}} < {{$grafik[$hematologi->id][0]->ring2}}",
                            @if($dataReg[$hematologi->id]->hasil_pemeriksaan >= $grafik[$hematologi->id][0]->ring1 && $dataReg[$hematologi->id]->hasil_pemeriksaan < $grafik[$hematologi->id][0]->ring2)
                                "a": "*",
                            @endif
                            @if(!empty($medianalat) && $medianalat->median >= $grafik[$hematologi->id][0]->ring1 && $medianalat->median < $grafik[$hematologi->id][0]->ring2)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik[$hematologi->id][0]->count_ring_1_alat}},
                        },
                        {
                            "name": ">= {{$grafik[$hematologi->id][0]->ring2}} < {{$grafik[$hematologi->id][0]->ring3}}",
                            @if($dataReg[$hematologi->id]->hasil_pemeriksaan >= $grafik[$hematologi->id][0]->ring2 && $dataReg[$hematologi->id]->hasil_pemeriksaan < $grafik[$hematologi->id][0]->ring3)
                                "a": "*",
                            @endif
                            @if(!empty($medianalat) && $median[$hematologi->id]->median >= $grafik[$hematologi->id][0]->ring2 && $median[$hematologi->id]->median < $grafik[$hematologi->id][0]->ring3)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik[$hematologi->id][0]->count_ring_2_alat}},
                        },
                        {
                            "name": ">= {{$grafik[$hematologi->id][0]->ring3}} < {{$grafik[$hematologi->id][0]->ring4}}",
                            @if($dataReg[$hematologi->id]->hasil_pemeriksaan >= $grafik[$hematologi->id][0]->ring3 && $dataReg[$hematologi->id]->hasil_pemeriksaan < $grafik[$hematologi->id][0]->ring4)
                                "a": "*",
                            @endif
                            @if(!empty($medianalat) && $median[$hematologi->id]->median >= $grafik[$hematologi->id][0]->ring3 && $median[$hematologi->id]->median < $grafik[$hematologi->id][0]->ring4)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik[$hematologi->id][0]->count_ring_3_alat}},
                        },
                        {
                            "name": ">= {{$grafik[$hematologi->id][0]->ring4}} <= {{$grafik[$hematologi->id][0]->ring5}}",
                            @if($dataReg[$hematologi->id]->hasil_pemeriksaan >= $grafik[$hematologi->id][0]->ring4 && $dataReg[$hematologi->id]->hasil_pemeriksaan <= $grafik[$hematologi->id][0]->ring5)
                                "a": "*",
                            @endif
                            @if(!empty($medianalat) && $medianalat->median >= $grafik[$hematologi->id][0]->ring4 && $medianalat->median <= $grafik[$hematologi->id][0]->ring5)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik[$hematologi->id][0]->count_ring_4_alat}},
                        },
                        {
                            "name": "> {{$grafik[$hematologi->id][0]->ring5}}",
                            @if($dataReg[$hematologi->id]->hasil_pemeriksaan > $grafik[$hematologi->id][0]->ring5)
                                "a": "*",
                            @endif
                        "color": '#BF0B23',
                            "y": {{$grafik[$hematologi->id][0]->out_atas_alat}},
                        },
                    ]
                }
            ],
        });
        var hg3 = Highcharts.chart('containermetode{{ $hematologi->id }}', {
            exporting: {
                enabled: true // hide button
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Grafik Distribusi Hasil {!!$parameter[$hematologi->id]->nama_parameter!!} <br> Kelompok {{$metode->metode_pemeriksaan}} {{$dataReg->metode_lain}}',
                style : {
                    'font-size' : "10px",
                    'font-weight' : "bold",
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
            title: {
                text : 'Jumlah Peserta'
            }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y} {point.a}',
                        crop: false,
                        overflow: 'none'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
            },

            "series": [
                {
                    "name": "Nilai yang Sama",
                    "colorByPoint": false,
                    "data": [
                        {
                        "name": "< {{$grafik[$hematologi->id][0]->ring1}}",
                        @if($dataReg[$hematologi->id]->hasil_pemeriksaan < $grafik[$hematologi->id][0]->ring1)
                        "a": "*",
                        @endif
                        "color": '#BF0B23',
                        "y": {{$grafik[$hematologi->id][0]->out_bawah_metode}},
                        },
                        {
                            "name": ">= {{$grafik[$hematologi->id][0]->ring1}} < {{$grafik[$hematologi->id][0]->ring2}}",
                            @if($dataReg[$hematologi->id]->hasil_pemeriksaan >= $grafik[$hematologi->id][0]->ring1 && $dataReg[$hematologi->id]->hasil_pemeriksaan < $grafik[$hematologi->id][0]->ring2)
                                "a": "*",
                            @endif
                            @if(!empty($medianmetode) && $medianmetode->median >= $grafik[$hematologi->id][0]->ring1 && $medianmetode->median < $grafik[$hematologi->id][0]->ring2)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik[$hematologi->id][0]->count_ring_1_metode}},
                        },
                        {
                            "name": ">= {{$grafik[$hematologi->id][0]->ring2}} < {{$grafik[$hematologi->id][0]->ring3}}",
                            @if($dataReg[$hematologi->id]->hasil_pemeriksaan >= $grafik[$hematologi->id][0]->ring2 && $dataReg[$hematologi->id]->hasil_pemeriksaan < $grafik[$hematologi->id][0]->ring3)
                                "a": "*",
                            @endif
                            @if(!empty($medianmetode) && $medianmetode->median >= $grafik[$hematologi->id][0]->ring2 && $medianmetode->median < $grafik[$hematologi->id][0]->ring3)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik[$hematologi->id][0]->count_ring_2_metode}},
                        },
                        {
                            "name": ">= {{$grafik[$hematologi->id][0]->ring3}} < {{$grafik[$hematologi->id][0]->ring4}}",
                            @if($dataReg[$hematologi->id]->hasil_pemeriksaan >= $grafik[$hematologi->id][0]->ring3 && $dataReg[$hematologi->id]->hasil_pemeriksaan < $grafik[$hematologi->id][0]->ring4)
                                "a": "*",
                            @endif
                            @if(!empty($medianmetode) && $medianmetode->median >= $grafik[$hematologi->id][0]->ring3 && $medianmetode->median < $grafik[$hematologi->id][0]->ring4)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik[$hematologi->id][0]->count_ring_3_metode}},
                        },
                        {
                            "name": ">= {{$grafik[$hematologi->id][0]->ring4}} <= {{$grafik[$hematologi->id][0]->ring5}}",
                            @if($dataReg[$hematologi->id]->hasil_pemeriksaan >= $grafik[$hematologi->id][0]->ring4 && $dataReg[$hematologi->id]->hasil_pemeriksaan <= $grafik[$hematologi->id][0]->ring5)
                                "a": "*",
                            @endif
                            @if(!empty($medianmetode) && $medianmetode->median >= $grafik[$hematologi->id][0]->ring4 && $medianmetode->median <= $grafik[$hematologi->id][0]->ring5)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik[$hematologi->id][0]->count_ring_4_metode}},
                        },
                        {
                            "name": "> {{$grafik[$hematologi->id][0]->ring5}}",
                            @if($dataReg[$hematologi->id]->hasil_pemeriksaan > $grafik[$hematologi->id][0]->ring5)
                                "a": "*",
                            @endif
                        "color": '#BF0B23',
                            "y": {{$grafik[$hematologi->id][0]->out_atas_metode}},
                        },
                    ]
                }
            ],
        });

        // B
        var hgb1 = Highcharts.chart('containerb{{ $hematologi->id }}', {
            exporting: {
                enabled: true // hide button
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Grafik Distribusi Hasil Parameter {!!$parameter[$hematologi->id]->nama_parameter!!} Kelompok Seluruh Peserta',
                style : {
                    'font-size' : "10px",
                    'font-weight' : "bold",
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
            title: {
                text : 'Jumlah Peserta'
            }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y} {point.a}',
                        crop: false,
                        overflow: 'none'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
            },

            "series": [
                {
                    "name": "Nilai yang Sama",
                    "colorByPoint": false,
                    "data": [
                        {
                        "name": "< {{$grafik2[$hematologi->id][0]->ring1}}",
                        @if($dataReg2[$hematologi->id]->hasil_pemeriksaan < $grafik2[$hematologi->id][0]->ring1)
                        "a": "*",
                        @endif
                        "color": '#BF0B23',
                        "y": {{$grafik2[$hematologi->id][0]->out_bawah_param}},
                        },
                        {
                            "name": ">= {{$grafik2[$hematologi->id][0]->ring1}} < {{$grafik2[$hematologi->id][0]->ring2}}",
                            @if($dataReg2[$hematologi->id]->hasil_pemeriksaan >= $grafik2[$hematologi->id][0]->ring1 && $dataReg2[$hematologi->id]->hasil_pemeriksaan < $grafik2[$hematologi->id][0]->ring2)
                                "a": "*",
                            @endif
                            @if($median2[$hematologi->id]->median >= $grafik2[$hematologi->id][0]->ring1 && $median2[$hematologi->id]->median < $grafik2[$hematologi->id][0]->ring2)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik2[$hematologi->id][0]->count_ring_1_param}},
                        },
                        {
                            "name": ">= {{$grafik2[$hematologi->id][0]->ring2}} < {{$grafik2[$hematologi->id][0]->ring3}}",
                            @if($dataReg2[$hematologi->id]->hasil_pemeriksaan >= $grafik2[$hematologi->id][0]->ring2 && $dataReg2[$hematologi->id]->hasil_pemeriksaan < $grafik2[$hematologi->id][0]->ring3)
                                "a": "*",
                            @endif
                            @if($median2[$hematologi->id]->median >= $grafik2[$hematologi->id][0]->ring2 && $median2[$hematologi->id]->median < $grafik2[$hematologi->id][0]->ring3)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik2[$hematologi->id][0]->count_ring_2_param}},
                        },
                        {
                            "name": ">= {{$grafik2[$hematologi->id][0]->ring3}} < {{$grafik2[$hematologi->id][0]->ring4}}",
                            @if($dataReg2[$hematologi->id]->hasil_pemeriksaan >= $grafik2[$hematologi->id][0]->ring3 && $dataReg2[$hematologi->id]->hasil_pemeriksaan < $grafik2[$hematologi->id][0]->ring4)
                                "a": "*",
                            @endif
                            @if($median2[$hematologi->id]->median >= $grafik2[$hematologi->id][0]->ring3 && $median2[$hematologi->id]->median < $grafik2[$hematologi->id][0]->ring4)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik2[$hematologi->id][0]->count_ring_3_param}},
                        },
                        {
                            "name": ">= {{$grafik2[$hematologi->id][0]->ring4}} <= {{$grafik2[$hematologi->id][0]->ring5}}",
                            @if($dataReg2[$hematologi->id]->hasil_pemeriksaan >= $grafik2[$hematologi->id][0]->ring4 && $dataReg2[$hematologi->id]->hasil_pemeriksaan <= $grafik2[$hematologi->id][0]->ring5)
                                "a": "*",
                            @endif
                            @if($median2[$hematologi->id]->median >= $grafik2[$hematologi->id][0]->ring4 && $median2[$hematologi->id]->median <= $grafik2[$hematologi->id][0]->ring5)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik2[$hematologi->id][0]->count_ring_4_param}},
                        },
                        {
                            "name": "> {{$grafik2[$hematologi->id][0]->ring5}}",
                            @if($dataReg2[$hematologi->id]->hasil_pemeriksaan > $grafik2[$hematologi->id][0]->ring5)
                                "a": "*",
                            @endif
                        "color": '#BF0B23',
                            "y": {{$grafik2[$hematologi->id][0]->out_atas_param}},
                        },
                    ]
                }
            ],
        });
        var hgb2 = Highcharts.chart('containeralatb{{ $hematologi->id }}', {
            exporting: {
                enabled: true // hide button
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Grafik Distribusi Hasil Parameter {!!$parameter[$hematologi->id]->nama_parameter!!} <br> Kelompok Alat {{$instrumen->instrumen}} {{$dataReg2->instrument_lain}}',
                style : {
                    'font-size' : "10px",
                    'font-weight' : "bold",
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
            title: {
                text : 'Jumlah Peserta'
            }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y} {point.a}',
                        crop: false,
                        overflow: 'none'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
            },

            "series": [
                {
                    "name": "Nilai yang Sama",
                    "colorByPoint": false,
                    "data": [
                        {
                        "name": "< {{$grafik2[$hematologi->id][0]->ring1}}",
                        @if($dataReg2[$hematologi->id]->hasil_pemeriksaan < $grafik2[$hematologi->id][0]->ring1)
                        "a": "*",
                        @endif
                        "color": '#BF0B23',
                        "y": {{$grafik2[$hematologi->id][0]->out_bawah_alat}},
                        },
                        {
                            "name": ">= {{$grafik2[$hematologi->id][0]->ring1}} < {{$grafik2[$hematologi->id][0]->ring2}}",
                            @if($dataReg2[$hematologi->id]->hasil_pemeriksaan >= $grafik2[$hematologi->id][0]->ring1 && $dataReg2[$hematologi->id]->hasil_pemeriksaan < $grafik2[$hematologi->id][0]->ring2)
                                "a": "*",
                            @endif
                            @if(!empty($medianalat2) && $medianalat2->median >= $grafik2[$hematologi->id][0]->ring1 && $medianalat2->median < $grafik2[$hematologi->id][0]->ring2)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik2[$hematologi->id][0]->count_ring_1_alat}},
                        },
                        {
                            "name": ">= {{$grafik2[$hematologi->id][0]->ring2}} < {{$grafik2[$hematologi->id][0]->ring3}}",
                            @if($dataReg2[$hematologi->id]->hasil_pemeriksaan >= $grafik2[$hematologi->id][0]->ring2 && $dataReg2[$hematologi->id]->hasil_pemeriksaan < $grafik2[$hematologi->id][0]->ring3)
                                "a": "*",
                            @endif
                            @if(!empty($medianalat2) && $median2[$hematologi->id]->median >= $grafik2[$hematologi->id][0]->ring2 && $median2[$hematologi->id]->median < $grafik2[$hematologi->id][0]->ring3)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik2[$hematologi->id][0]->count_ring_2_alat}},
                        },
                        {
                            "name": ">= {{$grafik2[$hematologi->id][0]->ring3}} < {{$grafik2[$hematologi->id][0]->ring4}}",
                            @if($dataReg2[$hematologi->id]->hasil_pemeriksaan >= $grafik2[$hematologi->id][0]->ring3 && $dataReg2[$hematologi->id]->hasil_pemeriksaan < $grafik2[$hematologi->id][0]->ring4)
                                "a": "*",
                            @endif
                            @if(!empty($medianalat2) && $median2[$hematologi->id]->median >= $grafik2[$hematologi->id][0]->ring3 && $median2[$hematologi->id]->median < $grafik2[$hematologi->id][0]->ring4)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik2[$hematologi->id][0]->count_ring_3_alat}},
                        },
                        {
                            "name": ">= {{$grafik2[$hematologi->id][0]->ring4}} <= {{$grafik2[$hematologi->id][0]->ring5}}",
                            @if($dataReg2[$hematologi->id]->hasil_pemeriksaan >= $grafik2[$hematologi->id][0]->ring4 && $dataReg2[$hematologi->id]->hasil_pemeriksaan <= $grafik2[$hematologi->id][0]->ring5)
                                "a": "*",
                            @endif
                            @if(!empty($medianalat2) && $medianalat2->median >= $grafik2[$hematologi->id][0]->ring4 && $medianalat2->median <= $grafik2[$hematologi->id][0]->ring5)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik2[$hematologi->id][0]->count_ring_4_alat}},
                        },
                        {
                            "name": "> {{$grafik2[$hematologi->id][0]->ring5}}",
                            @if($dataReg2[$hematologi->id]->hasil_pemeriksaan > $grafik2[$hematologi->id][0]->ring5)
                                "a": "*",
                            @endif
                        "color": '#BF0B23',
                            "y": {{$grafik2[$hematologi->id][0]->out_atas_alat}},
                        },
                    ]
                }
            ],
        });
        var hgb3 = Highcharts.chart('containermetodeb{{ $hematologi->id }}', {
            exporting: {
                enabled: true // hide button
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Grafik Distribusi Hasil {!!$parameter[$hematologi->id]->nama_parameter!!} <br> Kelompok {{$metode->metode_pemeriksaan}} {{$dataReg2->metode_lain}}',
                style : {
                    'font-size' : "10px",
                    'font-weight' : "bold",
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
            title: {
                text : 'Jumlah Peserta'
            }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y} {point.a}',
                        crop: false,
                        overflow: 'none'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
            },

            "series": [
                {
                    "name": "Nilai yang Sama",
                    "colorByPoint": false,
                    "data": [
                        {
                        "name": "< {{$grafik2[$hematologi->id][0]->ring1}}",
                        @if($dataReg2[$hematologi->id]->hasil_pemeriksaan < $grafik2[$hematologi->id][0]->ring1)
                        "a": "*",
                        @endif
                        "color": '#BF0B23',
                        "y": {{$grafik2[$hematologi->id][0]->out_bawah_metode}},
                        },
                        {
                            "name": ">= {{$grafik2[$hematologi->id][0]->ring1}} < {{$grafik2[$hematologi->id][0]->ring2}}",
                            @if($dataReg2[$hematologi->id]->hasil_pemeriksaan >= $grafik2[$hematologi->id][0]->ring1 && $dataReg2[$hematologi->id]->hasil_pemeriksaan < $grafik2[$hematologi->id][0]->ring2)
                                "a": "*",
                            @endif
                            @if(!empty($medianmetode2) && $medianmetode2->median >= $grafik2[$hematologi->id][0]->ring1 && $medianmetode2->median < $grafik2[$hematologi->id][0]->ring2)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik2[$hematologi->id][0]->count_ring_1_metode}},
                        },
                        {
                            "name": ">= {{$grafik2[$hematologi->id][0]->ring2}} < {{$grafik2[$hematologi->id][0]->ring3}}",
                            @if($dataReg2[$hematologi->id]->hasil_pemeriksaan >= $grafik2[$hematologi->id][0]->ring2 && $dataReg2[$hematologi->id]->hasil_pemeriksaan < $grafik2[$hematologi->id][0]->ring3)
                                "a": "*",
                            @endif
                            @if(!empty($medianmetode2) && $medianmetode2->median >= $grafik2[$hematologi->id][0]->ring2 && $medianmetode2->median < $grafik2[$hematologi->id][0]->ring3)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik2[$hematologi->id][0]->count_ring_2_metode}},
                        },
                        {
                            "name": ">= {{$grafik2[$hematologi->id][0]->ring3}} < {{$grafik2[$hematologi->id][0]->ring4}}",
                            @if($dataReg2[$hematologi->id]->hasil_pemeriksaan >= $grafik2[$hematologi->id][0]->ring3 && $dataReg2[$hematologi->id]->hasil_pemeriksaan < $grafik2[$hematologi->id][0]->ring4)
                                "a": "*",
                            @endif
                            @if(!empty($medianmetode2) && $medianmetode2->median >= $grafik2[$hematologi->id][0]->ring3 && $medianmetode2->median < $grafik2[$hematologi->id][0]->ring4)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik2[$hematologi->id][0]->count_ring_3_metode}},
                        },
                        {
                            "name": ">= {{$grafik2[$hematologi->id][0]->ring4}} <= {{$grafik2[$hematologi->id][0]->ring5}}",
                            @if($dataReg2[$hematologi->id]->hasil_pemeriksaan >= $grafik2[$hematologi->id][0]->ring4 && $dataReg2[$hematologi->id]->hasil_pemeriksaan <= $grafik2[$hematologi->id][0]->ring5)
                                "a": "*",
                            @endif
                            @if(!empty($medianmetode2) && $medianmetode2->median >= $grafik2[$hematologi->id][0]->ring4 && $medianmetode2->median <= $grafik2[$hematologi->id][0]->ring5)
                            "color": '#1bbd20',
                            @endif
                            "y": {{$grafik2[$hematologi->id][0]->count_ring_4_metode}},
                        },
                        {
                            "name": "> {{$grafik2[$hematologi->id][0]->ring5}}",
                            @if($dataReg2[$hematologi->id]->hasil_pemeriksaan > $grafik2[$hematologi->id][0]->ring5)
                                "a": "*",
                            @endif
                        "color": '#BF0B23',
                            "y": {{$grafik2[$hematologi->id][0]->out_atas_metode}},
                        },
                    ]
                }
            ],
        });
    </script>
@endforeach


<table>
	<thead>
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">Nama Instansi</th>
			<th rowspan="2">Alamat</th>
			<th rowspan="2">No. Telpon</th>
			<th rowspan="2">Personal</th>
			<th rowspan="2">No. HP</th>
			<th rowspan="2">Email</th>
			<th colspan="3"><center>Patologi Klinik</center></th>
			<th colspan="4"><center>Mikrobiologi</center></th>
			<th colspan="4"><center>Immunologi</center></th>
			<th colspan="2"><center>Kimia Kesehatan</center></th>
			<th rowspan="2">Total (Rp)</th>
		</tr>
		<tr>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		@if(count($data))
		<?php $no = 0; ?>
		@foreach($data as $val)
		<?php 
			$no++;
		?>
		<tr>
			<td>{{$no}}</td>
			<td>{{$val->Nama}}</td>
			<td>{{$val->alamat}}</td>
			<td>{{$val->Telp}}</td>
			<td>{{$val->personal}}</td>
			<td>{{$val->no_hp}}</td>
			<td>{{$val->email}}</td>
			<td style="text-align: right;">
				@if(count($val->h))
				{{number_format($val->h[0]->tarif)}}
				@endif
			</td>
			<td style="text-align: right;">
				@if(count($val->kk))
				{{number_format($val->kk[0]->tarif)}}
				@endif
			</td>
			<td style="text-align: right;">
				@if(count($val->uri))
				{{number_format($val->uri[0]->tarif)}}
				@endif
			</td>
			<td style="text-align: right;">
				@if(count($val->bta))
				{{number_format($val->bta[0]->tarif)}}
				@endif
			</td>
			<td style="text-align: right;">
				@if(count($val->tc))
				{{number_format($val->tc[0]->tarif)}}
				@endif
			</td>
			<td style="text-align: right;">
				@if(count($val->mal))
				{{number_format($val->mal[0]->tarif)}}
				@endif
			</td>
			<td style="text-align: right;">
				@if(count($val->bac))
				{{number_format($val->bac[0]->tarif)}}
				@endif
			</td>
			<td style="text-align: right;">
				@if(count($val->ahiv))
				{{number_format($val->ahiv[0]->tarif)}}
				@endif
			</td>
			<td style="text-align: right;">
				@if(count($val->syph))
				{{number_format($val->syph[0]->tarif)}}
				@endif
			</td>
			<td style="text-align: right;">
				@if(count($val->hbsag))
				{{number_format($val->hbsag[0]->tarif)}}
				@endif
			</td>
			<td style="text-align: right;">
				@if(count($val->ahcv))
				{{number_format($val->ahcv[0]->tarif)}}
				@endif
			</td>
			<td style="text-align: right;">
				@if(count($val->kai))
				{{number_format($val->kai[0]->tarif)}}
				@endif
			</td>
			<td style="text-align: right;">
				@if(count($val->kat))
				{{number_format($val->kat[0]->tarif)}}
				@endif
			</td>
			<td style="text-align: right;">
				@if(count($val->tot))
				{{number_format($val->tot)}}
				@endif
			</td>
		</tr>
		<?php unset($hema, $kk, $uri, $bta, $tc, $ahiv, $ahcv, $syph, $hbsag, $paket); ?>
		@endforeach
		@endif
		<tr>
			<td colspan="20" style="text-align: right;">Total (Rp) :</td>
			<td class="hasil"></td>
		</tr>
	</tbody>
</table>
@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Laporan Tanda Terima Bahan</div>

                <div class="panel-body">
                    <form method="post" id="evaluasi" enctype="multipart/form-data" target="_blank">
                      <div class="form-group">
                        <label for="bidang" class="control-label">Bidang</label>
                            <select class="form-control" name="bidang" id="bidang">
                                <option></option>
                                @foreach($bidang as $val)
                                <option value="{{$val->id}}">{{$val->parameter}}</option>
                                @endforeach
                            </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control" name="tahun">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div>
                      <div class="form-group">
                        <label for="siklus" class="control-label">Siklus</label>
                            <select class="form-control" name="siklus" id="siklus">
                                <option></option>
                                <option value="1">Siklus 1</option>
                                <option value="2">Siklus 2</option>
                            </select>
                      </div>

                      {{ csrf_field() }}
                      <button type="submit" name="proses" class="btn btn-info" onclick="viewtable()">Proses</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});

form=document.getElementById("evaluasi");
function viewtable() {
        form.action="{{ url('laporan-tanda-terima-bahan')}}";
        form.submit();
}
function printpdf() {
        form.action="{{ url('laporan-tanda-terima-bahan')}}";
        form.submit();
}
function printexcel() {
        form.action="{{ url('laporan-tanda-terima-bahan')}}";
        form.submit();
}
</script>
@endsection
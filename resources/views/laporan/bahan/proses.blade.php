<table>
	<thead>
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">Perusahaan</th>
			<th rowspan="2">Kode Lab</th>
			<th rowspan="2">Penerima</th>
			<th rowspan="2">Provinsi</th>
			<th rowspan="2">Kota</th>
			<th rowspan="2">Kecamatan</th>
			<th rowspan="2">Alamat</th>
			<?php
				if ($input['bidang'] <= 3) {
					$bidang = 2; 
				}elseif ($input['bidang'] == 4 || $input['bidang'] == 10) {
					$bidang = 10; 
				}elseif ($input['bidang'] == 5) {
					$bidang = 4;
				}elseif ($input['bidang'] >= 6 && $input['bidang'] <= 9) {
					$bidang = 5;
				}elseif ($input['bidang'] == 11 || $input['bidang'] == 12) {
					$bidang = 1;
				}elseif ($input['bidang'] == 13) {
					$bidang = 3;
				}
				for ($i=1; $i <= $bidang ; $i++) { 
			?>
				<th colspan="4">Bahan @if($i < 10) 0{{$i}} @else {{$i}} @endif</th>
			<?php } ?>
		</tr>
		<tr>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<?php
				for ($i=1; $i <= $bidang ; $i++) { 
			?>
				<th>Tanggal Penerimaan</th>
				<th>Kondisi Bahan</th>
				<th>Kode Bahan</th>
				<th>Keterangan</th>
			<?php } ?>
		</tr>
	</thead>
	<tbody>
		@foreach($data as $key => $val)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$val->nama_lab}}</td>
				<td>{{$val->kode_lebpes}}</td>
				<td>{{$val->penerima}}</td>
				<td>{{$val->Provinsi}}</td>
				<td>{{$val->Kota}}</td>
				<td>{{$val->Kecamatan}}</td>
				<td>{{$val->alamat}}</td>
				@foreach($val->bahan as $bahan)
				<td>{{$bahan->tanggal_penerimaan}}</td>
				<td>{{$bahan->kondisi_bahan}}</td>
				<td>{{$bahan->kode}}</td>
				<td>{{$bahan->keterangan}}</td>
				@endforeach
			</tr>
		@endforeach
	</tbody>
</table>
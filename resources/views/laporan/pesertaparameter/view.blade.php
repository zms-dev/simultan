<table id="peserta_pme" border="1">
	<thead>
		<tr>
			<th colspan="12" style="text-align: center;">Data Peserta {{$input['tahun']}} Siklus 1 & 2</th>
		</tr>
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">Kode Peserta</th>
			<th rowspan="2">Nomor TU</th>
			<th rowspan="2">Nama Instansi</th>
			@if($parameter->id <= 3)
			<th rowspan="2">Periode</th>
			@endif
			@if($parameter->id == '7' || $parameter->id == '3' || $parameter->id == '2' || $parameter->id == '1')
			<th colspan="2">Siklus 1</th>
			<th colspan="2">Siklus 2</th>
			@else
			<th rowspan="2">Siklus 1</th>
			<th rowspan="2">Siklus 2</th>
			@endif
			<th rowspan="2">Parameter</th>
			<th rowspan="2">Nama PJ Lab</th>
			<th rowspan="2">Nomer HP PJ lab</th>
			<th rowspan="2">Alamat Lab</th>
			<th rowspan="2">Kabupaten</th>
			<th rowspan="2">Propinsi</th>
			<th rowspan="2">Telp</th>
			<th rowspan="2">Email</th>
		</tr>
		<tr>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			@if($parameter->id == '7')
			<th>TP</th>
			<th>RPR</th>
			<th>TP</th>
			<th>RPR</th>
			@elseif($parameter->id == '3' || $parameter->id == '2' || $parameter->id == '1')
			<th></th>
			<th>I-01</th>
			<th>I-02</th>
			<th>II-01</th>
			<th>II-02</th>
			@else
			<th></th>
			<th></th>
			@endif
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		@if(count($data))
		<?php $no = 0; ?>
		@foreach($data as $val)
		<?php $no++;?>
		<tr>
			<td>{{$no}}</td>
			<td>{{$val->kode_lebpes}}</td>
			<td class="ada">{{substr($val->kode_lebpes,0, -9)}}PME/12{{substr($val->kode_lebpes, 11)}}</td>
			<td>{{$val->nama_lab}}</td>
			@if($parameter->id <= 3)
			<td>
				@if($val->periode == 2)
				2
				@else
				1
				@endif
			</td>
			@endif
			@if($parameter->id == '7')
			<td>
				@if($val->status_data1 == '2')
					Kirim Hasil
				@elseif($val->status_data1 == '1')
					Edit Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			<td>
				@if($val->status_datarpr1 == '2')
					Kirim Hasil
				@elseif($val->status_datarpr1 == '1')
					Edit Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			<td>
				@if($val->status_data2 == '2')
					Kirim Hasil
				@elseif($val->status_data2 == '1')
					Edit Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			<td>
				@if($val->status_datarpr2 == '2')
					Kirim Hasil
				@elseif($val->status_datarpr2 == '1')
					Edit Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			@elseif($parameter->id == '3' || $parameter->id == '2' || $parameter->id == '1')
			<td>
				@if($val->status_data1 == '2')
					Kirim Hasil
				@elseif($val->status_data1 == '1')
					Edit Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			<td>
				@if($val->status_data2 == '2')
					Kirim Hasil
				@elseif($val->status_data2 == '1')
					Edit Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			<td>
				@if($val->status_datarpr1 == '2')
					Kirim Hasil
				@elseif($val->status_datarpr1 == '1')
					Edit Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			<td>
				@if($val->status_datarpr2 == '2')
					Kirim Hasil
				@elseif($val->status_datarpr2 == '1')
					Edit Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			@else
			<td>
				@if($val->status_data1 == '2')
					Kirim Hasil
				@elseif($val->status_data1 == '1')
					Edit Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			<td>
				@if($val->status_data2 == '2')
					Kirim Hasil
				@elseif($val->status_data2 == '1')
					Edit Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			@endif
			<td>
				@if($val->bidang == '6')
					@if($val->pemerintah == '9' || $val->pemerintah == '8')
						HIV PMI
					@else
						{{$val->alias}}
					@endif
				@else
					{{$val->alias}}
				@endif
			</td>
			<td>{{$val->penanggung_jawab}}</td>
			<td>{{$val->no_hp}}</td>
			<td>{{$val->alamat}}</td>
			<td>{{$val->Kota}}</td>
			<td>{{$val->Provinsi}}</td>
			<td>{{$val->telp}}</td>
			<td>{{$val->email}}</td>
			
		</tr>
		@endforeach
		@endif
	</tbody>
</table>
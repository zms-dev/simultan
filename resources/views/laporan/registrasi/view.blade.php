
<table>
	<thead>
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">NO VA</th>
			<th rowspan="2">Kode Instansi</th>
			<th rowspan="2">Nama Instansi</th>
			<th rowspan="2">Alamat</th>
			<th rowspan="2">Jenis Instansi</th>
			<th rowspan="2">Provinsi</th>
			<th rowspan="2">Kabupaten / Kota</th>
			<th rowspan="2">No. Telpon</th>
			<th rowspan="2">Personal</th>
			<th rowspan="2">No. HP</th>
			<th rowspan="2">Pemantapan Mutu Internal</th>
			<th rowspan="2">Email Daftar PNPME</th>
			<th rowspan="2">Email Login</th>
			<th rowspan="2">Siklus</th>
			<th colspan="3"><center>Patologi Klinik</center></th>
			<th colspan="3"><center>Mikrobiologi</center></th>
			<th colspan="4"><center>Immunologi</center></th>
			<th colspan="2"><center>Kimia Kesehatan</center></th>
			<th rowspan="2">Bakteri</th>
		</tr>
		<tr>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		@if(count($data))
		<?php $no = 0; ?>
		@foreach($data as $val)
		<?php $no++;?>
		<tr>
			<td>{{$no}}</td>
			<td>@if($val->no_urut != NULL || $val->no_urut != '') {{$val->no_urut}} @else PKS @endif</td>
			<td>{{substr($val->kode_lebpes,0, 4)}}</td>
			<td>{{$val->Nama}}</td>
			<td>{{$val->alamat}}</td>
			<td>{{$val->Jenisbadan}}</td>
			<td>{{$val->Provinsi}}</td>
			<td>{{$val->Kota}}</td>
			<td>{{$val->Telp}}</td>
			<td>{{$val->personal}}</td>
			<td>{{$val->no_hp}}</td>
			<td>{{$val->pemantapan_mutu}} @if($val->pemantapan_mutu == 'Dilaksanakan'):@endif {{$val->dilaksanakan}}</td>
			<td>{{$val->pemail}}</td>
			<td>{{$val->email}}</td>
			<td>@if($val->siklus == 12 ) 1&2 @else {{$val->siklus}} @endif</td>
			@if($val->h == 1)
			<td>v</td>
			@else
			<td></td>
			@endif
			@if($val->kk == 1)
			<td>v</td>
			@else
			<td></td>
			@endif
			@if($val->uri == 1)
			<td>v</td>
			@else
			<td></td>
			@endif
			@if($val->bta == 1)
			<td>v</td>
			@else
			<td></td>
			@endif
			@if($val->tc == 1)
			<td>v</td>
			@else
			<td></td>
			@endif
			@if($val->mal == 1)
			<td>v</td>
			@else
			<td></td>
			@endif
			@if($val->ahiv == 1)
			<td>v</td>
			@else
			<td></td>
			@endif
			@if($val->syph == 1)
			<td>v</td>
			@else
			<td></td>
			@endif
			@if($val->hbsag == 1)
			<td>v</td>
			@else
			<td></td>
			@endif
			@if($val->ahcv == 1)
			<td>v</td>
			@else
			<td></td>
			@endif
			@if($val->kai == 1)
			<td>v</td>
			@else
			<td></td>
			@endif
			@if($val->kat == 1)
			<td>v</td>
			@else
			<td></td>
			@endif
			@if($val->bac == 1)
			<td>v</td>
			@else
			<td></td>
			@endif
		</tr>
		@endforeach
		@endif
	</tbody>
</table>
<table border="1">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Instansi</th>
            <th>Jenis Instansi</th>
            @foreach($parameterna as $val)
            <th>{!!$val->nama_parameter!!}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
      @php
        $no=0;
      @endphp
        @foreach($data as $val)
          @php
            $no++;
          @endphp
        <tr>
            <th>{{$no}}</th>
            <td>{{$val->nama_lab}}</td>
            <td>{{$val->badan_usaha}}</td>
            @foreach($val->parameter as $par)
                @foreach($par->detail as $hasil)
                    <td>'{{$hasil->hasil_pemeriksaan}}</td>
                @endforeach
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>

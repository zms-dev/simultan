
<table border="1">
	<thead>
		<tr>
			<th colspan="27" style="text-align: center; font-size: 16px">Rekapitulasi Nilai Peserta PME</th>
		</tr>
		<tr>
			<th rowspan="2">No</th>
      <th rowspan="2">Nama Instansi</th>
      <th rowspan="2">Jenis Instansi</th>
      @foreach ($parameterna as $key => $val)
        <th colspan="3">{!!$val->nama_parameter!!}</th>
      @endforeach
		</tr>
    <tr>
      @foreach ($parameterna as $key => $val)
				<th>Hasil</th>
        <th>Metode</th>
        <th>Alat</th>
      @endforeach
    </tr>
	</thead>
  @php
    $no = 0;
  @endphp
	<tbody>
		@foreach ($data as $key => $value)
			@php
        $no++;
      @endphp
			<tr>
				<td>{{$no}}</td>
        <td>{{$value->nama_lab}}</td>
        <td>{{$value->badan_usaha}}</td>
				@foreach($value->parameter as $par)
							@if(count($par->detail))
								@foreach($par->detail as $za)
									<td>
										{{$za->hasil_pemeriksaan}}
									</td>
									<td>
										{{$za->metode_pemeriksaan}}
									</td>
									<td>{{$za->instrumen}} {{$za->instrument_lain}}</td>
								@endforeach
							@else
								<td>-</td>
							@endif
				@endforeach
			</tr>
		@endforeach
  </tbody>
</table>

@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Laporan Jumlah Peserta per Provinsi per Bidang</div>

                <div class="panel-body">
                    <p>Tekan refresh ketika ingin pilih Tahun atau Bidang Baru<input type="submit"  onclick="document.location.reload(true)" value="Refresh" class="btn btn-primary right"></p>
                    <form class="" lass="form-horizontal" method="post" id="uangmasuk" enctype="multipart/form-data" target="_blank">
                      <div>
                          <?php
                              $sub = DB::table('sub_bidang')->get();
                          ?>
                          <label for="exampleInputEmail1">Bidang</label>
                          <div>
                              <select name="sub" class="form-control" id="sub" required>
                                <option></option>
                                <option value="all">SEMUA BIDANG</option>
                                @foreach($sub as $alias)
                                <option value="{{ $alias->alias }}">{{$alias->alias}}</option>
                                @endforeach  
                              </select>
                          </div><br>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-date="2017" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control" name="tahun" id="tahun" required>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div><br>
                      <div>
                        <input type="submit" name="export" value="Download Excel" class="btn btn-primary" >&nbsp;
                        <input type="submit" name="export" value="Export PDF" class="btn btn-primary" >
                      </div><br>
                      
                      <div class="table-responsive" id="semua">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                                <th>No</th>
                                <th>Provinsi</th>
                                <th>HEM</th>
                                <th>KKL</th>
                                <th>URI</th>
                                <th>BTA</th>
                                <th>TCC</th>
                                <th>HIV</th>
                                <th>SIF</th>
                                <th>HBS</th>
                                <th>HCV</th>
                                <th>MAL</th>
                                <th>KAI</th>
                                <th>KAT</th>
                                <th>BAC</th>
                                <th>Total</th>
                            </tr>
                          </thead>
                          <tbody class="body-siklus">
                            
                          </tbody>
                          <tbody class="body-total">
                            
                          </tbody>
                        </table>

                        <div id="chart2" style="height: 1000px;"></div>
                      </div>

                      <div class="table-responsive" id="bagian">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                                <th>No</th>
                                <th>Provinsi</th>
                                <th>Total</th>
                            </tr>
                          </thead>
                          <tbody class="body-siklus">
                            
                          </tbody>
                          <tbody class="body-total1">
                            
                          </tbody>
                        </table>

                        <div id="chart2na" style="height: 1000px;"></div>
                      </div>


                      <br>

                      {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){

  $(document).on('submit','#uangmasuk',function() {
    var tahun = $('#tahun').val();
    if(tahun == ''){
      alert("Harap isi input Tahun!");
      return false;
    }
  });
});  

$("#semua").hide();
$("#bagian").hide();
$('#sub').change(function(e){
  console.log($(this).val());
  if ($(this).val() == 'all') {
    $("#semua").show();
    $("#bagian").hide();
  }else{
    $("#bagian").show();
    $("#semua").hide();
  }
}); 



$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});


$("#tahun").change(function(){
  var val = $(this).val(), i, no = 0
  $(".body-siklus").html("<tr><td colspan='15'>Please Wait... </td></tr>")
  $.ajax({
    type: "GET",
    url : "{{url('datapesertaprovinsi').'/'}}"+val,
    success: function(addr){
      // y.innerHTML = '<tr id="datasiklus"><td id="nosiklus"></td><td id="paramsiklus"></td><td id="bidangsiklus"></td><td id="tarifsiklus"></td></tr>';
      $(".body-siklus").html("");
        if(addr.Hasil != undefined){
          if ($("#sub").val() == "all") {
          var no = 0;
          var hem = no;
          var kkl = no;
          var uri = no;
          var bta = no;
          var tcc = no;
          var hiv = no;
          var sif = no;
          var hbs = no;
          var hcv = no;
          var mal = no;
          var kai = no;
          var kat = no;
          var bac = no;
          var tot = no;
          $.each(addr.Hasil,function(e,item){
            no++;
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.hem+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.kkl+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.uri+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.bta+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.tcc+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.hiv+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.sif+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.hbs+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.hcv+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.mal+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.kai+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.kat+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.bac+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.tot+"</td>";
              $(".body-siklus").append(html);
              hem = hem + item.hem;
              kkl = kkl + item.kkl;
              uri = uri + item.uri;
              bta = bta + item.bta;
              tcc = tcc + item.tcc;
              hiv = hiv + item.hiv;
              sif = sif + item.sif;
              hbs = hbs + item.hbs;
              hcv = hcv + item.hcv;
              mal = mal + item.mal;
              kai = kai + item.kai;
              kat = kat + item.kat;
              bac = bac + item.bac;
              tot = tot + item.tot;
          })
          var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+hem+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+kkl+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+uri+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+bta+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+tcc+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+hiv+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+sif+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+hbs+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+hcv+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+mal+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+kai+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+kat+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+bac+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+tot+"</td></tr>";
          $(".body-total").append(htm);
        }else if($("#sub").val() == "HEM"){
          var no = 0;
          var hem = no;
          $.each(addr.Hasil,function(e,item){
            no++;
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.hem+"</td></tr>";
              $(".body-siklus").append(html);
              hem = hem + item.hem;
          })
          var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+hem+"</td></tr>";
          $(".body-total1").append(htm);
        }else if($("#sub").val() == "KKL"){
          var no = 0;
          var kkl = no;
          $.each(addr.Hasil,function(e,item){
            no++;
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.kkl+"</td></tr>";
              $(".body-siklus").append(html);
              kkl = kkl + item.kkl;
          })
          var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+kkl+"</td></tr>";
          $(".body-total1").append(htm);
        }else if($("#sub").val() == "KKL"){
          var no = 0;
          var kkl = no;
          $.each(addr.Hasil,function(e,item){
            no++;
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.kkl+"</td></tr>";
              $(".body-siklus").append(html);
              kkl = kkl + item.kkl;
          })
          var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+kkl+"</td></tr>";
          $(".body-total1").append(htm);
        }else if($("#sub").val() == "KKL"){
          var no = 0;
          var kkl = no;
          $.each(addr.Hasil,function(e,item){
            no++;
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.kkl+"</td></tr>";
              $(".body-siklus").append(html);
              kkl = kkl + item.kkl;
          })
          var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+kkl+"</td></tr>";
          $(".body-total1").append(htm);
        }else if($("#sub").val() == "URI"){
          var no = 0;
          var uri = no;
          $.each(addr.Hasil,function(e,item){
            no++;
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.uri+"</td></tr>";
              $(".body-siklus").append(html);
              uri = uri + item.uri;
          })
          var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+uri+"</td></tr>";
          $(".body-total1").append(htm);
        }else if($("#sub").val() == "BTA"){
          var no = 0;
          var bta = no;
          $.each(addr.Hasil,function(e,item){
            no++;
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.bta+"</td></tr>";
              $(".body-siklus").append(html);
              bta = bta + item.bta;
          })
          var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+bta+"</td></tr>";
          $(".body-total1").append(htm);
        }else if($("#sub").val() == "TCC"){
          var no = 0;
          var tcc = no;
          $.each(addr.Hasil,function(e,item){
            no++;
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.tcc+"</td></tr>";
              $(".body-siklus").append(html);
              tcc = tcc + item.tcc;
          })
          var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+tcc+"</td></tr>";
          $(".body-total1").append(htm);
        }else if($("#sub").val() == "HIV"){
          var no = 0;
          var hiv = no;
          $.each(addr.Hasil,function(e,item){
            no++;
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.hiv+"</td></tr>";
              $(".body-siklus").append(html);
              hiv = hiv + item.hiv;
          })
          var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+hiv+"</td></tr>";
          $(".body-total1").append(htm);
        }else if($("#sub").val() == "SIF"){
          var no = 0;
          var sif = no;
          $.each(addr.Hasil,function(e,item){
            no++;
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.sif+"</td></tr>";
              $(".body-siklus").append(html);
              sif = sif + item.sif;
          })
          var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+sif+"</td></tr>";
          $(".body-total1").append(htm);
        }else if($("#sub").val() == "HBS"){
          var no = 0;
          var hbs = no;
          $.each(addr.Hasil,function(e,item){
            no++;
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.hbs+"</td></tr>";
              $(".body-siklus").append(html);
              hbs = hbs + item.hbs;
          })
          var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+hbs+"</td></tr>";
          $(".body-total1").append(htm);
        }else if($("#sub").val() == "HCV"){
          var no = 0;
          var hcv = no;
          $.each(addr.Hasil,function(e,item){
            no++;
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.hcv+"</td></tr>";
              $(".body-siklus").append(html);
              hcv = hcv + item.hcv;
          })
          var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+hcv+"</td></tr>";
          $(".body-total1").append(htm);
        }else if($("#sub").val() == "MAL"){
          var no = 0;
          var mal = no;
          $.each(addr.Hasil,function(e,item){
            no++;
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.mal+"</td></tr>";
              $(".body-siklus").append(html);
              mal = mal + item.mal;
          })
          var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+mal+"</td></tr>";
          $(".body-total1").append(htm);
        }else if($("#sub").val() == "KAI"){
          var no = 0;
          var kai = no;
          $.each(addr.Hasil,function(e,item){
            no++;
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.kai+"</td></tr>";
              $(".body-siklus").append(html);
              kai = kai + item.kai;
          })
          var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+kai+"</td></tr>";
          $(".body-total1").append(htm);
        }else if($("#sub").val() == "KAT"){
          var no = 0;
          var kat = no;
          $.each(addr.Hasil,function(e,item){
            no++;
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.kat+"</td></tr>";
              $(".body-siklus").append(html);
              kat = kat + item.kat;
          })
          var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+kat+"</td></tr>";
          $(".body-total1").append(htm);
        }else if($("#sub").val() == "BAC"){
          var no = 0;
          var bac = no;
          $.each(addr.Hasil,function(e,item){
            no++;
              var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.bac+"</td></tr>";
              $(".body-siklus").append(html);
              bac = bac + item.bac;
          })
          var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+bac+"</td></tr>";
          $(".body-total1").append(htm);
        }    
      }
      return false;
    }
  });
});

$("#tahun").change(function(){
  var val = $('#tahun').val(), i, no = 0;

  $(function(){
    var chart2 = new Highcharts.Chart({
      chart: {
        renderTo: 'chart2',
        type: 'column',
        inverted: true,
        polar: false,
        events: {
          load: requestDataC1
        }
      },

    //   chart: {
    // },

      title: {
        text : 'Rekap Jumlah Peserta per Provinsi per Bidang  ' + val 
      },
      xAxis: {
        type: 'category'
      },
      yAxis: {
        title: {
          text : 'Jumlah Peserta'
        }
      },
      legend: {
        enabled : false
      },
      plotOptions: {
        series : {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            pointformat : '{point.y}'
          }
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br>'
      },
      series: [{
        name: 'Golongan',
        colorByPoint: true,
        data: []
      }]
    });
    function requestDataC1(){
      $.getJSON("{{ URL('/datapesertaprovinsi').'/'}}"+val, function(json){
        chart2.series[0].setData(json['rows'],true)
        console.log(json['rows']);
      });
    }
  });
});

$("#tahun").change(function(){
  var val = $('#tahun').val(), i, no = 0;

  $(function(){
    var chart2na = new Highcharts.Chart({
      chart: {
        renderTo: 'chart2na',
        type: 'column',
        inverted: true,
        polar: false,
        events: {
          load: requestDataC1
        }
      },

    //   chart: {
    // },

      title: {
        text : 'Rekap Jumlah Peserta per Provinsi per Bidang  ' + val 
      },
      xAxis: {
        type: 'category'
      },
      yAxis: {
        title: {
          text : 'Jumlah Peserta'
        }
      },
      legend: {
        enabled : false
      },
      plotOptions: {
        series : {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            pointformat : '{point.y}'
          }
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br>'
      },
      series: [{
        name: 'Golongan',
        colorByPoint: true,
        data: []
      }]
    });
    function requestDataC1(){
      if ($("#sub").val() == "HEM") {
        $.getJSON("{{ URL('/datapesertaprovinsi').'/'}}"+val, function(json){
          chart2na.series[0].setData(json['rowshem'],true)
          console.log(json['rowshem']);
        });
      }else if ($("#sub").val() == "KKL") {
        $.getJSON("{{ URL('/datapesertaprovinsi').'/'}}"+val, function(json){
          chart2na.series[0].setData(json['rowskkl'],true)
          console.log(json['rowskkl']);
        });
      }else if ($("#sub").val() == "URI") {
        $.getJSON("{{ URL('/datapesertaprovinsi').'/'}}"+val, function(json){
          chart2na.series[0].setData(json['rowsuri'],true)
          console.log(json['rowsuri']);
        });
      }else if ($("#sub").val() == "BTA") {
        $.getJSON("{{ URL('/datapesertaprovinsi').'/'}}"+val, function(json){
          chart2na.series[0].setData(json['rowsbta'],true)
          console.log(json['rowsbta']);
        });
      }else if ($("#sub").val() == "TCC") {
        $.getJSON("{{ URL('/datapesertaprovinsi').'/'}}"+val, function(json){
          chart2na.series[0].setData(json['rowstcc'],true)
          console.log(json['rowstcc']);
        });
      }else if ($("#sub").val() == "HIV") {
        $.getJSON("{{ URL('/datapesertaprovinsi').'/'}}"+val, function(json){
          chart2na.series[0].setData(json['rowshiv'],true)
          console.log(json['rowshiv']);
        });
      }else if ($("#sub").val() == "SIF") {
        $.getJSON("{{ URL('/datapesertaprovinsi').'/'}}"+val, function(json){
          chart2na.series[0].setData(json['rowssif'],true)
          console.log(json['rowssif']);
        });
      }else if ($("#sub").val() == "HBS") {
        $.getJSON("{{ URL('/datapesertaprovinsi').'/'}}"+val, function(json){
          chart2na.series[0].setData(json['rowshbs'],true)
          console.log(json['rowshbs']);
        });
      }else if ($("#sub").val() == "HCV") {
        $.getJSON("{{ URL('/datapesertaprovinsi').'/'}}"+val, function(json){
          chart2na.series[0].setData(json['rowshcv'],true)
          console.log(json['rowshcv']);
        });
      }else if ($("#sub").val() == "MAL") {
        $.getJSON("{{ URL('/datapesertaprovinsi').'/'}}"+val, function(json){
          chart2na.series[0].setData(json['rowsmal'],true)
          console.log(json['rowsmal']);
        });
      }else if ($("#sub").val() == "KAI") {
        $.getJSON("{{ URL('/datapesertaprovinsi').'/'}}"+val, function(json){
          chart2na.series[0].setData(json['rowskai'],true)
          console.log(json['rowskai']);
        });
      }else if ($("#sub").val() == "KAT") {
        $.getJSON("{{ URL('/datapesertaprovinsi').'/'}}"+val, function(json){
          chart2na.series[0].setData(json['rowskat'],true)
          console.log(json['rowskat']);
        });
      }else if ($("#sub").val() == "BAC") {
        $.getJSON("{{ URL('/datapesertaprovinsi').'/'}}"+val, function(json){
          chart2na.series[0].setData(json['rowsbac'],true)
          console.log(json['rowsbac']);
        });
      }
    }
  });
});

function reloadpage(){
  location.reload();
}
</script>
@endsection
@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Rekapitulasi Tarif Peserta PME Berdasarkan Jenis Instansi dan Parameter</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post" id="uangmasuk" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-date="2017" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control" name="tahun" id="tahun" required>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div><br>
                      <div>
                        <input type="submit" name="export" value="Export Excel" class="btn btn-primary">&nbsp;
                        <input type="submit" name="export" value="Export PDF" class="btn btn-primary">
                      </div><br>
                      <div class="table-responsive">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                                <th>Parameter</th>
                                <th>BBLK</th>
                                <th>BLK</th>
                                <th>RS-Pemerintah</th>
                                <th>Puskesmas</th>
                                <th>Labkesda</th>
                                <th>Rs-Swasta</th>
                                <th>LabKlinik Swasta</th>
                                <th>UTD-RS</th>
                                <th>UTD-PMI</th>
                                <th>RS TNI/Polri</th>
                            </tr>
                          </thead>
                          <tbody class="body-siklus">
                            
                          </tbody>
                          <tbody class="body-total">
                            
                          </tbody>
                        </table>
                      </div>
                          <br>

                      {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});

function formatnumber(n) {
    return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
}

$("#tahun").change(function(){
  var val = $(this).val(), i, no = 0
  $(".body-siklus").html("<tr><td colspan='11'>Please Wait... </td></tr>")
  $.ajax({
    type: "GET",
    url : "{{url('datapesertatransaksi').'/'}}"+val,
    success: function(addr){
      // y.innerHTML = '<tr id="datasiklus"><td id="nosiklus"></td><td id="paramsiklus"></td><td id="bidangsiklus"></td><td id="tarifsiklus"></td></tr>';
      $(".body-siklus").html("");
      if(addr.Hasil != undefined){
        var no = 0;
        var bblk = no;
        var blk = no;
        var rspem = no;
        var pus = no;
        var labkes = no;
        var rsswa = no;
        var labkl = no;
        var utdrs = no;
        var utdpmi = no;
        var rstni = no;
        $.each(addr.Hasil,function(e,item){
          var format1 = formatnumber(item.bblk);
          var format2 = formatnumber(item.blk);
          var format3 = formatnumber(item.rspem);
          var format4 = formatnumber(item.pus);
          var format5 = formatnumber(item.labkes);
          var format6 = formatnumber(item.rsswa);
          var format7 = formatnumber(item.labkl);
          var format8 = formatnumber(item.utdrs);
          var format9 = formatnumber(item.utdpmi);
          var format10 = formatnumber(item.rstni);
          no++;
            var html = "<tr><td class=\"paramsiklus\">"+item.alias+"</td><td style=\"text-align: right;\">"+format1+"</td><td style=\"text-align: right;\">"+format2+"</td><td style=\"text-align: right;\">"+format3+"</td><td style=\"text-align: right;\">"+format4+"</td><td style=\"text-align: right;\">"+format5+"</td><td style=\"text-align: right;\">"+format6+"</td><td style=\"text-align: right;\">"+format7+"</td><td style=\"text-align: right;\">"+format8+"</td><td style=\"text-align: right;\">"+format9+"</td><td style=\"text-align: right;\">"+format10+"</td>";
            $(".body-siklus").append(html);
            bblk = bblk + item.bblk;
            blk = blk + item.blk;
            rspem = rspem + item.rspem;
            pus = pus + item.pus;
            labkes = labkes + item.labkes;
            rsswa = rsswa + item.rsswa;
            labkl = labkl + item.labkl;
            utdrs = utdrs + item.utdrs;
            utdpmi = utdpmi + item.utdpmi;
            rstni = rstni + item.rstni;
        })

            var jbblk = formatnumber(bblk);
            var jblk = formatnumber(blk);
            var jrspem = formatnumber(rspem);
            var jpus = formatnumber(pus);
            var jlabkes = formatnumber(labkes);
            var jrsswa = formatnumber(rsswa);
            var jlabkl = formatnumber(labkl);
            var jutdrs = formatnumber(utdrs);
            var jutdpmi = formatnumber(utdpmi);
            var jrstni = formatnumber(rstni);
        var htm = "<tr><td class=\"nosiklus\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+jbblk+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+jblk+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+jrspem+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+jpus+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+jlabkes+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+jrsswa+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+jlabkl+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+jutdrs+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+jutdpmi+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+jrstni+"</td>";
        $(".body-total").append(htm);
      }
      return false;
    }
  });
});
</script>
@endsection
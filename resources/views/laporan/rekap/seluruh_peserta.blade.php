@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Rekap Seluruh Peserta dan Bidang</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post" id="uangmasuk" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-date="2017" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control" name="tahun" id="tahun" required>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div>
                      <div class="">
                          <label for="">Bidang</label>
                          <div>
                            <select class="form-control" name="bidang" id="bidang">
                              <option value=""></option>
                              @foreach ($bidang as $val)
                                <option value="{{$val->id}}">{{$val->alias}}</option>
                              @endforeach($bidang => $val)
                            </select>
                          </div>
                      </div>
                      <br>
                      <div>
                        <input type="submit" name="export" value="Export Excel" class="btn btn-primary">&nbsp;
                        <input type="submit" name="export" value="Export PDF" class="btn btn-primary">
                      </div><br>

                      {{-- <div class="table-responsive">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                                <th>No</th>
                                <th>Provinsi</th>
                                <th>HEM</th>
                                <th>KKL</th>
                                <th>URI</th>
                                <th>BTA</th>
                                <th>TCC</th>
                                <th>HIV</th>
                                <th>SIF</th>
                                <th>HBS</th>
                                <th>HCV</th>
                                <th>MAL</th>
                                <th>KAI</th>
                                <th>KAT</th>
                                <th>BAC</th>
                                <th>Total</th>
                            </tr>
                          </thead>
                          <tbody class="body-siklus">

                          </tbody>
                          <tbody class="body-total">

                          </tbody>
                        </table>
                      </div> --}}
                      <br>

                      {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});


$("#tahunasd").change(function(){
  var val = $(this).val(), i, no = 0
  $(".body-siklus").html("<tr><td colspan='15'>Please Wait... </td></tr>")
  $.ajax({
    type: "GET",
    url : "{{url('datapesertaprovinsi').'/'}}"+val,
    success: function(addr){
      // y.innerHTML = '<tr id="datasiklus"><td id="nosiklus"></td><td id="paramsiklus"></td><td id="bidangsiklus"></td><td id="tarifsiklus"></td></tr>';
      $(".body-siklus").html("");
      if(addr.Hasil != undefined){
        var no = 0;
        var hem = no;
        var kkl = no;
        var uri = no;
        var bta = no;
        var tcc = no;
        var hiv = no;
        var sif = no;
        var hbs = no;
        var hcv = no;
        var mal = no;
        var kai = no;
        var kat = no;
        var bac = no;
        var tot = no;
        $.each(addr.Hasil,function(e,item){
          no++;
            var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.hem+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.kkl+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.uri+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.bta+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.tcc+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.hiv+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.sif+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.hbs+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.hcv+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.mal+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.kai+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.kat+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.bac+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.tot+"</td>";
            $(".body-siklus").append(html);
            hem = hem + item.hem;
            kkl = kkl + item.kkl;
            uri = uri + item.uri;
            bta = bta + item.bta;
            tcc = tcc + item.tcc;
            hiv = hiv + item.hiv;
            sif = sif + item.sif;
            hbs = hbs + item.hbs;
            hcv = hcv + item.hcv;
            mal = mal + item.mal;
            kai = kai + item.kai;
            kat = kat + item.kat;
            bac = bac + item.bac;
            tot = tot + item.tot;
        })
        var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+hem+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+kkl+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+uri+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+bta+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+tcc+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+hiv+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+sif+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+hbs+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+hcv+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+mal+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+kai+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+kat+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+bac+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+tot+"</td>";
        $(".body-total").append(htm);
      }
      return false;
    }
  });
});
</script>
@endsection

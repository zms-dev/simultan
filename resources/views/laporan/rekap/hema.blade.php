
<table>
	<thead>
		<tr>
			<th colspan="27" style="text-align: center; font-size: 16px">Rekapitulasi Nilai Peserta PME</th>
		</tr>
		<tr>
			<th rowspan="2">No</th>
      <th rowspan="2">Nama Instansi</th>
      <th rowspan="2">Jenis Instansi</th>
      @foreach ($parameterna as $key => $val)
        <th colspan="3">{!!$val->nama_parameter!!}</th>
      @endforeach
		</tr>
    <tr>
      @foreach ($parameterna as $key => $val)
        <th>Peserta</th>
        <th>Metode</th>
        <th>Alat</th>
      @endforeach
    </tr>
	</thead>
  @php
    $no = 0;
  @endphp
	<tbody>
    @foreach ($datas as $key => $value)
      @php
        $no++;
      @endphp
      <tr>
        <td>{{$no}}</td>
        <td>{{$value->nama_lab}}</td>
        <td>{{$value->badan_usaha}}</td>
          @foreach($value->parameter as $par)
                @if(count($par->detail))
                  @foreach($par->detail as $za)
                    <td>
                      <?php
                      if($za->zpeserta == 'Tidak di analisis'){
                          $keterangan = 'Tidak di analisis';
                      }else if($za->zpeserta <= 2 && $za->zpeserta >= -2){
                          $keterangan = 'Memuaskan';
                      }else if($za->zpeserta <= 3 && $za->zpeserta >= -3){
                          $keterangan = 'Peringatan';
                      }else{
                          $keterangan = 'Tidak Memuaskan';
                      }
                      echo $keterangan;
                      ?>
                    </td>
                    <td>
                      <?php
                      if($za->zmetode == 'Tidak di analisis'){
                          $keterangan = 'Tidak di analisis';
                      }else if($za->zmetode <= 2 && $za->zmetode >= -2){
                          $keterangan = 'Memuaskan';
                      }else if($za->zmetode <= 3 && $za->zmetode >= -3){
                          $keterangan = 'Peringatan';
                      }else{
                          $keterangan = 'Tidak Memuaskan';
                      }
                      echo $keterangan;
                      ?>
                    </td>
                    <td>
                      <?php
                      if($za->zalat == 'Tidak di analisis'){
                          $keterangan = 'Tidak di analisis';
                      }else if($za->zalat <= 2 && $za->zalat >= -2){
                          $keterangan = 'Memuaskan';
                      }else if($za->zalat <= 3 && $za->zalat >= -3){
                          $keterangan = 'Peringatan';
                      }else{
                          $keterangan = 'Tidak Memuaskan';
                      }
                      echo $keterangan;
                      ?>
                    </td>
                  @endforeach
                @else
                  <td>-</td>
                @endif
          @endforeach
      </tr>
    @endforeach
	</tbody>
</table>

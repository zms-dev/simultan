@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Monitoring Pelaporan Kirim Hasil </div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post" id="uangmasuk" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <select name="siklus" id="siklus" class="form-control">
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div><br>
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-date="2017" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control" name="tahun" id="tahun">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div><br>
                      
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                              <th rowspan="2"><center>No</center></th>
                              <th rowspan="2"><center>Parameter</center></th>
                              <th rowspan="2"><center>Jumlah Peserta</center></th>
                              <th rowspan="2"><center>Belum Kirim Bahan</center></th>
                              <th colspan="2"><center>Belum Input Hasil</center></th>
                              <th colspan="2"><center>Edit Hasil</center></th>
                              <th colspan="2"><center>Kirim Hasil</center></th>
                              <!-- <th colspan="2"><center>Belum Evaluasi</center></th>
                              <th colspan="2"><center>Sudah Evaluasi</center></th> -->
                          </tr>
                          <tr>
                            <th style="text-align: center;">01</th>
                            <th style="text-align: center;">02</th>
                            <th style="text-align: center;">01</th>
                            <th style="text-align: center;">02</th>
                            <th style="text-align: center;">01</th>
                            <th style="text-align: center;">02</th>
                            <!-- <th style="text-align: center;">01</th>
                            <th style="text-align: center;">02</th>
                            <th style="text-align: center;">01</th>
                            <th style="text-align: center;">02</th>  -->
                          </tr>
                        </thead>
                        <tbody class="body-siklus-pato">
                          
                        </tbody>
                        <tbody class="body-total">
                          
                        </tbody>
                      </table><br>

                      <table class="table table-bordered">
                        <thead>
                          <tr>
                              <th rowspan="2"><center>No</center></th>
                              <th rowspan="2"><center>Parameter Periode 2</center></th>
                              <th rowspan="2"><center>Jumlah Peserta</center></th>
                              <th rowspan="2"><center>Belum Kirim Bahan</center></th>
                              <th colspan="2"><center>Belum Input Hasil</center></th>
                              <th colspan="2"><center>Edit Hasil</center></th>
                              <th colspan="2"><center>Kirim Hasil</center></th>
                              <!-- <th colspan="2"><center>Belum Evaluasi</center></th>
                              <th colspan="2"><center>Sudah Evaluasi</center></th> -->
                          </tr>
                          <tr>
                            <th style="text-align: center;">01</th>
                            <th style="text-align: center;">02</th>
                            <th style="text-align: center;">01</th>
                            <th style="text-align: center;">02</th>
                            <th style="text-align: center;">01</th>
                            <th style="text-align: center;">02</th>
                            <!-- <th style="text-align: center;">01</th>
                            <th style="text-align: center;">02</th>
                            <th style="text-align: center;">01</th>
                            <th style="text-align: center;">02</th>  -->
                          </tr>
                        </thead>
                        <tbody class="body-siklus-pato-2">
                          
                        </tbody>
                        <tbody class="body-total">
                          
                        </tbody>
                      </table><br>

                      <table class="table table-bordered">
                        <thead>
                          <tr>
                              <th><center>No</center></th>
                              <th><center>Parameter</center></th>
                              <th><center>Jumlah Peserta</center></th>
                              <th><center>Belum Kirim Bahan</center></th>
                              <th><center>Belum Input Hasil</center></th>
                              <th><center>Edit Hasil</center></th>
                              <th><center>Kirim Hasil</center></th>
                          </tr>
                        </thead>
                        <tbody class="body-siklus-all1">

                        </tbody>
                        <tbody class="body-total">

                        </tbody>
                      </table><br>

                      <table class="table table-bordered">
                        <thead>
                          <tr>
                              <th rowspan="2"><center>No</center></th>
                              <th rowspan="2"><center>Parameter</center></th>
                              <th rowspan="2"><center>Jumlah Peserta</center></th>
                              <th rowspan="2"><center>Belum Kirim Bahan</center></th>
                              <th colspan="2"><center>Belum Input Hasil</center></th>
                              <th colspan="2"><center>Edit Hasil</center></th>
                              <th colspan="2"><center>Kirim Hasil</center></th>
                          </tr>
                          <tr>
                            <th style="text-align: center;">TP</th>
                            <th style="text-align: center;">RPR</th>
                            <th style="text-align: center;">TP</th>
                            <th style="text-align: center;">RPR</th>
                            <th style="text-align: center;">TP</th>
                            <th style="text-align: center;">RPR</th>
                          </tr>
                        </thead>
                        <tbody class="body-siklus-sifilis">
                          
                        </tbody>
                        <tbody class="body-total">
                          
                        </tbody>
                      </table><br>

                      <table class="table table-bordered">
                        <thead>
                          <tr>
                              <th><center>No</center></th>
                              <th><center>Parameter</center></th>
                              <th><center>Jumlah Peserta</center></th>
                              <th><center>Belum Kirim Bahan</center></th>
                              <th><center>Belum Input Hasil</center></th>
                              <th><center>Edit Hasil</center></th>
                              <th><center>Kirim Hasil</center></th>
                          </tr>
                        </thead>
                        <tbody class="body-siklus-all2">

                        </tbody>
                        <tbody class="body-total">

                        </tbody>
                      </table><br>
                      {{ csrf_field() }}
                    </form>
                    <a href="{{url('laporan-peserta-parameter')}}" target="_blank"><button class="btn btn-primary">Download Detail Monitoring Kirim Hasil</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});


$("#tahun,#siklus").change(function(){
  var val = $("#tahun").val(), i, no = 0, siklus = $("#siklus").val()
  $(".body-siklus-pato").html("<tr><td colspan='10'>Please Wait... </td></tr>");
  $(".body-siklus-pato-2").html("<tr><td colspan='10'>Please Wait... </td></tr>");
  $(".body-siklus-sifilis").html("<tr><td colspan='10'>Please Wait... </td></tr>");
  $(".body-siklus-all1").html("<tr><td colspan='7'>Please Wait... </td></tr>");
  $(".body-siklus-all2").html("<tr><td colspan='7'>Please Wait... </td></tr>");
  $.ajax({
    type: "GET",
    url : "{{url('monitoringsimultan1')}}/"+ val+"?y="+ siklus,
    success: function(addr){
      // y.innerHTML = '<tr id="datasiklus"><td id="nosiklus"></td><td id="paramsiklus"></td><td id="bidangsiklus"></td><td id="tarifsiklus"></td></tr>';
      $(".body-siklus-pato").html("");
      $(".body-siklus-pato-2").html("");
      $(".body-siklus-sifilis").html("");
      $(".body-siklus-all1").html("");
      $(".body-siklus-all2").html("");
      if(addr.Hasil != undefined){
        var no = 0;
        var i = 0;
        var kuo = ['280','240','155','110','75','150','150','150','150','40','20','35','35'];
        var total = i;
        var daf = no;
        var cets = no;
        var cetb = no;
        var ceta = no;
        var evals = no;
        $.each(addr.Hasil,function(e,item){
          no++;
            console.log(item);
            if (item.id <= 3) {
              var html = "<tr><td class=\"nosiklus\"><center>"+no+"</center></td><td class=\"paramsiklus\">"+item.alias+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.jumlah+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.cankirim+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.beluminput1+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.beluminput2+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.input1+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.input2+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.sudahinput1+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.sudahinput2+"</td>";
              // <td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.belumevaluasi1+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.belumevaluasi2+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.sudahevaluasi1+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.sudahevaluasi2+"</td>";
              $(".body-siklus-pato").append(html);
              if (siklus == 1 && val == 2019) {
                var html2 = "<tr><td class=\"nosiklus\"><center>"+no+"</center></td><td class=\"paramsiklus\">"+item.alias+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.jumlah2+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.cankirim2+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.beluminput12+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.beluminput22+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.input12+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.input22+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.sudahinput12+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.sudahinput22+"</td>";
                // <td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.belumevaluasi1+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.belumevaluasi2+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.sudahevaluasi1+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.sudahevaluasi2+"</td>";
                $(".body-siklus-pato-2").append(html2);
              }
            }else if(item.id > 3 && item.id < 7){
              var html = "<tr><td class=\"nosiklus\"><center>"+no+"</center></td><td class=\"paramsiklus\">"+item.alias+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.jumlah+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.cankirim+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.beluminput1+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.input1+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.sudahinput1+"</td>";
              $(".body-siklus-all1").append(html);
            }else if(item.id == 7){
              var html = "<tr><td class=\"nosiklus\"><center>"+no+"</center></td><td class=\"paramsiklus\">"+item.alias+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.jumlah+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.cankirim+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.beluminputtp+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.beluminputrpr+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.inputtp+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.inputrpr+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.sudahinputtp+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.sudahinputrpr+"</td>";
              $(".body-siklus-sifilis").append(html);
            }else{
              var html = "<tr><td class=\"nosiklus\"><center>"+no+"</center></td><td class=\"paramsiklus\">"+item.alias+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.jumlah+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.cankirim+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.beluminput1+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.input1+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.sudahinput1+"</td>";
              $(".body-siklus-all2").append(html);
            }
            total = '1590';
            daf = daf + item.jumlah;
            cets = cets + item.cetaks;
            cetb = cetb + item.cetakb;
            ceta = ceta + item.cetaka;
            evals = evals + item.evaluasis;
            i++;
        })
        // var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+daf+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+cets+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+ceta+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+cetb+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+evals+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+cets+"</td>";
        // $(".body-total").append(htm);
      }
      return false;
    }
  });
});
</script>
@endsection
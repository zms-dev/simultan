<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
table, table td, table th {    
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

table th, table td {
    padding: 5px;
}
</style>
<table>
	<thead>
		<tr>
			<th colspan="12" style="text-align: center; font-size: 16px">Rekapitulasi Peserta PME Berdasarkan Jenis Instansi dan Parameter Tahun {{$tahun}}</th>
		</tr>
      <tr>
      	  <th>No</th>
          <th>Parameter</th>
          <th>BBLK</th>
          <th>BLK</th>
          <th>RS-Pemerintah</th>
          <th>Puskesmas</th>
          <th>Labkesda</th>
          <th>Rs-Swasta</th>
          <th>LabKlinik Swasta</th>
          <th>UTD-RS</th>
          <th>UTD-PMI</th>
          <th>RS TNI/Polri</th>
      </tr>
	</thead>
	<tbody>
		<?php 
			$no = 0;
			$bblk = 0;
			$blk = 0;
			$rspem = 0;
			$pus = 0;
			$labkes = 0;
			$rsswa = 0;
			$labkl = 0;
			$utdrs = 0;
			$utdpmi = 0;
			$rstni = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$bblk = $bblk + $val->bblk;
			$blk = $blk + $val->blk;
			$rspem = $rspem + $val->rspem;
			$pus = $pus + $val->pus;
			$labkes = $labkes + $val->labkes;
			$rsswa = $rsswa + $val->rsswa;
			$labkl = $labkl + $val->labkl;
			$utdrs = $utdrs + $val->utdrs;
			$utdpmi = $utdpmi + $val->utdpmi;
			$rstni = $rstni + $val->rstni;
		?>
		<tr>
			<td>{{$no}}</td>
			<td>{{$val->alias}}</td>
			<td style="text-align: right;">{{number_format($val->bblk)}}</td>
			<td style="text-align: right;">{{number_format($val->blk)}}</td>
			<td style="text-align: right;">{{number_format($val->rspem)}}</td>
			<td style="text-align: right;">{{number_format($val->pus)}}</td>
			<td style="text-align: right;">{{number_format($val->labkes)}}</td>
			<td style="text-align: right;">{{number_format($val->rsswa)}}</td>
			<td style="text-align: right;">{{number_format($val->labkl)}}</td>
			<td style="text-align: right;">{{number_format($val->utdrs)}}</td>
			<td style="text-align: right;">{{number_format($val->utdpmi)}}</td>
			<td style="text-align: right;">{{number_format($val->rstni)}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td>Total :</td>
			<td style="text-align: right;">{{number_format($bblk)}}</td>
			<td style="text-align: right;">{{number_format($blk)}}</td>
			<td style="text-align: right;">{{number_format($rspem)}}</td>
			<td style="text-align: right;">{{number_format($pus)}}</td>
			<td style="text-align: right;">{{number_format($labkes)}}</td>
			<td style="text-align: right;">{{number_format($rsswa)}}</td>
			<td style="text-align: right;">{{number_format($labkl)}}</td>
			<td style="text-align: right;">{{number_format($utdrs)}}</td>
			<td style="text-align: right;">{{number_format($utdpmi)}}</td>
			<td style="text-align: right;">{{number_format($rstni)}}</td>
		</tr>
	</tbody>
</table>
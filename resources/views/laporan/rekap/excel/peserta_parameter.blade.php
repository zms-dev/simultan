<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
table, table td, table th {    
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

table th, table td {
    padding: 5px;
}
</style>
<table >
	<thead>
		<tr>
			<th colspan="3" style="text-align: center; font-size: 16px">Rekap Peserta Berdasarkan Parameter Tahun {{$tahun}}</th>
		</tr>
		<tr>
			<th width="5%">No</th>
			<th>Parameter</th>
			<th>Jumlah Peserta</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$no = 0; 
			$jumlah = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$jumlah = $jumlah + $val->jumlah;
		?>
		<tr>
			<td>{{$no}}</td>
			<td>{{$val->bidang}}<br>{{$val->parameter}}</td>
			<td style="text-align: right;">{{$val->jumlah}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td>Total :</td>
			<td style="text-align: right;">{{$jumlah}}</td>
		</tr>
	</tbody>
</table>
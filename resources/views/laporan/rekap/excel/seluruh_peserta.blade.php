<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
table, table td, table th {
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

table th, table td {
    padding: 5px;
}
</style>
<table>
	<thead>
		<tr>
			<th colspan="16" style="text-align: center; font-size: 16px">Rekap Seluruh Peserta {{$bidang->alias}} Tahun {{$tahun}}</th>
		</tr>
      <tr>
        <th>No</th>
        <th>Instansi</th>
        <th>Provinsi</th>
        <th>Nilai</th>
      </tr>
	</thead>
	<tbody>

	</tbody>
</table>

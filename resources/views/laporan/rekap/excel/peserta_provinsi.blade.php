<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
table, table td, table th {    
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

table th, table td {
    padding: 5px;
}
</style>
@if($bidang == 'all')
<table>
	<thead>
		<tr>
			<th colspan="16" style="text-align: center; font-size: 16px">Rekap Peserta Berdasarkan Provinsi per Bidang Tahun {{$tahun}}</th>
		</tr>
      <tr>
          <th>No</th>
          <th>Provinsi</th>
          <th>HEM</th>
          <th>KKL</th>
          <th>URI</th>
          <th>BTA</th>
          <th>TCC</th>
          <th>HIV</th>
          <th>SIF</th>
          <th>HBS</th>
          <th>HCV</th>
          <th>MAL</th>
          <th>KAI</th>
          <th>KAT</th>
          <th>BAC</th>
          <th>Total</th>
      </tr>
	</thead>
	<tbody>
		<?php 
			$no = 0; 
			$hem = 0;
			$kkl = 0;
			$uri = 0;
			$bta = 0;
			$tcc = 0;
			$hiv = 0;
			$sif = 0;
			$hbs = 0;
			$hcv = 0;
			$mal = 0;
			$kai = 0;
			$kat = 0;
			$bac = 0;
			$tot = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$hem = $hem + $val->hem;
			$kkl = $kkl + $val->kkl;
			$uri = $uri + $val->uri;
			$bta = $bta + $val->bta;
			$tcc = $tcc + $val->tcc;
			$hiv = $hiv + $val->hiv;
			$sif = $sif + $val->sif;
			$hbs = $hbs + $val->hbs;
			$hcv = $hcv + $val->hcv;
			$mal = $mal + $val->mal;
			$kai = $kai + $val->kai;
			$kat = $kat + $val->kat;
			$bac = $bac + $val->bac;
			$tot = $tot + $val->tot;
		?>
		<tr>
			<td>{{$no}}</td>
			<td>{{$val->name}}</td>
			<td>{{$val->hem}}</td>
			<td>{{$val->kkl}}</td>
			<td>{{$val->uri}}</td>
			<td>{{$val->bta}}</td>
			<td>{{$val->tcc}}</td>
			<td>{{$val->hiv}}</td>
			<td>{{$val->sif}}</td>
			<td>{{$val->hbs}}</td>
			<td>{{$val->hcv}}</td>
			<td>{{$val->mal}}</td>
			<td>{{$val->kai}}</td>
			<td>{{$val->kat}}</td>
			<td>{{$val->bac}}</td>
			<td>{{$val->tot}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td>Total :</td>
			<td>{{$hem}}</td>
			<td>{{$kkl}}</td>
			<td>{{$uri}}</td>
			<td>{{$bta}}</td>
			<td>{{$tcc}}</td>
			<td>{{$hiv}}</td>
			<td>{{$sif}}</td>
			<td>{{$hbs}}</td>
			<td>{{$hcv}}</td>
			<td>{{$mal}}</td>
			<td>{{$kai}}</td>
			<td>{{$kat}}</td>
			<td>{{$bac}}</td>
			<td>{{$tot}}</td>
		</tr>
	</tbody>
</table>
@else
<table>
	<thead>
		<tr>
			<th colspan="17" style="text-align: center; font-size: 16px">Rekap Peserta Berdasarkan Provinsi Bidang @if($bidang=='HEM') Hematologi @elseif($bidang == 'KKL') Kimia Klinik @elseif($bidang == 'URI') Urinalisa @elseif($bidang == 'BTA') BTA @elseif($bidang == 'TCC') Telur Cacing @elseif($bidang == 'HIV') HIV @elseif($bidang == 'SIF') Sifilis @elseif($bidang == 'HBS') HBS @elseif($bidang == 'HCV') HCV @elseif($bidang == 'MAL') Malaria @elseif($bidang == 'KAI') Kimia Air @elseif($bidang == 'KAT') Kimia Air Terbatas @elseif($bidang == 'BAC') BAC @endif Tahun {{$tahun}}</th>
		</tr>
      <tr>
          <th colspan="1">No</th>
          <th colspan="8">Provinsi</th>
          <th colspan="8">Total</th>
      </tr>
	</thead>
	@if($bidang == 'HEM')
	<tbody>
		<?php 
			$no = 0; 
			$hem = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$hem = $hem + $val->hem;
		?>
		<tr>
			<td>{{$no}}</td>
			<td colspan="8">{{$val->name}}</td>
			<td colspan="8">{{$val->hem}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td colspan="8">Total :</td>
			<td colspan="8">{{$hem}}</td>
		</tr>
	</tbody>
	@elseif($bidang == 'KKL')
	<tbody>
		<?php 
			$no = 0; 
			$kkl = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$kkl = $kkl + $val->kkl;
		?>
		<tr>
			<td>{{$no}}</td>
			<td colspan="8">{{$val->name}}</td>
			<td colspan="8">{{$val->kkl}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td colspan="8">Total :</td>
			<td colspan="8">{{$kkl}}</td>
		</tr>
	</tbody>
	@elseif($bidang == 'URI')
	<tbody>
		<?php 
			$no = 0; 
			$uri = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$uri = $uri + $val->uri;
		?>
		<tr>
			<td>{{$no}}</td>
			<td colspan="8">{{$val->name}}</td>
			<td colspan="8">{{$val->uri}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td colspan="8">Total :</td>
			<td colspan="8">{{$uri}}</td>
		</tr>
	</tbody>
	@elseif($bidang == 'BTA')
	<tbody>
		<?php 
			$no = 0; 
			$bta = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$bta = $bta + $val->bta;
		?>
		<tr>
			<td>{{$no}}</td>
			<td colspan="8">{{$val->name}}</td>
			<td colspan="8">{{$val->bta}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td colspan="8">Total :</td>
			<td colspan="8">{{$bta}}</td>
		</tr>
	</tbody>
	@elseif($bidang == 'TCC')
	<tbody>
		<?php 
			$no = 0; 
			$tcc = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$tcc = $tcc + $val->tcc;
		?>
		<tr>
			<td>{{$no}}</td>
			<td colspan="8">{{$val->name}}</td>
			<td colspan="8">{{$val->tcc}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td colspan="8">Total :</td>
			<td colspan="8">{{$tcc}}</td>
		</tr>
	</tbody>
	@elseif($bidang == 'HIV')
	<tbody>
		<?php 
			$no = 0; 
			$hiv = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$hiv = $hiv + $val->hiv;
		?>
		<tr>
			<td>{{$no}}</td>
			<td colspan="8">{{$val->name}}</td>
			<td colspan="8">{{$val->hiv}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td colspan="8">Total :</td>
			<td colspan="8">{{$hiv}}</td>
		</tr>
	</tbody>
	@elseif($bidang == 'SIF')
	<tbody>
		<?php 
			$no = 0; 
			$sif = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$sif = $sif + $val->sif;
		?>
		<tr>
			<td>{{$no}}</td>
			<td colspan="8">{{$val->name}}</td>
			<td colspan="8">{{$val->sif}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td colspan="8">Total :</td>
			<td colspan="8">{{$sif}}</td>
		</tr>
	</tbody>
	@elseif($bidang == 'HBS')
	<tbody>
		<?php 
			$no = 0; 
			$hbs = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$hbs = $hbs + $val->hbs;
		?>
		<tr>
			<td>{{$no}}</td>
			<td colspan="8">{{$val->name}}</td>
			<td colspan="8">{{$val->hbs}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td colspan="8">Total :</td>
			<td colspan="8">{{$hbs}}</td>
		</tr>
	</tbody>
	@elseif($bidang == 'HCV')
	<tbody>
		<?php 
			$no = 0; 
			$hcv = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$hcv = $hcv + $val->hcv;
		?>
		<tr>
			<td>{{$no}}</td>
			<td colspan="8">{{$val->name}}</td>
			<td colspan="8">{{$val->hcv}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td colspan="8">Total :</td>
			<td colspan="8">{{$hcv}}</td>
		</tr>
	</tbody>
	@elseif($bidang == 'MAL')
	<tbody>
		<?php 
			$no = 0; 
			$mal = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$mal = $mal + $val->mal;
		?>
		<tr>
			<td>{{$no}}</td>
			<td colspan="8">{{$val->name}}</td>
			<td colspan="8">{{$val->mal}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td colspan="8">Total :</td>
			<td colspan="8">{{$mal}}</td>
		</tr>
	</tbody>
	@elseif($bidang == 'KAI')
	<tbody>
		<?php 
			$no = 0; 
			$kai = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$kai = $kai + $val->kai;
		?>
		<tr>
			<td>{{$no}}</td>
			<td colspan="8">{{$val->name}}</td>
			<td colspan="8">{{$val->kai}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td colspan="8">Total :</td>
			<td colspan="8">{{$kai}}</td>
		</tr>
	</tbody>
	@elseif($bidang == 'KAT')
	<tbody>
		<?php 
			$no = 0; 
			$kat = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$kat = $kat + $val->kat;
		?>
		<tr>
			<td>{{$no}}</td>
			<td colspan="8">{{$val->name}}</td>
			<td colspan="8">{{$val->kat}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td colspan="8">Total :</td>
			<td colspan="8">{{$kat}}</td>
		</tr>
	</tbody>
	@elseif($bidang == 'BAC')
	<tbody>
		<?php 
			$no = 0; 
			$bac = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$bac = $bac + $val->bac;
		?>
		<tr>
			<td>{{$no}}</td>
			<td colspan="8">{{$val->name}}</td>
			<td colspan="8">{{$val->bac}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td colspan="8">Total :</td>
			<td colspan="8">{{$bac}}</td>
		</tr>
	</tbody>
	@endif
</table>
@endif

<table>
	<thead>
		<tr>
			<th colspan="12" style="text-align: center; font-size: 16px">Rekapitulasi Nilai Peserta PME {{$bidangR}}</th>
		</tr>
		<tr>
			<th>No</th>
      <th>Nama Instansi</th>
      <th>Jenis Instansi</th>
      @if($bidang == 'AHIV')
        <th>Nilai</th>
      @elseif ($bidang == '7')
        <th>Anti TP</th>
        <th>RPR</th>
      @elseif ($bidang == '6')
        <th>Nilai</th>
      @elseif ($bidang == '3')
        <th>Skoring</th>
      @elseif ($bidang == '8')
        <th>Nilai</th>
      @elseif ($bidang == '9')
        <th>Nilai</th>
      @elseif ($bidang == '5')
        <th>Nilai</th>
      @elseif ($bidang == '4')
        <th>Nilai</th>
      @elseif ($bidang == '10')
        <th>Nilai</th>
      @elseif ($bidang == '13')
        <th>Nilai</th>
      @endif
		</tr>
	</thead>
  @php
    $no = 0;
  @endphp
	<tbody>
    @foreach ($datas as $key => $value)
      @php
        $no++;
      @endphp
      <tr>
        <td>{{$no}}</td>
        <td>{{$value->nama_lab}}</td>
        <td>{{$value->badan_usaha}}</td>
        @if ($bidang == 'AHIV')
          <td>{{$value->ketetapan}}</td>
        @elseif ($bidang == '7')
          <td>{{$value->ketetapanTP}}</td>
          <td>{{$value->ketetapanRPR}}</td>
        @elseif ($bidang == '6')
          <td>{{$value->ketetapan}}</td>
        @elseif ($bidang == '3')
          <td>{{$value->skoring}}</td>
        @elseif ($bidang == '8')
          <td>{{$value->ketetapan}}</td>
        @elseif ($bidang == '9')
          <td>{{$value->ketetapan}}</td>
        @elseif ($bidang == '5')
          <td>{{$value->nilai}}</td>
        @elseif ($bidang == '4')
          <td>{{$value->nilai}}</td>
        @elseif ($bidang == '10')
          <td>{{$value->nilai}}</td>
        @elseif ($bidang == '13')
          <td>{{$value->nilai}}</td>
        @endif
      </tr>
    @endforeach
	</tbody>
</table>

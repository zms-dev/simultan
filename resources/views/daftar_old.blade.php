@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Formulir Pendaftaran</div>

                <div class="panel-body">
                  @if(Session::has('message'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      {{ Session::get('message') }}
                    </div>
                  @endif
                  <form class="form-horizontal" id="myform" action="{{url('daftar_old')}}" method="post" enctype="multipart/form-data"  >
                    <div class="form-group">
                      <label for="siklus" class="col-sm-3 control-label">Siklus</label>
                      <div class="col-sm-9">
                          <select class="form-control" name="siklus" id="siklus">
                              <option></option>
                              <?php
                                $date = date('Y-m');
                                $siklus1 = date('Y').'-01';
                                $siklus2 = date('Y').'-06';
                              ?>
                              @if($date >= $siklus1 && $date < $siklus2)
                              <!-- <option class="1" value="1">Siklus 1</option> -->
                              <option class="12" value="12">Siklus 1 & 2</option>
                              @else
                              <!-- <option class="2" value="2">Siklus 2</option> -->
                              @endif
                          </select>
                      </div>
                    </div>
                  <div class="table-responsive"  id="bidangpengujian">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                            <th>No</th>
                            <th colspan="2" style="text-align: center;">Bidang Pengujian Yang Dipilih</th>
                            <th>Tarif (Rp)</th>
                        </tr>
                      </thead>
                      <tbody class="body-siklus">

                      </tbody>
                        <!-- <tr id="datasiklus">
                          <td id="nosiklus"></td>
                          <td id="paramsiklus"></td>
                          <td id="bidangsiklus"></td>
                          <td id="tarifsiklus"></td>
                        </tr> -->
                    </table>
                  </div>
                  <label>Total Pembayaran:</label><br>
                  <div class="total"></div>
                  <label>Catatan :</label>
                  <p>*) Pilih metode pembayaran anda dengan benar.</p>
                  <label for="telp" class="control-label">Metode Pembayaran</label>
                  <select id="pembayaran" class="form-control" name="pembayaran" onchange="Notifpembayaran()">
                    <option></option>
                    <option value="transfer">Transfer via virtual account</option>
                    <option value="sptjm">Perjanjian Kerjasama (PKS)</option>
                  </select>
                  <div id="sptjm" class="notifpembayaran" style="display:none;">
                    <p>Perjanjian kerjasama hanya dapat dilakukan oleh instansi yang  membuat MOU mendaftar dan membayar secara kolektif.<br>
                    Pastikan instansi Anda telah memiliki MOU dengan BBLK Surabaya.</p>
                  </div>
                  <div id="transfer" class="notifpembayaran" style="display:none;">
                    <p>Kode virtual account anda 89160XXXX (xxxx merupakan nomor urut peserta)</p>

                    <p>1. Silahkan melakukan pembayaran minimal 1 (satu) jam setelah anda menerima kode virtual account<br>
                    2. Kode virtual account berlaku 1x24 jam<br>
                    3. Tata cara pembayaran via virtual account klik disini (link)<br>
                    3. Jika membutuhkan bantuan dapat menghubungi (031)5021451</p>
                  </div>
                  <!-- <textarea class="form-control" name="sptjm" style="display:none;">-</textarea>
                  <div id="transfer" class="pembayaran" style="display:none">
                    <label for="telp" class="control-label">File</label><small> *Bukti Transfer</small>
                    <input type="file" class="myFile" id="telp" placeholder="Nama Laboratorium" name="file" accept="image/*">
                  </div>
                  <br> -->
                  <textarea class="form-control" name="sptjm" style="display:none;">-</textarea>
                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                  <div class="tombolna">
                  </div>
                  <div class="pembayaran" style="display:none">
                    <input type="submit" id="Daftar" name="simpan" value="Kirim" class="btn btn-submit">
                  </div>
                  @foreach($bidang as $val)
                  <input type="hidden" name="alias[]" value="{{$val->alias}}">
                  @endforeach
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"> 

$('.myFile').bind('change', function() {
  if(this.files[0].size >= 5120000){
      alert('File bukti pembayaran melebihi 5Mb!');
      $(".tombolna").html("");
  }else{
      $(".tombolna").html("");
        var exportna = "<input type=\"submit\" name=\"simpan\" value=\"Kirim\" class=\"btn btn-submit\">";
        $(".tombolna").append(exportna);
  }
});

function Notifpembayaran() {
  var x = document.getElementById("pembayaran").value;
  console.log(x);
  if (x == 'transfer') {
    $("#transfer").show();
    $("#sptjm").hide(); 
    $("#Daftar").val('Daftar & Bayar'); 
  }else{
    $("#transfer").hide();
    $("#sptjm").show();
    $("#Daftar").val('Daftar'); 
  }
}

$(function() {
    $('#pembayaran').change(function(){
        $('.pembayaran').show();
        $('#' + $(this).val()).show();
    });
});

function formatNumber (num, currency) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}



$("#siklus").change(function(){
  var val = $(this).val(), i, no = 0;
  var y = document.getElementById('datasiklus')
  $(".body-siklus").html("<tr><td colspan='4'>Please Wait... </td></tr>")
  $.ajax({
    type: "GET",
    url : "{{url('datasiklus').'/'}}"+val,
    success: function(addr){
      // y.innerHTML = '<tr id="datasiklus"><td id="nosiklus"></td><td id="paramsiklus"></td><td id="bidangsiklus"></td><td id="tarifsiklus"></td></tr>';
      $(".body-siklus").html("");
      if(addr.Hasil != undefined){
        var no = 1;
        $.each(addr.Hasil,function(e,item){
          var tarif2 = formatNumber(item.tarif / 2);
          var tarif1 = formatNumber(item.tarif);
          var tarif3 = formatNumber(item.tarif * 2);
          if (val != '12') {
            if (val == '1') {
              if (item.sisa_kuota_1 > '0') {
                var html = "<tr><td class=\"nosiklus\"><center>"+no+"</center></td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif2+"</td>";
                $(".body-siklus").append(html);
              }else{
                var html = "<tr><td class=\"nosiklus\"><center>"+no+"</center></td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif2+"</td>";
                $(".body-siklus").append(html);
              }
            }
            if (val == '2') {
              if (item.sisa_kuota_2 > '0') {
                var html = "<tr><td class=\"nosiklus\"><center>"+no+"</center></td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif2+"</td>";
                $(".body-siklus").append(html);
              }else{
                var html = "<tr><td class=\"nosiklus\"><center>"+no+"</center></td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif2+"</td>";
                $(".body-siklus").append(html);
              }
            }
          }else{
              if (item.sisa_kuota_1 > '0') {
                if (item.daftar.length === 0) {
                  var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif1+"</td>";
                  $(".body-siklus").append(html);
                }else{
                  var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+" disabled></td><td class='tarifsiklus' style='text-align: right'>"+tarif1+"</td>";
                  $(".body-siklus").append(html);
                }
              }else{
                var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif1+"</td>";
                $(".body-siklus").append(html);
              }
          }
          no++;
        })

        $("input[type=checkbox]").click(function(){
            var cekbok  = $("input[type=checkbox]"),
                total   = 0;
            cekbok.each(function(){
              var e = $(this).parents('tr').find('.tarifsiklus').html().replace(/,/g,'');
              if($(this).prop('checked') == true){
                total = total + parseInt(e);
              }
            });
            $(".total").html(formatNumber(total));

        });

      }
      return false;
    }
  });
});


$('#myform').submit(function() {
  var x = document.getElementById("pemantapan_mutu").value;

  if($('.checkbidang:checkbox:checked').length == 0){
    alert('Pilih minimal 1 Bidang Pengujian');
    $('html, body').animate({ 
      scrollTop: $('#bidangpengujian').offset().top 
    }, 'slow');
    return false;
  }
  $('#Daftar').on('click', function () {
    if ($("#myform").valid()) {
      var $btn = $(this).button('loading')
    }
  })
  $('#Daftar').button('loading')
});
</script>
@endsection
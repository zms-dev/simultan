@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Download Evaluasi</div>

                <div class="panel-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Type</th>
                            <th>Laboratorium</th>
                            <th>Bidang</th>
                            <th>Download</th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>
                        <tr>
                          <td><center>{{$no}}</center></td>
                          <td>{{$val->type}}</td>
                          <td>{{$val->nama_lab}}</td>
                          <td>{{$val->parameter}}</td>
                          <td><center>
                            <a href="{{URL::asset('asset/backend/evaluasi').'/'.$val->file}}" download>
                                <i class="glyphicon glyphicon-cloud-download"></i></center>
                            </a>
                          </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="5">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
                    {{ $data->links() }}<br>
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Laboratorium</th>
                            <th>Bidang</th>
                            <th>Sertifikat</th>
                        </tr>
                        @if(count($sertifikat))
                        <?php $no = 0; ?>
                        @foreach($sertifikat as $val)
                        <?php $no++ ?>
                        <tr>
                            <td>{{$no}}</td>
                            <td>{{$val->nama_lab}}</td>
                            <td>{{$val->parameter}}</td>
                            <td><center>
                                <a href="{{URL('cetak-sertifikat').'/'.$val->id}}" target="_blank">
                                    <i class="glyphicon glyphicon-cloud-download"></i>
                                </a>
                                </center>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
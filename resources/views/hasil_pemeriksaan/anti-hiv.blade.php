@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" id="myform" method="post" enctype="multipart/form-data">
                    <center><label>FORMULIR HASIL PEMERIKSAAN PROGRAM  NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI SIKLUS {{$siklus}} TAHUN {{$date}}<br> HASIL PEMERIKSAAN ANTI HIV</label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$perusahaan}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Pendidikan Pelaksana PME</label>
                        <div class="col-sm-5">
                            <select id="pendidikan" class="form-control" name="pendidikan" required>
                                <option selected></option>
                                @foreach($pendidikan as $pen)
                                    <option value="{{$pen->id}}">{{$pen->tingkat}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <div id="pendidikan_lain" class="pendidikan_lain">
                                <input id="inputpendidikan_lain" class="form-control" type="text" name="pendidikan_lain" placeholder="Pendidikan Lainnya" />
                            </div>
                        </div>
                    </div>
                    <label>2. BAHAN UJI :</label>
                    <table class="table-bordered table">
                            <tr>
                                <td>Diterima tanggal :</td>
                                <td>
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="" required class="form_datetime form-control" name="tgl_diterima" autocomplete="off">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                </td>
                                <td>Tanggal Pemeriksaan :</td>
                                <td>
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="" required class="form_datetime form-control" name="tgl_diperiksa" autocomplete="off">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                </td>
                            </tr>
                    </table>

                    <table class="table-bordered table">
                            <tr>
                                <td rowspan="6" width="20%"> Kondisi bahan uji saat diterima</td>
                                <td>Kode bahan uji</td>
                                <td>Baik/Jernih </td>
                                <td>Keruh </td>
                                <td>Lain-lain</td>
                            </tr>
                            <tr>
                                <td><select class="form-control tabung1" name="no_tabung[]" id="tabung11" required>
                                    <option></option>
                                        @foreach($rujukan as $ruj)
                                            @if($ruj->group == 1)
                                                <option value="{{$ruj->kode_bahan_uji}}">{{$ruj->kode_bahan_uji}}</option>
                                            @endif
                                        @endforeach
                                </select></td>
                                <td><input class="jenis1" type="checkbox" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input class="jenis1" type="checkbox" name="jenis[]" value="keruh"></td>
                                <td><input class="jenis1" type="checkbox" name="jenis[]" value="lain-lain"></td>
                            </tr>
                            <tr>
                               <td><select class="form-control tabung1" name="no_tabung[]" id="tabung21" required>
                                    <option></option>
                                        @foreach($rujukan as $ruj)
                                            @if($ruj->group == 2)
                                                <option value="{{$ruj->kode_bahan_uji}}">{{$ruj->kode_bahan_uji}}</option>
                                            @endif
                                        @endforeach
                                </select></td>
                                <td><input class="jenis2" type="checkbox" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input class="jenis2" type="checkbox" name="jenis[]" value="keruh"></td>
                                <td><input class="jenis2" type="checkbox" name="jenis[]" value="lain-lain"></td>
                            </tr>
                            <tr>
                                <td><select class="form-control tabung1" name="no_tabung[]" id="tabung31" required>
                                    <option></option>
                                        @foreach($rujukan as $ruj)
                                            @if($ruj->group == 3)
                                                <option value="{{$ruj->kode_bahan_uji}}">{{$ruj->kode_bahan_uji}}</option>
                                            @endif
                                        @endforeach
                                </select></td>
                                <td><input class="jenis3" type="checkbox" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input class="jenis3" type="checkbox" name="jenis[]" value="keruh"></td>
                                <td><input class="jenis3" type="checkbox" name="jenis[]" value="lain-lain"></td>
                            </tr>
                            <tr>
                                <td><select class="form-control tabung1" name="no_tabung[]" id="tabung41" required>
                                    <option></option>
                                        @foreach($rujukan as $ruj)
                                            @if($ruj->group == 4)
                                                <option value="{{$ruj->kode_bahan_uji}}">{{$ruj->kode_bahan_uji}}</option>
                                            @endif
                                        @endforeach
                                </select></td>
                                <td><input class="jenis4" type="checkbox" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input class="jenis4" type="checkbox" name="jenis[]" value="keruh"></td>
                                <td><input class="jenis4" type="checkbox" name="jenis[]" value="lain-lain"></td>
                            </tr>
                            <tr>
                                <td><select class="form-control tabung1" name="no_tabung[]" id="tabung51" required>
                                    <option></option>
                                        @foreach($rujukan as $ruj)
                                            @if($ruj->group == 5)
                                                <option value="{{$ruj->kode_bahan_uji}}">{{$ruj->kode_bahan_uji}}</option>
                                            @endif
                                        @endforeach
                                </select></td>
                                <td><input class="jenis5" type="checkbox" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input class="jenis5" type="checkbox" name="jenis[]" value="keruh"></td>
                                <td><input class="jenis5" type="checkbox" name="jenis[]" value="lain-lain"></td>
                            </tr>
                    </table>
                    <label>3. REAGEN</label>
                    <table class="table table-bordered">
                        <tr>
                            <td>Keterangan</td>
                            <td>Tes I</td>
                            <td>Tes II</td>
                            <td>Tes III</td>
                        </tr>
                        <tr>
                            <td>Nama Reagen</td>
                            <td>
                                <select id="alat1" class="form-control" name="nama_reagen[]" idx='1' class="form-control" required>
                                    <option selected></option>
                                    @foreach($reagen as $val)
                                      <option value="{{$val->id}}">{{$val->reagen}}</option>
                                    @endforeach
                                </select>
                                <div id="row_alat1" class="inpualat1">
                                    <input id="inpualat1" class="form-control" type="text" name="reagen_lain[]" />
                                </div>
                            </td>
                            <td>
                                <select id="alat2" class="form-control" name="nama_reagen[]" idx='2' class="form-control" >
                                    <option selected></option>
                                    @foreach($reagen as $val)
                                      <option value="{{$val->id}}">{{$val->reagen}}</option>
                                    @endforeach
                                </select>
                                <div id="row_alat2" class="inpualat2">
                                    <input id="inpualat2" class="form-control" type="text" name="reagen_lain[]" />
                                </div>
                            </td>
                            <td>
                                <select id="alat3" class="form-control" name="nama_reagen[]" idx='3' class="form-control" >
                                    <option selected></option>
                                    @foreach($reagen as $val)
                                      <option value="{{$val->id}}">{{$val->reagen}}</option>
                                    @endforeach
                                </select>
                                <div id="row_alat3" class="inpualat3">
                                    <input id="inpualat3" class="form-control" type="text" name="reagen_lain[]" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Metode Pemeriksaan </td>
                            <td>
                                <input type="text" name="metode[]" value="" id="re1" required="" class="form-control" readonly>
                            </td>
                            <td>
                                <input type="text" name="metode[]" value="" id="re2" required="" class="form-control" readonly>
                            </td>
                            <td>
                                <input type="text" name="metode[]" value="" id="re3" required="" class="form-control" readonly>
                            </td>   
                        </tr>
@for($i=1; $i < 4; $i++)
<script>                            
$(function() {
    $('#row_alat{{$i}}').hide(); 
    $('#alat{{$i}}').change(function(){
    var setan  = $("#alat{{$i}} option:selected").text();
        if(setan.match('Lain - lain.*')) {
            $('#row_alat{{$i}}').show(); 
        } else {
            $('#row_alat{{$i}}').hide(); 
        } 
    });
});
</script>        
@endfor                
                        <tr>
                            <td>Nama Produsen</td>
                            <td><input type="text" name="nama_produsen[]" class="form-control" id="produsen1" readonly=""></td>
                            <td><input type="text" name="nama_produsen[]" class="form-control" id="produsen2" readonly=""></td>
                            <td><input type="text" name="nama_produsen[]" class="form-control" id="produsen3" readonly=""></td>
                        </tr>
                        <tr>
                            <td>Nomor Lot/Batch</td>
                            <td><input type="text" name="nomor_lot[]" class="form-control" required id="lot1"></td>
                            <td><input type="text" name="nomor_lot[]" class="form-control" required id="lot2"></td>
                            <td><input type="text" name="nomor_lot[]" class="form-control" required id="lot3"></td>
                        </tr>
                        <tr>
                            <td>Tanggal Kadaluarsa</td>
                            <td>
                              <div class="controls input-append date" data-link-field="dtp_input1">
                                  <input size="16" type="text" readonly class="form_datetime form-control" name="tgl_kadaluarsa[]" id="tanggal1">
                                  <span class="add-on"><i class="icon-th"></i></span>
                              </div>
                            </td>
                            <td>
                              <div class="controls input-append date" data-link-field="dtp_input1">
                                  <input size="16" type="text" readonly class="form_datetime form-control" name="tgl_kadaluarsa[]" id="tanggal2">
                                  <span class="add-on"><i class="icon-th"></i></span>
                              </div>
                            </td>
                            <td>
                              <div class="controls input-append date" data-link-field="dtp_input1">
                                  <input size="16" type="text" readonly class="form_datetime form-control" name="tgl_kadaluarsa[]" id="tanggal3">
                                  <span class="add-on"><i class="icon-th"></i></span>
                              </div>
                            </td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                       
                    </script>
                    <label>4. HASIL PEMERIKSAAN </label>
                    <table class="table table-bordered">
                        <tr>
                            <td>Kode Bahan Uji</td>
                            <td colspan="4"><input type="text" id="tabung12" name="kode_bahan_kontrol[]"  readonly></td>
                        </tr>
                        <tr>
                            <td>Tes</td>
                            <td>Abs atau OD (A) (Bila dengan  EIA / Setara)</td>
                            <td>Cut Off (B) (Bila dengan EIA / Setara)</td>
                            <td>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA / Setara)</td>
                            <td>Interpretasi hasil </td>
                        </tr>
                        <tr>
                            <td>I</td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od1[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off1[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco1[]"/></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>II</td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od1[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off1[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco1[]"/></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>III</td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od1[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off1[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco1[]"/></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <td>Kode Bahan Uji</td>
                            <td colspan="4"><input type="text" id="tabung22" name="kode_bahan_kontrol[]"  readonly></td>
                        </tr>
                        <tr>
                            <td>Tes</td>
                            <td>Abs atau OD (A) (Bila dengan  EIA / Setara)</td>
                            <td>Cut Off (B) (Bila dengan EIA / Setara)</td>
                            <td>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA / Setara)</td>
                            <td>Interpretasi hasil </td>
                        </tr>
                        <tr>
                            <td>I</td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od2[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off2[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco2[]"/></td>
                            <td>
                                <select name="interpretasi2[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>II</td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od2[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off2[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco2[]"/></td>
                            <td>
                                <select name="interpretasi2[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>III</td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od2[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off2[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco2[]"/></td>
                            <td>
                                <select name="interpretasi2[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <td>Kode Bahan Uji</td>
                            <td colspan="4"><input type="text" id="tabung32" name="kode_bahan_kontrol[]"  readonly></td>
                        </tr>
                        <tr>
                            <td>Tes</td>
                            <td>Abs atau OD (A) (Bila dengan  EIA / Setara)</td>
                            <td>Cut Off (B) (Bila dengan EIA / Setara)</td>
                            <td>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA / Setara)</td>
                            <td>Interpretasi hasil </td>
                        </tr>
                        <tr>
                            <td>I</td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od3[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off3[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco3[]"/></td>
                            <td>
                                <select name="interpretasi3[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>II</td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od3[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off3[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco3[]"/></td>
                            <td>
                                <select name="interpretasi3[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>III</td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od3[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off3[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco3[]"/></td>
                            <td>
                                <select name="interpretasi3[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <td>Kode Bahan Uji</td>
                            <td colspan="4"><input type="text" id="tabung42" name="kode_bahan_kontrol[]"  readonly></td>
                        </tr>
                        <tr>
                            <td>Tes</td>
                            <td>Abs atau OD (A) (Bila dengan  EIA / Setara)</td>
                            <td>Cut Off (B) (Bila dengan EIA / Setara)</td>
                            <td>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA / Setara)</td>
                            <td>Interpretasi hasil </td>
                        </tr>
                        <tr>
                            <td>I</td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od4[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off4[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco4[]"/></td>
                            <td>
                                <select name="interpretasi4[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>II</td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od4[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off4[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco4[]"/></td>
                            <td>
                                <select name="interpretasi4[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>III</td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od4[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off4[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco4[]"/></td>
                            <td>
                                <select name="interpretasi4[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <td>Kode Bahan Uji</td>
                            <td colspan="4"><input type="text" id="tabung52" name="kode_bahan_kontrol[]"  readonly></td>
                        </tr>
                        <tr>
                            <td>Tes</td>
                            <td>Abs atau OD (A) (Bila dengan  EIA / Setara)</td>
                            <td>Cut Off (B) (Bila dengan EIA / Setara)</td>
                            <td>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA / Setara)</td>
                            <td>Interpretasi hasil </td>
                        </tr>
                        <tr>
                            <td>I</td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od5[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off5[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco5[]"/></td>
                            <td>
                                <select name="interpretasi5[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>II</td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od5[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off5[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco5[]"/></td>
                            <td>
                                <select name="interpretasi5[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>III</td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od5[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off5[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco5[]"/></td>
                            <td>
                                <select name="interpretasi5[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <div class="col-sm-6">
                        <label>Alasan bila tidak melakukan pemeriksaan :</label>
                        <textarea name="keterangan" class="form-control">-</textarea>
                    </div>
                    <div class="col-sm-6">
                        <label>Petugas yang melakukan pemeriksaan :</label>
                        <input type="text" name="petugas_pemeriksaan" class="form-control" required>
                    </div><br>

                      {{ csrf_field() }}
                    <input type="submit" name="simpan" id="simpan" value="Simpan" class="btn btn-submit" style="margin: 15px 0px 0px 15px;">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(function() {
    $('#pendidikan_lain').hide(); 
    $('#pendidikan').change(function(){
    var setan  = $("#pendidikan option:selected").text();
        if(setan.match('Lain - lain.*')) {
            $('#pendidikan_lain').show(); 
            $("#inputpendidikan_lain").prop('required',true);
        } else {
            $('#pendidikan_lain').hide(); 
            $("#inputpendidikan_lain").prop('required',false);
            $("#inputpendidikan_lain").val('');
        } 
    });
});
$("form input").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Tidak boleh kosong');
    }
});

$('.decimal').keyup(function(){
    var val = $(this).val();
    if(isNaN(val)){
        val = val.replace(/[^0-9\.\<\>\:\/]/g,'');
        if(val.split('.').length>2) 
            val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});
</script>

<script type="text/javascript">

$(document).ready(function(){
    $("#tabung11").change(function(){
        $val = $(this).val();
        console.log($val);
        $('#tabung12').val($val);
    });
    $("#tabung21").change(function(){
        $val1 = $(this).val();
        $('#tabung22').val($val1);
    });
    $("#tabung31").change(function(){
        $val2 = $(this).val();
        $('#tabung32').val($val2);
    });
    $("#tabung41").change(function(){
        $val3 = $(this).val();
        $('#tabung42').val($val3);
    });
    $("#tabung51").change(function(){
        $val4 = $(this).val();
        $('#tabung52').val($val4);
    });
});

$("input:checkbox").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input:checkbox[class='" + $box.attr("class") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});

$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});


$("select[name='nama_reagen[]']").change(function(){
  var val = $(this);
  console.log(val);
  var y = $('#re'+val.attr('idx'));
  var x = $('#produsen'+val.attr('idx'));
  var n = $('#lot'+val.attr('idx'));
  var t = $('#tanggal'+val.attr('idx'));
  $.ajax({
    type: "GET",
    url : "{{url('getreagenimun').'/'}}"+val.val(),
    success: function(addr){
        y.val(addr.Hasil[0].Metode);
        x.val(addr.Hasil[0].Produsen);
        if (y.val() == '-') {
            n.attr("required",false);
            t.attr("required",false);
        }else{
            n.attr("required",true);
            t.attr("required",true);
        }
    }
  });
});


$(".tabung1").change(function(){
    var e       = $(this),
        val     = e.val(),
        arr     = [],
        tabung  = $(".tabung1");
        tabung.find('option').css("display", "");
    tabung.each(function(){
        var t = $(this);
        arr.push(t.val());
    });
    $.each(arr,function(idx, value){
        tabung.find('option[value="'+value+'"]').css("display", "none");
    });
});

$('#myform').submit(function() {
  $('#simpan').button('loading')
});
</script>
@endsection

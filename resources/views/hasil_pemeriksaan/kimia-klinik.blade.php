@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" id="form_d" enctype="multipart/form-data">
                    <center><label> 
                        FORMULIR HASIL PEMERIKSAAN PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL KIMIA KLINIK <br>SIKLUS {{$siklus}} TAHUN {{$date}}<input type="hidden" name="type" value="{{$type}}">
                    </label></center><br>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{ $perusahaan }}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Bahan </label>
                        <div class="col-sm-9">
                            <?php 
                                if ($siklus == '1') {
                                    if ($type == 'a') {
                                        $kodebahan = 'I-01';
                                    }else{
                                        $kodebahan = 'I-02';
                                    }
                                }else{
                                    if ($type == 'a') {
                                        $kodebahan = 'II-01';
                                    }else{
                                        $kodebahan = 'II-02';
                                    }
                                }
                            ?>
                            <input type="text" class="form-control" id="kode_bahan" value="{{$kodebahan}}" name="kode_bahan" placeholder="Kode Bahan" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Penerimaan </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-date="2017-07-07" data-link-field="dtp_input1">
                              <input size="16" type="text" value="{{$validasi->tanggal_penerimaan}}" autocomplete="off" required class="form-control" name="tanggal_penerimaan" required readonly>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kualitas Bahan </label>
                        <div class="col-sm-9">  
                          <input type="radio" required name="kualitas" value="{{$validasi->kondisi_bahan}}" checked> {{$validasi->kondisi_bahan}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Pemeriksaan </label>
                        <div class="col-sm-9">
                        <div class="controls input-append date" data-date="2017-07-07" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" autocomplete="off" required class="form_datetime form-control" name="tanggal_pemeriksaan" required>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Pendidikan Pelaksana PME</label>
                        <div class="col-sm-5">
                            <select id="pendidikan" class="form-control" name="pendidikan" required>
                                <option selected></option>
                                @foreach($pendidikan as $pen)
                                    <option value="{{$pen->id}}">{{$pen->tingkat}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <div id="pendidikan_lain" class="pendidikan_lain">
                                <input id="inputpendidikan_lain" class="form-control" type="text" name="pendidikan_lain" placeholder="Pendidikan Lainnya" />
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Pelaksanaan PMI</label>
                        <div class="col-sm-5">
                            <select id="pelaksanaan_pmi" class="form-control" name="pelaksanaan_pmi" required>
                                <option selected></option>
                                <option value="Dilaksanakan">Dilaksanakan</option>
                                <option value="Tidak Dilaksanakan">Tidak Dilaksanakan</option>
                            </select>
                        </div>
                    </div> -->

                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Parameter</th>
                                <th>Instrument</th>
                                <th>Metode Pemeriksaan</th>
                                <th>Hasil Pemeriksaan</th>
                                <th>Catatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($data))
                            <?php $no = 0; ?>
                            @foreach($data as $dkey => $val)
                            <?php $no++; ?>
                            <tr>
                                <td>{{$no}}</td>
                                <td>{!!$val->nama_parameter!!}<input type="hidden" name="parameter_id[{{$dkey}}]" value="{{ $val->id }}" /></td>
                                <td>
                                    @if(count($val->Instrumen))
                                    <select id="alat{{$val->id}}" class="form-control" name="alat[{{$dkey}}]" class="form-control" >
                                        <option selected></option>
                                        <option value="Tidak Mengerjakan">Tidak Mengerjakan</option>
                                        @foreach($val->Instrumen as $ins)
                                            <option value="{{$ins->id}}">{{$ins->instrumen}}</option>
                                        @endforeach
                                    </select>
                                    @else
                                    <input type="text" id="alat{{$val->id}}" name="alat[{{$dkey}}]" class="form-control">
                                    @endif
                                    <div id="row_alat{{$val->id}}" class="inpualat{{$val->id}}">
                                        <input id="inpualat{{$val->id}}" class="form-control" type="text" name="instrument_lain[{{$dkey}}]" />
                                    </div>
                                </td>
                                <td id="metodelain{{$val->id}}">
                                @if(count($val->metodePemeriksaan))
                                <select id="kodemetode{{$val->id}}" class="form-control" name="kode[{{$dkey}}]" class="form-control">
                                    <option selected=""></option>
                                    <option value="Tidak Mengerjakan">Tidak Mengerjakan</option>
                                    @foreach($val->metodePemeriksaan as $val2)
                                      <option value="{{$val2->id}}">{!!$val2->metode_pemeriksaan!!}</option>
                                    @endforeach
                                </select>
                                @else
                                <input type="text" name="kode[{{$dkey}}]" class="form-control">
                                @endif
                                <div id="row_dim{{$val->id}}" class="inputkode{{$val->id}}">
                                    <input id="inputkode{{$val->id}}" class="form-control" type="text" name="metode_lain[{{$dkey}}]" />
                                </div>

<script>

$('#alat{{$val->id}}').change(function(e){
  console.log($(this).val());
  if ($(this).val() == 'Tidak Mengerjakan') {
    $('#hasil{{$val->id}}').attr('readonly','readonly').val('-');
    $('#kodemetode{{$val->id}} option[value!="Tidak Mengerjakan"]').prop('disabled', true);
    $('#kodemetode{{$val->id}}').attr('readonly','readonly').val('Tidak Mengerjakan').show();
  }else{
    if ($('#kodemetode{{$val->id}}').val() == 'Tidak Mengerjakan') {
        $('#kodemetode{{$val->id}}').removeAttr('readonly').val('').prop('required',true);
        $('#hasil{{$val->id}}').removeAttr('readonly').val('').prop('required',true);
    }
    $('#kodemetode{{$val->id}}').removeAttr('readonly');
    $('#kodemetode{{$val->id}} option').prop('disabled', false);
    $('#hasil{{$val->id}}').removeAttr('readonly');
  }
    
});


$('#kodemetode{{$val->id}}').change(function(e){
  console.log($(this).val());
  if ($(this).val() == 'Tidak Mengerjakan') {
    $('#hasil{{$val->id}}').attr('readonly','readonly').val('-');
    $('#alat{{$val->id}} option[value!="Tidak Mengerjakan"]').prop('disabled', true);
    $('#alat{{$val->id}}').attr('readonly','readonly').val('Tidak Mengerjakan').show();
  }else{
    if ($('#alat{{$val->id}}').val() == 'Tidak Mengerjakan') {
    $('#alat{{$val->id}}').removeAttr('readonly').val('').prop('required',true);
    $('#hasil{{$val->id}}').removeAttr('readonly').val('').prop('required',true);
    }
    $('#alat{{$val->id}}').removeAttr('readonly');
    $('#alat{{$val->id}} option').prop('disabled', false);
    $('#hasil{{$val->id}}').removeAttr('readonly');
  }
});

$(document).ready(function(){
    $("#hasil{{$val->id}}").keyup(function(){
    var value = $(this).val();
        if (value == '-' || value == 'NaN'){
            console.log('bisa');
                $("#alat{{$val->id}}").prop('required',false);
                $("#kodemetode{{$val->id}}").prop('required',false);
        }else{
            if(value == ''){
                console.log('bisa');  
                $("#alat{{$val->id}}").prop('required',false);
                $("#kodemetode{{$val->id}}").prop('required',false);
            }else{
                console.log('gak bisa');
                $("#alat{{$val->id}}").prop('required',true);
                $("#kodemetode{{$val->id}}").prop('required',true);
            }
        }
    });
});
$(function() {
    $('#row_alat{{$val->id}}').hide(); 
    $('#alat{{$val->id}}').change(function(){
    var setan  = $("#alat{{$val->id}} option:selected").text();
        if(setan.match('Alat Lain.*')) {
            $('#row_alat{{$val->id}}').show(); 
            $("#inpualat{{$val->id}}").prop('required',true);
        } else {
            $('#row_alat{{$val->id}}').hide(); 
            $("#inpualat{{$val->id}}").prop('required',false);
            $("#inpualat{{$val->id}}").val('');
        } 
    });
});

$(function() {
    $('#row_dim{{$val->id}}').hide(); 
    $('#kodemetode{{$val->id}}').change(function(){
    var setan  = $("#kodemetode{{$val->id}} option:selected").text();
        if(setan.match('Metode lain.*') ) {
            $('#row_dim{{$val->id}}').show();
            $("#inputkode{{$val->id}}").prop('required',true);
        } else {
            $('#row_dim{{$val->id}}').hide(); 
            $("#inputkode{{$val->id}}").prop('required',false);
            $("#inpuakode{{$val->id}}").val('');
        } 
    });
});
</script>
                                </td>
                                <td><input type="text" class="form-control hasil validate-hasil" autocomplete="off" id="hasil{{$val->id}}" size="16" name="hasil[{{$dkey}}]"  required></td>
                                <td>{{$val->catatan}}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <label>Keterangan :</label>
                    <p> 
                        -   Jika ada parameter yang tidak Saudara kerjakan, isi kolom hasil dengan tanda strip “-“<br>
                        -   Tanda desimal diisi menggunakan titik (.)
                    </p>
                    <br>
                    <div class="col-sm-6">
                        <label>Catatan :</label>
                        <textarea class="form-control" name="catatan"></textarea>
                    </div>
                    <div class="col-sm-6">
                        <label>Nama Penanggung jawab lab :</label>
                        <input type="text" name="penanggung_jawab" class="form-control" required>
                    </div><br>
                      {{ csrf_field() }}
                    <input type="submit" name="simpan" value="Simpan" id="simpan" class="btn btn-submit" style="margin: 15px 0px 0px 15px;">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(function() {
    $('#pendidikan_lain').hide(); 
    $('#pendidikan').change(function(){
    var setan  = $("#pendidikan option:selected").text();
        if(setan.match('Lain - lain.*')) {
            $('#pendidikan_lain').show(); 
            $("#inputpendidikan_lain").prop('required',true);
        } else {
            $('#pendidikan_lain').hide(); 
            $("#inputpendidikan_lain").prop('required',false);
            $("#inputpendidikan_lain").val('');
        } 
    });
});

$("form input").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Wajib Diisi');
    }

    @if(count($data))
    @foreach($data as $val)
        var textfield2 = $ ("#hasil{{$val->id}}").get(0);
        textfield2.setCustomValidity("");
        if (!textfield2.validity.valid) {
          textfield2.setCustomValidity('Isi dengan angka, simbol titik (.) atau strip (-)');
        }
    @endforeach
    @endif
});
$("form select").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Wajib Diisi');
    } 
});

$(document).ready(function(){
    $('#autoUpdate').fadeOut('slow');
    $('#checkbox1').change(function(){
    if(this.checked)
        $('#autoUpdate').fadeIn('slow');
    else
        $('#autoUpdate').fadeOut('slow');

    });
});


@foreach($data as $val)
@if(($val->catatan == 'Hasil pemeriksaan tanpa desimal') || ($val->catatan == 'Hasil pemeriksaan menggunakan 1 (satu) desimal') || ($val->catatan == 'Hasil pemeriksaan menggunakan 2 (dua) desimal') || ($val->catatan == 'Hasil pemeriksaan tanpa desimal'))
$("body").on("keypress","#hasil{{$val->id}}", function(evt) {
    console.log(evt.keyCode)
    var charCode = (evt.which) ? evt.which : event.keyCode
    if ((charCode < 46 || charCode > 46) && (charCode < 48 || charCode > 57))
    return false;
});
@endif
@endforeach

$(document).ready(function(){
@foreach($data as $val)
    $('#hasil{{$val->id}}').on('focus',function(){
    var placeHolder = $(this).val();
        if(placeHolder == "-"){
            $(this).val("");
        }
    });
@endforeach
});

$(document).ready(function(){
    @foreach($data as $val)
    $('#hasil{{$val->id}}').blur(function(){
        var num = parseFloat($(this).val());
        @if($val->catatan == 'Hasil pemeriksaan menggunakan 1 (satu) desimal')
        console.log(num);
        var cleanNum = num.toFixed(1);
        @elseif($val->catatan == 'Hasil pemeriksaan menggunakan 2 (dua) desimal')
        var cleanNum = num.toFixed(2);
        @elseif($val->catatan == 'Hasil pemeriksaan tanpa desimal')
        var cleanNum = num.toFixed(0);
        @endif
        if (cleanNum == 'NaN') {
            $(this).val('-');
        }else{
            $(this).val(cleanNum);
        }
    });
    @endforeach
});


$(function() {
    @foreach($data as $val)
        $('#hasil{{$val->id}}').on('input', function() {
            @if($val->catatan == 'Hasil pemeriksaan menggunakan 1 (satu) desimal')
            match = (/(\d{0,3})[^.]*((?:\.\d{0,1})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
            @elseif($val->catatan == 'Hasil pemeriksaan menggunakan 2 (dua) desimal')
            match = (/(\d{0,3})[^.]*((?:\.\d{0,2})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
            @elseif($val->catatan == 'Hasil pemeriksaan tanpa desimal')
            match = (/(\d{0,3})[^.]*((?:\.\d{0,0})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
            @endif
            this.value = match[1] + match[2];
        });
    @endforeach
});

$('#form_d').submit(function() {
  $('#simpan').button('loading')
});
</script>
<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: true,
        minView: 2
    });
</script>
@endsection

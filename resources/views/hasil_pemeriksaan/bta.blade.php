@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" id="myform" method="post" enctype="multipart/form-data">
                    <center><label>Program Pemantapan Nasional Pemantapan Mutu Eksternal Mikroskopis BTA Siklus {{$siklus}} Tahun {{$date}}</label></center><br>
                    
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$perusahaan}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Penerimaan </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" required class="form_datetime form-control" name="tgl_penerimaan" autocomplete="off">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kondisi Bahan Uji </label>
                        <div class="col-sm-9">  
                          <input type="radio" name="kondisi" value="baik" required> Baik
                          <input type="radio" name="kondisi" value="pecah/tumpah"> Pecah/Tumpah
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Pemeriksaan </label>
                        <div class="col-sm-9">
                            <div class="controls input-append date" data-link-field="dtp_input1">
                                  <input size="16" type="text" value="" required class="form_datetime form-control" name="tgl_pemeriksaan" autocomplete="off">
                                  <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Pemeriksa </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_pemeriksa" placeholder="Nama Pemeriksa" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nomor Hp </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nomor_hp" placeholder="Nomor HP" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Pendidikan Pelaksana PME</label>
                        <div class="col-sm-5">
                            <select id="pendidikan" class="form-control" name="pendidikan" required>
                                <option selected></option>
                                @foreach($pendidikan as $pen)
                                    <option value="{{$pen->id}}">{{$pen->tingkat}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <div id="pendidikan_lain" class="pendidikan_lain">
                                <input id="inputpendidikan_lain" class="form-control" type="text" name="pendidikan_lain" placeholder="Pendidikan Lainnya" />
                            </div>
                        </div>
                    </div>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Bahan Uji</th>
                                <th>Hasil Pemeriksaan</th>
                            </tr>
                        </thead>
                        <tbody id="wrapper">
                            <?php
                                for ($i=1; $i <= 10 ; $i++) { 
                            ?>
                            <tr>
                                <td>{{$i}}</td>
                                <td><input type="text" class="form-control" size="16" name="kode[]" value="{{substr($perusahaan,0, -5)}}{{$i}}/{{$siklus}}{{substr($perusahaan, 11)}}"></td>
                                <td>
                                    <select class="form-control" name="hasil[]" required>
                                        <option value=""></option>
                                        <option value="Tidak Mengerjakan">Tidak Mengerjakan</option>
                                        <option value="Negatif">Negatif</option>
                                        <option value="Scanty">Scanty</option>
                                        <option value="1+">1+</option>
                                        <option value="2+">2+</option>
                                        <option value="3+">3+</option>
                                    </select>
                                </td>
                            </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                    <!-- <div><input type="button" id="more_fields" onclick="add_fields();" value="Tambah Baris" /></div> -->
                    <div class="col-sm-6">
                        <label>Catatan :</label>
                        <textarea class="form-control" name="catatan"></textarea>
                    </div>
                    <div class="col-sm-6">
                        <label>Nama Penanggung jawab lab :</label>
                        <input type="text" name="penanggung_jawab" class="form-control" required>
                    </div><br>

                      {{ csrf_field() }}
                    <input type="submit" name="simpan" id="simpan" value="Simpan" class="btn btn-submit"  style="margin: 15px 0px 0px 15px;">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(function() {
    $('#pendidikan_lain').hide(); 
    $('#pendidikan').change(function(){
    var setan  = $("#pendidikan option:selected").text();
        if(setan.match('Lain - lain.*')) {
            $('#pendidikan_lain').show(); 
            $("#inputpendidikan_lain").prop('required',true);
        } else {
            $('#pendidikan_lain').hide(); 
            $("#inputpendidikan_lain").prop('required',false);
            $("#inputpendidikan_lain").val('');
        } 
    });
});

$("form input").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Tidak boleh kosong');
    }
});

$('#myform').submit(function() {
  $('#simpan').button('loading')
});

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = mm + '/' + dd + '/' + yyyy;
document.getElementById("demo").innerHTML = today;
</script>

<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: true,
        minView: 2
    });

function add_fields() {
    document.getElementById('wrapper').innerHTML += '<tr><td><input type="text" class="form-control" size="16" name="kode[]"></td><td><input type="text" class="form-control" size="16" name="hasil[]"></td></tr>';
}
</script>
@endsection

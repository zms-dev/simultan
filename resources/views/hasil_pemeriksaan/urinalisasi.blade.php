@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" id="form_d" method="post" enctype="multipart/form-data">
                    <center><label> 
                        FORMULIR HASIL PEMERIKSAAN PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL URINALISA <br>SIKLUS {{$siklus}} TAHUN {{$date}} <input type="hidden" name="type" value="{{$type}}">
                    </label></center><br>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <!-- <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{substr($perusahaan,0, -5)}}{{$siklus}}{{substr($perusahaan, 11)}}" placeholder="Kode Peserta" readonly> -->
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$perusahaan}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Bahan </label>
                        <div class="col-sm-9">
                            <?php 
                                if ($siklus == '1') {
                                    if ($type == 'a') {
                                        $kodebahan = 'I-01';
                                    }else{
                                        $kodebahan = 'I-02';
                                    }
                                }else{
                                    if ($type == 'a') {
                                        $kodebahan = 'II-01';
                                    }else{
                                        $kodebahan = 'II-02';
                                    }
                                }
                            ?>
                            <input type="text" class="form-control" id="kode_bahan" name="kode_bahan" value="{{$kodebahan}}" placeholder="Kode Bahan" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Penerimaan </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-link-field="dtp_input1">
                              <input size="16" type="text" value="{{$validasi->tanggal_penerimaan}}" required class="form-control" name="tanggal_penerimaan" required autocomplete="off" readonly>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kualitas Bahan </label>
                        <div class="col-sm-9">  
                          <input type="radio" required name="kualitas" value="{{$validasi->kondisi_bahan}}" checked> {{$validasi->kondisi_bahan}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Pemeriksaan </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" required class="form_datetime form-control" name="tanggal_pemeriksaan" required autocomplete="off">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Pendidikan Pelaksana PME</label>
                        <div class="col-sm-5">
                            <select id="pendidikan" class="form-control" name="pendidikan" required>
                                <option selected></option>
                                @foreach($pendidikan as $pen)
                                    <option value="{{$pen->id}}">{{$pen->tingkat}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <div id="pendidikan_lain" class="pendidikan_lain">
                                <input id="inputpendidikan_lain" class="form-control" type="text" name="pendidikan_lain" placeholder="Pendidikan Lainnya" />
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Pelaksanaan PMI</label>
                        <div class="col-sm-5">
                            <select id="pelaksanaan_pmi" class="form-control" name="pelaksanaan_pmi" required>
                                <option selected></option>
                                <option value="Dilaksanakan">Dilaksanakan</option>
                                <option value="Tidak Dilaksanakan">Tidak Dilaksanakan</option>
                            </select>
                        </div>
                    </div> -->

                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Parameter</th>
                                <th>Instrument</th>
                                <th>Reagen/Kit Carik Celup</th>
                                <th>Metode Pemeriksaan</th>
                                <th>Hasil Pemeriksaan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($data))
                            <?php $no = 0; ?>
                            @foreach($data as $dkey => $val)
                            <?php $no++; ?>
                            <tr>
                                <td>{{$no}}</td>
                                <td>{!!$val->nama_parameter!!}<input type="hidden" name="parameter_id[{{$dkey}}]" value="{{ $val->id }}" /></td>
                                <td><input type="text" class="form-control" required size="16" name="alat[{{$dkey}}]" id="instrument{{$val->id}}"></td>
                                <td>
                                    <select id="alat{{$val->id}}" class="form-control" name="reagen[{{$dkey}}]" class="form-control" >
                                        <option></option>
                                        @if($val->id == '19')
                                            @foreach($val->ReagenImunologi as $valr)
                                              <option value="{{$valr->id}}">{!!$valr->reagen!!}</option>
                                            @endforeach
                                        @else
                                            @foreach($reagen as $reg)
                                              <option value="{{$reg->id}}">{!!$reg->reagen!!}</option>
                                            @endforeach
                                        @endif
                                        <option value="Tidak Mengerjakan">Tidak Mengerjakan</option>
                                    </select>
                                    <div id="row_alat{{$val->id}}" class="inpualat{{$val->id}}">
                                        <input id="inpualat{{$val->id}}" class="form-control" type="text" name="reagen_lain[{{$dkey}}]" />
                                    </div>
                                </td>
                                <td id="metodelain{{$val->id}}">
                                    <select id="kodemetode{{$val->id}}" class="form-control" name="kode[{{$dkey}}]" class="form-control" >
                                        <option selected=""></option>
                                        @foreach($val->metodePemeriksaan as $val2)
                                          <option value="{{$val2->id}}">{!!$val2->metode_pemeriksaan!!}</option>
                                        @endforeach
                                        <option value="Tidak Mengerjakan">Tidak Mengerjakan</option>
                                    </select>
                                    <div id="row_dim{{$val->id}}" class="inputkode{{$val->id}}">
                                        <input id="inputkode{{$val->id}}" class="form-control" type="text" name="metode_lain[{{$dkey}}]" />
                                    </div>
                                </td>
                                <td>
                                    @if(count($val->hasilPemeriksaan))
                                    <select class="form-control" name="hasil[{{$dkey}}]" class="form-control" required id="hasil{{$val->id}}">
                                        <option selected></option>
                                        @foreach($val->hasilPemeriksaan as $val2)
                                          <option value="{{$val2->id}}">{!!$val2->hp!!}</option>
                                        @endforeach
                                        <option value="Tidak Mengerjakan">Tidak Mengerjakan</option>
                                    </select>
                                    @else
                                        @if($val->id == 9)
                                            <select class="form-control" id="hasil{{$val->id}}" name="hasil[{{$dkey}}]" required>
                                                <option selected=""></option>
                                                <option value="1.000">1.000</option>
                                                <option value="1.005">1.005</option>
                                                <option value="1.010">1.010</option>
                                                <option value="1.015">1.015</option>
                                                <option value="1.020">1.020</option>
                                                <option value="1.025">1.025</option>
                                                <option value="1.030">1.030</option>
                                                <option value="Tidak Mengerjakan">Tidak Mengerjakan</option>
                                            </select>
                                        @elseif($val->id == 10)
                                            <select class="form-control" id="hasil{{$val->id}}" name="hasil[{{$dkey}}]" required>
                                                <option selected=""></option>
                                                <option value="5.0">5.0</option>
                                                <option value="5.5">5.5</option>
                                                <option value="6.0">6.0</option>
                                                <option value="6.5">6.5</option>
                                                <option value="7.0">7.0</option>
                                                <option value="7.5">7.5</option>
                                                <option value="8.0">8.0</option>
                                                <option value="8.5">8.5</option>
                                                <option value="9.0">9.0</option>
                                                <option value="Tidak Mengerjakan">Tidak Mengerjakan</option>
                                            </select>
                                        @endif
                                    @endif
                                </td>
                            </tr>
<script>

$('#hasil{{$val->id}}').change(function(e){
  console.log($(this).val());
  if ($(this).val() == 'Tidak Mengerjakan') {
    $('#alat{{$val->id}} option[value!="Tidak Mengerjakan"]').prop('disabled', true);
    $('#alat{{$val->id}}').attr('readonly','readonly').val('Tidak Mengerjakan').show();
    $('#kodemetode{{$val->id}} option[value!="Tidak Mengerjakan"]').prop('disabled', true);
    $('#kodemetode{{$val->id}}').attr('readonly','readonly').val('Tidak Mengerjakan').show();
    $('#instrument{{$val->id}}').attr('readonly','readonly').val('-').show();
  }else{
    if($('#kodemetode{{$val->id}}').val() == 'Tidak Mengerjakan') {
    $('#kodemetode{{$val->id}}').removeAttr('readonly').val('').prop('required',true);
    $('#alat{{$val->id}}').removeAttr('readonly').val('').prop('required',true);
    $('#instrument{{$val->id}}').removeAttr('readonly').val('').prop('required',true);
    }
    $('#kodemetode{{$val->id}}').removeAttr('readonly');
    $('#kodemetode{{$val->id}} option').prop('disabled', false);
    $('#alat{{$val->id}} option').prop('disabled', false);
    $('#alat{{$val->id}}').removeAttr('readonly');
    $('#instrument{{$val->id}}').removeAttr('readonly');
  }
    
});

$('#alat{{$val->id}}').change(function(e){
  console.log($(this).val());
  if ($(this).val() == 'Tidak Mengerjakan') {
    $('#kodemetode{{$val->id}} option[value!="Tidak Mengerjakan"]').prop('disabled', true);
    $('#kodemetode{{$val->id}}').attr('readonly','readonly').val('Tidak Mengerjakan').show();
    $('#hasil{{$val->id}} option[value!="Tidak Mengerjakan"]').prop('disabled', true);
    $('#hasil{{$val->id}}').attr('readonly','readonly').val('Tidak Mengerjakan').show();
    $('#instrument{{$val->id}}').attr('readonly','readonly').val('-').show();
  }else{
    if($('#kodemetode{{$val->id}}').val() == 'Tidak Mengerjakan') {
    $('#kodemetode{{$val->id}}').removeAttr('readonly').val('').prop('required',true);
    $('#hasil{{$val->id}}').removeAttr('readonly').val('').prop('required',true);
    $('#instrument{{$val->id}}').removeAttr('readonly').val('').prop('required',true);
    }
    $('#kodemetode{{$val->id}}').removeAttr('readonly');
    $('#kodemetode{{$val->id}} option').prop('disabled', false);
    $('#hasil{{$val->id}} option').prop('disabled', false);
    $('#instrument{{$val->id}}').removeAttr('readonly');
  }
    
});


$('#kodemetode{{$val->id}}').change(function(e){
  console.log($(this).val());
  if ($(this).val() == 'Tidak Mengerjakan') {
    $('#alat{{$val->id}} option[value!="Tidak Mengerjakan"]').prop('disabled', true);
    $('#alat{{$val->id}}').attr('readonly','readonly').val('Tidak Mengerjakan').show();
    $('#hasil{{$val->id}} option[value!="Tidak Mengerjakan"]').prop('disabled', true);
    $('#hasil{{$val->id}}').attr('readonly','readonly').val('Tidak Mengerjakan').show();
    $('#instrument{{$val->id}}').attr('readonly','readonly').val('-').show();
  }else{
    if ($('#hasil{{$val->id}}').val() == 'Tidak Mengerjakan') {
    $('#hasil{{$val->id}}').removeAttr('readonly').val('').prop('required',true);
    $('#alat{{$val->id}}').removeAttr('readonly').val('').prop('required',true);
    $('#instrument{{$val->id}}').removeAttr('readonly').val('').prop('required',true);
    }
    $('#alat{{$val->id}}').removeAttr('readonly');
    $('#hasil{{$val->id}} option').prop('disabled', false);
    $('#alat{{$val->id}} option').prop('disabled', false);
    $('#hasil{{$val->id}}').removeAttr('readonly');
    $('#instrument{{$val->id}}').removeAttr('readonly');
  }
});

$(document).ready(function(){
    $("#hasil{{$val->id}}").change(function(){
    var value = $(this).val();
        if (value == '-' || value == 'NaN' || value == 'Tidak Mengerjakan'){
            console.log('bisa');
                $("#instrument{{$val->id}}").prop('required',false);
                $("#alat{{$val->id}}").prop('required',false);
                $("#kodemetode{{$val->id}}").prop('required',false);
        }else{
            if(value == ''){
                console.log('bisa');  
                $("#instrument{{$val->id}}").prop('required',false);
                $("#alat{{$val->id}}").prop('required',false);
                $("#kodemetode{{$val->id}}").prop('required',false);
            }else{
                console.log('gak bisa');
                $("#instrument{{$val->id}}").prop('required',true);
                $("#alat{{$val->id}}").prop('required',true);
                $("#kodemetode{{$val->id}}").prop('required',true);
            }
        }
    });
});
$(function() {
    $('#row_alat{{$val->id}}').hide(); 
    $('#alat{{$val->id}}').change(function(){
    var setan  = $("#alat{{$val->id}} option:selected").text();
        if(setan.match('Reagen Lain.*')) {
            $('#row_alat{{$val->id}}').show(); 
            $("#inpualat{{$val->id}}").prop('required',true);
        } else {
            $('#row_alat{{$val->id}}').hide(); 
            $("#inpualat{{$val->id}}").prop('required',false);
            $("#inpualat{{$val->id}}").val('');
        } 
    });
});

$(function() {
    $('#row_dim{{$val->id}}').hide(); 
    $('#kodemetode{{$val->id}}').change(function(){
    var setan  = $("#kodemetode{{$val->id}} option:selected").text();
        if(setan.match('lain-lain.*')) {
            $('#row_dim{{$val->id}}').show(); 
            $("#inputkode{{$val->id}}").prop('required',true);
        } else {
            $('#row_dim{{$val->id}}').hide(); 
            $("#inputkode{{$val->id}}").prop('required',false);
            $("#inputkode{{$val->id}}").val('');
        } 
    });
});
</script>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <label>Keterangan :</label>
                    <p> 
                        - Pilih tombol “SIMPAN” untuk menyimpan perubahan input hasil<br>
                        - Pilih tombol “KIRIM” untuk mengirim hasil final Saudara ke penyelenggara<br>
                        - Hasil yang sudah dikirim tidak bisa dirubah lagi. Kesalahan dalam penginputan hasil yang telah dikirim sepenuhnya tanggung jawab peserta.   
                    </p><br>
                    <div class="col-sm-6">
                        <label>Catatan :</label>
                        <textarea class="form-control" name="catatan"></textarea>
                    </div>
                    <div class="col-sm-6">
                        <label>Nama Penanggung jawab lab :</label>
                        <input type="text" class="form-control" name="penanggung_jawab" required>
                    </div><br>
                      {{ csrf_field() }}
                    <input type="submit" name="simpan" id="simpan" value="Simpan" class="btn btn-submit" style="margin: 15px 0px 0px 15px;">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(function() {
    $('#pendidikan_lain').hide(); 
    $('#pendidikan').change(function(){
    var setan  = $("#pendidikan option:selected").text();
        if(setan.match('Lain - lain.*')) {
            $('#pendidikan_lain').show(); 
            $("#inputpendidikan_lain").prop('required',true);
        } else {
            $('#pendidikan_lain').hide(); 
            $("#inputpendidikan_lain").prop('required',false);
            $("#inputpendidikan_lain").val('');
        } 
    });
});
$("form input").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Wajib di Isi');
    }

    @if(count($data))
    @foreach($data as $val)
        var textfield2 = $ ("#hasil{{$val->id}}").get(0);
        textfield2.setCustomValidity("");
        if (!textfield2.validity.valid) {
          textfield2.setCustomValidity('Wajib di Isi');
        }
    @endforeach
    @endif
});
$("form select").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Wajib di Isi');
    }
});

$(document).ready(function(){
@foreach($data as $val)
    $('#instrument{{$val->id}}').on('focus',function(){
    var placeHolder = $(this).val();
        if(placeHolder == "-"){
            $(this).val("");
        }
    });

    $('#instrument{{$val->id}}').on('blur',function(){
    var placeHolder = $(this).val();
        if(placeHolder == ""){
            $(this).val("-");
        }
    });
@endforeach
});


@foreach($data as $val)
$("body").on("keypress","#instrument{{$val->id}}", function(evt) {
    console.log(evt.keyCode)
    var charCode = (evt.which) ? evt.which : event.keyCode
    if ((charCode < 48 || charCode > 57) && (charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)
    return false;
});
@endforeach


// $(document).ready(function(){
//     @foreach($data as $val)
//     $('#hasil{{$val->id}}').blur(function(){
//         var num = parseFloat($(this).val());
//         @if($val->catatan == 'Hasil pemeriksaan menggunakan 1 (satu) desimal')
//         console.log(num);
//         var cleanNum = num.toFixed(1);
//         @elseif($val->catatan == 'Hasil pemeriksaan menggunakan 3 (tiga) desimal')
//         var cleanNum = num.toFixed(3);
//         @endif
//         if (cleanNum == 'NaN') {
//             $(this).val('-');
//         }else{
//             $(this).val(cleanNum);
//         }
//     });
//     @endforeach
// });

$(document).ready(function(){
    $('#autoUpdate').fadeOut('slow');
    $('#checkbox1').change(function(){
    if(this.checked)
        $('#autoUpdate').fadeIn('slow');
    else
        $('#autoUpdate').fadeOut('slow');

    });
});

$('#form_d').submit(function() {
  $('#simpan').button('loading')
});
</script>
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});
</script>
@endsection

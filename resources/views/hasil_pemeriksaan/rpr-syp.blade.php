@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" id="myform" method="post" enctype="multipart/form-data">
                    <center><label>FORMULIR HASIL PEMERIKSAAN PROGRAM  NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI SIKLUS {{$siklus}} TAHUN {{$date}}<br> HASIL PEMERIKSAAN RPR <input type="hidden" name="type" value="{{$type}}"></label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$perusahaan}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Pendidikan Pelaksana PME</label>
                        <div class="col-sm-5">
                            <select id="pendidikan" class="form-control" name="pendidikan" required>
                                <option selected></option>
                                @foreach($pendidikan as $pen)
                                    <option value="{{$pen->id}}">{{$pen->tingkat}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <div id="pendidikan_lain" class="pendidikan_lain">
                                <input id="inputpendidikan_lain" class="form-control" type="text" name="pendidikan_lain" placeholder="Pendidikan Lainnya" />
                            </div>
                        </div>
                    </div>
                    <label>2. BAHAN UJI :</label>
                    <table class="table-bordered table">
                            <tr>
                                <td>Diterima tanggal :</td>
                                <td>
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="" required class="form_datetime form-control" name="tgl_diterima" autocomplete="off">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                </td>
                                <td>Diperiksa tanggal :</td>
                                <td>
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="" required class="form_datetime form-control" name="tgl_diperiksa"autocomplete="off">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                </td>
                            </tr>
                    </table>

                    <table class="table-bordered table">
                            <tr>
                                <td rowspan="6" width="20%"> Kondisi bahan Uji saat diterima</td>
                                <td>Kode Bahan Uji</td>
                                <td>Baik/Jernih </td>
                                <td>Keruh </td>
                                <td>Lain-lain</td>
                            </tr>
                            <tr>
                                <td><select class="form-control tabung1" name="no_tabung[]" required id="tabung11">
                                        <option></option>
                                        @foreach($rujukan as $ruj)
                                            @if($ruj->group == 1)
                                                <option value="{{$ruj->kode_bahan_uji}}">{{$ruj->kode_bahan_uji}}</option>
                                            @endif
                                        @endforeach
                                    </select></td>
                                <td><input type="checkbox" class="jenis1" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input type="checkbox" class="jenis1" name="jenis[]" value="keruh"></td>
                                <td>
                                    <input type="checkbox" class="jenis1" name="jenis[]" value="lain-lain" class="lain21">
                                    <div id="row_dim1">
                                        <input type="text" name="lain[]" class="laintext1 form-control">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><select class="form-control tabung1" name="no_tabung[]" required id="tabung21">
                                        <option></option>
                                        @foreach($rujukan as $ruj)
                                            @if($ruj->group == 2)
                                                <option value="{{$ruj->kode_bahan_uji}}">{{$ruj->kode_bahan_uji}}</option>
                                            @endif
                                        @endforeach
                                    </select></td>
                                <td><input type="checkbox" class="jenis2" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input type="checkbox" class="jenis2" name="jenis[]" value="keruh"></td>
                                <td>
                                    <input type="checkbox" class="jenis2" name="jenis[]" value="lain-lain" class="lain22">
                                    <div id="row_dim2">
                                        <input type="text" name="lain[]" class="laintext2 form-control">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><select class="form-control tabung1" name="no_tabung[]" required id="tabung31">
                                        <option></option>
                                        @foreach($rujukan as $ruj)
                                            @if($ruj->group == 3)
                                                <option value="{{$ruj->kode_bahan_uji}}">{{$ruj->kode_bahan_uji}}</option>
                                            @endif
                                        @endforeach
                                    </select></td>
                                <td><input type="checkbox" class="jenis3" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input type="checkbox" class="jenis3" name="jenis[]" value="keruh"></td>
                                <td>
                                    <input type="checkbox" class="jenis3" name="jenis[]" value="lain-lain" class="lain23">
                                    <div id="row_dim3">
                                        <input type="text" name="lain[]" class="laintext3 form-control">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><select class="form-control tabung1" name="no_tabung[]" required id="tabung41">
                                        <option></option>
                                        @foreach($rujukan as $ruj)
                                            @if($ruj->group == 4)
                                                <option value="{{$ruj->kode_bahan_uji}}">{{$ruj->kode_bahan_uji}}</option>
                                            @endif
                                        @endforeach
                                    </select></td>
                                <td><input type="checkbox" class="jenis4" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input type="checkbox" class="jenis4" name="jenis[]" value="keruh"></td>
                                <td>
                                    <input type="checkbox" class="jenis4" name="jenis[]" value="lain-lain" class="lain24">
                                    <div id="row_dim4">
                                        <input type="text" name="lain[]" class="laintext4 form-control">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><select class="form-control tabung1" name="no_tabung[]" required id="tabung51">
                                        <option></option>
                                        @foreach($rujukan as $ruj)
                                            @if($ruj->group == 5)
                                                <option value="{{$ruj->kode_bahan_uji}}">{{$ruj->kode_bahan_uji}}</option>
                                            @endif
                                        @endforeach
                                    </select></td>
                                <td><input type="checkbox" class="jenis5" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input type="checkbox" class="jenis5" name="jenis[]" value="keruh"></td>
                                <td>
                                    <input type="checkbox" class="jenis5" name="jenis[]" value="lain-lain" class="lain25">
                                    <div id="row_dim5">
                                        <input type="text" name="lain[]" class="laintext5 form-control">
                                    </div>
                                </td>
                            </tr>
                            @for ($i = 0; $i < 6; $i++)
                            <script type="text/javascript">
                            $(function() {
                              enable_cb{{$i}}();
                              $(".lain2{{$i}}").click(enable_cb{{$i}});
                            });
                            function enable_cb{{$i}}() {
                              if (this.checked) {
                                $('#row_dim{{$i}}').show(); 
                              } else {
                                $('#row_dim{{$i}}').hide(); 
                              }
                            }
                            </script>
                            @endfor
                    </table>
                    <label>3. REAGEN</label>
                    <table class="table table-bordered">
                        <tr>
                            <td>Nama Reagen</td>
                            <td>
                                <select id="alat1" class="form-control" name="nama_reagen[]" idx='1' class="form-control" required>
                                    <option selected></option>
                                    @foreach($reagen as $val)
                                      <option value="{{$val->id}}">{{$val->reagen}}</option>
                                    @endforeach
                                </select>
                                <div id="row_alat1" class="inpualat1">
                                    <input id="inpualat1" class="form-control" type="text" name="reagen_lain[]" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Metode Pemeriksaan </td>
                            <td>
                                <input type="text" name="metode[]" value="" id="re1" required="" class="form-control" readonly>
                            </td>
                        </tr>
<script>                            
$(function() {
    $('#row_alat1').hide(); 
    $('#alat1').change(function(){
    var setan  = $("#alat1 option:selected").text();
        if(setan.match('Lain - lain.*')) {
            $('#row_alat1').show(); 
        } else {
            $('#row_alat1').hide(); 
        } 
    });
});
</script>                        
                        <tr>
                            <td>Nama Produsen</td>
                            <td><input type="text" name="nama_produsen[]" class="form-control" id="produsen1" readonly="" required></td>
                        </tr>
                        <tr>
                            <td>Nomor Lot/Batch</td>
                            <td><input type="text" name="nomor_lot[]" class="form-control" required id="lot1"></td>
                        </tr>
                        <tr>
                            <td>Tanggal Kadaluarsa</td>
                            <td>
                              <div class="controls input-append date" data-link-field="dtp_input1">
                                  <input size="16" type="text" value="" class="form-control form_datemo" id="tanggal1" name="tgl_kadaluarsa[]">
                                  <span class="add-on"><i class="icon-th"></i></span>
                              </div>
                            </td>
                        </tr>
                    </table>
                    <label>4. HASIL PEMERIKSAAN </label>
                    <table class="table table-bordered">
                        <tr>
                            <th>Kode Bahan Uji</th>
                            <td>Interpretasi hasil </td>
                            <td>Titer</td>
                        </tr>
                        <tr>
                            <td><input type="text" name="kode_bahan_kontrol[]" id="tabung12" class="form-control" required value="" readonly></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control " id="non1" required="">
                                    <option></option>
                                    <option value="Tanpa Test">Tanpa Test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                            <td>
                                <div class="pilihtiter1">
                                    <select name="titer[]" class="form-control" id="tit1" required="">
                                        <option></option>
                                        <option value="-">-</option>
                                        <option value="1:1">1:1</option><option value="1:2">1:2</option><option value="1:4">1:4</option><option value="1:8">1:8</option><option value="1:16">1:16</option><option value="1:32">1:32</option><option value="1:64">1:64</option><option value="1:128">1:128</option><option value="1:256">1:256</option><option value="1:512">1:512</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><input type="text" name="kode_bahan_kontrol[]" id="tabung22" class="form-control" required value="" readonly></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control " id="non2" required="">
                                    <option></option>
                                    <option value="Tanpa Test">Tanpa Test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                            <td>
                                <div class="pilihtiter2">
                                    <select name="titer[]" class="form-control" id="tit2" required="">
                                        <option></option>
                                        <option value="-">-</option>
                                        <option value="1:1">1:1</option><option value="1:2">1:2</option><option value="1:4">1:4</option><option value="1:8">1:8</option><option value="1:16">1:16</option><option value="1:32">1:32</option><option value="1:64">1:64</option><option value="1:128">1:128</option><option value="1:256">1:256</option><option value="1:512">1:512</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><input type="text" name="kode_bahan_kontrol[]" id="tabung32" class="form-control" required value="" readonly></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control" id="non3" required="">
                                    <option></option>
                                    <option value="Tanpa Test">Tanpa Test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                            <td>
                                <div class="pilihtiter3">
                                    <select name="titer[]" class="form-control" id="tit3" required="">
                                        <option></option>
                                        <option value="-">-</option>
                                        <option value="1:1">1:1</option><option value="1:2">1:2</option><option value="1:4">1:4</option><option value="1:8">1:8</option><option value="1:16">1:16</option><option value="1:32">1:32</option><option value="1:64">1:64</option><option value="1:128">1:128</option><option value="1:256">1:256</option><option value="1:512">1:512</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><input type="text" name="kode_bahan_kontrol[]" id="tabung42" class="form-control" required value="" readonly></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control " id="non4" required="">
                                    <option></option>
                                    <option value="Tanpa Test">Tanpa Test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                            <td>
                                <div class="pilihtiter4">
                                    <select name="titer[]" class="form-control" id="tit4" required="">
                                        <option></option>
                                        <option value="-">-</option>
                                        <option value="1:1">1:1</option><option value="1:2">1:2</option><option value="1:4">1:4</option><option value="1:8">1:8</option><option value="1:16">1:16</option><option value="1:32">1:32</option><option value="1:64">1:64</option><option value="1:128">1:128</option><option value="1:256">1:256</option><option value="1:512">1:512</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><input type="text" name="kode_bahan_kontrol[]" id="tabung52" class="form-control" required value="" readonly></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control" id="non5" required="">
                                    <option></option>
                                    <option value="Tanpa Test">Tanpa Test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                            <td>
                                <div class="pilihtiter5">
                                    <select name="titer[]" class="form-control" id="tit5" required="">
                                        <option></option>
                                        <option value="-">-</option>
                                        <option value="1:1">1:1</option><option value="1:2">1:2</option><option value="1:4">1:4</option><option value="1:8">1:8</option><option value="1:16">1:16</option><option value="1:32">1:32</option><option value="1:64">1:64</option><option value="1:128">1:128</option><option value="1:256">1:256</option><option value="1:512">1:512</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </table>             
@for ($i = 0; $i < 6; $i++)
<script type="text/javascript">                      
    $('#non{{$i}}').change(function(){
       if ($(this).val() == 'Tanpa Test') {
        console.log($(this).val());
        $('.pilihtiter{{$i}}').hide();
        $('#tit{{$i}} option[value="-"]').show();
        $('#tit{{$i}} option[value="-"]').attr('selected','selected');
        $('#tit{{$i}}').attr('readonly','readonly').val('-').show();
      }else if($(this).val() == 'Non Reaktif'){
        console.log($(this).val());
        $('.pilihtiter{{$i}}').hide();
        $('#tit{{$i}} option[value="-"]').show();
        $('#tit{{$i}} option[value="-"]').attr('selected','selected');
        $('#tit{{$i}}').attr('readonly','readonly').val('-').show();
      }else if($(this).val() == 'Reaktif'){
        console.log($(this).val());
        $('#tit{{$i}} option[value="-"]').hide();
        $('.pilihtiter{{$i}}').show();
        $('#tit{{$i}} option').prop('disabled', false);
        $('#tit{{$i}}').removeAttr('readonly').val('');
      }else{
        console.log($(this).val());
        $('#tit{{$i}} option[value="-"]').hide();
        $('.pilihtiter{{$i}}').show();
        $('#tit{{$i}} option').prop('disabled', false);
        $('#tit{{$i}}').removeAttr('readonly').val('');
      }
    });
</script>
@endfor
                    <div class="col-sm-6">
                        <label>Alasan bila tidak melakukan pemeriksaan :</label>
                        <textarea name="keterangan" class="form-control">-</textarea>
                    </div><br>
                    <div class="col-sm-6">
                        <label>Petugas yang melakukan pemeriksaan :</label>
                        <input type="text" name="petugas_pemeriksaan" class="form-control" required>
                    </div><br>

                      {{ csrf_field() }}
                    <input type="submit" id="simpan" name="simpan" value="Simpan" class="btn btn-submit" style="margin-top: 20px">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(function() {
    $('#pendidikan_lain').hide(); 
    $('#pendidikan').change(function(){
    var setan  = $("#pendidikan option:selected").text();
        if(setan.match('Lain - lain.*')) {
            $('#pendidikan_lain').show(); 
            $("#inputpendidikan_lain").prop('required',true);
        } else {
            $('#pendidikan_lain').hide(); 
            $("#inputpendidikan_lain").prop('required',false);
            $("#inputpendidikan_lain").val('');
        } 
    });
});
$("form input").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Tidak boleh kosong');
    }
});
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = mm + '/' + dd + '/' + yyyy;
document.getElementById("demo").innerHTML = today;


$('.decimal').keyup(function(){
    var val = $(this).val();
    if(isNaN(val)){
        val = val.replace(/[^0-9\.\<\>\:\/]/g,'');
        if(val.split('.').length>2) 
            val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});
</script>
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});

$(".form_datemo").datetimepicker({
    todayBtn: true,
    autoclose: true,
    format: "yyyy-mm-dd",
    minView: 3
});
$(document).ready(function(){
    $("#tabung11").change(function(){
        // console.log($(this).val());
        $val = $(this).val();
        $('#tabung12').val($val);
    });
    $("#tabung21").change(function(){
        $val1 = $(this).val();
        $('#tabung22').val($val1);
    });
    $("#tabung31").change(function(){
        $val2 = $(this).val();
        $('#tabung32').val($val2);
    });
    $("#tabung41").change(function(){
        $val1 = $(this).val();
        $('#tabung42').val($val1);
    });
    $("#tabung51").change(function(){
        $val2 = $(this).val();
        $('#tabung52').val($val2);
    });

    // $("#tabung11").change(function(){
    //     var asw = $(this).val(); 
    //     console.log(asw);
    //     if (asw) {
    //         $('#tabung21 option[value='+ asw +']').prop('disabled', true);
    //         $('#tabung31 option[value='+ asw +']').prop('disabled', true);
    //         $('#tabung41 option[value='+ asw +']').prop('disabled', true);
    //         $('#tabung51 option[value='+ asw +']').prop('disabled', true);
    //     }else{
    //         $('#tabung21 option').prop('disabled', false);
    //     }
    // });
    // $("#tabung11").change(function(){
    //     var asw = $(this).val(); 
    //     console.log(asw);
    //     if (asw) {
    //         $('#tabung21 option[value='+ asw +']').prop('disabled', true);
    //         $('#tabung31 option[value='+ asw +']').prop('disabled', true);
    //         $('#tabung41 option[value='+ asw +']').prop('disabled', true);
    //         $('#tabung51 option[value='+ asw +']').prop('disabled', true);
    //     }else{
    //         console.log('assw  ');
    //         // $('#tabung21').removeProp('disabled');
    //         $('#tabung21 option').prop('disabled', false);
    //         $('#tabung41 option').prop('disabled', false);
    //         $('#tabung51 option').prop('disabled', false);
    //     }
    // });

    $(".s-target").change(function(){
        var e = $(this),
        otherE = $(".s-target");
        otherE.each(function(){
            var o = $(this),
            tVal = null;
            console.log(o.attr('id'));
            if(o.attr('id') !== e.attr('id')){
                tVal = o.find('option[value="'+e.val()+'"]');
                tVal.css('display','none');
                var notMatch = o.find('option').not('[value="'+e.val()+'"]');
                notMatch.css('display','');
            }
        });

    });



});

$("input:checkbox").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input:checkbox[class='" + $box.attr("class") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});

$("select[name='nama_reagen[]']").change(function(){
  var val = $(this);
  console.log(val);
  var y = $('#re'+val.attr('idx'));
  var x = $('#produsen'+val.attr('idx'));
  var n = $('#lot'+val.attr('idx'));
  var t = $('#tanggal'+val.attr('idx'));
  $.ajax({
    type: "GET",
    url : "{{url('getreagenimun').'/'}}"+val.val(),
    success: function(addr){
        y.val(addr.Hasil[0].Metode);
        x.val(addr.Hasil[0].Produsen);
        if (y.val() == '-') {
            n.attr("required",false);
            t.attr("required",false);
        }else{
            n.attr("required",true);
            t.attr("required",true);
        }
    }
  });
});

$(".tabung1").change(function(){
    var e       = $(this),
        val     = e.val(),
        arr     = [],
        tabung  = $(".tabung1");
        tabung.find('option').css("display", "");
    tabung.each(function(){
        var t = $(this);
        arr.push(t.val());
    });
    $.each(arr,function(idx, value){
        tabung.find('option[value="'+value+'"]').css("display", "none");
    });
});

$('#myform').submit(function() {
  $('#simpan').button('loading')
});
</script>
@endsection

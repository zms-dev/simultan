@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Status PNPME Tahun {{$tahun}} Siklus {{$siklus}}</div>

                <div class="panel-body">
                  <form class="form-horizontal" method="get" enctype="multipart/form-data">
                  <div class="panel-body">
                    <div>
                        <label for="exampleInputEmail1">Siklus</label>
                        <select name="siklus" id="siklus" class="form-control coba">
                          @if(count($siklus))
                          <option value="{{$siklus}}">{{$siklus}}</option>
                          @endif
                          <option value="1">1</option>
                          <option value="2">2</option>
                        </select>
                    </div>
                    <div>
                        <label for="exampleInputEmail1">Tahun</label>
                        <div class="controls input-append date form_datetime" data-link-field="dtp_input1">
                          @if(count($siklus))
                            <input size="16" type="text" value="{{$tahun}}" readonly class="form-control coba" name="tahun" id="tahun" required>
                          @else
                            <input size="16" type="text" value="2019" readonly class="form-control coba" name="tahun" id="tahun" required>
                          @endif
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                    </div><br>
                    <input type="submit" name="proses" value="Proses" class="btn btn-submit">
                  </form>
                  <hr>
                  <p>Keterangan Warna Teks:</p>
                  <!-- <div class="terlewati">
                    Sudah Selesai
                  </div> -->
                  <div class="posisi">
                    Sedang Berjalan
                  </div><br>
                  <table class="table table-bordered">
                    <tbody class="body-data">
                      <tr>
                        <th rowspan="2"><center>No</center></th>
                        <th rowspan="2"><center>Parameter Terdaftar</center></th>
                        <th colspan="2"><center>Status</center></th>
                      </tr>
                      <tr>
                        <th>01 / TP</th>
                        <th>02 / RPR</th>
                      </tr>
                      @if(count($data))
                      @foreach($data as $key => $val)
                      <tr>
                        <td><center>{{$key+1}}</center></td>
                        <td>{{$val->bidang}}<br>{{$val->parameter}}</td>
                        @if($val->Bidang <= 3)
                        <td>
                          <div {{ ($val->pembayaran->status == 1) ? 'class=posisi' : '' }}>Proses Pembayaran</div>
                          <div {{ ($val->pembayaran->status == 2) ? 'class=posisi' : '' }}>Kirim Bahan</div>
                          <div {{ ($val->pembayaran->status == 3 && $val->typea->status == NULL) ? 'class=posisi' : '' }}>Input Hasil</div>
                          <div {{ ($val->pembayaran->status == 3 && $val->typea->status == 1) ? 'class=posisi' : '' }}>Edit Hasil</div>
                          <div {{ ($val->pembayaran->status == 3 && $val->typea->status == 2 && $val->laporan == NULL) ? 'class=posisi' : '' }}>Proses Evaluasi</div>
                          <!-- <div>Proses Evaluasi</div> -->
                          <div {{ ($val->pembayaran->status == 3 && $val->typea->status == 2 && $val->laporan != NULL) ? 'class=posisi' : '' }}>Laporan Akhir</div>
                        </td>
                        <td>
                          <div {{ ($val->pembayaran->status == 1) ? 'class=posisi' : '' }}>Proses Pembayaran</div>
                          <div {{ ($val->pembayaran->status == 2) ? 'class=posisi' : '' }}>Kirim Bahan</div>
                          <div {{ ($val->pembayaran->status == 3 && $val->typeb->status == NULL) ? 'class=posisi' : '' }}>Input Hasil</div>
                          <div {{ ($val->pembayaran->status == 3 && $val->typeb->status == 1) ? 'class=posisi' : '' }}>Edit Hasil</div>
                          <div {{ ($val->pembayaran->status == 3 && $val->typeb->status == 2 && $val->laporan == NULL) ? 'class=posisi' : '' }}>Proses Evaluasi</div>
                          <!-- <div>Proses Evaluasi</div> -->
                          <div {{ ($val->pembayaran->status == 3 && $val->typeb->status == 2 && $val->laporan != NULL) ? 'class=posisi' : '' }}>Laporan Akhir</div>
                        </td>
                        @elseif($val->Bidang == 7)
                        <td>
                          <div {{ ($val->pembayaran->status == 1) ? 'class=posisi' : '' }}>Proses Pembayaran</div>
                          <div {{ ($val->pembayaran->status == 2) ? 'class=posisi' : '' }}>Kirim Bahan</div>
                          <div {{ ($val->pembayaran->status == 3 && $val->tp->status == NULL) ? 'class=posisi' : '' }}>Input Hasil</div>
                          <div {{ ($val->pembayaran->status == 3 && $val->tp->status == 1) ? 'class=posisi' : '' }}>Edit Hasil</div>
                          <div {{ ($val->pembayaran->status == 3 && $val->tp->status == 2 && $val->laporan == NULL) ? 'class=posisi' : '' }}>Proses Evaluasi</div>
                          <!-- <div>Proses Evaluasi</div> -->
                          <div {{ ($val->pembayaran->status == 3 && $val->tp->status == 2 && $val->laporan != NULL) ? 'class=posisi' : '' }}>Laporan Akhir</div>
                        </td>
                        <td>
                          <div {{ ($val->pembayaran->status == 1) ? 'class=posisi' : '' }}>Proses Pembayaran</div>
                          <div {{ ($val->pembayaran->status == 2) ? 'class=posisi' : '' }}>Kirim Bahan</div>
                          <div {{ ($val->pembayaran->status == 3 && $val->rpr->status == NULL) ? 'class=posisi' : '' }}>Input Hasil</div>
                          <div {{ ($val->pembayaran->status == 3 && $val->rpr->status == 1) ? 'class=posisi' : '' }}>Edit Hasil</div>
                          <div {{ ($val->pembayaran->status == 3 && $val->rpr->status == 2 && $val->laporan == NULL) ? 'class=posisi' : '' }}>Proses Evaluasi</div>
                          <!-- <div>Proses Evaluasi</div> -->
                          <div {{ ($val->pembayaran->status == 3 && $val->rpr->status == 2 && $val->laporan != NULL) ? 'class=posisi' : '' }}>Laporan Akhir</div>
                        </td>
                        @else
                        <td colspan="2">
                          <div {{ ($val->pembayaran->status == 1) ? 'class=posisi' : '' }}>Proses Pembayaran</div>
                          <div {{ ($val->pembayaran->status == 2) ? 'class=posisi' : '' }}>Kirim Bahan</div>
                          <div {{ ($val->pembayaran->status == 3 && $val->status->status == NULL) ? 'class=posisi' : '' }}>Input Hasil</div>
                          <div {{ ($val->pembayaran->status == 3 && $val->status->status == 1) ? 'class=posisi' : '' }}>Edit Hasil</div>
                          <div {{ ($val->pembayaran->status == 3 && $val->status->status == 2 && $val->laporan == NULL) ? 'class=posisi' : '' }}>Proses Evaluasi</div>
                          <!-- <div>Proses Evaluasi</div> -->
                          <div {{ ($val->pembayaran->status == 3 && $val->status->status == 2 && $val->laporan != NULL) ? 'class=posisi' : '' }}>Laporan Akhir</div>
                        </td>
                        @endif
                      </tr>
                      @endforeach
                      @endif
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "yyyy",
        autoclose: true,
        todayBtn: true,
        pickerPosition: "bottom-right",
        minView: 4,
        startView: 4
    });
</script>
@endsection
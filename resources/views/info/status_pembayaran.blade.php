@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                  <div class="col-md-6 col-md-offset-3">
                  @if(count($data))
                  @foreach($data as $val)
                    <h4 style="text-align: center;" id="coutdown1{{$val->no_urut}}"></h4>
                    <div class="jumbotron" style="text-align: center;">
                      <div class="container">
                        <h2 id="coutdown{{$val->no_urut}}"></h2>
                      </div>
                    </div>
                    <div class="alert alert-warning" role="alert">
                      Pastikan transaksi Mandiri Virtual Account Anda telah terverifikasi sebelum melakukan transaksi kembali dengan metode yang sama.
                    </div>
                    <p>Transfer pembayaran ke nomor Virtual Account :</p>
                    <table>
                      <tr>
                        <td><img src="{{URL::asset('asset/img/mandiri.jpg')}}" width="200px"> &nbsp;</td>
                        <td> <h2 id="kodevr"></h2></td>
                      </tr>
                    </table>
                    <hr>
                    <p>Jumlah yang harus dibayar :</p>
                    <p><font style="color: #e7282c; font-size: 20px">Rp. {{number_format($val->total)}}</font></p>
                    <hr style="border-top: 3px solid #eee;">
                  @endforeach
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              Melalui teller bank
                            </a>
                          </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <ol>
                              <li>Isi formulir untuk tranfer dengan menggunakan virtual account bank Mandiri</li>
                              <li>Pembayaran dilakukan di teller bank mandiri</li>
                            </ol>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              Melalui mobile banking Mandiri
                            </a>
                          </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">
                            <ol>
                              <li>Log in Mobile Banking Mandiri Online </li>
                              <li>Pilih menu “Bayar” atau “Pembayaran”</li>
                              <li>Pilih “Buat Pembayaran Baru” </li>
                              <li>Pilih “Multi Payment” </li>
                              <li>Pilih Penyedia Jasa “BBLK Surabaya 89160” </li>
                              <li>Pilih “No. Virtual” </li>
                              <li>Masukkan no virtual account yang telah didapatkan lalu pilih “Tambah Sebagai Nomor Baru” </li>
                              <li>Masukkan “Nominal” lalu pilih “Konfirmasi” </li>
                              <li>Pilih “Lanjut” </li>
                              <li>Muncul tampilan konfirmasi pembayaran </li>
                              <li>Scroll ke bawah di tampilan konfirmasi pembayaran lalu pilih “Konfirmasi” </li>
                              <li>Masukkan “PIN” dan transaksi telah selesai</li>
                            </ol>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              Melalui mobile banking non Mandiri
                            </a>
                          </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">
                            <ol>
                              <li> Pilih menu transfer</li>
                              <li> Pilih bank mandiri (kode bank 008)</li>
                              <li> Masukkan nomor virtual account</li>
                              <li> Masukkan nominal yang akan dibayarkan</li>
                              <li> Pada layar konfirmasi pastikan nominal dan tujuan telah sesuai</li>
                            </ol>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                              Melalui ATM Mandiri
                            </a>
                          </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">
                            <ol>
                              <li> Pilih menu bayar/beli</li>
                              <li> Pilih multi payment</li>
                              <li> Masukkan kode 89160 lalu tekan benar</li>
                              <li> Masukkan nomer virtual account</li>
                              <li> Masukkan nominal yang akan dibayarkan</li>
                              <li> Pada layar konfirmasi pastikan nominal dan tujuan telah sesuai</li>
                            </ol>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                              Melalui ATM non Mandiri
                            </a>
                          </h4>
                        </div>
                        <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">
                            <ol>  
                              <li> Masukkan kartu atm lalu tekan password spt biasa.</li>
                              <li> Pilih menu "transaksi lainnya”</li>
                              <li> Pilih menu “transfer".</li>
                              <li> Pilih menu “ke rek bank lain”</li>
                              <li> Silakan masukan kode bank MaNdiri “ketik 008”</li>
                              <li> Pilih benar</li>
                              <li> Silakan masukan jumlah tagihan </li>
                              <li> Pilih benar</li>
                              <li> Silakan masukan nomor rekening yang dituju (diisi dengan kode virtual account)</li>
                              <li> Pilih Benar</li>
                              <li> Muncul konfirmasi — klik benar</li>
                              <li> Selesai (keluar bukti pembayaran)</li>
                            </ol>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                              Melalui Internet Banking Mandiri
                            </a>
                          </h4>
                        </div>
                        <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">
                            <ol>  
                              <li>Kunjungi website Mandiri Internet Banking: <a href="https://ibank.bankmandiri.co.id/retail3/" target="_blank">https://ibank.bankmandiri.co.id/retail3/</a> </li>
                              <li>Login dengan memasukkan USER ID dan PIN </li>
                              <li>Pilih "Pembayaran" </li>
                              <li>Pilih "Multi Payment" </li>
                              <li>Pilih "No Rekening Anda" </li>
                              <li>Pilih Penyedia Jasa "BBLK Surabaya 89160" </li>
                              <li>Pilih "No Virtual Account" </li>
                              <li>Masukkan nomor Virtual Account anda </li>
                              <li>Masuk ke halaman konfirmasi 1 </li>
                              <li>Apabila benar/sesuai, klik tombol tagihan TOTAL, kemudian klik "LANJUTKAN" </li>
                              <li>Masuk ke halaman konfirmasi 2 </li>
                              <li>Masukkan Challenge Code yang dikirimkan ke Token Internet Banking Anda, kemudian klik "Kirim" </li>
                              <li>Anda akan masuk ke halaman konfirmasi jika pembayaran telah selesai</li>
                            </ol>
                          </div>
                        </div>
                      </div>
                    </div>
                  @endif
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
@if(count($data))
@foreach($data as $val)
var countDownDate1 = new Date("{{$val->coutdown1jam}}").getTime();
var x1 = setInterval(function() {
  var now1 = new Date().getTime();
  var distance1 = countDownDate1 - now1;
  var days1 = Math.floor(distance1 / (1000 * 60 * 60 * 24));
  var hours1 = Math.floor((distance1 % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes1 = Math.floor((distance1 % (1000 * 60 * 60)) / (1000 * 60));
  var seconds1 = Math.floor((distance1 % (1000 * 60)) / 1000);
  @if(count($emailva))
    document.getElementById("coutdown1{{$val->no_urut}}").innerHTML = "Kode Virtual Akun anda telah aktif. Silahkan lakukan pembayaran sebelum waktunya habis";
  @else
    document.getElementById("coutdown1{{$val->no_urut}}").innerHTML = "Kode Virtual Akun anda akan di kirim ke Email terdaftar dalam waktu 15 menit / " + minutes1 + ":" + seconds1 + " detik";
  @endif
  if (distance1 < 0) {
    clearInterval(x);
    @if(count($emailva))
      document.getElementById("coutdown1{{$val->no_urut}}").innerHTML = "Kode Virtual Akun anda telah aktif. Silahkan lakukan pembayaran sebelum waktunya habis";
    @endif
    var countDownDate = new Date("{{$val->coutdown}}").getTime();
      var x = setInterval(function() {
        var now = new Date().getTime();
        var distance = countDownDate - now;
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        document.getElementById("coutdown{{$val->no_urut}}").innerHTML =  hours + " : "+ minutes + " : " + seconds + " detik";
        @if(count($emailva))
        document.getElementById("kodevr").innerHTML = "89160 - {{$val->no_urut}}";
        @endif
        if (distance < 0) {
          clearInterval(x);
          document.getElementById("coutdown{{$val->no_urut}}").innerHTML = "Silahkan Lakukan Pembayaran";
        }
      }, 1000);
  }
}, 1000);
@endforeach
@endif
</script>
@endsection
@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Input Hasil Bidang</div>
                <div class="panel-body">
                    @if(Session::has('message'))
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ Session::get('message') }}
                      </div>
                    @endif
                    <label>
                        <!-- - Pengisian Hasil Online PNPME Siklus 1 diperpanjang sampai tanggal 24 Mei 2018 -->
                    </label>
                    <table class="table table-bordered">
                        <tr>
                            <th width="5%"><center>No</center></th>
                            <th><center>Bidang</center></th>
                            <th colspan="2" width="15%" ><center>Siklus&nbsp;1</center></th>
                            <th colspan="2" width="15%" ><center>Siklus&nbsp;2</center></th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>

                        @if($val->id_bidang > '5')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                <!-- IF BIDANG SIPILIS -->
                                @if($val->bidang == '7')
                                    @if($val->siklus == 1 || $val->siklus == 12)
                                        @if($val->siklus_1 == 'done')
                                            @if($val->pemeriksaan == 'done')
                                                <td width="7.5%" class="siklus1{{$no}}"><center><i class="glyphicon glyphicon-ok"></i></center></td>
                                            @else
                                                <td width="7.5%" class="siklus1{{$no}}">
                                                    <a href="{{URL('').$val->Link}}/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                                </td>
                                            @endif
                                            @if($val->rpr1 == 'done')
                                                <td width="7.5%" class="siklus1{{$no}}"><center><i class="glyphicon glyphicon-ok"></i></center></td>
                                            @else
                                                <td width="7.5%" class="siklus1{{$no}}">
                                                    <a href="{{URL('hasil-pemeriksaan/rpr-syphilis')}}/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>RPR</center></a>
                                                </td>
                                            @endif
                                        @else
                                            <td width="7.5%" class="siklus1{{$no}}">
                                                <a href="{{URL('').$val->Link}}/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                            </td>
                                            <td width="7.5%" class="siklus1{{$no}}">
                                                <a href="{{URL('hasil-pemeriksaan/rpr-syphilis')}}/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>RPR</center></a>
                                            </td>
                                        @endif
                                    @else
                                        <td colspan="2" class="siklus1{{$no}}"></td>
                                    @endif
                                    @if($val->siklus == 2 || $val->siklus == 12)
                                        @if($val->siklus_2 == 'done')
                                            @if($val->pemeriksaan2 == 'done')
                                                <td width="7.5%" class="siklus2{{$no}}"><center><i class="glyphicon glyphicon-ok"></i></center></td>
                                            @else
                                                <td width="7.5%" class="siklus2{{$no}}">
                                                    <a href="{{URL('').$val->Link}}/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                                </td>
                                            @endif
                                            @if($val->rpr2 == 'done')
                                                <td width="7.5%" class="siklus2{{$no}}"><center><i class="glyphicon glyphicon-ok"></i></center></td>
                                            @else
                                                <td width="7.5%" class="siklus2{{$no}}">
                                                    <a href="{{URL('hasil-pemeriksaan/rpr-syphilis')}}/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>RPR</center></a>
                                                </td>
                                            @endif
                                        @else
                                            <td width="7.5%" class="siklus2{{$no}}">
                                                <a href="{{URL('').$val->Link}}/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                            </td>
                                            <td class="siklus2{{$no}}">
                                                <a href="{{URL('hasil-pemeriksaan/rpr-syphilis')}}/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>RPR</center></a>
                                            </td>
                                        @endif
                                    @else
                                        <td colspan="2" class="siklus2{{$no}}"></td>
                                    @endif
                                @elseif($val->SubBidang == '13')
                                    <td colspan="2" class="siklus1{{$no}}">
                                        @if($val->siklus == 1 || $val->siklus == 12)
                                            @if($val->siklus_1 == 'done')
                                            <center><i class="glyphicon glyphicon-ok"></i></center>
                                            @else
                                            <?php
                                                $date = date('Y');
                                                $lembar = DB::table('data_antibiotik')
                                                            ->where(DB::raw('YEAR(created_at)'), $date)
                                                            ->where('siklus', '1')
                                                            ->where('id_registrasi', $val->id)
                                                            ->orderBy('id', 'desc')
                                                            ->first();
                                            ?>
                                            <a href="{{URL('').$val->Link}}/{{$val->id}}?y=1"><center>
                                                <i class="glyphicon glyphicon-pencil"></i><br>Lembar @if($lembar !== NULL) {{$lembar->lembar + 1}}@else 1 @endif <br>dari 3
                                            </center></a>
                                            @endif
                                        @endif
                                    </td>
                                    <td colspan="2" class="siklus2{{$no}}">
                                        @if($val->siklus == 2 || $val->siklus == 12)
                                            @if($val->siklus_2 == 'done')
                                            <center><i class="glyphicon glyphicon-ok"></i></center>
                                            @else
                                            <?php
                                                $date = date('Y');
                                                $lembar2 = DB::table('data_antibiotik')
                                                            ->where(DB::raw('YEAR(created_at)'), $date)
                                                            ->where('siklus', '2')
                                                            ->where('id_registrasi', $val->id)
                                                            ->orderBy('id', 'desc')
                                                            ->first();
                                            ?>
                                            <a href="{{URL('').$val->Link}}/{{$val->id}}?y=2"><center>
                                                <i class="glyphicon glyphicon-pencil"></i><br>Lembar @if($lembar2 !== NULL) {{$lembar2->lembar + 1}}@else 1 @endif <br>dari 3
                                            </center></a>
                                            @endif
                                        @endif
                                    </td>
                                @else
                                    <td colspan="2" class="siklus1{{$no}}">
                                        @if($val->siklus == 1 || $val->siklus == 12)
                                            @if($val->siklus_1 == 'done')
                                            <center><i class="glyphicon glyphicon-ok"></i></center>
                                            @else
                                            <a href="{{URL('').$val->Link}}/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                            @endif
                                        @endif
                                    </td>
                                    <td colspan="2" class="siklus2{{$no}}">
                                        @if($val->siklus == 2 || $val->siklus == 12)
                                            @if($val->siklus_2 == 'done')
                                            <center><i class="glyphicon glyphicon-ok"></i></center>
                                            @else
                                            <a href="{{URL('').$val->Link}}/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                            @endif
                                        @endif
                                    </td>
                                @endif
                            </tr>
                        @elseif($val->id_bidang < '5')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                @if($val->siklus == 1 || $val->siklus == 12)
                                    @if($val->siklus_1 == 'done')
                                        @if($val->pemeriksaan == 'done')
                                        <td class="siklus1{{$no}}"><center><i class="glyphicon glyphicon-ok"></i></center></td>
                                        @else
                                        <td class="siklus1{{$no}}">
                                            <a href="{{URL('').$val->Link}}/{{$val->id}}?x=a&y=1"><center><i class="glyphicon glyphicon-pencil"></i>
                                            <br>I-01</center></a>
                                        </td>
                                        @endif
                                        @if($val->pemeriksaan2 == 'done')
                                        <td class="siklus1{{$no}}"><center><i class="glyphicon glyphicon-ok"></i></center></td>
                                        @else
                                        <td class="siklus1{{$no}}">
                                            <a href="{{URL('').$val->Link}}/{{$val->id}}?x=b&y=1"><center><i class="glyphicon glyphicon-pencil"></i>
                                            <br>I-02</center></a>
                                        </td>
                                        @endif
                                    @else 
                                        <td class="siklus1{{$no}}">
                                            <a href="{{URL('').$val->Link}}/{{$val->id}}?x=a&y=1"><center><i class="glyphicon glyphicon-pencil"></i>
                                            <br>I-01</center></a>
                                        </td>
                                        <td class="siklus1{{$no}}">
                                            <a href="{{URL('').$val->Link}}/{{$val->id}}?x=b&y=1"><center><i class="glyphicon glyphicon-pencil"></i>
                                            <br>I-02</center></a>
                                        </td>
                                    @endif
                                @else
                                    <td colspan="2" class="siklus1{{$no}}"></td>
                                @endif
                                @if($val->siklus == 2 || $val->siklus == 12)
                                    @if($val->siklus_2 == 'done')
                                        @if($val->rpr1 == 'done')
                                        <td class="siklus2{{$no}}"><center><i class="glyphicon glyphicon-ok"></i></center></td>
                                        @else
                                        <td class="siklus2{{$no}}">
                                            <a href="{{URL('').$val->Link}}/{{$val->id}}?x=a&y=2"><center><i class="glyphicon glyphicon-pencil"></i>
                                            <br>II-01</center></a>
                                        </td>
                                        @endif
                                        @if($val->rpr2 == 'done')
                                        <td class="siklus2{{$no}}"><center><i class="glyphicon glyphicon-ok"></i></center></td>
                                        @else
                                        <td class="siklus2{{$no}}">
                                            <a href="{{URL('').$val->Link}}/{{$val->id}}?x=b&y=2"><center><i class="glyphicon glyphicon-pencil"></i>
                                            <br>II-02</center></a>
                                        </td>
                                        @endif
                                    @else 
                                        <td class="siklus2{{$no}}">
                                            <a href="{{URL('').$val->Link}}/{{$val->id}}?x=a&y=2"><center><i class="glyphicon glyphicon-pencil"></i>
                                            <br>II-01</center></a>
                                        </td>
                                        <td class="siklus2{{$no}}">
                                            <a href="{{URL('').$val->Link}}/{{$val->id}}?x=b&y=2"><center><i class="glyphicon glyphicon-pencil"></i>
                                            <br>II-02</center></a>
                                        </td>
                                    @endif
                                @else
                                    <td colspan="2" class="siklus1{{$no}}"></td>
                                @endif
                                </td>
                            </tr>
                        @else
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                <td colspan="2" class="siklus1{{$no}}">
                                    @if($val->siklus == 1 || $val->siklus == 12)
                                        @if($val->siklus_1 == 'done')
                                        <center><i class="glyphicon glyphicon-ok"></i></center>
                                        @else
                                        <a href="{{URL('').$val->Link}}/{{$val->id}}?x=a&y=1"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                        @endif
                                    @endif
                                </td>
                                <td colspan="2" class="siklus2{{$no}}">
                                    @if($val->siklus == 2 || $val->siklus == 12)
                                        @if($val->siklus_2 == 'done')
                                        <center><i class="glyphicon glyphicon-ok"></i></center>
                                        @else
                                        <a href="{{URL('').$val->Link}}/{{$val->id}}?x=a&y=2"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6">Data Kosong</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

$(document).ready(function() {
    <?php $no = 0; ?>
    @foreach($data as $val)
    <?php $no++ ?>
        @if($val->status_2 != 3)
            $('.siklus2{{$no}}').empty();
        @endif
    @endforeach
});

(function (global) {

    if(typeof (global) === "undefined")
    {
        throw new Error("window is undefined");
    }

    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";

        // making sure we have the fruit available for juice....
        // 50 milliseconds for just once do not cost much (^__^)
        global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };
    
    // Earlier we had setInerval here....
    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () {
        
        noBackPlease();

        // disables backspace on page except on input fields and textarea..
        document.body.onkeydown = function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                e.preventDefault();
            }
            // stopping event bubbling up the DOM tree..
            e.stopPropagation();
        };
        
    };

})(window);
</script>
@endsection
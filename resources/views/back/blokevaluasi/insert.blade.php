@extends('layouts.navbar')  
@section('content')
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                Tambah Blok Evaluasi Peserta
              </header>
              <div class="panel-body">  
                    <form method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Email</label>
                          <select name="user_id" class="form-control select2">
                            <option></option>
                            @foreach($user as $val)
                            <option value="{{$val->id}}">{{$val->email}} - {{$val->nama_lab}}</option>
                            @endforeach
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Siklus</label>
                          <select name="siklus" class="form-control select2">
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Tahun</label>
                          <select name="tahun" class="form-control select2">
                            <option></option>
                            <?php for ($i=2018; $i <= date('Y') ; $i++) { ?>
                            <option value="{{$i}}">{{$i}}</option>
                            <?php } ?>
                          </select>
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
<script type="text/javascript">
</script>
@endsection
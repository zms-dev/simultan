@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                Edit Blok Evaluasi Peserta
              </header>
              <div class="panel-body">  
                <form method="post" enctype="multipart/form-data">
                  <div class="form-group">
                      <label for="exampleInputEmail1">Email</label>
                      <select name="user_id" class="form-control select2">
                        <option value="{{$data->id}}">{{$data->email}} - {{$data->nama_lab}}</option>
                        @foreach($user as $val)
                        <option value="{{$val->id}}">{{$val->email}} - {{$val->nama_lab}}</option>
                        @endforeach
                      </select>
                  </div>
                  <div class="form-group">
                      <label for="exampleInputEmail1">Siklus</label>
                      <select name="siklus" class="form-control select2">
                        <option value="{{$data->siklus}}"> {{$data->siklus}} </option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                      </select>
                  </div>
                  <div class="form-group">
                      <label for="exampleInputEmail1">Tahun</label>
                      <select name="tahun" class="form-control select2">
                        <option value="{{$data->tahun}}">{{$data->tahun}}</option>
                        <?php for ($i=2018; $i <= date('Y') ; $i++) { ?>
                        <option value="{{$i}}">{{$i}}</option>
                        <?php } ?>
                      </select>
                  </div>
                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                  <button type="submit" class="btn btn-info">Update</button>
                </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
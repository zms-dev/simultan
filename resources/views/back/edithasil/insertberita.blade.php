@extends('layouts.navbar')  
@section('content') 
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Tambah Berita
            </header>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </p>
            @endif
            <div class="panel-body">
                <form method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Judul</label>
                        <input type="text" class="form-control" name="judul" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Image</label>
                        <input type="file" name="img" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Teaser</label>
                        <textarea name="isi" id="editor1"></textarea>
                    </div>
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-info">Simpan</button>
                </form>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<script type="text/javascript">
CKEDITOR.replace( 'editor1' );
</script>
@endsection
@extends('layouts.navbar')  
@section('content') 
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Setting Menu Website PNPME
            </header>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </p>
            @endif
            <div class="panel-body">
            <table class="table table-bordered">
                <tr>
                    <th>Menu</th>
                    <th>Status</th>
                </tr>
                @foreach($data as $val)
                <form method="post">
                {{ csrf_field() }}
                <tr>
                    <input type="hidden" name="id" value="{{$val->id}}">
                    <td>{{$val->menu}}</td>
                    <td>
                        @if($val->status == 'hilang')
                            <button type="submit" class="btn btn-warning btn-circle">
                                <i class="glyphicon glyphicon-remove"></i>
                            </button>
                        @else
                            <button type="submit" class="btn btn-info btn-circle">
                                <i class="glyphicon glyphicon-ok"></i>
                            </button>
                        @endif
                    </td>
                </tr>
                </form>
                @endforeach
            </table>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
@endsection
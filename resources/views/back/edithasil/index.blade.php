@extends('layouts.navbar')  
@section('content') 
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Update Edit Hasil
            </header>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </p>
            @endif
            <div class="panel-body">
            <form method="post">
                  {{ csrf_field() }}
                <div class="form-group">
                    <label for="exampleInputEmail1">Siklus</label>
                    <select class="form-control" required name="siklus">
                        <option></option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select>
                </div>
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
                            <input type="submit" name="proses" class="btn green" value="Proses">
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
@endsection
@extends('layouts.navbar')  
@section('content') 
<link href="{{URL::asset('css/datatables.min.css')}}" rel="stylesheet" />
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Table Perusahaan
            </header>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </p>
            @endif
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <form method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Nama Instansi</label>
                          <input type="text" class="form-control" name="nama_lab" value="{{$data->nama_lab}}">
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Update</button>
                    </form>
                    <center><h2>Setting Edit Hasil dan Kirim Hasil</h2></center>
                    <p>
                        Klik tombol Kirim Hasil untuk mengembalikan ke Edit Hasil <br> 
                        Klik tombol Edit Hasil untuk mengembalikan ke Kirim Hasil 
                    </p>
                    <table class="table table-striped table-bordered table-hover dataTables-data1" width="100%">
                        <thead>
                            <tr>
                                <th rowspan="2">No</th>
                                <th rowspan="2"><center>Set Data Siklus 1</center></th>
                                <th rowspan="2"><center>Set Data Siklus 2</center></th>
                                <th colspan="2"><center>Hapus Tanda Terima Bahan</center></th>
                                <th rowspan="2"><center>Tahun</center></th>
                                <th rowspan="2"><center>Parameter</center></th>
                            </tr>
                            <tr>
                                <th><center>Siklus 1</center></th>
                                <th><center>Siklus 2</center></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <center><h2>Data Bidang Pendaftaran</h2></center>
                    <!-- <p>Apabila ingin mendaftar ulang, hapus semua parameter di tahun yang sama</p>
                    <p>Contoh : <br>Ingin mendaftar ulang di tahun 2019. hapus semua parameter yang ada di tahun 2019</p> -->
                    <a href="{{URL('admin/data-perusahaan/tambah-bidang')}}/{{$id}}"><button class="btn btn-primarry">Tambah Bidang Pendaftaran</button></a>
                    <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                        <thead>
                        <tr>
                            <th><center>Hapus</center></th>
                            <th>No</th>
                            <th><center>Status</center></th>
                            <th><center>Tahun</center></th>
                            <th><center>Bidang</center></th>
                            <th><center>Parameter</center></th>
                            <th><center>No VA</center></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>

@stop

@section('scriptBlock')
<script src="{{url::asset('backend/js/datatables.min.js')}}"></script>
<script type="text/javascript">
function ConfirmDelete()
{
    var x = confirm("Apakah ingin dihapus?");
    if (x)
        return true;
    else
        return false;
}
function ConfirmDeleteData()
{
    var x = confirm("Apakah ingin menghapus data input?");
    if (x)
        return true;
    else
        return false;
}
</script>
<script>
var table = $(".dataTables-data");
var dataTable = table.DataTable({
    responsive:!0,
    "serverSide":true,
    "processing":true,
    "ajax":{
        url : "{{url('admin/data-perusahaan')}}/" + "edit" + "/{{$id}}" ,
    },
    dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
    language:{
        paginate:{
            previous:"&laquo;",
            next:"&raquo;"
        },search:"_INPUT_",
        searchPlaceholder:"Search..."
    },
    "columns":[
        {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
        {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
        {"data":"status","name":"status","searchable":true,"orderable":true},
        {"data":"tahun","name":"created_at","searchable":true,"orderable":true},
        {"data":"nama","name":"nama","searchable":true,"orderable":true},
        {"data":"parameter","name":"parameter","searchable":true,"orderable":true},
        {"data":"no_urut","name":"no_urut","searchable":true,"orderable":true},
    ],
    order:[[1,"asc"]]
});


var table1 = $(".dataTables-data1");
var dataTable = table1.DataTable({
    responsive:!0,
    "serverSide":true,
    "processing":true,
    "ajax":{
        url : "{{url('admin/data-perusahaan')}}/" + "edit1" + "/{{$id}}" ,
    },
    dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
    language:{
        paginate:{
            previous:"&laquo;",
            next:"&raquo;"
        },search:"_INPUT_",
        searchPlaceholder:"Search..."
    },
    "columns":[
        {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
        {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "15%"},
        {"data":"action1","name":"action1","searchable":false,"orderable":false,"width" : "15%"},
        {"data":"bahan1","name":"bahan1","searchable":false,"orderable":false,"width" : "15%"},
        {"data":"bahan2","name":"bahan2","searchable":false,"orderable":false,"width" : "15%"},
        {"data":"tahun","name":"created_at","searchable":true,"orderable":true},
        {"data":"parameter","name":"parameter","searchable":true,"orderable":true},
    ],
    order:[[1,"asc"]]
});
</script>
@stop
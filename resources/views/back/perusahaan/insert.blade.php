@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Tambah Bidang Pendaftaran
              </header>
              <div class="panel-body">  
                    <form method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Bidang</label>
                          <select class="form-control" name="bidang" required="">
                            <option></option>
                            @foreach($parameter as $val)
                            <option value="{{$val->id}}">{{$val->bidang}} | {{$val->parameter}}</option>
                            @endforeach
                          </select>
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
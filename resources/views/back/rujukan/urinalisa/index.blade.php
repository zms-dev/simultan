@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Rujukan Urinalisa</div>
                <div class="panel-body">
                @if(Session::has('message'))
                  <div class="alert {{ Session::get('alert-class') }}" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                  </div>
                @endif
                    <div class="clearfix">
                        <div class="btn-group">
                            <a href="{{URL('admin/rujukan-urinalisa/insert')}}">
                            <button class="btn green">
                                Tambah <i class="icon-plus"></i>
                            </button>
                            </a>
                        </div>
                    </div>
                    <br>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Tahun</th>
                          <th>Siklus</th>
                          <th>Bahan</th>
                          <th>Type</th>
                          <th width="5%">Periode</th>
                          <th colspan="2" width="15%">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($rujukan as $val)
                        <tr>
                            <td>{{$val->tahun}}</td>
                            <td>{{$val->siklus}}</td>
                            <td>{{$val->bahan}}</td>
                            <td>@if($val->type == 'a') 01 @else 02 @endif</td>
                            <td>{{$val->periode}}</td>
                            <td>
                                <a href="{{URL('admin/rujukan-urinalisa/edit').'/'.$val->id}}" style="float: left; margin-bottom: 10px">
                                    <button class="btn btn-primary btn-xs">
                                        &nbsp;<i class="glyphicon glyphicon-pencil"></i>&nbsp;
                                    </button>
                                </a> &nbsp;
                                <!-- <a href="{{URL('admin/rujukan-urinalisa/delete').'/'.$val->id}}">
                                    <button class="btn btn-danger btn-xs">
                                        &nbsp;<i class="glyphicon glyphicon-trash"></i>&nbsp;
                                    </button>
                                </a> -->
                            </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{ $rujukan->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
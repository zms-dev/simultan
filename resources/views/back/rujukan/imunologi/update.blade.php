@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Edit Rujukan Imunologi
              </header>
              <div class="panel-body"> 
              <?php $i=0?> 
                    
                    <form action="{{url('admin/rujukan-imunologi/edit/')}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                      
                      <label for="exampleInputEmail1">Parameter</label>
                        <input type="text" name="parameter" class="form-control" value="{{ $data[0]->parameter }}" required="" readonly="">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Siklus</label>
                          <select class="form-control" name="siklus">
                            <option value="{{$data[0]->siklus}}">
                              @if($data[0]->siklus == 12)
                              1 & 2
                              @else
                              {{$data[0]->siklus}}
                              @endif
                            </option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="12">1 & 2</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Tahun</label>
                          <select class="form-control" name="tahun">
                            <option value="{{$data[0]->tahun}}">{{$data[0]->tahun}}</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                          </select>
                      </div>

                      <table class="table table-bordered" id="myTable" @if(Auth::user()->name != 'Imunologi Syphilis') style="width: 70%;" @else  @if($data[0]->parameter == 'RPR') style="width: 100%;" @elseif($data[0]->parameter == 'Anti TP') style="width: 70%;" @endif @endif>
                        <tr>
                          <th>Hapus</th>
                          <th>Angka Pertama</th>
                          <th>Angka Terakhir</th>
                          <th>Nilai Rujukan</th>
                          @if(Auth::user()->name == 'Imunologi Syphilis')
                          @if($data[0]->parameter == 'RPR')
                          <th id="rangeth1">Range Awal <small>*(Jika rpr)</th>
                          <th id="rangeth2">Range Akhir <small>*(Jika rpr)</small></th>
                          @endif
                          @endif
                        </tr>
                        <?php 
                          $pertama = 0;
                          $terakhir = 0;
                          $rujukan = 0;
                          $group = 0;
                          $range1 = "";
                          $range2 = "";
                          $angka = 0;
                          $total = count($data) - 1;
                          foreach ($data as $key => $val) {
                            if ($angka == 0) {
                              $pertama = $val->kode_bahan_uji;
                              $terakhir = $val->kode_bahan_uji;
                              $rujukan = $val->nilai_rujukan;
                              $group = $val->group;
                              $range1 = $val->range1;
                              $range2 = $val->range2;
                              $angka++;
                            }else{
                              if ($val->group == $group) {
                                $angka++;
                                $terakhir = $val->kode_bahan_uji;
                                if ($total == $key) {
                        ?>
                        <tr>
                          <td width="1%"><input type="button" class="btn btn-danger delete-row" value="X"></td>
                          <td>
                            <input type="text" name="pertama[]" value="{{ $pertama }}" class="form-control" required="">
                          </td>
                          <td>
                            <input type="text" name="terakhir[]" value="{{ $terakhir }}" class="form-control" required="">
                          </td>
                          <td>
                            <select class="form-control" name="nilai_rujukan[]" required="">
                              <option value="{{$rujukan}}">{{$rujukan}}</option>
                              <option value="Reaktif">Reaktif</option>
                              <option value="Non Reaktif">Non Reaktif</option>
                              @if($data[0]->parameter == 'Anti HCV')
                              <option value="Reaktif / Non Reaktif">Reaktif / Non Reaktif</option>
                              @endif
                            </select>
                          </td>
                          @if(Auth::user()->name == 'Imunologi Syphilis')
                          @if($val->parameter == 'RPR')
                          <td width="10%" class="last1"><input type="text" class="form-control last1" name="range1[]"  value="{{$range1}}"></td>
                          <td width="10%" class="last1"><input type="text" class="form-control last1" name="range2[]"  value="{{$range2}}"></td>
                          @endif
                          @endif
                        </tr>
                        <?php
                                }
                              }else{ 
                                $angka = 1;
                        ?>
                        <tr>
                          <td width="1%"><input type="button" class="btn btn-danger delete-row" value="X"></td>
                          <td>
                            <input type="text" name="pertama[]" value="{{ $pertama }}" class="form-control" required="">
                          </td>
                          <td>
                            <input type="text" name="terakhir[]" value="{{ $terakhir }}" class="form-control" required="">
                          </td>
                          <td>
                            <select class="form-control" name="nilai_rujukan[]" required="">
                              <option value="{{$rujukan}}">{{$rujukan}}</option>
                              <option value="Reaktif">Reaktif</option>
                              <option value="Non Reaktif">Non Reaktif</option>
                              @if($data[0]->parameter == 'Anti HCV')
                              <option value="Reaktif / Non Reaktif">Reaktif / Non Reaktif</option>
                              @endif
                            </select>
                          </td>
                          @if(Auth::user()->name == 'Imunologi Syphilis')
                          @if($val->parameter == 'RPR')
                          <td width="10%" class="last1"><input type="text" class="form-control last1" name="range1[]"  value="{{$range1}}"></td>
                          <td width="10%" class="last1"><input type="text" class="form-control last1" name="range2[]"  value="{{$range2}}"></td>
                          @endif
                          @endif
                        </tr>
                        <?php
                                $pertama = $val->kode_bahan_uji;
                                $terakhir = $val->kode_bahan_uji;
                                $rujukan = $val->nilai_rujukan;
                                $group = $val->group;
                                $range1 = $val->range1;
                                $range2 = $val->range2;
                              }
                            }
                          }
                        ?>
                      </table>
                      <input id="add" type="button" class="btn btn-success" value="Tambah Baris"><br><br><br>
                      {{ csrf_field() }}
                    <button type="submit" class="btn btn-info">Update</button>
                    </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
@section('scriptBlock')
<script type="text/javascript">
  var tbody = $('#myTable').children('tbody');
  var table = tbody.length ? tbody : $('#myTable');

  @if(Auth::user()->penyelenggara != 7)
  $('#add').click(function(){
    table.append('<tr><td><input type="button" class="btn btn-danger delete-row" value="X"></td><td><input type="text" name="pertama[]" class="form-control" required=""></td><td><input type="text" name="terakhir[]" class="form-control" required=""></td><td><select class="form-control" name="nilai_rujukan[]" required=""><option value=""></option><option value="Reaktif">Reaktif</option><option value="Non Reaktif">Non Reaktif</option></select></td></tr>');
  })
  @else
  $('#add').click(function(){
    table.append('<tr><td><input type="button" class="btn btn-danger delete-row" value="X"></td><td><input type="text" name="pertama[]" class="form-control" required=""></td><td><input type="text" name="terakhir[]" class="form-control" required=""></td><td><select class="form-control" name="nilai_rujukan[]" required=""><option value=""></option><option value="Reaktif">Reaktif</option><option value="Non Reaktif">Non Reaktif</option></select></td><td><select class="form-control last2" name="range1[]" id="tit5" required=""><option></option><option value="Tidak ada titer">Tidak ada titer</option><option value="1:1">1:1</option><option value="1:2">1:2</option><option value="1:4">1:4</option><option value="1:8">1:8</option><option value="1:16">1:16</option><option value="1:32">1:32</option><option value="1:64">1:64</option><option value="1:128">1:128</option><option value="1:256">1:256</option></select></td><td><select class="form-control last2" name="range2[]" id="tit5" required=""><option></option><option value="Tidak ada titer">Tidak ada titer</option><option value="1:1">1:1</option><option value="1:2">1:2</option><option value="1:4">1:4</option><option value="1:8">1:8</option><option value="1:16">1:16</option><option value="1:32">1:32</option><option value="1:64">1:64</option><option value="1:128">1:128</option><option value="1:256">1:256</option></td></tr>');

    if ($('#parameter').val() == 'RPR') {
      $(".last2").prop('required',true);
      $(".last2").prop('disabled', false);
    }else{
      $(".last2").prop('disabled', true);
      $(".last2").prop('required',false);
    }
  })
  @endif

  $(document).on('click', '.delete-row', function () {
      $(this).closest('tr').remove();
      return false;
  });

  $('.lastI').hide();
  $('.lastI').hide();
  $('#rangeth1').hide();
  $('#rangeth2').hide();
  $('#parameter').change(function(e){
    console.log($(this).val());
    if ($(this).val() == 'RPR') {
      $(".lastI").prop('required',true).show();
      $(".lastI").prop('required',true).show();
      $(".last2").prop('required',true);
      $(".last2").prop('required',true);
      $('#rangeth1').show();
      $('#rangeth2').show();
    }else{
      $(".lastI").hide();
      $(".lastI").hide();
      $(".last2").prop('required',false);
      $(".last2").prop('required',false);
      $('#rangeth1').hide();
      $('#rangeth2').hide();
    }
  }); 
</script>
@endsection
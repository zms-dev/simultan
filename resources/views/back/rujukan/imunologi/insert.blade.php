@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Tambah Rujukan Imunologi
              </header>
              <div class="panel-body">  
                    <form action="{{url('admin/rujukan-imunologi/insert')}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">

                          <label for="exampleInputEmail1">Parameter</label>
                          @if(Auth::user()->name != 'Imunologi Syphilis')  
                          <input type="" name="parameter" class="form-control"
                              @if(Auth::user()->name == 'Imunologi Anti HIV')
                                value="Anti HIV"
                              @elseif(Auth::user()->name == 'Imunologi HbsAg')
                                value="HBsAg"
                              @elseif(Auth::user()->name == 'Anti HIV PMI')
                                value="HIV PMI"
                              @elseif(Auth::user()->name == 'Imunologi Anti HCV')
                                value="Anti HCV"
                              @endif readonly
                          >
                          @else
                          <select class="form-control" name="parameter" id="parameter" required="">
                            <option value=""></option>
                            <option value="Anti TP">Anti TP</option>
                            <option value="RPR">RPR</option>
                          </select>
                          @endif
                      </div>

                      <div class="form-group">
                          <label for="exampleInputEmail1">Siklus</label>
                          <select class="form-control" name="siklus" required="">
                            <option value=""></option>
                             <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="12">1 & 2</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Tahun</label>
                          <select class="form-control" name="tahun" required="">
                            <option value=""></option>
                            <?php for ($i=2018; $i <= date('Y') ; $i++) { ?>
                            <option value="{{$i}}">{{$i}}</option>
                            <?php } ?>
                          </select>
                      </div>
                    
                      <table class="table table-bordered asu" id="myTable" @if(Auth::user()->name != 'Imunologi Syphilis') style="width: 70%;" @else style="width: 70%;" @endif>
                        <tr>
                          <th>Hapus</th>
                          <th>Angka Pertama</th>
                          <th>Angka Terakhir</th>
                          <th>Nilai Rujukan</th>
                          @if(Auth::user()->name == 'Imunologi Syphilis')
                          <th id="rangeth1">Range Awal <small>*(Jika rpr)</small></th>
                          <th id="rangeth2">Range Akhir <small>*(Jika rpr)</small></th>
                          @endif
                        </tr>
                          <?php $no=1;  $kd=0;?>
                          @for ($i = $no; $i <= 1; $i++)
                          <?php $kd++ ?>
                          <tr>
                            <td width="1%"><input type="button" class="btn btn-danger delete-row" value="X"></td>
                            <td width="20%">
                              <input type="text" name="pertama[]" class="form-control" required="" 
                              @if(Auth::user()->name == 'Imunologi Syphilis') 
                                @if($kd <= 9) value="SI00{{$kd}}" @else value="SI0{{$kd}}"
                                @endif 
                              @elseif(Auth::user()->name == 'Imunologi Anti HIV')
                                @if($kd <= 9) value="HV00{{$kd}}" @else value="HV0{{$kd}}"
                                @endif
                              @elseif(Auth::user()->name == 'Imunologi HbsAg')
                                @if($kd <= 9) value="HB00{{$kd}}" @else value="HB0{{$kd}}"
                                @endif
                              @elseif(Auth::user()->name == 'Anti HIV PMI')
                                @if($kd <= 9) value="HV00{{$kd}}" @else value="HV0{{$kd}}"
                                @endif
                              @elseif(Auth::user()->name == 'Imunologi Anti HCV')
                                @if($kd <= 9) value="HC00{{$kd}}" @else value="HC0{{$kd}}"
                                @endif
                              @endif
                                >
                            </td>
                            <td width="20%">
                              <input type="text" name="terakhir[]" class="form-control" required="" 
                              @if(Auth::user()->name == 'Imunologi Syphilis') 
                                @if($kd <= 9) value="SI00{{$kd}}" @else value="SI0{{$kd}}"
                                @endif 
                              @elseif(Auth::user()->name == 'Imunologi Anti HIV')
                                @if($kd <= 9) value="HV00{{$kd}}" @else value="HV0{{$kd}}"
                                @endif
                              @elseif(Auth::user()->name == 'Imunologi HbsAg')
                                @if($kd <= 9) value="HB00{{$kd}}" @else value="HB0{{$kd}}"
                                @endif
                              @elseif(Auth::user()->name == 'Anti HIV PMI')
                                @if($kd <= 9) value="HV00{{$kd}}" @else value="HV0{{$kd}}"
                                @endif
                              @elseif(Auth::user()->name == 'Imunologi Anti HCV')
                                @if($kd <= 9) value="HC00{{$kd}}" @else value="HC0{{$kd}}"
                                @endif
                              @endif
                                >
                            </td>
                            <td width="20%">
                              <select class="form-control" name="nilai_rujukan[]" required="">
                                <option value=""></option>
                                <option value="Reaktif">Reaktif</option>
                                <option value="Non Reaktif">Non Reaktif</option>
                                @if(Auth::user()->name == 'Imunologi Anti HCV')
                                <option value="Reaktif / Non Reaktif">Reaktif / Non Reaktif</option>
                                @endif
                              </select>
                            </td>
                            @if(Auth::user()->name == 'Imunologi Syphilis')
                            <td class="lastI" width="10%;" id="range1">                                
                              <select class="form-control last2" name="range1[]" id="tit5" required="">
                                <option></option>
                                <option value="Tidak ada titer">Tidak ada titer</option>
                                <option value="1:1">1:1</option><option value="1:2">1:2</option><option value="1:4">1:4</option><option value="1:8">1:8</option><option value="1:16">1:16</option><option value="1:32">1:32</option><option value="1:64">1:64</option><option value="1:128">1:128</option><option value="1:256">1:256</option><option value="1:512">1:512</option>
                              </select>
                            </td>
                            <td class="lastI" width="10%;" id="range2">                        
                              <select class="form-control last2" name="range2[]" id="tit5" required="">
                                <option></option>
                                <option value="Tidak ada titer">Tidak ada titer</option>
                                <option value="1:1">1:1</option><option value="1:2">1:2</option><option value="1:4">1:4</option><option value="1:8">1:8</option><option value="1:16">1:16</option><option value="1:32">1:32</option><option value="1:64">1:64</option><option value="1:128">1:128</option><option value="1:256">1:256</option><option value="1:512">1:512</option>
                              </select>
                            </td>
                            @endif
                          </tr>
                          @endfor
                      </table>
                      <input id="add" type="button" class="btn btn-success" value="Tambah Baris"><br><br><br>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
@section('scriptBlock')
<script type="text/javascript">
var tbody = $('#myTable').children('tbody');
var table = tbody.length ? tbody : $('#myTable');

@if(Auth::user()->penyelenggara != 7)
$('#add').click(function(){
  table.append('<tr><td><input type="button" class="btn btn-danger delete-row" value="X"></td><td><input type="text" name="pertama[]" class="form-control" required=""></td><td><input type="text" name="terakhir[]" class="form-control" required=""></td><td><select class="form-control" name="nilai_rujukan[]" required=""><option value=""></option><option value="Reaktif">Reaktif</option><option value="Non Reaktif">Non Reaktif</option></select></td></tr>');
})
@else
$('#add').click(function(){
  table.append('<tr><td><input type="button" class="btn btn-danger delete-row" value="X"></td><td><input type="text" name="pertama[]" class="form-control" required=""></td><td><input type="text" name="terakhir[]" class="form-control" required=""></td><td><select class="form-control" name="nilai_rujukan[]" required=""><option value=""></option><option value="Reaktif">Reaktif</option><option value="Non Reaktif">Non Reaktif</option></select></td><td><select class="form-control last2" name="range1[]" id="tit5" required=""><option></option><option value="Tidak ada titer">Tidak ada titer</option><option value="1:1">1:1</option><option value="1:2">1:2</option><option value="1:4">1:4</option><option value="1:8">1:8</option><option value="1:16">1:16</option><option value="1:32">1:32</option><option value="1:64">1:64</option><option value="1:128">1:128</option><option value="1:256">1:256</option></select></td><td><select class="form-control last2" name="range2[]" id="tit5" required=""><option></option><option value="Tidak ada titer">Tidak ada titer</option><option value="1:1">1:1</option><option value="1:2">1:2</option><option value="1:4">1:4</option><option value="1:8">1:8</option><option value="1:16">1:16</option><option value="1:32">1:32</option><option value="1:64">1:64</option><option value="1:128">1:128</option><option value="1:256">1:256</option></td></tr>');

  if ($('#parameter').val() == 'RPR') {
    $(".last2").prop('required',true);
    $(".last2").prop('disabled', false);
  }else{
    $(".last2").prop('disabled', true);
    $(".last2").prop('required',false);
  }
})
@endif


$(document).on('click', '.delete-row', function () {
    $(this).closest('tr').remove();
    return false;
});

$('.lastI').hide();
$('.lastI').hide();
$('#rangeth1').hide();
$('#rangeth2').hide();
$('#parameter').change(function(e){
  console.log($(this).val());
  if ($(this).val() == 'RPR') {
    $(".lastI").prop('required',true).show();
    $(".lastI").prop('required',true).show();
    $(".last2").prop('required',true);
    $(".last2").prop('required',true);
    $(".asu").css('width', '100%');
    $('#rangeth1').show();
    $('#rangeth2').show();
  }else{
    $(".lastI").hide();
    $(".lastI").hide();
    $(".last2").prop('required',false);
    $(".last2").prop('required',false);
    $(".asu").css('width', '70%');
    $('#rangeth1').hide();
    $('#rangeth2').hide();
  }
}); 
</script>
@endsection
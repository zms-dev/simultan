@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Tambah Rujukan TC
              </header>
              <div class="panel-body">  
                    <form action="{{url('admin/rujukan-tc/insert')}}" method="post" enctype="multipart/form-data">
                      <input type="hidden" class="form-control" name="form" value="tc">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Kode Sediaan</label>
                          <input type="text" class="form-control" name="kode_sediaan">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Rujukan</label>
                          <textarea name="rujukan" id="editor" rows="10" cols="80"></textarea>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Siklus</label>
                          <select class="form-control" name="siklus">
                            <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="12">1 & 2</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Tahun</label>
                          <select class="form-control" name="tahun">
                            <option value=""></option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                          </select>
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
<script type="text/javascript">
    CKEDITOR.replace( 'editor' );
</script>
@endsection
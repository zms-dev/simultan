@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Edit Rujukan Malaria
              </header>
              <div class="panel-body">  
                   <?php $i = 0;?>
                    @foreach($data as $val)
                    <?php $i++; ?>
                    <form action="{{url('admin/rujukan-malaria/edit').'/'.$val->id}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Kode Bahan Uji</label>
                          <input type="text" class="form-control" name="kode_bahan_uji" value="{{$val->kode_bahan_uji}}">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Rujukan</label>
                          <select class="form-control" name="rujukan" id="alat1">
                              <option value="{{$val->rujukan}}">{{$val->rujukan}}</option>
                           
                              <option value="Positif">Positif</option>
                              <option value="Negatif">Negatif</option>
                          </select>
                      </div>
                      <div id="row_alat1" class="form-group">
                          <select class="form-control" name="spesies">
                              <option value="{{$val->spesies}}">{{$val->spesies}}</option>
                              <option value="Plasmodium falciparum">Plasmodium falciparum</option>
                              <option value="Plasmodium vivax">Plasmodium vivax</option>
                              <option value="Plasmodium malariae">Plasmodium malariae</option>
                              <option value="Plasmodium ovale">Plasmodium ovale</option>
                              <option value="Plasmodium knowlesi">Plasmodium knowlesi</option>
                              <option value="Mix (Plasmodium falciparum dan Plasmodium vivax">Mix (Plasmodium falciparum dan Plasmodium vivax</option>
                              <option value="Mix (Plasmodium falciparum dan Plasmodium malariae)">Mix (Plasmodium falciparum dan Plasmodium malariae)</option>
                          </select><br>
                                <?php
                                        $stadium = explode(",", $val->stadium);
                                    ?>

                                      <select name="stadium[]" class="selectpicker form-control" multiple>
                                        <option value="" disabled>Pilih Stadium</option>
                                        <?php
                                            $selected = "";
                                            foreach ($stadium as $fkey => $value) {
                                                if($selected == ""){
                                                    if($value == "Tropozoid"){
                                                        $selected = 'selected="selected"';
                                                    }
                                                }
                                            }
                                        ?>
                                        <option value="Tropozoid"  {{ $selected }}>Tropozoid</option>
                                        <?php
                                            $selected = "";
                                            foreach ($stadium as $fkey => $value) {
                                                if($selected == ""){
                                                    if($value == "Schizont"){
                                                        $selected = 'selected="selected"';
                                                    }
                                                }
                                            }
                                        ?>
                                        <option value="Schizont" {{ $selected }}>Schizont</option>
                                        <?php
                                            $selected = "";
                                            foreach ($stadium as $fkey => $value) {
                                                if($selected == ""){
                                                    if($value == "Gametosit"){
                                                        $selected = 'selected="selected"';
                                                    }
                                                }
                                            }
                                        ?>
                                        <option value="Gametosit" {{ $selected }}>Gametosit</option>
                                    </select>
                                    <br>
                          <input type="text" name="parasit" value="{{$val->parasit}}" class="form-control" placeholder="Jumlah Parasit">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Siklus</label>
                          <select class="form-control" name="siklus">
                            <option value="{{$val->siklus}}">
                              @if($val->siklus == 12)
                              1 & 2
                              @else
                              {{$val->siklus}}
                              @endif
                            </option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="12">1 & 2</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Tahun</label>
                          <select class="form-control" name="tahun">
                            <option value="{{$val->tahun}}">{{$val->tahun}}</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                          </select>
                      </div>
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                      <button type="submit" class="btn btn-info">Update</button>
                    </form>
                    @endforeach
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
@section('scriptBlock')
      <script>                            
      $(function() {
          
          var setan  = $("#alat1 option:selected").text();
              if(setan.match('Positif.*')) {
                  $('#row_alat1').show(); 
              } else {
                $('#row_alat1').hide(); 
              }

          $('#alat1').change(function(){
          var setan  = $("#alat1 option:selected").text();
              if(setan.match('Positif.*')) {
                  $('#row_alat1').show(); 
              } else {
                  $('#row_alat1').hide(); 
              } 
          });
      });
      jQuery(document).ready(function() {
          EditableTable.init();
      });
      </script>
@endsection
@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Edit Tujukan Kimia Klinik
              </header>
              <div class="panel-body">  
                    @foreach($data as $val)
                    <form action="{{url('admin/rujukan-kimiaklinik/edit').'/'.$val->id}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Kode</label>
                          <input type="text" class="form-control" name="kode" value="{{$val->kode}}">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Nilai</label>
                          <input type="text" class="form-control" name="nilai" value="{{$val->nilai}}">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Kriteria</label>
                          <input type="text" class="form-control" name="kriteria" value="{{$val->kriteria}}">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Siklus</label>
                          <select class="form-control" name="siklus">
                            <option value="{{$val->siklus}}">
                              @if($val->siklus == 12)
                              1 & 2
                              @else
                              {{$val->siklus}}
                              @endif
                            </option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="12">1 & 2</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Tahun</label>
                          <select class="form-control" name="tahun">
                            <option value="{{$val->tahun}}">{{$val->tahun}}</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                          </select>
                      </div>
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                      <button type="submit" class="btn btn-info">Update</button>
                    </form>
                    @endforeach
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Edit Alat / Instrumen
              </header>
              <div class="panel-body">  
                    <form method="post" >
                      <div class="form-group">
                          <label for="exampleInputEmail1">Parameter</label>
                          <select class="form-control" name="parameter" required>
                              <option value="{{$data->id_parameter}}">{{$data->nama_parameter}}</option>
                              @foreach($parameter as $val)
                              <option value="{{$val->id}}">{{$val->nama_parameter}}</option>
                              @endforeach
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputPassword1">Alat / Instrumen</label>
                          <input type="text" class="form-control" name="instrumen" required value="{{$data->instrumen}}">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Status</label>
                          <select class="form-control" name="status" required>
                              <option value="{{$data->status}}">@if($data->status == 1) Tampil @else Tidak Tampil @endif</option>
                              <option value="0">Tidak Tampil</option>
                              <option value="1">Tampil</option>
                          </select>
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
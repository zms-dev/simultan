@extends('layouts.navbar')  
@section('content')

<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Ubah Jadwal
                    </header>
                    <div class="panel-body">  
                        @if(Session::has('message'))
                            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('message') }}
                            </p>
                        @endif
                        <form method="post" enctype="multipart/form-data">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th><center>Kegiatan</center></th>
                                        <th><center>Siklus 1</center></th>
                                        <th><center>Siklus 2</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $val)
                                    <tr>
                                        <td>{{$val->activity}}<input type="hidden" name="id[]" value="{{$val->id}}"></td>
                                        <td><input class="form-control" type="text" name="cycle_date_1[]" value="{{$val->cycle_date_1}}"></td>
                                        <td><input class="form-control" type="text" name="cycle_date_2[]" value="{{$val->cycle_date_2}}"></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-info">Update</button>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>

<script type="text/javascript" >
    CKEDITOR.replace( 'editor1' );
</script>
@endsection
@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Tambah Kelurahan
              </header>
              <div class="panel-body">  
                    <form action="{{url('admin/kelurahan/insert')}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Kecamatan</label>
                          <select class="form-control" name="district_id">
                            <option></option>
                            @if(count($data))
                            @foreach($data as $val)
                            <option value="{{$val->id}}">{{$val->name}}</option>
                            @endforeach
                            @endif
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Kelurahan</label>
                          <input type="text" class="form-control" name="name">
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
@extends('layouts.navbar')  
@section('content') 
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Log Input Peserta
            </header>
            <div class="panel-body">
            <form method="post" target="_blank">
                  {{ csrf_field() }}
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <select class="select2 form-control" name="email">
                        @foreach($user as $val)
                        <option value="{{$val->id}}">{{$val->email}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tahun</label>
                    <select name="tahun" class="form-control" required>
                        <option readonly>Pilih Tahun</option>
                        <option {{ $menu->tahun =="2018"?'selected':'' }} value="2018">2018</option>
                        <option {{ $menu->tahun =="2019"?'selected':'' }} value="2019">2019</option>
                        <option {{ $menu->tahun =="2020"?'selected':'' }} value="2020">2020</option>
                        <option {{ $menu->tahun =="2021"?'selected':'' }} value="2021">2021</option>
                        <option {{ $menu->tahun =="2022"?'selected':'' }} value="2022">2022</option>
                        <option {{ $menu->tahun =="2023"?'selected':'' }} value="2023">2023</option>
                        <option {{ $menu->tahun =="2024"?'selected':'' }} value="2024">2024</option>
                        <option {{ $menu->tahun =="2025"?'selected':'' }} value="2025">2025</option>
                    </select>
                </div>
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
                            <input type="submit" name="proses" class="btn green" value="Proses">
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
@endsection
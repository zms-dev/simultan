@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Edit Banner
              </header>
              <div class="panel-body">  
                    @foreach($data as $val)
                    <form action="{{url('admin/banner/edit').'/'.$val->id}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Judul</label>
                          <input type="text" class="form-control" name="judul" value="{{$val->judul}}">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputPassword1">Teaser</label>
                          <input type="text" class="form-control" name="teaser" value="{{$val->teaser}}">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputFile">Image</label>
                          <input type="file" name="image">
                      </div>
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                      <button type="submit" class="btn btn-info">Update</button>
                    </form>
                    @endforeach
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
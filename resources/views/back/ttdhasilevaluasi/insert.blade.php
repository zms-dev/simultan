@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Tambah TTD Hasil Evaluasi
              </header>
              <div class="panel-body">  
                    <form method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Tahun</label>
                          <select class="form-control" name="tahun" required>
                            <option></option>
                            <?php for ($i=2018; $i <= date('Y') ; $i++) { ?>
                              <option value="{{$i}}">{{$i}}</option>
                            <?php } ?>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputPassword1">Siklus</label>
                          <select class="form-control" name="siklus" required>
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputPassword1">Bidang</label>
                          <select class="form-control" name="bidang" required id="bidang">
                            <option></option>
                            @foreach($bidang as $val)
                              <option value="{{$val->id}}">{{$val->parameter}}</option>
                            @endforeach
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputPassword1">Nama</label>
                          <input type="text" name="nama" class="form-control" required="">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputPassword1">NIP</label>
                          <input type="number" name="nip" class="form-control" required="">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputPassword1">Tanggal Evaluasi</label>
                          <input type="text" name="tanggal" class="form-control" required="">
                      </div>
                      <div class="row_bagian form-group">
                          <label for="exampleInputEmail1">Periode</label>
                          <select class="form-control" name="periode">
                            <option value=""></option>
                            <option value="2">2</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputFile">Image</label>
                          <input type="file" name="image">
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
<script src="{{URL::asset('backend/js/jquery-1.8.3.min.js')}}"></script>
<script>                            
$(function() {
    $('.row_bagian').hide(); 
    $('#bidang').change(function(){
    var setan  = $("#bidang option:selected").val();
        console.log(setan);
        if(setan <= '3') {
            $('.row_bagian').show(); 
        } else {
            $('.row_bagian').hide(); 
        } 
    });
});
</script>   
@endsection
@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                <b>Edit No Sertifikat Siklus {{$siklus}} Tahun {{$tahun}} Bidang {{$bidang->bidang}}</b>
              </header>
              <div class="panel-body">  
                <div class="form-group">
                  <label for="exampleInputEmail1">No Sertifikat Awal</label>
                  <input type="text" name="" id="awal" >
                </div>
                <form method="post" action="{{url('admin/nomor-sertifikat/update')}}" enctype="multipart/form-data">
                  <input type="hidden" name="bidang" value="{{$bidang->id}}">
                  <input type="hidden" name="siklus" value="{{$siklus}}">
                  <input type="hidden" name="tahun" value="{{$tahun}}">
                  <table class="table table-bordered">
                    <tr>
                      <th>No</th>
                      <th>Nama Instansi</th>
                      <th>No Sertifikat</th>
                    </tr>
                    <?php $no = 0; ?>
                    @foreach($data as $val)
                    <tr>
                      <td>{{$no+1}} <input type="hidden" name="sub_bidang[]" value="{{$val->bidang}}"></td>
                      <td>{{$val->nama_lab}} <input type="hidden" name="id[]" value="{{$val->id}}"></td>
                      <td><input type="text" name="nomor[]" id="nomor{{$no}}" class="form-control" required value="{{$val->nomor}}"></td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                  </table>
                  {{ csrf_field() }}
                  <button type="submit" class="btn btn-info">Simpan</button>
                </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@stop
@section(scriptBlock)
<script>
$(document).ready(function(){
  $("#awal").keyup(function(){
    var awal = $("#awal").val();
    <?php $no = 0; ?>
    @foreach($data as $val)
      var jumlah = parseInt(awal) + {{$no}};
      $("#nomor{{$no}}").val(jumlah);
    <?php $no++; ?>
    @endforeach
  });
});
</script>
@endsection
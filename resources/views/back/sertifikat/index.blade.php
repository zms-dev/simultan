@extends('layouts.navbar')  
@section('content') 
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Table Data Sertifikat
            </header>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </p>
            @endif
            <div class="panel-body">
                <form class="form-inline" action="{{URL('admin/nomor-sertifikat/insert')}}" method="get">
                    <div class="form-group">
                        <select class="form-control" name="siklus" required>
                            <option disabled selected>Siklus</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="tahun" required>
                            <option disabled selected>Tahun</option>
                            <?php for ($i=2018; $i <= date('Y'); $i++) {?>
                                <option value="{{$i}}">{{$i}}</option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="bidang" required>
                            <option disabled selected>Bidang</option>
                            @foreach($bidang as $val)
                                <option value="{{$val->id}}">{{$val->bidang}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-default">Proses</button>
                </form>
                <div class="adv-table editable-table ">
                    <div class="space15"></div>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="40%">Bidang</th>
                            <th width="5%">Siklus</th>
                            <th width="10%">Tahun</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($data))
                        <?php
                        $no = 0;
                        ?>
                        @foreach($data as $val)
                        <?php
                        $no++;
                        ?>
                        <tr>
                            <td>{{$no}}</td>
                            <td>{{$val->bidang}}</td>
                            <td>{{$val->siklus}}</td>
                            <td>{{$val->tahun}}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
@endsection
@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Ubah Manual Book
              </header>
              <div class="panel-body">  
                    @foreach($data as $val)
                    <form action="{{url('admin/manual-book/edit').'/'.$val->id}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Keterangan Manual Book</label>
                          <input type="text" class="form-control" name="desc" value="{{$val->desc}}">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">File</label>
                          <input type="file" class="form-control" name="file">
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
                  @endforeach
              </div>
          </section>
      </div>
    </div>
  </section>
</section>

<script type="text/javascript" >
    CKEDITOR.replace( 'editor1' );
</script>
@endsection
@extends('layouts.navbar')  
@section('content') 
<link href="{{URL::asset('css/datatables.min.css')}}" rel="stylesheet" />
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Table Upload Evaluasi
            </header>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </p>
            @endif
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                    <table class="table table-striped table-bordered table-hover dataTables-data1" width="100%">
                        <thead>
                        <tr>
                            <th colspan="2"><center>Aksi</center></th>
                            <th rowspan="2">No</th>
                            <th rowspan="2">Perusahaan</th>
                            <th rowspan="2">Siklus</th>
                            <th rowspan="2">Bidang</th>
                        </tr>
                        <tr>
                            <th>A</th>
                            <th>B</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>
                    <div class="space15"></div>
                    <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                        <thead>
                        <tr>
                            <th><center>Aksi</center></th>
                            <th>No</th>
                            <th>Perusahaan</th>
                            <th>Siklus</th>
                            <th>Type</th>
                            <th>Bidang</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
@stop
@section('scriptBlock')
<script src="{{url::asset('backend/js/datatables.min.js')}}"></script>
<script>
        var table = $(".dataTables-data");
        var dataTable = table.DataTable({
        responsive:!0,
        "serverSide":true,
        "processing":true,
        "ajax":{
            url : "{{url('admin/upload-evaluasi')}}"
        },
        dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
        language:{
            paginate:{
                previous:"&laquo;",
                next:"&raquo;"
            },search:"_INPUT_",
            searchPlaceholder:"Search..."
        },
        "columns":[
            {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "5%"},
            {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
            {"data":"nama_lab","name":"perusahaan.nama_lab","searchable":true,"orderable":true},
            {"data":"siklus","name":"siklus","searchable":false,"orderable":true},
            {"data":"type","name":"type","searchable":false,"orderable":true},
            {"data":"parameter","name":"sub_bidang.parameter","searchable":true,"orderable":true},
        ],
        order:[[1,"asc"]]
    });


        var table1 = $(".dataTables-data1");
        var dataTable = table1.DataTable({
        responsive:!0,
        "serverSide":true,
        "processing":true,
        "ajax":{
            url : "{{url('admin/upload-evaluasi/register')}}"
        },
        dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
        language:{
            paginate:{
                previous:"&laquo;",
                next:"&raquo;"
            },search:"_INPUT_",
            searchPlaceholder:"Search..."
        },
        "columns":[
            {"data":"actiona","name":"actiona","searchable":false,"orderable":false,"width" : "5%"},
            {"data":"actionb","name":"actionb","searchable":false,"orderable":false,"width" : "5%"},
            {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
            {"data":"nama_lab","name":"perusahaan.nama_lab","searchable":true,"orderable":true},
            {"data":"siklus","name":"siklus","searchable":false,"orderable":true},
            {"data":"parameter","name":"sub_bidang.parameter","searchable":true,"orderable":true},
        ],
        order:[[1,"asc"]]
    });

</script>
@stop
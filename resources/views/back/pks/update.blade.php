@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Edit Perjanjian Kerjasama
              </header>
              <div class="panel-body">  
                    @foreach($data as $val)
                    <form action="{{url('admin/pks/edit').'/'.$val->id}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">PKS</label>
                          <input type="text" class="form-control" name="pks" value="{{$val->pks}}">
                      </div>
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                      <button type="submit" class="btn btn-info">Update</button>
                    </form>
                    @endforeach
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
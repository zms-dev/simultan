@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Tambah Metode Pemeriksaan
              </header>
              <div class="panel-body">  
                    <form method="post" >
                      <div class="form-group">
                          <label for="exampleInputEmail1">Parameter</label>
                          <select class="form-control" name="parameter" required>
                              <option></option>
                              @foreach($parameter as $val)
                              <option value="{{$val->id}}">{{$val->nama_parameter}}</option>
                              @endforeach
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputPassword1">Metode Pemeriksaan</label>
                          <input type="text" class="form-control" name="metode_pemeriksaan" required>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputPassword1">Kode</label>
                          <input type="text" class="form-control" name="kode" required>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Status</label>
                          <select class="form-control" name="status" required>
                              <option></option>
                              <option value="0">Tidak Tampil</option>
                              <option value="1">Tampil</option>
                          </select>
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
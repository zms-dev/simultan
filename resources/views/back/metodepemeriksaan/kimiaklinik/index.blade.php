@extends('layouts.navbar')  
@section('content') 
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Table Metode Pemeriksaan
            </header>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </p>
            @endif
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
                            <a href="{{URL('admin/metode-pemeriksaan/kimia-klinik/insert')}}">
                            <button class="btn green">
                                Tambah Data <i class="icon-plus"></i>
                            </button>
                            </a>
                        </div>
                    </div>
                    <div class="space15"></div>
                    <br>
                    <form class="form-horizontal" method="get" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="nama" class="col-sm-2 control-label">Parameter :</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="parameter" required>
                                    <option></option>
                                    @foreach($parameter as $val)
                                    <option value="{{$val->id}}">{{$val->nama_parameter}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <input type="submit" name="cari" class="btn btn-primary" value="Cari">
                            </div>
                        </div>
                    </form>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                        <tr>
                            <th width="5%"><center>Id</center></th>
                            <th>Kode</th>
                            <th>Metode Pemeriksaan</th>
                            <th>Parameter</th>
                            <th>Status</th>
                            <th width="5%">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($data))
                        <?php
                        $no = 0;
                        ?>
                        @foreach($data as $val)
                        <?php
                        $no++;
                        ?>
                        <tr>
                            <td><center>{{$val->id}}</center></td>
                            <td>{{$val->kode}}</td>
                            <td>{!!$val->metode_pemeriksaan!!}</td>
                            <td>{{$val->nama_parameter}}</td>
                            <td>@if($val->status == 1) Tampil @else Tidak Tampil @endif</td>
                            <td>
                                <a href="{{URL('admin/metode-pemeriksaan/kimia-klinik/edit').'/'.$val->id}}" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        <i class="icon-pencil"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
@endsection
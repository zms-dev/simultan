@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Tambah Master Provinsi
              </header>
              <div class="panel-body">  
                    <form action="{{url('admin/provinsi/insert')}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Provinsi</label>
                          <input type="text" class="form-control" name="name">
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
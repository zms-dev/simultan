@extends('layouts.navbar')  
@section('content') 
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Table Banner
            </header>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </p>
            @endif
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
                            <a href="{{URL('admin/banner/insert')}}">
                            <button class="btn green">
                                Tambah <i class="icon-plus"></i>
                            </button>
                            </a>
                        </div>
                    </div>
                    <div class="space15"></div>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="40%">Judul</th>
                            <th width="40%">Teaser</th>
                            <th>Image</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($banner))
                        <?php
                        $no = 0;
                        ?>
                        @foreach($banner as $val)
                        <?php
                        $no++;
                        ?>
                        <tr>
                            <td>{{$no}}</td>
                            <td>{{$val->judul}}</td>
                            <td>{{$val->teaser}}</td>
                            <td><img src="{{URL::asset('asset/backend/banner').'/'.$val->image}}" width="100px"></td>
                            <td>
                                <a href="{{URL('admin/banner/edit').'/'.$val->id}}">
                                    <button class="btn btn-primary btn-xs">
                                        <i class="icon-pencil"></i>
                                    </button>
                                </a><p></p>
                                <a href="{{URL('admin/banner/delete').'/'.$val->id}}">
                                    <button class="btn btn-danger btn-xs">
                                        <i class="icon-trash "></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
@endsection
@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Tambah Banner
              </header>
              <div class="panel-body">  
                    <form action="{{url('admin/banner/insert')}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Judul</label>
                          <input type="text" class="form-control" name="judul">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputPassword1">Teaser</label>
                          <input type="text" class="form-control" name="teaser">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputFile">Image</label>
                          <input type="file" name="image">
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
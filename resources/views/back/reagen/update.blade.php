@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Edit Reagen
              </header>
              <div class="panel-body">  
                    @foreach($data as $val)
                    <form action="{{url('admin/reagen-imunologi/edit').'/'.$val->id}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Reagen</label>
                          <input type="text" class="form-control" name="reagen" value="{{$val->reagen}}" required>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Kelompok</label>
                          <select class="form-control" name="kelompok" required>
                            <option value="{{$val->kelompok}}">{{$val->kelompok}}</option>
                            <option value="Anti HIV">Anti HIV</option>
                            <option value="Anti HCV">Anti HCV</option>
                            <option value="Anti TP">Anti TP</option>
                            <option value="RPR">RPR</option>
                            <option value="hbsag">hbsag</option>
                            <option value="Urinalisa">Urinalisa</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Produsen</label>
                          <input type="text" class="form-control" name="produsen" value="{{$val->produsen}}" required>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Metode</label>
                          <select class="form-control" name="metode" required>
                            <option value="{{$val->metode}}">{{$val->metode}}</option>
                            <option value="EIA / Setara">EIA / Setara</option>
                            <option value="Rapid">Rapid</option>
                            <option value="Flokulasi">Flokulasi</option>
                            <option value="aglutinasi">aglutinasi</option>
                          </select>
                      </div>
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                      <button type="submit" class="btn btn-info">Update</button>
                    </form>
                    @endforeach
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Tambah Reagen
              </header>
              <div class="panel-body">  
                    <form action="{{url('admin/reagen-imunologi/insert')}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Reagen</label>
                          <input type="text" class="form-control" name="reagen" required>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Kelompok</label>
                          <select class="form-control" name="kelompok" required>
                            <option value=""></option>
                            <option value="Anti HIV">Anti HIV</option>
                            <option value="Anti HCV">Anti HCV</option>
                            <option value="Anti TP">Anti TP</option>
                            <option value="RPR">RPR</option>
                            <option value="hbsag">hbsag</option>
                            <option value="Urinalisa">Urinalisa</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Produsen</label>
                          <input type="text" class="form-control" name="produsen" required>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Metode</label>
                          <select class="form-control" name="metode" required>
                            <option value=""></option>
                            <option value="EIA / Setara">EIA / Setara</option>
                            <option value="Rapid">Rapid</option>
                            <option value="Flokulasi">Flokulasi</option>
                            <option value="aglutinasi">aglutinasi</option>
                          </select>
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
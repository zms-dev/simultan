@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Informasi dan Dokumen PNPME</div>

                <div class="panel-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>File</th>
                            <th>Download</th>
                        </tr>
                        @if(count($sptjm))
                        <?php $no = 0; ?>
                        @foreach($sptjm as $val)
                        <?php $no++ ?>
                        <tr>
                          <td><center>{{$no}}</center></td>
                          <td width="90%">{{$val->file}}</td>
                          <td><center>
                            <a href="{{URL::asset('asset/backend/sptjm').'/'.$val->file}}" download>
                                <i class="glyphicon glyphicon-cloud-download"></i></center>
                            </a>
                          </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="3">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
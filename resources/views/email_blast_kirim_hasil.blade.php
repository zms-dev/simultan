@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Email Blast Kirim Hasil</div>

                <div class="panel-body">
                    @if($showAllertPending)
                        <div class="alert alert-info">System masih melakukan proses pengiriman Email..</div><br>
                    @endif
                    @if($showAllertSuccess)
                        <div class="alert alert-success">System telah selesai melakukan proses pengiriman Email..</div><br>
                    @endif
                    @if($showForm)
                    <form class="form-horizontal" method="post" enctype="multipart/form-data"  >
                        <div>
                            <label for="exampleInputEmail1">Siklus</label>
                            <select class="form-control" name="siklus">
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                            </select>
                            <label for="exampleInputEmail1">Bidang</label>
                            <select class="form-control" name="bidang">
                                <option></option>
                                @foreach($data as $val)
                                <option value="{{$val->id}}">{{$val->alias}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div>
                            <label>Subjek :</label>
                            <input type="text" name="sub" class="form-control">
                            <label>Deskripsi :</label>
                            <textarea class="form-control" id="editor1" name="desc"></textarea> 
                            <br>
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <input type="submit" name="kirim" class="btn btn-succes" value="Kirim Email">
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
CKEDITOR.replace( 'editor1' );
</script>
@endsection
@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Data Peserta Virtual Account</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="5px">Check</th>
                                    <th><center>Nama Instansi</center></th>
                                    <th><center>No.Hp / No.WA</center></th>
                                    <th><center>Email</center></th>
                                    <th><center>Virtual Akun</center></th>
                                    <th><center>Daftar</center></th>
                                    <th width="5px"><center>Hapus Pendaftaran</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $key => $val)
                                <tr>
                                    <td>
                                        <center>
                                            @if(count($val->emailva))
                                            @else
                                                <input type='checkbox' name='peserta[]' class='checkbidang' value="{{$val->id}}">
                                            @endif
                                        </center>
                                    </td>
                                    <td>{{$val->name}}</td>
                                    <td>{{$val->no_hp}} / {{$val->no_wa}}</td>
                                    <td>{{$val->email}}</td>
                                    <td>{{date("d-m-Y H:i:s", strtotime($val->created_at))}}</td>
                                    <td><center>{{$val->no_urut}}</center></td>
                                    <td>
                                        <a onclick="return confirm('Apa anda yakin ingin menghapus data pendaftaran ini?');" href="{{URL('data-peserta-va/hapus')}}/{{$val->id}}"><center><i class="glyphicon glyphicon-trash"></i></center></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-info">Kirim Email</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
 $('#example').DataTable( {
      "order": false
  } );
});

$("#checkAll").click(function () {
$('input:checkbox').not(this).prop('checked', this.checked);
});
</script>
@endsection
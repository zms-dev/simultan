@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Rujukan per Jenis Instansi</div>
                <div class="panel-body">
                  <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                  @if($sub->id == 7)
                  <div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>

<script>
@if($sub->id == 7)
Highcharts.chart('container2', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
        @foreach($data as $val)
            '{{$val->badan_usaha}}',
        @endforeach
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Peserta (SIF RPR)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            dataLabels: {
                    enabled: true,
                    crop: false,
                    overflow: 'none'
                }
        }
    },
    series: [{
        name: 'Sesuai Rujukan',
        data: [
                @foreach($data as $val)
                {{$val->baikrpr}},
                @endforeach
                ]

    }, {
        name: 'Tidak Sesuai Rujukan',
        data: [
                @foreach($data as $val)
                {{$val->tidakbaikrpr}},
                @endforeach
                ]

    }]
});
@endif
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
        @foreach($data as $val)
            '{{$val->badan_usaha}}',
        @endforeach
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            @if($sub->id == 7)
                text: 'Peserta ({{$sub->alias}} TP)'
            @else
                text: 'Peserta ({{$sub->alias}})'
            @endif
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            dataLabels: {
                    enabled: true,
                    crop: false,
                    overflow: 'none'
                }
        }
    },
    series: [{
        name: 'Sesuai Rujukan',
        data: [
                @foreach($data as $val)
                {{$val->baik}},
                @endforeach
                ]

    }, {
        name: 'Tidak Sesuai Rujukan',
        data: [
                @foreach($data as $val)
                {{$val->tidakbaik}},
                @endforeach
                ]

    }]
});
</script>
@endsection
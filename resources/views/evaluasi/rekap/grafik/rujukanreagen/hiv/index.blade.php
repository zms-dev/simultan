@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Rujukan per Reagen</div>
                <div class="panel-body">
                  <div id="container" style="min-width: 310px; margin: 0 auto"></div>
                  <div id="container2" style="min-width: 310px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

Highcharts.chart('container', {
  chart: {
    type: 'column'
  },
  title: {
    text: ''
  },
  subtitle: {
    @if(Auth::user()->penyelenggara==7)
      text: 'KESESUAIAN DENGAN RUJUKAN PER REAGEN ANTI TP'
    @else
      text: 'KESESUAIAN DENGAN RUJUKAN PER REAGEN '
    @endif
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Jumlah'
    }
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
  },

  "series": [
    {
      "name": "Reagen",
      "colorByPoint": true,
      "data": [
        @foreach($data as $val)
          @if(Auth::user()->penyelenggara==7)
            @if($val->jenis_form == "Syphilis")
            {
              "name": "{{$val->reagen}}",
              "y": {{$val->total}},
              "drilldown": "{{$val->reagen}}"
            },
            @endif
          @else
            {
              "name": "{{$val->reagen}}",
              "y": {{$val->total}},
              "drilldown": "{{$val->reagen}}"
            },
          @endif
        @endforeach
      ]
    }
  ],
  "drilldown": {
    "series": [
      @foreach($data as $val)
      {
        "colorByPoint": true,
        "name": "{{$val->reagen}}",
        "id": "{{$val->reagen}}",
        "data": [
          [
            "Total",
            {{$val->total}}
          ],
          [
            "Sesuai",
            {{$val->sesuai}}
          ],
          [
            "Tidak Sesuai",
            {{$val->tidaksesuai}}
          ],
        ]
      },
      @endforeach
    ]
  }
});

@if(Auth::user()->penyelenggara==7)
Highcharts.chart('container2', {
  chart: {
    type: 'column'
  },
  title: {
    text: ''
  },
  subtitle: {
    text: 'KESESUAIAN DENGAN RUJUKAN PER REAGEN RPR'
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Jumlah'
    }
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
  },

  "series": [
    {
      "name": "Reagen",
      "colorByPoint": true,
      "data": [
        @foreach($data as $val)
        @if($val->jenis_form == "rpr-syphilis")
        {
          "name": "{{$val->reagen}}",
          "y": {{$val->total}},
          "drilldown": "{{$val->reagen}}"
        },
        @endif
        @endforeach
      ]
    }
  ],
  "drilldown": {
    "series": [
      @foreach($data as $val)
      {
        "colorByPoint": true,
        "name": "{{$val->reagen}}",
        "id": "{{$val->reagen}}",
        "data": [
          [
            "Total",
            {{$val->total}}
          ],
          [
            "Sesuai",
            {{$val->sesuai}}
          ],
          [
            "Tidak Sesuai",
            {{$val->tidaksesuai}}
          ],
        ]
      },
      @endforeach
    ]
  }
});
@endif
</script>
@endsection
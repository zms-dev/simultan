@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Nilai Peserta</div>
                <div class="panel-body">
                  <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                  <div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
            'Kategori',
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Peserta ({{$sub->alias}})'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            dataLabels: {
                    enabled: true,
                    crop: false,
                    overflow: 'none'
                }
        }
    },
    series: [{
        name: 'Nilai Baik',
        data: [
                @if(Auth::user()->penyelenggara==7)
                {{$baik}},
                @else
                {{$baik}}
                @endif
                ]

    }, {
        name: 'Nilai Kurang',
        data: [
                @if(Auth::user()->penyelenggara==7)
                {{$tidakbaik}},
                @else
                {{$tidakbaik}}
                @endif
                ]

    },{
        name: 'Tidak Dapat Nilai',
        data: [
                @if(Auth::user()->penyelenggara==7)
                {{$tidakdapatnilai}},
                @else
                {{$tidakdapatnilai}}
                @endif
                ]

    }]
});

@if(Auth::user()->penyelenggara==7)
Highcharts.chart('container2', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
            'Kategori',
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Peserta (RPR)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            dataLabels: {
                    enabled: true,
                    crop: false,
                    overflow: 'none'
                }
        }
    },
    series: [{
        name: 'Nilai Baik',
        data: [
                @if(Auth::user()->penyelenggara==7)
                {{$baikrpr}},
                @else
                {{$baik}}
                @endif
                ]

    }, {
        name: 'Nilai Kurang',
        data: [
                @if(Auth::user()->penyelenggara==7)
                {{$tidakbaikrpr}},
                @else
                {{$tidakbaik}}
                @endif
                ]

    },{
        name: 'Tidak Dapat Nilai',
        data: [
                @if(Auth::user()->penyelenggara==7)
                {{$tidakdapatnilai}},
                @else
                {{$tidakdapatnilai}}
                @endif
                ]

    }]
});
@endif
</script>
@endsection
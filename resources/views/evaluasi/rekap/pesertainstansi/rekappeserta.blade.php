@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Rekap Peserta per Instansi</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post" id="uangmasuk" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Parameter</label>
                          <select class="form-control" name="bidang">
                            <option></option>
                            @foreach($data as $val)
                            <option value="{{$val->id}}">{{$val->alias}}</option>
                            @endforeach
                          </select>
                          <div>
                                <label for="exampleInputEmail1">Tahun</label>
                                <div class="controls input-append date form_datetime" data-link-field="dtp_input1">
                                    <input size="16" type="text" value="" readonly class="form-control" name="tahun" id="tahun">
                                    <span class="add-on"><i class="icon-th"></i></span>
                                </div>
                          </div>
                      </div><br>
                    <input type="submit" name="proses" class="btn btn-primary" value="Proses">
                    {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});
</script>
@endsection
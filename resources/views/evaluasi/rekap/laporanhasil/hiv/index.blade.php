<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
}

#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;   
    text-align: center;
}

table, tr, td {
    text-align: center;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 2px;
}
</style>
@if(count($data))
<table id="peserta_pme">
@foreach($data as $perusahaan)
    <tr class="text-tengah">
        <th>{{$perusahaan->kode_lab}}</th>
        <th>Nama Reagen</th>
        <th>Hasil Pemeriksaan</th>
        <th>Hasil Rujukan</th>
    </tr>
    <?php $no = 0;?>
    @foreach($perusahaan->data2 as $hasil)
    <?php $no++; ?>
    <tr>
        <td>Tabung&nbsp;{{$no}}</td>
        <td>{{$hasil->reagen}} &nbsp; {{$hasil->reagen_lain}}</td>
        <td><center>{{$hasil->interpretasi}}</center></td>
        <td>{{$hasil->nilai_rujukan}}</td>
    </tr>
    @endforeach
@endforeach
</table>
@endif
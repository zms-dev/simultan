<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
}

#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;   
    text-align: center;
}

table, tr, td {
    text-align: center;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 2px;
}
</style>
@if(count($data))
<table id="peserta_pme">
    <tr class="text-tengah">
        <th>Kode Peserta</th>
        <th>Nama Instansi</th>
    </tr>
    <?php $no = 0;?>
    @foreach($data as $val)
    <?php $no++; ?>
    <tr>
        <td>{{$val->kode_lebpes}}</td>
        <td>{{$val->nama_lab}}</td>
    </tr>
    @endforeach
</table>
@endif
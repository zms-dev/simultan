<html>
	<head>
		<style type="text/css">
		html{
			width: 1200px;
			height: 830px;
			/*padding-top: 30px;*/
			/*padding-bottom: 30px;*/
		}
		body{
			padding: 0;
			margin: 0;
			padding-bottom: 25px;
		}
		.container{
		background: url("{{url('asset/img/Urine.jpg')}}");
			position:relative;
			background-repeat: no-repeat;;
			background-size: cover;
			background-position: center;
			margin: 0;
			height: 100%;
			width: 100%;
		}
		.header{
		position: fixed;
		}
		.kode{
		 	font-family: Times New Roman;
		 	font-size: 19px;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 260px;
		 }


		 .peserta{
		 	font-family: Arial;
		 	width: 70%;
		 	font-size: 25px;
		 	font-weight : 900;
		 	position: absolute;
		 	left: 180;
		 	right: 0;
			text-align: center;
			margin-top: 345px;
		 	font-style: italic;
		 }

		 .siklus{
		 	font-family: Times New Roman;
		 	font-size: 20px;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 400px;
		 }
		 .bawah{
		 	font-family: Times new roman;
		 	font-size: 18px;
		 	font-weight: bold;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 675px;
		 }
		 .tanggal{
		 	font-family: Times new roman;
		 	font-size: 18px;
		 	font-weight: bold;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 580px;
		 }

		 .jenis{
		 	font-family: Times New Roman;
		 	font-size: 19px;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 475px;
		 }
		 .jenis div{
			 width:75%;
			 display:inline-block;
			 margin:auto;
		 }
		 .serti{
			 text-decoration: underline;
 		 	font-weight: 1000;
			letter-spacing: 5;
 		 	position: absolute;
 		 	left: 0;
 		 	right: 0;
 			text-align: center;
 			margin-top: 205px;
		 }

		 .diberikan{
			font-size: 20px;
			font-weight: bold;
 		 	position: absolute;
 		 	left: 0;
 		 	right: 0;
 			text-align: center;
 			margin-top: 317px;
		 }
		</style>
<!-- <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'serti.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'serti.png')}}"  style="margin-right: 9px; position: absolute; width: 110%; height: 102%;  top: -9%; right: -6%;"> -->
	</head>
	<body>
		<div class="container">
			@foreach($data as $data2)
		<?php
			$kode = $data2->kode_lebpes;
			$kode1 = substr($kode,5,-6);
		?>
		<div class="serti">
			<h1>
			SERTIFIKAT
			</h1>
		</div>
		<div style="" class="kode"><b>Nomor : YM.01.03/XLI.3/{{ $data2->nomor_sertifikat }}/{{$tahun}}</b></div>
		<div class="diberikan">
			DIBERIKAN KEPADA:
		</div>
		<div style="" class="peserta"><b>{{ strtoupper($data2->nama_lab)}}</b></div>
		<div style="" class="siklus"><b>
			SEBAGAI TANDA KEIKUTSERTAAN <br>
			PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL <br>
			BIDANG URINALISA SIKLUS {{$type}} TAHUN {{$tahun}}</b></div>
		<div style="" class="jenis">
			@if(count($param))
			Parameter :
			<br>
		 	<div>
			<b>
				@php
					ob_start();
					foreach ($param as $key => $value) {
						echo $value->nama_parameter . ', ';
					}
					$output = ob_get_clean();
					echo rtrim($output, ', ');
				@endphp
			</b>
		 	</div>
		 	@endif
		</div>
		<div style="" class="tanggal">Surabaya, @if($tanggal>0){{ _dateIndo($tanggal)}} @endif <br> K e p a l a, </div>
		@endforeach
		<div class="bawah">dr. H. Abidin, MPH<br>
		NIP. 196104051988031003
		</div>
		</div>
	</body>
</html>

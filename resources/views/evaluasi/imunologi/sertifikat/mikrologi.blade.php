<html>
	<head>
		<style type="text/css">
		html{
			width: 1200px;
			height: 830px;
			/*padding-top: 30px;*/
			/*padding-bottom: 30px;*/
		}
		body{
			padding: 0;
			margin: 0;
			padding-bottom: 25px;
		}
		.container{
			background: url("{{url('asset/img/mikro.png')}}");
			position:relative;
			background-repeat: no-repeat;;
			background-size: cover;
			background-position: center;
			margin: 0;
			height: 100%;
			width: 100%;
		}
		.header{
		position: fixed;
		}
		.kode{
		 	font-family: Arial narrow;
		 	font-size: 19px;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 283px;
		 }


		 .peserta{
		 	font-family: Arial;
		 	width: 70%;
		 	font-size: 24px;
		 	font-weight : 900;
		 	position: absolute;
		 	left: 180;
		 	right: 0;
			text-align: center;
			margin-top: 345px;
		 	font-style: italic;
		 }

		 .siklus{
		 	font-family: Arial narrow;
		 	font-size: 20px;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 456px;
		 }
		 .bawah{
		 	font-family: Arial;
		 	font-size: 17px;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 680px;
		 }
		 .tanggal{
		 	font-family: Arial;
		 	font-size: 17px;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 585px;
		 }

		 .jenis{
		 	font-family: Arial;
		 	font-size: 17px;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 485px;
		 }
		 .jenis div{
			 width:68%;
			 display:inline-block;
			 margin:auto;
		 }
		</style>
<!-- <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'serti.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'serti.png')}}"  style="margin-right: 9px; position: absolute; width: 110%; height: 102%;  top: -9%; right: -6%;"> -->
	</head>
	<body>
		<div class="container">
			@foreach($data as $data2)
		<?php
			$kode = $data2->kode_lebpes;
			$kode1 = substr($kode,5,-6);
		?>
		<div style="" class="kode">Nomor : YM.01.03/XLI.3/{{ $data2->nomor_sertifikat }}/{{$tahun}}</div>
		<div style="" class="peserta"><b>{{ strtoupper($data2->nama_lab)}}</b></div>
		<div style="" class="siklus"><b>BIDANG MIKROBIOLOGI SIKLUS {{$type}} TAHUN {{$tahun}}</b></div>
		<div style="" class="jenis">
			Parameter : <br>
		 	<div>
			<b>{{ $data2->jenis_form}}&nbsp;</b>
		 	</div>
		</div>
		<div style="" class="tanggal">Surabaya, @if($tanggal>0){{ _dateIndo($tanggal)}} @endif
			<br>K e p a l a,</div>
		@endforeach
		<div class="bawah">dr. H. Abidin, MPH<br>
		NIP. 196104051988031003
		</div>
		</div>
	</body>
</html>

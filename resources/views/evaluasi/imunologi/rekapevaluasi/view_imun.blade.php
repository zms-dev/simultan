<table border="1">
    <thead>
        <tr>
            <th>No</th>
            <th>Instansi</th>
            <th>Kode Peserta</th>
            <th>Form</th>
            @if($type == 1)
                <th>Nama Produsen</th>
                <th>Reagen</th>
                <th>Metode</th>
                <th>Lot</th>
                <th>Exp. Reagen</th>
            @else
                <th>Nama Produsen 1</th>
                <th>Reagen 1</th>
                <th>Metode 1</th>
                <th>Lot 1</th>
                <th>Exp. Reagen 1</th>
                <th>Nama Produsen 2</th>
                <th>Reagen 2</th>
                <th>Metode 2</th>
                <th>Lot 2</th>
                <th>Exp. Reagen 2</th>
                <th>Nama Produsen 3</th>
                <th>Reagen 3</th>
                <th>Metode 3</th>
                <th>Lot 3</th>
                <th>Exp. Reagen 3</th>
            @endif
            <!-- <?php $no = 0; ?>
            @foreach($data[0]->strategi as $st)
                <?php $no++; ?>
                <th>Hasil {{$st->kode_bahan_kontrol}}</th>
                @if($type == 1)
                <th>Kesesuaian Hasil {{$st->kode_bahan_kontrol}}</th>
                @endif
            @endforeach -->
            @if($type == 1)
                <?php for ($i=1; $i <= 5; $i++) { ?>
                <th>Kode Bahan Uji {{$i}}</th>
                <th>Hasil Bahan Uji {{$i}}</th>
                <th>Rujukan Bahan Uji {{$i}}</th>
                <th>Kesesuaian Bahan Uji {{$i}}</th>
                <?php } ?>
            @else
                <?php for ($i=1; $i <= 5; $i++) { ?>
                <th>Kode Bahan Uji {{$i}}</th>
                <th>Hasil Reagen 1</th>
                <th>Hasil Reagen 2</th>
                <th>Hasil Reagen 3</th>
                <th>Rujukan Bahan Uji {{$i}}</th>
                <th>Kesesuaian Bahan Uji {{$i}}</th>
                <?php } ?>
            @endif
            <th>Penilaian</th>
            <th>Saran</th>
            <th>Ketetapan Hasil</th>
            <th>Kesesuaian Strategi</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0; ?>
        @foreach($data as $val)
        <?php $no++; ?>
        <tr>
            <td>{{$no}}</td>
            <td>{{$val->nama_lab}}</td>
            <td>{{$val->kode_lebpes}}</td>
            <td>{{$val->jenis_form}}</td>
            @if($type == 1)
                @foreach($val->reagen as $reg)
                <td>{{$reg->nama_produsen }}</td>
                <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
                <td>{{$reg->metode}}</td>
                <td>{{$reg->nomor_lot}}</td>
                <td>{{$reg->tgl_kadaluarsa}}</td>
                @endforeach
                @foreach($val->strategi as $st)
                    <td>{{$st->kode_bahan_kontrol}}</td>
                    <td>{{$st->interpretasi}}</td>
                    <td>{{$st->nilai_rujukan}}</td>
                    <td>
                        <?php 
                            if ($st->interpretasi == $st->nilai_rujukan) {
                                echo "Benar";
                            }elseif ($st->interpretasi == 'Tanpa test') {
                                echo "Tidak dapat dinilai";
                            }else{
                                echo "Salah";
                            }
                        ?>
                    </td>
                @endforeach
            @else
                @if(count($val->reagen) == 3)
                    @foreach($val->reagen as $reg)
                    <td>{{$reg->nama_produsen }}</td>
                    <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
                    <td>{{$reg->metode}}</td>
                    <td>{{$reg->nomor_lot}}</td>
                    <td>{{$reg->tgl_kadaluarsa}}</td>
                    @endforeach
                @elseif(count($val->reagen) == 2)
                    @foreach($val->reagen as $reg)
                    <td>{{$reg->nama_produsen }}</td>
                    <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
                    <td>{{$reg->metode}}</td>
                    <td>{{$reg->nomor_lot}}</td>
                    <td>{{$reg->tgl_kadaluarsa}}</td>
                    @endforeach
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @elseif(count($val->reagen) == 1)
                    @foreach($val->reagen as $reg)
                    <td>{{$reg->nama_produsen }}</td>
                    <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
                    <td>{{$reg->metode}}</td>
                    <td>{{$reg->nomor_lot}}</td>
                    <td>{{$reg->tgl_kadaluarsa}}</td>
                    @endforeach
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @endif
                <?php $non = 0; $nilai = 0; $salah = 0; ?>
                @foreach($val->strategi as $st)
                    <?php $non++; ?>
                    <?php if ($non == 1) {?>
                        <td>{{$st->kode_bahan_kontrol}}</td>
                    <?php }?>
                    <td>{{$st->interpretasi}}</td>
                    <?php 
                        if($st->interpretasi == $st->nilai_rujukan){ 
                        }elseif($st->interpretasi == 'Tanpa test'){
                            $nilai++;
                        }elseif($st->interpretasi != $st->nilai_rujukan){
                            $salah++;
                        } 
                    ?>
                    <?php if ($non == 3) { $non = 0?>
                    <td>{{$st->nilai_rujukan}}</td>
                    <td>
                        <?php 
                            if ($salah >= 1) {
                                echo "Salah";
                            }elseif ($nilai == 3) {
                                echo "Tidak dapat dinilai";
                            }else{
                                echo "Benar";
                            }
                        ?>
                    </td>
                    <?php $nilai = 0; $salah = 0;} ?>
                @endforeach
            @endif
            <td>
                @if(count($val->saran))
                    @if($val->saran->nilai_1 != NULL)
                        Tahap pemeriksaan yang Saudara kerjakan sudah 
                            @if($val->saran->nilai_1 == '1') Sesuai @else Tidak Sesuai @endif
                        dengan strategi III pemeriksaan Anti HIV. |
                    @endif
                    @if($val->saran->nilai_2 != NULL)
                        Tahap pemeriksaan yang Saudara kerjakan
                            @if($val->saran->nilai_2 == '1') Sesuai @else Tidak Sesuai @endif
                        dengan strategi III pemeriksaan Anti HIV. Gunakan alur pemeriksaan yang sesuai dengan strategi III untuk menegakkan diagnosis HIV. |
                    @endif
                    @if($val->saran->nilai_3 != NULL)
                        Sensitifitas dan spesifisitas reagen yang Saudara gunakan
                            @if($val->saran->nilai_3 == '1') Sesuai @else Tidak Sesuai @endif
                        dengan ketentuan Permenkes 15 tahun 2015. |
                    @endif
                    @if($val->saran->nilai_4 != NULL)
                        Sensitifitas dan spesifisitas reagen yang Saudara gunakan 
                            @if($val->saran->nilai_4 == '1') Sesuai @else Tidak Sesuai @endif
                        dengan ketentuan Permenkes 15 tahun 2015. Gunakan urutan reagen berdasarkan ketentuan sensitivitas dan spesifisitas pada permenkes No. 15 tahun 2015. |
                    @endif
                    @if($val->saran->nilai_5 != NULL)
                        Reagen yang Saudara gunakan
                            @if($val->saran->nilai_5 == '1') Sesuai @else Tidak Sesuai @endif
                        dengan hasil evaluasi laboratorium rujukan nasional RSUPN Cipto Mangunkusumo. Gunakan jenis reagen yang sudah dilakukan evaluasi oleh RSUPN Cipto Mangunkusumo. |
                    @endif
                    @if($val->saran->nilai_6 != NULL)
                        Tahap pemeriksaan yang Saudara kerjakan
                            @if($val->saran->nilai_6 == '1') Sesuai @else Tidak Sesuai @endif
                        dengan strategi I pemeriksaan Anti HIV. |
                    @endif
                @endif
            </td>
            <td>
                @if(count($val->saran)){{$val->saran->lain}}@endif</td>
            <td>{!!html_entity_decode($val->ketepatan)!!}</td>
            <td>{!!html_entity_decode($val->kesesuaian)!!}</td>
        </tr>
        @endforeach
    </tbody>
</table>
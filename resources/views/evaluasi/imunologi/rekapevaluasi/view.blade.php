<table border="1">
    <thead>
        <tr>
            <th>No</th>
            <th>Instansi</th>
            <th>Kode Peserta</th>
            <th>Nama Produsen</th>
            <th>Reagen</th>
            <th>Metode</th>
            <th>Lot</th>
            <th>Exp. Reagen</th>
            <?php
                for ($i=1; $i <= 5; $i++) {
            ?>
                <th>Kode Bahan Uji {{$i}}</th>
                <th>Hasil Bahan Uji {{$i}}</th>
                <th>Rujukan Bahan Uji {{$i}}</th>
                @if($type == 1)
                <th>Titer Bahan Uji {{$i}}</th>
                <th>Rentan Titer Bahan Uji {{$i}}</th>
                @endif
                <th>Kesesuaian Bahan Uji {{$i}}</th>
            <?php } ?>
            <th>Ketetapan Hasil</th>
            <th>Saran</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0; ?>
        @foreach($data as $val)
        <?php $no++; ?>
        <tr>
            <td>{{$no}}</td>
            <td>{{$val->nama_lab}}</td>
            <td>{{$val->kode_lebpes}}</td>
            @if(count($val->reagen))
              @foreach($val->reagen as $reagen)
              <td>{{$reagen->nama_produsen }}</td>
              @endforeach
            @else
              <td></td>
            @endif
            @if(count($val->reagen))
                @foreach($val->reagen as $reg)
                <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
                <td>{{$reg->metode}}</td>
                <td>{{$reg->nomor_lot}}</td>
                <td>{{$reg->tgl_kadaluarsa}}</td>
                @endforeach
            @else
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            @endif
            @if(count($val->strategi))
            @foreach($val->strategi as $st)
                <td>{{$st->kode_bahan_kontrol}}</td>
                <td>{{$st->interpretasi}}</td>
                <td>{{$st->nilai_rujukan}}</td>
                @if($type == 1)
                    <th>{{$st->titer}}</th>
                    <th>{{$st->range1}} - {{$st->range2}}</th>
                @endif
                <td>
                @if($type == 1)
                    @if($st->interpretasi == $st->nilai_rujukan)
                        <?php
                            $kesesuaian_hasil = 'Benar';
                        ?>
                    @elseif($st->interpretasi == 'tanpa-test')
                    @else
                        <?php
                            $kesesuaian_hasil = 'Salah';
                        ?>
                    @endif
                    @if(count($st->titer))
                        @if(explode(":", $st->titer) >= explode(":", $st->range1) && explode(":", $st->titer) <= explode(":", $st->range2))
                            <?php $titer = 'Benar';?>
                        @else
                            <?php $titer = 'Salah';?>
                        @endif
                    @else
                        <?php
                            $titer = '';
                        ?>
                    @endif
                    <?php
                        if($st->interpretasi == 'Non Reaktif'){
                            if ($kesesuaian_hasil == 'Benar') {
                                echo "Benar";
                            }else{
                                echo "Salah";
                            }
                        }else{
                            if ($titer == '') {
                                echo "-";
                            }else if (($kesesuaian_hasil == 'Benar') && ($titer == 'Benar')) {
                                echo "Benar";
                            }else if (($kesesuaian_hasil == 'Salah') && ($titer == 'Salah')) {
                                echo "Salah";
                            }else{
                                echo "Salah";
                            }
                        }
                    ?>
                @else

                    @if($st->interpretasi == 'Tanpa test')
                    Tidak Dapat dinilai
                    @else
                        @if($st->nilai_rujukan == 'Reaktif / Non Reaktif')
                            Benar
                        @elseif($st->interpretasi == $st->nilai_rujukan)
                            Benar
                        @else
                            Salah
                        @endif
                    @endif
                @endif
                </td>
            @endforeach
            @else
            <?php
                for ($i=1; $i <= 5; $i++) {
            ?>
                <th></th>
                @if($type == 1)
                <th></th>
                <th></th>
                @endif
                <th></th>
            <?php } ?>
            @endif
            <td>
            @foreach($val->kesimpulan as $kesimpulan)
                {{$kesimpulan->ketepatan}}
            @endforeach
            </td>
            @if(count($val->catatan)>0)
            <td>{{strip_tags($val->catatan->catatan)}}</td>
            @else
            <td>...</td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>

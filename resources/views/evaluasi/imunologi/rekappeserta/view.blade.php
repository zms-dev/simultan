<style type="text/css">
table {
    border-collapse: collapse;
}

table, th, td {
    border: 1px solid black;
}
</style>
<table>
    <thead>
        <tr>
            <th colspan="20"><h2>Rekap Input Peserta</h2></th>
        </tr>
        <tr>
            <th>No</th>
            <th>Instansi</th>
            <th>Kode Peserta</th>
            <th>Nama Produsen</th>
            <th>Reagen</th>
            <th>Metode</th>
            <th>Lot</th>
            <th>Exp. Reagen</th>
            <?php for ($i=1; $i <= 5; $i++) { ?>
            <th>Kode Bahan Uji {{$i}}</th>
            <th>Hasil Bahan Uji {{$i}}</th>
            @if($type == 1)
                <th>Hasil Titer {{$i}}</th>
            @endif
            <?php } ?>
            <th>Alasan Tidak Mengerjakan</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0; ?>
        @foreach($data as $val)
        <?php $no++; ?>
        <tr>
            <td>{{$no}}</td>
            <td>{{$val->nama_lab}}</td>
            <td>{{$val->kode_lebpes}}</td>
            @if(count($val->reagen)>0)
            @foreach($val->reagen as $reg)
            <td>{{$reg->nama_produsen}}</td>
            <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
            <td>{{$reg->metode}}</td>
            <td>{{$reg->nomor_lot}}</td>
            <td>{{$reg->tgl_kadaluarsa}}</td>
            @endforeach
            @else
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            @endif
            @if(count($val->hpimun)>0)
            @foreach($val->hpimun as $st)
                <td>{{$st->kode_bahan_kontrol}}</td>
                <td>{{$st->interpretasi}}</td>
                @if($type == 1)
                    <th>{{$st->titer}}</th>
                @endif
            @endforeach
            @else
            <?php
            for ($i=1; $i <11 ; $i++) { 
                echo "<td $i></td>";
            }
            ?>
            @endif
            @if(count($val->reagen))
            <td>{{$val->reagen[0]->keterangan}}</td>
            @else
            <td></td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
<br>
@if(isset($datarpr))
<table>
    <thead>
        <tr>
            <th colspan="25"><h2>Rekap Input Peserta RPR</h2></th>
        </tr>
        <tr>
            <th>No</th>
            <th>Instansi</th>
            <th>Kode Peserta</th>
            <th>Nama Produsen</th>
            <th>Reagen</th>
            <th>Metode</th>
            <th>Lot</th>
            <th>Exp. Reagen</th>
            <?php for ($i=1; $i <= 5; $i++) { ?>
            <th>Kode Bahan Uji {{$i}}</th>
            <th>Hasil Bahan Uji {{$i}}</th>
            <th>Hasil Titer {{$i}}</th>
            <?php } ?>
            <th>Alasan Tidak Mengerjakan</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0; ?>
        @foreach($data as $val)
        <?php $no++; ?>
        <tr>
            <td>{{$no}}</td>
            <td>{{$val->nama_lab}}</td>
            <td>{{$val->kode_lebpes}}</td>
            @if(count($val->reagen)>0)
            @foreach($val->reagen as $reg)
            <td>{{$reg->nama_produsen}}</td>
            <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
            <td>{{$reg->metode}}</td>
            <td>{{$reg->nomor_lot}}</td>
            <td>{{$reg->tgl_kadaluarsa}}</td>
            @endforeach
            @else
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            @endif
            @if(count($val->hpimun)>0)
            @foreach($val->hpimun as $st)
                <td>{{$st->kode_bahan_kontrol}}</td>
                <td>{{$st->interpretasi}}</td>
                <th>{{$st->titer}}</th>
            @endforeach
            @else
            <?php
            for ($i=1; $i <16 ; $i++) { 
                echo "<td $i></td>";
            }
            ?>
            @endif
            
            @if(count($val->reagen))
            <td>{{$val->reagen[0]->keterangan}}</td>
            @else
            <td></td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
@endif
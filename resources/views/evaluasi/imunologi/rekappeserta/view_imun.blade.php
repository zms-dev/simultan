<style type="text/css">
table {
    border-collapse: collapse;
}

table, th, td {
    border: 1px solid black;
}
</style>
<table>
    <tr>
        <th colspan="40"><h2>Rekap Input Peserta</h2></th>
    </tr>
    <tr>
        <th>No</th>
        <th>Instansi</th>
        <th>Kode Peserta</th>
        @if($type == 1)
            <th>Nama Produsen</th>
            <th>Nama Reagen</th>
            <th>Metode</th>
            <th>Lot</th>
            <th>Exp. Reagen</th>
        <?php 
            for ($i=1; $i <= 5 ; $i++) {
        ?>
            <th>Kode Bahan Uji {{$i}}</th>
            <th>Hasil Bahan Uji {{$i}}</th>
        <?php }?>
        @else
            <th>Nama Produsen 1</th>
            <th>Nama Reagen 1</th>
            <th>Metode 1</th>
            <th>Lot 1</th>
            <th>Exp. Reagen 1</th>
            <th>Nama Produsen 2</th>
            <th>Nama Reagen 2</th>
            <th>Metode 2</th>
            <th>Lot 2</th>
            <th>Exp. Reagen 2</th>
            <th>Nama Produsen 3</th>
            <th>Nama Reagen 3</th>
            <th>Metode 3</th>
            <th>Lot 3</th>
            <th>Exp. Reagen 3</th>
        <?php 
            $no = 0; 
            for ($i=1; $i <= 5 ; $i++) {
            $no++; 
        ?>
            <th>Kode Bahan Uji {{$i}}</th>
            <th>Hasil Reagen 1</th>
            <th>Hasil Reagen 2</th>
            <th>Hasil Reagen 3</th>
        <?php if ($i == 3) { $no = 0;} }?>
        @endif
        <th>Alasan Tidak Mengerjakan</th>
    </tr>
    <?php $no = 0; ?>
    @foreach($data as $val)
    <?php $no++; ?>
    <tr>
        <td>{{$no}}</td>
        <td>{{$val->nama_lab}}</td>
        <td>{{$val->kode_lebpes}}</td>
        @if(count($val->reagen)>0)
        @if($type == 1)
            @foreach($val->reagen as $reg)
            <td>{{$reg->nama_produsen}}</td>
            <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
            <td>{{$reg->metode}}</td>
            <td>{{$reg->nomor_lot}}</td>
            <td>{{$reg->tgl_kadaluarsa}}</td>
            @endforeach
            @foreach($val->hpimun as $st)
                <td>{{$st->kode_bahan_kontrol}}</td>
                <td>{{$st->interpretasi}}</td>
            @endforeach
        @else
            @if(count($val->reagen) == 3)
                @foreach($val->reagen as $reg)
                <td>{{$reg->nama_produsen}}</td>
                <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
                <td>{{$reg->metode}}</td>
                <td>{{$reg->nomor_lot}}</td>
                <td>{{$reg->tgl_kadaluarsa}}</td>
                @endforeach
            @elseif(count($val->reagen) == 2)
                @foreach($val->reagen as $reg)
                <td>{{$reg->nama_produsen}}</td>
                <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
                <td>{{$reg->metode}}</td>
                <td>{{$reg->nomor_lot}}</td>
                <td>{{$reg->tgl_kadaluarsa}}</td>
                @endforeach
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            @elseif(count($val->reagen) == 1)
                @foreach($val->reagen as $reg)
                <td>{{$reg->nama_produsen}}</td>
                <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
                <td>{{$reg->metode}}</td>
                <td>{{$reg->nomor_lot}}</td>
                <td>{{$reg->tgl_kadaluarsa}}</td>
                @endforeach
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            @endif
            
            <?php $non = 0; ?>
            @foreach($val->hpimun as $st)
                <?php $non++; ?>
                @if($non == 1)
                <td>{{$st->kode_bahan_kontrol}}</td>
                @endif
                <td>{{$st->interpretasi}}</td>
                <?php if ($non == 3) { $non = 0;}?>
            @endforeach
        @endif
        @else
        @if(Auth::user()->badan_usaha  == 9)
        <?php 
            for ($i=1; $i <= 16 ; $i++) { 
                echo "<td $i></td>";
            }
        ?>
        @else
        <?php 
            for ($i=1; $i <= 36; $i++) { 
                echo "<td $i></td>";
            }
        ?>
        @endif
        @endif
        @if(count($val->reagen))
        <td>{{$val->reagen[0]->keterangan}}</td>
        @endif
    </tr>
    @endforeach
</table>
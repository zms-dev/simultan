<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
}

#peserta_pme td,#peserta_pme th{
    border: 1px solid #ddd;
    text-align: center;
}

.reagen {
    width: 75%;
    margin-left: 20px;
}
.reagen:before{
    position: absolute;
    margin-left: -15px;
    color: #000;
    content: counter(paragraph) ". ";
    counter-increment: paragraph;
}
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 2px;
}

.header{
position: fixed;
}

.header {
top: 0;
}
</style>
<table width="100%" cellpadding="0" border="0">
    <thead>
        <tr>
            <th>
                <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px">
            </th>
            <th width="100%" style="text-align: center;">
                <span style="font-size: 16px; margin-left:-49px;">KEMENTERIAN KESEHATAN RI</span><br>
                <span style="font-size: 12px; margin-left:-44px;">PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL ANTI HIV SIKLUS {{$type}} TAHUN {{$tahun}}</span>
                <pre style="font-family: Helvetica;font-size: 12px; padding: -7 -25px;margin-left: -105px;">
                 Penyelenggara : Balai Besar Laboratorium Kesehatan Surabaya
                 Jl.Karangmenjangan No. 18 Surabaya 60286
             Telepon : 031-5021451 Fax.031-5020388, 031-5053076
             Email : pme.bblksub@gmail.com
                </pre>
            </th>
        </tr>
        <tr>
            <th colspan="2"><hr></th>
        </tr>
    </thead>
</table>

<center><label><b>LAMPIRAN EVALUASI PESERTA <br>
PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL ANTI HIV SIKLUS {{$type}} TAHUN {{$tahun}}</b>
<input type="hidden" name="type" value="{{$type}}"></label></center><br>
<form class="form-horizontal" method="post" enctype="multipart/form-data">
@foreach($data2 as $hasil)
<b>Nama Instansi :</b> &nbsp;{{$hasil->nama_lab}}<br>
<b>Kode Peserta :</b> &nbsp;{{$hasil->kode_lebpes}}
@break
@endforeach
<br><br>

<table id="peserta_pme">
    <tr class="titlerowna">
        <th>Nama Reagen</th>
        <th>Kode Bahan Uji</th>
        <th>Test</th>
        <th>Hasil Pemeriksaan</th>
        <th>Hasil Rujukan</th>
        <th>Kesesuaian Hasil</th>
        <th>Nilai Peserta</th>
    </tr>
    <?php
        $no = 0;
        $val = ['1','2','3','4'];
        $tabung = '';
    ?>
    @if(count($data2))
    @foreach($data2 as $hasil)
    @if($hasil->tabung == '1')
    <tr>
        @if($hasil->interpretasi == 'tanpa-test')
        @else
            @if(!empty($reagen[$no]))
                @if($no != 0)
                @else
                    <td rowspan="15" style="text-align: left; vertical-align: top;">
                        @foreach($reagen as $reg)
                        @if($reg->reagen != null)
                        <p class="reagen">
                            @if($reg->reagen == 'Lain - lain')
                                {{$reg->reagen_lain}}
                            @else
                                {{$reg->reagen}}
                            @endif
                        </p>
                        @else
                        @endif
                        @endforeach
                    </td>
                @endif
            @else
            @endif
        @endif
        @if($tabung == $hasil->tabung)
        @else
        <td rowspan="3" align="middle">{{$hasil->kode_bahan_kontrol}}</td>
        @endif
        <td>REAGEN {{$val[$no]}}</td>
        <td>@if($hasil->interpretasi == 'Tanpa test')@else {{$hasil->interpretasi}} @endif</td>
        @if($tabung == $hasil->tabung)
        @else
            <td rowspan="3">{{$hasil->nilai_rujukan}}</td>
        @endif
        @if($tabung == $hasil->tabung)
        @else
        <td rowspan="3">
            <?php $salah = 0;$benar = 0;?>
            @foreach($data2 as $keshasil)
            @if($keshasil->tabung == '1')
                <?php
                    if($keshasil->interpretasi == 'Tanpa test'){

                    }else{
                        if($keshasil->interpretasi == $keshasil->nilai_rujukan){
                            $benar++;
                        }elseif($keshasil->interpretasi != $keshasil->nilai_rujukan){
                            $salah++;
                        }
                    }
                ?>
            @endif
            @endforeach
            @if($salah > 0) Salah @elseif($benar > 0) Benar @endif
        </td>
        @endif

        @if($no != 0)
        @else
        <td rowspan="15">{{$kesimpulan->ketepatan}}</td>
        @endif
    <?php $no++; $tabung = $hasil->tabung ?>
    </tr>
    @endif
    @endforeach
    <!-- <tr>
        <td colspan="5"></td>
        <td>Evaluasi</td>
        <td style="font-weight: bold;" class="evaluasi1"></td>
    </tr> -->
    <?php
        $no = 0;
        $val = ['1','2','3','4'];
        $tabung = '';
    ?>
    @foreach($data2 as $hasil)
    @if($hasil->tabung == '2')
    <tr>
        @if($tabung == $hasil->tabung)
        @else
        <td rowspan="3" align="middle">{{$hasil->kode_bahan_kontrol}}</td>
        @endif
        <td>REAGEN {{$val[$no]}}</td>
        <td>@if($hasil->interpretasi == 'Tanpa test')@else {{$hasil->interpretasi}} @endif</td>
        @if($tabung == $hasil->tabung)
        @else
            <td rowspan="3">{{$hasil->nilai_rujukan}}</td>
        @endif
        @if($tabung == $hasil->tabung)
        @else
        <td rowspan="3">
            <?php $salah = 0;$benar = 0;?>
            @foreach($data2 as $keshasil)
            @if($keshasil->tabung == '2')
                <?php
                    if($keshasil->interpretasi == 'Tanpa test'){

                    }else{
                        if($keshasil->interpretasi == $keshasil->nilai_rujukan){
                            $benar++;
                        }elseif($keshasil->interpretasi != $keshasil->nilai_rujukan){
                            $salah++;
                        }
                    }
                ?>
            @endif
            @endforeach
            @if($salah > 0) Salah @elseif($benar > 0) Benar @endif
        </td>
        @endif
    </tr>
    <?php $no++; $tabung = $hasil->tabung?>
    @endif
    @endforeach
    <!-- <tr>
        <td colspan="5"></td>
        <td>Evaluasi</td>
        <td style="font-weight: bold;" class="evaluasi2"></td>
    </tr> -->
    <?php
        $no = 0;
        $val = ['1','2','3','4'];
        $tabung = '';
    ?>
    @foreach($data2 as $hasil)
    @if($hasil->tabung == '3')
    <tr>
        @if($tabung == $hasil->tabung)
        @else
        <td rowspan="3" align="middle">{{$hasil->kode_bahan_kontrol}}</td>
        @endif
        <td>REAGEN {{$val[$no]}}</td>
        <td>@if($hasil->interpretasi == 'Tanpa test')@else {{$hasil->interpretasi}} @endif</td>
        @if($tabung == $hasil->tabung)
        @else
            <td rowspan="3">{{$hasil->nilai_rujukan}}</td>
        @endif
        @if($tabung == $hasil->tabung)
        @else
        <td rowspan="3">
            <?php $salah = 0;$benar = 0;?>
            @foreach($data2 as $keshasil)
            @if($keshasil->tabung == '3')
                <?php
                    if($keshasil->interpretasi == 'Tanpa test'){

                    }else{
                        if($keshasil->interpretasi == $keshasil->nilai_rujukan){
                            $benar++;
                        }elseif($keshasil->interpretasi != $keshasil->nilai_rujukan){
                            $salah++;
                        }
                    }
                ?>
            @endif
            @endforeach
            @if($salah > 0) Salah @elseif($benar > 0) Benar @endif
        </td>
        @endif
    </tr>
    <?php $no++; $tabung = $hasil->tabung?>
    @endif
    @endforeach

    <?php
        $no = 0;
        $val = ['1','2','3','4'];
        $tabung = '';
    ?>
    @foreach($data2 as $hasil)
    @if($hasil->tabung == '4')
    <tr>
        @if($tabung == $hasil->tabung)
        @else
        <td rowspan="3" align="middle">{{$hasil->kode_bahan_kontrol}}</td>
        @endif
        <td>REAGEN {{$val[$no]}}</td>
        <td>@if($hasil->interpretasi == 'Tanpa test')@else {{$hasil->interpretasi}} @endif</td>
        @if($tabung == $hasil->tabung)
        @else
            <td rowspan="3">{{$hasil->nilai_rujukan}}</td>
        @endif
        @if($tabung == $hasil->tabung)
        @else
        <td rowspan="3">
            <?php $salah = 0;$benar = 0;?>
            @foreach($data2 as $keshasil)
            @if($keshasil->tabung == '4')
                <?php
                    if($keshasil->interpretasi == 'Tanpa test'){

                    }else{
                        if($keshasil->interpretasi == $keshasil->nilai_rujukan){
                            $benar++;
                        }elseif($keshasil->interpretasi != $keshasil->nilai_rujukan){
                            $salah++;
                        }
                    }
                ?>
            @endif
            @endforeach
            @if($salah > 0) Salah @elseif($benar > 0) Benar @endif
        </td>
        @endif
    </tr>
    <?php $no++; $tabung = $hasil->tabung?>
    @endif
    @endforeach

    <?php
        $no = 0;
        $val = ['1','2','3','4'];
        $tabung = '';
    ?>
    @foreach($data2 as $hasil)
    @if($hasil->tabung == '5')
    <tr>
        @if($tabung == $hasil->tabung)
        @else
        <td rowspan="3" align="middle">{{$hasil->kode_bahan_kontrol}}</td>
        @endif
        <td>REAGEN {{$val[$no]}}</td>
        <td>@if($hasil->interpretasi == 'Tanpa test')@else {{$hasil->interpretasi}} @endif</td>
        @if($tabung == $hasil->tabung)
        @else
            <td rowspan="3">{{$hasil->nilai_rujukan}}</td>
        @endif
        @if($tabung == $hasil->tabung)
        @else
        <td rowspan="3">
            <?php $salah = 0;$benar = 0;?>
            @foreach($data2 as $keshasil)
            @if($keshasil->tabung == '5')
                <?php
                    if($keshasil->interpretasi == 'Tanpa test'){

                    }else{
                        if($keshasil->interpretasi == $keshasil->nilai_rujukan){
                            $benar++;
                        }elseif($keshasil->interpretasi != $keshasil->nilai_rujukan){
                            $salah++;
                        }
                    }
                ?>
            @endif
            @endforeach
            @if($salah > 0) Salah @elseif($benar > 0) Benar @endif
        </td>
        @endif
    </tr>
    <?php $no++; $tabung = $hasil->tabung?>
    @endif
    @endforeach
    @else
    <?php for ($i=0; $i < 5; $i++) { ?>
    <tr>
        <td>-</td>
        <td>-</td>
        <td>TIDAK DIKERJAKAN</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        @if($i != 0)
        @else
        <td rowspan="5">{{$kesimpulan->ketepatan}}</td>
        @endif
    </tr>
    <?php } ?>
    <input type="hidden" name="id_master_imunologi" value="">
    @endif
</table>
<h4>Komentar dan Saran :</h4>
<ul>
@if(count($evaluasi))
    @if($evaluasi->nilai_1 != NULL)
        <li>
        Tahap pemeriksaan yang Saudara kerjakan sudah <b>
        @if($evaluasi->nilai_1 == 1)
            Sesuai
        @else
            Tidak Sesuai
        @endif
        </b>dengan strategi III pemeriksaan Anti HIV<br>
        </li>
    @endif
@else
@endif
@if(count($evaluasi))
    @if($evaluasi->nilai_2 != NULL)
    <li>
        Tahap pemeriksaan yang Saudara kerjakan <b>
        @if($evaluasi->nilai_2 == 1)
            Sesuai
        @else
            Tidak Sesuai
        @endif
        </b>dengan strategi III pemeriksaan Anti HIV. Gunakan alur pemeriksaan yang sesuai dengan strategi III untuk menegakkan diagnosis HIV.<br>
    </li>
    @endif
@else
@endif
@if(count($evaluasi))
    @if($evaluasi->nilai_3 != NULL)
    <li>
        Sensitifitas dan spesifisitas reagen yang Saudara gunakan<b>
        @if($evaluasi->nilai_3 == 1)
            Sesuai
        @else
            Tidak Sesuai
        @endif
        </b>dengan ketentuan Permenkes 15 tahun 2015. <br>
    </li>
    @endif
@else
@endif
@if(count($evaluasi))
    @if($evaluasi->nilai_4 != NULL)
    <li>
        Sensitifitas dan spesifisitas reagen yang Saudara gunakan <b>
        @if($evaluasi->nilai_4 == 1)
            Sesuai
        @else
            Tidak Sesuai
        @endif
        </b>dengan ketentuan Permenkes 15 tahun 2015. Gunakan urutan reagen berdasarkan ketentuan sensitivitas dan spesifisitas pada permenkes No. 15 tahun 2015.<br>
    </li>
    @endif
@else
@endif
@if(count($evaluasi))
    @if($evaluasi->nilai_5 != NULL)
    <li>
    Reagen yang Saudara gunakan <b>
        @if($evaluasi->nilai_5 == 1)
            Sesuai
        @else
            Tidak Sesuai
        @endif
    </b>dengan hasil evaluasi laboratorium rujukan nasional RSUPN Cipto Mangunkusumo. Gunakan jenis reagen yang sudah dilakukan evaluasi oleh RSUPN Cipto Mangunkusumo. <br>
    </li>
    @endif
@else
@endif
@if(count($evaluasi))
    @if($evaluasi->nilai_6 != NULL)
    <li>
    Tahap pemeriksaan yang Saudara kerjakan <b>
        @if($evaluasi->nilai_6 == 1)
            Sesuai
        @else
            Tidak Sesuai
        @endif
    </b>dengan strategi I pemeriksaan Anti HIV.<br>
    </li>
    @endif
@else
@endif
@if(count($evaluasi))
    @if($evaluasi->lain != NULL)
        {{$evaluasi->lain}}<br>
    @endif
@else
@endif
</ul>
<br>
<p>
<div style="position: relative; top: 22px">
Surabaya, {{$ttd->tanggal}} {{$tahun}}<br>
Manajer Teknis
</div>
<p>
    <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.'ttdevaluasi'.DIRECTORY_SEPARATOR.$ttd->image)}}" width="80" height="80" style="margin-left: 20px !important;">
</p>

<div style="position: relative; top:-20px">
    {{$ttd->nama}}<br>NIP. {{$ttd->nip}}
</div>
</p>

<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>

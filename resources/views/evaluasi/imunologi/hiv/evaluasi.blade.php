
@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluasi</div>
                <div class="panel-body">
                    <center><label>EVALUASI PESERTA PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI TAHUN {{$date}} <br> HASIL PEMERIKSAAN ANTI HIV <input type="hidden" name="type" value="{{$type}}"></label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Kode Peserta </label>
                        <div class="col-sm-10">
                            <input readonly="" type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{ $kodeperusahaan }}" placeholder="Kode Peserta">
                        </div>
                    </div><br>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Nama Instansi </label>
                        <div class="col-sm-10">
                            <input readonly="" type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{ $perusahaan }}" placeholder="Kode Peserta">
                        </div>
                    </div>


                    <label>2. HASIL PEMERIKSAAN</label>
                    @if(count($evaluasi))
                        <form id="someForm" action="" method="post">
                    @else
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                    @endif
                    <table class="table table-bordered" id="peserta_pme">
                        <tr class="titlerowna">
                            <th><center>Test</center></th>
                            <th><center>Kode Bahan Uji</center></th>
                            <th><center>Metode Pemeriksaan</center></th>
                            <th><center>Nama Reagen</center></th>
                            <th><center>Hasil Pemeriksaan</center></th>
                            <th><center>Hasil Rujukan</center></th>
                            <th><center>Kesesuaian Hasil</center></th>
                            <!-- <td>Kesesuaian Strategi</td> -->
                        </tr>
                        <?php
                            $no = 0;
                            $val = ['1','2','3','4'];
                            $tabung = '';
                            $hasilp = 0;
                            $tanpatest = 0;
                        ?>
                        @if(count($data2))
                        @if(count($strategi))
                            @foreach($strategi as $str)
                                <input type="hidden" name="idevaluasi[]" value="{{$str->id}}">
                            @endforeach
                        @endif
                        @foreach($data2 as $hasil)
                        @if($hasil->tabung == '1')
                        <tr>
                            <td><center>{{$val[$no]}}</center></td>
                            <td><input readonly="" type="text" name="kode_bahan_kontrol[]" class="form-control" required value="{{$hasil->kode_bahan_kontrol}}"></td>
                            @if($hasil->interpretasi == 'Tanpa test')
                            <td></td>
                            <td></td>
                            @else
                                @if(!empty($reagen[$no]))
                                <td style="text-transform: uppercase;"><center>{{$reagen[$no]->metode}}</center></td>
                                <td>
                                    {{-- {!! str_replace('Lain - lain', '', $reagen[$no]->reagen_lain.$reagen[$no]->reagen)!!} --}}
                                    @if($reagen[$no]->id == '121')
                                      {!! str_replace('Lain - lain', '', $reagen[$no]->reagen_lain)!!}
                                    @else
                                      {{$reagen[$no]->reagen}}
                                    @endif
                                </td>
                                @else
                                <td></td>
                                <td></td>
                                @endif
                            @endif
                            <td><center>{{$hasil->interpretasi}}</center></td>
                            @if($tabung == $hasil->tabung)
                            @else
                                <td rowspan="3" style="text-align: center;">{{$hasil->nilai_rujukan}}</td>
                            @endif
                            <td style="text-align: center">
                                @if($hasil->interpretasi == $hasil->nilai_rujukan)
                                <?php if($val[$no] == 1){$hasilp++;} ?>
                                Benar
                                @elseif($hasil->interpretasi == 'Tanpa test')
                                <?php if($val[$no] == 1){$tanpatest++;} ?>
                                @else
                                Salah
                                @endif
                            </td>
                        <?php $no++; $tabung = $hasil->tabung ?>
                        </tr>
                        @endif
                        @endforeach
                        <!-- <tr>
                            <td colspan="5"></td>
                            <td>Evaluasi</td>
                            <td style="font-weight: bold;" class="evaluasi1"></td>
                        </tr> -->
                        <?php
                            $no = 0;
                            $val = ['1','2','3','4'];
                            $tabung = '';
                        ?>
                        @foreach($data2 as $hasil)
                        @if($hasil->tabung == '2')
                        <tr>
                            <td><center>{{$val[$no]}}</center></td>
                            <td><input readonly="" type="text" name="kode_bahan_kontrol[]" class="form-control" required value="{{$hasil->kode_bahan_kontrol}}"></td>
                            @if($hasil->interpretasi == 'Tanpa test')
                            <td></td>
                            <td></td>
                                @else
                                @if(!empty($reagen[$no]))
                                <td style="text-transform: uppercase;"><center>{{$reagen[$no]->metode}}</center></td>
                                <td>
                                    {{-- {!! str_replace('Lain - lain', '', $reagen[$no]->reagen_lain.$reagen[$no]->reagen)!!} --}}
                                    @if($reagen[$no]->id == '121')
                                      {!! str_replace('Lain - lain', '', $reagen[$no]->reagen_lain)!!}
                                    @else
                                      {{$reagen[$no]->reagen}}
                                    @endif
                                </td>
                                @else
                                <td></td>
                                <td></td>
                                @endif
                            @endif
                            <td><center>{{$hasil->interpretasi}}</center></td>
                            @if($tabung == $hasil->tabung)
                            @else
                                <td rowspan="3" style="text-align: center;">{{$hasil->nilai_rujukan}}</td>
                            @endif
                            <td style="text-align: center">
                                @if($hasil->interpretasi == $hasil->nilai_rujukan)
                                <?php if($val[$no] == 1){$hasilp++;} ?>
                                Benar
                                @elseif($hasil->interpretasi == 'Tanpa test')
                                <?php if($val[$no] == 1){$tanpatest++;} ?>
                                @else
                                Salah
                                @endif
                            </td>
                        </tr>
                        <?php $no++; $tabung = $hasil->tabung?>
                        @endif
                        @endforeach
                        <!-- <tr>
                            <td colspan="5"></td>
                            <td>Evaluasi</td>
                            <td style="font-weight: bold;" class="evaluasi2"></td>
                        </tr> -->
                        <?php
                            $no = 0;
                            $val = ['1','2','3','4'];
                            $tabung = '';
                        ?>
                        @foreach($data2 as $hasil)
                        @if($hasil->tabung == '3')
                        <tr>
                            <td><center>{{$val[$no]}}</center></td>
                            <td><input readonly="" type="text" name="kode_bahan_kontrol[]" class="form-control" required value="{{$hasil->kode_bahan_kontrol}}"></td>
                            @if($hasil->interpretasi == 'Tanpa test')
                            <td></td>
                            <td></td>
                            @else
                                @if(!empty($reagen[$no]))
                                <td style="text-transform: uppercase;"><center>{{$reagen[$no]->metode}}</center></td>
                                <td>
                                    {{-- {!! str_replace('Lain - lain', '', $reagen[$no]->reagen_lain.$reagen[$no]->reagen)!!} --}}
                                    @if($reagen[$no]->id == '121')
                                      {!! str_replace('Lain - lain', '', $reagen[$no]->reagen_lain)!!}
                                    @else
                                      {{$reagen[$no]->reagen}}
                                    @endif
                                </td>
                                @else
                                <td></td>
                                <td></td>
                                @endif
                            @endif
                            <td><center>{{$hasil->interpretasi}}</center></td>
                            @if($tabung == $hasil->tabung)
                            @else
                                <td rowspan="3" style="text-align: center;">{{$hasil->nilai_rujukan}}</td>
                            @endif
                            <td style="text-align: center">
                                @if($hasil->interpretasi == $hasil->nilai_rujukan)
                                <?php if($val[$no] == 1){$hasilp++;} ?>
                                Benar
                                @elseif($hasil->interpretasi == 'Tanpa test')
                                <?php if($val[$no] == 1){$tanpatest++;} ?>
                                @else
                                Salah
                                @endif
                            </td>
                        </tr>
                        <?php $no++; $tabung = $hasil->tabung?>
                        @endif
                        @endforeach
                        <!-- <tr>
                            <td colspan="5"></td>
                            <td>Evaluasi</td>
                            <td style="font-weight: bold;" class="evaluasi3"></td>
                        </tr> -->
                        <?php
                            $no = 0;
                            $val = ['1','2','3','4'];
                            $tabung = '';
                        ?>
                        @foreach($data2 as $hasil)
                        @if($hasil->tabung == '4')
                        <tr>
                            <td><center>{{$val[$no]}}</center></td>
                            <td><input readonly="" type="text" name="kode_bahan_kontrol[]" class="form-control" required value="{{$hasil->kode_bahan_kontrol}}"></td>
                            @if($hasil->interpretasi == 'Tanpa test')
                            <td></td>
                            <td></td>
                            @else
                                @if(!empty($reagen[$no]))
                                <td style="text-transform: uppercase;"><center>{{$reagen[$no]->metode}}</center></td>
                                <td>
                                   {{-- {!! str_replace('Lain - lain', '', $reagen[$no]->reagen_lain.$reagen[$no]->reagen)!!} --}}
                                   @if($reagen[$no]->id == '121')
                                     {!! str_replace('Lain - lain', '', $reagen[$no]->reagen_lain)!!}
                                   @else
                                     {{$reagen[$no]->reagen}}
                                   @endif
                                </td>
                                @else
                                <td></td>
                                <td></td>
                                @endif
                            @endif
                            <td><center>{{$hasil->interpretasi}}</center></td>
                            @if($tabung == $hasil->tabung)
                            @else
                                <td rowspan="3" style="text-align: center;">{{$hasil->nilai_rujukan}}</td>
                            @endif
                            <td style="text-align: center">
                                @if($hasil->interpretasi == $hasil->nilai_rujukan)
                                <?php if($val[$no] == 1){$hasilp++;} ?>
                                Benar
                                @elseif($hasil->interpretasi == 'Tanpa test')
                                <?php if($val[$no] == 1){$tanpatest++;} ?>
                                @else
                                Salah
                                @endif
                            </td>

                        <?php $no++; $tabung = $hasil->tabung ?>
                        </tr>
                        @endif
                        @endforeach

                        <?php
                            $no = 0;
                            $val = ['1','2','3','4'];
                            $tabung = '';
                        ?>
                        @foreach($data2 as $hasil)
                        @if($hasil->tabung == '5')
                        <tr>
                            <td><center>{{$val[$no]}}</center></td>
                            <td><input readonly="" type="text" name="kode_bahan_kontrol[]" class="form-control" required value="{{$hasil->kode_bahan_kontrol}}"></td>
                            @if($hasil->interpretasi == 'Tanpa test')
                            <td></td>
                            <td></td>
                            @else
                                @if(!empty($reagen[$no]))
                                <td style="text-transform: uppercase;"><center>{{$reagen[$no]->metode}}</center></td>
                                <td>
                                    {{-- {!! str_replace('Lain - lain', '', $reagen[$no]->reagen_lain.$reagen[$no]->reagen)!!} --}}
                                    @if($reagen[$no]->id == '121')
                                      {!! str_replace('Lain - lain', '', $reagen[$no]->reagen_lain)!!}
                                    @else
                                      {{$reagen[$no]->reagen}}
                                    @endif
                                </td>
                                @else
                                <td></td>
                                <td></td>
                                @endif
                            @endif
                            <td><center>{{$hasil->interpretasi}}</center></td>
                            @if($tabung == $hasil->tabung)
                            @else
                                <td rowspan="3" style="text-align: center;">{{$hasil->nilai_rujukan}}</td>
                            @endif
                            <td style="text-align: center">
                                @if($hasil->interpretasi == $hasil->nilai_rujukan)
                                <?php if($val[$no] == 1){$hasilp++;} ?>
                                Benar
                                @elseif($hasil->interpretasi == 'Tanpa test')
                                <?php if($val[$no] == 1){$tanpatest++;} ?>
                                @else
                                Salah
                                @endif
                            </td>
                            <input type="hidden" name="id_master_imunologi" value="{{$hasil->id_master_imunologi}}">
<!--                        @if($tabung == $hasil->tabung)
                            @else
                                <td rowspan="3">
                                    <select name="strategi[]" class="form-control" id="strategi1">
                                            @if(count($strategi))
                                                @foreach($strategi as $str)
                                                    @if($str->tabung == 5)
                                                        <option value="{{$str->nilai}}">{{$str->kategori}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        <option value=""></option>
                                        <option value="0">Tidak Sesuai</option>
                                        <option value="5">Sesuai</option>
                                    </select>
                                </td>
                            @endif -->

                        <?php $no++; $tabung = $hasil->tabung ?>
                        </tr>
                        @endif
                        @endforeach
                        @else
                        <?php for ($i=0; $i < 5; $i++) { ?>
                        <tr>
                            <td>-</td>
                            <td>-</td>
                            <td>TIDAK DIKERJAKAN</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                        <?php } ?>
                        <input type="hidden" name="id_master_imunologi" value="">
                        @endif
                    </table>

                    <p>
                        1.   Tahap pemeriksaan yang Saudara kerjakan sudah
                        <select name="nilai_1">
                            @if(count($evaluasi))
                                @if($evaluasi->nilai_1 != NULL)
                                    @if($evaluasi->nilai_1 == 1)
                                        <option value="1">Sesuai</option>
                                    @else
                                        <option value="0">Tidak Sesuai</option>
                                    @endif
                                @else
                                <option value="">Tidak dinilai</option>
                              @endif
                            @endif
                            <option value=""></option>
                            <option value="1">Sesuai</option>
                            <option value="0">Tidak Sesuai</option>
                        </select>
                        dengan strategi III pemeriksaan Anti HIV
                    </p>
                    <p>
                        2.  Tahap pemeriksaan yang Saudara kerjakan
                        <select name="nilai_2">
                            @if(count($evaluasi))
                                @if($evaluasi->nilai_2 != NULL)
                                    @if($evaluasi->nilai_2 == 1)
                                        <option value="1">Sesuai</option>
                                    @else
                                        <option value="0">Tidak Sesuai</option>
                                    @endif
                                  @else
                                  <option value="">Tidak dinilai</option>
                              @endif
                            @endif
                            <option value=""></option>
                            <option value="1">Sesuai</option>
                            <option value="0">Tidak Sesuai</option>
                        </select>
                        dengan strategi III pemeriksaan Anti HIV. Gunakan alur pemeriksaan yang sesuai dengan strategi III untuk menegakkan diagnosis HIV.
                    </p>
                    <p>
                        3. Sensitifitas dan spesifisitas reagen yang Saudara gunakan
                        <select name="nilai_3">
                                @if(count($evaluasi))
                                    @if($evaluasi->nilai_3 != NULL)
                                        @if($evaluasi->nilai_3 == 1)
                                            <option value="1">Sesuai</option>
                                        @else
                                            <option value="0">Tidak Sesuai</option>
                                        @endif
                                    @else
                                    <option value="">Tidak dinilai</option>
                                    @endif
                                @endif
                            <option value=""></option>
                            <option value="1">Sesuai</option>
                            <option value="0">Tidak Sesuai</option>
                        </select>
                        dengan ketentuan Permenkes 15 tahun 2015.
                    </p>
                    <p>
                        4. Sensitifitas dan spesifisitas reagen yang Saudara gunakan
                        <select name="nilai_4">
                            @if(count($evaluasi))
                                @if($evaluasi->nilai_4 != NULL)
                                    @if($evaluasi->nilai_4 == 1)
                                        <option value="1">Sesuai</option>
                                    @else
                                        <option value="0">Tidak Sesuai</option>
                                    @endif
                                @else
                                <option value="">Tidak dinilai</option>
                                @endif
                            @endif
                            <option value=""></option>
                            <option value="1">Sesuai</option>
                            <option value="0">Tidak Sesuai</option>
                        </select>
                        dengan ketentuan Permenkes 15 tahun 2015. Gunakan urutan reagen berdasarkan ketentuan sensitivitas dan spesifisitas pada permenkes No. 15 tahun 2015.
                    </p>
                    <p>
                        5.  Reagen yang Saudara gunakan
                        <select name="nilai_5">
                            @if(count($evaluasi))
                                @if($evaluasi->nilai_5 != NULL)
                                    @if($evaluasi->nilai_5 == 1)
                                        <option value="1">Sesuai</option>
                                    @else
                                        <option value="0">Tidak Sesuai</option>
                                    @endif
                                @else
                                <option value="">Tidak dinilai</option>
                                @endif
                            @endif
                            <option value=""></option>
                            <option value="1">Sesuai</option>
                            <option value="0">Tidak Sesuai</option>
                        </select>
                        dengan hasil evaluasi laboratorium rujukan nasional RSUPN Cipto Mangunkusumo. Gunakan jenis reagen yang sudah dilakukan evaluasi oleh RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        6.  Tahap pemeriksaan yang Saudara kerjakan
                        <select name="nilai_6">
                            @if(count($evaluasi))
                                @if($evaluasi->nilai_6 != NULL)
                                    @if($evaluasi->nilai_6 == 1)
                                        <option value="1">Sesuai</option>
                                    @else
                                        <option value="0">Tidak Sesuai</option>
                                    @endif
                                @else
                                <option value="">Tidak dinilai</option>
                                @endif
                            @endif
                            <option value=""></option>
                            <option value="1">Sesuai</option>
                            <option value="0">Tidak Sesuai</option>
                        </select>
                        dengan strategi I pemeriksaan Anti HIV.
                    </p>
                    <p>
                        7.
                            @if(count($evaluasi))
                                @if($evaluasi->lain != NULL)
                                    <textarea class="form-control" name="lain">{{$evaluasi->lain}}</textarea>
                                @else
                                    <textarea class="form-control" name="lain"></textarea>
                                @endif
                            @else
                                    <textarea class="form-control" name="lain"></textarea>
                            @endif
                    </p>

                    <h4>Penilaian Hasil Pemeriksaan Peserta :</h4>
                    <blockquote>
                        1. Ketepatan Hasil  <select name="ketepatan">
                                                @if(count($kesimpulan))
                                                    @if($kesimpulan->ketepatan != NULL)
                                                        <option value="{{$kesimpulan->ketepatan}}">{{$kesimpulan->ketepatan}}</option>
                                                        <option value="Baik">Baik</option>
                                                        <option value="Kurang">Kurang</option>
                                                        <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                    @else
                                                        @if($hasilp == 5)
                                                        <option value="Baik">Baik</option>
                                                        @elseif($tanpatest > 0)
                                                        <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                        @else
                                                        <option value="Kurang">Kurang</option>
                                                        @endif
                                                        <option></option>
                                                        <option value="Baik">Baik</option>
                                                        <option value="Kurang">Kurang</option>
                                                        <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                    @endif
                                                @else
                                                    @if($hasilp == 5)
                                                    <option value="Baik">Baik</option>
                                                    @elseif($tanpatest > 0)
                                                    <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                    @else
                                                        @if(count($data2))
                                                            <option value="Kurang">Kurang</option>
                                                        @else
                                                            <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                        @endif
                                                    @endif
                                                    <option></option>
                                                    <option value="Baik">Baik</option>
                                                    <option value="Kurang">Kurang</option>
                                                    <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                @endif
                                            </select><br>
                        2. Kesesuaian Strategi  <select name="kesesuaian">
                                                    @if(count($kesimpulan))
                                                        @if($kesimpulan->ketepatan != NULL)
                                                        <option value="{{$kesimpulan->kesesuaian}}">{{$kesimpulan->kesesuaian}}</option>
                                                            <option value="Sesuai">Sesuai</option>
                                                            <option value="Tidak Sesuai">Tidak Sesuai</option>
                                                        @else
                                                            <option></option>
                                                            <option value="Sesuai">Sesuai</option>
                                                            <option value="Tidak Sesuai">Tidak Sesuai</option>
                                                        @endif
                                                    @else
                                                        <option></option>
                                                        <option value="Sesuai">Sesuai</option>
                                                        <option value="Tidak Sesuai">Tidak Sesuai</option>
                                                    @endif
                                                </select>
                    </blockquote>
                     <label hidden="">Tanggal TTD :</label>
                    <div class="controls input-append date" hidden="" data-link-field="dtp_input1">
                        @if(count($evaluasi))
                        <input size="16" type="text" value="{{$evaluasi->tanggal_ttd}}" class="form_datetime form-control" name="ttd">
                        @else
                        <input size="16" type="text" value="" class="form_datetime form-control" name="ttd">
                        @endif
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div><br>
                    @if(count($evaluasi))
                    <a href="{{url('hasil-pemeriksaan/anti-hiv/evaluasi/print').'/'.$register->id.'?y='.$type}}">
                        <input type="button" class="btn btn-primari" value="Print" name="print"/>
                    </a>
                    <input type="submit" class="btn btn-primari" value="Update" name="update" onclick="askForUpdate()" />
                    @else
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-primary">
                    @endif
                      {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
form=document.getElementById("someForm");
function askForPrint() {
        form.action="{{url('hasil-pemeriksaan/anti-hiv/evaluasi/print').'/'.$register->id.'?y='.$type}}";
        form.submit();
}
function askForUpdate() {
        form.action="{{url('hasil-pemeriksaan/anti-hiv/evaluasi/update').'/'.$register->id.'?y='.$type}}";
        form.submit();
}

var total1=[0,0,0];
var total2=[0,0,0];
var total3=[0,0,0];
$(document).ready(function(){

    var $dataRows=$("#peserta_pme tr:not('.titlerowna')");

    $dataRows.each(function() {
        $(this).find('.colomngitung1').each(function(i){
            total1[i]+=parseInt( $(this).html());
            hasil1 = total1[i] / 3;
        });
        $(this).find('.colomngitung2').each(function(i){
            total2[i]+=parseInt( $(this).html());
            hasil2 = total2[i] / 3;
        });
        $(this).find('.colomngitung3').each(function(i){
            total3[i]+=parseInt( $(this).html());
            hasil3 = total3[i] / 3;
        });
    });

    $("#peserta_pme td.evaluasi1").each(function(i){
        if (hasil1 < 4) {
            console.log( "no ready!" );
            $(".evaluasi1").html("Tidak Baik");
        }else{
            $(".evaluasi1").html("Baik");
            console.log( "Baik" );
        }
    });
    $("#peserta_pme td.evaluasi2").each(function(i){
        if (hasil2 < 4) {
            console.log( "no ready!" );
            $(".evaluasi2").html("Tidak Baik");
        }else{
            $(".evaluasi2").html("Baik");
            console.log( "Baik" );
        }
    });
    $("#peserta_pme td.evaluasi3").each(function(i){
        if (hasil3 < 4) {
            console.log( "no ready!" );
            $(".evaluasi3").html("Tidak Baik");
        }else{
            $(".evaluasi3").html("Baik");
            console.log( "Baik" );
        }
    });

});

$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});

$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});
</script>
@endsection

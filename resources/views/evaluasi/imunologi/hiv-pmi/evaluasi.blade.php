@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluasi</div>
                <div class="panel-body">
                    <center><label>EVALUASI PESERTA PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI TAHUN {{$date}} <br> HASIL PEMERIKSAAN HIV-PMI <input type="hidden" name="type" value="{{$type}}"></label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Kode Peserta </label>
                        <div class="col-sm-10">
                            <input readonly="" type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{ $kodeperusahaan }}" placeholder="Kode Peserta">
                        </div>
                    </div><br>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Nama Instansi </label>
                        <div class="col-sm-10">
                            <input readonly="" type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{ $perusahaan }}" placeholder="Kode Peserta">
                        </div>
                    </div>
                    @if(count($kesimpulan))
                        <form id="someForm" action="" method="post">
                    @else
                        <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    @endif
                    <label>2. HASIL EVALUASI</label>
                    <table class="table table-bordered" id="peserta_pme">
                        <tr class="titlerowna">
                            <th style="text-align: center;">Kode Bahan Uji</th>
                            <th style="text-align: center;">Metode Pemeriksaan</th>
                            <th style="text-align: center;">Reagen</th>
                            <th style="text-align: center;">Hasil Pemeriksaan</th>
                            <th style="text-align: center;">Hasil Rujukan</th>
                            <th style="text-align: center;">Kesesuaian Hasil</th>
                        </tr>
                        <?php
                            $no = 0;
                            $val = ['I','II','III','IV','V'];
                            $hasilp = 0;
                            $tanpatest = 0;
                        ?>
                        @foreach($data2 as $hasil)
                        <tr><input readonly="" type="hidden" name="idhp[]" value="{{$hasil->id}}">
                            <td><input readonly="" type="text" name="kode_bahan_kontrol[]" class="form-control" required value="{{$hasil->kode_bahan_kontrol}}"></td>
                        @if($hasil->interpretasi == 'Tanpa test')
                        <td></td>
                        <td></td>
                        @else
                            @if(!empty($reagen[0]))
                                <td style="text-transform: uppercase; text-align: center;">{{$reagen[0]->metode}}</td>
                                <td style="text-align: center;">
                                    {!! str_replace('Lain - lain', '', $reagen[0]->reagen_lain.$reagen[0]->reagen)!!}
                                </td>
                            @else
                            <td></td>
                            <td></td>
                            @endif
                        @endif
                            <td style="text-align: center;">{{$hasil->interpretasi}}</td>
                            <td style="text-align: center;">{{$hasil->nilai_rujukan}}</td>
                            <td style="text-align: center;">
                                @if($hasil->interpretasi == $hasil->nilai_rujukan)
                                <?php $hasilp++; ?>
                                Benar
                                <input type="hidden" name="strategi[]" value="5">
                                @elseif($hasil->interpretasi == 'tanpa-test')
                                <?php $tanpatest++; ?>
                                @else
                                Salah
                                <input type="hidden" name="strategi[]" value="0">
                                @endif
                            </td>
                            <input type="hidden" name="id_master_imunologi" value="{{$hasil->id_master_imunologi}}">
                        </tr>
                        <?php $no++; ?>
                        @endforeach
                    </table>
                    <p>
                        1.   Tahap pemeriksaan yang Saudara kerjakan sudah
                        <select name="nilai_1">
                            @if(count($evaluasi))
                                @if($evaluasi->nilai_1 != NULL)
                                    @if($evaluasi->nilai_1 == 1)
                                        <option value="1">Sesuai</option>
                                    @else
                                        <option value="0">Tidak Sesuai</option>
                                    @endif
                                @else
                                <option value="">Tidak dinilai</option>
                              @endif
                            @endif
                            <option value=""></option>
                            <option value="1">Sesuai</option>
                            <option value="0">Tidak Sesuai</option>
                        </select>
                        dengan strategi III pemeriksaan Anti HIV
                    </p>
                    <p>
                        2.  Tahap pemeriksaan yang Saudara kerjakan
                        <select name="nilai_2">
                            @if(count($evaluasi))
                                @if($evaluasi->nilai_2 != NULL)
                                    @if($evaluasi->nilai_2 == 1)
                                        <option value="1">Sesuai</option>
                                    @else
                                        <option value="0">Tidak Sesuai</option>
                                    @endif
                                  @else
                                  <option value="">Tidak dinilai</option>
                              @endif
                            @endif
                            <option value=""></option>
                            <option value="1">Sesuai</option>
                            <option value="0">Tidak Sesuai</option>
                        </select>
                        dengan strategi III pemeriksaan Anti HIV. Gunakan alur pemeriksaan yang sesuai dengan strategi III untuk menegakkan diagnosis HIV.
                    </p>
                    <p>
                        3. Sensitifitas dan spesifisitas reagen yang Saudara gunakan
                        <select name="nilai_3">
                                @if(count($evaluasi))
                                    @if($evaluasi->nilai_3 != NULL)
                                        @if($evaluasi->nilai_3 == 1)
                                            <option value="1">Sesuai</option>
                                        @else
                                            <option value="0">Tidak Sesuai</option>
                                        @endif
                                    @else
                                    <option value="">Tidak dinilai</option>
                                    @endif
                                @endif
                            <option value=""></option>
                            <option value="1">Sesuai</option>
                            <option value="0">Tidak Sesuai</option>
                        </select>
                        dengan ketentuan Permenkes 15 tahun 2015.
                    </p>
                    <p>
                        4. Sensitifitas dan spesifisitas reagen yang Saudara gunakan
                        <select name="nilai_4">
                            @if(count($evaluasi))
                                @if($evaluasi->nilai_4 != NULL)
                                    @if($evaluasi->nilai_4 == 1)
                                        <option value="1">Sesuai</option>
                                    @else
                                        <option value="0">Tidak Sesuai</option>
                                    @endif
                                @else
                                <option value="">Tidak dinilai</option>
                                @endif
                            @endif
                            <option value=""></option>
                            <option value="1">Sesuai</option>
                            <option value="0">Tidak Sesuai</option>
                        </select>
                        dengan ketentuan Permenkes 15 tahun 2015. Gunakan urutan reagen berdasarkan ketentuan sensitivitas dan spesifisitas pada permenkes No. 15 tahun 2015.
                    </p>
                    <p>
                        5. Reagen yang Saudara gunakan
                        <select name="nilai_5">
                            @if(count($evaluasi))
                                @if($evaluasi->nilai_5 != NULL)
                                    @if($evaluasi->nilai_5 == 1)
                                        <option value="1">Sesuai</option>
                                    @else
                                        <option value="0">Tidak Sesuai</option>
                                    @endif
                                @else
                                <option value="">Tidak dinilai</option>
                                @endif
                            @endif
                            <option value=""></option>
                            <option value="1">Sesuai</option>
                            <option value="0">Tidak Sesuai</option>
                        </select>
                        dengan hasil evaluasi laboratorium rujukan nasional RSUPN Cipto Mangunkusumo. Gunakan jenis reagen yang sudah dilakukan evaluasi oleh RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        6.  Tahap pemeriksaan yang Saudara kerjakan
                        <select name="nilai_6">
                            @if(count($evaluasi))
                                @if($evaluasi->nilai_6 != NULL)
                                    @if($evaluasi->nilai_6 == 1)
                                        <option value="1">Sesuai</option>
                                    @else
                                        <option value="0">Tidak Sesuai</option>
                                    @endif
                                @else
                                <option value="">Tidak dinilai</option>
                                @endif
                            @endif
                            <option value=""></option>
                            <option value="1">Sesuai</option>
                            <option value="0">Tidak Sesuai</option>
                        </select>
                        dengan strategi I pemeriksaan Anti HIV.
                    </p>
                    <p>
                        7.
                            @if(count($evaluasi))
                                @if($evaluasi->lain != NULL)
                                    <textarea class="form-control" name="lain">{{$evaluasi->lain}}</textarea>
                                @else
                                    <textarea class="form-control" name="lain"></textarea>
                                @endif
                            @else
                                    <textarea class="form-control" name="lain"></textarea>
                            @endif
                    </p>

                    <h4>Penilaian Hasil Pemeriksaan Peserta :</h4>
                    <blockquote>
                        1. Ketepatan Hasil  <select name="ketepatan">
                                                @if(count($kesimpulan))
                                                    @if($kesimpulan->ketepatan != NULL)
                                                        <option value="{{$kesimpulan->ketepatan}}">{{$kesimpulan->ketepatan}}</option>
                                                        <option value="Baik">Baik</option>
                                                        <option value="Kurang">Kurang</option>
                                                        <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                    @else
                                                        @if($hasilp == 5)
                                                        <option value="Baik">Baik</option>
                                                        @elseif($tanpatest > 0)
                                                        <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                        @else
                                                        <option value="Kurang">Kurang</option>
                                                        @endif
                                                        <option></option>
                                                        <option value="Baik">Baik</option>
                                                        <option value="Kurang">Kurang</option>
                                                        <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                    @endif
                                                @else
                                                    @if($hasilp == 5)
                                                    <option value="Baik">Baik</option>
                                                    @elseif($tanpatest > 0)
                                                    <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                    @else
                                                    <option value="Kurang">Kurang</option>
                                                    @endif
                                                    <option></option>
                                                    <option value="Baik">Baik</option>
                                                    <option value="Kurang">Kurang</option>
                                                    <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                @endif
                                            </select><br>
                        2. Kesesuaian Strategi  <select name="kesesuaian">
                                                    @if(count($kesimpulan))
                                                        @if($kesimpulan->ketepatan != NULL)
                                                        <option value="{{$kesimpulan->kesesuaian}}">{{$kesimpulan->kesesuaian}}</option>
                                                            <option value="Sesuai">Sesuai</option>
                                                            <option value="Tidak Sesuai">Tidak Sesuai</option>
                                                        @else
                                                            <option></option>
                                                            <option value="Sesuai">Sesuai</option>
                                                            <option value="Tidak Sesuai">Tidak Sesuai</option>
                                                        @endif
                                                    @else
                                                        <option></option>
                                                        <option value="Sesuai">Sesuai</option>
                                                        <option value="Tidak Sesuai">Tidak Sesuai</option>
                                                    @endif
                                                </select>
                    </blockquote>
                    @if(count($kesimpulan))
                    <a href="{{url('hasil-pemeriksaan/anti-hiv/evaluasi/print').'/'.$register->id.'?y='.$type}}">
                        <input type="button" class="btn btn-primari" value="Print" name="print"/>
                    </a>
                    <input type="button" class="btn btn-primari" value="Update" name="update" onclick="askForUpdate()" />
                    @else
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-primari">
                    @endif
                      {{ csrf_field() }}
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
form=document.getElementById("someForm");
function askForPrint() {
        form.action="{{url('hasil-pemeriksaan/anti-hiv/evaluasi/print').'/'.$register->id.'?y='.$type}}";
        form.submit();
}
function askForUpdate() {
        form.action="{{url('hasil-pemeriksaan/anti-hiv/evaluasi/update').'/'.$register->id.'?y='.$type}}";
        form.submit();
}

var total2=[0,0,0];
$(document).ready(function(){

    var $dataRows=$("#peserta_pme tr:not('.titlerowna')");

    $dataRows.each(function() {
        $(this).find('.colomngitung').each(function(i){
            total2[i]+=parseInt( $(this).html());
        });
    });
    $("#peserta_pme td.evaluasi").each(function(i){
        if (total2[i] < 15) {
            console.log( "no ready!" );
            $(".evaluasi").html("Tidak Baik");
        }else{
            $(".evaluasi").html("Baik");
            console.log( "Baik" );
        }
    });

});

$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});
</script>
@endsection

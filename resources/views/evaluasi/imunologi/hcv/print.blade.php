<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{
    border: 1px solid #ddd;
    text-align: center;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}
</style>
<table width="100%" cellpadding="0" border="0">
    <thead>
        <tr>
            <th>
                <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px">
            </th>
            <th style="text-align:center; " width="100%">
                <span style="font-size: 16px;margin-left:-85px;">KEMENTERIAN KESEHATAN RI</span><br>
                <span style="font-size: 12px;margin-left:-70px;">PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL ANTI HCV SIKLUS {{$type}} TAHUN {{$tahun}}</span>
                <pre style="font-family: Helvetica;font-size: 12px; padding: -10px; margin-left: -140px;">
                Penyelenggara : Balai Besar Laboratorium Kesehatan Surabaya
                Jl.Karangmenjangan No. 18 Surabaya 60286
                Telepon : 031-5021451 Fax.031-5020388, 031-5053076
                Email : pme.bblksub@gmail.com
                </pre>
            </th>
        </tr>
        <tr>
            <th colspan="2"><hr></th>
        </tr>
    </thead>
</table>

<center><label><b>LAMPIRAN EVALUASI PESERTA<br>
PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL ANTI HCV <br>SIKLUS {{$type}} TAHUN {{$tahun}}</b>
<input type="hidden" name="type" value="{{$type}}"></label></center><br>
<form class="form-horizontal" method="post" enctype="multipart/form-data">
<b>Nama Instansi :</b> &nbsp;{{$perusahaan}}<br>
<b>Kode Peserta :</b> &nbsp;{{$kodeperusahaan}}
<br><br>
<table class="table table-bordered" id="peserta_pme">
    <tr class="titlerowna">
        <th>Nama Reagen</th>
        <th>Kode Bahan Uji</th>
        <th>Hasil Pemeriksaan</th>
        <th>Hasil Rujukan</th>
        <th>Kesesuaian Hasil</th>
        <th>Nilai Peserta</th>
    </tr>
    <?php
        $no = 0;
        $val = ['I','II','III','IV','V'];
        $tabung = '';
    ?>
    @if(count($data2))
    @foreach($data2 as $hasil)
    @if($hasil->tabung == '1')
    <tr>
        @if($hasil->interpretasi == 'Tanpa test')
            @if($no != 0)
            @else
                <td rowspan="5"></td>
            @endif
        @else
            @if($no != 0)
            @else
                @if(!empty($reagen[0]))
                    <td rowspan="5">
                        @if($reagen[0]->reagen == 'Lain - lain')
                        {{$reagen[0]->reagen_lain}}
                        @else
                        {{$reagen[0]->reagen}}
                        @endif
                    </td>
                @else
                    <td rowspan="5"></td>
                @endif
            @endif
        @endif
        <td align="middle">{{$hasil->kode_bahan_kontrol}}</td>
        <td>{{$hasil->interpretasi}}</td>
        <td>{{$hasil->nilai_rujukan}}</td>
        <td>
            @if($hasil->interpretasi == 'Tanpa test')
            Tidak Dapat dinilai
            @else
                @if($hasil->nilai_rujukan == 'Reaktif / Non Reaktif')
                    Benar
                @elseif($hasil->interpretasi == $hasil->nilai_rujukan)
                    Benar
                @else
                    Salah
                @endif
            @endif
        </td>

        @if($no != 0)
        @else
        <td rowspan="5">{{$kesimpulan->ketepatan}}</td>
        @endif
    <?php $no++; $tabung = $hasil->tabung ?>
    </tr>
    @endif
    @endforeach
    @else
        <?php for ($i=0; $i < 5; $i++) { ?>
        <tr>
            <td>-</td>
            <td>-</td>
            <td>TIDAK DIKERJAKAN</td>
            <td>-</td>
            <td>-</td>
            @if($i != 0)
            @else
            <td rowspan="5">{{$kesimpulan->ketepatan}}</td>
            @endif
        </tr>
        <?php } ?>
    @endif
</table>
<br>
<p>
    {!!$catatan->catatan!!}
</p>
<p>
<div style="position: relative; top: 22px">
Surabaya, {{$ttd->tanggal}} {{$tahun}}<br>
Manajer Teknis
</div>
<p>
    <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.'ttdevaluasi'.DIRECTORY_SEPARATOR.$ttd->image)}}" width="80" height="80" style="margin-left: 20px !important;">
</p>

<div style="position: relative; top:-20px">
  {{$ttd->nama}}<br>NIP. {{$ttd->nip}}
</div>
</p>
<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>

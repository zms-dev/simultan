@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Upload Laporan Akhir</div>
                <div class="panel-body">
                	@foreach($data as $asu)
                    <form action="{{url('evaluasi/upload-laporan-akhir/update').'/'.$asu->id}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                      <div>
                          <label for="exampleInputEmail1">Parameter</label>
                          <select id="alat1" class="form-control" name="parameter" class="form-control" required>
                              <option value="{{$asu->parameter}}">
                              	{{ $asu->alias }}
                              </option>
                              @foreach($parameter as $val)
                                <option value="{{$val->id}}">{{$val->alias}}</option>
                              @endforeach
                          </select>
                      </div>
                      <div id="row_alat1" class="inpualat1">
                          <label for="exampleInputEmail1">Tipe</label>
                          <select class="form-control" name="tipe">
                            <option value="{{$asu->tipe}}">@if($asu->tipe == "a") 01 @else 02 @endif</option>
                            <option value="a">01</option>
                            <option value="b">02</option>
                          </select>
                      </div>
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <select class="form-control" name="siklus">
	                            <option value="{{$asu->siklus}}">{{ $asu->siklus }}</option>
	                            <option value="1">1</option>
	                            <option value="2">2</option>
                          </select>
                      </div>
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <select class="form-control" name="tahun">
                            <option value="{{ $asu->tahun }}">{{$asu->tahun}}</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                          </select>
                      </div>
                      <div>
                        <input type="hidden" name="bagian" value="Isi">
                          <!-- <label for="exampleInputEmail1">Bagian</label>
                          <select class="form-control" name="bagian">
                            <option value="{{ $asu->bagian }}">{{ $asu->bagian }}</option>
                            <option value="Pendahuluan">Pendahulan</option>
                            <option value="Isi">Isi</option>
                            <option value="Penutup">Penutup</option>
                          </select> -->
                      </div>
                      <br>
                       @if($errors->any())
                        <div class="{{ $errors->has('file') ? 'has-error' : 'has-success'}}">
                        @else 
                          <div class=""> 
                        @endif 
                        <input type="file" name="file" value="">
                        @if($errors->has('file')) 
                          <span class="help-block">{{ $errors->first('file') }}</span>
                        @endif 
                        </div>

                      <br>
                      	<input type="submit" name="proses" class="btn btn-primary" value="Proses">
                    	{{ csrf_field() }}
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>	
<script>                            
$(function() {
    $('#row_alat1').hide(); 
    $('#alat1').change(function(){
    var setan  = $("#alat1 option:selected").val();
        if(setan <= '3') {
            $('#row_alat1').show(); 
        } else {
            $('#row_alat1').hide(); 
        } 
    });
});
</script>    
@endsection
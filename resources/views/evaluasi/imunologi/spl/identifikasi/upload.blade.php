@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Data Upload Evaluasi Identifikasi</div>
                <div class="panel-body">
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ Session::get('message') }}
                    </p>
                @endif
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                      <div>
                          <label for="exampleInputEmail1">Parameter</label>
                          <select id="alat1" class="form-control" name="parameter" class="form-control" required>
                              <option selected></option>
                              @foreach($parameter as $val)
                                <option value="{{$val->id}}">{{$val->alias}}</option>
                              @endforeach
                          </select>
                      </div>
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <select class="form-control" name="siklus">
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div>
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <select class="form-control" name="tahun">
                            <option></option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                          </select>
                      </div>
                      <div>
                          <label for="exampleInputEmail1">Peserta</label>
                          <select class="form-control" name="id_registrasi">
                            <option></option>
                            @if(count($peserta))
                            @foreach($peserta as $val)
                            <option value="{{$val->id}}">{{$val->nama_lab}} | {{$val->kode_lebpes}}</option>
                            @endforeach
                            @endif
                          </select>
                      </div>
                      <div>
                        <br>
                        @if($errors->any())
                        <div class="{{ $errors->has('file') ? 'has-error' : 'has-success'}}">
                        @else 
                          <div class=""> 
                        @endif 
                        <input type="file" name="file">
                        @if($errors->has('file')) 
                          <span class="help-block">{{ $errors->first('file') }}</span>
                        @endif 
                        </div>

                      </div>
                      <br>
                    <input type="submit" name="proses" class="btn btn-primary" value="Proses">
                    {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection
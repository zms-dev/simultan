@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cetak Sertifikat Imunologi Siklus {{$siklus}}</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="get" enctype="multipart/form-data">
                    <div>
                        <label for="exampleInputEmail1">Pilih Tanggal Sertifikat :</label>
                        <div class="controls input-append date tanggal" data-link-field="dtp_input1">
                            <input size="16" type="text" value="" readonly class="form-control" name="tanggal" id="tanggal">
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                    </div><br>
                    <table class="table table-bordered">
                        <tr>
                            <th width="5%">No</th>
                            <th>Bidang</th>
                            <th colspan="2" width="15%"><center>Aksi</center></th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>

                        @if($val->id_bidang > '5')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}</td>
                                @if($siklus == 1)
                                    <td colspan="2" class="text-center" style="padding: 2%;">
                                        @if($val->siklus_1 == 'done')
                                            <a href="" class="go-link"  data-id="{{$val->perusahaan_id}}" data-y="1" data-link="{{$val->Link}}" target="_blank"><center><i class="glyphicon glyphicon-pencil"></i><br></center></a>
                                        @else
                                            <a href="" class="go-link"  data-id="{{$val->perusahaan_id}}" data-y="1" data-link="{{$val->Link}}" target="_blank"><center><i class="glyphicon glyphicon-pencil"></i><br></center></a>
                                        <!-- Belum Dikirim -->
                                        @endif
                                    </td>
                                @else
                                    <td colspan="2" class="text-center"  style="padding: 2%;">
                                        @if($val->siklus_2 == 'done')
                                           <a href="" class="go-link"  data-id="{{$val->perusahaan_id}}" data-y="2" data-link="{{$val->Link}}" target="_blank"><center><i class="glyphicon glyphicon-pencil"></i><br></center></a>
                                        @else
                                           <a href="" class="go-link"  data-id="{{$val->perusahaan_id}}" data-y="2" data-link="{{$val->Link}}" target="_blank"><center><i class="glyphicon glyphicon-pencil"></i><br></center></a>
                                         <!-- Belum Dikirim -->
                                        @endif
                                    </td>
                                @endif
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(".go-link").on("click",function(e){
    e.preventDefault()
    var y = $(this).data("y");
    var idLink = $(this).data("id");
    var link = $(this).data("link");
    var tanggal = $("#tanggal").val();
    var baseUrl = "{{URL('')}}"+link+"/sertifikat/print/"+idLink+"?y="+y+"&tanggal="+tanggal+"&download=true";
    var url = baseUrl;
    console.log(url);

    window.location.href = url;
});

$(".go-link-rpr").on("click",function(e){
    e.preventDefault()
    var y = $(this).data("y");
    var idLink = $(this).data("id");
    var tanggal = $("#tanggal").val();
    var baseUrl = "{{URL('')}}"+"/hasil-pemeriksaan/rpr-syphilis"+"/sertifikat/print/"+idLink+"?y="+y+"&tanggal="+tanggal+"&download=true";
    var url = baseUrl;
    console.log(url);

    window.location.href = url;
});

$(".tanggal").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 2,
    startView: 4
});
</script>
@endsection

@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cetak Evaluasi Siklus {{$siklus}}</div>

                <div class="panel-body">
                    <table class="table table-bordered">
                        <tr>
                            <th width="5%">No</th>
                            <th>Bidang</th>
                            <th colspan="2" width="15%"><center>Aksi</center></th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>

                        @if($val->id_bidang > '5')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                @if($val->bidang == '7')
                                    @if($siklus == 1)
                                        @if($val->siklus_1 == 'done')
                                            @if($val->rpr1 == 'done')
                                                @if($val->pemeriksaan == 'done')
                                                    @if($val->status_data1 == 2)
                                                    <td>
                                                        <a href="{{URL('').$val->Link}}/evaluasi/print/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                                    </td>
                                                    @else
                                                    <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                                    @endif
                                                    @if($val->status_datarpr1 == 2)
                                                    <td>
                                                        <a href="{{URL('hasil-pemeriksaan/rpr-syphilis/evaluasi/print')}}/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>RPR</center></a>
                                                    </td>
                                                    @else
                                                    <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                                    @endif
                                                @else
                                                    @if($val->status_data1 == 2)
                                                    <td>
                                                        <a href="{{URL('hasil-pemeriksaan/rpr-syphilis/evaluasi/print')}}/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>RPR</center></a>
                                                    </td>
                                                    @else
                                                    <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                                    @endif
                                                @endif
                                            @else
                                                @if($val->status_data1 == 2)
                                                <td>
                                                    <a href="{{URL('').$val->Link}}/evaluasi/print/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                                </td>
                                                @else
                                                <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                                @endif
                                            @endif
                                        @else
                                            @if($val->rpr1 == 'done')
                                                @if($val->pemeriksaan == 'done')
                                                    @if($val->status_data1 == 2)
                                                    <td>
                                                        <a href="{{URL('').$val->Link}}/evaluasi/print/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                                    </td>
                                                    <td>
                                                        <a href="{{URL('hasil-pemeriksaan/rpr-syphilis/evaluasi/print')}}/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>RPR</center></a>
                                                    </td>
                                                    @else
                                                    <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                                    @endif
                                                    @else
                                                    @if($val->status_data1 == 2)
                                                    <td>
                                                        <a href="{{URL('hasil-pemeriksaan/rpr-syphilis/evaluasi/print')}}/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>RPR</center></a>
                                                    </td>
                                                    @else
                                                    <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                                    @endif
                                                @endif
                                            @else
                                                @if($val->status_data1 == 2)
                                                <td>
                                                    <a href="{{URL('').$val->Link}}/evaluasi/print/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                                </td>
                                                @else
                                                <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                                @endif
                                            @endif                                    
                                        @endif
                                    @else
                                        @if($val->siklus_2 == 'done')
                                            @if($val->rpr2 == 'done')
                                                @if($val->pemeriksaan2 == 'done')
                                                    @if($val->status_data2 == 2)
                                                    <td>
                                                        <a href="{{URL('').$val->Link}}/evaluasi/print/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                                    </td>
                                                    <td>
                                                        <a href="{{URL('hasil-pemeriksaan/rpr-syphilis/evaluasi/print')}}/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>RPR</center></a>
                                                    </td>
                                                    @else
                                                    <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                                    @endif
                                                @else
                                                    @if($val->status_data2 == 2)
                                                    <td>
                                                        <a href="{{URL('hasil-pemeriksaan/rpr-syphilis/evaluasi/print')}}/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>RPR</center></a>
                                                    </td>
                                                    @else
                                                    <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                                    @endif
                                                @endif
                                            @else
                                                @if($val->status_data2 == 2)
                                                <td>
                                                    <a href="{{URL('').$val->Link}}/evaluasi/print/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                                </td>
                                                @else
                                                <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                                @endif
                                            @endif
                                        @else
                                            @if($val->rpr2 == 'done')
                                                @if($val->pemeriksaan2 == 'done')
                                                    @if($val->status_data2 == 2)
                                                    <td>
                                                        <a href="{{URL('').$val->Link}}/evaluasi/print/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                                    </td>
                                                    <td>
                                                        <a href="{{URL('hasil-pemeriksaan/rpr-syphilis/evaluasi/print')}}/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>RPR</center></a>
                                                    </td>
                                                    @else
                                                    <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                                    @endif
                                                    @else
                                                    @if($val->status_data2 == 2)
                                                    <td>
                                                        <a href="{{URL('hasil-pemeriksaan/rpr-syphilis/evaluasi/print')}}/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>RPR</center></a>
                                                    </td>
                                                    @else
                                                    <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                                    @endif
                                                @endif
                                            @else
                                                @if($val->status_data2 == 2)
                                                <td>
                                                    <a href="{{URL('').$val->Link}}/evaluasi/print/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                                </td>
                                                @else
                                                <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                                @endif
                                            @endif                                    
                                        @endif
                                    @endif
                                @else
                                    @if($siklus == 1)
                                    <td colspan="2" class="text-center" style="padding: 2%;">
                                        @if($val->siklus_1 == 'done')
                                            @if($val->status_data1 == 2)
                                            <a href="{{URL('').$val->Link}}/evaluasi/print/{{$val->id}}?y=1" target="_blank"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                            @else
                                            Belum Dikirim
                                            @endif
                                        @else
                                        Belum Dikirim
                                        @endif
                                    </td>
                                    @else
                                    <td colspan="2" class="text-center"  style="padding: 2%;">
                                        @if($val->siklus_2 == 'done')
                                            @if($val->status_data2 == 2)
                                            <a href="{{URL('').$val->Link}}/evaluasi/print/{{$val->id}}?y=2" target="_blank"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                            @else
                                            Belum Dikirim
                                            @endif
                                        @else
                                         Belum Dikirim
                                        @endif
                                    </td>
                                    @endif
                                @endif
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<table border="1">
    <thead>
        <tr>
            <th rowspan="2">Kode Lab</th>
            @foreach($parameterna as $val)
            <th colspan="2">{!!$val->nama_parameter!!}</th>
            @endforeach
            <th rowspan="2">Tanggal Kirim Data</th>
        </tr>
        <tr>
            <th></th>
            @foreach($parameterna as $val)
            <th><center>Hasil</center></th>
            <th><center>Metode</center></th>
            @endforeach
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $val)
        <tr>
            <td>{{$val->kode_lab}}</td>
            @foreach($val->parameter as $par)
                @foreach($par->detail as $hasil)
                    <td>'{{$hasil->hasil_pemeriksaan}}</td>
                    <td>{!!$hasil->metode_pemeriksaan!!} @if($hasil->metode_pemeriksaan == 'Metode lain :') {{$hasil->metode_lain}} @endif</td>
                @endforeach
            @endforeach
            <td>{{$val->updated_at}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
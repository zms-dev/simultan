<style type="text/css">
@page {
margin: 150px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 2px;
    text-align: center;
    border: 1px solid #ddd;
    font-size:9px;
}

#header {
    position: fixed;
    border-bottom:1px solid gray;
    padding-top: -140px
}
#footer {
    position: fixed;
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
</style>
<div id="header">
    <table width="100%" cellpadding="0" border="0" >
        <thead>
            <tr>
                <th width="11%"></th>
                <th>
                   <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px"> 
                </th>
                <td width="50%" style="padding-left: 30px;" >
                    <span style="font-size: 12px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 10px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL KIMIA KLINIK SIKLUS {{$siklus}}  TAHUN {{$date}}</b></span><br>
                    <span style="font-size: 10px">Penyelenggara : <b>Balai Besar Laboratorium Kesehatan Surabaya</b></span>
                    <div style="padding-left: 75px; margin-top: -15px; font-size: 10px">
                        <span><br/>Jalan Karangmenjangan No, 18 Surabaya - 60286<br>Telepon : (031) 5021451 | Fax : (031) 5020388<br>Web Online : simultan.bblksurabaya.id <br>Email : pme.bblksub@gmail.com</span>
                    </div>
                </td>
                <th width="%"></th>
                <th>
                    <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" height="90px">
                </th>

                
            </tr>
            <tr>
                <th colspan="6"><hr></th>
            </tr>
        </thead>
    </table>
</div>
@if($type == 'a')
    @php $halaman = '6'; @endphp
@else
    @php $halaman = '7'; @endphp
@endif
<div id="footer">
    PNPME-BBLK SURABAYA <span style="margin-left: 79%;">Halaman {{$halaman}} dari 43</span>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body"><br>
               <table class="table utama">
                        <thead>
                            <tr>
                                <th colspan="17">
                                    <center>
                                        <font size="10px">
                                            <b>HASIL HASIL EVALUASI PNPME HEMATOLOGI SIKLUS {{$siklus}}-{{$urut}} TAHUN {{$date}}</b>
                                            {{-- KODE PESERTA : {{$datas->kode_lab}} --}}
                                            {{-- KELOMPOK (SELURUH PESERTA) --}}
                                        </font>
                                    </center>
                                </th>
                            </tr>
                            <tr>
                              <th rowspan="2">Kode Lab</th>
                              <th rowspan="2">Parameter</th>
                              <th rowspan="2">Instrument</th>
                              <th rowspan="2">Metode Pemeriksaan</th>
                              <th rowspan="2">Hasil</th>
                              <th colspan="4" align="center">Seluruh Peserta</th>
                              <th colspan="4" align="center">Kelompok Alat</th>
                              <th colspan="4" align="center">Kelompok Metode</th>
                            </tr>
                            <tr>
                                <th>Nilai Target</th>
                                <th>n</th>
                                <th>Z-Score</th>
                                <th>Keterangan</th>
                                <th>Nilai Target</th>
                                <th>n</th>
                                <th>Z-Score</th>
                                <th>Keterangan</th>
                                <th>Nilai Target</th>
                                <th>n</th>
                                <th>Z-Score</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $val)
                            <tr>
                                <td>{{$val->kode_lab}}</td>
                                <td>
                                    {!!$val->nama_parameter!!}
                                </td>
                                <td>{{$val->instrumen}}  {{$val->instrument_lain}}</td>
                                <td>{!!$val->metode_pemeriksaan!!}  {{$val->metode_lain}}</td>

                                <td>
                                  @if($val->catatan == 'Hasil pemeriksaan menggunakan 1 (satu) desimal')
                                    @if($val->hasil_pemeriksaan != '-')
                                      {{ number_format($val->hasil_pemeriksaan, 1) }}
                                    @else
                                      {{$val->hasil_pemeriksaan}}
                                    @endif
                                  @else
                                    @if($val->hasil_pemeriksaan != '-')
                                      {{ number_format($val->hasil_pemeriksaan, 0) }}
                                    @else
                                      {{$val->hasil_pemeriksaan}}
                                    @endif
                                  @endif
                                </td>
                                <td>
                                    @if (count($val->sd))
                                      @if($val->catatan == 'Hasil pemeriksaan menggunakan 1 (satu) desimal')
                                        @if($val->sd[0]->median != '-')
                                          {{ number_format($val->sd[0]->median, 1) }}
                                        @else
                                          {{$val->sd[0]->median}}
                                        @endif
                                      @else
                                        @if($val->sd[0]->median != '-')
                                          {{ number_format($val->sd[0]->median, 0) }}
                                        @else
                                          {{$val->sd[0]->median}}
                                        @endif
                                      @endif
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if (count($val->sd))
                                      @if($val->catatan == 'Hasil pemeriksaan menggunakan 1 (satu) desimal')
                                        @if($val->sd[0]->median != '-')
                                          {{$val->totalp}}
                                        @else
                                          {{$val->totalp}}
                                        @endif
                                      @else
                                        @if($val->sd[0]->median != '-')
                                          {{$val->totalp}}
                                        @else
                                          {{$val->totalp}}
                                        @endif
                                      @endif
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($val->zscore == 'Tidak di analisis')
                                        Tidak di analisis
                                    @elseif($val->zscore == "Tidak di Evaluasi")
                                        -
                                    @else
                                        {{$val->zscore}}
                                    @endif
                                </td>
                                <td>
                                    <?php
                                        if($val->zscore == 'Tidak di analisis'){
                                            $keterangan = 'Tidak di analisis';
                                        }elseif($val->zscore == "Tidak di Evaluasi"){
                                          $keterangan = 'Tidak Dapat di Evaluasi';
                                        }elseif($val->zscore == "-"){
                                          $keterangan = 'Tidak Dapat di Evaluasi';
                                        }else if($val->zscore <= 2 && $val->zscore >= -2){
                                            $keterangan = 'Memuaskan';
                                        }else if($val->zscore <= 3 && $val->zscore >= -3){
                                            $keterangan = 'Peringatan';
                                        }else{
                                            $keterangan = 'Tidak Memuaskan';
                                        }
                                    ?>
                                    {{$keterangan}}
                                </td>
                                <td>
                                    @if(count($val->sd_alat))
                                        @if($val->sd_alat[0]->median != '-' && $val->sd_alat[0]->median != '')
                                            {{$val->sd_alat[0]->median}}
                                        @else
                                            -
                                        @endif
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if(count($val->sd_alat))
                                        @if($val->sd_alat[0]->median != '-' && $val->sd_alat[0]->median != '')
                                            {{$val->totala}}
                                        @else
                                            -
                                        @endif
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($val->zscore_alat == 'Tidak di analisis')
                                        Tidak di analisis
                                    @elseif($val->zscore_alat == "Tidak dapat dievaluasi")
                                        -
                                    @else
                                        {{$val->zscore_alat}}
                                    @endif
                                </td>
                                <td>
                                    <?php
                                        if($val->zscore_alat == 'Tidak di analisis'){
                                            $keterangan = 'Tidak di analisis';
                                        }elseif($val->zscore_alat == "Tidak dapat dievaluasi"){
                                          $keterangan = 'Tidak Dapat di Evaluasi';
                                        }elseif($val->zscore_alat == "-"){
                                          $keterangan = 'Tidak Dapat di Evaluasi';
                                        }else if($val->zscore_alat <= 2 && $val->zscore_alat >= -2){
                                            $keterangan = 'Memuaskan';
                                        }else if($val->zscore_alat <= 3 && $val->zscore_alat >= -3){
                                            $keterangan = 'Peringatan';
                                        }else{
                                            $keterangan = 'Tidak Memuaskan';
                                        }
                                    ?>
                                    {{$keterangan}}
                                </td>
                                <td>
                                    @if (count($val->sd_metode))
                                      @if($val->catatan == 'Hasil pemeriksaan menggunakan 1 (satu) desimal')
                                        @if($val->sd_metode[0]->median != '-' && $val->sd_metode[0]->median != '')
                                            {{ number_format($val->sd_metode[0]->median, 1) }}
                                        @else
                                            -
                                        @endif
                                      @else
                                        @if($val->sd_metode[0]->median != '-' && $val->sd_metode[0]->median != '')
                                            {{ number_format($val->sd_metode[0]->median, 0) }}
                                          @else
                                            -
                                          @endif
                                      @endif
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if (count($val->sd_metode))
                                      @if($val->catatan == 'Hasil pemeriksaan menggunakan 1 (satu) desimal')
                                        @if($val->sd_metode[0]->median != '-' && $val->sd_metode[0]->median != '')
                                            {{$val->totalm}}
                                        @else
                                            -
                                        @endif
                                      @else
                                        @if($val->sd_metode[0]->median != '-' && $val->sd_metode[0]->median != '')
                                            {{$val->totalm}}
                                          @else
                                            -
                                          @endif
                                      @endif
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    @if($val->zscore_metode == 'Tidak di analisis')
                                    Tidak di analisis
                                    @else
                                        {{$val->zscore_metode}}
                                    @endif
                                </td>
                                <td>
                                    <?php
                                        if($val->zscore_metode == 'Tidak di analisis'){
                                            $keterangan = 'Tidak di analisis';
                                        }elseif($val->zscore_metode == "-"){
                                          $keterangan = 'Tidak Dapat Evaluasi';
                                        }else if($val->zscore_metode <= 2 && $val->zscore_metode >= -2){
                                            $keterangan = 'Memuaskan';
                                        }else if($val->zscore_metode <= 3 && $val->zscore_metode >= -3){
                                            $keterangan = 'Peringatan';
                                        }else{
                                            $keterangan = 'Tidak Memuaskan';
                                        }
                                    ?>
                                    {{$keterangan}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                <br>
                <table border="0">
                    <tr>
                        <th>Komentar / Saran: </th>
                    </tr>
                    <tr>
                        <td>
                            Pertahankan hasil Saudara yang sudah "memuaskan". Untuk hasil "peringatan" dan "tidak memuaskan" agar dilakukan evaluasi hasil PMI, proses praanalitik (penanganan sempel), analitik dan pasca analitik (penginputan hasil)
                        </td>
                    </tr>
                </table>
                <table width="100%" style="margin-left: 0px !important; page-break-inside: avoid;">
                    <tr>
                        <td></td>
                        <td>Surabaya, {{$ttd->tanggal}} {{$date}}<br></td>
                    </tr>
                    <tr>
                        <th width="80%">Keterangan :</th>
                        <td width="20%">Manajer Teknis</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 20px" rowspan="3">
                            Kriteria Skor<br>
                            I Z Score I <span style="font-family:DejaVu Sans, Book">&le;</span> 2 = Memuaskan<br>
                            2 > I Z Score I <span style="font-family:DejaVu Sans, Book">&le;</span> 3 = Peringatan<br>
                            I Z Score I > 3 = Tidak Memuaskan
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.'ttdevaluasi'.DIRECTORY_SEPARATOR.$ttd->image)}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.'ttdevaluasi'.DIRECTORY_SEPARATOR.$ttd->image)}}" width="120" height="120" style="margin-left: -10px !important; position: absolute; top: -7%;" >
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 4%;">
                                {{$ttd->nama}}<br>NIP. {{$ttd->nip}}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Parameter Hematologi dengan Nilai yang Sama Tipe @if($type == 'a') 01 @else 02 @endif</div>
                <div class="panel-body" id="datana">
                    <style>
                        .report-chart-title{
                            text-align:center;
                            padding:15px 0px;
                            margin-bottom:30px;
                        }
                        .report-chart-title h3{
                            font-size:22px;
                            margin:0px;
                            padding:0px;
                        }
                        .row-chart{
                            padding:0px;
                        }
                        .row-chart .col{
                            display:block;
                            width:50%;
                            float:left;
                        }
                        .row-chart:after{
                            content : " ";
                            clear:both;
                            display:block;
                        }
                        .graph-container{
                            padding:0px;
                            margin-bottom:30px;
                            width:100%;
                            display:block;
                            padding:0;
                            text-align:center;
                        }
                        .graph-container .graph{
                            width:90%;
                            margin:auto;
                            display:inline-block;
                            height:300px;
                        }
                        .table-information{
                            border-bottom:1px solid  #ddd;
                        }
                        .table-information td{
                            height:36px;
                            vertical-align:middle !important;
                        }
                        .report-chart-title h3{
                            font-size:16px;
                        }
                        .color-black{
                            background: #000 !important;color: #000 !important;display:inline-block;width:15px;height:25px;
                        }
                        .color-red{
                            background: #BF0B23 !important;color: #BF0B23 !important;display:inline-block;width:15px;height:25px;
                        }
                        .color-orange{
                            background: #ffa756 !important;color: #ffa756 !important;display:inline-block;width:15px;height:25px;
                        }
                        .color-green{
                            background: #67ca3b !important;color: #67ca3b !important;display:inline-block;width:15px;height:25px;
                        }
                        @media print {
                            .graph-container .graph{
                                width:100%;
                                margin: auto;
                                height:300px;
                                display: inline-block;
                            }

                            .color-black{
                                -webkit-print-color-adjust: exact;
                                background: #000 !important;display:inline-block;width:15px;height:25px;
                            }
                            .color-red{
                                -webkit-print-color-adjust: exact;
                                background: #BF0B23 !important;display:inline-block;width:15px;height:25px;
                            }
                            .color-orange{
                                -webkit-print-color-adjust: exact;
                                background: #ffa756 !important;display:inline-block;width:15px;height:25px;
                            }
                            .color-green{
                                -webkit-print-color-adjust: exact;
                                background: #67ca3b !important;display:inline-block;width:15px;height:25px;
                            }
                        }
                        @media screen {
                            .left{
                                display: none;
                            }
                            .print-area .footer-content .left{
                                display: none;
                            }
                        }
                        .print-area .footer-content{
                            position:absolute;
                            bottom:0;
                            left:0;
                            right:0;
                            border-top:1px solid #333;
                        }
                        .print-area .footer-content .left{
                            float:left;
                            width:50%;
                            font-size: 14px;
                            font-weight: 400;
                        }
                        .print-area .footer-content .right{
                            float:right;
                            width:50%;
                            text-align:right;
                        }
                        .print-area .footer-content:after{
                            content : " ";
                            display : block;
                            clear:both;
                        }
                    </style>
                    <div class="report-chart-title">
                        <h3>GRAFIK DISTRIBUSI HASIL PNPME BIDANG HEMATOLOGI</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-information table-condensed">
                                <tbody>
                                    <tr>
                                        <td width="150px" style="font-weight:bold;">KODE PESERTA</td>
                                        <td width="10px">:</td>
                                        <td width="40%">{{$parameter[0]->dataReg->kode_lebpes}}</td>
                                        <td width="50px"><b>*</b></td>
                                        <td width="10px">:</td>
                                        <td>Hasil Peserta</td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight:bold;">TAHUN</td>
                                        <td>:</td>
                                        <td>{{$tahun}}</td>
                                        <td><div class="color-green"></div></td>
                                        <td>:</td>
                                        <td>Nilai Target</td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight:bold;">SIKLUS</td>
                                        <td>:</td>
                                        <td>{{$siklus}} - {{($type == 'a' ? '01' : '02')}}</td>
                                        <td style="font-weight:bold;"><!-- <div class="color-red"></div> --></td>
                                        <td></td>
                                        <td><!--  > +- 2 SD --></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <table width="100%">
                        @foreach($parameter as $val)
                        <tr>
                            <td width="33.3%"><div id="container{{$val->id}}"></div></td>
                            <td width="33.3%"><div id="containeralat{{$val->id}}"></div></td>
                            <td width="33.3%"><div id="containermetode{{$val->id}}"></div></td>
                        </tr>
                        @endforeach
                    </table><br>
                </div>
                <a style="margin: 10px" onclick="javascript:printDiv('datana')" class="btn btn-primary">Print</a>
            </div>
        </div>
    </div>
</div>

<script src="{{URL('/js/jquery.printarea.js')}}"></script>
<script src="https://code.highcharts.com/modules/pattern-fill.js"></script>

<script type="text/javascript">
function printDiv(divID) {
    var headElements = '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>,<meta name="viewport" content="width=device-width, initial-scale=1">';
    var options = { mode : "popup", popClose : true,extraHead : headElements };
    $( '#'+divID ).printArea( options );    
}

Highcharts.exportCharts = function (charts, options, e) {
    options = Highcharts.merge(Highcharts.getOptions().exporting, options);
    Highcharts.post(options.url, {
        filename: options.filename || 'chart',
        type: options.type,
        width: options.width,
        svg: Highcharts.getSVG(charts, e)
    });
};

@foreach($parameter as $val)
var hg{{$val->id}} = Highcharts.chart('container{{$val->id}}', {
    exporting: {
        enabled: true // hide button
    },
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Distribusi Hasil Parameter {!!$val->nama_parameter!!} Kelompok Seluruh Peserta Type {{($type == "a" ? "01" : "02")}}',
        style : {
            'font-size' : "10px",
            'font-weight' : "bold",
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text : 'Jumlah Peserta'
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y} {point.a}',
                crop: false,
                overflow: 'none'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
    },


    "series": [
    {
        "name": "Nilai yang Sama",
        "colorByPoint": false,
        "data": [
            {
                "name": "< {{$val->grafik->ring1}}",
                @if($val->dataReg->hasil_pemeriksaan < $val->grafik->ring1)
                "a": "*",
                @endif
                // "color": '#BF0B23',
                "y": {{$val->grafik->out_bawah_param}},
            },
            {
                "name": ">= {{$val->grafik->ring1}} < {{$val->grafik->ring2}}",
                @if($val->dataReg->hasil_pemeriksaan >= $val->grafik->ring1 && $val->dataReg->hasil_pemeriksaan < $val->grafik->ring2)
                "a": "*",
                @endif
                @if($val->median->median >= $val->grafik->ring1 && $val->median->median < $val->grafik->ring2)
                "color": '#1bbd20',
                @endif
                "y": {{$val->grafik->count_ring_1_param}},
            },
            {
                "name": ">= {{$val->grafik->ring2}} < {{$val->grafik->ring3}}",
                @if($val->dataReg->hasil_pemeriksaan >= $val->grafik->ring2 && $val->dataReg->hasil_pemeriksaan < $val->grafik->ring3)
                "a": "*",
                @endif
                @if($val->median->median >= $val->grafik->ring2 && $val->median->median < $val->grafik->ring3)
                "color": '#1bbd20',
                @endif
                "y": {{$val->grafik->count_ring_2_param}},
            },
            {
                "name": ">= {{$val->grafik->ring3}} < {{$val->grafik->ring4}}",
                @if($val->dataReg->hasil_pemeriksaan >= $val->grafik->ring3 && $val->dataReg->hasil_pemeriksaan < $val->grafik->ring4)
                "a": "*",
                @endif
                @if($val->median->median >= $val->grafik->ring3 && $val->median->median < $val->grafik->ring4)
                "color": '#1bbd20',
                @endif
                "y": {{$val->grafik->count_ring_3_param}},
            },
            {
                "name": ">= {{$val->grafik->ring4}} <= {{$val->grafik->ring5}}",
                @if($val->dataReg->hasil_pemeriksaan >= $val->grafik->ring4 && $val->dataReg->hasil_pemeriksaan <= $val->grafik->ring5)
                "a": "*",
                @endif
                @if($val->median->median >= $val->grafik->ring4 && $val->median->median <= $val->grafik->ring5)
                "color": '#1bbd20',
                @endif
                "y": {{$val->grafik->count_ring_4_param}},
            },
            {
                "name": "> {{$val->grafik->ring5}}",
                @if($val->dataReg->hasil_pemeriksaan > $val->grafik->ring5)
                "a": "*",
                @endif
                // "color": '#BF0B23',
                "y": {{$val->grafik->out_atas_param}},
            },
        ]
    }],
});

var hgalat{{$val->id}} = Highcharts.chart('containeralat{{$val->id}}', {
    exporting: {
        enabled: true // hide button
    },
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Distribusi Hasil Parameter {!!$val->nama_parameter!!} <br> Kelompok Alat {{$val->instrumen->instrumen}} {{$val->dataReg->instrument_lain}} Type {{($type == "a" ? "01" : "02")}}',
        style : {
            'font-size' : "10px",
            'font-weight' : "bold",
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text : 'Jumlah Peserta'
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y} {point.a}',
                crop: false,
                overflow: 'none'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
    },


    "series": [
    {
        "name": "Nilai yang Sama",
        "colorByPoint": false,
        "data": [
            {
                "name": "< {{$val->grafik->ring1}}",
                @if($val->dataReg->hasil_pemeriksaan < $val->grafik->ring1)
                "a": "*",
                @endif
                // "color": '#BF0B23',
                "y": {{$val->grafik->out_bawah_alat}},
            },
            {
                "name": ">= {{$val->grafik->ring1}} < {{$val->grafik->ring2}}",
                @if($val->dataReg->hasil_pemeriksaan >= $val->grafik->ring1 && $val->dataReg->hasil_pemeriksaan < $val->grafik->ring2)
                "a": "*",
                @endif
                @if(!empty($val->medianalat) && $val->medianalat->median >= $val->grafik->ring1 && $val->medianalat->median < $val->grafik->ring2)
                "color": '#1bbd20',
                @endif
                "y": {{$val->grafik->count_ring_1_alat}},
            },
            {
                "name": ">= {{$val->grafik->ring2}} < {{$val->grafik->ring3}}",
                @if($val->dataReg->hasil_pemeriksaan >= $val->grafik->ring2 && $val->dataReg->hasil_pemeriksaan < $val->grafik->ring3)
                "a": "*",
                @endif
                @if(!empty($val->medianalat) && $val->medianalat->median >= $val->grafik->ring2 && $val->medianalat->median < $val->grafik->ring3)
                "color": '#1bbd20',
                @endif
                "y": {{$val->grafik->count_ring_2_alat}},
            },
            {
                "name": ">= {{$val->grafik->ring3}} < {{$val->grafik->ring4}}",
                @if($val->dataReg->hasil_pemeriksaan >= $val->grafik->ring3 && $val->dataReg->hasil_pemeriksaan < $val->grafik->ring4)
                "a": "*",
                @endif
                @if(!empty($val->medianalat) && $val->medianalat->median >= $val->grafik->ring3 && $val->medianalat->median < $val->grafik->ring4)
                "color": '#1bbd20',
                @endif
                "y": {{$val->grafik->count_ring_3_alat}},
            },
            {
                "name": ">= {{$val->grafik->ring4}} <= {{$val->grafik->ring5}}",
                @if($val->dataReg->hasil_pemeriksaan >= $val->grafik->ring4 && $val->dataReg->hasil_pemeriksaan <= $val->grafik->ring5)
                "a": "*",
                @endif
                @if(!empty($val->medianalat) && $val->medianalat->median >= $val->grafik->ring4 && $val->medianalat->median <= $val->grafik->ring5)
                "color": '#1bbd20',
                @endif
                "y": {{$val->grafik->count_ring_4_alat}},
            },
            {
                "name": "> {{$val->grafik->ring5}}",
                @if($val->dataReg->hasil_pemeriksaan > $val->grafik->ring5)
                "a": "*",
                @endif
                // "color": '#BF0B23',
                "y": {{$val->grafik->out_atas_alat}},
            },
        ]
    }],
});

var hgmetode{{$val->id}} = Highcharts.chart('containermetode{{$val->id}}', {
    exporting: {
        enabled: true // hide button
    },
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Distribusi Hasil {!!$val->nama_parameter!!} <br> Kelompok {{$val->metode->metode_pemeriksaan}} {{$val->dataReg->metode_lain}} Type {{($type == "a" ? "01" : "02")}}',
        style : {
            'font-size' : "10px",
            'font-weight' : "bold",
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text : 'Jumlah Peserta'
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y} {point.a}',
                crop: false,
                overflow: 'none'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
    },


    "series": [
    {
        "name": "Nilai yang Sama",
        "colorByPoint": false,
        "data": [
        {
            "name": "< {{$val->grafik->ring1}}",
            @if($val->dataReg->hasil_pemeriksaan < $val->grafik->ring1)
            "a": "*",
            @endif
            // "color": '#BF0B23',
            "y": {{$val->grafik->out_bawah_metode}},
        },
        {
            "name": ">= {{$val->grafik->ring1}} < {{$val->grafik->ring2}}",
            @if($val->dataReg->hasil_pemeriksaan >= $val->grafik->ring1 && $val->dataReg->hasil_pemeriksaan < $val->grafik->ring2)
            "a": "*",
            @endif
            @if(!empty($val->medianmetode) && $val->medianmetode->median >= $val->grafik->ring1 && $val->medianmetode->median < $val->grafik->ring2)
            "color": '#1bbd20',
            @endif
            "y": {{$val->grafik->count_ring_1_metode}},
        },
        {
            "name": ">= {{$val->grafik->ring2}} < {{$val->grafik->ring3}}",
            @if($val->dataReg->hasil_pemeriksaan >= $val->grafik->ring2 && $val->dataReg->hasil_pemeriksaan < $val->grafik->ring3)
            "a": "*",
            @endif
            @if(!empty($val->medianmetode) && $val->medianmetode->median >= $val->grafik->ring2 && $val->medianmetode->median < $val->grafik->ring3)
            "color": '#1bbd20',
            @endif
            "y": {{$val->grafik->count_ring_2_metode}},
        },
        {
            "name": ">= {{$val->grafik->ring3}} < {{$val->grafik->ring4}}",
            @if($val->dataReg->hasil_pemeriksaan >= $val->grafik->ring3 && $val->dataReg->hasil_pemeriksaan < $val->grafik->ring4)
            "a": "*",
            @endif
            @if(!empty($val->medianmetode) && $val->medianmetode->median >= $val->grafik->ring3 && $val->medianmetode->median < $val->grafik->ring4)
            "color": '#1bbd20',
            @endif
            "y": {{$val->grafik->count_ring_3_metode}},
        },
        {
            "name": ">= {{$val->grafik->ring4}} <= {{$val->grafik->ring5}}",
            @if($val->dataReg->hasil_pemeriksaan >= $val->grafik->ring4 && $val->dataReg->hasil_pemeriksaan <= $val->grafik->ring5)
            "a": "*",
            @endif
            @if(!empty($val->medianmetode) && $val->medianmetode->median >= $val->grafik->ring4 && $val->medianmetode->median <= $val->grafik->ring5)
            "color": '#1bbd20',
            @endif
            "y": {{$val->grafik->count_ring_4_metode}},
        },
        {
            "name": "> {{$val->grafik->ring5}}",
            @if($val->dataReg->hasil_pemeriksaan > $val->grafik->ring5)
            "a": "*",
            @endif
            // "color": '#BF0B23',
            "y": {{$val->grafik->out_atas_metode}},
        },
        ]
    }],
});
@endforeach
</script>

@endsection
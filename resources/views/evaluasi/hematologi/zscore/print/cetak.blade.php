<style type="text/css">
@page {
margin: 150px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 5px;
    text-align: left;
    border: 1px solid #ddd;
}

#header {
    position: fixed;
    border-bottom:1px solid gray;
    padding-top: -140px
}
#footer {
    position: fixed;
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
</style>
<div id="header">
    <table width="100%" cellpadding="0" border="0" >
        <thead>
            <tr>
                <th>
                   <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px">
                </th>
                <td width="100%" style="padding-left: 20px">
                    <span style="font-size: 12px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 10px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL HEMATOLOGI SIKLUS {{$siklus}} TAHUN {{$date}}</b></span><br>
                    <span style="font-size: 10px">Penyelenggara : <b>Balai Besar Laboratorium Kesehatan Surabaya</b></span>
                    <div style="padding-left: 75px; margin-top: -15px; font-size: 10px">
                        <span><br/>Jalan Karangmenjangan No, 18 Surabaya - 60286<br>Telepon : (031) 5021451 | Fax : (031) 5020388<br>Web Online : simultan.bblksurabaya.id <br>Email : pme.bblksub@gmail.com</span>
                    </div>
                </td>
                <th>
                    <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" height="90px">
                </th>
            </tr>
            <tr>
                <th colspan="3"><hr></th>
            </tr>
        </thead>
    </table>
</div>
<div id="footer">
    EVALUASI PNPME HEMATOLOGI SIKLUS {{$siklus}} TAHUN {{$date}}
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body">
                    <label>
                        <center>
                            <font size="10px">
                                <b>HASIL EVALUASI PNPME HEMATOLOGI SIKLUS {{$siklus}} TAHUN {{$date}}</b><br><br>
                                KODE PESERTA : {{$datas->kode_lab}}
                            </font>
                        </center>
                    </label><br>
               <table class="table utama">
                        <thead>
                            <tr>
                                <th>Kode Lab</th>
                                <th>Parameter</th>
                                <th>Instrument</th>
                                <th>Metode Pemeriksaan</th>
                                <th>Hasil</th>
                                <th>Nilai Target</th>
                                <th>Z-Score</th>
                                @if(count($zscore))
                                <th>Keterangan</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $val)
                            <tr>
                                <td>{{$val->kode_lab}}</td>
                                <td>
                                    {!!$val->nama_parameter!!}
                                    <input type="hidden" name="parameter[]" value="{{$val->id}}">
                                </td>
                                <td>{{$val->instrumen}}  {{$val->instrument_lain}}</td>
                                <td>{!!$val->metode_pemeriksaan!!}  {{$val->metode_lain}}</td>
                                <td>{{$val->hasil_pemeriksaan}}</td>
                                <td>
                                    @if (count($val->sd))
                                        {{$val->sd[0]->median}}
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    <?php
                                        if (count($val->sd)) {
                                            $z = ($val->hasil_pemeriksaan - $val->sd[0]->median) / $val->sd[0]->sd;
                                        }else{
                                            $z = $val->hasil_pemeriksaan;
                                        }
                                    ?>
                                    @if(count($zscore))
                                        @foreach($zscore as $za)
                                            @if($za->parameter == $val->id)
                                                @if($za->zscore == 'Tidak di analisis')
                                                Tidak di analisis
                                                @else
                                                {{number_format($za->zscore, 1)}}
                                                @endif
                                            @endif
                                        @endforeach
                                    @else
                                        @if($val->hasil_pemeriksaan == NULL)
                                        Tidak di analisis
                                        <input type="hidden" name="zscore[]" value="Tidak di analisis">
                                        @else
                                        {{number_format($z, 1)}}
                                        <input type="hidden" name="zscore[]" value="{{number_format($z, 1)}}">
                                        @endif
                                    @endif
                                </td>
                                @if(count($zscore))
                                    @foreach($zscore as $za)
                                        @if($za->parameter == $val->id)
                                            <td>
                                                <?php
                                                    if($za->zscore == 'Tidak di analisis'){
                                                        $keterangan = 'Tidak di analisis';
                                                    }else if($za->zscore <= 2 && $za->zscore >= -2){
                                                        $keterangan = 'Memuaskan';
                                                    }else if($za->zscore <= 3 && $za->zscore >= -3){
                                                        $keterangan = 'Peringatan';
                                                    }else{
                                                        $keterangan = 'Tidak Memuaskan';
                                                    }
                                                ?>
                                                {{$keterangan}}
                                            </td>
                                        @endif
                                    @endforeach
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table><br>
                    <p><b>Keterangan :</b></p><br>
                    <p style="position: relative; left: 3%;">
                        Kriteria Skor<br>
                        > 3,00 = Sangat Baik<br>
                        > 2,00 – 3,00 = Baik<br>
                        > 1,00 – 2,00 = Kurang<br>
                        <= 1,00 = Buruk
                    </p>

                    <div style="position: relative; left: 81%; top: -15%;">
                    <p>
                        <div style="position: relative; top: 22px">
                        Surabaya, {{ _dateIndo($tanggal) }}<br>
                        Manajer Teknis
                        </div>
                        <p>
                            @if($date == '2018')
                                <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'ttd_martini.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'ttd_martini.png')}}" width="120" height="120" style="margin-left: -10px !important; position: absolute; top: -7%;" >
                            @else
                                <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'ttd_2019.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'ttd_2019.png')}}" width="120" height="120" style="margin-left: -10px !important; position: absolute; top: -7%;" >
                            @endif
                        </p>

                        <div style="position: relative; top:-20px">
                            @if($date == '2018')
                                Martini,S,Si<br>NIP. 196609141992032002
                            @else
                                Wargianti,S.Si<br>NIP. 196107191982012001
                            @endif
                        </div>
                    </p>
                    </div>
            </div>
        </div>
    </div>
</div>

@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Input Range Per Parameter (Hematologi)</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-md-6">
                          <div>
                              <label for="exampleInputEmail1">Siklus</label>
                              <select class="form-control" name="siklus" required>
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Tahun</label>
                              <select class="form-control" name="tahun" required>
                                <option></option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                              </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div>
                              <label for="exampleInputEmail1">Type</label>
                              <select class="form-control" name="tipe" required>
                                <option></option>
                                <option value="a">01</option>
                                <option value="b">02</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Periode <small>* (Untuk tahun 2019 siklus 1 dan priode 2)</small></label>
                              <select class="form-control" name="periode">
                                <option></option>
                                <option value="2">2</option>
                              </select>
                          </div>
                        </div>
                      </div>
                      <br>
                      <table class="table table-bordered">
                        <tr>
                          <th>Parameter</th>
                          <th>Range 1</th>
                          <th>Range 2</th>
                          <th>Range 3</th>
                          <th>Range 4</th>
                          <th>Range 5</th>
                          <th>Range 6</th>
                        </tr>
                        @foreach($parameter as $val)
                        <tr>
                          <td><input type="hidden" name="parameter[]" class="form-control" value="{{$val->id}}">{!!$val->nama_parameter!!}</td>
                          <td><input type="text" name="ring1[]" class="form-control ring"></td>
                          <td><input type="text" name="ring2[]" class="form-control ring"></td>
                          <td><input type="text" name="ring3[]" class="form-control ring"></td>
                          <td><input type="text" name="ring4[]" class="form-control ring"></td>
                          <td><input type="text" name="ring5[]" class="form-control ring"></td>
                          <td><input type="text" name="ring6[]" class="form-control ring"></td>
                        </tr>
                        @endforeach
                      </table>
                    <input type="submit" name="proses" class="btn btn-primary" value="Simpan">
                    {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('.ring').keyup(function(){
    var val = $(this).val();
    if(isNaN(val)){
         val = val.replace(/[^0-9\.]/g,'');
         if(val.split('.').length>2) 
             val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});
</script>
@endsection
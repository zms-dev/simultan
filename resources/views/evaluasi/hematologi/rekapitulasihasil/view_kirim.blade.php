<style type="text/css">
body{
    font-family: Open-sans;
    font-size: 10px;
}
td, th{
    border: 1px solid #333;
    text-align: center;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 2px 5px;
}
.footer{
  position: fixed;

}
footer {
    position: fixed;
    bottom: -25px;
    left: 0px;
    right: 0px;
    height: 50px;

    /** Extra personal styles **/
    /* background-color: #03a9f4;
    /* color: white; */
    text-align: left;
    line-height: 35px;
}
</style>
<body>
  

<main>
  <?php $no = 0;?>
  <?php $halaman = 4;?>
  @foreach($parameter as $val)
  <?php $no++; ?>
  @if($no == 5)
  <?php $no = 0;?>
  <?php $no++; ?>
  <?php $halaman++; ?>
  <div style="page-break-before:always">
  @else
  <div>
  @endif
  <h4>Rekapitulasi Hasil PESERTA PNPME Parameter {!!$val->terbilang!!} Siklus {{$input['siklus']}} @if($input['periode'] == 2)Periode 2 @endif</h4>
  <table border="1">
      <thead>
          <tr>
              <th rowspan="3">Kriteria</th>
              <th colspan="2">Siklus {{$input['siklus']}}.01</th>
              <th colspan="2">Siklus {{$input['siklus']}}.02</th>
          </tr>
          <tr>
              <th colspan="2">Peserta</th>
              <th colspan="2">Peserta</th>
          </tr>
          <tr>
              <th>Jumlah</th>
              <th>%</th>
              <th>Jumlah</th>
              <th>%</th>
          </tr>
      </thead>
      <tbody>
          <tr>
              <td>Kirim Hasil</td>
              <td>
                  {{$val->datapesertaa1->KIRIM_HASIL_PESERTA}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->datapesertaa1->KIRIM_HASIL_PESERTA / ($val->datapesertaa1->KIRIM_HASIL_PESERTA + $val->datapesertaa1->TIDAK_KIRIM_HASIL_PESERTA_1 + $val->datapesertaa1->TIDAK_KIRIM_HASIL_PESERTA_2)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
              <td>
                  {{$val->datapesertaa2->KIRIM_HASIL_PESERTA}}
              </td>
              <td>
                  <?php
                      $persen2 = ($val->datapesertaa2->KIRIM_HASIL_PESERTA / ($val->datapesertaa2->KIRIM_HASIL_PESERTA + $val->datapesertaa2->TIDAK_KIRIM_HASIL_PESERTA_1 + $val->datapesertaa2->TIDAK_KIRIM_HASIL_PESERTA_2)) * 100;
                      echo number_format($persen2, 2)."%";
                  ?>
              </td>
          </tr>
          <tr>
              <td>Tidak Kirim Hasil</td>
              <td>
                  {{$val->datapesertaa1->TIDAK_KIRIM_HASIL_PESERTA_1 + $val->datapesertaa1->TIDAK_KIRIM_HASIL_PESERTA_2}}
              </td>
              <td>
                  <?php
                      $persen1 = (($val->datapesertaa1->TIDAK_KIRIM_HASIL_PESERTA_1 + $val->datapesertaa1->TIDAK_KIRIM_HASIL_PESERTA_2) / ($val->datapesertaa1->KIRIM_HASIL_PESERTA + $val->datapesertaa1->TIDAK_KIRIM_HASIL_PESERTA_1 + $val->datapesertaa1->TIDAK_KIRIM_HASIL_PESERTA_2)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
              <td>
                  {{$val->datapesertaa2->TIDAK_KIRIM_HASIL_PESERTA_1 + $val->datapesertaa2->TIDAK_KIRIM_HASIL_PESERTA_2}}
              </td>
              <td>
                  <?php
                      $persen2 = (($val->datapesertaa2->TIDAK_KIRIM_HASIL_PESERTA_1 + $val->datapesertaa2->TIDAK_KIRIM_HASIL_PESERTA_2) / ($val->datapesertaa2->KIRIM_HASIL_PESERTA + $val->datapesertaa2->TIDAK_KIRIM_HASIL_PESERTA_1 + $val->datapesertaa2->TIDAK_KIRIM_HASIL_PESERTA_2)) * 100;
                      echo number_format($persen2, 2)."%";
                  ?>
              </td>
          </tr>
          <tr>
              <td style="text-align: center;">JUMLAH</td>
              <td>{{$val->datapesertaa1->KIRIM_HASIL_PESERTA + $val->datapesertaa1->TIDAK_KIRIM_HASIL_PESERTA_1 + $val->datapesertaa1->TIDAK_KIRIM_HASIL_PESERTA_2}}</td>
              <td>100%</td>
              <td>{{$val->datapesertaa2->KIRIM_HASIL_PESERTA + $val->datapesertaa2->TIDAK_KIRIM_HASIL_PESERTA_1 + $val->datapesertaa2->TIDAK_KIRIM_HASIL_PESERTA_2}}</td>
              <td>100%</td>
          </tr>
      </tbody>
  </table>
  </div>
  <footer class="footer">PNPME-BBLK SURABAYA <span style="margin-left: 500px;">Halaman {{$halaman}} dari 43</span></footer>
  @endforeach
</main>
</body>

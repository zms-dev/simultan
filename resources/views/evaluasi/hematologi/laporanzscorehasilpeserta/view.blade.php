<table border="1">
    <thead>
        <tr>
            <th>Kode Lab</th>
            @foreach($parameterna as $val)
            <th>{!!$val->nama_parameter!!}</th>
            @endforeach
            <th>Tanggal Kirim Data</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $val)
        <tr>
            <td>{{$val->kode_lab}}</td>
            <td>@if($val->hemoglobin != NULL) {{$val->hemoglobin}} @else - @endif</td>
            <td>@if($val->lekosit != NULL) {{$val->lekosit}} @else - @endif</td>
            <td>@if($val->eritrosit != NULL) {{$val->eritrosit}} @else - @endif</td>
            <td>@if($val->hematokrit != NULL) {{$val->hematokrit}} @else - @endif</td>
            <td>@if($val->mcv != NULL) {{$val->mcv}} @else - @endif</td>
            <td>@if($val->mch != NULL) {{$val->mch}} @else - @endif</td>
            <td>@if($val->mchc != NULL) {{$val->mchc}} @else - @endif</td>
            <td>@if($val->trombosit != NULL) {{$val->trombosit}} @else - @endif</td>
            <td>@if($val->tanggal_kirim != NULL){{$val->tanggal_kirim}} @else {{$val->updated_at}} @endif</td>
        </tr>
        @endforeach
    </tbody>
</table>

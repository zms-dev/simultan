<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <script src="{{URL::asset('asset/js/jquery.min.js')}}"></script>
  <script src="{{URL::asset('asset/js/highstock.js')}}"></script>
  <script src="{{URL::asset('asset/js/exporting.js')}}"></script>
  <script src="{{URL::asset('asset/js/export-data.js')}}"></script>
  <script src="{{URL::asset('asset/js/highcharts-3d.js')}}"></script>
  <link rel="stylesheet" href="{{URL::asset('asset/css/bootstrap.css')}}">
  <style type="text/css">
    body{
      font-family: arial
    }
  </style>
</head>
<body>
  <div class="container-fluid">
      <div class="row">
          <div class="col-md-10 col-md-offset-1">
              <div class="panel panel-default">
                <div class="panel-heading">Grafik Z-Score Peserta per Parameter HEMATOLOGI</div>
                  <div class="panel-body" id="datana">
                      <style>
                          .report-chart-title{
                              text-align:center;
                              padding:15px 0px;
                              margin-bottom:30px;
                          }
                          .report-chart-title h3{
                              font-size:22px;
                              margin:0px;
                              padding:0px;
                          }
                          .row-chart{
                              padding:0px;
                          }
                          .row-chart .col{
                              display:block;
                              width:50%;
                              float:left;
                          }
                          .row-chart:after{
                              content : " ";
                              clear:both;
                              display:block;
                          }
                          .graph-container{
                              padding:0px;
                              margin-bottom:30px;
                              width:100%;
                              display:block;
                              padding:0;
                              text-align:center;
                          }
                          .graph-container .graph{
                              width:90%;
                              margin:auto;
                              display:inline-block;
                              height:300px;
                          }
                          .table-information{
                              border-bottom:1px solid  #ddd;
                          }
                          .table-information td{
                              height:36px;
                              vertical-align:middle !important;
                          }
                          .report-chart-title h3{
                              font-size:16px;
                          }
                          .color-black{
                              background: #000 !important;color: #000 !important;display:inline-block;width:15px;height:25px;
                          }
                          .color-red{
                              background: #BF0B23 !important;color: #BF0B23 !important;display:inline-block;width:15px;height:25px;
                          }
                          .color-orange{
                              background: #ffa756 !important;color: #ffa756 !important;display:inline-block;width:15px;height:25px;
                          }
                          .color-green{
                              background: #67ca3b !important;color: #67ca3b !important;display:inline-block;width:15px;height:25px;
                          }
                          @media print {
                              .graph-container .graph{
                                  width:100%;
                                  margin: auto;
                                  height:300px;
                                  display: inline-block;
                              }

                              .color-black{
                                  -webkit-print-color-adjust: exact;
                                  background: #000 !important;display:inline-block;width:15px;height:25px;
                              }
                              .color-red{
                                  -webkit-print-color-adjust: exact;
                                  background: #BF0B23 !important;display:inline-block;width:15px;height:25px;
                              }
                              .color-orange{
                                  -webkit-print-color-adjust: exact;
                                  background: #ffa756 !important;display:inline-block;width:15px;height:25px;
                              }
                              .color-green{
                                  -webkit-print-color-adjust: exact;
                                  background: #67ca3b !important;display:inline-block;width:15px;height:25px;
                              }
                          }
                          @media screen {
                              .left{
                                  display: none;
                              }
                              .print-area .footer-content .left{
                                  display: none;
                              }
                          }
                          .print-area .footer-content{
                              position:absolute;
                              bottom:0;
                              left:0;
                              right:0;
                              border-top:1px solid #333;
                          }
                          .print-area .footer-content .left{
                              float:left;
                              width:50%;
                              font-size: 14px;
                              font-weight: 400;
                          }
                          .print-area .footer-content .right{
                              float:right;
                              width:50%;
                              text-align:right;
                          }
                          .print-area .footer-content:after{
                              content : " ";
                              display : block;
                              clear:both;
                          }
                      </style>
                      <div class="report-chart-title">
                          <h3>GRAFIK HISTOGRAM HASIL PNPME BIDANG HEMATOLOGI</h3>
                      </div>
                      <div class="row">
                          <div class="col-md-12">
                              <table class="table table-information table-condensed">
                                  <tbody>
                                      <tr>
                                          <td width="150px" style="font-weight:bold;">KODE PESERTA</td>
                                          <td width="10px">:</td>
                                          <td width="40%">{{$instansi->kode_lebpes}}</td>
                                          <td width="50px" style="font-weight:bold;"><div class="color-red"></div></td>
                                          <td width="10px">:</td>
                                          <td>Tidak Memuaskan</td>
                                      </tr>
                                      <tr>
                                          <td style="font-weight:bold;">TAHUN</td>
                                          <td>:</td>
                                          <td>{{$tahun}}</td>
                                          <td><div class="color-orange"></div></td>
                                          <td>:</td>
                                          <td>Peringatan</td>
                                      </tr>
                                      <tr>
                                          <td style="font-weight:bold;">SIKLUS</td>
                                          <td>:</td>
                                          <td>{{$siklus}} - {{($type == 'a' ? '01' : '02')}}</td>
                                          <td><div class="color-green"></div></td>
                                          <td>:</td>
                                          <td>Memuaskan</td>
                                      </tr>
                                      <tr>
                                          <td><b>*</b></td>
                                          <td>:</td>
                                          <td>Posisi Peserta</td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                      </tr>
                                      <tr>
                                          <td style="font-weight:bold;">Catatan</td>
                                          <td>:</td>
                                          <td colspan="4">Grafik yang ditampilkan hanya untuk kelompok parameter yang dievaluasi dengan syarat jumlah kelompok >= 8 (Delapan)</td>
                                      </tr>
                                  </tbody>
                              </table>
                            @if($type == 'a')
                                @php $siklus2 = '1'; $lampiran = '3'; @endphp
                            @else
                                @php $siklus2 = '2'; $lampiran = '4'; @endphp
                            @endif
                            <div class="footer-content" style="position: fixed; bottom: 0;  border-top: 1px solid black; padding-top: 2px; width: 100%;">
                                <div class="left" style="font-size: 14px; font-weight: 100; font-family: arial; float: left;">
                                    GRAFIK HISTOGRAM PNPME HEMATOLOGI SIKLUS {{$siklus}} - 0{{$siklus2}} TAHUN {{$tahun}}
                                </div>
                                <div style="text-align: right; float: right;">
                                    Lampiran {{$lampiran}} -&nbsp;
                                </div>
                            </div>
                          </div>
                      </div>
                    <table width="100%">
                      @foreach($parameter as $param)
                      <tr>
                        <td width="33.3%"><div id="container{{$param->id}}"></div></td>
                        <td width="33.3%"><div id="container_alat{{$param->id}}"></div></td>
                        <td width="33.3%"><div id="container_metode{{$param->id}}"></div></td>
                      </tr>
                      @endforeach
                    </table><br>
                  </div>
                    <a style="margin: 10px" onclick="javascript:printDiv('datana')" class="btn btn-primary">Print</a>
              </div>
          </div>
      </div>
  </div>
  <script src="{{URL('/js/jquery.printarea.js')}}"></script>
  <script src="https://code.highcharts.com/modules/pattern-fill.js"></script>
  <script type="text/javascript">
  function printDiv(divID) {
      var headElements = '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>,<meta name="viewport" content="width=device-width, initial-scale=1">';
      var options = { mode : "popup", popClose : true,extraHead : headElements };
      $( '#'+divID ).printArea( options );    
  }
  @foreach($parameter as $key => $param)
    var chart1{{$param->id}} = Highcharts.chart('container{{$param->id}}', {
      title: {
        text: '{{$key+1}} - {!!$param->nama_parameter!!}<br>Z-Score Saudara = {{$param->zscore->zscore}}',
        style : {
            'font-size' : "10px",
            'font-weight' : "bold",
        }
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        categories: [
          @foreach($param->data as $val)
            '{{$val->nilainya}}',
          @endforeach],
        labels: {
          style: {
            fontSize: '7px',
            color: '#000',
          }
        }
      },
      yAxis: {
        title: {
          text : 'Jumlah Peserta',
        }
      },
      plotOptions: {
        series: {
          borderWidth: 0,dataLabels: {
            enabled: true,
            format:'{point.y} {point.a}',
            y: 0,
            crop: false,
            overflow: 'none',
            style: {
              fontSize : '10px'
            }
          }
        }
      },
      series: [{
          showInLegend: false,  
          type: 'column',
          name : 'Z-score',
          data: [
          @foreach($param->data as $val)
            {
              y: {{$val->nilai1}},
                <?php if($val->nilainya == '-2 sd 2') { ?>
                  color: '#67ca3b',
                <?php }else if($val->nilainya == '< -2 sd -3' || $val->nilainya == '> 2 sd 3') { ?>
                  color: '#ffa756',
                <?php }else if($val->nilainya == '< -3' || $val->nilainya == '> 3') { ?>
                  color: '#ca3b3b',
                <?php }else if($val->nilainya == '<= -0.01 sd -2' || $val->nilainya == '>= 0.01 sd 2') { ?>
                  color: '#67ca3b',
                <?php } ?>

              @foreach($param->datap as $valu)
                <?php if($valu->nilainya == $val->nilainya) { ?>
                  <?php if($valu->nilai1 == '1') { ?>
                    a: "*",
                  <?php } ?>
                <?php } ?>
              @endforeach
            },
          @endforeach
          ],
        }]
    });

    var chart2{{$param->id}} = Highcharts.chart('container_alat{{$param->id}}', {
      title: {
        text: '{{$key+1}} - {!!$param->nama_parameter!!} <br> Kelompok Alat {!!$param->alatna->instrumen!!} {{$param->dataReg->instrument_lain}}<br>Z-Score Saudara = {{$param->zscore_alat->zscore}}',
        style : {
          'font-size' : "10px",
          'font-weight' : "bold",
        }
      },
      subtitle: {text: ''},
      xAxis: {
        categories: [
          @foreach($param->data_alat as $val)
            '{{$val->nilainya}}',
          @endforeach],
        labels: {
          style: {
            fontSize: '7px',
            color: '#000',
          }
        }
      },
      yAxis: {
        title: {
          text : 'Jumlah Peserta'
        }
      },
      plotOptions: {
        series: {
          borderWidth: 0,dataLabels: {
            enabled: true,
            format:'{point.y} {point.a}',
            y: 0,
            crop: false,
            overflow: 'none'
          }
        }
      },
      series: [{type: 'column',
        name : 'Z-score',
        data: [
        @foreach($param->data_alat as $val)
          {
            y: {{$val->nilai1}},
              <?php if($val->nilainya == '-2 sd 2') { ?>
                color: '#67ca3b',
              <?php }else if($val->nilainya == '< -2 sd -3' || $val->nilainya == '> 2 sd 3') { ?>
                color: '#ffa756',
              <?php }else if($val->nilainya == '< -3' || $val->nilainya == '> 3') { ?>
                color: '#ca3b3b',
              <?php }else if($val->nilainya == '<= -0.01 sd -2' || $val->nilainya == '>= 0.01 sd 2') { ?>
                color: '#67ca3b',
              <?php } ?>

              @foreach($param->datap_alat as $valu)
                <?php if($valu->nilainya == $val->nilainya) { ?>
                  <?php if($valu->nilai1 == '1') { ?>
                    a: "*",
                  <?php } ?>
                <?php } ?>
              @endforeach
          },
        @endforeach
        ],
        showInLegend: false}
      ]
    });

    var chart3{{$param->id}} = Highcharts.chart('container_metode{{$param->id}}', {
      title: {
        text: '{{$key+1}} - {!!$param->nama_parameter!!} <br> Kelompok {!!$param->metodena->metode_pemeriksaan!!} {{$param->dataReg->metode_lain}}<br>Z-Score Saudara = {{$param->zscore_metode->zscore}}',
        style : {
          'font-size' : "10px",
          'font-weight' : "bold",
        }
      },
      subtitle: {text: ''},
      xAxis: {
        categories: [
          @foreach($param->data_metode as $val)
            '{{$val->nilainya}}',
          @endforeach
        ],
        labels: {
          style: {
            fontSize: '7px',
            color: '#000',
          }
        }
      },
      yAxis: {
        title: {
          text : 'Jumlah Peserta'
        }
      },
      plotOptions: {
        series: {
          borderWidth: 0,dataLabels: {
            enabled: true,
            format:'{point.y} {point.a}',
            y: 0,
            crop: false,
            overflow: 'none'
          }
        }
      },
      series: [{type: 'column',
          name : 'Z-score',
          data: [
          @foreach($param->data_metode as $val)
              {
              y: {{$val->nilai1}},
                <?php if($val->nilainya == '-2 sd 2') { ?>
                  color: '#67ca3b',
                <?php }else if($val->nilainya == '< -2 sd -3' || $val->nilainya == '> 2 sd 3') { ?>
                  color: '#ffa756',
                <?php }else if($val->nilainya == '< -3' || $val->nilainya == '> 3') { ?>
                  color: '#ca3b3b',
                <?php }else if($val->nilainya == '<= -0.01 sd -2' || $val->nilainya == '>= 0.01 sd 2') { ?>
                  color: '#67ca3b',
                <?php } ?>

              @foreach($param->datap_metode as $valu)
                <?php if($valu->nilainya == $val->nilainya) { ?>
                  <?php if($valu->nilai1 == '1') { ?>
                    a: "*",
                  <?php } ?>
                <?php } ?>
              @endforeach
              },
          @endforeach
          ],
          showInLegend: false}]
    });
  @endforeach
  </script>
</body>
</html>
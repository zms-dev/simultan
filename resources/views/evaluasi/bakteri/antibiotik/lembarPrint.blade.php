<style type="text/css">
@page {
margin: 150px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;   
    text-align: center;
}

table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 5px;
    text-align: left;
    border: 1px solid #000;
}

#header {
    position: fixed;
    border-bottom:1px solid gray;
    padding-top: -140px
}
#footer {
    position: fixed;
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
</style>
<div id="header">
    <table width="100%" cellpadding="0" border="0" >
        <thead>
            <tr>
                <th>
                   <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px">
                </th>
                <td width="100%" style="padding-left: 20px">
                    <span style="font-size: 12px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 10px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL IDENTIFIKASI BAKTERI DAN UJI KEPEKAAN ANTIBIOTIK SIKLUS {{$siklus}} TAHUN {{$years}}</b></span><br>
                    <span style="font-size: 10px">Penyelenggara : <b>Balai Besar Laboratorium Kesehatan Surabaya</b></span>
                    <div style="padding-left: 75px; margin-top: -15px; font-size: 10px">
                        <span><br/>Jalan Karangmenjangan No, 18 Surabaya - 60286<br>Telepon : (031) 5021451 | Fax : (031) 5020388<br>Web Online : simultan.bblksurabaya.id <br>Email : pme.bblksub@gmail.com</span>
                    </div>
                </td>
                <th>
                    <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" height="90px">
                </th>
            </tr>
            <tr>
                <th colspan="3"><hr></th>
            </tr>
        </thead>
    </table>
</div>

<center>
    <font size="10px">
        <b>HASIL AKHIR EVALUASI IDENTIFIKASI BAKTERI DAN UJI KEPEKAAN ANTIBIOTIK SIKLUS {{$siklus}} TAHUN {{$years}}</b><br><br>
    </font>
</center>
<div id="footer">
    EVALUASI PNPME IDENTIFIKASI BAKTERI DAN UJI KEPEKAAN ANTIBIOTIK SIKLUS {{$siklus}} TAHUN {{$years}}
</div>
<table>
    <tr>
        <td>Kode Leb Peserta</td>
        <td>:</td>
        <td>{{$register->kode_lebpes}}</td>
    </tr>
    <tr>
        <td>Nama Instansi</td>
        <td>:</td>
        <td>{{$perusahaan->nama_lab}}</td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>:</td>
        <td>{{$perusahaan->alamat}}</td>
    </tr>
</table><br>
<p style="font-weight: bold;">IDENTIFIKASI BAKTERI :</p>
<table class="utama">
    <tr>
        <th style="text-align: center;">No</th>
        <th style="text-align: center;">Kode Bahan</th>
        <th style="text-align: center;">Jenis Bahan</th>
        <th style="text-align: center;">Hasil Lab Peserta</th>
        <th style="text-align: center;">Nilai Acuan</th>
        <th style="text-align: center;">Nilai</th>
    </tr>
    <?php $no = 0; ?>
    @foreach($data as $val)
    <?php $no++; ?>
    <tr>
        <td style="text-align: center;">{{$no}}</td>
        <td>
            <?php 
                $kodebahan = substr($val->kode_lab, 0, 9).$val->kd_bahan."/".$val->siklus."/".substr($val->kode_lab, 12);
            ?>
            {{$kodebahan}}
        </td>
        <td>{{$val->jenis_bahan}}</td>
        <td>@if($val->spesies_kultur != NULL) {!!$val->spesies_kultur!!} @else {!!$val->spesies_auto!!} @endif</td>
        <td>{!!$val->rujukan->rujukan!!}</td>
        @if(count($saran))
            @foreach($evaluasi as $ev)
                @if($ev->kode_bahan == $kodebahan)
                    <td style="text-align: center;">{{$ev->nilai_1}}</td>
                @endif
            @endforeach
        @else
        @endif
    </tr>
    @endforeach
    <tr>
        <td colspan="5" style="text-align: center; font-weight: bold;">Total Nilai</td>
        <td style="text-align: center;">{{$totalnilai->nilai_1}}</td>
    </tr>
</table>
<p style="font-weight: bold;">SKOR IDENTIFIKASI :</p>
<table class="utama">
    <tr>
        <th style="text-align: center; font-size: 16px">{{$totalnilai->nilai_1}} / 12</th>
    </tr>
</table>
@if($totalnilai->nilai_1 != NULL)
<p style="font-weight: bold;">KESIMPULAN : <font style="text-decoration: underline;">{{ $saran->kesimpulan_1 }}</font></p>
@endif
<p style="font-weight: bold;">KOMENTAR DAN SARAN :</p>
<table class="utama">
    <tr>
        <th>Jenis Kesalahan</th>
        <th>Saran Tindakan</th>
    </tr>
    <tr>
        @if(count($saran))
        <td>{!! $saran->kesalahan_1 !!}</td>
        <td>{!! $saran->tindakan_1 !!}</td>
        @else
        @endif
    </tr>
</table><br>
<p style="font-weight: bold;">UJI KEPEKAAN ANTIBIOTIK :</p>
<table class="utama">
    <tr>
        <th style="text-align: center;">No</th>
        <th style="text-align: center;">Kode Bahan</th>
        <th style="text-align: center;">Jenis Bahan</th>
        <th style="text-align: center;">Hasil Lab Peserta</th>
        <th style="text-align: center;">Nilai Acuan</th>
        <th style="text-align: center;">Nilai</th>
    </tr>
    <?php $no = 0; ?>
    @foreach($data as $val)
    <?php $no++; ?>
    <tr>
        <td style="text-align: center;">{{$no}}</td>
        <td>
            <?php 
                $kodebahan = substr($val->kode_lab, 0, 9).$val->kd_bahan."/".$val->siklus."/".substr($val->kode_lab, 12);
            ?>
            {{$kodebahan}}
        </td>
        <td>{{$val->jenis_bahan}}</td>
        <td  style="vertical-align: center">
            <ol>
            <?php $mrs = 0; ?>
            @foreach($val->kepekaan as $kepekaan)
                <li>
                    {{$kepekaan->antibiotik}}
                    @if($kepekaan->lain_lain != NULL)
                    ({{$kepekaan->lain_lain}})
                    @endif
                    @if($kepekaan->kesimpulan != NULL)
                        ({{$kepekaan->kesimpulan}})
                    @elseif($kepekaan->hasil1 != NULL)
                        ({{$kepekaan->hasil1}})
                    @elseif($kepekaan->hasil2 != NULL)
                        ({{$kepekaan->hasil2}})
                    @endif
                </li>
            <?php $mrs++; ?>
            @endforeach
            @if($val->resistensi == NULL || $val->resistensi == '')
                @if($val->mrsa != NULL && $mrs != 0)
                <li>
                    Golongan MRSA : {{$val->mrsa}}
                </li>
                @endif
                @if($val->esbl != NULL && $mrs != 0)
                <li>
                    Golongan ESBL : {{$val->esbl}}
                </li>
                @endif
            @else
                <li>Resistensi : 
                    @if($val->resistensi_lain != '')
                        {{$val->resistensi_lain}}
                    @else
                        {{$val->name}}
                    @endif
                </li>
            @endif
            </ol>
        </td>
        @if(count($saran))
            @foreach($evaluasi as $ev)
                @if($ev->kode_bahan == $kodebahan)
                    <td style="vertical-align: center">{!!$ev->acuan!!}</td>
                    <td style="text-align: center;">{{$ev->nilai_2}}</td>
                @endif
            @endforeach
        @else
        @endif
    </tr>
    @endforeach
    <tr>
        <td colspan="5" style="text-align: center; font-weight: bold;">Total Nilai</td>
        <td style="text-align: center;">{{$totalnilai->nilai_2}}</td>
    </tr>
</table>
<p style="font-weight: bold;">SKOR UJI KEPEKAAN :</p>
<table class="utama">
    <tr>
        <th style="text-align: center; font-size: 16px">{{$totalnilai->nilai_2}} / 12</th>
    </tr>
</table>
@if($totalnilai->nilai_2 != NULL)
<p style="font-weight: bold;">KESIMPULAN : <font style="text-decoration: underline;">{{ $saran->kesimpulan_2 }}</font></p>
@endif
<p style="font-weight: bold;">KOMENTAR DAN SARAN :</p>
<table class="utama">
    <tr>
        <th>Jenis Kesalahan</th>
        <th>Saran Tindakan</th>
    </tr>
    <tr>
        @if(count($saran))
        <td>{!! $saran->kesalahan_2 !!}</td>
        <td>{!! $saran->tindakan_2 !!}</td>
        @else
        @endif
    </tr>
</table>
<br>
<div style="margin-left: 550px; margin-top: -1%;">
<div style="position: relative; top: 17px">
Surabaya, {{$ttd->tanggal}} {{$years}}<br>
Manajer Teknis
</div>
<p>
    <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.'ttdevaluasi'.DIRECTORY_SEPARATOR.$ttd->image)}}" width="80" height="80" style="margin-left: 20px !important;">
</p>

<div style="position: relative; top:-20px">
    {{$ttd->nama}}<br>NIP. {{$ttd->nip}}
</div><br>
</div>
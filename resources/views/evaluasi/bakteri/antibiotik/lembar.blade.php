@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<script src="//cdn.ckeditor.com/4.10.1/basic/ckeditor.js"></script>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Uji Kepekaan Antibiotik</div>
                <div class="panel-body">
                   @if(Session::has('message'))
                    <div class="alert alert-success" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      {{ Session::get('message') }}
                    </div>
                  @endif
                    <form class="form-horizontal"  method="post" enctype="multipart/form-data">
                        <table>
                            <tr>
                                <td>Kode Leb Peserta</td>
                                <td>&nbsp;:&nbsp;</td>
                                <td>{{$register->kode_lebpes}}</td>
                            </tr>
                            <tr>
                                <td>Nama Instansi</td>
                                <td>&nbsp;:&nbsp;</td>
                                <td>{{$perusahaan->nama_lab}}</td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>&nbsp;:&nbsp;</td>
                                <td>{{$perusahaan->alamat}}</td>
                            </tr>
                        </table><br>
                        <p style="font-weight: bold;">IDENTIFIKASI BAKTERI :</p>
                        <table class="table table-bordered">
                            <tr>
                                <th style="text-align: center;">No</th>
                                <th style="text-align: center;">Kode Bahan</th>
                                <th style="text-align: center;">Jenis Bahan</th>
                                <th style="text-align: center;">Hasil Lab Peserta</th>
                                <th style="text-align: center;">Nilai Acuan</th>
                                <th style="text-align: center;">Nilai</th>
                            </tr>
                            <?php $no = 0; ?>
                            @foreach($data as $val)
                            <?php $no++; ?>
                            <tr>
                                <td style="text-align: center;">{{$no}}</td>
                                <td>
                                    <?php 
                                        $kodebahan = substr($val->kode_lab, 0, 9).$val->kd_bahan."/".$val->siklus."/".substr($val->kode_lab, 12);
                                    ?>
                                    {{$kodebahan}}
                                    <input type="hidden" name="kode_bahan[]" value="{{$kodebahan}}">
                                </td>
                                <td>{{$val->jenis_bahan}}</td>
                                <td>@if($val->spesies_kultur != NULL) {!!$val->spesies_kultur!!} @else {!!$val->spesies_auto!!} @endif</td>
                                <td>
                                    @if($val->Iden != NULL)
                                        {!!$val->Iden->rujukan!!}
                                    @endif
                                </td>
                                @if(count($evaluasi))
                                    @foreach($evaluasi as $ev)
                                        @if($ev->kode_bahan == $kodebahan)
                                            <input type="hidden" name="id[]" value="{{$ev->id}}">
                                            <td><input type="text" name="nilai_1[]" value="{{$ev->nilai_1}}" required style="width: 50px"></td>
                                        @endif
                                    @endforeach
                                @else
                                <td><input type="text" name="nilai_1[]" required style="width: 50px"></td>
                                @endif
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="5" style="text-align: center; font-weight: bold;">Total Nilai</td>
                                <td>{{$totalnilai->nilai_1}}</td>
                            </tr>
                        </table>
                        <p style="font-weight: bold;">KESIMPULAN : 
                            <select name="kesimpulan_1">
                                @if($totalnilai->nilai_1 != NULL || $totalnilai->nilai_1 == 0 )
                                <option value="{{$saran->kesimpulan_1}}">{{$saran->kesimpulan_1}}</option>
                                @endif
                                <option></option>
                                <option value="BAIK">BAIK</option>
                                <option value="KURANG">KURANG</option>
                            </select>
                        </p>
                        <p style="font-weight: bold;">KOMENTAR DAN SARAN :</p>
                        <table class="table table-bordered">
                            <tr>
                                <th>Jenis Kesalahan</th>
                                <th>Saran Tindakan</th>
                            </tr>
                            <tr>
                                @if(count($saran))
                                <td><textarea id="ckeditorkesalahan1" name="kesalahan_1" style="width: 100%">{{$saran->kesalahan_1}}</textarea></td>
                                <td><textarea id="ckeditortindakan1" name="tindakan_1" style="width: 100%">{{$saran->tindakan_1}}</textarea></td>
                                @else
                                <td><textarea id="ckeditorkesalahan1" name="kesalahan_1" style="width: 100%"></textarea></td>
                                <td><textarea id="ckeditortindakan1" name="tindakan_1" style="width: 100%"></textarea></td>
                                @endif
                            </tr>
                        </table>
                        <p style="font-weight: bold;">UJI KEPEKAAN ANTIBIOTIK :</p>
                        <table class="table table-bordered">
                            <tr>
                                <th style="text-align: center;">No</th>
                                <th style="text-align: center;">Kode Bahan</th>
                                <th style="text-align: center;">Jenis Bahan</th>
                                <th style="text-align: center;">Hasil Lab Peserta</th>
                                <th style="text-align: center;">Nilai Acuan</th>
                                <th style="text-align: center;">Nilai</th>
                            </tr>
                            <?php $no = 0; ?>
                            @foreach($data as $val)
                            <?php $no++; ?>
                            <tr>
                                <td style="text-align: center;">{{$no}}</td>
                                <td>
                                    <?php 
                                        $kodebahan = substr($val->kode_lab, 0, 9).$val->kd_bahan."/".$val->siklus."/".substr($val->kode_lab, 12);
                                    ?>
                                    {{$kodebahan}}
                                </td>
                                <td>{{$val->jenis_bahan}}</td>
                                <td>
                                    <table width="100%">
                                    <?php $mrs = 0; ?>
                                    @foreach($val->kepekaan as $kepekaan)
                                        <tr>
                                            <td>
                                                {{$kepekaan->antibiotik}}
                                                @if($kepekaan->lain_lain != NULL)
                                                ({{$kepekaan->lain_lain}})
                                                @endif
                                                <br>
                                            </td>
                                            <td>
                                                <div style="float: right;">
                                                    @if($kepekaan->kesimpulan != NULL)
                                                        ({{$kepekaan->kesimpulan}})
                                                    @elseif($kepekaan->hasil1 != NULL)
                                                        ({{$kepekaan->hasil1}})
                                                    @elseif($kepekaan->hasil2 != NULL)
                                                        ({{$kepekaan->hasil2}})
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $mrs++; ?>
                                    @endforeach
                                    @if($val->resistensi == NULL || $val->resistensi == '')
                                        @if($val->mrsa != NULL && $mrs != 0)
                                        <tr>
                                            <td>Golongan MRSA</td>
                                            <td>
                                                {{$val->mrsa}}
                                            </td>
                                        </tr>
                                        @endif
                                        @if($val->esbl != NULL && $mrs != 0)
                                        <tr>
                                            <td>Golongan ESBL</td>
                                            <td>
                                                {{$val->esbl}}
                                            </td>
                                        </tr>
                                        @endif
                                    @else
                                    <tr>
                                        <td>Resistensi</td>
                                        <td>
                                            @if($val->resistensi_lain != '')
                                                {{$val->resistensi_lain}}
                                            @else
                                                {{$val->name}}
                                            @endif
                                        </td>
                                    </tr>
                                    @endif
                                    </table>
                                </td>
                                @if(count($evaluasi))
                                    @foreach($evaluasi as $ev)
                                        @if($ev->kode_bahan == $kodebahan)
                                            <td><textarea name="acuan[]" id="ckeditor{{$no}}">{{$ev->acuan}}</textarea></td>
                                            <td><input type="text" name="nilai_2[]" value="{{$ev->nilai_2}}" required style="width: 50px"></td>
                                        @endif
                                    @endforeach
                                @else
                                <td>
                                    <textarea name="acuan[]" id="ckeditor{{$no}}">
                                        @if($val->Anti != NULL)
                                            {!!$val->Anti->rujukan!!}
                                        @endif    
                                    </textarea>
                                </td>
                                <td><input type="text" name="nilai_2[]" required style="width: 50px"></td>
                                @endif
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="5" style="text-align: center; font-weight: bold;">Total Nilai</td>
                                <td>{{$totalnilai->nilai_2}}</td>
                            </tr>
                        </table>
                        <p style="font-weight: bold;">KESIMPULAN : 
                            <select name="kesimpulan_2">
                                @if($totalnilai->nilai_2 != NULL || $totalnilai->nilai_2 == 0 )
                                <option value="{{$saran->kesimpulan_2}}">{{$saran->kesimpulan_2}}</option>
                                @endif
                                <option></option>
                                <option value="BAIK">BAIK</option>
                                <option value="KURANG">KURANG</option>
                            </select>
                        </p>
                        <p style="font-weight: bold;">KOMENTAR DAN SARAN :</p>
                        <table class="table table-bordered">
                            <tr>
                                <th>Jenis Kesalahan</th>
                                <th>Saran Tindakan</th>
                            </tr>
                            <tr>
                                @if(count($saran))
                                <td><textarea id="ckeditorkesalahan2" name="kesalahan_2" style="width: 100%">{{$saran->kesalahan_2}}</textarea></td>
                                <td><textarea id="ckeditortindakan2" name="tindakan_2" style="width: 100%">{{$saran->tindakan_2}}</textarea></td>
                                @else
                                <td><textarea id="ckeditorkesalahan2" name="kesalahan_2" style="width: 100%"></textarea></td>
                                <td><textarea id="ckeditortindakan2" name="tindakan_2" style="width: 100%"></textarea></td>
                                @endif
                            </tr>
                        </table>
                        {{ csrf_field() }}
                        @if(count($evaluasi))
                            <input type="submit" name="simpan" class="btn btn-primary" style="" value="Update">
                            <a href="{{url('evaluasi/antibiotik-lembar/print').'/'.$id.'?y='.$siklus}}">
                                <input type="button" class="btn btn-primary" value="Print" name="print"/>
                            </a>
                        @else
                            <input type="submit" name="simpan" class="btn btn-primary" style="" value="Simpan">
                        @endif
                    </form>
                </div>
            </section>
        </div>
    </div>
  </section>
</section>
@endsection
@section('scriptBlock')
<script type="text/javascript">
    $('#year').datepicker({
        format: 'yyyy',
        viewMode: "years", //this
        minViewMode: "years",//and this
        autoclose : true,
    });

    CKEDITOR.replace( 'ckeditor1',
            {
                toolbar : 'Basic', /* this does the magic */
                enterMode : CKEDITOR.ENTER_BR,
                shiftEnterMode : CKEDITOR.ENTER_P
            });
    CKEDITOR.replace( 'ckeditor2',
            {
                toolbar : 'Basic', /* this does the magic */
                enterMode : CKEDITOR.ENTER_BR,
                shiftEnterMode : CKEDITOR.ENTER_P
            });
    CKEDITOR.replace( 'ckeditor3',
            {
                toolbar : 'Basic', /* this does the magic */
                enterMode : CKEDITOR.ENTER_BR,
                shiftEnterMode : CKEDITOR.ENTER_P
            });

    CKEDITOR.replace( 'ckeditorkesalahan1',
            {
                toolbar : 'Basic', /* this does the magic */
                enterMode : CKEDITOR.ENTER_BR,
                shiftEnterMode : CKEDITOR.ENTER_P
            });

    CKEDITOR.replace( 'ckeditortindakan1',
            {
                toolbar : 'Basic', /* this does the magic */
                enterMode : CKEDITOR.ENTER_BR,
                shiftEnterMode : CKEDITOR.ENTER_P
            });

    CKEDITOR.replace( 'ckeditorkesalahan2',
            {
                toolbar : 'Basic', /* this does the magic */
                enterMode : CKEDITOR.ENTER_BR,
                shiftEnterMode : CKEDITOR.ENTER_P
            });

    CKEDITOR.replace( 'ckeditortindakan2',
            {
                toolbar : 'Basic', /* this does the magic */
                enterMode : CKEDITOR.ENTER_BR,
                shiftEnterMode : CKEDITOR.ENTER_P
            });
</script>
@stop
<style type="text/css">
@page {
margin: 100px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 14px;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 2px 5px;
}

tr th{
    text-align: center;
}

tr.border_bottom td{
  border-bottom:1pt solid black;
}
</style>
<?php $no = 1; $lembar = 1; $page = 1?>
@foreach($data as $val)
@if($no == 1)
<table border="1">
    <tr>
        <th>No</th>
        <th>Kode Bahan</th>
        <th>Jenis Bahan</th>
        <th>Hasil Lab Peserta</th>
        <th>Nilai Acuan</th>
        <th>Nilai</th>
        <th>Skor Identifikasi</th>
        <th>Kesimpulan</th>
    </tr>
@endif
@if($lembar == 1 && $page == 10)
</table>
<table border="1" style="page-break-before: always;">
    <tr>
        <th>No</th>
        <th>Kode Bahan</th>
        <th>Jenis Bahan</th>
        <th>Hasil Lab Peserta</th>
        <th>Nilai Acuan</th>
        <th>Nilai</th>
        <th>Skor Identifikasi</th>
        <th>Kesimpulan</th>
    </tr>
@endif
    <tr>
        @if($lembar == 1)
        <td rowspan="3">{{$no++}}</td>
        @endif
        <td>{{$val->kode_bahan}}</td>
        @if($type == 0)
            @foreach($val->datana as $datana)
                @if($datana->lembar == $lembar)
                    <td>{{$datana->jenis_bahan}}</td>
                    <td>{!! $datana->spesies_kultur!!}</td>
                    <td>{!! $datana->rujukan!!}</td>
                @endif
            @endforeach
            <td>{{$val->nilai_1}}</td>
        @else
            @foreach($val->datana as $datana)
                @if($datana->lembar == $lembar)
                    <td>{{$datana->jenis_bahan}}</td>
                    <td style="vertical-align: center">
                        <ol>
                            @foreach($datana->kepekaan as $kepekaan)
                                <li>
                                    {{$kepekaan->antibiotik}}
                                    @if($kepekaan->lain_lain != NULL)
                                    ({{$kepekaan->lain_lain}})
                                    @endif
                                    <div style="float: right;">
                                        @if($kepekaan->hasil1 != NULL)({{$kepekaan->hasil1}})@endif
                                        @if($kepekaan->hasil2 != NULL)({{$kepekaan->hasil2}})@endif
                                    </div>
                                </li>
                            @endforeach
                        </ol>
                    </td>
                @endif
            @endforeach
            <td style="vertical-align: center">{!! $val->acuan!!}</td>
            <td>{{$val->nilai_2}}</td>
        @endif
        @if($lembar == 1)
        <td rowspan="3"><center>{{$val->total->total}}/12</center></td>
        <td rowspan="3"><center>{{$val->kesimpulan_1}}</center></td>
        @endif
    </tr>
<?php 
    $lembar++; 
    if($lembar > 3){ 
        $lembar=1;
        if ($page == 10) {
            $page = 1;
        }else{
            $page++;
        }
    } 
?>
@endforeach
</table>
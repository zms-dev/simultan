<?php $no = 1; $lembar = 1; ?>
@foreach($data as $val)
@if($lembar == 1)
<table border="1">
    <tr>
        <th>No</th>
        <th>Kode Bahan</th>
        <th>Perusahaan</th>
        <th>Kode Lab</th>
        <th>Jenis Bahan</th>
        <th>Hasil Lab Peserta</th>
        <th>Nilai Acuan</th>
        <th>Nilai</th>
        <th>Skor Identifikasi</th>
        <th>Kesimpulan</th>
    </tr>
@endif
    <tr>
        @if($lembar == 1)
        <td rowspan="3">{{$no++}}</td>
        @else
        <td></td>
        @endif
        <td>{{$val->kode_bahan}}</td>
        <td>{{$val->nama_lab}}</td>
        <td>{{$val->kode_lebpes}}</td>
        @if($type == 0)
            @foreach($val->datana as $datana)
                @if($datana->lembar == $lembar)
                    <td>{{$datana->jenis_bahan}}</td>
                    <td>{!! $datana->spesies_kultur!!}</td>
                    <td>{!! $datana->rujukan!!}</td>
                @endif
            @endforeach
            <td>{{$val->nilai_1}}</td>
        @else
            @foreach($val->datana as $datana)
                @if($datana->lembar == $lembar)
                    <td>{{$datana->jenis_bahan}}</td>
                    <td style="vertical-align: center">
                        <ol>
                            @foreach($datana->kepekaan as $kepekaan)
                                <li>
                                    {{$kepekaan->antibiotik}}
                                    @if($kepekaan->lain_lain != NULL)
                                    ({{$kepekaan->lain_lain}})
                                    @endif
                                    <div style="float: right;">
                                        @if($kepekaan->hasil1 != NULL)({{$kepekaan->hasil1}})@endif
                                        @if($kepekaan->hasil2 != NULL)({{$kepekaan->hasil2}})@endif
                                    </div>
                                </li>
                            @endforeach
                        </ol>
                    </td>
                @endif
            @endforeach
            <td style="vertical-align: center">{!! $val->acuan!!}</td>
            <td>{{$val->nilai_2}}</td>
        @endif
        @if($lembar == 1)
        <td rowspan="3"><center>{{$val->total->total}}/12</center></td>
        <td rowspan="3"><center>{{$val->kesimpulan_2}}</center></td>
        @else
        <td></td>
        <td></td>
        @endif
    </tr>
@if($lembar == 3)
</table>
@endif
<?php $lembar++; if($lembar > 3){ $lembar=1;} ?>
@endforeach
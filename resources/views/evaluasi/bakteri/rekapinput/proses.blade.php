<table border="1">
	<tr>
		<th>No</th>
		<th>Nama Instansi</th>
		<th>Nama Pemeriksa</th>
		<th>Kode Bahan</th>
		<th>Siklus</th>
		<th>Jenis Bahan</th>
		<th>Tanggal Pelaksanaan</th>
		<th>Metode Identifikasi Bakteri</th>
		<th>Media yang Digunakan</th>
		<th>Buatan Sendiri</th>
		<th>Media Komersial</th>
		<th>Hasil Kultur</th>
		<th>Spesies Kultur</th>
		<th>Pewarnaan Gram</th>
		<th>Kebutuhan Oksidasi</th>
		@foreach($fermentasi as $fern)
		@if($fern->gram == 'negatif')
		<th>{{$fern->fermentasi}}</th>
		@endif
		@endforeach
		<th>Lain-lain</th>
		<th>Hemolisa</th>
		<th>Kebutuhan terhadap faktor X + V</th>
		<th>Kebutuhan Oksigen</th>
		@foreach($fermentasi as $ferp)
		@if($ferp->gram == 'positif')
		<th>{{$ferp->fermentasi}}</th>
		@endif
		@endforeach
		<th>Lain-lain</th>
		<th>UJI KEPEKAAN ANTIBIOTIK</th>
		<th>Hasil Identifikasi</th>
		<th>Standart</th>
		<th>Metode / Automatisasi</th>
		@foreach($antibiotik as $anti)
		<th>{{$anti->antibiotik}}</th>
		@endforeach
		<th>Resistensi</th>
	</tr>
	<?php $no = 0; ?>
	@foreach($data as $val)
	<tr>
		<td>{{$no+1}}</td>
		<td>{{$val->nama_instansi}}</td>
		<td>{{$val->nama_pemeriksa}}</td>
		<td>{{$val->kd_bahan}}</td>
		<td>{{$val->siklus}}</td>
		<td>{{$val->jenis_bahan}}</td>
		<td>{{$val->tgl_pelaksanaan}}</td>
		<td>
			@if($val->metode_konvensional != null ) Metode Konvensional
			@elseif($val->metode_otomatis != null) Metode Otomatis : {{$val->test_metode_identifikasi}}
			@else Metode Lainnya : {{$val->test_metode_identifikasi}}
			@endif
		</td>
		<td>{{$val->media_digunakan}}</td>
		<td>@if($val->buatan_sendiri == "v") V @endif</td>
		<td>{{$val->media_komersial}}</td>
		<td>
			@if($val->hasil_kultur == "ada") Terdapat pertumbuhan bakteri patogen @else Tidak terdapat pertumbuhan bakteri patogen @endif
		</td>
		<td>{!! $val->spesies_kultur !!}</td>
		<td>
			@if($val->pewarnaan_gram_p != NULL)
				Gram Positif : {{$val->pewarnaan_gram_p}}
			@elseif($val->pewarnaan_gram_n != NULL)
				Gram Negatif : {{$val->pewarnaan_gram_n}}
			@elseif($val->pewarnaan_gram_y != NULL)
				Yeast : {{$val->pewarnaan_gram_y}}
			@endif
		</td>
		<td>{{$val->kebutuhan_oksidasi}}</td>
		@foreach($val->fermentasinegatif as $valfn)
			<td>@if($valfn->status != "Tanpa Test") {{$valfn->status}} @else - @endif</td>
		@endforeach
		<td>{{$val->fermentasilainn}}</td>
		<td>{{$val->hemolisa}}</td>
		<td>{{$val->faktorxv}}</td>
		<td>{{$val->kebutuhan_oksigen}}</td>
		@foreach($val->fermentasipositif as $valfp)
			<td>@if($valfp->status != "Tanpa Test") {{$valfp->status}} @else - @endif</td>
		@endforeach
		<td>{{$val->fermentasilainp}}</td>
		<td></td>
		<td>{{$val->peka->hasil_identifikasi}}</td>
		<td>{{$val->peka->standart}}</td>
		<td>{{$val->peka->metode}}</td>
		<?php $nopk = 1 ?>
		@foreach($val->kepekaan as $kepeka)
		<td>
			@if($kepeka->lain_lain != NULL) {{$kepeka->lain_lain}}, @endif
			@if($kepeka->disk != NULL)Disk difusi : {{$kepeka->disk}}, @endif 
			@if($kepeka->hasil1 != NULL)Interpretasi hasil : {{$kepeka->hasil1}}, @endif
			@if($kepeka->mic != NULL) M.I.C : {{$kepeka->mic}}, @endif
			@if($kepeka->hasil2 != NULL) Interpretasi hasil : {{$kepeka->hasil2}}, @endif
			@if($kepeka->kesimpulan) Kesimpulan : {{$kepeka->kesimpulan}} @endif
		</td>
		<?php $nopk++;?>
		@endforeach
		<?php for ($i=$nopk; $i <= 37; $i++) { ?>
			<td></td>
		<?php } ?>
		<td>
			@if($val->mrsa != "")
				MRSA : {{$val->mrsa}}
			@elseif($val->esbl != "")
				ESBL : {{$val->esbl}}
			@elseif($val->name_resisten != "")
				{{$val->name_resisten}} @if($val->resistensi_lain != "") : {{$val->resistensi_lain}} @endif
			@endif
		</td>
	</tr>
	<?php $no++;?>
	@endforeach
</table>
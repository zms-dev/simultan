<style type="text/css">
@page {
margin: 150px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 5px;
    text-align: left;
    border: 1px solid #ddd;
}

#header { 
    position: fixed; 
    border-bottom:1px solid gray;
    padding-top: -140px
}
#footer { 
    position: fixed; 
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
</style>
    <table width="100%" cellpadding="0" border="" style="margin-top: -14%;">
    <thead>
        <tr>
            <th>
                 <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px"> 
            </th>
            <th width="2%;"></th>
            <td width="100%">
                <span style="font-size: 16px"><b>KEMENTERIAN KESEHATAN R.I.</b></span><br>
                <span style="font-size: 16px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL MIKROBIOLOGI BAKTERI IDENTIFIKASI SIKLUS {{$siklus}} TAHUN {{$years}}</b></span><br>
                <span style="font-size: 15px;">Penyelenggara :</span>
                <span style="font-size: 17px;"> 
                    <b>Balai Besar Laboratorium Kesehatan Surabaya</b>
                </span>
                <pre style="font-family: Helvetica;font-size: 12px; padding: -25px; margin-left: -18px; margin-top: 10px;"> 
                                                Jl.Karangmenjangan No. 18 Surabaya 60286
                                                Telepon : 031-5021451 Fax.031-5020388
                                                Email : pme.bblksub@gmail.com / mikrobblksub@gmail.com
                </pre>
            </td>
        </tr>
        <tr>
            <th colspan="4" style="padding: 5px;"></th>
        </tr>
        <tr>
            <th colspan="4" style=""><hr></th>
        </tr>
          <tr>
            <th colspan="4" style="padding: 1px;"></th>
        </tr>
     
    </thead>
</table>

<center><label style="font-size: 14px;"><b>LAMPIRAN EVALUASI PESERTA<br>
PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL MIKROBIOLOGI<br> MIKROSKOPIS BAKTERI IDENTIFIKASI SIKLUS {{$siklus}} TAHUN {{$years}}
</b><input type="hidden" name="type" value="{{$siklus}}"></label></center><br>
<form class="form-horizontal" method="post" enctype="multipart/form-data">
<br>

<table style="margin-left: 15%; font-size: 13px;">
    <tr>
        <td>Sifat</td>
        <td>:</td>
        <td><b>RAHASIA</b></td>
    </tr>
    <tr>
        <td>Kode Lab Peserta</td>
        <td>:</td>
        <td>{{ substr($register->kode_lebpes,1,-6) }}</td>
    </tr>
    <tr>
        <td>Nama Instansi</td>
        <td>:</td>
        <td>{{ $register->perusahaan->nama_lab }}</td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>:</td>
        <td>{{ $register->perusahaan->alamat }}</td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>Hasil Pemeriksaan </td>
        <td>:</td>
    </tr>
</table>
<br>
<table class="table utama" style="font-size: 13px;">
                    <thead>
                        <tr class="titlerowna">
                            <th>Kode Soal</th>
                            <th>Jenis Pemeriksaan</th>
                            <th>Nilai Rujukan</th>
                            <th>Jawaban Peserta</th>
                            <th>Skor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 0; $i=0;?>
                        @foreach($data as $datas)
                        <?php $no++; ?>  
                        <tr>
                     
                            <td class="" rowspan="">0{{ $datas->lembar }}</td>
                            <td>{{ $datas->jenis_pemeriksaan }}</td>
                            <td>{!! $datas->rujukan !!}</td>
                            @if($datas->jenis_pemeriksaan == "Identifikasi")
                            <td>{!! $datas->spesies_auto !!}</td>
                            @elseif($datas->jenis_pemeriksaan== "Uji Kepekaan Antibiotik")
                            <td>{!! $datas->spesies_kultur !!}</td>
                            @endif
                            <td >
                                    @if(count($evaluasi))
                                        <?php $skor = 0; ?>
                                        @if(!empty($evaluasi[$i]->id_data_antibiotik))
                                            @if($evaluasi[$i]->id_data_antibiotik == $datas->id)
                                            {{$evaluasi[$i]->skor}}
                                            @endif
                                        @endif
                                    @endif
                            </td>
                        </tr>
                        <?php
                         $i++;?>
                        @endforeach

                    </tbody>
                </table>
<br>
<div style="font-size: 13px;"><b>Keterangan :</b> &nbsp;{{$status->keterangan}}<br></div>
<br>
<div style="margin-left: 552px;">
<div style="position: relative; top: 17px">
Surabaya,  @if($siklus == 1)31 Juli @else 10 Desember @endif {{$years}}<br>
Manajer Teknis
</div>
<p>
    <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'TTD4.png')}}" width="80" height="80" style="margin-left: 20px !important;">
</p>

<div style="position: relative; top:-20px;font-size: 13px;">
Ita Andayani, S.St<br>
NIP.197004101992032003
</div>
</div><br>


<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>



    
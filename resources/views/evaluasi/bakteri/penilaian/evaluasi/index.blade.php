@extends('layouts.app')

@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluasi Bakteri Identifikasi</div>

                <div class="panel-body">
                @if(count($evaluasi))
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{url('hasil-pemeriksaan/antibiotik/evaluasi')}}/print/{{$id}}?y={{$siklus}}" >
                @else
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                @endif

                <table class="table table-bordered"  id="example">
                    <thead>
                        <tr class="titlerowna">
                            <th>Kode Soal</th>
                            <th>Jenis Pemeriksaan</th>
                            <th>Nilai Rujukan</th>
                            <th>Jawaban Peserta</th>
                            <th>Skor</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($evaluasi as $val)
                            <input type="hidden"  name="id[]" value="{{ $val->id }}">
                        @endforeach
                        <?php $no = 0; $i=0;?>
                        @foreach($data as $datas)
                        <?php $no++; ?>  
                        <tr>
                            <input type="hidden"  name="kode_soal[]" value="{{$datas->lembar}}">
                            <input type="hidden"  name="id_data_antibiotik[]" value="{{$datas->id}}">
                            <input type="hidden"  name="jenis_pemeriksaan[]" value="{{ $datas->jenis_pemeriksaan }}" >
                            <input type="hidden"  name="rujukan[]" value="{{$datas->rujukan}}">
                            <input type="hidden"  name="jawaban_peserta1[]" value="{{ $datas->spesies_auto }}">
                            <input type="hidden"  name="jawaban_peserta2[]" value="{{ $datas->spesies_kultur }}">

                            <td class="" rowspan="">0{{ $datas->lembar }}</td>
                            <td>{{ $datas->jenis_pemeriksaan }}</td>
                            <td>{!! $datas->rujukan !!}</td>
                            @if($datas->jenis_pemeriksaan == "Identifikasi")
                            <td>{!! $datas->spesies_auto !!}</td>
                            @elseif($datas->jenis_pemeriksaan== "Uji Kepekaan Antibiotik")
                            <td>{!! $datas->spesies_kultur !!}</td>
                            @endif
                            <td >
                                <select name="skor[]" class="form-control " style="width: 100%">

                                    @if(count($evaluasi))
                                        <?php $skor = 0; ?>
                                        @if(!empty($evaluasi[$i]->id_data_antibiotik))
                                            @if($evaluasi[$i]->id_data_antibiotik == $datas->id)
                                            <option value="{{$evaluasi[$i]->skor}}">{{$evaluasi[$i]->skor}}</option>
                                            @endif
                                        @endif
                                    @endif
                                    <option  value="0">0</option>   
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                            </td>
                        </tr>
                        <?php
                         $i++;?>
                        @endforeach

                    </tbody>
                </table>
            {{--  --}}
              
                <div class="row"> 
                    <div class="col-md-6">
                     <table  class="table table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th>Keterangan</th>               
                            </tr>    
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    
                                    <select class="form-control" required="" name="keterangan">
                                        @if(count($status))
                                        <option value="{{$status->keterangan}}">{{$status->keterangan}}</option>
                                        @else
                                        <option></option>
                                        @endif
                                        <option value="Lulus">Lulus</option>
                                        <option value="Tidak Lulus">Tidak Lulus</option>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>   
                    </div>
                    <div class="col-md-6 col-xs-12" style="text-align: right; ">
                         
                        @if(count($evaluasi))
                        <input type="submit" name="simpan" class="btn btn-success" value="Print">
                        <input type="submit" name="simpan" class="btn btn-primary" value="Update">
                        @else
                        <input type="submit" name="simpan" class="btn btn-primary" value="Simpan">
                        @endif
                        {{ csrf_field() }}
        
                    </div>
                </div>
                </form>
                
                <table  style="margin-top: 20px;">
                    <tr>
                        <td width="">Skor Penilain</td>
                        <td width="">:</td>
                        
                    </tr>
                    <tr>
                        <td>Skor Identifikasi</td>
                        <td width="8%" class="text-right">:&nbsp;</td>
                        <td>Jumlah skor yang diperoleh  / jumlah skor sempurna</td>
                    </tr>

                    
                </table>

                <table  style=""  >
                    <tr>
                        <td width=26%">&nbsp;</td>
                        <td width="11%" class="text-right">:</td>
                        <td width="50%">&nbsp;Jumlah skor yang diperoleh</td>
                        <td>&nbsp;&nbsp;=&nbsp;</td>
                        <td> @if(count($evaluasi) < 1)
                             0
                            @else
                            @foreach($identifikasi as $iden)
                                {{$iden->skor}}
                            @endforeach
                            @endif
                        </td>
                    </tr>
                     <tr>
                        <td width=""></td>
                        <td width="" class="text-right">: </td>
                        <td>&nbsp;Jumlah skor sempurna</td>
                        <td>&nbsp;&nbsp;=&nbsp;</td>
                        <td>@if(count($evaluasi) < 1)
                                0
                            @else
                                12
                            @endif
                        </td>
                    </tr>
                     <tr>
                        <td width=""></td>
                        <td width="" class="text-right">: </td>
                        <td>&nbsp;Skor Identifikasi</td>
                        <td>&nbsp;&nbsp;=&nbsp;</td>
                        <td>
                            @if(count($evaluasi) < 1)
                             0
                            @else
                            @foreach($identifikasi as $iden)
                                {{$iden->skor}}/12
                            @endforeach
                            @endif
                        </td>
                    </tr>
                    
                    
                </table>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scriptBlock')
<script type="text/javascript">
$(function() {
     // clone table to display "before"   
    var beforeTable = $('#example').clone().removeAttr('id').appendTo('#before')
    // code for grouping in "after" table
    var $rows = $('#example tbody tr');
    var items = [],
        itemtext = [],
        currGroupStartIdx = 0;
    $rows.each(function(i) {
        var $this = $(this);
        var itemCell = $(this).find('td:eq(0)')
        var item = itemCell.text();
        itemCell.remove();
        if ($.inArray(item, itemtext) === -1) {
            itemtext.push(item);
            items.push([i, item]);
            groupRowSpan = 1;
            currGroupStartIdx = i;
            $this.data('rowspan', 1)
        } else {
            var rowspan = $rows.eq(currGroupStartIdx).data('rowspan') + 1;
            $rows.eq(currGroupStartIdx).data('rowspan', rowspan);
        }

    });
    $.each(items, function(i) {
        var $row = $rows.eq(this[0]);
        var rowspan = $row.data('rowspan');
        $row.prepend('<td rowspan="' + rowspan + '">' + this[1] + '</td>');
    });


});
</script>
@stop
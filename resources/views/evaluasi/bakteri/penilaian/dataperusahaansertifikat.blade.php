@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Sertifikat Identifikasi Siklus {{$siklus}}</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="get" enctype="multipart/form-data">
                    <div>
                        <label for="exampleInputEmail1">Pilih Tanggal Sertifikat :</label>
                        <div class="controls input-append date tanggal" data-link-field="dtp_input1">
                            <input size="16" type="text" value="" readonly class="form-control" name="tanggal" id="tanggal">
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                    </div><br>
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Bidang</th>
                            <th >Cetak</th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>

                        @if($val->bidang == '13')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}</td>
                                @if($siklus == 1)
                                <td class="text-center" style="padding: 2%;">
                                    @if(($val->pemeriksaan == 'done') && ($val->status_data1 == '2'))
                                    <a href="" class="go-link"  data-id="{{$val->perusahaan_id}}" data-y="1" data-tahun="{{$tahun}}" data-link="{{$val->Link}}" target="_blank"><center><i class="glyphicon glyphicon-pencil"></i><br></center></a>
                                    @else
                                    Belum Dikirim
                                    @endif
                                </td>
                                @else
                                <td class="text-center" style="padding: 2%;">
                                    @if(($val->pemeriksaan2 == 'done') && ($val->status_data2 == '2'))
                                    <a href="" class="go-link"  data-id="{{$val->perusahaan_id}}" data-y="2" data-tahun="{{$tahun}}" data-link="{{$val->Link}}" target="_blank"><center><i class="glyphicon glyphicon-pencil"></i><br></center></a>
                                    @else
                                    Belum Dikirim
                                    @endif
                                </td>
                                @endif
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(".go-link").on("click",function(e){
    e.preventDefault()
    var y = $(this).data("y");
    var tahun = $(this).data("tahun");
    var idLink = $(this).data("id");
    var link = $(this).data("link");
    var tanggal = $("#tanggal").val();
    var baseUrl = "{{URL('')}}"+link+"/sertifikat/print/"+idLink+"?y="+y+"&tahun="+tahun+"&tanggal="+tanggal+"&download=true";
    var url = baseUrl;
    console.log(url);

    window.location.href = url;
});

$(".tanggal").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 2,
    startView: 4
});
</script>
@endsection

<table>
    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">Kode Peserta</th>
        <th colspan="4">Skor Penilaian</th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th>BAC/1/{{$input['siklus']}}/{{$input['tahun']}}</th>
        <th>BAC/2/{{$input['siklus']}}/{{$input['tahun']}}</th>
        <th>BAC/3/{{$input['siklus']}}/{{$input['tahun']}}</th>
        <th>Total Skor</th>
    </tr>
    <?php $no=1;?>
    @foreach($data as $val)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$val->kode_lebpes}}</td>
        @if($type == 0)
            <?php $total = 0; ?>
            @if(count($val->bac))
            @foreach($val->bac as $bac)
                <td>{{$bac->nilai_1}}</td>
                <?php $total = $total + $bac->nilai_1; ?>
            @endforeach
            @else
            <td></td>
            <td></td>
            <td></td>
            @endif
        @else
            <?php $total = 0; ?>
            @if(count($val->bac))
            @foreach($val->bac as $bac)
                <td>{{$bac->nilai_2}}</td>
                <?php $total = $total + $bac->nilai_2; ?>
            @endforeach
            @else
            <td></td>
            <td></td>
            <td></td>
            @endif
        @endif
        <td>@if($total == 0) @else {{$total}}/12 @endif</td>
    </tr>
    @endforeach
</table>
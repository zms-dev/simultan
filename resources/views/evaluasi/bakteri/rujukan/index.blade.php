@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Rujukan Bakteri</div>
                <div class="panel-body">
                @if(Session::has('message'))
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                  </div>
                @endif
                    <div class="clearfix">
                        <div class="btn-group">
                            <a href="{{URL('/evaluasi/input-rujukan/bakteri/insert/')}}">
                            <button class="btn green">
                                Tambah <i class="icon-plus"></i>
                            </button>
                            </a>
                        </div>
                    </div>
                    <br>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Jenis Pemeriksaan</th>
                          <th>Siklus</th>
                          <th>Tahun</th>
                          <th>Lembar</th>
                          <th>Rujukan</th>
                          <th colspan="2" width="15%">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no=1 ?>
                        @foreach($rujukan as $val)
                        <tr>
                          <td>{{ $no++ }}</td>                          
                          <td>{{$val->jenis_pemeriksaan}}</td>
                          <td>{{$val->siklus}}</td>
                          <td>{{$val->tahun}}</td>
                          <td>{{$val->lembar}}</td>
                          <td>{!!$val->rujukan!!}</td>
                          <td>
                                <a href="{{URL('/evaluasi/input-rujukan/bakteri/edit').'/'.$val->id}}" style="float: left; margin-bottom: 10px">
                                    <button class="btn btn-primary btn-xs">
                                        &nbsp;<i class="glyphicon glyphicon-pencil"></i>&nbsp;
                                    </button>
                                </a> &nbsp;
                                <a href="{{URL('/evaluasi/input-rujukan/bakteri/delete').'/'.$val->id}}">
                                    <button class="btn btn-danger btn-xs" onclick="return confirm('Apakah Ingin Dihapus')">
                                        &nbsp;<i class="glyphicon glyphicon-trash"></i>&nbsp;
                                    </button>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{ $rujukan->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
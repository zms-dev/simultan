@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Rekap Hasil Identifikasi Peserta</div>
                <div class="panel-body">
                    <b>Tabel Hasil identifikasi peserta kode bahan BAC/{{$input['lembar']}}/{{$input['siklus']}}/{{date('y', strtotime($input['tahun']))}}</b>
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Jenis Jawaban Peserta</th>
                            <th>Jumlah</th>
                        </tr>
                        <?php $no=1; $total=0;?>
                        @foreach($data as $val)
                        <tr>
                            <td>{{$no++}}</td>
                            <td>{!!$val->spesies_kultur!!}</td>
                            <td>{!!$val->jumlah!!}</td>
                            <?php $total = $total + $val->jumlah; ?>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="2" style="text-align: right;">Total</td>
                            <td>{{$total}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Rekap Jenis Antibiotik yang Di Gunakan Peserta</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <select class="form-control" name="siklus" required>
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div><br>
                      <div>
                         	<label for="exampleInputEmail1">Tahun</label><br>
                          <div class="controls input-append date " data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control form_datetime" name="tahun" id="tahun">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div><br>
                      <div>
                          <label for="exampleInputEmail1">Lembar</label>
                          <select class="form-control" name="lembar" required>
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                          </select>
                      </div><br>
                    <input type="submit" name="proses" class="btn btn-primary" value="Proses">
                    {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scriptBlock')
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});
</script>
@stop
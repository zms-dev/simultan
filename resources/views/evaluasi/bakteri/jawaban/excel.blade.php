@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Rekap Hasil Identifikasi Peserta</div>
                <div class="panel-body">
                    <b>kode bahan BAC/{{$input['lembar']}}/{{$input['siklus']}}/{{date('y', strtotime($input['tahun']))}}</b>
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2">Jenis Antibiotik</th>
                            <th colspan="3">Jumlah Jawaban / Interpretasi</th>
                        </tr>
                            <th>S</th>
                            <th>I</th>
                            <th>R</th>
                        <tr>
                            
                        </tr>
                        <?php $no=1; $total=0;?>
                        @foreach($data as $val)
                        @if($val->SS != 0 || $val->II != 0 || $val->RR != 0)
                        <tr>
                            <td>{{$val->antibiotik}} @if($val->lain_lain != NULL) {{$val->lain_lain}} @endif</td>
                            <td>{{$val->SS}}</td>
                            <td>{{$val->II}}</td>   
                            <td>{{$val->RR}}</td>
                        </tr>
                        @endif
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
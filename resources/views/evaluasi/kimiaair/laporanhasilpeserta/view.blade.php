<table border="1">
    <thead>
        <tr>
        @foreach($data as $val)
            <th rowspan="2">Kode Lab</th>
            @foreach($val->parameter as $para)
                @if(count($para->detail))
                    <th colspan="2">{!!$para->nama_parameter!!}</th>
                @endif
            @endforeach
        @break
        @endforeach
        </tr>
        <tr>
            <th></th>
            @foreach($data as $val)
                @foreach($val->parameter as $para)
                    @if(count($para->detail))
                        <th>Metode Pemeriksaan</th>
                        <th>Hasil Pemeriksaan</th>
                    @endif
                @endforeach
            @break
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($data as $val)
        <tr>
            <td>{{$val->kode_lab}}</td>
            @foreach($val->parameter as $par)
                @if(count($par->detail))
                    @foreach($par->detail as $hasil)
                        <td>{{$hasil->kode_metode_pemeriksaan}}</td>
                        <td>'{{$hasil->hasil_pemeriksaan}}</td>
                    @endforeach
                @endif
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>
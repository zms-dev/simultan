@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Hitung Z-Score Kimia Air</div>

                <div class="panel-body">
                @if(count($zscore))
                <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{url('/hasil-pemeriksaan/kimia-air/evaluasi')}}/print/{{$id}}?x={{$type}}&y={{$siklus}}" target="_blank">
                @else
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                @endif
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Kode Lab</th>
                                <th>Parameter</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Alat & Merek</th>
                                <th>Metode Pengujian</th>
                                <th>Hasil Pengujian</th>
                                <th>Z-Score</th>
                                @if(count($zscore))
                                <th>Keterangan</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $val)
                            <tr>
                                <td>{{$val->kode_lab}}</td>
                                <td>
                                    {{$val->nama_parameter}}
                                    <input type="hidden" name="parameter[]" value="{{$val->id}}">
                                </td>
                                <td>{{date('d/m/Y', strtotime($val->tanggal_mulai))}}</td>
                                <td>{{date('d/m/Y', strtotime($val->tanggal_selesai))}}</td>
                                <td>{{$val->alat}}</td>
                                <td>{{$val->kode_metode_pemeriksaan}}</td>
                                <td>{{$val->hasil_pemeriksaan}}</td>
                                <td>
                                    <?php
                                        if ($val->hasil_pemeriksaan == '0') {
                                            $z = 'Tidak di Evaluasi';
                                        }else{
                                            $z = ($val->hasil_pemeriksaan - $val->sd[0]->median) / $val->sd[0]->sd;
                                            if ($z < 0) {
                                                $zs = $z * -1;
                                                $z = number_format($zs, 3);
                                            }else{
                                                $z = number_format($z, 3);
                                            }
                                        }
                                    ?>
                                    @if(count($zscore))
                                        @foreach($zscore as $za)
                                            @if($za->parameter == $val->id)
                                                {{$za->zscore}}
                                            @endif
                                        @endforeach
                                    @else
                                    {{$z}}
                                    <input type="hidden" name="zscore[]" value="{{$z}}">
                                    @endif
                                </td>
                                @if(count($zscore))
                                    @foreach($zscore as $za)
                                        @if($za->parameter == $val->id)
                                            <td>
                                                <?php 
                                                if ($za->zscore != 'Tidak di Evaluasi') {
                                                    if($za->zscore <= 2 && $za->zscore >= -2){
                                                        $keterangan = 'Memuaskan';
                                                    }else if($za->zscore <= 3 && $za->zscore >= -3){
                                                        $keterangan = 'Meragukan';
                                                    }else{
                                                        $keterangan = 'Kurang Memuaskan';
                                                    }
                                                }else{
                                                    $keterangan = 'Tidak di Evaluasi';
                                                }
                                                ?>
                                                {{$keterangan}}
                                            </td>
                                        @endif
                                    @endforeach
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @if(count($zscore))
                <input type="submit" name="simpan" class="btn btn-primary" value="Print">
                @else
                <input type="submit" name="simpan" class="btn btn-primary" value="Simpan">
                @endif
                {{ csrf_field() }}
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
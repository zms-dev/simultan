@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Sertifikat KIMKES Siklus {{$siklus}}</div>

                <div class="panel-body">
                	<form class="form-horizontal" method="get" enctype="multipart/form-data">
                          <div>
                                <label for="exampleInputEmail1">Pilih Tanggal Sertifikat :</label>
                                <div class="controls input-append date" data-link-field="dtp_input1">
                                    <input size="16" type="text" value="" class="form_datetime form-control" name="tanggal" id="tanggal" required autocomplete="off">
                                    <span class="add-on"><i class="icon-th"></i></span>
                                </div>

                          </div><br>
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Bidang</th>
                            <th>Cetak</th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>

                        @if($val->bidang == '11')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}</td>
                                @if($siklus == 1)
                                <td class="text-center" style="padding: 2%;">
                                    @if(($val->pemeriksaan == 'done') && ($val->siklus_1 == 'done'))
                                    <a href="" class="go-link" data-id="{{$val->perusahaan_id}}" data-x="a" data-y="1" data-link="{{$val->Link}}"><center><i class="glyphicon glyphicon-print"></i><br></center></a>
                                    @else
                                    Belum Dikirim
                                    @endif
                                </td>
                                @else
                                <td class="text-center" style="padding: 2%;">
                                    @if(($val->pemeriksaan == 'done') && ($val->siklus_2 == 'done'))

                                    <a href="" class="go-link" data-id="{{$val->perusahaan_id}}" data-x="a" data-y="2" data-link="{{$val->Link}}"><center><i class="glyphicon glyphicon-print"></i><br></center></a>
                                    @else
                                    Belum Dikirim
                                    @endif
                                </td>
                                @endif
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
              		</form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(".go-link").on("click",function(e){
        e.preventDefault()
        var x = $(this).data("x");
        var y = $(this).data("y");
        var idLink = $(this).data("id");
        var link = $(this).data("link");
        var tanggal = $("#tanggal").val();
        var baseUrl = "{{URL('')}}"+link+"/sertifikat/print/"+idLink+"?x="+x+"&y="+y+"&tanggal="+tanggal+"&download=true";
        var url = baseUrl;
        console.log(url);
        window.location.href = url;
    })
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: true,
        minView: 2
    });
</script>
@endsection

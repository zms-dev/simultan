@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Parameter Kimia Air dengan Nilai yang Sama</div>
                <div class="panel-body">
                    @foreach($data as $val)
                        <div id="container{{$val->id}}" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
</div>

<script>
@foreach($data as $val)
Highcharts.chart('container{{$val->id}}', {
    chart: {
        type: 'pie'
    },
    title: {
        text: 'Grafik Nilai Peserta Kimia Air <br> Parameter {{$val->nama_parameter}}'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total Nilai Peserta Kimia Air'
        }

    },
    legend: {
        enabled: true
    },
    plotOptions: {
        series: {
            allowPointSelect: true,
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f} %'
            },
            showInLegend: true
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.1f} %</b><br/>'
    },

    "series": [
        {
            "name": "Keterangan",
            "colorByPoint": true,
            "data": [
                @php
                    $arrC = [
                        0 => 0,
                        1 => 0,
                        2 => 0,
                        3 => 0,
                    ];
                @endphp
                @if(count($zscore))
                    @foreach($zscore as $za)
                        @if($za->parameter == $val->id)
                            <?php
                                if ($za->zscore != 'Tidak di Evaluasi') {
                                    if($za->zscore <= 2 && $za->zscore >= -2){
                                        $keterangan = 'Memuaskan';
                                        $arrC[0]++;
                                        $persen1 = ($arrC[0]/$val->count)*100;
                                    }else if($za->zscore <= 3 && $za->zscore >= -3){
                                        $keterangan = 'Meragukan';
                                        $arrC[1]++;
                                        $persen2 = ($arrC[1]/$val->count)*100;
                                    }else{
                                        $keterangan = 'Kurang Memuaskan';
                                        $arrC[2]++;
                                        $persen3 = ($arrC[2]/$val->count)*100;
                                    }
                                }else{
                                    $keterangan = 'Tidak di Evaluasi';
                                    $arrC[3]++;
                                    $persen4 = ($arrC[3]/$val->count)*100;
                                }
                            ?>
                        @endif
                    @endforeach
                @endif
                {
                    "name": "Memuaskan",
                    "y" :{{$persen1}},
                },
                {
                    "name": "Meragukan",
                    "y" :{{$persen2}},
                },
                {
                    "name": "Kurang Memuaskan",
                    "y" :{{$persen3}},
                },
                {
                    "name": "Tidak di Evaluasi",
                    "y" :{{$persen4}},
                },
            ]
        }
    ],
});
@endforeach
</script>
@endsection

<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <script src="{{URL::asset('asset/js/jquery.min.js')}}"></script>
    <script src="{{URL::asset('asset/js/highstock.js')}}"></script>
    <script src="{{URL::asset('asset/js/exporting.js')}}"></script>
    <script src="{{URL::asset('asset/js/export-data.js')}}"></script>
    <script src="{{URL::asset('asset/js/highcharts-3d.js')}}"></script>
    <link rel="stylesheet" href="{{URL::asset('asset/css/bootstrap.css')}}">
    <style type="text/css">
        body{
            font-family: arial
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-body">
                      @foreach($data as $val)
                          <div id="container{{$val->id}}"></div>
                          <br>
                          <br>

                      @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    @foreach ($data as $val)
    Highcharts.chart('container{{$val->id}}', {
    	chart: {
    	    type: 'column'
    	},
    	title: {
    	    text: 'Grafik Sebaran Z-Score',
    	},
        subtitle: {
            text: '{{$val->nama_parameter}}',
            style: {
                "fontSize": "17px"
            }
        },
    	xAxis: {
            categories: [
            	@foreach($val->perusahaan as $p)
            		'{{$p->kode_lebpes}}',
            	@endforeach
            ],
            labels: {
                rotation: -45
            }
        },
        yAxis: {
          title: {
            text : 'Z-Score'
          }
        },
    	credits: {
    		enabled: false
    	},
    	legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.3f}',
                    rotation: -90,
                    y: -20,
                    crop: false,
                    overflow: 'none'
                }
            }
        },
    	series: [
    		{
    			name: 'Lebpes',
    	    	data: [
    	    		@foreach($val->datas as $d)
    	        		{{$d->zscore}},
    	        	@endforeach
    	    	],
    		}
    	]
    });
    @endforeach

    </script>
</body>
</html>
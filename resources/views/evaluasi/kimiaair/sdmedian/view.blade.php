@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Nilai SD dan Median Per Parameter (Kimia Air)</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-md-6">
                          <div>
                              <label for="exampleInputEmail1">Parameter</label>
                              <select class="form-control" name="parameter">
                                <option value="{{$sd->parameter}}">{{$sd->nama_parameter}}</option>
                                @foreach($parameter as $val)
                                <option value="{{$val->id}}">{{$val->nama_parameter}}</option>
                                @endforeach
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Siklus</label>
                              <select class="form-control" name="siklus">
                                <option value="{{$sd->siklus}}">{{$sd->siklus}}</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Tahun</label>
                              <select class="form-control" name="tahun">
                                <option value="{{$sd->tahun}}">{{$sd->tahun}}</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                              </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div>
                              <label for="exampleInputEmail1">SD</label>
                              <input type="text" onkeypress="return onlyNumbersWithDot(event);" name="sd" class="form-control" value="{{$sd->sd}}">
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Median</label>
                              <input type="text" onkeypress="return onlyNumbersWithDot(event);" name="median" class="form-control" value="{{$sd->median}}">
                          </div>
                        </div>
                      </div>
                      <br>
                    <input type="submit" name="proses" class="btn btn-primary" value="Update">
                    {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function onlyNumbersWithDot(e) {           
    var charCode;
    if (e.keyCode > 0) {
        charCode = e.which || e.keyCode;
    }
    else if (typeof (e.charCode) != "undefined") {
        charCode = e.which || e.keyCode;
    }
    console.log(charCode);
    if (charCode == 46 || charCode == 45)
        return true
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>
@endsection
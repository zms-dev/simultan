@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Rekap Tanda Terima Bahan Peserta Siklus {{$siklus}}</div>

                <div class="panel-body">
                    <table class="table table-bordered">
                        <tr>
                            <th width="5%"><center>No</center></th>
                            <th><center>Bidang</center></th>
                            <th colspan="2" width="200px"><center>Aksi</center></th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                <td>
                                    <a href="{{URL('').$val->Link}}/tanda-terima/print/{{$val->id}}?y={{$siklus}}" target="_blank"><center><i class="glyphicon glyphicon-print"></i></center></a>
                                </td>
                            </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Rekap Hasil BTA</div>
                <div class="panel-body">
                  <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var chart = Highcharts.chart('container', {
    @foreach($data as $val)
        title: {text: 'Grafik Jawaban Peserta Kode Sediaan {{$input['kode_sediaan']}}'},
    @break
    @endforeach
    subtitle: {text: ''},
    xAxis: {categories: [
        @foreach($data as $val)
            '{{$val->hasil}}',
        @endforeach]},
    yAxis: {
        title: {
            text : ''
        }
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: { 
                enabled: true,
                format:'{point.y}'
            }
        }
    },
    legend: {
        enabled: false
    },
    series: [{type: 'column',
        name: 'Jumlah',
        data: [
        @foreach($data as $val)
            {
                name: '{{$val->keterangan}}',
                y: {{$val->jumlah}},
                @if($val->keterangan == 'Benar')
                color: '#337ab7'
                @elseif($val->keterangan == 'NPR' || $val->keterangan == 'KH' || $val->keterangan == 'PPR')
                color: '#f3ed2b'
                @elseif($val->keterangan == 'PPT' || $val->keterangan == 'NPT')
                color: '#ea2641'
                @endif
            },
        @endforeach
        ],
        showInLegend: true
    }]
});
</script>
@endsection
@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Jawaban Peserta</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <select class="form-control" name="siklus" required>
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div><br>
                      <div>
                          <label for="exampleInputEmail1">Kode Sediaan</label>
                          <select class="form-control" name="kode_sediaan" required>
                            <option></option>
                            <?php for ($i=1; $i <= 10; $i++) { ?>
                              <option value="{{$i}}">{{$i}}</option>
                            <?php } ?>
                          </select>
                      </div><br>
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <select class="form-control" name="tahun" required>
                            <?php for ($i=date('Y'); $i >= 2018; $i--) { ?>
                              <option value="{{$i}}">{{$i}}</option>
                            <?php } ?>
                          </select>
                      </div><br>
                    <input type="submit" name="proses" class="btn btn-primary" value="Proses">
                    {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<table border="1">
    <tr>
        <th>No</th>
        <th>Kode</th>
        <?php for ($i=1; $i < 11; $i++) { ?>
        <th>{{$i}}/{{$input['siklus']}}/{{$input['tahun']}}</th>
        <?php } ?>
    </tr>
    <?php $no = 0; ?>
    @foreach($data as $val)
    <?php $no++; ?>
    <tr>
        <td>{{$no}}</td>
        <td>{{$val->kode_lebpes}}</td>
        @foreach($val->hasil as $hasil)
        <td>{{$hasil->hasil}}</td>
        @endforeach
    </tr>
    @endforeach
</table>
<style type="text/css">
@page {
margin: 100px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 12px;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}

tr.border_bottom td{
  border-bottom:1pt solid black;
}
</style>
<table border="1" cellspacing="1" cellpadding="1" width="100%" style="margin-top: 1%; font-size: 14px;">
    <thead>
        <tr class="titlerowna" style="">
            <th><center>No</center></th>
            <th><center>Kode Sediaan</center></th>
            <th><center>Hasil Peserta</center></th>
            <th><center>Nilai Acuan</center></th>
            <th><center>Nilai</center></th>
            <th><center>Total</center></th>
            <th><center>kesimpulan</center></th>
        </tr>
    </thead>
    <tbody>
        <?php $urutan = 0; ?>
        @foreach($peserta as $pesertaa)
            <?php $urutan++; ?>
            <?php $urutanno; ?>
            @php $id = $pesertaa->id_registrasi @endphp
            
            @if(count($data))
                <?php $no = 0; ?>
                @foreach($datas[$id] as $val)
                    <?php $no++; ?>
                    <tr>
                        @if($no == 1)
                            <td rowspan="10">
                                <center>{{ $urutan }}</center>
                            </td>
                        @endif
                        <td>{!!$val->kode!!}</td>
                        <td>
                            {{ $val->hasil }}
                        </td>
                        <td>
                            @if(count($rujuk[$id]) > 0)
                                @foreach($rujuk[$id] as $valu)
                                    {{$valu->rujukan}}
                                @endforeach
                            @endif
                        </td>

                        @if(count($nilai[$id]) > 0)
                            @foreach($nilai[$id] as $valu)
                                @if($valu->kode_sediaan == $val->kode)
                                    <td>{{$valu->nilai}}</td>
                                @endif
                            @endforeach
                        @else
                            <td></td>
                        @endif

                        @if($no == 1)
                            <td rowspan="10">
                                <center>{{ $evaluasi[$id] }}</center>
                            </td>

                            <td rowspan="10"><center>
                                {{$status[$id]->status}}
                            </center></td>
                        @endif
                    </tr>
                @endforeach
            @endif

            @if($urutan % 3 == 0)
                <tr><td colspan="7" style="padding: 0"><div style="page-break-before: always;"></div></td></tr>
            @endif
        @endforeach
    </tbody>
</table>

<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>
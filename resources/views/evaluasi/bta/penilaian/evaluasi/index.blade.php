@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluasi BTA</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                <table class="table table-bordered" id="peserta_pme">
                    <thead>
                        <tr class="titlerowna">
                            <th>Kode Sediaan</th>
                            <th>Nilai Acuan</th>
                            <th>Hasil Peserta</th>
                            <th>Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($evaluasi))
                        @foreach($evaluasi as $val)

                            <input type="hidden" name="id[]" value="{{$val->id}}">
                            
                        @endforeach
                        @endif
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($datas as $val)
                        <?php $no++; ?>
                        <tr>
                            <td>{!!$val->kode!!}<input type="hidden" value="{!!$val->kode!!}" name="kode_sediaan[]"></td>
                            @if(count($rujuk))
                                @foreach($rujuk as $re)
                                    @if($re->kode_bahan_uji == $no)
                                        <td>{{$re->rujukan}}</td>
                                    @endif
                                @endforeach
                            @else
                            <td></td>
                            @endif
                            <td>{{ $val->hasil }}</td>
                            <td>
                                <select name="nilai[]">
                                    @if(count($evaluasi))
                                    @foreach($evaluasi as $valu)
                                        @if($valu->kode_sediaan == $val->kode)
                                        <option value="{{$valu->keterangan}}">{{$valu->keterangan}}</option>
                                        @endif
                                    @endforeach
                                    @endif
                                    @if(count($rujuk))
                                        @foreach($rujuk as $re)
                                            @if($re->kode_bahan_uji == $no)

                                                @if($val->hasil == 'Negatif' && $re->rujukan == 'Negatif')
                                                    <option value="Benar">Benar</option>
                                                @elseif($val->hasil == 'Negatif' && $re->rujukan == 'Scanty')
                                                    <option value="NPR">NPR</option>
                                                @elseif($val->hasil == 'Negatif' && $re->rujukan == '1+')
                                                    <option value="NPT">NPT</option>
                                                @elseif($val->hasil == 'Negatif' && $re->rujukan == '2+')
                                                    <option value="NPT">NPT</option>
                                                @elseif($val->hasil == 'Negatif' && $re->rujukan == '3+')
                                                    <option value="NPT">NPT</option>

                                                @elseif($val->hasil == 'Scanty' && $re->rujukan == 'Scanty')
                                                    <option value="Benar">Benar</option>
                                                @elseif($val->hasil == 'Scanty' && $re->rujukan == 'Negatif')
                                                    <option value="PPR">PPR</option>
                                                @elseif($val->hasil == 'Scanty' && $re->rujukan == '1+')
                                                    <option value="Benar">Benar</option>
                                                @elseif($val->hasil == 'Scanty' && $re->rujukan == '2+')
                                                    <option value="KH">KH</option>
                                                @elseif($val->hasil == 'Scanty' && $re->rujukan == '3+')
                                                    <option value="KH">KH</option>

                                                @elseif($val->hasil == '1+' && $re->rujukan == 'Negatif')
                                                    <option value="PPT">PPT</option>
                                                @elseif($val->hasil == '1+' && $re->rujukan == 'Scanty')
                                                    <option value="Benar">Benar</option>
                                                @elseif($val->hasil == '1+' && $re->rujukan == '1+')
                                                    <option value="Benar">Benar</option>
                                                @elseif($val->hasil == '1+' && $re->rujukan == '2+')
                                                    <option value="Benar">Benar</option>
                                                @elseif($val->hasil == '1+' && $re->rujukan == '3+')
                                                    <option value="KH">KH</option>

                                                @elseif($val->hasil == '2+' && $re->rujukan == 'Negatif')
                                                    <option value="PPT">PPT</option>
                                                @elseif($val->hasil == '2+' && $re->rujukan == 'Scanty')
                                                    <option value="KH">KH</option>
                                                @elseif($val->hasil == '2+' && $re->rujukan == '1+')
                                                    <option value="Benar">Benar</option>
                                                @elseif($val->hasil == '2+' && $re->rujukan == '2+')
                                                    <option value="Benar">Benar</option>
                                                @elseif($val->hasil == '2+' && $re->rujukan == '3+')
                                                    <option value="Benar">Benar</option>

                                                @elseif($val->hasil == '3+' && $re->rujukan == 'Negatif')
                                                    <option value="PPT">PPT</option>
                                                @elseif($val->hasil == '3+' && $re->rujukan == 'Scanty')
                                                    <option value="KH">KH</option>
                                                @elseif($val->hasil == '3+' && $re->rujukan == '1+')
                                                    <option value="KH">KH</option>
                                                @elseif($val->hasil == '3+' && $re->rujukan == '2+')
                                                    <option value="Benar">Benar</option>
                                                @elseif($val->hasil == '3+' && $re->rujukan == '3+')
                                                    <option value="Benar">Benar</option>
                                                @else
                                                    <option value=""></option>
                                                @endif

                                            @endif
                                        @endforeach
                                    @else
                                        <option value=""></option>
                                    @endif
                                    <option value="Benar">Benar</option>
                                    <option value="KH">KH</option>
                                    <option value="PPR">PPR</option>
                                    <option value="NPR">NPR</option>
                                    <option value="PPT">PPT</option>
                                    <option value="NPT">NPT</option>
                                </select>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-md-6">
                        <div>
                            <label for="exampleInputEmail1">Status</label>
                            <select class="form-control" name="status" required>
                                @if(count($status))
                                <option value="{{$status->status}}">{{$status->status}}</option>
                                @else
                                <option></option>
                                @endif
                                <option value="Lulus">Lulus</option>
                                <option value="Tidak Lulus">Tidak Lulus</option>
                            </select>
                        </div>
                    </div>
                </div><br>
                @if(count($evaluasi))
                <input type="submit" name="simpan" class="btn btn-primary" value="Update">
                <a href="{{ url('/hasil-pemeriksaan/bta/evaluasi/cetak/'. $id.'?y='.$siklus) }}" class="btn btn-success"   target="_BLANK">cetak</a>
                @else
                <input type="submit" name="simpan" class="btn btn-primary" value="Simpan">
                @endif
                {{ csrf_field() }}
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
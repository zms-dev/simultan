<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}
</style>
<table width="100%" cellpadding="0" border="0" style="margin-top: -2%:">
    <thead>
        <tr>
            <th>
                <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px"> 
            </th>
            <th width="2%;"></th>
            <td width="100%">
                <span style="font-size: 16px"><b>KEMENTERIAN KESEHATAN R.I.</b></span><br>
                <span style="font-size: 16px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL MIKROBIOLOGI BTA SIKLUS {{$siklus}} TAHUN {{$tahun}}</b></span><br>
                <span style="font-size: 15px;">Penyelenggara :</span>
                <span style="font-size: 17px;"> 
                    <b>Balai Besar Laboratorium Kesehatan Surabaya</b>
                </span>
                <pre style="font-family: Helvetica;font-size: 12px; padding: -25px; margin-left: -18px; margin-top: 10px;">
                                                Jl.Karangmenjangan No. 18 Surabaya 60286
                                                Telepon : 031-5021451 Fax.031-5020388
                                                Email : pme.bblksub@gmail.com / mikrobblksub@gmail.com
                </pre>
            </td>
        </tr>
        <tr>
            <th colspan="4"><hr></th>
        </tr>
    </thead>
</table>

<center><label><b>LAMPIRAN EVALUASI PESERTA<br>
PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL MIKROBIOLOGI<br> MIKROSKOPIS BTA SIKLUS {{$siklus}} TAHUN {{$tahun}}
</b><input type="hidden" name="type" value="{{$siklus}}"></label></center>
<br>
<table>
  <tr>
    <td>Sifat</td>
    <td>:</td>
    <th>Rahasia</th>
  </tr>
  <tr>
    <td>Kode Lab Peserta</td>
    <td>:</td>
    <td>{{substr($register->kode_lebpes,1,-6)}}</td>
  </tr>
  <tr>
    <td>Nama Instansi</td>
    <td>:</td>
    <td>{{$perusahaan}}</td>
  </tr>
   <tr>
    <td>Alamat</td>
    <td>:</td>
    <td>{{$alamat}}</td>
  </tr>
  <tr>
    <td></td>
  </tr>
  <tr>
      <td>Hasil Pemeriksaan</td>
      <td>:</td>
  </tr>
</table><br>
<table border="1" width="100%">
    <tr class="titlerowna" style="text-align: center;">
        <th>Kode Sediaan</th>
        <th>Nilai Acuan</th>
        <th>Hasil Peserta</th>
        <th>Nilai</th>
    </tr>
     @if(count($data))
        <?php 
          $no = 0;
          $kh = 0;
          $npr = 0;
          $ppr = 0;
          $npt = 0;
          $ppt = 0;
        ?>
        @foreach($datas as $val)
        <?php $no++; ?>
        <tr  style="text-align: center;">
            <td>{!!$val->kode!!}<input type="hidden" value="{!!$val->kode!!}" name="kode_sediaan[]"></td>
            @if(count($rujuk))
                @foreach($rujuk as $re)
                    @if($re->kode_bahan_uji == $no)
                        <td>{{$re->rujukan}}</td>
                    @endif
                @endforeach
            @else
            <td></td>
            @endif
            <td>{{ $val->hasil }}</td>
            <td>
              @if(count($evaluasi))
              @foreach($evaluasi as $valu)
                  @if($valu->kode_sediaan == $val->kode)
                  {{$valu->keterangan}}
                  <?php 
                    if ($valu->keterangan == 'KH') {
                      $kh++;
                    }elseif($valu->keterangan == 'NPR') {
                      $npr++;
                    }elseif($valu->keterangan == 'PPR') {
                      $ppr++;
                    }elseif($valu->keterangan == 'NPT') {
                      $npt++;
                    }elseif($valu->keterangan == 'PPT') {
                      $ppt++;
                    }
                  ?>
                  @endif
              @endforeach
              @endif
            </td>
        </tr>
        @endforeach
        @endif
</table><br>
<table border="1">
  <tr>
    <th>Kesalahan Hitung (KH)</th>
    <td><center>{{$kh}}</center></td>
  </tr>
  <tr>
    <th>Negatif Palsu Rendah (NPR)</th>
    <td><center>{{$npr}}</center></td>
  </tr>
  <tr>
    <th>Positif Palsu Rendah (PPR)</th>
    <td><center>{{$ppr}}</center></td>
  </tr>
  <tr>
    <th>Negatif Palsu Tinggi (NPT)</th>
    <td><center>{{$npt}}</center></td>
  </tr>
  <tr>
    <th>Positif Palsu Tinggi (PPT)</th>
    <td><center>{{$ppt}}</center></td>
  </tr>
  <tr>
    <th>Skor Total</th>
    <th><center>{{$skor}}</center></th>
  </tr>
  <tr>
    <th>Keputusan</th>
    <th><center>{{$status->status}}</center></th>
  </tr>
</table>
<br>
<table border="1" width="100%">
  <tr style="text-align: center;">
    <th>Jenis Kesalahan</th>
    <th>Kemungkinan Penyebab</th>
    <th>Saran Tindakan</th>
  </tr>
  @if($npr > 0 || $npt > 0)
  <tr>
    <td>NPR/NPT</td>
    <td>a. Teknik pemeriksaan mikroskopis BTA tidak sesuai petunjuk teknis<br>
        b. Pembacaan kurang dari 100LP<br>
        c. Kerusakan Mikroskop<br>
        d. Salah menyalin hasil
    </td>
    <td>a. Observasi, apakah implementasi prosedur tetap sudah tepat?<br>
        b. Periksa fungsi mikroskop<br>
        c. Periksa kembali jawaban yang sudah disalin
    </td>
  </tr>
  @endif
  @if($ppr > 0 || $ppt > 0)
  <tr>
    <td>PPR/PPT</td>
    <td>a. Petugas tidak mengenal bentuk BTA<br>
        b. BTA terbawa melalui pipet minyak emersi dari sediaan BTA positif sebelumnya (carry over)<br>
        c. Salah menyalin hasil<br>
        <!-- d. Artefak (endapan zat warna atau Kristal)<br>
        e. Warna BTA pudar , sehingga dibaca negatif oleh petugas Laboratorium. -->
    </td>
    <td>a. Uji dengan sediaan BTA positif saat supervisi<br>
        b. Saat meneteskan minyak emersi ujung pipet tidak boleh menyentuh kaca sediaan. Bersihkan lensa objektif 100x<br>
        c. Periksa kembali jawaban yang sudah disalin<br>
        <!-- d. Carbol fuchsin disaring saat pewarnaan<br>
        e. Pewarnaan ulang <br>
        f. Saat supervisi lakukan uji kualitas ZN -->
    </td>
  </tr>
  @endif
  @if($kh > 0)
  <tr>
    <td>Kesalahan Hitung (Kesalahan Kecil)</td>
    <td>Terjadi kesalahan hitung jumlah BTA dalam 100LP
    </td>
    <td>Lakukan pencatatan hasil baca sesuai skala IUATLD
    </td>
  </tr>
  @endif
</table>
<br>
<div style="margin-left: 550px; margin-top: -1%;">
<div style="position: relative; top: 17px">
Surabaya, {{$ttd->tanggal}} {{$tahun}}<br>
Manajer Teknis
</div>
<p>
    <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.'ttdevaluasi'.DIRECTORY_SEPARATOR.$ttd->image)}}" width="80" height="80" style="margin-left: 20px !important;">
</p>

<div style="position: relative; top:-20px">
  {{$ttd->nama}}<br>NIP. {{$ttd->nip}}
</div><br>
</div>

<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>

<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <script src="{{URL::asset('asset/js/jquery.min.js')}}"></script>
    <script src="{{URL::asset('asset/js/highstock.js')}}"></script>
    <script src="{{URL::asset('asset/js/exporting.js')}}"></script>
    <script src="{{URL::asset('asset/js/export-data.js')}}"></script>
    <script src="{{URL::asset('asset/js/highcharts-3d.js')}}"></script>
    <link rel="stylesheet" href="{{URL::asset('asset/css/bootstrap.css')}}">
    <script src="{{URL('/asset/js/html2pdf.bundle.min.js')}}"></script>
    <script src="{{URL('/js/highchartexport.js')}}"></script>
    <script src="{{URL('/js/jquery.printarea.js')}}"></script>
    <style type="text/css">
        body{
            font-family: arial
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                        <a style="margin: 10px" onclick="javascript:printDiv('report-chart')"><li class="glyphicon glyphicon-print"></li></a>
                        <div class="report-chart" id="report-chart">

                        <style>
                            .report-chart-title{
                                text-align:center;
                                padding:15px 0px;
                                margin-bottom:30px;
                            }
                            .report-chart-title h3{
                                font-size:22px;
                                margin:0px;
                                padding:0px;
                            }
                            .row-chart{
                                padding:0px;
                            }
                            .row-chart .col{
                                display:block;
                                width:50%;
                                float:left;
                            }
                            .row-chart:after{
                                content : " ";
                                clear:both;
                                display:block;
                            }
                            .graph-container{
                                padding:0px;
                                margin-bottom:30px;
                                width:100%;
                                display:block;
                                padding:0;
                                text-align:center;
                            }
                            .graph-container .graph{
                                width:90%;
                                margin:auto;
                                display:inline-block;
                                height:300px;
                            }
                            .table-information{
                                border-bottom:1px solid  #ddd;
                            }
                            .table-information td{
                                height:36px;
                                vertical-align:middle !important;
                            }
                            .report-chart-title h3{
                                font-size:16px;
                            }
                            .color-green{
                                background: #1bbd20 !important;color: #1bbd20 !important;display:inline-block;width:15px;height:25px;
                            }
                            .color-red{
                                background: #BF0B23 !important;color: #BF0B23 !important;display:inline-block;width:15px;height:25px;
                            }
                            @media print {
                                .graph-container .graph{
                                    width:100%;
                                    margin: auto;
                                    height:300px;
                                    display: inline-block;
                                }

                                .color-green{
                                    -webkit-print-color-adjust: exact;
                                    background: #1bbd20 !important;display:inline-block;width:15px;height:25px;
                                }
                                .color-red{
                                    -webkit-print-color-adjust: exact;
                                    background: #BF0B23 !important;display:inline-block;width:15px;height:25px;
                                }
                            }
                            @media screen {
                                .left{
                                    display: none;
                                }
                                .print-area .footer-content .left{
                                    display: none;
                                }
                            }
                            .print-area .footer-content{
                                position:absolute;
                                bottom:0;
                                left:0;
                                right:0;
                                border-top:1px solid #333;
                            }
                            .print-area .footer-content .left{
                                float:left;
                                width:50%;
                                font-size: 14px;
                                font-weight: 400;
                            }
                            .print-area .footer-content .right{
                                float:right;
                                width:50%;
                                text-align:right;
                            }
                            .print-area .footer-content:after{
                                content : " ";
                                display : block;
                                clear:both;
                            }
                        </style>
                        <div class="report-chart-title">
                            <h3>GRAFIK HISTOGRAM DISTRIBUSI HASIL PNPME DAN Z SCORE BIDANG KIMIA KLINIK<br>BERDASARKAN KELOMPOK SELURUH PESERTA</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-information table-condensed">
                                    <tbody>
                                        <tr>
                                            <td width="150px" style="font-weight:bold;">KODE PESERTA</td>
                                            <td width="10px">:</td>
                                            <td width="40%">{{$data->kode_lebpes}}</td>
                                            <td width="50px" style="font-weight:bold;"><div class="color-red"></div></td>
                                            <td width="10px">:</td>
                                            <td>Tidak Memuaskan</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight:bold;">SIKLUS</td>
                                            <td>:</td>
                                            <td>{{$siklus}} - {{($type == 'a' ? '01' : '02')}}</td>
                                            <td><div class="color-green"></div></td>
                                            <td>:</td>
                                            <td>Nilai Target</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight:bold;">TAHUN</td>
                                            <td>:</td>
                                            <td>{{$tahun}}</td>
                                            <td><b>*</b></td>
                                            <td>:</td>
                                            <td>Posisi Saudara</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12">
                                <div class="row-chart">
                                    <table width="100%">
                                        <tr>
                                            <td style="vertical-align: top;">
                                            <?php $a = 0;?>
                                            @foreach($param as $key => $val)
                                                @if(!empty($val->hasil_pemeriksaan) && $val->hasil_pemeriksaan != "-" && $val->hasil_pemeriksaan != "0" && $val->hasil_pemeriksaan != "0.00" && $val->hasil_pemeriksaan != "0.0" && !empty($val->batas_bawah) && !empty($val->batas_atas) && is_numeric($val->hasil_pemeriksaan))
                                                    <div class="graph graph-nilai-sama" id="graph-nilai-sama-{{$key}}" data-json="{{json_encode($val)}}"></div>
                                                @endif
                                            <?php 
                                                $a++; 
                                                if($a == 4):$a=0;
                                                else:
                                                endif;
                                            ?>
                                            @endforeach
                                            </td>
                                            <td style="vertical-align: top;">
                                            @foreach($paramalat as $key => $val)
                                                @if(!empty($val->hasil_pemeriksaan) && $val->hasil_pemeriksaan != "-" && $val->hasil_pemeriksaan != "0" && $val->hasil_pemeriksaan != "0.00" && $val->hasil_pemeriksaan != "0.0" && !empty($val->batas_bawah) && !empty($val->batas_atas) && is_numeric($val->hasil_pemeriksaan))
                                                    <div class="graph graph-nilai-alat-sama" id="graph-nilai-alat-sama-{{$key}}" data-json="{{json_encode($val)}}"></div>
                                                @endif
                                            <?php 
                                                $a++; 
                                                if($a == 4):$a=0;
                                                else:
                                                endif;
                                            ?>
                                            @endforeach
                                            </td>
                                            <td style="vertical-align: top;">
                                            @foreach($parammetode as $key => $val)
                                                @if(!empty($val->hasil_pemeriksaan) && $val->hasil_pemeriksaan != "-" && $val->hasil_pemeriksaan != "0" && $val->hasil_pemeriksaan != "0.00" && $val->hasil_pemeriksaan != "0.0" && !empty($val->batas_bawah) && !empty($val->batas_atas) && is_numeric($val->hasil_pemeriksaan))
                                                    <div class="graph graph-nilai-metode-sama" id="graph-nilai-metode-sama-{{$key}}" data-json="{{json_encode($val)}}"></div>
                                                @endif
                                            <?php 
                                                $a++; 
                                                if($a == 4):$a=0;
                                                else:
                                                endif;
                                            ?>
                                            @endforeach
                                            </td>
                                        </tr>
                                    </table>
                                        @if(Request::get('x') == 'a')
                                            @php $siklus2 = '1'; @endphp
                                        @else
                                            @php $siklus2 = '2'; @endphp
                                        @endif
                                    <div class="footer-content" style="position: fixed; bottom: 0;  border-top: 1px solid black; padding-top: 2px; width: 100%;">
                                        <div class="left" style="font-size: 14px; font-weight: 100; font-family: arial; float: left;">
                                            GRAFIK SELURUH PESERTA PNPME KIMIA KLINIK SIKLUS {{$siklus}} - 0{{$siklus2}} TAHUN {{$tahun}}
                                        </div>
                                        <div style="text-align: right; float: right;">
                                            Lampiran @if($siklus2 == 1) 2 @else 3 @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    var graphList = $(".graph-nilai-sama");
    $.each(graphList,function(e,f){
        var thisContainer = $(this);
        console.log($(this).find('svg'));
        var id = $(this).attr('id');
        var dataJson =  $(this).data('json');
        dataJson.batas_bawah = dataJson.batas_bawah *1;
        dataJson.batas_atas = dataJson.batas_atas *1;
        dataJson.ring_1 = dataJson.ring_1 *1;
        dataJson.ring_2 = dataJson.ring_2 *1;
        dataJson.ring_3 = dataJson.ring_3 *1;
        dataJson.ring_4 = dataJson.ring_4 *1;
        dataJson.ring_5 = dataJson.ring_5 *1;
        dataJson.out_bawah = dataJson.out_bawah *1;
        dataJson.out_atas = dataJson.out_atas *1;
        dataJson.ring_1_bawah = dataJson.ring_1_bawah *1;
        dataJson.ring_1_atas = dataJson.ring_1_atas *1;
        dataJson.ring_2_bawah = dataJson.ring_2_bawah *1;
        dataJson.ring_2_atas = dataJson.ring_2_atas *1;
        dataJson.ring_3_bawah = dataJson.ring_3_bawah *1;
        dataJson.ring_3_atas = dataJson.ring_3_atas *1;
        dataJson.ring_4_bawah = dataJson.ring_4_bawah *1;
        dataJson.ring_4_atas = dataJson.ring_4_atas *1;
        dataJson.ring_5_bawah = dataJson.ring_5_bawah *1;
        dataJson.ring_5_atas = dataJson.ring_5_atas *1;
        var series = [
            {
                "name": "Distribusi Hasil",
                "colorByPoint": false,
                data : [
                    {
                        "name": "< " + dataJson.batas_bawah,
                        // "color": (dataJson.hasil_pemeriksaan < dataJson.batas_bawah ? '#1bbd20' : '#BF0B23'),
                        "color": '#BF0B23',
                        "y" : dataJson.out_bawah,
                        "a" : (dataJson.hasil_pemeriksaan < dataJson.batas_bawah ? '*' : '' )
                    },
                    {
                        "name": "" + dataJson.ring_1_bawah + " - " + dataJson.ring_1_atas,
                        // "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_1_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_1_atas ? '#1bbd20' : '#42b9ff'),
                        "y" : dataJson.ring_1,
                        "a" : (dataJson.hasil_pemeriksaan >= dataJson.ring_1_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_1_atas ? '*' : '' )
                    },
                    {
                        "name": "" + dataJson.ring_2_bawah + " - " + dataJson.ring_2_atas,
                        // "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_2_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_2_atas ? '#1bbd20' : '#42b9ff'),
                        "y" : dataJson.ring_2,
                        "a" : (dataJson.hasil_pemeriksaan >= dataJson.ring_2_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_2_atas ? '*' : '' )
                    },
                    {
                        "name": "" + dataJson.ring_3_bawah + " - " + dataJson.ring_3_atas,
                        // "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_3_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_3_atas ? '#1bbd20' : '#42b9ff'),
                        "color": '#1bbd20',
                        "y" : dataJson.ring_3,
                        "a" : (dataJson.hasil_pemeriksaan >= dataJson.ring_3_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_3_atas ? '*' : '' )
                    },
                    {
                        "name": "" + dataJson.ring_4_bawah + " - " + dataJson.ring_4_atas,
                        // "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_4_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_4_atas ? '#1bbd20' : '#42b9ff'),
                        "y" : dataJson.ring_4,
                        "a" : (dataJson.hasil_pemeriksaan >= dataJson.ring_4_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_4_atas ? '*' : '' )
                    },
                    {
                        "name": "" + dataJson.ring_5_bawah + " - " + dataJson.ring_5_atas,
                        // "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_5_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_5_atas ? '#1bbd20' : '#42b9ff'),
                        "y" : dataJson.ring_5,
                        "a" : (dataJson.hasil_pemeriksaan >= dataJson.ring_5_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_5_atas ? '*' : '' )
                    },
                    {
                        "name": "> " + dataJson.batas_atas,
                        // "color": (dataJson.hasil_pemeriksaan > dataJson.batas_atas ? '#1bbd20' : '#BF0B23'),
                        "color": '#BF0B23',
                        "y" : dataJson.out_atas,
                        "a" : (dataJson.hasil_pemeriksaan > dataJson.batas_atas ? '*' : '' )
                    },
                ]
            }
        ]
        var chart = Highcharts.chart(id, {
            exporting: { enabled: false },
            chart: {
                type: 'column',
                "events" : {
                    load: function () {
                        console.log("as");
                        this.oldhasUserSize = this.hasUserSize;
                        this.resetParams = [this.chartWidth, this.chartHeight, false];
                        this.setSize(340, 300, false);
                    },
                }
            },
            title: {
                text: 'Grafik Distribusi Hasil ' + dataJson.nama_parameter +'<br>Nilai Saudara : ' + dataJson.hasil_pemeriksaan,
                style : {
                    'font-size' : "10px",
                    'font-weight' : "bold",
                },
                useHtml : true
            },
            xAxis: {
                type: 'category',
                labels : {
                    style : {
                        'font-size' : "8px",
                        'font-weight' : "bold",
                    }
                }
            },
            yAxis: {
                title: {
                    text : 'Jumlah Peserta',
                    style : {
                        'font-size' : "8px"
                    }
                },
                labels : {
                    style : {
                        'font-size' : "8px",
                        'font-weight' : "bold",
                    }
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y} {point.a}'
                    }
                },
                column: {
                    dataLabels: {
                        enabled: true,
                        crop: false,
                        overflow: 'none'
                    }
                }

            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
            },

            "series":series
            
        })

    })

    var graphListAlat = $(".graph-nilai-alat-sama");
    $.each(graphListAlat,function(e,f){
        var thisContainer = $(this);
        console.log($(this).find('svg'));
        var id = $(this).attr('id');
        var dataJson =  $(this).data('json');
        dataJson.batas_bawah = dataJson.batas_bawah *1;
        dataJson.batas_atas = dataJson.batas_atas *1;
        dataJson.ring_1 = dataJson.ring_1 *1;
        dataJson.ring_2 = dataJson.ring_2 *1;
        dataJson.ring_3 = dataJson.ring_3 *1;
        dataJson.ring_4 = dataJson.ring_4 *1;
        dataJson.ring_5 = dataJson.ring_5 *1;
        dataJson.out_bawah = dataJson.out_bawah *1;
        dataJson.out_atas = dataJson.out_atas *1;
        dataJson.ring_1_bawah = dataJson.ring_1_bawah *1;
        dataJson.ring_1_atas = dataJson.ring_1_atas *1;
        dataJson.ring_2_bawah = dataJson.ring_2_bawah *1;
        dataJson.ring_2_atas = dataJson.ring_2_atas *1;
        dataJson.ring_3_bawah = dataJson.ring_3_bawah *1;
        dataJson.ring_3_atas = dataJson.ring_3_atas *1;
        dataJson.ring_4_bawah = dataJson.ring_4_bawah *1;
        dataJson.ring_4_atas = dataJson.ring_4_atas *1;
        dataJson.ring_5_bawah = dataJson.ring_5_bawah *1;
        dataJson.ring_5_atas = dataJson.ring_5_atas *1;
        var series = [
            {
                "name": "Distribusi Hasil",
                "colorByPoint": false,
                data : [
                    {
                        "name": "< " + dataJson.batas_bawah,
                        // "color": (dataJson.hasil_pemeriksaan < dataJson.batas_bawah ? '#1bbd20' : '#BF0B23'),
                        "color": '#BF0B23',
                        "y" : dataJson.out_bawah,
                        "a" : (dataJson.hasil_pemeriksaan < dataJson.batas_bawah ? '*' : '' )
                    },
                    {
                        "name": "" + dataJson.ring_1_bawah + " - " + dataJson.ring_1_atas,
                        // "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_1_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_1_atas ? '#1bbd20' : '#42b9ff'),
                        "y" : dataJson.ring_1,
                        "a" : (dataJson.hasil_pemeriksaan >= dataJson.ring_1_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_1_atas ? '*' : '' )
                    },
                    {
                        "name": "" + dataJson.ring_2_bawah + " - " + dataJson.ring_2_atas,
                        // "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_2_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_2_atas ? '#1bbd20' : '#42b9ff'),
                        "y" : dataJson.ring_2,
                        "a" : (dataJson.hasil_pemeriksaan >= dataJson.ring_2_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_2_atas ? '*' : '' )
                    },
                    {
                        "name": "" + dataJson.ring_3_bawah + " - " + dataJson.ring_3_atas,
                        // "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_3_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_3_atas ? '#1bbd20' : '#42b9ff'),
                        "color": '#1bbd20',
                        "y" : dataJson.ring_3,
                        "a" : (dataJson.hasil_pemeriksaan >= dataJson.ring_3_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_3_atas ? '*' : '' )
                    },
                    {
                        "name": "" + dataJson.ring_4_bawah + " - " + dataJson.ring_4_atas,
                        // "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_4_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_4_atas ? '#1bbd20' : '#42b9ff'),
                        "y" : dataJson.ring_4,
                        "a" : (dataJson.hasil_pemeriksaan >= dataJson.ring_4_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_4_atas ? '*' : '' )
                    },
                    {
                        "name": "" + dataJson.ring_5_bawah + " - " + dataJson.ring_5_atas,
                        // "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_5_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_5_atas ? '#1bbd20' : '#42b9ff'),
                        "y" : dataJson.ring_5,
                        "a" : (dataJson.hasil_pemeriksaan >= dataJson.ring_5_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_5_atas ? '*' : '' )
                    },
                    {
                        "name": "> " + dataJson.batas_atas,
                        // "color": (dataJson.hasil_pemeriksaan > dataJson.batas_atas ? '#1bbd20' : '#BF0B23'),
                        "color": '#BF0B23',
                        "y" : dataJson.out_atas,
                        "a" : (dataJson.hasil_pemeriksaan > dataJson.batas_atas ? '*' : '' )
                    },
                ]
            }
        ]
        var chart = Highcharts.chart(id, {
            exporting: { enabled: false },
            chart: {
                type: 'column',
                "events" : {
                    load: function () {
                        console.log("as");
                        this.oldhasUserSize = this.hasUserSize;
                        this.resetParams = [this.chartWidth, this.chartHeight, false];
                        this.setSize(340, 300, false);
                    },
                }
            },
            title: {
                text: 'Grafik Distribusi Hasil ' + dataJson.nama_parameter+ ' <br> Alat : ' + dataJson.nama_instrumen + '<br>Nilai Saudara : ' + dataJson.hasil_pemeriksaan,
                style : {
                    'font-size' : "10px",
                    'font-weight' : "bold",
                },
                useHtml : true
            },
            xAxis: {
                type: 'category',
                labels : {
                    style : {
                        'font-size' : "8px",
                        'font-weight' : "bold",
                    }
                }
            },
            yAxis: {
                title: {
                    text : 'Jumlah Peserta',
                    style : {
                        'font-size' : "8px"
                    }
                },
                labels : {
                    style : {
                        'font-size' : "8px",
                        'font-weight' : "bold",
                    }
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y} {point.a}'
                    }
                },
                column: {
                    dataLabels: {
                        enabled: true,
                        crop: false,
                        overflow: 'none'
                    }
                }

            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
            },

            "series":series
            
        })

    })

    var graphListMetode = $(".graph-nilai-metode-sama");
    $.each(graphListMetode,function(e,f){
        var thisContainer = $(this);
        console.log($(this).find('svg'));
        var id = $(this).attr('id');
        var dataJson =  $(this).data('json');
        dataJson.batas_bawah = dataJson.batas_bawah *1;
        dataJson.batas_atas = dataJson.batas_atas *1;
        dataJson.ring_1 = dataJson.ring_1 *1;
        dataJson.ring_2 = dataJson.ring_2 *1;
        dataJson.ring_3 = dataJson.ring_3 *1;
        dataJson.ring_4 = dataJson.ring_4 *1;
        dataJson.ring_5 = dataJson.ring_5 *1;
        dataJson.out_bawah = dataJson.out_bawah *1;
        dataJson.out_atas = dataJson.out_atas *1;
        dataJson.ring_1_bawah = dataJson.ring_1_bawah *1;
        dataJson.ring_1_atas = dataJson.ring_1_atas *1;
        dataJson.ring_2_bawah = dataJson.ring_2_bawah *1;
        dataJson.ring_2_atas = dataJson.ring_2_atas *1;
        dataJson.ring_3_bawah = dataJson.ring_3_bawah *1;
        dataJson.ring_3_atas = dataJson.ring_3_atas *1;
        dataJson.ring_4_bawah = dataJson.ring_4_bawah *1;
        dataJson.ring_4_atas = dataJson.ring_4_atas *1;
        dataJson.ring_5_bawah = dataJson.ring_5_bawah *1;
        dataJson.ring_5_atas = dataJson.ring_5_atas *1;
        var series = [
            {
                "name": "Distribusi Hasil",
                "colorByPoint": false,
                data : [
                    {
                        "name": "< " + dataJson.batas_bawah,
                        // "color": (dataJson.hasil_pemeriksaan < dataJson.batas_bawah ? '#1bbd20' : '#BF0B23'),
                        "color": '#BF0B23',
                        "y" : dataJson.out_bawah,
                        "a" : (dataJson.hasil_pemeriksaan < dataJson.batas_bawah ? '*' : '' )
                    },
                    {
                        "name": "" + dataJson.ring_1_bawah + " - " + dataJson.ring_1_atas,
                        // "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_1_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_1_atas ? '#1bbd20' : '#42b9ff'),
                        "y" : dataJson.ring_1,
                        "a" : (dataJson.hasil_pemeriksaan >= dataJson.ring_1_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_1_atas ? '*' : '' )
                    },
                    {
                        "name": "" + dataJson.ring_2_bawah + " - " + dataJson.ring_2_atas,
                        // "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_2_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_2_atas ? '#1bbd20' : '#42b9ff'),
                        "y" : dataJson.ring_2,
                        "a" : (dataJson.hasil_pemeriksaan >= dataJson.ring_2_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_2_atas ? '*' : '' )
                    },
                    {
                        "name": "" + dataJson.ring_3_bawah + " - " + dataJson.ring_3_atas,
                        // "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_3_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_3_atas ? '#1bbd20' : '#42b9ff'),
                        "color": '#1bbd20',
                        "y" : dataJson.ring_3,
                        "a" : (dataJson.hasil_pemeriksaan >= dataJson.ring_3_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_3_atas ? '*' : '' )
                    },
                    {
                        "name": "" + dataJson.ring_4_bawah + " - " + dataJson.ring_4_atas,
                        // "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_4_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_4_atas ? '#1bbd20' : '#42b9ff'),
                        "y" : dataJson.ring_4,
                        "a" : (dataJson.hasil_pemeriksaan >= dataJson.ring_4_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_4_atas ? '*' : '' )
                    },
                    {
                        "name": "" + dataJson.ring_5_bawah + " - " + dataJson.ring_5_atas,
                        // "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_5_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_5_atas ? '#1bbd20' : '#42b9ff'),
                        "y" : dataJson.ring_5,
                        "a" : (dataJson.hasil_pemeriksaan >= dataJson.ring_5_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_5_atas ? '*' : '' )
                    },
                    {
                        "name": "> " + dataJson.batas_atas,
                        // "color": (dataJson.hasil_pemeriksaan > dataJson.batas_atas ? '#1bbd20' : '#BF0B23'),
                        "color": '#BF0B23',
                        "y" : dataJson.out_atas,
                        "a" : (dataJson.hasil_pemeriksaan > dataJson.batas_atas ? '*' : '' )
                    },
                ]
            }
        ]
        var chart = Highcharts.chart(id, {
            exporting: { enabled: false },
            chart: {
                type: 'column',
                "events" : {
                    load: function () {
                        console.log("as");
                        this.oldhasUserSize = this.hasUserSize;
                        this.resetParams = [this.chartWidth, this.chartHeight, false];
                        this.setSize(340, 300, false);
                    },
                }
            },
            title: {
                text: 'Grafik Zscore ' + dataJson.nama_parameter+ ' <br> Metode : ' + dataJson.nama_metode + '<br> Nilai Z-Score Saudara : ' + dataJson.zscore,
                style : {
                    'font-size' : "10px",
                    'font-weight' : "bold",
                },
                useHtml : true
            },
            xAxis: {
                type: 'category',
                labels : {
                    style : {
                        'font-size' : "8px",
                        'font-weight' : "bold",
                    }
                }
            },
            yAxis: {
                title: {
                    text : 'Jumlah Peserta',
                    style : {
                        'font-size' : "8px"
                    }
                },
                labels : {
                    style : {
                        'font-size' : "8px",
                        'font-weight' : "bold",
                    }
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y} {point.a}'
                    }
                },
                column: {
                    dataLabels: {
                        enabled: true,
                        crop: false,
                        overflow: 'none'
                    }
                }

            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
            },

            "series":series
            
        })

    })

    function printDiv(divID) {
        var headElements = '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>,<meta name="viewport" content="width=device-width, initial-scale=1">';
        var options = { mode : "popup", popClose : true,extraHead : headElements };
        $( '#'+divID ).printArea( options );    
    }
    </script>
</body>
</html>
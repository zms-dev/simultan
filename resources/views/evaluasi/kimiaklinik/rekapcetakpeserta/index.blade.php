@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Hitung Z-Scroe Metode Kimia Klinik</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Kode Lab</th>
                                <th>Parameter</th>
                                <th>Metode Pemeriksaan</th>
                                <th>Hasil</th>
                                <th>Z-Score</th>
                                @if(count($zscore))
                                <th>Keterangan</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $val)
                            <tr>
                                <td>{{$val->kode_lab}}</td>
                                <td>
                                    {{$val->nama_parameter}}
                                    <input type="hidden" name="parameter[]" value="{{$val->id_parameter}}">
                                </td>
                                <input type="hidden" name="metode[]" value="{{$val->id}}">
                                <td>{!!$val->metode_pemeriksaan!!} / {{$val->metode_lain}}</td>
                                <td>{{$val->hasil_pemeriksaan}}</td>
                                <td>
                                    <?php
                                        if (empty($val->sd[0])) {
                                            $z = $val->hasil_pemeriksaan;
                                        }else{
                                            $z = ($val->hasil_pemeriksaan - $val->sd[0]->median) / $val->sd[0]->sd;
                                        }
                                    ?>
                                    @if(count($zscore))
                                        @foreach($zscore as $za)
                                            @if($za->parameter == $val->id_parameter)
                                                @if($za->metode == $val->id)
                                                @if($za->zscore == 'Tidak di analisis')
                                                Tidak di analisis
                                                @else
                                                {{number_format($za->zscore, 1)}}
                                                @endif
                                                @endif
                                            @endif
                                        @endforeach
                                    @else
                                        @if($val->hasil_pemeriksaan == NULL)
                                        Tidak di analisis
                                        <input type="hidden" name="zscore[]" value="Tidak di analisis">
                                        @else
                                        {{number_format($z, 1)}}
                                        <input type="hidden" name="zscore[]" value="{{number_format($z, 1)}}">
                                        @endif
                                    @endif
                                </td>
                                @if(count($zscore))
                                    @foreach($zscore as $za)
                                        @if($za->parameter == $val->id_parameter)
                                            @if($za->metode == $val->id)
                                                <td>
                                                    <?php 
                                                        if($za->zscore <= 2 && $za->zscore >= -2){
                                                            $keterangan = 'Memuaskan';
                                                        }else if($za->zscore <= 3 && $za->zscore >= -3){
                                                            $keterangan = 'Peringatan';
                                                        }else{
                                                            $keterangan = 'Tidak Memuaskan';
                                                        }
                                                    ?>
                                                    {{$keterangan}}
                                                </td>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @if(count($zscore))
                @else
                <input type="submit" name="simpan" class="btn btn-primary" value="Simpan">
                @endif
                {{ csrf_field() }}
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
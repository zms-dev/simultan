<style type="text/css">
@page {
margin: 150px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 0px 5px;
    text-align: left;
    border: 1px solid #333;
}

#header { 
    position: fixed; 
    border-bottom:1px solid gray;
    padding-top: -140px
}
.footer { 
    position: fixed; 
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}

.table-bordered tbody td{
    height:30px;

}
</style>
<div id="header">
    <table width="100%" cellpadding="0" border="0" >
        <thead>
            <tr>
                <th width="11%"></th>
                <th>
                   <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px"> 
                </th>
                <td width="50%" style="padding-left: 30px;" >
                    <span style="font-size: 14px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 12px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL KIMIA KLINIK SIKLUS {{$siklus}}  TAHUN {{$date}}</b></span><br>
                    <span style="font-size: 12px">Penyelenggara : <b>Balai Besar Laboratorium Kesehatan Surabaya</b></span>
                    <div style="padding-left: 90px; margin-top: -15px; font-size: 12px">
                        <span><br/>Jalan Karangmenjangan No, 18 Surabaya - 60286<br>Telepon : (031) 5021451 | Fax : (031) 5020388<br>Web Online : simultan.bblksurabaya.id <br>Email : pme.bblksub@gmail.com</span>
                    </div>
                </td>
                <th width="%"></th>
                <th>
                    <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" height="90px">
                </th>

                
            </tr>
            <tr>
                <th colspan="6"><hr></th>
            </tr>
        </thead>
    </table>
</div>
<div class="footer">
    EVALUASI PNPME KIMIA KLINIK SIKLUS {{$siklus}} -@if($type == 'a') 01 @else 02 @endif TAHUN {{$date}}
</div>
<div class="footer" style="text-align: right;">
    Lampiran @if($type == 'a') 1 @else 2 @endif
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body">
                    <label>
                        <center>
                            <font size="10px">
                                <b>HASIL EVALUASI PNPME KIMIA KLINIK SIKLUS {{$siklus}} -@if($type == 'a') 01 @else 02 @endif TAHUN {{$date}}</b><br>
                                KODE PESERTA : {{$datas->kode_lab}}
                            </font>
                        </center>
                    </label><br>
                <table class="table utama table-bordered" border="1" cellspacing="0" cellpadding="1">
                        <thead>
                            <tr>
                                <th rowspan="2" width="2%" style="text-align:center;">No.</th>
                                <th rowspan="2" width="15%" style="text-align:center;">Parameter</th>
                                <th rowspan="2" width="10%" style="text-align:center;">Instrument</th>
                                <th rowspan="2" width="15%" style="text-align:center;">Metode Pemeriksaan</th>
                                <th rowspan="2" width="5%" style="text-align:center;">Hasil Saudara</th>
                                @if(count($zscore))
                                <th colspan="4" style="text-align:center;">Peserta</th>
                                @endif
                                @if(count($zscoremetode))
                                <th colspan="4" style="text-align:center;">Metode</th>
                                @else
                                <th colspan="2" style="text-align:center;">Metode</th>
                                @endif
                                @if(count($zscorealat))
                                <th colspan="4" style="text-align:center;">Alat / Instrumen</th>
                                @else
                                <th colspan="2" style="text-align:center;">Alat / Instrumen</th>
                                @endif
                            </tr>
                            <tr>
                                <th width="5%" style="text-align:center;">Target</th>
                                <th width="5%" style="text-align:center;">n</th>
                                <th width="5%" style="text-align:center;">Z-Score</th>
                                @if(count($zscore))
                                <th width="10%" style="text-align:center;">Keterangan</th>
                                @endif
                                <th width="5%" style="text-align:center;">Target</th>
                                <th width="5%" style="text-align:center;">n</th>
                                <th width="5%" style="text-align:center;">Z-Score</th>
                                @if(count($zscoremetode))
                                <th width="10%" style="text-align:center;">Keterangan</th>
                                @endif
                                <th width="5%" style="text-align:center;">Target</th>
                                <th width="5%" style="text-align:center;">n</th>
                                <th width="5%" style="text-align:center;">Z-Score</th>
                                @if(count($zscorealat))
                                <th  width="10%" style="text-align:center;">Keterangan</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody><?php $no = 1;?>
                            @foreach($data as $val)
                            <?php
                                $labelHasil = $val->hasil_pemeriksaan;
                                try{
                                    $val->hasil_pemeriksaan = $val->hasil_pemeriksaan  *1;
                                }catch(\Exception $e){
                                    $val->hasil_pemeriksaan = $val->hasil_pemeriksaan;
                                }
                                if($val->hasil_pemeriksaan == "-" || $val->hasil_pemeriksaan == null || $val->hasil_pemeriksaan == '0'  || $val->hasil_pemeriksaan == '0.00' ||  $val->hasil_pemeriksaan == '0.0' || empty($val->hasil_pemeriksaan) || is_string($val->hasil_pemeriksaan)){
                                    if($val->hasil_pemeriksaan == '0' || $val->hasil_pemeriksaan == '0.0' || $val->hasil_pemeriksaan == '0.00' || is_string($val->hasil_pemeriksaan)){
                                        $val->hasil_pemeriksaan = $val->hasil_pemeriksaan;
                                    }else{
                                        $val->hasil_pemeriksaan = NULL;
                                    }
                                    $target_parameter = "-";
                                    $zscore_parameter = "-";
                                    $keteranganParameter = "Tidak dianalisis";
                                    $target_metode = "-";
                                    $zscore_metode = "-";
                                    $keteranganMetode = "Tidak dianalisis";
                                    $target_alat = "-";
                                    $zscore_alat = "-";
                                    $keteranganAlat = "Tidak dianalisis";
                                }else{
                                    $target_parameter = (!empty($val->sd) ? (!empty($val->sd->median) ? $val->sd->median : '-') : '-');
                                    $sd_parameter = (!empty($val->sd) ? (!empty($val->sd->sd) ? $val->sd->sd : '-') : '-');
                                    $zscore_parameter = '-';
                                    if(($target_parameter != '-' && $sd_parameter != '-')){
                                        $zscore_parameter = number_format(($val->hasil_pemeriksaan - $val->sd->median) / $val->sd->sd,2);
                                    }
                                    $keteranganParameter = "";
                                    if($zscore_parameter == '-'){
                                        $keteranganParameter = 'Tidak Dievaluasi';
                                    }else if($zscore_parameter <= 2 && $zscore_parameter >= -2){
                                        $keteranganParameter = 'Memuaskan';
                                    }else if($zscore_parameter <= 3 && $zscore_parameter >= -3){
                                        $keteranganParameter = 'Peringatan';
                                    }else{
                                        $keteranganParameter = 'Tidak Memuaskan';
                                    }
                                    $target_metode = (!empty($val->sdmetode) ? (!empty($val->sdmetode->median) ? $val->sdmetode->median : '-') : '-');
                                    $sd_metode = (!empty($val->sdmetode) ? (!empty($val->sdmetode->sd) ? $val->sdmetode->sd : '-') : '-');
                                    $zscore_metode = '-';
                                    if(($target_metode != '-' && $sd_metode != '-')){
                                        $zscore_metode = number_format(($val->hasil_pemeriksaan - $val->sdmetode->median) / $val->sdmetode->sd,2);
                                    }
                                    $keteranganMetode = "";
                                    if($zscore_metode == '-'){
                                        $keteranganMetode = 'Tidak Dievaluasi';
                                    }else if($zscore_metode <= 2 && $zscore_metode >= -2){
                                        $keteranganMetode = 'Memuaskan';
                                    }else if($zscore_metode <= 3 && $zscore_metode >= -3){
                                        $keteranganMetode = 'Peringatan';
                                    }else{
                                        $keteranganMetode = 'Tidak Memuaskan';
                                    }

                                    $target_alat = (!empty($val->sdalat) ? (!empty($val->sdalat->median) ? $val->sdalat->median : '-') : '-');
                                    $sd_alat = (!empty($val->sdalat) ? (!empty($val->sdalat->sd) ? $val->sdalat->sd : '-') : '-');
                                    $zscore_alat = '-';
                                    if(($target_alat != '-' && $sd_alat != '-')){
                                        $zscore_alat = number_format(($val->hasil_pemeriksaan - $val->sdalat->median) / $val->sdalat->sd,2);
                                    }
                                    $keteranganAlat = "";
                                    if($zscore_alat == '-'){
                                        $keteranganAlat = 'Tidak Dievaluasi';
                                    }else if($zscore_alat <= 2 && $zscore_alat >= -2){
                                        $keteranganAlat = 'Memuaskan';
                                    }else if($zscore_alat <= 3 && $zscore_alat >= -3){
                                        $keteranganAlat = 'Peringatan';
                                    }else{
                                        $keteranganAlat = 'Tidak Memuaskan';
                                    }
                                    
                                    
                                }
                                
                            ?>
                            <tr>
                                <td>{{$no++}}</td>
                                <td>
                                    {{$val->nama_parameter}}
                                    <input type="hidden" name="parameter[]" value="{{$val->id}}">
                                    <input type="hidden" name="zscore[]" value="{{$zscore_parameter}}">
                                    <input type="hidden" name="zscoremetode[]" value="{{$zscore_metode}}">
                                    <input type="hidden" name="zscorealat[]" value="{{$zscore_alat}}">
                                </td>
                                <td>
                                    @if($val->instrumen == NULL) {{(!empty($val->Instrumen) ? $val->Instrumen : '-')}} @else {{$val->instrumen}} @endif  {{(!empty($val->instrument_lain) ? $val->instrument_lain : '' )}}
                                    <input type="hidden" name="alat[]" value="{{$val->idalat}}">
                                </td>
                                <td>
                                    {!!(!empty($val->metode_pemeriksaan) ? $val->metode_pemeriksaan : '-')!!} {{$val->metode_lain}}
                                    <input type="hidden" name="metode[]" value="{{$val->idmetode}}">
                                </td>
                                <td style="text-align:center;">{{($labelHasil)}}</td>
                                <td style="text-align:center;">
                                    {{$target_parameter}}
                                </td>
                                <td style="text-align:center;">
                                    @if($target_parameter == '-') - @else {{$val->totalp}} @endif
                                </td>
                                <td style="text-align:center;">
                                    {{$zscore_parameter}}
                                </td>
                                <td style="text-align:center;">
                                    {{$keteranganParameter}}
                                </td>
                                <td style="text-align:center;">
                                    {{$target_metode}}
                                </td>
                                <td style="text-align:center;">
                                    @if($val->metode_lain != NULL || $val->totalm == '0') - @else {{$val->totalm}} @endif
                                </td>
                                <td style="text-align:center;">
                                    {{$zscore_metode}}
                                </td>
                                <td style="text-align:center;">
                                    {{$keteranganMetode}}
                                </td>
                                <td style="text-align:center;">
                                    {{$target_alat}}
                                </td>
                                <td style="text-align:center;">
                                    @if(empty($val->Instrumen) || $val->Instrumen == 'Tidak Mengerjakan' || $val->instrument_lain != NULL || $val->totala == '0') - @else {{$val->totala}} @endif
                                </td>
                                <td style="text-align:center;">
                                    {{$zscore_alat}}
                                </td>
                                <td style="text-align:center;">
                                    {{$keteranganAlat}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <br>
                <table border="0">
                    <tr>
                        <th>Komentar / Saran: </th>
                    </tr>
                    <tr>
                        <td>
                            Pertahankan hasil Saudara yang sudah "memuaskan". Untuk hasil "peringatan" dan "tidak memuaskan" agar dilakukan evaluasi hasil PMI, proses praanalitik (penanganan sempel), analitik dan pasca analitik (penginputan hasil)
                        </td>
                    </tr>
                </table>
                <table width="100%" border="">
                    <tr>
                        <td width="50%" style="vertical-align:top;padding-top:20px;">
                            Kriteria Nilai : 
                            <ul>
                                <li><span style="width:80px;display:inline-block;"><b>|</b> Z Score |<span style="font-family: DejaVu Sans; sans-serif;">&le;</span> 2</span> = Memuaskan</li>
                                <li><span style="width:80px;display:inline-block;">2 &lt;<b>|</b> Z Score|<span style="font-family: DejaVu Sans; sans-serif;">&le;</span> 3</span> = Peringatan</li>
                                <li><span style="width:80px;display:inline-block;"><b>|</b> Z Score |&gt; 3</span> = Tidak Memuaskan</li>
                            </ul>
                        </td>
                        <td width="50%" style="vertical-align:top;">
                        <div style="position: relative;left: 73%; top: -3%;">
                        <p>
                        <div style="position: relative; top: 22px">
                        Surabaya, {{$ttd->tanggal}} {{$date}}<br>
                        Manajer Teknis
                        </div>
                        <p style="margin-left: 8px !important">
                            <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.'ttdevaluasi'.DIRECTORY_SEPARATOR.$ttd->image)}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.'ttdevaluasi'.DIRECTORY_SEPARATOR.$ttd->image)}}" width="120" height="120" style="margin-left: -20px !important;">
                        </p>
                        <div style="position: relative; top:-40px">
                            {{$ttd->nama}}<br>NIP. {{$ttd->nip}}
                        </div>
                        </p>
                        </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
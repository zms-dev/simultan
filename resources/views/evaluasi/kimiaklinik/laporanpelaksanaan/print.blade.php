<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama, table.utama td, table.utama th {    
    border: 1px solid #ddd;
    text-align: left;
}
table.utama th.no-border{
    border:none;
}

table.utama {
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td {
    padding: 5px;
    min-height:50px;
}
</style>
<table width="100%" class="utama">
    <thead>
        <tr>
            <th colspan="10" class="no-border">
                <center>LAPORAN PELAKSAAN PNPME BIDANG PATOLOGI</center>
            </th>
        </tr>
        <tr>
            <th colspan="10" class="no-border">
                <center>BALAI BESAR LABORATORIUM KESEHATAN SURABAYA</center>
            </th>
        </tr>
        <tr>
            <th colspan="10" class="no-border">
                <center>SIKLUS : {{$input['siklus']}}</center>
            </th>
        </tr>
        <tr>
            <th colspan="10" class="no-border">
                <center>TAHUN : {{$input['tahun']}}</center>
            </th>
        </tr>
        <tr>
            <th rowspan="2">NO</th>
            <th rowspan="2">Kode Lab</th>
            <th rowspan="2">Provinsi</th>
            <th rowspan="2">Tanggal Terima Bahan</th>
            <th colspan="2">Kualitas Bahan</th>
            <th colspan="2">Tanggal Mengerjakan</th>
            <th colspan="2">Tanggal Kirim Hasil</th>
        </tr>
        <tr>
            <th>01</th>
            <th>02</th>
            <th>01</th>
            <th>02</th>
            <th>01</th>
            <th>02</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1;?>
        @foreach($data as $val)
        <tr>
            <td>{{$no}}</td>
            <td>{{$val->kode_lab}}</td>
            <td>{{$val->name}}</td>
            <td>{{$val->tgl_penerimaan}}</td>
            @if(!empty($val->kualitas_bahan_a))
                <td style="text-transform: capitalize;">{{$val->kualitas_bahan_a}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
            @if(!empty($val->kualitas_bahan_b))
                <td style="text-transform: capitalize;">{{$val->kualitas_bahan_b}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
            @if(!empty($val->tgl_pemeriksaan_a))
                <td style="text-transform: capitalize;">{{$val->tgl_pemeriksaan_a}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
            @if(!empty($val->tgl_pemeriksaan_b))
                <td style="text-transform: capitalize;">{{$val->tgl_pemeriksaan_b}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
            @if(!empty($val->tgl_pemeriksaan_a))
                <td style="text-transform: capitalize;">{{$val->tgl_kirim_a}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
            @if(!empty($val->tgl_pemeriksaan_b))
                <td style="text-transform: capitalize;">{{$val->tgl_kirim_b}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
        </tr>
        <?php $no++;?>
        @endforeach
    </tbody>
</table>
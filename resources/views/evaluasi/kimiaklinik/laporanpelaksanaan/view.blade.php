<table border="1">
    <thead>
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">Kode Lab</th>
            <th rowspan="2">Provinsi</th>
            <th rowspan="2">Tanggal Terima Bahan</th>
            <th colspan="2">Kualitas Bahan</th>
            <th colspan="2">Tanggal Mengerjakan</th>
            <th colspan="2">Tanggal Kirim Hasil</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th>01</th>
            <th>02</th>
            <th>01</th>
            <th>02</th>
            <th>01</th>
            <th>02</th>
        </tr>
    </thead>
    <tbody>
        @php 
            $no = 1;
        @endphp
        @foreach($data as $val)
        <tr>
            <td>{{$no++}}</td>
            <td>{{$val->kode_lab}}</td>
            <td>{{$val->name}}</td>
            <td>{{$val->tgl_penerimaan}}</td>
            @if(!empty($val->kualitas_bahan_a))
                <td style="text-transform: capitalize;">{{$val->kualitas_bahan_a}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
            @if(!empty($val->kualitas_bahan_b))
                <td style="text-transform: capitalize;">{{$val->kualitas_bahan_b}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
            @if(!empty($val->tgl_pemeriksaan_a))
                <td style="text-transform: capitalize;">{{$val->tgl_pemeriksaan_a}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
            @if(!empty($val->tgl_pemeriksaan_b))
                <td style="text-transform: capitalize;">{{$val->tgl_pemeriksaan_b}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
            @if(!empty($val->tgl_pemeriksaan_a))
                <td style="text-transform: capitalize;">{{$val->tgl_kirim_a}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
            @if(!empty($val->tgl_pemeriksaan_b))
                <td style="text-transform: capitalize;">{{$val->tgl_kirim_b}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
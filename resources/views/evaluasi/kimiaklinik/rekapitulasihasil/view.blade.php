<style type="text/css">
body{
    font-family: Open-sans;
    font-size: 10px;
}
td, th{
    border: 1px solid #333;
    text-align: center;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 2px 5px;
}
.footer{
  position: fixed;

}
footer {
    position: fixed;
    bottom: -25px;
    left: 0px;
    right: 0px;
    height: 50px;

    /** Extra personal styles **/
    /* background-color: #03a9f4;
    /* color: white; */
    text-align: left;
    line-height: 35px;
}
</style>
<body>
  

<main>
  <?php $no = 0;?>
  <?php $halaman = 4;?>
  @foreach($parameter as $val)
  <?php $no++; ?>
  @if($no == 5)
  <?php $no = 0;?>
  <?php $no++; ?>
  <?php $halaman++; ?>
  <div style="page-break-before:always">
  @else
  <div>
  @endif
  <h4>Rekapitulasi Hasil PNPME Parameter {!!$val->terbilang!!} Siklus {{$input['siklus']}} @if($input['periode'] == 2)Periode 2 @endif</h4>
  <table border="1">
      <thead>
          <tr>
              <th rowspan="3" width="20%">Nilai</th>
              <th rowspan="3">Kriteria</th>
              <th colspan="6">Siklus {{$input['siklus']}}.01</th>
              <th colspan="6">Siklus {{$input['siklus']}}.02</th>
          </tr>
          <tr>
              <th colspan="2">Peserta</th>
              <th colspan="2">Metode</th>
              <th colspan="2">Alat</th>
              <th colspan="2">Peserta</th>
              <th colspan="2">Metode</th>
              <th colspan="2">Alat</th>
          </tr>
          <tr>
              <th>Jumlah</th>
              <th>%</th>
              <th>Jumlah</th>
              <th>%</th>
              <th>Jumlah</th>
              <th>%</th>
              <th>Jumlah</th>
              <th>%</th>
              <th>Jumlah</th>
              <th>%</th>
              <th>Jumlah</th>
              <th>%</th>
          </tr>
      </thead>
      <tbody>
          <tr>
              <td>| Z Score | <span style="font-family: DejaVu Sans; sans-serif;">&le;</span> 2</td>
              <td>Memuaskan</td>
              <td>
                  {{$val->datapesertaa1->MEMUASKAN}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->datapesertaa1->MEMUASKAN / ($val->datapesertaa1->MEMUASKAN + $val->datapesertaa1->PERINGATAN + $val->datapesertaa1->TIDAKS_MEMUASKAN)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
              <td>
                  {{$val->datametodea1->MEMUASKAN_METODE}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->datametodea1->MEMUASKAN_METODE / ($val->datametodea1->MEMUASKAN_METODE + $val->datametodea1->PERINGATAN_METODE + $val->datametodea1->TIDAKS_MEMUASKAN_METODE)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
              <td>
                  {{$val->dataalata1->MEMUASKAN_ALAT}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->dataalata1->MEMUASKAN_ALAT / ($val->dataalata1->MEMUASKAN_ALAT + $val->dataalata1->PERINGATAN_ALAT + $val->dataalata1->TIDAKS_MEMUASKAN_ALAT)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
              <td>
                  {{$val->datapesertaa2->MEMUASKAN}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->datapesertaa2->MEMUASKAN / ($val->datapesertaa2->MEMUASKAN + $val->datapesertaa2->PERINGATAN + $val->datapesertaa2->TIDAKS_MEMUASKAN)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
              <td>
                  {{$val->datametodea2->MEMUASKAN_METODE}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->datametodea2->MEMUASKAN_METODE / ($val->datametodea2->MEMUASKAN_METODE + $val->datametodea2->PERINGATAN_METODE + $val->datametodea2->TIDAKS_MEMUASKAN_METODE)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
              <td>
                  {{$val->dataalata2->MEMUASKAN_ALAT}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->dataalata2->MEMUASKAN_ALAT / ($val->dataalata2->MEMUASKAN_ALAT + $val->dataalata2->PERINGATAN_ALAT + $val->dataalata2->TIDAKS_MEMUASKAN_ALAT)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
          </tr>
          <tr>
              <td>2 &lt; | Z Score | <span style="font-family: DejaVu Sans; sans-serif;">&le;</span> 3</td>
              <td>Peringatan</td>
              <td>
                  {{$val->datapesertaa1->PERINGATAN}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->datapesertaa1->PERINGATAN / ($val->datapesertaa1->MEMUASKAN + $val->datapesertaa1->PERINGATAN + $val->datapesertaa1->TIDAKS_MEMUASKAN)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
              <td>
                  {{$val->datametodea1->PERINGATAN_METODE}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->datametodea1->PERINGATAN_METODE / ($val->datametodea1->MEMUASKAN_METODE + $val->datametodea1->PERINGATAN_METODE + $val->datametodea1->TIDAKS_MEMUASKAN_METODE)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
              <td>
                  {{$val->dataalata1->PERINGATAN_ALAT}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->dataalata1->PERINGATAN_ALAT / ($val->dataalata1->MEMUASKAN_ALAT + $val->dataalata1->PERINGATAN_ALAT + $val->dataalata1->TIDAKS_MEMUASKAN_ALAT)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
              <td>
                  {{$val->datapesertaa2->PERINGATAN}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->datapesertaa2->PERINGATAN / ($val->datapesertaa2->MEMUASKAN + $val->datapesertaa2->PERINGATAN + $val->datapesertaa2->TIDAKS_MEMUASKAN)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
              <td>
                  {{$val->datametodea2->PERINGATAN_METODE}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->datametodea2->PERINGATAN_METODE / ($val->datametodea2->MEMUASKAN_METODE + $val->datametodea2->PERINGATAN_METODE + $val->datametodea2->TIDAKS_MEMUASKAN_METODE)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
              <td>
                  {{$val->dataalata2->PERINGATAN_ALAT}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->dataalata2->PERINGATAN_ALAT / ($val->dataalata2->MEMUASKAN_ALAT + $val->dataalata2->PERINGATAN_ALAT + $val->dataalata2->TIDAKS_MEMUASKAN_ALAT)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
          </tr>
          <tr>
              <td>| Z Score | > 3</td>
              <td>Tidak Memuaskan</td>
              <td>
                  {{$val->datapesertaa1->TIDAKS_MEMUASKAN}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->datapesertaa1->TIDAKS_MEMUASKAN / ($val->datapesertaa1->MEMUASKAN + $val->datapesertaa1->PERINGATAN + $val->datapesertaa1->TIDAKS_MEMUASKAN)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
              <td>
                  {{$val->datametodea1->TIDAKS_MEMUASKAN_METODE}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->datametodea1->TIDAKS_MEMUASKAN_METODE / ($val->datametodea1->MEMUASKAN_METODE + $val->datametodea1->PERINGATAN_METODE + $val->datametodea1->TIDAKS_MEMUASKAN_METODE)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
              <td>
                  {{$val->dataalata1->TIDAKS_MEMUASKAN_ALAT}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->dataalata1->TIDAKS_MEMUASKAN_ALAT / ($val->dataalata1->MEMUASKAN_ALAT + $val->dataalata1->PERINGATAN_ALAT + $val->dataalata1->TIDAKS_MEMUASKAN_ALAT)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
              <td>
                  {{$val->datapesertaa2->TIDAKS_MEMUASKAN}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->datapesertaa2->TIDAKS_MEMUASKAN / ($val->datapesertaa2->MEMUASKAN + $val->datapesertaa2->PERINGATAN + $val->datapesertaa2->TIDAKS_MEMUASKAN)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
              <td>
                  {{$val->datametodea2->TIDAKS_MEMUASKAN_METODE}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->datametodea2->TIDAKS_MEMUASKAN_METODE / ($val->datametodea2->MEMUASKAN_METODE + $val->datametodea2->PERINGATAN_METODE + $val->datametodea2->TIDAKS_MEMUASKAN_METODE)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
              <td>
                  {{$val->dataalata2->TIDAKS_MEMUASKAN_ALAT}}
              </td>
              <td>
                  <?php
                      $persen1 = ($val->dataalata2->TIDAKS_MEMUASKAN_ALAT / ($val->dataalata2->MEMUASKAN_ALAT + $val->dataalata2->PERINGATAN_ALAT + $val->dataalata2->TIDAKS_MEMUASKAN_ALAT)) * 100;
                      echo number_format($persen1, 2)."%";
                  ?>
              </td>
          </tr>
          <tr>
              <td colspan="2" style="text-align: center;">JUMLAH</td>
              <td>{{$val->datapesertaa1->MEMUASKAN + $val->datapesertaa1->PERINGATAN + $val->datapesertaa1->TIDAKS_MEMUASKAN}}</td>
              <td>100%</td>
              <td>{{$val->datametodea1->MEMUASKAN_METODE + $val->datametodea1->PERINGATAN_METODE + $val->datametodea1->TIDAKS_MEMUASKAN_METODE}}</td>
              <td>100%</td>
              <td>{{$val->dataalata1->MEMUASKAN_ALAT + $val->dataalata1->PERINGATAN_ALAT + $val->dataalata1->TIDAKS_MEMUASKAN_ALAT}}</td>
              <td>100%</td>
              <td>{{$val->datapesertaa2->MEMUASKAN + $val->datapesertaa2->PERINGATAN + $val->datapesertaa2->TIDAKS_MEMUASKAN}}</td>
              <td>100%</td>
              <td>{{$val->datametodea2->MEMUASKAN_METODE + $val->datametodea2->PERINGATAN_METODE + $val->datametodea2->TIDAKS_MEMUASKAN_METODE}}</td>
              <td>100%</td>
              <td>{{$val->dataalata2->MEMUASKAN_ALAT + $val->dataalata2->PERINGATAN_ALAT + $val->dataalata2->TIDAKS_MEMUASKAN_ALAT}}</td>
              <td>100%</td>
          </tr>
      </tbody>
  </table>
  </div>
  <footer class="footer">PNPME-BBLK SURABAYA <span style="margin-left: 500px;">Halaman {{$halaman}} dari 43</span></footer>
  @endforeach
</main>
</body>

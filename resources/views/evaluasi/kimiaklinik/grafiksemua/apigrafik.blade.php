<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <script src="{{URL::asset('asset/js/jquery.min.js')}}"></script>
  <script src="{{URL::asset('asset/js/highstock.js')}}"></script>
  <script src="{{URL::asset('asset/js/exporting.js')}}"></script>
  <script src="{{URL::asset('asset/js/export-data.js')}}"></script>
  <script src="{{URL::asset('asset/js/highcharts-3d.js')}}"></script>
  <link rel="stylesheet" href="{{URL::asset('asset/css/bootstrap.css')}}">
  <style type="text/css">
      body{
          font-family: arial
      }
  </style>
</head>
<body>
  <div class="container-fluid">
      <div class="row">
          <div class="col-md-10 col-md-offset-1">
              <div class="panel panel-default">
                  <div class="panel-body" id="datana">
                     <style type="text/css">
                      @media print{          
                          #color-red{
                              -webkit-print-color-adjust: exact!important;
                              background: #1bbd20 !important;display:inline-block;width:15px;height:25px;
                          }
                          .col-md-4{
                            width: 33.3% !important;
                            float: left !important;
                          }
                      }
                    </style>
                    <div class="row">
                      <div class="col-md-12">
                        <h5>Kode Peserta : {{$reg->kode_lebpes}}</h5>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div id="container"></div>
                      </div>
                      <div class="col-md-4">
                        <div id="container_alat"></div>
                      </div>
                      <div class="col-md-4">
                        <div id="container_metode"></div>
                      </div>
                      <div class="col-md-4">
                        <div id="container1"></div>
                      </div>
                      <div class="col-md-4">
                        <div id="container_alat1"></div>
                      </div>
                      <div class="col-md-4">
                        <div id="container_metode1"></div>    
                      </div>
                    </div>
                    <br>
                    <blockquote style="font-size: 14px">
                      <table>
                        <tr>
                          <td>Catatan : &nbsp;</td>
                          <td>- | Z Score | <= 2 </td>
                          <td>&nbsp; = Memuaskan</td>
                          <td width="20%" id="color-red" style="text-align: right;">&nbsp;<kbd class="color-green" style="background-color: #67ca3b;"></kbd>&nbsp;</td>
                          <td>&nbsp;Nilai Target</td>
                        </tr>
                        <tr>
                          <td></td>
                          <td>- 2 < | Z Score | <= 3 </td>
                          <td>&nbsp; = Peringatan</td>
                          <td style="text-align: right;">*&nbsp;</td>
                          <td>&nbsp;Posisi Peserta</td>
                        </tr>
                        <tr>
                          <td></td>
                          <td>- | Z Score | >= 3 </td>
                          <td>&nbsp; = Tidak Memuaskan</td>
                          <td></td>
                          <td></td>
                        </tr>
                      </table>
                    </blockquote>
                  </div>
                    <a style="margin: 10px" onclick="javascript:printDiv('datana')" class="btn btn-primary">Print</a>
              </div>
          </div>
      </div>
  </div>
  <script src="{{URL('/js/jquery.printarea.js')}}"></script>
  <script src="https://code.highcharts.com/modules/pattern-fill.js"></script>
  <script type="text/javascript">
  function printDiv(divID) {
      var headElements = '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>,<meta name="viewport" content="width=device-width, initial-scale=1">';
      var options = { mode : "popup", popClose : true,extraHead : headElements };
      $( '#'+divID ).printArea( options );    
  }

  var chart1 = Highcharts.chart('container', {
    @foreach($data as $val)
    title: {
      text: '01 - {!!$param->nama_parameter!!}',
      style : {
          'font-size' : "10px",
          'font-weight' : "bold",
      }
    },
    @break
    @endforeach
    subtitle: {
      text: ''
    },
    xAxis: {
      categories: [
        @foreach($data as $val)
        '{{$val->nilainya}}',
        @endforeach],
      labels: {
            style: {
                fontSize: '7px',
                color: '#000',
            }
      }
    },
    yAxis: {
      title: {
        text : 'Jumlah Peserta',
      }
    },
    plotOptions: {
        series: {
            borderWidth: 0,dataLabels: {
                enabled: true,
                format:'{point.y} {point.a}',
                y: 0,
                crop: false,
                overflow: 'none',
                style: {
                    fontSize : '10px'
                }
            }
        }
    },
    series: [{
        showInLegend: false,  
        type: 'column',
        name : 'Z-score',
        data: [
        @foreach($data as $val)
            {
            y: {{$val->nilai1}},
            <?php if($val->nilainya == '0.0') { ?>
                  color: '#67ca3b',
            <?php } ?>
            <?php if($val->nilainya == '< -3' || $val->nilainya == '> 3') { ?>
                  color: '#ca3b3b',
            <?php } ?>
            @foreach($datap as $valu)
              <?php if($valu->nilainya == $val->nilainya) { ?>
                <?php if($valu->nilai1 == '1') { ?>
                  a: "*",
                <?php } ?>
              <?php } ?>
            @endforeach
            },
        @endforeach
        ],
      }]
  });

  var chart4 = Highcharts.chart('container1', {
    @foreach($data_b as $val)
    title: {
      text: '02 - {!!$param->nama_parameter!!}',
      style : {
          'font-size' : "10px",
          'font-weight' : "bold",
      }
    },
    @break
    @endforeach
    subtitle: {
      text: ''
    },
    xAxis: {
      categories: [
        @foreach($data_b as $val)
        '{{$val->nilainya}}',
        @endforeach],
      labels: {
            style: {
                fontSize: '7px',
                color: '#000',
            }
      }},
    yAxis: {
      title: {
        text : 'Jumlah Peserta',
      }
    },
    plotOptions: {
        series: {
            borderWidth: 0,dataLabels: {
                enabled: true,
                format:'{point.y} {point.a}',
                y: 0,
                crop: false,
                overflow: 'none'
            }
        }
    },
    series: [{type: 'column',
        name : 'Z-score',
        data: [
        @foreach($data_b as $val)
            {
            y: {{$val->nilai1}},
            <?php if($val->nilainya == '0.0') { ?>
                  color: '#67ca3b',
            <?php } ?>
            <?php if($val->nilainya == '< -3' || $val->nilainya == '> 3') { ?>
                  color: '#ca3b3b',
            <?php } ?>
            @foreach($datap_b as $valu)
              <?php if($valu->nilainya == $val->nilainya) { ?>
                <?php if($valu->nilai1 == '1') { ?>
                  a: "*",
                <?php } ?>
              <?php } ?>
            @endforeach
            },
        @endforeach
        ],
        showInLegend: false}]
  });

  var chart2 = Highcharts.chart('container_alat', {
    @foreach($data as $val)
    title: {
      text: '01 - {!!$param->nama_parameter!!} <br>Kelompok Alat {!!$alatna->instrumen!!} {{$dataReg->instrument_lain}}',
      style : {
          'font-size' : "10px",
          'font-weight' : "bold",
      }
    },
    @break
    @endforeach
    subtitle: {text: ''},
    xAxis: {
      categories: [
        @foreach($data as $val)
        '{{$val->nilainya}}',
        @endforeach],
      labels: {
            style: {
                fontSize: '7px',
                color: '#000',
            }
      }},
    yAxis: {
      title: {
        text : 'Jumlah Peserta'
      }
    },
    plotOptions: {
        series: {
            borderWidth: 0,dataLabels: {
                enabled: true,
                format:'{point.y} {point.a}',
                y: 0,
                crop: false,
                overflow: 'none'
            }
        }
    },
    series: [{type: 'column',
        name : 'Z-score',
        data: [
        @foreach($data_alat as $val)
            {
            y: {{$val->nilai1}},
            <?php if($val->nilainya == '0.0') { ?>
                  color: '#67ca3b',
            <?php } ?>
            <?php if($val->nilainya == '< -3' || $val->nilainya == '> 3') { ?>
                  color: '#ca3b3b',
            <?php } ?>
            @foreach($datap_alat as $valu)
              <?php if($valu->nilainya == $val->nilainya) { ?>
                <?php if($valu->nilai1 == '1') { ?>
                  a: "*",
                <?php } ?>
              <?php } ?>
            @endforeach
            },
        @endforeach
        ],
        showInLegend: false}]
  });

  var chart5 = Highcharts.chart('container_alat1', {
    @foreach($data as $val)
    title: {
      text: '02 - {!!$param->nama_parameter!!} <br>Kelompok Alat {!!$alatna_b->instrumen!!} {{$dataReg_b->instrument_lain}}',
      style : {
          'font-size' : "10px",
          'font-weight' : "bold",
      }
    },
    @break
    @endforeach
    subtitle: {text: ''},
    xAxis: {
      categories: [
        @foreach($data_alat_b as $val)
        '{{$val->nilainya}}',
        @endforeach],
      labels: {
            style: {
                fontSize: '7px',
                color: '#000',
            }
      }},
    yAxis: {
      title: {
        text : 'Jumlah Peserta'
      }
    },
    plotOptions: {
        series: {
            borderWidth: 0,dataLabels: {
                enabled: true,
                format:'{point.y} {point.a}',
                y: 0,
                crop: false,
                overflow: 'none'
            }
        }
    },
    series: [{type: 'column',
        name : 'Z-score',
        data: [
        @foreach($data_alat_b as $val)
            {
            y: {{$val->nilai1}},
            <?php if($val->nilainya == '0.0') { ?>
                  color: '#67ca3b',
            <?php } ?>
            <?php if($val->nilainya == '< -3' || $val->nilainya == '> 3') { ?>
                  color: '#ca3b3b',
            <?php } ?>
            @foreach($datap_alat_b as $valu)
              <?php if($valu->nilainya == $val->nilainya) { ?>
                <?php if($valu->nilai1 == '1') { ?>
                  a: "*",
                <?php } ?>
              <?php } ?>
            @endforeach
            },
        @endforeach
        ],
        showInLegend: false}]
  });


  var chart3 = Highcharts.chart('container_metode', {
    @foreach($data as $val)
    title: {
      text: '01 - {!!$param->nama_parameter!!} <br> Kelompok {!!$metodena->metode_pemeriksaan!!} {{$dataReg->metode_lain}}',
      style : {
          'font-size' : "10px",
          'font-weight' : "bold",
      }
    },
    @break
    @endforeach
    subtitle: {text: ''},
    xAxis: {
      categories: [
        @foreach($data as $val)
        '{{$val->nilainya}}',
        @endforeach],
      labels: {
            style: {
                fontSize: '7px',
                color: '#000',
            }
      }},
    yAxis: {
      title: {
        text : 'Jumlah Peserta'
      }
    },
    plotOptions: {
        series: {
            borderWidth: 0,dataLabels: {
                enabled: true,
                format:'{point.y} {point.a}',
                y: 0,
                crop: false,
                overflow: 'none'
            }
        }
    },
    series: [{type: 'column',
        name : 'Z-score',
        data: [
        @foreach($data_metode as $val)
            {
            y: {{$val->nilai1}},
            <?php if($val->nilainya == '0.0') { ?>
              color: '#67ca3b',
            <?php } ?>
            <?php if($val->nilainya == '< -3' || $val->nilainya == '> 3') { ?>
                  color: '#ca3b3b',
            <?php } ?>
            @foreach($datap_metode as $valu)
              <?php if($valu->nilainya == $val->nilainya) { ?>
                <?php if($valu->nilai1 == '1') { ?>
                  a: "*",
                <?php } ?>
              <?php } ?>
            @endforeach
            },
        @endforeach
        ],
        showInLegend: false}]
  });

  var chart6 = Highcharts.chart('container_metode1', {
    @foreach($data as $val)
    title: {
      text: '02 - {!!$param->nama_parameter!!} <br> Kelompok {!!$metodena->metode_pemeriksaan!!} {{$dataReg_b->metode_lain}}',
      style : {
          'font-size' : "10px",
          'font-weight' : "bold",
      }
    },
    @break
    @endforeach
    subtitle: {text: ''},
    xAxis: {
      categories: [
        @foreach($data as $val)
        '{{$val->nilainya}}',
        @endforeach],
      labels: {
            style: {
                fontSize: '7px',
                color: '#000',
            }
      }},
    yAxis: {
      title: {
        text : 'Jumlah Peserta'
      }
    },
    plotOptions: {
        series: {
            borderWidth: 0,dataLabels: {
                enabled: true,
                format:'{point.y} {point.a}',
                y: 0,
                crop: false,
                overflow: 'none'
            }
        }
    },
    series: [{type: 'column',
        name : 'Z-score',
        data: [
        @foreach($data_metode_b as $val)
            {
            y: {{$val->nilai1}},
            <?php if($val->nilainya == '0.0') { ?>
              color: '#67ca3b',
            <?php } ?>
            <?php if($val->nilainya == '< -3' || $val->nilainya == '> 3') { ?>
                  color: '#ca3b3b',
            <?php } ?>
            @foreach($datap_metode_b as $valu)
              <?php if($valu->nilainya == $val->nilainya) { ?>
                <?php if($valu->nilai1 == '1') { ?>
                  a: "*",
                <?php } ?>
              <?php } ?>
            @endforeach
            },
        @endforeach
        ],
        showInLegend: false}]
  });
  </script>
</body>
</html>
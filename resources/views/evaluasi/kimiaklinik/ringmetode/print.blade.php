<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
table{
    border-collapse: collapse;
    width: 100%;
}

table th, table td  {
    padding: 0px 5px;
    text-align: left;
    border: 1px solid #333;
}

.table-bordered tbody td{
    height:20px;
}
</style>
<h2>Nilai Range per Metode Tahun {{$tahun}} Siklus {{$siklus}} Tipe @if($tipe == 'a') 01 @else 02 @endif @if($periode == 2) Periode 2 @endif</h2>
<table class="table table-bordered" width="100%">
    <thead>
        <tr>
        <th rowspan="2" width="20%">Parameter</th>
        <th rowspan="2" width="20%">Metode</th>
        <th rowspan="2" width="10%">Batas Bawah</th>
        <th rowspan="2" width="10%">Batas Atas</th>
        <th rowspan="2" width="10%">Interval</th>
        @for($i = 1; $i <= 3; $i++)
            <th colspan="2" width="10%">Ring {{$i}}</th>
        @endfor
        </tr>
        <tr>
            @for($i = 1; $i <= 3; $i++)
                <th width="5%">Bawah</th>
                <th width="5%">Atas</th>
            @endfor
            
        </tr>
    </thead>
    <tbody class="tbody-ring">
        @foreach($sd as $key => $val)
        <?php 
            $batas_bawah = ($val->batas_bawah != NULL) ? $val->batas_bawah : -((3 * $val->sd)-$val->median) ;
            $batas_atas = ($val->batas_atas != NULL) ? $val->batas_atas : ((3 * $val->sd)+$val->median) ; 
            if ($val->catatan == 'Hasil pemeriksaan tanpa desimal') {
                $batas_bawah = number_format($batas_bawah, 0);
                $batas_atas = number_format($batas_atas, 0);
            }else if($val->catatan == 'Hasil pemeriksaan menggunakan 1 (satu) desimal'){
                $batas_bawah = number_format($batas_bawah, 1);
                $batas_atas = number_format($batas_atas, 1);
            }else if($val->catatan == 'Hasil pemeriksaan menggunakan 2 (dua) desimal'){
                $batas_bawah = number_format($batas_bawah, 2);
                $batas_atas = number_format($batas_atas, 2);
            }else if($val->catatan == 'Hasil pemeriksaan menggunakan 3 (tiga) desimal'){
                $batas_bawah = number_format($batas_bawah, 3);
                $batas_atas = number_format($batas_atas, 3);
            }
        ?>
        <tr>
        <td>
            {{$val->nama_parameter}}
        </td>
        <td>
            {!!$val->nama_metode!!}
        </td>
        <td style="text-align: right;">
            {{$batas_bawah}}
        </td>
        <td style="text-align: right;">
            {{$batas_atas}}
        </td>
        <td style="text-align: right;">{{$val->batas_interval *1}}</td>
        @for($i = 1; $i <= 3; $i++)
            <?php $ring_bawah = "ring_".$i."_bawah";?>
            <?php $ring_atas = "ring_".$i."_atas";?>
            <td style="text-align: right;">{{$val->$ring_bawah * 1}}</td>
            <td style="text-align: right;">{{$val->$ring_atas * 1}}</td>
            @endfor
        </tr>
        @endforeach
    </tbody>
</table>
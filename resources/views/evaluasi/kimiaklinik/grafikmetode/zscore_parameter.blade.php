@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
              <div class="panel-heading">Grafik Z-Score per Metode Kimia Klinik</div>
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                <div class="panel-body">
                  <div>
                      <label for="exampleInputEmail1">Parameter</label>
                      <select class="form-control coba" name="parameter" id="parameter">
                        <option></option>
                        @foreach($param as $val)
                        <option value="{{$val->id}}">{!!$val->nama_parameter!!}</option>
                        @endforeach
                      </select>
                  </div>
                  <div>
                      <label for="exampleInputEmail1">Metode</label>
                      <select class="form-control coba" name="metode" id="metode">
                        <option></option>
                      </select>
                  </div>
                  <div>
                      <label for="exampleInputEmail1">Tahun</label>
                      <input type="text" value="{{$tahun}}" readonly class="form-control coba" name="tahun" id="tahun" required>
                  </div>
                  <div>
                      <label for="exampleInputEmail1">Type</label>
                      <input type="text" value="{{$type}}" readonly class="form-control coba" name="type" id="type" required>
                  </div>
                  <div>
                      <label for="exampleInputEmail1">Siklus</label>
                      <input type="text" value="{{$siklus}}" readonly class="form-control coba" name="siklus" id="siklus" required>
                  </div><br>
                  <input type="submit" name="proses" class="btn btn-primary" value="Proses">
                  {{ csrf_field() }}
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$("#parameter").change(function(){
  var val = $(this).val(), i;
  var y = document.getElementById('metode');
  $("#metode").val("");
  y.innerHTML = "<option>Please Wait... </option>";
  $.ajax({
    type: "GET",
    url : "{{url('getmetode').'/'}}"+val,
    success: function(addr){
      y.innerHTML = "<option> </option>"
      for(i = 0; i < addr.Hasil.length; i++){
        var option = document.createElement("option");
        option.value = addr.Hasil[i].Kode;
        option.text = addr.Hasil[i].Nama;
        y.add(option);
      }
      return false;
    }
  });
});
</script>
@endsection
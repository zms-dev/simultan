@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Metode Kimia Klinik dengan Nilai yang Sama</div>

                <div class="panel-body">
                  <div id="container"></div>

                  <label>Keterangan :</label>
                  <blockquote style="font-size: 12px">
                    <label style="background-color: #BF0B23">&nbsp; &nbsp;</label> < <?=$ring->batas_bawah;?> Out Bawah / > <?=$ring->batas_atas;?> Out Atas<br>
                    <label style="background-color: #1bbd20">&nbsp; &nbsp;</label> Median<br>
                    <label > &nbsp;*</label> Posisi Peserta<br>
                  </blockquote>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

// Create the chart
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Nilai yang Sama dengan Metode {{$metode->metode_pemeriksaan}}'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
      title: {
        text : 'Jumlah Peserta'
      }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y} {point.a}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
    },

    "series": [
        {
            "name": "Nilai yang Sama",
            "colorByPoint": false,
            "data": [
              {
                "name": "< {{$ring->batas_bawah}}",
                "color": '#BF0B23',
                "y": {{$ring->out_bawah}},
                @if(!empty($median) && $median->median < $ring->batas_bawah)
                  "a": '*', 
                @endif
                @if(!empty($peserta) && $peserta->hasil_pemeriksaan < $ring->batas_bawah)
                  "color": '#1bbd20',
                @else
                  "color": '#BF0B23',
                @endif
              },
              {
                "name": "{{$ring->ring_1_bawah}} - {{$ring->ring_1_atas}}",
                "y": {{$ring->ring_1}},
                @if(!empty($median) && $median->median >= $ring->ring_1_bawah && $median->median <= $ring->ring_1_atas)
                  "a": '*', 
                @endif
                @if(!empty($peserta) &&  $peserta->hasil_pemeriksaan >= $ring->ring_1_bawah && $peserta->hasil_pemeriksaan <= $ring->ring_1_atas)
                  "color": '#1bbd20',
                @endif
              },
              {
                "name": "{{$ring->ring_2_bawah}} - {{$ring->ring_2_atas}}",
                "y": {{$ring->ring_2}},
                @if(!empty($median) && $median->median >= $ring->ring_2_bawah && $median->median <= $ring->ring_2_atas)
                  "a": '*', 
                @endif
                @if(!empty($peserta) && $peserta->hasil_pemeriksaan >= $ring->ring_2_bawah && $peserta->hasil_pemeriksaan <= $ring->ring_2_atas)
                  "color": '#1bbd20',
                @endif
              },
              {
                "name": "{{$ring->ring_3_bawah}} - {{$ring->ring_3_atas}}",
                "y": {{$ring->ring_3}},
                @if(!empty($median) && $median->median >= $ring->ring_3_bawah && $median->median <= $ring->ring_3_atas)
                  "a": '*', 
                @endif
                @if(!empty($peserta) && $peserta->hasil_pemeriksaan >= $ring->ring_3_bawah && $peserta->hasil_pemeriksaan <= $ring->ring_3_atas)
                  "color": '#1bbd20',
                @endif
              },
              {
                "name": "{{$ring->ring_4_bawah}} - {{$ring->ring_4_atas}}",
                "y": {{$ring->ring_4}},
                @if(!empty($median) && $median->median >= $ring->ring_4_bawah && $median->median <= $ring->ring_4_atas)
                  "a": '*',  
                @endif
                @if(!empty($peserta) && $peserta->hasil_pemeriksaan >= $ring->ring_4_bawah && $peserta->hasil_pemeriksaan <= $ring->ring_4_atas)
                  "color": '#1bbd20',
                @endif
              },
              {
                "name": "{{$ring->ring_5_bawah}} - {{$ring->ring_5_atas}}",
                "y": {{$ring->ring_5}},
                @if(!empty($median) && $median->median >= $ring->ring_5_bawah && $median->median <= $ring->ring_5_atas)
                  "a": '*', 
                @endif
                @if(!empty($peserta) && $peserta->hasil_pemeriksaan >= $ring->ring_5_bawah && $peserta->hasil_pemeriksaan <= $ring->ring_5_atas)
                  "color": '#1bbd20',
                @endif
              },
              {
                "name": "> {{$ring->batas_atas}}",
                "color": '#BF0B23',
                "y": {{$ring->out_atas}},
                @if(!empty($median) && $median->median > $ring->batas_atas)
                  "a": '*', 
                @endif
                @if(!empty($peserta) && $peserta->hasil_pemeriksaan > $ring->batas_atas)
                  "color": '#1bbd20',
                @endif
              },
            ]
        }
    ],
});
</script>
@endsection
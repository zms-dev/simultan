@if($i != 0)
<style type="text/css">
@page {
margin: 100px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 12px;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}

tr.border_bottom td{
  border-bottom:1pt solid black;
}
</style>
<table border="1" cellspacing="1" cellpadding="1" width="100%" style="margin-top: 1%; font-size: 14px;">
    <thead>
        <tr class="titlerowna" style="">
            <th><center>No </center></th>
            <th><center>Kode Sediaan </center></th>
            <th><center>Nilai Acuan </center></th>
            <th><center>Hasil Peserta </center></th>
            <th><center>Nilai </center></th>
            <th><center>Total </center></th>
            <th><center>Kesimpulan </center></th>
        </tr>
    </thead>
    <tbody>
        <?php $urutan = 0; ?>

        @foreach($pesertatcs as $pesertatc)
            <?php $urutan++; ?>
            <?php $id = $pesertatc->id_registrasi; ?>
            <?php $no = 0; ?>
            @foreach($datas[$id] as $val)
            <?php $no++; ?>
            <tr class="border_bottom">
                @if($no == 1)
                    <td rowspan="4">
                        <center>{{ $urutan }}</center>
                    </td>
                @endif

                <td ><center>{!!$val->kode_botol!!}</center>{{-- <input type="hidden" name="kode_sediaan[]" value="{!!$val->kode_botol!!}"> --}}</td>
                @foreach($rujuk[$id] as $re)
                @if($re->kode_sediaan == $no)
                <td>
                    <center>{!!$re->rujukan!!}</center>
                </td>
                @endif
                @endforeach
                <td style="vertical-align: top">
                    {!!html_entity_decode($val->hasil) !!} </td>
                <td class="colomngitung">
                    @if(count($evaluasi[$id]))
                    @foreach($evaluasi[$id] as $valu)
                        @if($valu->kode_sediaan == $val->kode_botol)
                            {{-- <input type="number" name="nilai[]" value=" --}}<center>{{$valu->nilai}}</center>{{-- "> --}}
                        @endif
                    @endforeach
                    @else
                {{--  <input type="number" name="nilai[]"> --}}
                    @endif
                </td>

                @if($no == 1)
                    <td rowspan="4">
                        <center>{{ $nilaievaluasi[$id]->nilai }}</center>
                    </td>

                    <td rowspan="4"><center>
                        @if($nilaievaluasi[$id]->nilai >= 6)
                        Baik
                        @else
                        Kurang
                        @endif
                    </center></td>
                @endif
            </tr>
            @endforeach

            @if($urutan % 5 == 0)
                <tr><td><div style="page-break-before: always;"></div></td></tr>
            @endif
        @endforeach

    </tbody>
</table>

<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>
@else
<table>
    <thead>
        <tr class="titlerowna" style="">
            <th><center>No </center></th>
            <th><center>Kode Sediaan </center></th>
            <th><center>Nilai Acuan </center></th>
            <th><center>Hasil Peserta </center></th>
            <th><center>Nilai </center></th>
            <th><center>Total </center></th>
            <th><center>Kesimpulan </center></th>
        </tr>
    </thead>
    <tbody>
        <?php $urutan = 0; ?>

        @foreach($pesertatcs as $pesertatc)
            <?php $urutan++; ?>
            <?php $id = $pesertatc->id_registrasi; ?>
            <?php $no = 0; ?>
            @foreach($datas[$id] as $val)
            <?php $no++; ?>
            <tr >
                @if($no == 1)
                    <td rowspan="4">
                        <center>{{ $urutan }}</center>
                    </td>
                @endif

                <td ><center>{!!$val->kode_botol!!}</center>
                @foreach($rujuk[$id] as $re)
                @if($re->kode_sediaan == $no)
                <td>
                    <center>{!!$re->rujukan!!}</center>
                </td>
                @endif
                @endforeach
                <td style="vertical-align: top">
                    {!!html_entity_decode($val->hasil) !!} </td>
                <td class="colomngitung">
                    @if(count($evaluasi[$id]))
                    @foreach($evaluasi[$id] as $valu)
                        @if($valu->kode_sediaan == $val->kode_botol)
                            {{-- <input type="number" name="nilai[]" value=" --}}<center>{{$valu->nilai}}</center>{{-- "> --}}
                        @endif
                    @endforeach
                    @else
                {{--  <input type="number" name="nilai[]"> --}}
                    @endif
                </td>

                @if($no == 1)
                    <td rowspan="4">
                        <center>{{ $nilaievaluasi[$id]->nilai }}</center>
                    </td>

                    <td rowspan="4"><center>
                        @if($nilaievaluasi[$id]->nilai >= 6)
                        Baik
                        @else
                        Kurang
                        @endif
                    </center></td>
                @endif
            </tr>
            @endforeach
        @endforeach

    </tbody>
</table>
@endif





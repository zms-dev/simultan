@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluasi TC</div>

                <div class="panel-body">
                @if(count($evaluasi))
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{url('hasil-pemeriksaan/telur-cacing/evaluasi/')}}/print/{{$id}}?y={{$siklus}}" >
                @else
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                @endif

                <table class="table table-bordered" id="peserta_pme">
                    <thead>
                        <tr class="titlerowna">
                            <th>Kode Sediaan</th>
                            <th>Nilai Acuan</th>
                            <th>Hasil Peserta</th>
                            <th>Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($evaluasi))
                        @foreach($evaluasi as $val)
                            <input type="hidden" name="id[]" value="{{$val->id}}">
                        @endforeach
                    @endif
                    @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($datas as $val)
                        <?php $no++; ?>
                        <tr>
                            <td>{!!$val->kode_botol!!}<input type="hidden" name="kode_sediaan[]" value="{!!$val->kode_botol!!}"></td>
                            <td>
                            @foreach($rujuk as $re)
                            @if($re->kode_sediaan == $no)
                                {!!$re->rujukan!!}
                            @endif
                            @endforeach
                            </td>
                            <td>
                                {!!html_entity_decode($val->hasil) !!}
                            </td>
                            <td class="colomngitung">
                                @if(count($evaluasi))
                                    @foreach($evaluasi as $valu)
                                        @if($valu->kode_sediaan == $val->kode_botol)
                                            <input type="number" name="nilai[]" value="{{$valu->nilai}}">
                                        @endif
                                    @endforeach
                                @else
                                <input type="number" name="nilai[]">
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    @endif
                        <tr>
                            <th colspan="3">Total Nilai:
                            </th>
                            <th class="total">{{$nilaievaluasi->nilai}}</th>
                        </tr>
                    </tbody>
                </table>
                <div>
                    <h4>Kesimpulan :
                        <span style="text-decoration: underline;">@if(count($nilaievaluasi))
                                @if($nilaievaluasi->nilai >= 6)
                                Baik
                                @else
                                Kurang
                                @endif
                                @endif 
                        </span>
                    </h4>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <br>
                        <table class="table table-bordered">
                            <tr>
                                <td><center>Jenis Kesalahan</center></td>
                            </tr>
                            <tr>
                                @if(count($evaluasisaran)) 
                                    <td><textarea class="form-control" id="ckeditorkesalahan1" name="tindakan">{{ $evaluasisaran->tindakan }}</textarea></td>
                                @else
                                    <td><textarea class="form-control" id="ckeditorkesalahan1" name="tindakan"></textarea></td> 
                                @endif
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <br>
                        <table class="table table-bordered">
                            <tr>
                                <td><center>Saran & Tindakan</center></td>
                            </tr>
                            <tr>
                                @if(count($evaluasisaran))
                                    <td><textarea class="form-control" id="ckeditortindakan1" name="saran">{{ $evaluasisaran->saran }}</textarea></td>
                                @else
                                <td><textarea class="form-control" id="ckeditortindakan1" name="saran"></textarea></td>
                                @endif
                            </tr>
                        </table>
                    </div>
                </div>
                 @if(count($evaluasi))
                    <input type="submit" name="simpan" class="btn btn-success" value="Print">
                    <input type="submit" name="simpan" class="btn btn-primary" value="Update">
                @else
                    <input type="submit" name="simpan" class="btn btn-primary" value="Simpan">
                @endif
                {{ csrf_field() }}
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    CKEDITOR.replace( 'ckeditorkesalahan1',
            {
                toolbar : 'Basic', /* this does the magic */
                enterMode : CKEDITOR.ENTER_BR,
                shiftEnterMode : CKEDITOR.ENTER_P
            });

    CKEDITOR.replace( 'ckeditortindakan1',
            {
                toolbar : 'Basic', /* this does the magic */
                enterMode : CKEDITOR.ENTER_BR,
                shiftEnterMode : CKEDITOR.ENTER_P
            });
</script>
@endsection

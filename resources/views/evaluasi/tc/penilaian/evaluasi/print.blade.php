<style type="text/css">
@page {
margin: 170px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 12px;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}


#header {
    position: fixed;
    border-bottom:1px solid gray;
    padding-top: -140px
}
#footer {
    position: fixed;
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
</style>
<div id="header">
    <table width="100%" cellpadding="0" border="0" >
        <thead>
            <tr>
                <th>
                   <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px">
                </th>
                <td width="100%" style="padding-left: 20px">
                    <span style="font-size: 12px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 10px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL MIKROBIOLOGI TELUR CACING SIKLUS {{$siklus}} TAHUN {{$tahun->year}}</b></span><br>
                    <span style="font-size: 10px">Penyelenggara : <b>Balai Besar Laboratorium Kesehatan Surabaya</b></span>
                    <div style="padding-left: 75px; margin-top: -15px; font-size: 10px">
                        <span><br/>Jalan Karangmenjangan No, 18 Surabaya - 60286<br>Telepon : (031) 5021451 | Fax : (031) 5020388<br>Web Online : simultan.bblksurabaya.id <br>Email : pme.bblksub@gmail.com</span>
                    </div>
                </td>
                <th>
                    <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" height="90px">
                </th>
            </tr>
            <tr>
                <th colspan="3"><hr></th>
            </tr>
        </thead>
    </table>
</div>
<br>
<center><label style="font-size: 12px;"><b>LAMPIRAN EVALUASI PESERTA<br>
PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL MIKROBIOLOGI<br> MIKROSKOPIS TELUR CACING SIKLUS {{$siklus}} TAHUN {{$tahun->year}}
</b><input type="hidden" name="type" value="{{$siklus}}"></label></center><br>

<table style="margin-left: 15%; font-size: 12px;">
    <tr>
        <td>Sifat</td>
        <td>:</td>
        <td><b>RAHASIA</b></td>
    </tr>
    <tr>
        <td>Kode Lab Peserta</td>
        <td>:</td>
        <td>{{ substr($data_peserta->kode_peserta,1,-6) }}</td>
    </tr>
    <tr>
        <td>Nama Instansi</td>
        <td>:</td>
        <td>{{ $data_peserta->nama_lab }}</td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>:</td>
        <td>{{$data_peserta->alamat}}</td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>Hasil Pemeriksaan </td>
        <td>:</td>
    </tr>
</table>
<table border="1" cellspacing="1" cellpadding="1" width="100%" style="margin-top: 1%; font-size: 12px;">
    <thead>
        <tr class="titlerowna" style="">
            <th><center>Kode Bahan Uji </center></th>
            <th><center>Nilai Acuan </center></th>
            <th><center>Hasil Peserta </center></th>
            <th><center>Nilai </center></th>
        </tr>
    </thead>
    <tbody>
        @if(count($data))
        <?php $no = 0; ?>
        @foreach($datas as $val)
        <?php $no++; ?>
        <tr>
            <td ><center>{!!$val->kode_botol!!}</center>{{-- <input type="hidden" name="kode_sediaan[]" value="{!!$val->kode_botol!!}"> --}}</td>
            <td>
            @foreach($rujuk as $re)
            @if($re->kode_sediaan == $no)
                <center>{!!$re->rujukan!!}</center>
            @endif
            @endforeach
            </td>
            <td style="vertical-align: top">
                {!!html_entity_decode($val->hasil) !!} </td>
            <td class="colomngitung">
                @if(count($evaluasi))
                @foreach($evaluasi as $valu)
                    @if($valu->kode_sediaan == $val->kode_botol)
                        {{-- <input type="number" name="nilai[]" value=" --}}<center>{{$valu->nilai}}</center>{{-- "> --}}
                    @endif
                @endforeach
                @else
               {{--  <input type="number" name="nilai[]"> --}}
                @endif
            </td>
        </tr>
        @endforeach
        @endif
        <tr>
            <th colspan="3"><center>Total</center>
            </th>
            <th class="total"><center>{{$nilaievaluasi->nilai}}</center></th>
        </tr>
    </tbody>
</table>
<br>
<table style="width: 100%;font-size: 12px">
    <tr>
        <td width="15%" style="font-size: 16px"><b>Kesimpulan</b></td>
        <td width="5%">:</td>
        <td width="80%">
            <span style="text-decoration: underline;font-size: 16px">
                @if(count($nilaievaluasi))
                    @if($nilaievaluasi->nilai >= 6)
                        Baik
                    @else
                        Kurang
                    @endif
                @endif
            </span>
        </td>
    </tr>
</table><br>
<table border="1" width="100%">        
    <tr>
        <td width="50%"><b>Jenis Kesalahan</b> </td>
        <td width="50%"><b>Saran & Tindakan</b> </td>
    </tr>
    <tr>
        <td>{!! $evaluasisaran->tindakan !!}</td>
        <td>{!! $evaluasisaran->saran !!}</td>
    </tr>
</table>

<br>
<div style="margin-left: 500px; margin-top: -1%;">
<div style="position: relative; top: 17px">
Surabaya, {{$ttd->tanggal}} {{$tahun->year}}<br>
Manajer Teknis
</div>
<p>
    <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.'ttdevaluasi'.DIRECTORY_SEPARATOR.$ttd->image)}}" width="80" height="80" style="margin-left: 20px !important;">
</p>

<div style="position: relative; top:-20px">
  {{$ttd->nama}}<br>NIP. {{$ttd->nip}}
</div><br>
</div>
<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>




<table border="1">
    <tr>
        <th>No</th>
        <th>Kode Lab</th>
        <th>Nama Peserta</th>
        <th>Instansi</th>
        <?php for ($i=1; $i < 5; $i++) { ?>
        <th>{{$i}}/{{$input['siklus']}}/{{$input['tahun']}}</th>
        <?php } ?>
    </tr>
    <?php $no = 0; ?>
    @foreach($data as $val)
    <?php $no++; ?>
    <tr>
        <td>{{$no}}</td>
        <td>{{$val->kode_lebpes}}</td>
        <td>{{$val->nama_lab}}</td>
        <td>{{$val->badan_usaha}}</td>
        @foreach($val->hasil as $hasil)
        <td>{!!html_entity_decode($hasil->hasil) !!} </td>
        @endforeach
    </tr>
    @endforeach
</table>
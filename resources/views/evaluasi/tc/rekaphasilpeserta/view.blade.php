<table border="1">
    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">Kode Lab</th>
        <th rowspan="2">Nama Peserta</th>
        <th rowspan="2">Instansi</th>
        <?php for ($i=1; $i < 5; $i++) { ?>
        <th>Jwb</th>
        <th>Nilai</th>
        <?php } ?>
        <th rowspan="2">Total Nilai</th>
        <th rowspan="2">Keterangan</th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <?php for ($i=1; $i < 5; $i++) { ?>
        <th colspan="2">{{$i}}/{{$input['siklus']}}/{{$input['tahun']}}</th>
        <?php } ?>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    <?php $no = 0; ?>
    @foreach($data as $val)
    <?php $no++; ?>
    <tr>
        <td>{{$no}}</td>
        <td>{{$val->kode_lebpes}}</td>
        <td>{{$val->nama_lab}}</td>
        <td>{{$val->badan_usaha}}</td>
        @foreach($val->hasil as $hasil)
        <td>{!!html_entity_decode($hasil->hasil) !!} </td>
        <td>{{$hasil->nilai}}</td>
        @endforeach
        <td>{{$val->total[0]->nilai}}</td>
        <td>
            @if($val->total[0]->nilai >= 6)
            Baik
            @else
            Kurang
            @endif
        </td>
    </tr>
    @endforeach
</table>
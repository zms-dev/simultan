<table>
    <thead>
        <tr>
            <td>No</td>
            <td>Kode Sediaan</td>
            <td>Nama Peserta</td>
            @for($i = 1;$i <= 10;$i++)
                <td><center>A{{ $i }}</center></td>
            @endfor
        </tr>
    </thead>

    <tbody>
        @php $urutan = 0; @endphp
        @foreach($data as $val)
            @php $urutan++; @endphp
            <tr>
                <td>{{ $urutan++ }}</td>
                <td>{{ $val->kode_peserta }}</td>
                <td>{{ $val->nama_lab }}</td>
                @foreach($val->datas as $valu)
                    @if($valu->hasil == 'Positif')
                        <td>{{ $valu->hasil }} | {{ $valu->spesies }} | {{ $valu->stadium }} | {{ $valu->parasit }}</td>
                    @else
                        <td>{{ $valu->hasil }}</td>
                    @endif
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>
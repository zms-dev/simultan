@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Kesimpulan Evaluasi Peserta</div>
                <div class="panel-body">
                  <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var chart = Highcharts.chart('container', {
    title: {
        text: 'Grafik Kesimpulan Evaluasi Peserta'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
            @foreach($data as $val)
                '{{$val->kesimpulan}}'
            @endforeach
        ]
    },
    series: [{
        type: 'column',
        colorByPoint: true,
        data: [
            @foreach($data as $val)
                {{$val->jumlah}},
            @endforeach
            ],
        showInLegend: false
    }]

});
</script>
@endsection
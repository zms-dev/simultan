<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
}
.peserta_pme td,.peserta_pme th{ 
    border: 1px solid #ddd;
}

table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 5px;
    text-align: left;
    border: 1px solid #000;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}

</style>

<table width="100%" cellpadding="0" border="" style="margin-top: 15%; position: fixed; margin-bottom: 10%; top: -20px; left: 0px; right: 0px;">
    <thead>
        <tr>
            <th>
                <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px"> 
            </th>
            <th width="2%;"></th>
            <td width="100%">
                <span style="font-size: 16px"><b>KEMENTERIAN KESEHATAN R.I.</b></span><br>
                <span style="font-size: 16px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL MIKROBIOLOGI MALARIA SIKLUS {{$siklus}} TAHUN {{$tahun}}</b></span><br>
                <span style="font-size: 15px;">Penyelenggara :</span>
                <span style="font-size: 17px;"> 
                    <b>Balai Besar Laboratorium Kesehatan Surabaya</b>
                </span>
                <pre style="font-family: Helvetica;font-size: 12px; padding: -25px; margin-left: -18px; margin-top: 10px;"> 
                                                Jl.Karangmenjangan No. 18 Surabaya 60286
                                                Telepon : 031-5021451 Fax.031-5020388
                                                Email : pme.bblksub@gmail.com / mikrobblksub@gmail.com
                </pre>
            </td>
        </tr>
        <tr>
            <th colspan="4" style="padding: 5px;"></th>
        </tr>
        <tr>
            <th colspan="4" style=""><hr></th>
        </tr>
          <tr>
            <th colspan="4" style="padding: 1px;"></th>
        </tr>
    </thead>
</table>


<center><label style=""><b>LAMPIRAN EVALUASI PESERTA<br>
PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL MIKROBIOLOGI<br> MIKROSKOPIS MALARIA SIKLUS {{$siklus}} TAHUN {{$tahun}}
</b><input type="hidden" name="type" value="{{$siklus}}"></label></center>
<br>

<table style='font-size: 13px'>
  <tr>
    <td>Sifat</td>
    <td>:</td>
    <th>Rahasia</th>
  </tr>
  <tr>
    <td>Kode Lab Peserta</td>
    <td>:</td>
    <td>{{substr($datass->kode_peserta,1,-6)}}</td>
  </tr>
  <tr>
    <td>Nama Instansi</td>
    <td>:</td>
    <td>{{$datass->nama_lab}}</td>
  </tr>
   <tr>
    <td>Alamat</td>
    <td>:</td>
    <td>{{$datass->alamat}}</td>
  </tr>
  <tr>
    <td></td>
  </tr>
</table><br>
<table class="" border="1" width="100%" style='font-size: 13px'>
    <tr class="titlerowna" style="text-align: center;">
        <th width="10%;">Kode Sediaan</th>
        <th width="35%;">Hasil Peserta</th>
        <th width="35%;">Nilai Acuan</th>
        <th width="10%;">Nilai</th>
    </tr>
    <tbody>
        @if(count($data))
        <?php $no = 0; ?>
        @foreach($datas as $val)
        <?php $no++; ?>
        <tr>
            <td>{!!$val->kode!!}</td>
            <td>
                <li style='margin-left: 10px'>{{ $val->hasil }}</li>
                @if($val->hasil == 'Positif')
                    @if(empty($val->spesies))
                    @else
                        <li style='margin-left: 10px'>{{$val->spesies}}</li> 
                    @endif

                    @if(empty($val->stadium))
                    @else
                        <li style='margin-left: 10px'>{{$val->stadium}}</li> 
                    @endif
                    
                    @if(empty($val->parasit))
                    @else
                        <li style='margin-left: 10px'>{{$val->parasit}}</li> 
                    @endif
                @endif
            </td>
            <td>
            @foreach($rujuk as $re)
            @if($re->kode_bahan_uji == substr($val->kode,9,-5))
                <li style='margin-left: 10px'>{{$re->rujukan}}</li>
                @if($re->rujukan == 'Positif')
                    @if(empty($re->spesies))
                    @else
                        <li style='margin-left: 10px'>{{$re->spesies}}</li> 
                    @endif

                    @if(empty($re->stadium))
                    @else
                        <li style='margin-left: 10px'>{{$re->stadium}}</li> 
                    @endif
                    
                    @if(empty($re->parasit))
                    @else
                        <li style='margin-left: 10px'>{{$re->parasit}}</li> 
                    @endif
                @endif
            @endif
            @endforeach
            <td class="colomngitung" style="text-align: center;">
            @foreach($nilai as $valu)
                @if($valu->kode_sediaan == $val->kode)
                    {{$valu->nilai}}
                @endif
            @endforeach
            </td>
        </tr>
        @endforeach
        @endif
        <tr>
            <th colspan="3"><center>Total</center>
            </th>
            <th class="total" style="text-align: center;">{{$evaluasi->total}}</th>
        </tr>
    </tbody>
</table><br>

<div style='page-break-inside: avoid; font-size: 13px'>
<p style="font-weight: bold;">KESIMPULAN : <font style="text-decoration: underline;">{!! $evaluasi->kesimpulan !!}</font></p>
<p style="font-weight: bold;">KOMENTAR DAN SARAN :</p>
<table class="utama">
    <tr>
        <th>Jenis Kesalahan</th>
        <th>Saran Tindakan</th>
    </tr>
    <tr>
        <td>{!! $evaluasi->kesalahan !!}</td>
        <td>
            @if($evaluasi->total >= 90)
                Selamat dengan kinerja yang sempurna
            @elseif($evaluasi->total >= 80 && $evaluasi->total < 90)
                Selamat untuk kinerja yang sangat baik, dan pertahankan
            @elseif($evaluasi->total >= 70 && $evaluasi->total < 80)
                Selamat untuk kinerja yang baik, dan lakukan tidakan perbaikan. Periksa Kompetensi Staf,pertimbangkan untuk OJT,periksa kualitas reagen, periksa mikroskop
            @elseif($evaluasi->total < 70)
                Lakukan segara tindakan perbaikan. Lakukan pengawasan ditempat, Periksa kompetensi staf, pertimbangkan untuk OJT,periksa kualitas reagen Periksa Mikroskop
            @endif
        </td>
    </tr>
</table>
<br>
<div style="margin-left: 550px;">
<div style="position: relative; top: 17px">
Surabaya, {{$ttd->tanggal}} {{$tahun}}<br>
Manajer Teknis
</div>
<p>
    <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.'ttdevaluasi'.DIRECTORY_SEPARATOR.$ttd->image)}}" width="80" height="80" style="margin-left: 20px !important;">
</p>

<div style="position: relative; top:-20px">
    {{$ttd->nama}}<br>NIP. {{$ttd->nip}}
</div><br>
</div>
</div>
@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluasi Malaria</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                <table class="table table-bordered" id="peserta_pme">
                    <thead>
                        <tr class="titlerowna">
                            <th>Kode Sediaan</th>
                            <th>Hasil Peserta</th>
                            <th>Nilai Acuan</th>
                            <th>Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($datas as $val)
                        <?php $no++; ?>
                        <tr>
                            <td>{!! $val->kode !!}<input type="hidden" name="kode_sediaan[]" value="{!!$val->kode!!}"></td>
                            <td>
                                <li>{{ $val->hasil }}</li>
                                @if($val->hasil == 'Positif')
                                    @if(empty($val->spesies))
                                    @else
                                        <li>{{$val->spesies}}</li> 
                                    @endif

                                    @if(empty($val->stadium))
                                    @else
                                        <li>{{$val->stadium}}</li> 
                                    @endif
                                    
                                    @if(empty($val->parasit))
                                    @else
                                        <li>{{$val->parasit}}</li> 
                                    @endif
                                @endif
                            </td>
                            <td>
                            @foreach($rujuk as $re)
                            @if($re->kode_bahan_uji == substr($val->kode,9,-5))
                                <li>{{$re->rujukan}}</li>
                                @if($re->rujukan == 'Positif')
                                    @if(empty($re->spesies))
                                    @else
                                        <li>{{$re->spesies}}</li> 
                                    @endif

                                    @if(empty($re->stadium))
                                    @else
                                        <li>{{$re->stadium}}</li> 
                                    @endif
                                    
                                    @if(empty($re->parasit))
                                    @else
                                        <li>{{$re->parasit}}</li> 
                                    @endif
                                @endif
                            @endif
                            @endforeach
                            </td>
                            <td class="colomngitung">
                            @if(count($evaluasi))
                                @foreach($nilai as $valu)
                                    @if($valu->kode_sediaan == $val->kode)
                                        <input type="hidden" name="idnilai[]" value="{{$valu->id}}">
                                        <input type="number" name="rujukan[]" max="10" value="{{$valu->nilai}}">
                                    @endif
                                @endforeach
                            @else
                                <input type="number" name="rujukan[]" max="10" value="">
                            @endif

                            <!-- @foreach($rujuk as $re)
                            @if($re->kode_bahan_uji == $no)
                                @if($re->rujukan == $val->hasil)
                                    @if($re->rujukan == 'Positif')
                                        @if($re->spesies == $val->spesies)
                                            @if($re->stadium == $val->stadium)
                                                <?php $nilai1 = $re->parasit - $re->parasit*(25/100); $nilai2 = $re->parasit + $re->parasit*(25/100);?>
                                                    @if($val->parasit > $nilai1 && $val->parasit < $nilai2)
                                                    10<input type="text" name="rujukan[]" value="10">
                                                    @else
                                                    6<input type="text" name="rujukan[]" value="6">
                                                    @endif
                                            @else
                                                6<input type="text" name="rujukan[]" value="6">
                                            @endif
                                        @else
                                            3<input type="text" name="rujukan[]" value="3">
                                        @endif
                                    @else
                                        10<input type="text" name="rujukan[]" value="10">
                                    @endif
                                @else
                                    0<input type="text" name="rujukan[]" value="0">
                                @endif
                            @endif
                            @endforeach -->
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        <tr>
                            <th colspan="3">Total :
                            </th>
                            @if(count($evaluasi))
                                <th>
                                    {{$evaluasi->total}}
                                </th>
                            @else
                                <th class="total"></th>
                            @endif
                        </tr>

                    </tbody>
                </table>

                    Kesimpulan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                    @if(count($evaluasi))
                        <blockquote>
                            {{$evaluasi->kesimpulan}}
                        </blockquote>
                        <input type="hidden" name="kesimpulan" value="{{$evaluasi->kesimpulan}}">
                    @else
                        <input type="hidden" name="kesimpulan">
                    @endif
                    <div>
                        Komentar dan Saran :
                        <table class="table table-bordered"">
                            <tr>
                                <td width="50%"><center>Jenis Kesalahan</center></td>
                                <td width="50%"><center>Saran Tindakan</center></td>
                            </tr>
                            <tr>
                                @if(count($evaluasi))
                                <td><textarea class="form-control" id="ckeditor1" name="kesalahan">{{$evaluasi->kesalahan}}</textarea></td>
                                <td>
                                    @if($evaluasi->total >= 90)
                                        Selamat dengan kinerja yang sempurna
                                    @elseif($evaluasi->total >= 80 && $evaluasi->total < 90)
                                        Selamat untuk kinerja yang sangat baik, dan pertahankan
                                    @elseif($evaluasi->total >= 70 && $evaluasi->total < 80)
                                        Selamat untuk kinerja yang baik, dan lakukan tidakan perbaikan. Periksa Kompetensi Staf,pertimbangkan untuk OJT,periksa kualitas reagen, periksa mikroskop
                                    @elseif($evaluasi->total < 70)
                                        Lakukan segara tindakan perbaikan. Lakukan pengawasan ditempat, Periksa kompetensi staf, pertimbangkan untuk OJT,periksa kualitas reagen Periksa Mikroskop
                                    @endif

                                </td>
                                @else
                                <td><textarea class="form-control " id="ckeditor1" name="kesalahan"></textarea></td>
                                <td><textarea class="form-control " id="ckeditor2" name="tindakan" readonly=""></textarea></td>
                                @endif
                            </tr>
                        </table>
                    </div>
                @if(count($evaluasi))
                <a href="{{ url('/hasil-pemeriksaan/malaria/evaluasi/cetak/'. $id.'?y='.$siklus) }}" class="btn btn-success"   target="_BLANK">cetak</a>
                <input type="submit" name="simpan" class="btn btn-primary" value="Update">
                @else
                <input type="submit" name="simpan" class="btn btn-primary" value="Simpan">
                @endif
                {{ csrf_field() }}
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
CKEDITOR.replace( 'ckeditor1',
        {
            toolbar : 'Basic', /* this does the magic */
            enterMode : CKEDITOR.ENTER_BR,
            shiftEnterMode : CKEDITOR.ENTER_P
        });
CKEDITOR.replace( 'ckeditor2',
        {
            toolbar : 'Basic', /* this does the magic */
            enterMode : CKEDITOR.ENTER_BR,
            shiftEnterMode : CKEDITOR.ENTER_P
        });
</script>
@endsection
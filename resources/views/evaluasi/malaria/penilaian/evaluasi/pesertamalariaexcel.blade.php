<table>
    <thead>
        <tr>
            <td rowspan="2">No</td>
            <td rowspan="2">Kode Sediaan</td>
            <td rowspan="2">Nama Peserta</td>
            @for($i = 1;$i <= 10;$i++)
                <td><center>Hasil Peserta</center></td>
                <td><center>Nilai</center></td>
            @endfor
            <td rowspan="2"><center>Total</center></td>
            <td rowspan="2">Kesimpulan</center></td>
        </tr>
        <tr>
                <td></td>
            @for($i = 0;$i <= 10;$i++)
                <td colspan="2"><center>A{{ $i }}</center></td>
            @endfor
        </tr>
    </thead>

    <tbody>
        @php $urutan = 0; @endphp
        @foreach($pesertamalarias as $pesertamalaria)
            @php $urutan++; $id = $pesertamalaria->id_registrasi @endphp

            <tr>
                <td>{{ $urutan++ }}</td>
                <td>{{ $data[$id]->kode_peserta }}</td>
                <td>{{ $data[$id]->nama_lab }}</td>
                @foreach($datas[$id] as $val)
                    @if($val->hasil == 'Positif')
                        <td>{{ $val->hasil }} | {{ $val->spesies }} | {{ $val->stadium }} | {{ $val->parasit }}</td>
                    @else
                        <td>{{ $val->hasil }}</td>
                    @endif

                    @if(count($nilai[$id]) > 0)
                        @foreach($nilai[$id] as $valu)
                            @if($valu->kode_sediaan == $val->kode)
                                <td>{{$valu->nilai}}</td>
                            @endif
                        @endforeach
                    @else
                        <td></td>
                    @endif
                @endforeach
                <td>{{ $evaluasi[$id][0]->total }}</td>
                <td>{{ $evaluasi[$id][0]->kesimpulan }}</td>
            </tr>
        @endforeach
    </tbody>
</table>



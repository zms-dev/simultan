<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <script src="{{URL::asset('asset/js/jquery.min.js')}}"></script>
  <script src="{{URL::asset('asset/js/highstock.js')}}"></script>
  <script src="{{URL::asset('asset/js/exporting.js')}}"></script>
  <script src="{{URL::asset('asset/js/export-data.js')}}"></script>
  <script src="{{URL::asset('asset/js/highcharts-3d.js')}}"></script>
  <link rel="stylesheet" href="{{URL::asset('asset/css/bootstrap.css')}}">
  <style type="text/css">
      body{
          font-family: arial
      }
  </style>
</head>
<body>
  <div class="container">
      <div class="row">
          <div class="col-md-10 col-md-offset-1">
              <div class="panel panel-default">
                  <div class="panel-body">
                    <div id="container"></div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <script type="text/javascript">
  var chart = Highcharts.chart('container', {
    @foreach($data as $val)
    title: {text: 'Grafik Z-Score Peserta per Parameter {{$param->nama_parameter}}'},
    @break
    @endforeach
    subtitle: {text: 'Plain'},
    xAxis: {categories: [
        @foreach($data as $val)
        '{{$val->nilainya}}',
        @endforeach]},
    yAxis: {
      title: {
        text : 'Jumlah Peserta'
      }
    },
    plotOptions: {
        series: {
            borderWidth: 0,dataLabels: { 
                enabled: true,
                format:'{point.y}'
            }
        }
    },
    series: [{type: 'column',
      name: 'Nilai Z-Score',
        data: [
        @foreach($data as $val)
            {
            y: {{$val->nilai1}},
                  @foreach($datap as $valu)
                  <?php if($valu->nilainya == $val->nilainya) { ?>
                    <?php if($valu->nilai1 == '1') { ?>
                      color: '#BF0B23'
                    <?php } ?>
                  <?php } ?>
                  @endforeach
            },
        @endforeach
        ],
        showInLegend: true}]
  });
  </script>
</body>
</html>
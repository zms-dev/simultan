<table border="1">
    <thead>
        <tr>
            <th rowspan="2">Kode Lab</th>
            @foreach($parameterna as $val)
            <th colspan="2">{!!$val->nama_parameter!!}</th>
            @endforeach
        </tr>
        <tr>
            <th></th>
            @foreach($parameterna as $val)
            <th>Metode Pemeriksaan</th>
            <th>Hasil Pemeriksaan</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($data as $val)
        <tr>
            <td>{{$val->kode_lab}}</td>
            @foreach($val->parameter as $par)
                @foreach($par->detail as $hasil)
                    <td>{{$hasil->kode_metode_pemeriksaan}}</td>
                    <td>'{{$hasil->hasil_pemeriksaan}}</td>
                @endforeach
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>
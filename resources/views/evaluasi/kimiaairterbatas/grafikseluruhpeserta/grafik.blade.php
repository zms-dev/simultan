@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Sebaran Z-score</div>

                <div class="panel-body">
                	<div id="container"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
Highcharts.chart('container', {
	chart: {
	    type: 'column'
	},
	title: {
	    text: 'Grafik Sebaran Z-Score',
	},
    subtitle: {
        text: '{{$subtitle->nama_parameter}}',
        style: {
            "fontSize": "17px"
        }
    },
	xAxis: {
        categories: [
        	@foreach($perusahaan as $p)
        		'{{$p->kode_lebpes}}',
        	@endforeach
        ],
        labels: {
            rotation: -45
        }
    },
    yAxis: {
      title: {
        text : 'Z-Score'
      }
    },
	credits: {
		enabled: false
	},
	legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.3f}',
                rotation: -90,
                y: -20,
                crop: false,
                overflow: 'none'
            }
        }
    },
	series: [
		{
			name: 'Lebpes',
	    	data: [
	    		@foreach($data as $d)
	        		{{$d->zscore}},
	        	@endforeach
	    	],
		}
	]
});
</script>
@endsection

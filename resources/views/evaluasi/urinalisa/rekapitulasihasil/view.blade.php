<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
td, th{ 
    border: 1px solid #333;   
    text-align: center;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 2px 5px;
}
</style>
<table border="1">
    <thead>
        <tr>
            <th rowspan="3" colspan="2" width="30%">KRITERIA NILAI</th>
            <th colspan="4">Siklus {{$input['siklus']}} @if($input['periode'] == 2)Periode 2 @endif</th>
        </tr>
        <tr>
            <th colspan="2">Type 01</th>
            <th colspan="2">Type 02</th>
        </tr>
        <tr>
            <th>Jumlah</th>
            <th>%</th>
            <th>Jumlah</th>
            <th>%</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><span style="font-family: DejaVu Sans; sans-serif;">> 3.00</span></td>
            <td>Sangat Baik</td>
            <td>{{$dataa->SANGAT_BAIK}}</td>
            <td>
                <?php 
                    $per = ($dataa->SANGAT_BAIK / $kirima) * 100;
                    echo number_format($per, 2);
                ?> %
            </td>
            <td>{{$datab->SANGAT_BAIK}}</td>
            <td>
                <?php 
                    $per = ($datab->SANGAT_BAIK / $kirimb) * 100;
                    echo number_format($per, 2);
                ?> %
            </td>
        </tr>
        <tr>
            <td><span style="font-family: DejaVu Sans; sans-serif;">> 2.00 - 3.00</span></td>
            <td>Baik</td>
            <td>{{$dataa->BAIK}}</td>
            <td>
                <?php 
                    $per = ($dataa->BAIK / $kirima) * 100;
                    echo number_format($per, 2);
                ?> %
            </td>
            <td>{{$datab->BAIK}}</td>
            <td>
                <?php 
                    $per = ($datab->BAIK / $kirimb) * 100;
                    echo number_format($per, 2);
                ?> %
            </td>
        </tr>
        <tr>
            <td><span style="font-family: DejaVu Sans; sans-serif;">> 1.00 - 2.00</span></td>
            <td>Kurang</td>
            <td>{{$dataa->KURANG}}</td>
            <td>
                <?php 
                    $per = ($dataa->KURANG / $kirima) * 100;
                    echo number_format($per, 2);
                ?> %
            </td>
            <td>{{$datab->KURANG}}</td>
            <td>
                <?php 
                    $per = ($datab->KURANG / $kirimb) * 100;
                    echo number_format($per, 2);
                ?> %
            </td>
        </tr>
        <tr>
            <td><span style="font-family: DejaVu Sans; sans-serif;">&le; 1.00</span></td>
            <td>Buruk</td>
            <td>{{$dataa->BURUK}}</td>
            <td>
                <?php 
                    $per = ($dataa->BURUK / $kirima) * 100;
                    echo number_format($per, 2);
                ?> %
            </td>
            <td>{{$datab->BURUK}}</td>
            <td>
                <?php 
                    $per = ($datab->BURUK / $kirimb) * 100;
                    echo number_format($per, 2);
                ?> %
            </td>
        </tr>
        <tr>
            <td colspan="2">Jumlah Peserta</td>
            <td>{{$kirima}}</td>
            <td>100 %</td>
            <td>{{$kirimb}}</td>
            <td>100 %</td>
        </tr>
    </tbody>
</table>
</div>
<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
td, th{ 
    border: 1px solid #333;   
    text-align: center;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 2px 5px;
}
</style>
<table border="1">
    <thead>
        <tr>
            <th rowspan="3" colspan="2" width="30%">KRITERIA NILAI</th>
            <th colspan="4">Siklus {{$input['siklus']}} @if($input['periode'] == 2)Periode 2 @endif</th>
        </tr>
        <tr>
            <th colspan="2">Type 01</th>
            <th colspan="2">Type 02</th>
        </tr>
        <tr>
            <th>Jumlah</th>
            <th>%</th>
            <th>Jumlah</th>
            <th>%</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2">Kirim Hasil</td>
            <td>{{$kirima}}</td>
            <td>
                <?php 
                    $per = ($kirima / $data) * 100;
                    echo number_format($per, 2);
                ?> %
            </td>
            <td>{{$kirimb}}</td>
            <td>
                <?php 
                    $per = ($kirimb / $data) * 100;
                    echo number_format($per, 2);
                ?> %
            </td>
        </tr>
        <tr>
            <td colspan="2">Tidak Kirim Hasil</td>
            <td>{{$beluminput1}}</td>
            <td>
                <?php 
                    $per = ($beluminput1 / $data) * 100;
                    echo number_format($per, 2);
                ?> %
            </td>
            <td>{{$beluminput2}}</td>
            <td>
                <?php 
                    $per = ($beluminput2 / $data) * 100;
                    echo number_format($per, 2);
                ?> %
            </td>
        </tr>
        <tr>
            <td colspan="2">Jumlah Peserta</td>
            <td>{{$data}}</td>
            <td>100 %</td>
            <td>{{$data}}</td>
            <td>100 %</td>
        </tr>
    </tbody>
</table>
</div>
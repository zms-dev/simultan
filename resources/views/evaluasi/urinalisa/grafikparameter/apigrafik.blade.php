<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <script src="{{URL::asset('asset/js/jquery.min.js')}}"></script>
    <script src="{{URL::asset('asset/js/highstock.js')}}"></script>
    <script src="{{URL::asset('asset/js/exporting.js')}}"></script>
    <script src="{{URL::asset('asset/js/export-data.js')}}"></script>
    <script src="{{URL::asset('asset/js/highcharts-3d.js')}}"></script>
    <link rel="stylesheet" href="{{URL::asset('asset/css/bootstrap.css')}}">
    <style type="text/css">
        body{
            font-family: arial
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <a style="margin: 10px" onclick="javascript:printDiv('datana')"><li class="glyphicon glyphicon-print"></li></a>
                    <div class="panel-body" id="datana">
                        <style>
                            @media print{
                                
                                .color-red{
                                    -webkit-print-color-adjust: exact;
                                    background: #BF0B23 !important;display:inline-block;width:15px;height:25px;
                                }
                                
                                body,html{
                                    height:100%;

                                }
                                .print-area{
                                    display:block;
                                    page-break-after:always;
                                    position:relative;
                                    height:100%;
                                }
                                .panel-body{
                                    height:100%;
                                }

                                .print-area .main{
                                    position:absolute;
                                    left:0;
                                    right:0;
                                    height:100%;
                                    top:0;
                                }
                                .print-area .footer-content{
                                    position:absolute;
                                    bottom:0;
                                    left:0;
                                    right:0;
                                    border-top:1px solid #333;
                                }
                                .print-area .footer-content .left{
                                    float:left;
                                    width:50%;
                                }
                                .print-area .footer-content .right{
                                    float:right;
                                    width:50%;
                                    text-align:right;
                                }
                                .print-area .footer-content:after{
                                    content : " ";
                                    display : block;
                                    clear:both;
                                }
                                .col-md-4{
                                    width: 33.3% !important;
                                    float: left !important;
                                }
                            }
                            
                        </style>
                        <div class="print-area">
                            <div class="main">
                                <center><h4>HISTOGRAM HASIL URINALISA SEMUA PESERTA</h4></center>
                                <table>
                                    <tr>
                                        <th>KODE PESERTA </th>
                                        <th> : </th>
                                        <td>&nbsp;{{$peserta->kode_lebpes}}</td>
                                    </tr>
                                    <tr>
                                        <th>SIKLUS </th>
                                        <th> : </th>
                                        <td>&nbsp;{{$siklus}} - @if($type == 'a') 01 @else 02 @endif</td>
                                    </tr>
                                    <tr>
                                        <th>TAHUN </th>
                                        <th> : </th>
                                        <td>&nbsp;{{$date}}</td>
                                    </tr>
                                     <tr>
                                        <th>*<!-- <div class="color-red" style="background-color:#BF0B23;width: 20px;height: 20px;"></div> --></th>
                                        <th>:</th>
                                        <td>&nbsp;Posisi Peserta</td>
                                    </tr>
                                </table>
                                <hr>
                                <div class="row">
                                <?php $no = 0;?>
                                <?php $x = 0;?>
                                <?php $hal = ($type == 'a' ? 8 : 10);?>
                                @foreach($parameter as $val)
                                <?php $no++ ;?>
                                <?php $x++ ;?>
                                    <div class="col-md-4">
                                        <table style="margin: 10px 0px">
                                            <tr>
                                                <th>Parameter</th>
                                                <th>&nbsp;:</th>
                                                <td>&nbsp;{{$val->nama_parameter}}</td>
                                            </tr>
                                            <tr style="color: #bf0b23">
                                                <th>Target Parameter </th>
                                                <th>&nbsp;:</th>
                                                <td>&nbsp;{{$val->target->rujukan}}</td>
                                            </tr>
                                            <tr>
                                                <th>Hasil Saudara </th>
                                                <th>&nbsp;:</th>
                                                @if(count($val->hasilna) && $val->hasilna->hp != '-')
                                                <td>&nbsp;@if($val->hasilna->hp == NULL) {{$val->hasilna->hasil_pemeriksaan}} @else {{$val->hasilna->hp}} @endif *</td>
                                                @else
                                                    @if($val->id <= 10)
                                                        <td>&nbsp;-</td>
                                                    @else
                                                        <td>&nbsp;Tidak Mengerjakan</td>
                                                    @endif
                                                @endif
                                            </tr>
                                        </table>
                                        <div id="container{{$val->id}}"></div>
                                    </div>
                                @endforeach
                                </div>
                            </div>
                        </div>
                            <?php if($x < 6):$hal++;?>
                                </tr>
                            </table>
                        </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{URL('/js/jquery.printarea.js')}}"></script>
    <script type="text/javascript">
    function printDiv(divID) {
        var headElements = '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>,<meta name="viewport" content="width=device-width, initial-scale=1">';
        var options = { 
            mode : "popup", 
            popClose : true,
            extraHead : headElements 
        };
        $( '#'+divID ).printArea( options );


        
    }
    @foreach($parameter as $val)
        var chart = Highcharts.chart('container{{$val->id}}', {
            chart: {
                renderTo: 'container',
                "events" : {
                    load: function () {
                        console.log("as");
                        this.oldhasUserSize = this.hasUserSize;
                        this.resetParams = [this.chartWidth, this.chartHeight, false];
                        this.setSize(290, 350, false);
                    },
                }
            },
            title: {
                text: '{{$val->nama_parameter}}'
            },
            subtitle: {
                text: 'Plain'
            },
            yAxis: { //--- Primary yAxis
                title: {
                    text: 'Jumlah Peserta'
                }
            },
            xAxis: {
                categories: [
                @foreach($val->masing as $data)
                    "{{$data->hp}}",
                @endforeach
                ]
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y} {point.a}'
                    }
                }
            },

            series: [{type: 'column',name:'Hasil Peserta',

                data: [
                @foreach($val->masing as $data)
                    {
                    y: {{$data->Jumlah}},
                    @if(count($val->hasilna))
                        @if($val->hasilna->hp == NULL)
                            @if($val->hasilna->hasil_pemeriksaan == $data->hp)
                                a: "*",
                            @else
                            @endif
                        @else
                            @if($val->hasilna->hp == $data->hp)
                                a: "*",
                            @else
                            @endif
                        @endif
                    @else
                    @endif
                        <?php if($val->target->rujukan == $data->hp) { ?>
                            color: '#BF0B23'
                        <?php } ?>
                    },
                @endforeach
                ],
                showInLegend: true}]

        });
    @endforeach
    </script>
</body>
</html>
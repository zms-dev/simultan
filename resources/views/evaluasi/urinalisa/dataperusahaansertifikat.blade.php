@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Sertifikat Urinalisa Siklus {{$siklus}}</div>

                <div class="panel-body">
                    @if(Session::has('message'))
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ Session::get('message') }}
                      </div>
                    @endif
                    <form class="form-horizontal" method="get" enctype="multipart/form-data">
                          <div>
                                <label for="exampleInputEmail1">Pilih Tanggal Sertifikat :</label>
                                <div class="controls input-append date " data-link-field="dtp_input1">
                                    <input size="16" type="text" value="" readonly class="form-control form_datetime1" name="tanggal" id="tanggal">
                                    <span class="add-on"><i class="icon-th"></i></span>
                                </div>

                          </div><br>
                        <table class="table table-bordered">
                            <tr>
                                <th>No</th>
                                <th>Bidang</th>
                                <th><center>Aksi</center></th>
                            </tr>
                            @if(count($data))
                            <?php $no = 0; ?>
                            @foreach($data as $val)
                            <?php $no++ ?>

                            @if($val->id_bidang == '4')
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{$val->Bidang}}</td>
                                    @if($siklus == 1)
                                        @if($val->siklus_1 == 'done')
                                                <td>
                                                    <a href="" class="go-link" data-id="{{$val->perusahaan_id}}" data-x="a" data-y="1" data-link="{{$val->Link}}"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                                </td>
                                        @else
                                                <td>
                                                    <a href="" class="go-link" data-id="{{$val->perusahaan_id}}" data-x="a" data-y="1" data-link="{{$val->Link}}"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                                </td>
                                        <!-- <td class="text-center" style="padding: 2%;">Belum Dikirim</td> -->
                                        @endif
                                    @else
                                        @if($val->siklus_2 == 'done')
                                            @if($val->rpr1 == 'done')
                                                <td>
                                                    <a href="" class="go-link" data-id="{{$val->perusahaan_id}}" data-x="a" data-y="2" data-link="{{$val->Link}}"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                                </td>
                                            @else
                                                <td>
                                                    <a href="" class="go-link" data-id="{{$val->perusahaan_id}}" data-x="a" data-y="2" data-link="{{$val->Link}}"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                                </td>
                                            <!-- <td class="text-center" style="padding: 2%;">Belum Dikirim</td> -->
                                            @endif
                                        @else
                                        <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                        @endif
                                    @endif
                                </tr>
                            @endif
                            @endforeach
                            @else
                            <tr>
                                <td colspan="4">Data tidak ditemukan</td>
                            </tr>
                            @endif
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(".go-link").on("click",function(e){
    e.preventDefault()
    var x = $(this).data("x");
    var y = $(this).data("y");
    var idLink = $(this).data("id");
    var link = $(this).data("link");
    var tanggal = $("#tanggal").val();
    var baseUrl = "{{URL('')}}"+link+"/sertifikat/print/"+idLink+"?x="+x+"&y="+y+"&tanggal="+tanggal+"&download=true";
    var url = baseUrl;
    console.log(url);
    window.location.href = url;
})
$(".form_datetime1").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});
</script>
@endsection

@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body" id="datana">
                @if(count($skoring))
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{url('hasil-pemeriksaan/urinalisasi/evaluasi')}}/print/{{$id}}?x={{$type}}&y={{$siklus}}" >
                @else
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                @endif
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Parameter</th>
                                <th>Hasil Saudara</th>
                                <th>Reagen</th>
                                <th>Nilai Target</th>
                                <th>Skor</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($skoring as $val)
                                <input type="hidden"  name="id" value="{{ $val->id }}">
                            @endforeach
                            @if(count($data))
                            <?php $no = 0; $jumlah = 0;?>

                            @foreach($data as $val)
                            <?php $no++; ?>
                            <tr>
                                <td>{{$no}}<input type="hidden" name="id_detail[]" value="{{$val->id_detail}}"></td>
                                <td>{!!$val->nama_parameter!!}<input type="hidden" name="parameter_id[]" value="{{ $val->id }}" /></td>
                                <td>
                                    @if($val->id <= 10)
                                        @if( $val->Hasil != "Tidak Mengerjakan")
                                            @if($val->Hasil != "-")
                                                @if($val->id == $val->parameter_id)
                                                    {!!$val->hp!!}
                                                @else
                                                    {!!$val->Hasil!!}
                                                @endif
                                            @else
                                                -
                                                <?php $jumlah++; ?>
                                            @endif
                                        @else
                                            @if( $val->hp != NULL)
                                                {!!$val->hp!!}
                                            @else
                                                {{$val->Hasil}}
                                                <?php $jumlah++; ?>
                                            @endif
                                        @endif
                                    @else
                                        @if(count($val->hp))
                                            @if(count($val->hasilPemeriksaan))
                                                {!!$val->hp!!}
                                            @else
                                                {{$val->Hasil}}
                                            @endif
                                        @else
                                            Tidak mengerjakan
                                            <?php $jumlah++; ?>
                                        @endif
                                    @endif
                                </td>

                                <td>
                                    @if($val->id <= 10)
                                        @if($val->Hasil != "-")
                                            @if($val->hp_reagen == '-')
                                                -
                                            @else
                                                @if($val->imu_reagen != null || $val->reagen_lain != null)
                                                    {!!$val->imu_reagen!!}<br>
                                                    {!!$val->reagen_lain!!}
                                                @else
                                                    -
                                                @endif
                                            @endif
                                        @else
                                            -
                                        @endif
                                    @else
                                        @if(count($val->hp))
                                            @if($val->hp_reagen == '-')
                                                -
                                            @else
                                                {!!$val->imu_reagen!!}<br>
                                                {!!$val->reagen_lain!!}
                                            @endif
                                        @else
                                            -
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    @if($val->id <= 10)
                                        @if( $val->Hasil != "Tidak Mengerjakan")
                                            @if($val->Hasil != "-")
                                                @foreach($val->target as $target)
                                                    {{$target->rujukan}}
                                                @endforeach
                                            @else
                                            -
                                            @endif
                                        @else
                                            -
                                        @endif
                                    @else
                                        @if(count($val->hp))
                                            @foreach($val->target as $target)
                                                {{$target->rujukan}}
                                            @endforeach
                                        @else
                                        -
                                        @endif
                                    @endif
                                </td>
                                <td class="ngitung">

                                    @if($val->id <= 10)
                                        @if($val->id == 9)
                                            @if($val->Hasil != "-" ||  $val->Hasil == "Tidak Mengerjakan")
                                                <?php
                                                   if ($val->Hasil == "Tidak Mengerjakan") {
                                                      echo "-";
                                                    }else{
                                                        if ($val->hp != NULL && $val->parameter_id == $val->id) {
                                                            if ($val->hp >= $target->rujukan) {
                                                                $total = $val->hp - $target->rujukan;
                                                            }else{
                                                                $total = $target->rujukan - $val->hp ;
                                                            }
                                                        }else{
                                                            if ($val->Hasil >= $target->rujukan) {
                                                                $total = $val->Hasil - $target->rujukan;
                                                            }else{
                                                                $total = $target->rujukan - $val->Hasil ;
                                                            }
                                                        }
                                                        $total = number_format($total, 3);
                                                        if ($total == 0) {
                                                            echo "4";
                                                        }elseif ($total > 0 && $total <= 0.005) {
                                                            echo "3";
                                                        }elseif($total > 0.005 && $total <= 0.010){
                                                            echo "2";
                                                        }elseif($total > 0.010 && $total <= 0.015) {
                                                            echo "1";
                                                        }else{
                                                            echo "0";
                                                        }
                                                    }
                                                ?>
                                            @else
                                                -
                                            @endif
                                        @elseif($val->id == 10)
                                            @if($val->Hasil != "-" || $val->Hasil == "Tidak Mengerjakan")
                                                <?php
                                                    if ($val->Hasil == "Tidak Mengerjakan") {
                                                      echo "-";
                                                    }else{
                                                        if ($val->hp != NULL && $val->parameter_id == $val->id) {
                                                            if ($val->hp >= $target->rujukan) {
                                                                $total = $val->hp - $target->rujukan;
                                                            }else{
                                                                $total = $target->rujukan - $val->hp;
                                                            }
                                                        }else{
                                                            if ($val->Hasil >= $target->rujukan) {
                                                                $total = $val->Hasil - $target->rujukan;
                                                            }else{
                                                                $total = $target->rujukan - $val->Hasil ;
                                                            }
                                                        }
                                                        if ($total == 0) {
                                                            echo "4";
                                                        }elseif ($total > 0 && $total <= 0.5) {
                                                            echo "3";
                                                        }elseif($total > 0.5 && $total <= 1.0){
                                                            echo "2";
                                                        }elseif ($total > 1.0 && $total <= 1.5) {
                                                            echo "1";
                                                        }else{
                                                            echo "0";
                                                        }
                                                    }
                                                ?>
                                            @else
                                                -
                                            @endif
                                        @endif
                                    @else
                                        @if(count($val->hp))
                                            @if($target->rujukan == 'Negatif')
                                                <?php
                                                    if ($val->hp != $target->rujukan) {
                                                        echo "0";
                                                    }else{
                                                        echo "4";
                                                    }
                                                ?>
                                            @elseif($target->rujukan == 'Positif')
                                                <?php
                                                    if ($val->hp != $target->rujukan) {
                                                        echo "0";
                                                    }else{
                                                        echo "4";
                                                    }
                                                ?>
                                            @elseif($val->hp == 'Negatif')
                                                <?php
                                                    if ($val->hp != $target->rujukan) {
                                                        echo "0";
                                                    }else{
                                                        echo "4";
                                                    }
                                                ?>
                                            @else
                                                <?php
                                                    if($val->hp == $target->rujukan){
                                                        $total = "0";
                                                    }elseif ($val->hp >= $target->rujukan) {
                                                        $total = str_replace('+', '', $val->hp) - str_replace('+', '', $target->rujukan) ;
                                                    }else{
                                                        $total =  str_replace('+', '', $target->rujukan) - str_replace('+', '', $val->hp);
                                                    }
                                                    if ($total == "0") {
                                                        echo "4";
                                                    }elseif($total == "1" || $total == "-1"){
                                                        echo "3";
                                                    }elseif ($total == "2" || $total == "-2") {
                                                        echo "2";
                                                    }elseif ($total == "3" || $total == "-3") {
                                                        echo "1";
                                                    }
                                                ?>
                                            @endif
                                        @else
                                            -
                                        @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @endif
                            <tr>
                                <td colspan="5" style="text-align: right;">Skor Rata-rata :</td>
                                @if(count($skoring))
                                @foreach($skoring as $val)
                                    <td>{{$val->skoring}}</td>
                                @endforeach
                                @else
                                <td class="isi"></td>
                                <input type="hidden" name="skoring" id="skoring">
                                @endif
                            </tr>
                        </tbody>
                    </table>
                    <input type="hidden" value="{{$input['bahan']}}" name="bahan">

              <div class="form-horizontal">
                        <label>Komentar / Saran:</label>
                        <div class="row">
                            <div class="col-md-6">
                                @if(count($skoring))
                                    @foreach($skoring as $catatan)
                                        <textarea name="catatan" class="form-control" placeholder="-">{{ $catatan->catatan }}</textarea>
                                    @endforeach
                                @else
                                    <textarea name="catatan" class="form-control" placeholder="-"></textarea>
                                @endif
                            </div>
                        </div>
                    </div><br>

                @if(count($skoring))
                    <input type="submit" name="simpan" class="btn btn-primary" value="Print">
                    <input type="submit" name="simpan" class="btn btn-danger" value="Batal Evaluasi">
                @else
                    <input type="submit" name="simpan" class="btn btn-primary" value="Simpan">
                @endif
                {{ csrf_field() }}
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
form=document.getElementById("someForm");
var total2=[0];
$(document).ready(function(){
    var $dataRows=$("#datana tr:not('.titlerowna')");
    $dataRows.each(function() {
        $(this).find('.ngitung').each(function(i){
            if (isNaN($(this).html())) {
            }else{
                total2[i]+=parseInt( $(this).html());
                console.log(total2[i])
            }
        });
    });
    $("#datana td.isi").each(function(i){
        var data = total2[i] / ({{$no}} - {{$jumlah}});
        $(this).html(data.toFixed(2));
        $('#skoring').val(data.toFixed(2));
    });
});
</script>
@endsection

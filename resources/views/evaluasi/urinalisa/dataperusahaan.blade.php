@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluasi Urinalisa Siklus {{$siklus}}</div>

                <div class="panel-body">
                    @if(Session::has('message'))
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ Session::get('message') }}
                      </div>
                    @endif
                    <blockquote style="font-size: 12px">
                        Silahkan mengisi menu EVALUASI-TABEL RUJUKAN apabila data tidak tampil !
                    </blockquote>
                    <form class="form-horizontal" method="get" enctype="multipart/form-data">
                          {{-- <div>
                              <label for="exampleInputEmail1">Bahan :</label>
                                <select name="bahan" id="bahan" >
                                    <option value="Positif">Positif</option>
                                    <option value="Negatif">Negatif</option>
                                </select>
                          </div> --}}
                        <table class="table table-bordered">
                            <tr>
                                <th>No</th>
                                <th>Bidang</th>
                                <th colspan="2"><center>Aksi</center></th>
                            </tr>
                            @if(count($data))
                            <?php $no = 0; ?>
                            @foreach($data as $val)
                            <?php $no++ ?>

                            @if($val->id_bidang == '4')
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                    @if($siklus == 1)
                                        @if($val->siklus_1 == 'done')
                                            @if($val->pemeriksaan == 'done')
                                                <td>
                                                    <a href="{{URL('').$val->Link}}/evaluasi/{{$val->id}}?x=a&y=1&bahan=Negatif"><center><i class="glyphicon glyphicon-pencil"></i><br>I-01</center></a>
                                                </td>
                                            @else
                                            <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                            @endif
                                            @if($val->pemeriksaan2 == 'done')
                                                <td>
                                                    <a href="{{URL('').$val->Link}}/evaluasi/{{$val->id}}?x=b&y=1&bahan=Positif"><center><i class="glyphicon glyphicon-pencil"></i><br>I-02</center></a>
                                                </td>
                                            @else
                                            <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                            @endif
                                        @else
                                        <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                        @endif
                                    @else
                                        @if($val->siklus_2 == 'done')
                                            @if($val->rpr1 == 'done')
                                                <td>
                                                    <a href="{{URL('').$val->Link}}/evaluasi/{{$val->id}}?x=a&y=2&bahan=Negatif" class="go-link"><center><i class="glyphicon glyphicon-pencil"></i><br>II-01</center></a>
                                                </td>
                                            @else
                                            <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                            @endif
                                            @if($val->rpr2 == 'done')
                                                <td>
                                                    <a href="{{URL('').$val->Link}}/evaluasi/{{$val->id}}?x=b&y=2&bahan=Positif" class="go-link"><center><i class="glyphicon glyphicon-pencil"></i><br>II-02</center></a>
                                                </td>
                                            @else
                                            <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                            @endif
                                        @else
                                        <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                        @endif
                                    @endif
                                </tr>
                            @endif
                            @endforeach
                            @else
                            <tr>
                                <td colspan="4">Belum Input Hasil</td>
                            </tr>
                            @endif
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <script type="text/javascript">
    $(".go-link").on("click",function(e){
        e.preventDefault()
        var x = $(this).data("x");
        var y = $(this).data("y");
        var idLink = $(this).data("id");
        var link = $(this).data("link");
        var bahan = $("#bahan").val();
        var baseUrl = "{{URL('')}}"+link+"/evaluasi/"+idLink+"?x="+x+"&y="+y+"&bahan="+bahan;
        var url = baseUrl;
        console.log(url);
        window.location.href = url;
    })
</script> --}}
@endsection
<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <script src="{{URL::asset('asset/js/jquery.min.js')}}"></script>
    <script src="{{URL::asset('asset/js/highstock.js')}}"></script>
    <script src="{{URL::asset('asset/js/exporting.js')}}"></script>
    <script src="{{URL::asset('asset/js/export-data.js')}}"></script>
    <script src="{{URL::asset('asset/js/highcharts-3d.js')}}"></script>
    <link rel="stylesheet" href="{{URL::asset('asset/css/bootstrap.css')}}">
    <style type="text/css">
        body{
            font-family: arial
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <a style="margin: 10px" onclick="javascript:printDiv('datana')"><li class="glyphicon glyphicon-print"></li></a>
                    <div class="panel-body" id="datana">
                        <style type="text/css">
                            @media print {
                                .color-red{
                                    -webkit-print-color-adjust: exact;
                                    background: #BF0B23 !important;display:inline-block;width:15px;height:25px;
                                }
                            }
                        </style>
                        @foreach($reagen as $val)
                        <table style="margin: 10px 0px">
                            <tr>
                                <th>Kode Peserta</th>
                                <th>&nbsp;:</th>
                                <td>&nbsp;{{$peserta->kode_lebpes}}</td>
                            </tr>
                            <tr>
                                <th>Siklus </th>
                                <th>&nbsp;:</th>
                                <td>&nbsp;{{$siklus}} - @if($type == 'a') 01 @else 02 @endif</td>
                            </tr>
                            <tr>
                                <th>Tahun </th>
                                <th>&nbsp;:</th>
                                <td>&nbsp;{{$date}}</td>
                            </tr>
                            <tr>
                                <th><div class="color-red" style="background-color:#BF0B23;width: 20px;height: 20px;"></div></th>
                                <th>&nbsp;:</th>
                                <td>&nbsp;Posisi Peserta</td>
                            </tr>
                        </table>
                        @break
                        @endforeach
                        <div id="container"></div><br>
                        <div id="container1"></div>
                            @if($type== 'a')
                            <div style="position:fixed;bottom:0;" class="noprint">PNPME SIKLUS 1-2018 BBLK Surabaya</div>
                            <div style="position:fixed;bottom:0; margin-left: 82%;" class="noprint">halaman 5 dari 13</div>
                            @else
                            <div style="position:fixed;bottom:0;" class="noprint">PNPME SIKLUS 1-2018 BBLK Surabaya</div>
                            <div style="position:fixed;bottom:0; margin-left: 82%;" class="noprint">halaman 6 dari 13</div>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{URL('/js/jquery.printarea.js')}}"></script>
    <script type="text/javascript">
    function printDiv(divID) {
        var headElements = '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>,<meta name="viewport" content="width=device-width, initial-scale=1">';
        var options = { mode : "popup", popClose : true,extraHead : headElements };
        $( '#'+divID ).printArea( options );


        
    }
    var chart = Highcharts.chart('container', {
        @foreach($reagen as $val)
        title: {text: 'HISTOGRAM PENGGUNAAN REAGEN BIDANG URINALISA<br>BERDASARKAN KELOMPOK SELURUH PESERTA'},
        @break
        @endforeach
        chart: {
            "events" : {
                load: function () {
                    this.oldhasUserSize = this.hasUserSize;
                    this.resetParams = [this.chartWidth, this.chartHeight, false];
                },
            }
        },
        xAxis: {categories: [
            @foreach($reagen as $val)
            '{{$val->reagen}}',
            @endforeach]},
        yAxis: {
            title: {
                text: 'Jumlah Peserta'
            }
        },
        plotOptions: {
            series: {
                borderWidth: 0,dataLabels: {
                    enabled: true,
                    format:'{point.y}'
                }
            }
        },
        series: [{type: 'column',
            name: 'REAGEN PESERTA',
            data: [
            @foreach($reagen as $val)
                {
                y: {{$val->jumlah}},
                    @foreach($data as $valu)
                    <?php if($valu->id == $val->id) { ?>
                        color: '#BF0B23'
                    <?php } ?>
                    @break
                    @endforeach
                },
            @endforeach
            ],
            showInLegend: true}]
    });

    var chart = Highcharts.chart('container1', {
        @foreach($kehamilan as $val)
        title: {text: 'HISTOGRAM PENGGUNAAN REAGEN TEST KEHAMILAN BIDANG URINALISA <br>BERDASARKAN KELOMPOK SELURUH PESERTA'},
        @break
        @endforeach
        chart: {
            "events" : {
                load: function () {
                    this.oldhasUserSize = this.hasUserSize;
                    this.resetParams = [this.chartWidth, this.chartHeight, false];
                },
            }
        },
        xAxis: {categories: [
            @foreach($kehamilan as $val)
            '{{$val->reagen}}',
            @endforeach]},
        yAxis: {
            title: {
                text: 'Jumlah Peserta'
            }
        },
        plotOptions: {
            series: {
                borderWidth: 0,dataLabels: {
                    enabled: true,
                    format:'{point.y}'
                }
            }
        },
        series: [{type: 'column',
            name: 'REAGEN PESERTA',
            data: [
            @foreach($kehamilan as $val)
                {
                y: {{$val->jumlah}},
                    @foreach($data2 as $valu)
                    <?php if($valu->id == $val->id) { ?>
                        color: '#BF0B23'
                    <?php } ?>
                    @endforeach
                },
            @endforeach
            ],
            showInLegend: true}]

    });
    </script>
</body>
</html>
<style type="text/css">
@page {
margin: 150px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 5px;
    text-align: left;
    border: 1px solid #ddd;
}

#header {
    position: fixed;
    border-bottom:1px solid gray;
    padding-top: -140px
}
#footer {
    position: fixed;
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
#footer1 {
    position: fixed;
    bottom: 0px;
    width: 100%;
    left: 77%;
}
</style>
@if(count($skoring) > 0)
<div style="">
<div id="header" >
    <table width="100%" cellpadding="0" border="0" >
        <thead>
            <tr>
                <th>
                     <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px">
                </th>
                <td width="100%" style="padding-left: 20px">
                    <span style="font-size: 12px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 10px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL URINALISA SIKLUS {{$siklus}} TAHUN {{$date}}</b></span><br>
                    <span style="font-size: 10px">Penyelenggara : <b>Balai Besar Laboratorium Kesehatan Surabaya</b></span>
                    <div style="padding-left: 75px; margin-top: -15px; font-size: 10px">
                        <span><br/>Jalan Karangmenjangan No, 18 Surabaya - 60286<br>Telepon : (031) 5021451 | Fax : (031) 5020388<br>Web Online : simultan.bblksurabaya.id <br>Email : pme.bblksub@gmail.com</span>
                    </div>
                </td>
                <th>
                    <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" height="90px">
                </th>
            </tr>
            <tr>
                <th colspan="3"><hr></th>
            </tr>
        </thead>
    </table>
</div>
<div id="footer">
    PNPME Urinalisa SIKLUS {{$siklus}}-{{$date}} BBLK Surabaya
</div>
<div id="footer1">
    Lampiran @if($type=='a') 1 @else 2 @endif
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body">
                    <label>
                        <center>
                            <font size="10px">
                                <b>HASIL EVALUASI PNPME URINALISA SIKLUS {{$siklus}}-@if($type=='a')01 @else 02 @endif TAHUN {{$date}}</b><br><br>
                                KODE PESERTA : {{$datas->kode_lab}}
                            </font>
                        </center>
                    </label><br>
                <table class="table utama">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Parameter</th>
                            <th>Hasil Saudara</th>
                            <th>Reagen</th>
                            <th>Nilai Target</th>
                            <th>Skor</th>
                        </tr>
                    </thead>
                    <tbody>
                            @if(count($data))
                            <?php $no = 0; ?>
                            @foreach($data as $val)
                            <?php $no++; ?>
                            <tr>
                                <td>{{$no}}<input type="hidden" name="id_detail[]" value="{{$val->id_detail}}"></td>
                                <td>{!!$val->nama_parameter!!}<input type="hidden" name="parameter_id[]" value="{{ $val->id }}" /></td>
                                <td>
                                    @if($val->id <= 10)
                                        @if( $val->Hasil != "Tidak Mengerjakan")
                                            @if($val->id == $val->parameter_id)
                                                @if($val->Hasil != "-" && $val->hp != NULL)
                                                    {!!$val->hp!!}
                                                @else
                                                    -
                                                @endif
                                            @else
                                                {{$val->Hasil}}
                                            @endif
                                        @else
                                            @if( $val->hp != NULL && $val->id == $val->parameter_id)
                                                {!!$val->hp!!}
                                            @else
                                                {{$val->Hasil}}
                                            @endif
                                        @endif
                                    @else
                                        @if(count($val->hp))
                                            @if(count($val->hasilPemeriksaan))
                                                {!!$val->hp!!}
                                            @else
                                                {{$val->Hasil}}
                                            @endif
                                        @else
                                            Tidak mengerjakan
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    @if($val->id <= 10)
                                        @if($val->Hasil != "-")
                                            @if($val->hp_reagen == '-')
                                            -
                                            @else
                                                @if($val->imu_reagen != null || $val->reagen_lain != null)
                                                    {!!$val->imu_reagen!!}<br>
                                                    {!!$val->reagen_lain!!}
                                                @else
                                                -
                                                @endif
                                            @endif
                                        @else
                                            -
                                        @endif
                                    @else
                                        @if(count($val->hp))
                                            @if($val->hp_reagen == '-')
                                                -
                                            @else
                                                {!!$val->imu_reagen!!}<br>
                                                {!!$val->reagen_lain!!}
                                            @endif
                                        @else
                                            -
                                        @endif
                                    @endif                                    
                                </td>
                                <td>
                                    @if($val->id <= 10)
                                        @if( $val->Hasil != "Tidak Mengerjakan")
                                            @if($val->Hasil != "-")
                                                @foreach($val->target as $target)
                                                    {{$target->rujukan}}
                                                @endforeach
                                            @else
                                            -
                                            @endif
                                        @else
                                            -
                                        @endif
                                    @else
                                        @if(count($val->hp))
                                            @foreach($val->target as $target)
                                                {{$target->rujukan}}
                                            @endforeach
                                        @else
                                        -
                                        @endif
                                    @endif
                                </td>
                                <td class="ngitung">
                                    @if($val->id <= 10)
                                        @if($val->id == 9)
                                            @if($val->Hasil != "-")
                                                <?php
                                                    if($val->id == $val->parameter_id){
                                                        $hasil = $val->hp;
                                                    }else{
                                                        $hasil = $val->Hasil;
                                                    }
                                                    if ($hasil == "Tidak Mengerjakan") {
                                                      echo "-";
                                                    }else{
                                                        if ($hasil >= $target->rujukan) {
                                                            $total = $hasil - $target->rujukan;
                                                        }else{
                                                            $total = $target->rujukan - $hasil ;
                                                        }
                                                        $total = number_format($total, 3);
                                                        if ($total == 0) {
                                                            echo "4";
                                                        }elseif ($total > 0 && $total <= 0.005) {
                                                            echo "3";
                                                        }elseif($total > 0.005 && $total <= 0.010){
                                                            echo "2";
                                                        }elseif($total > 0.010 && $total <= 0.015) {
                                                            echo "1";
                                                        }else{
                                                            echo "0";
                                                        }
                                                    }
                                                ?>
                                            @else
                                                -
                                            @endif
                                        @elseif($val->id == 10)
                                            @if($val->Hasil != "-")
                                                <?php
                                                    if($val->id == $val->parameter_id){
                                                        $hasil = $val->hp;
                                                    }else{
                                                        $hasil = $val->Hasil;
                                                    }
                                                    if ($hasil == "Tidak Mengerjakan") {
                                                      echo "-";
                                                    }else{
                                                        if ($hasil >= $target->rujukan) {
                                                            $total = $hasil*1 - $target->rujukan*1;
                                                        }else{
                                                            $total =  str_replace('+', '', $target->rujukan) - str_replace('+', '', $hasil);
                                                            // $total = $target->rujukan*1 - $val->hp*1;
                                                        }
                                                        if ($total == 0) {
                                                            echo "4";
                                                        }elseif ($total > 0 && $total <= 0.5) {
                                                            echo "3";
                                                        }elseif($total > 0.5 && $total <= 1.0){
                                                            echo "2";
                                                        }elseif ($total > 1.0 && $total <= 1.5) {
                                                            echo "1";
                                                        }else{
                                                            echo "0";
                                                        }
                                                    }
                                                ?>
                                            @else
                                                -
                                            @endif
                                        @endif
                                    @else
                                         @if(count($val->hp))
                                            @if($target->rujukan == 'Negatif')
                                                <?php
                                                    if ($val->hp != $target->rujukan) {
                                                        echo "0";
                                                    }else{
                                                        echo "4";
                                                    }
                                                ?>
                                            @elseif($target->rujukan == 'Positif')
                                                <?php
                                                    if ($val->hp != $target->rujukan) {
                                                        echo "0";
                                                    }else{
                                                        echo "4";
                                                    }
                                                ?>
                                            @elseif($val->hp == 'Negatif')
                                                <?php
                                                    if ($val->hp != $target->rujukan) {
                                                        echo "0";
                                                    }else{
                                                        echo "4";
                                                    }
                                                ?>
                                            @else
                                                <?php
                                                    if($val->hp == $target->rujukan){
                                                        $total = "0";
                                                    }elseif ($val->hp >= $target->rujukan) {
                                                        $total = str_replace('+', '', $val->hp) - str_replace('+', '', $target->rujukan) ;
                                                    }else{
                                                        $total =  str_replace('+', '', $target->rujukan) - str_replace('+', '', $val->hp);
                                                    }
                                                    if ($total == "0") {
                                                        echo "4";
                                                    }elseif($total == "1" || $total == "-1"){
                                                        echo "3";
                                                    }elseif ($total == "2" || $total == "-2") {
                                                        echo "2";
                                                    }elseif ($total == "3" || $total == "-3") {
                                                        echo "1";
                                                    }
                                                ?>
                                            @endif
                                        @else
                                            -
                                        @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5" style="text-align: right;">Skor Rata-rata : </td>
                            @if(count($skoring))
                                @foreach($skoring as $val)
                                    <td>{{$val->skoring}}</td>
                                @endforeach
                                @else
                                <td class="isi"></td>
                                <input type="hidden" name="skoring" id="skoring">
                                @endif
                        </tr>
                    </tfoot>
                </table><br>
                <table>
                    <tr>
                        <th>Komentar / Saran: </th>
                    </tr>
                    <tr>
                        <td>@if($val->catatan == '')
                            -
                            @else
                            {{ $val->catatan }}
                            @endif
                        </td>
                    </tr>
                </table>
                <br><br>
                    <p style="position: relative; left: 3%;">
                        Kriteria Skor<br>
                         > 3,00 = Sangat Baik<br>
                         > 2,00 – 3,00 = Baik<br>
                         > 1,00 – 2,00 = Kurang<br>
                         <span style="font-family: DejaVu Sans; sans-serif;">&le; 1,00 = Buruk</span>
                    </p>

                    <div style="position: relative; left: 81%; top: -15%;">
                    <p>
                        <div style="position: relative; top: 22px">
                        Surabaya, {{$ttd->tanggal}} 
                        {{$date}}<br>
                        Manajer Teknis
                        </div>
                        <p>
                            <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.'ttdevaluasi'.DIRECTORY_SEPARATOR.$ttd->image)}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'backend'.DIRECTORY_SEPARATOR.'ttdevaluasi'.DIRECTORY_SEPARATOR.$ttd->image)}}" width="120" height="120" style="margin-left: -20px !important;">
                        </p>

                        <div style="position: relative; top:-40px">
                            {{$ttd->nama}}<br>NIP. {{$ttd->nip}}
                        </div>
                    </p>
                    </div>

            </div>
        </div>
    </div>
</div>
</div>
@else
<h1><center>Sedang Dievaluasi Kembali</center></h1>
@endif
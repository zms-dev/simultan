@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Skoring Hasil PNPME Seluruh Peserta</div>
                <a style="margin: 10px" onclick="javascript:printDiv('datana')"><li class="glyphicon glyphicon-print"></li></a>
                
                <div class="panel-body" id="datana">
                    <div id="container"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{URL('/js/jquery.printarea.js')}}"></script>
<script type="text/javascript">
function printDiv(divID) {
    var headElements = '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>,<meta name="viewport" content="width=device-width, initial-scale=1">';
    var options = { mode : "popup", popClose : true,extraHead : headElements };
    $( '#'+divID ).printArea( options );


    
}
var chart = Highcharts.chart('container', {
  @foreach($data as $val)
  title: {text: 'GRAFIK SCORING HASIL PNPME BIDANG URINALISA <br>BERDASARKAN KELOMPOK SELURUH PESERTA <br>SIKLUS {{$siklus}} -@if($type == "a") 01 @else 02 @endif TAHUN {{$tahun}}'},
  @break
  @endforeach
  chart: {
        type: 'column',
        "events" : {
            load: function () {
                console.log("as");
                this.oldhasUserSize = this.hasUserSize;
                this.resetParams = [this.chartWidth, this.chartHeight, false];
                this.setSize(800, 400, false);
            },
        }
    },
  subtitle: {text: ''},
  xAxis: {categories: [
      @foreach($data as $val)
      '{{$val->nilainya}}',
      @endforeach]},
  yAxis: {
    title: {
      text : 'Jumlah Peserta'
    }
  },
  plotOptions: {
      series: {
          borderWidth: 0,dataLabels: { 
              enabled: true,
              format:'{point.y}'
          }
      }
  },
  series: [{type: 'column',
      name : 'Kriteria Nilai',
      data: [
      @foreach($data as $val)
          {
          y: {{$val->nilai1}},
          },
      @endforeach
      ],
      showInLegend: true}]
});
</script>
@endsection
@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Penggunaan Reagen Kehamilan Peserta</div>

                <div class="panel-body" id="datana">
                    <div id="container"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var chart = Highcharts.chart('container', {
    @foreach($reagen as $val)
    title: {text: 'Penggunaan Reagen Kehamilan Peserta {{$val->kode_lab}}'},
    @break
    @endforeach
    subtitle: {text: 'Plain'},
    xAxis: {categories: [
        @foreach($reagen as $val)
        '{{$val->reagen}}',
        @endforeach]},
    plotOptions: {
        series: {
            borderWidth: 0,dataLabels: { 
                enabled: true,
                format:'{point.y}'
            }
        }
    },
    series: [{type: 'column',
        data: [
        @foreach($reagen as $val)
            {
            y: {{$val->jumlah}},
                @foreach($data as $valu)
                <?php if($valu->id == $val->id) { ?>
                    color: '#BF0B23'
                <?php } ?>
                @endforeach
            },
        @endforeach
        ],
        showInLegend: true}]

});
</script>
@endsection